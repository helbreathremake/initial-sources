﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace GameUpdater
{
    public partial class Form1 : Form
    {
        private string server;
        private string mainUrl;
        private string updateUrl;
        private string listUrl;
        private string currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\";
        private string updateDirectory = @"Update\";
        private Dictionary<string, string> files;
        private Dictionary<string, string> filesToInstall;

        private System.Threading.Timer t;
        private UpdateState state;
        private enum UpdateState
        {
            Waiting,
            Starting,
            Checking,
            Downloading,
            Updating,
            Ready
        }

        public Form1()
        {
            InitializeComponent();

            state = UpdateState.Starting;
            progressBar1.Maximum = 100;

            t = new System.Threading.Timer(new TimerCallback(Tick), null, 1000, 200);

            LoadLoginConfiguration();
            mainUrl = "http://" + server + ":6830/Splash.aspx";
            updateUrl = "http://" + server + ":6830/update/";
            listUrl = "http://" + server + ":6830/update/updatelist.xml";
            webBrowser1.Navigate(mainUrl);

            lblName.Text = "Helbreath Champions Launcher v" + Assembly.GetExecutingAssembly().GetName().Version;
        }

        private void LoadLoginConfiguration()
        {
            try
            {
                string installPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");

                if (!File.Exists(installPath + @"\login.xml")) return;

                XmlDocument document = new XmlDocument();
                document.Load(installPath + @"\login.xml");

                // server
                bool resolveDNS = false;
                XmlNode node = document.DocumentElement.SelectSingleNode("//Server");
                if (node.Attributes["ResolveDNS"] != null)
                    resolveDNS = Boolean.Parse(node.Attributes["ResolveDNS"].InnerText);

                if (resolveDNS)
                {

                    IPHostEntry hostEntry = new IPHostEntry();
                    hostEntry.AddressList = Dns.GetHostAddresses(node.InnerText);

                    if (hostEntry.AddressList.Length > 0)
                    {
                        server = hostEntry.AddressList[0].ToString();
                    }
                }
                else
                {
                    server = node.InnerText;
                }
            }
            catch
            {
            }
        }

        public void Tick(object o)
        {
            switch (state)
            {
                case UpdateState.Starting:
                    if (!Directory.Exists(updateDirectory)) Directory.CreateDirectory(updateDirectory);
                    UpdateStatus("Checking for updates...");
                    UpdateStatusBar(5);
                    state = UpdateState.Waiting;
                    ThreadPool.QueueUserWorkItem(new WaitCallback(CheckForUpdates));
                    break;
                case UpdateState.Checking:
                    if (files != null && files.Count > 0)
                    {
                        UpdateStatus("Checking " + files.Count + " files...");
                        state = UpdateState.Waiting;
                        ThreadPool.QueueUserWorkItem(new WaitCallback(DownloadUpdates));
                    }
                    else
                    {
                        UpdateStatus("No files to check");
                        state = UpdateState.Ready;
                    }
                    break;
                case UpdateState.Downloading:
                    if (filesToInstall != null && filesToInstall.Count > 0)
                    {
                        UpdateStatus("Installing " + filesToInstall.Count + " files...");
                        state = UpdateState.Waiting;
                        ThreadPool.QueueUserWorkItem(new WaitCallback(InstallUpdates));
                    }
                    else
                    {
                        UpdateStatus("No files to install");
                        state = UpdateState.Ready;
                    }
                    break;
                case UpdateState.Updating:
                    state = UpdateState.Ready;
                    break;
                case UpdateState.Ready:
                    UpdateStatus("Client ready");
                    UpdateStatusBar(5);
                    EnableLaunchButton();
                    t.Dispose();
                    break;
            }            
        }

        public void CheckForUpdates(object o)
        {
            try
            {
                files = new Dictionary<string, string>();
                XmlTextReader reader = new XmlTextReader(listUrl);
                while (reader.Read())
                    if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("file"))
                    {
                        if (File.Exists(currentDirectory + reader["name"]))
                        {
                            var versionInfo = FileVersionInfo.GetVersionInfo(currentDirectory + reader["name"]);
                            string version = versionInfo.ProductVersion;
                            if (reader["version"] != null)
                            {
                                if (version != reader["version"])
                                    files.Add(reader["name"], reader["version"]);
                            }
                            else if (reader["force"] != null && Boolean.Parse(reader["force"]))
                                files.Add(reader["name"], "");
                        }
                        else files.Add(reader["name"], "");
                    }
            }
            catch (Exception ex) 
            { 
                Console.WriteLine(ex.ToString()); 
            }

            state = UpdateState.Checking;
        }

        public void DownloadUpdates(object o)
        {
            filesToInstall = new Dictionary<string, string>();

            DateTime localFileModifiedTime, onlineFileModifiedTime;
            WebClient client = new WebClient();
            foreach (KeyValuePair<string,string> file in files)
            {
                try
                {
                    if (file.Key.Contains("Font"))
                    {
                        int asd = 123;
                    }

                    if (File.Exists(currentDirectory + file.Key.Replace('/', '\\')))
                    {
                        HttpWebRequest gameFile = (HttpWebRequest)WebRequest.Create(updateUrl + file.Key);
                        using (HttpWebResponse gameFileResponse = (HttpWebResponse)gameFile.GetResponse())
                        {
                            localFileModifiedTime = File.GetLastWriteTime(currentDirectory + file.Key.Replace('/', '\\'));
                            onlineFileModifiedTime = gameFileResponse.LastModified;
                        }

                        if (localFileModifiedTime < onlineFileModifiedTime)
                        {
                            UpdateStatus("Downloading " + file.Key + "...");

                            if (file.Key.Contains('/'))
                            {
                                string newDir = file.Key.Substring(0, file.Key.IndexOf('/'));
                                if (!Directory.Exists(currentDirectory + updateDirectory + newDir))
                                    Directory.CreateDirectory(currentDirectory + updateDirectory + newDir);
                            }
                            client.DownloadFile(updateUrl + file.Key, currentDirectory + updateDirectory + file.Key.Replace('/', '\\'));

                            filesToInstall.Add(file.Key, file.Value);
                        }
                    }
                    else
                    {
                        UpdateStatus("Downloading " + file.Key + "...");

                        if (file.Key.Contains('/'))
                        {
                            try
                            {
                                string newDir = file.Key.Substring(0, file.Key.LastIndexOf('/'));
                                if (!Directory.Exists(currentDirectory + updateDirectory + newDir))
                                    Directory.CreateDirectory(currentDirectory + updateDirectory + newDir);
                            }
                            catch { }
                        }
                        client.DownloadFile(updateUrl + file.Key, currentDirectory + updateDirectory + file.Key.Replace('/', '\\'));
                        filesToInstall.Add(file.Key, file.Value);
                    }
                }
                catch (Exception ex) { }

                UpdateStatusBar((int)((double)25 / (double)files.Count));
            }

            state = UpdateState.Downloading;
        }

        public void InstallUpdates(object o)
        {
            foreach (KeyValuePair<string, string> file in files)
            {
                UpdateStatus("Installing " + file.Key + "...");
                try
                {
                    File.Copy(currentDirectory + updateDirectory + file.Key.Replace('/', '\\'), currentDirectory + file.Key.Replace('/', '\\'), true);
                }
                catch { }

                UpdateStatusBar((int)((double)25 / (double)files.Count));
            }

            state = UpdateState.Updating;
        }

        public void UpdateStatus(string text)
        {
            if (lblProgress.InvokeRequired)
            {
                lblProgress.Invoke((MethodInvoker)delegate
                {
                    lblProgress.Text = text;
                });
            }
            else lblProgress.Text = text;
        }

        public void UpdateStatusBar(int step)
        {
            if (progressBar1.InvokeRequired)
            {
                progressBar1.Invoke((MethodInvoker)delegate
                {
                    progressBar1.Step = step;
                    progressBar1.PerformStep();
                });
            }
            else
            {
                progressBar1.Step = step;
                progressBar1.PerformStep();
            }
        }

        public void EnableLaunchButton()
        {
            if (progressBar1.InvokeRequired)
            {
                progressBar1.Invoke((MethodInvoker)delegate
                {
                    progressBar1.Value = progressBar1.Maximum;
                    button2.ForeColor = Color.White;
                });
            }
            else
            {
                progressBar1.Value = progressBar1.Maximum;
                button2.ForeColor = Color.White;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_MouseHover(object sender, EventArgs e)
        {
            if (((Button)sender).ForeColor != Color.DimGray)
                ((Button)sender).ForeColor = Color.DarkOrange;
        }

        private void button_MouseLeave(object sender, EventArgs e)
        {
            if (((Button)sender).ForeColor != Color.DimGray)
                ((Button)sender).ForeColor = Color.White;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (((Button)sender).ForeColor != Color.DimGray)
                Process.Start(currentDirectory + "HelGame.exe", "UpToDate");
        }
    }
}
