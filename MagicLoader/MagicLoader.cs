﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using HelbreathWorld;
using HelbreathWorld.Common.Assets;

namespace MagicLoader
{
    public partial class MagicLoader : Form
    {
        List<Magic> magicCfg;
        public MagicLoader()
        {
            InitializeComponent();

            magicCfg = new List<Magic>();

            dataGridView1.Columns.Add("Index", "Index");
            dataGridView1.Columns.Add("Name", "Name");
            dataGridView1.Columns.Add("Type", "Type");
            dataGridView1.Columns.Add("Delay", "Delay");
            dataGridView1.Columns.Add("Lasts", "Lasts");
            dataGridView1.Columns.Add("Mana", "Mana");
            dataGridView1.Columns.Add("RangeY", "RangeY");
            dataGridView1.Columns.Add("RangeX", "RangeX");
            dataGridView1.Columns.Add("Effect1", "Effect1");
            dataGridView1.Columns.Add("Effect2", "Effect2");
            dataGridView1.Columns.Add("Effect3", "Effect3");
            dataGridView1.Columns.Add("Effect4", "Effect4");
            dataGridView1.Columns.Add("Effect5", "Effect5");
            dataGridView1.Columns.Add("Effect6", "Effect6");
            dataGridView1.Columns.Add("Effect7", "Effect7");
            dataGridView1.Columns.Add("Effect8", "Effect8");
            dataGridView1.Columns.Add("Effect9", "Effect9");
            dataGridView1.Columns.Add("RequiredInt", "RequiredInt");
            dataGridView1.Columns.Add("Cost", "Cost");
            dataGridView1.Columns.Add("Category", "Category");
            dataGridView1.Columns.Add("Attribute", "Attribute");
            dataGridView1.Columns.Add("IgnorePFM", "IgnorePFM");
        }

        private void StartOpenFile(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void StartSaveFile(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void LoadFile(object sender, CancelEventArgs e)
        {
            string filename = openFileDialog1.FileName;

            StreamReader file = new StreamReader(filename);

            string line;
            while ((line = file.ReadLine()) != null)
            {
                if (line.ToLower().StartsWith("magic"))
                {
                    string[] tokens = line.Replace('\t', ' ').Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens.Length < 4) continue;
                    Magic magic = new Magic(Int32.Parse(tokens[2]));
                    magic.Name = tokens[3];
                    magic.Type = (MagicType)Int32.Parse(tokens[4]);
                    magic.DelayTime = new TimeSpan(0, 0, Int32.Parse(tokens[5]));
                    magic.LastTime = new TimeSpan(0, 0, Int32.Parse(tokens[6]));
                    magic.ManaCost = Int32.Parse(tokens[7]);
                    magic.RangeY = Int32.Parse(tokens[8]);
                    magic.RangeX = Int32.Parse(tokens[9]);
                    magic.Effect1 = Int32.Parse(tokens[10]);
                    magic.Effect2 = Int32.Parse(tokens[11]);
                    magic.Effect3 = Int32.Parse(tokens[12]);
                    magic.Effect4 = Int32.Parse(tokens[13]);
                    magic.Effect5 = Int32.Parse(tokens[14]);
                    magic.Effect6 = Int32.Parse(tokens[15]);
                    magic.Effect7 = Int32.Parse(tokens[16]);
                    magic.Effect8 = Int32.Parse(tokens[17]);
                    magic.Effect9 = Int32.Parse(tokens[18]);
                    magic.RequiredIntelligence = Int32.Parse(tokens[19]);
                    magic.GoldCost = Int32.Parse(tokens[20]);
                    magic.Category = (MagicCategory)Int32.Parse(tokens[21]);
                    magic.Attribute = (MagicAttribute)Int32.Parse(tokens[22]);
                    magic.IgnorePFM = (bool)(magic.Index > 80);

                    magicCfg.Add(magic);

                    dataGridView1.Rows.Add(
                        new object[]
                    {
                        magic.Index,
                        magic.Name,
                        magic.Type,
                        magic.DelayTime,
                        magic.LastTime,
                        magic.ManaCost,
                        magic.RangeY,
                        magic.RangeX,
                        magic.Effect1,
                        magic.Effect2,
                        magic.Effect3,
                        magic.Effect4,
                        magic.Effect5,
                        magic.Effect6,
                        magic.Effect7,
                        magic.Effect8,
                        magic.Effect9,
                        magic.RequiredIntelligence,
                        magic.GoldCost,
                        magic.Category,
                        magic.Attribute,
                        magic.IgnorePFM
                    }
                        );
                }
            }

            file.Close();

        }

        private void SaveFile(object sender, CancelEventArgs e)
        {
            string filename = saveFileDialog1.FileName;

            TextWriter file = new StreamWriter(filename);

            file.WriteLine("<Spells>");

            foreach (Magic magic in magicCfg)
            {
                StringBuilder line = new StringBuilder();
                line.Append("   <Spell Name=\"" + magic.Name + "\" ");
                line.Append("Index=\"" +magic.Index + "\" ");
                line.Append("Type=\"" + Enum.GetName(typeof(MagicType), magic.Type) + "\" ");
                line.Append("DelayTime=\"" + magic.DelayTime + "\" ");
                line.Append("LastTime=\"" + magic.LastTime + "\" ");
                line.Append("ManaCost=\"" + magic.ManaCost + "\" ");
                line.Append("RangeY=\"" + magic.RangeY + "\" ");
                line.Append("RangeX=\"" + magic.RangeX + "\" ");
                line.Append("Effect1=\"" + magic.Effect1 + "\" ");
                line.Append("Effect2=\"" + magic.Effect2 + "\" ");
                line.Append("Effect3=\"" + magic.Effect3 + "\" ");
                line.Append("Effect4=\"" + magic.Effect4 + "\" ");
                line.Append("Effect5=\"" + magic.Effect5 + "\" ");
                line.Append("Effect6=\"" + magic.Effect6 + "\" ");
                line.Append("Effect7=\"" + magic.Effect7 + "\" ");
                line.Append("Effect8=\"" + magic.Effect8 + "\" ");
                line.Append("Effect9=\"" + magic.Effect9 + "\" ");
                line.Append("RequiredIntelligence=\"" + magic.RequiredIntelligence + "\" ");
                line.Append("GoldCost=\"" + magic.GoldCost + "\" ");
                line.Append("Category=\"" + Enum.GetName(typeof(MagicCategory), magic.Category) + "\" ");
                line.Append("Attribute=\"" + Enum.GetName(typeof(MagicAttribute), magic.Attribute)  + "\" ");
                line.Append("IgnorePFM=\"" + magic.IgnorePFM + "\" ");
                line.Append(" />");

                file.WriteLine(line.ToString());
            }

            file.WriteLine("</Spells>");

            file.Close();
        }
    }
}
