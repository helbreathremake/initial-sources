﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

using HelbreathWorld;
using HelbreathWorld.Data;
using HelbreathWorld.Common;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Common.Assets.Objects;

namespace HelbreathWorld.Server
{
    public class LoginServer
    {
        private bool running;
        private DateTime startTime;

        public delegate void LogHandler(string message, LogType logType);
        public event LogHandler MessageLogged;

        private Queue<Command> messages;
        private object messageLock;
        private Thread mainThread;
        private int clientPort, gamePort;
        private ServerInfo clientServer;
        private ServerInfo gameServer;

        private Connection connection;
        private AccountData accountData;
        private CharacterData characterData;
        private Dictionary<String,Account> accounts;
        private List<Game> gameServers;

        public Boolean Running
        {
            get { return running; }
        }

        public DateTime StartTime
        {
            get { return startTime; }
        }


        public LoginServer(int clientPort, int gamePort, string connectionString)
        {
            this.clientPort = clientPort;
            this.gamePort = gamePort;
            this.messageLock = new object();
            this.running = false;

            this.connection = new Connection(connectionString);
            this.connection.MessageLogged += new Connection.LogHandler(LogConnectionMessage);
            this.accountData = new AccountData(connection);
            this.characterData = new CharacterData(connection);
            this.accounts = new Dictionary<String, Account>();
            this.gameServers = new List<Game>();

            LoadItemConfiguration();
        }

        private bool LoadItemConfiguration()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\items.xml";

                World.ItemConfiguration = ItemHelper.LoadItems(configPath);

                return true;
            }
            catch (Exception ex)
            {
                LogMessage("Error loading items: " + ex.ToString(), LogType.Error);
                return false;
            }
        }

        /// <summary>
        /// Initializes async server listener for incoming connections for clients and game servers.
        /// </summary>
        public void Start()
        {
            try
            {
                if (connection.Test())
                {
                    LogMessage("Test to SQL Server Successful");
                    clientServer = new ServerInfo(clientPort);
                    clientServer.ClientReady += new ClientEvent(ClientReady);
                    clientServer.DefaultEncryptionType = EncryptionType.None; // client uses custom encryption

                    gameServer = new ServerInfo(gamePort);
                    gameServer.ClientReady += new ClientEvent(GameReady);
                    gameServer.DefaultEncryptionType = EncryptionType.None;

                    messages = new Queue<Command>();
                    //mainThread = new Thread(new ThreadStart(MainLoop));
                    //mainThread.Start();

                    this.running = true;
                    this.startTime = DateTime.Now;

                    LogMessage("Login server started successfully. Listening for clients on port " + clientPort);


                }
                else LogMessage("CRITICAL ERROR: Test to SQL Server Failed. Server will not start.");
            }
            catch (Exception ex)
            {
                LogMessage("CRITICAL ERROR: Error starting login server on port " + clientPort + ". " + ex.ToString(), LogType.Error);
            }
        }

        /// <summary>
        /// Legacy Helbreath command handler to split up data in byte array. Since we have no character for splitting data,
        /// we imitate the byte pointer by using BitConverter to collect the bytes. This is possible as we know the index, 
        /// length and data type by reading the client's C++ code. Amend accordingly.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="clientID"></param>
        private void HandleCommand(byte[] data, int clientID)
        {
            string username, password, world, gender, token, version;
            int typeTemp = (int)BitConverter.ToInt32(data, 0);
            CommandType type;

            if (Enum.TryParse<CommandType>(typeTemp.ToString(), out type))
                try
                {
                    switch (type)
                    {
                        // first data is 2 bytes after the type. not sure what it is at the moment, so the first index is 6 for data.
                        // use Trim('\0') to remove null bytes from strings
                        case CommandType.RequestLogin:
                            username = Encoding.ASCII.GetString(data, 6, 10).Trim('\0');
                            password = Encoding.ASCII.GetString(data, 16, 10).Trim('\0');
                            world = Encoding.ASCII.GetString(data, 26, 30).Trim('\0');
                            version = Encoding.ASCII.GetString(data, 56, 10).Trim('\0');
                            //string preshared = Encoding.ASCII.GetString(data, 56, 69).Trim('\0');

                            LogMessage("Client " + clientID + ": Login Requested as [" + username + "]");

                            string thisVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                            if (version.Equals(thisVersion))
                                Login(clientID, username, password);// , preshared);
                            else Send(clientID, CommandType.IncorrectVersion);
                            break;
                        case CommandType.CreateAccount:
                            username = Encoding.ASCII.GetString(data, 6, 10).Trim('\0');
                            password = Encoding.ASCII.GetString(data, 16, 10).Trim('\0');
                            string email = Encoding.ASCII.GetString(data, 26, 50).Trim('\0');
                            gender = Encoding.ASCII.GetString(data, 76, 10).Trim('\0');
                            string age = Encoding.ASCII.GetString(data, 86, 10).Trim('\0');
                            string str1 = Encoding.ASCII.GetString(data, 96, 4).Trim('\0');
                            string str2 = Encoding.ASCII.GetString(data, 100, 2).Trim('\0');
                            string str3 = Encoding.ASCII.GetString(data, 102, 2).Trim('\0');
                            string country = Encoding.ASCII.GetString(data, 104, 17).Trim('\0');
                            string ssn = Encoding.ASCII.GetString(data, 121, 28).Trim('\0');
                            string quiz = Encoding.ASCII.GetString(data, 149, 45).Trim('\0');
                            string answer = Encoding.ASCII.GetString(data, 194, 20).Trim('\0');
                            //token = Encoding.ASCII.GetString(data, 214, 50).Trim('\0');

                            CreateAccount(clientID, username, password, email, quiz, answer);
                            break;
                        case CommandType.ChangePassword:
                            username = Encoding.ASCII.GetString(data, 6, 10).Trim('\0');
                            password = Encoding.ASCII.GetString(data, 16, 10).Trim('\0');
                            string newPw = Encoding.ASCII.GetString(data, 26, 10).Trim('\0');
                            string newPwConfirm = Encoding.ASCII.GetString(data, 36, 10).Trim('\0');

                            ChangePassword(clientID, username, password, newPw, newPwConfirm);
                            break;
                        case CommandType.CreateCharacter:
                            username = Encoding.ASCII.GetString(data, 16, 10).Trim('\0');
                            password = Encoding.ASCII.GetString(data, 26, 10).Trim('\0');
                            //world = Encoding.ASCII.GetString(data, 36, 30).Trim('\0');

                            Character character = new Character();
                            character.Parse(data);
                            CreateCharacter(clientID, username, password, character);

                            break;
                        case CommandType.RequestEnterGame:
                            string characterName = Encoding.ASCII.GetString(data, 6, 10).Trim('\0');
                            string mapName = Encoding.ASCII.GetString(data, 16, 10).Trim('\0');
                            username = Encoding.ASCII.GetString(data, 26, 10).Trim('\0');
                            password = Encoding.ASCII.GetString(data, 36, 10).Trim('\0');
                            int level = BitConverter.ToInt32(data, 46);
                            world = Encoding.ASCII.GetString(data, 50, 30).Trim('\0');
                            token = Encoding.ASCII.GetString(data, 80, 120).Trim('\0');

                            EnterGame(clientID, username, characterName, mapName);
                            break;
                        case CommandType.DeleteCharacter:
                            string deleteName = Encoding.ASCII.GetString(data, 6, 10).Trim('\0');
                            username = Encoding.ASCII.GetString(data, 16, 10).Trim('\0');
                            DeleteCharacter(clientID, username, deleteName);
                            break;
                        default:
                            LogMessage("Unknown Command: " + string.Format("0x{0:x8}", typeTemp));
                            break;
                    }
                }
                catch (Exception ex)
                {
                    LogMessage("Command failed: " + string.Format("0x{0:x8}", typeTemp) + ". " + ex.ToString(), LogType.Error);
                }
            else
            {
                LogMessage("Unknown Command: " + string.Format("0x{0:x8}", typeTemp));
            }
        }

        private void EnterGame(int clientID, string username, string characterName, string mapName)
        {
            // TODO - somehow determine if player is logged in already.
            // then Send(clientID, CommandType.EnterGameAlreadyLoggedIn);

            bool found = false;
            foreach(Game game in gameServers)
                foreach(Map map in game.Maps)
                    if (map.Name.Equals(mapName))
                    {
                        Send(clientID, CommandType.EnterGameSucceed, game.GetEnterGameSucceedData());
                        found = true;
                        break;
                    }

            if (!found) Send(clientID, CommandType.EnterGameFailed, new byte[1] { (byte)3 }); // 3 = game server unavailable
            else
            {
                // update last login
                characterData.UpdateLastLogin(characterName);
            }
        }

        private void DeleteCharacter(int clientID, string username, string characterName)
        {
            // check cache first
            if (accounts.ContainsKey(username))
            {
                if (!accounts[username].Characters.ContainsKey(characterName))
                    Send(clientID, CommandType.DeleteCharacterFailed);
            }

            if (characterData.DeleteCharacter(username, characterName))
            {
                // clear from cache
                if (accounts.ContainsKey(username)) accounts[username].Characters.Remove(characterName);

                Send(clientID, CommandType.DeleteCharacterSucceed);
            }
        }

        private void CreateCharacter(int clientID, string username, string password, Character character)
        {
            if (characterData.CharacterExists(character.Name)) Send(clientID, CommandType.NewCharacterAlreadyExists);
            else
            {
                Account account;
                if (accounts.ContainsKey(username))
                    account = accounts[username];
                else
                {
                    account = accountData.LoadAccount(username);
                    account.Characters = characterData.LoadAllForLogin(account.ID);
                }

                // disallow names that could impersonate a GM //TODO add more restrictions to this so xml files don't get messed up ect.
                if (username.EndsWith("GM") || username.Contains('[') || username.Contains(']'))
                {
                    Send(clientID, CommandType.NewCharacterFailed);
                    return;
                }

                if (account.Characters.Count >= 4)
                {
                    Send(clientID, CommandType.NewCharacterFailed);
                    return;
                }

                if (characterData.CreateCharacter(account.ID, character))
                {
                    account.Characters = characterData.LoadAllForLogin(account.ID);
                    Send(clientID, CommandType.NewCharacterCreated, account.GetNewCharacterData(character.Name));
                    LogMessage("New character created: " + character.Name);
                }
                else Send(clientID, CommandType.NewCharacterFailed);
            }
        }

        private void ChangePassword(int clientID, string username, string password, string newPassword, string newPasswordConfirm)
        {
            if (newPassword.Equals(newPasswordConfirm))
            {
                if (accountData.ChangePassword(username, password, newPassword))
                {
                    Send(clientID, CommandType.ChangePasswordSucceed);
                    LogMessage("Password reset for: " + username);
                }
                else Send(clientID, CommandType.ChangePasswordFailed);
            }
            else Send(clientID, CommandType.ChangePasswordFailed);
        }

        private void CreateAccount(int clientID, string username, string password, string email, string quiz, string answer)
        {
            if (accountData.LoginExists(username)) Send(clientID, CommandType.NewAccountAlreadyExists);
            else
            {
                if (accountData.CreateAccount(username, password, email, quiz, answer))
                {
                    Send(clientID, CommandType.NewAccountCreated);
                    LogMessage("New account created: " + username + "  - " + email);
                }
                else Send(clientID, CommandType.NewAccountFailed);
            }
        }

        private void Login(int clientID, string username, string password)//, string preshared)
        {
            //if (preshared.Equals(PresharedKey))
            if (true)
            {               
                int ret = accountData.Login(username, password);
                if (ret > 0)
                {
                    Account account;
                    if (accounts.ContainsKey(username))
                    {
                        account = accounts[username];           // account cache for faster login
                        accountData.LoadCharacters(account);    // reloads characters in case anything changed
                        account.Characters = characterData.LoadAllForLogin(account.ID);
                    }
                    else
                    {
                        account = accountData.LoadAccount(username);     // load from SQL if not cached
                        account.Characters = characterData.LoadAllForLogin(account.ID);
                    }

                    if (account == null) Send(clientID, CommandType.Reject);
                    else
                    {
                        account.ClientID = clientID;
                        if (!accounts.ContainsKey(username)) accounts.Add(username, account); // no need to add if already in cache (exception will be thrown anyway)

                        Send(account, CommandType.Confirm, account.GetLoginData());
                    }
                }
                else if (ret == -1) Send(clientID, CommandType.LoginUnavailable);
                else if (ret == 0) Send(clientID, CommandType.PasswordIncorrect);
            }
            //else Send(clientID, CommandType.Reject);
        }

        private void Send(Account account, CommandType type) { Send(account.ClientID, type); }
        private void Send(Account account, CommandType type, byte[] sendData) { Send(account.ClientID, type, sendData); }
        private void Send(int clientID, CommandType type) { Send(clientID, type, null); }
        private void Send(int clientID, CommandType type, byte[] sendData)
        {
            byte[] command, data;

            switch (type)
            {
                case CommandType.PasswordIncorrect:
                case CommandType.Reject:
                case CommandType.LoginUnavailable:
                case CommandType.IncorrectVersion:
                case CommandType.NewAccountCreated:
                case CommandType.NewAccountFailed:
                case CommandType.NewAccountAlreadyExists:
                case CommandType.ChangePasswordFailed:
                case CommandType.ChangePasswordSucceed:
                case CommandType.NewCharacterAlreadyExists:
                case CommandType.NewCharacterFailed:
                case CommandType.DeleteCharacterSucceed:
                case CommandType.DeleteCharacterFailed:
                    command = new byte[10];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 4);
                    data = Utility.Encrypt(command, false);

                    if (clientServer[clientID] != null) clientServer[clientID].Send(data);
                    break;
                case CommandType.Confirm:
                case CommandType.NewCharacterCreated:
                case CommandType.EnterGameSucceed:
                case CommandType.EnterGameFailed:
                case CommandType.EnterGameAlreadyLoggedIn:
                    command = new byte[sendData.Length + 10];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 4);
                    Buffer.BlockCopy(sendData, 0, command, 6, sendData.Length);
                    data = Utility.Encrypt(command, false);

                    if (clientServer[clientID] != null) clientServer[clientID].Send(data);
                    break;
            }
        }

        #region NETWORK CONNECTIONS

        /// <summary>
        /// Establishes that a client has successfully connected.
        /// </summary>
        /// <param name="serv"></param>
        /// <param name="newClient"></param>
        /// <returns></returns>
        private bool ClientReady(ServerInfo serv, ClientInfo newClient)
        {
            newClient.MessageType = MessageType.Length;
            newClient.OnReadBytes += new ConnectionReadBytes(ClientRead);
            newClient.OnClose += new ConnectionClosed(ClientClose);
            LogMessage("Client connected: " + newClient.ID + " (" + newClient.IPAddress + ")");
            return true;
        }

        /// <summary>
        /// Handles disconnection of a client
        /// </summary>
        /// <param name="ci"></param>
        private void ClientClose(ClientInfo ci)
        {
            LogMessage("Client disconnected: " + ci.ID + " (" + ci.IPAddress + ")");
        }

        /// <summary>
        /// Handles incoming messages from a client and queues them for the Message
        /// </summary>
        /// <param name="ci">ClientInfo object</param>
        /// <param name="data">Data received from the client</param>
        /// <param name="len">Length of data received</param>
        private void ClientRead(ClientInfo ci, byte[] data, int len)
        {
            /*lock (messageLock)
            {
                messages.Enqueue(new Command(Utility.Decrypt(data), ci.ID));
            }*/
            HandleCommand(Utility.Decrypt(data), ci.ID);
        }

        private bool GameReady(ServerInfo serv, ClientInfo newClient)
        {
            newClient.MessageType = MessageType.Length;
            LogMessage("Game server connected: " + newClient.ID + " (" + newClient.IPAddress + ")");
            newClient.OnReadBytes += new ConnectionReadBytes(GameRead);
            newClient.OnClose += new ConnectionClosed(GameClose);
            Game game = new Game();
            gameServers.Add(game);
            newClient.Data = game;
            return true;
        }

        private void GameClose(ClientInfo ci)
        {
            LogMessage("Game Server disconnected: " + ci.ID + " (" + ci.IPAddress + ")");
            Game game = ((Game)ci.Data);
            gameServers.Remove(game);
        }

        private void GameRead(ClientInfo ci, byte[] data, int len)
        {
            string message = Encoding.ASCII.GetString(data, 0, len);

            string[] msg = message.Split('|');

            switch (msg[0])
            {
                case "REGISTER":
                    Game game = ((Game)ci.Data);
                    if (game.Parse(msg))
                    {
                        LogMessage("Game Server Registered: " + game.Name + " (IP: " + game.IPAddress + "  Port: " + game.Port + ")");
                        foreach (Map map in game.Maps) LogMessage("Map Registered: " + map.Name);
                    }
                    else LogMessage("CRITICAL ERROR: Failed to Register Game Server: " + game.Name);
                    break;
                case "CHAT":
                    /*int x = Int32.Parse(msg[1]);
                    int y = Int32.Parse(msg[2]);
                    string name = msg[3];
                    string chat = msg[4];

                    foreach (Game game in gameServers)
                    {

                    }*/ //TODO
                    break;
            }
        }

        #endregion

        #region LOGGING
        public void LogMessage(string message)
        {
            if (MessageLogged != null) MessageLogged(message, LogType.Login);
        }

        public void LogMessage(string message, LogType logType)
        {
            if (MessageLogged != null) MessageLogged(message, logType);
        }

        public void LogConnectionMessage(string message)
        {
            if (MessageLogged != null) MessageLogged(message, LogType.Database);
        }
        #endregion
    }
}
