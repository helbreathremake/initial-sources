﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HelbreathWorld.Common;
using HelbreathWorld.Common.Events;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Common.Assets.Objects;
using HelbreathWorld.Common.Assets.Objects.Dynamic;

namespace HelbreathWorld.Common
{
    public delegate void LogHandler(string message, LogType type);
    public delegate void WorldLogHandler(string message);
    public delegate void MapLogHandler(string message);
    public delegate void SendDataHandler(int clientID, byte[] data);

    public delegate void WorldEventHandler(WorldEventResult result);
    public delegate bool GuildHandler(Guild guild);
    public delegate void SkillHandler(Skill skill);
    public delegate void ItemPurchaseHandler(string id, int count);
    public delegate void ItemSoldHandler(MerchantItem item, bool stack);
    public delegate void MapHandler(Map map);
    public delegate void DynamicObjectHandler(IDynamicObject dynamicObject);
    public delegate void CharacterHandler(Character character);
    public delegate void SummonHandler(Character character, Npc npc, int destinationX, int destinationY);
    public delegate void OwnerHandler(IOwner owner);
    public delegate void MotionHandler(IOwner owner, CommandMessageType motionType, int destinationX, int destinationY);
    public delegate void ItemHandler(IOwner owner, Item item);
    public delegate void DamageHandler(IOwner owner, DamageType type, IOwner attacker, int damage, MotionDirection flyDirection, int hitCount, int totalDamage);
    public delegate void VitalsChangedHandler(IOwner owner, int amount, VitalType type); //TODO combine replenish and deplete into one event handler, 
    public delegate void DeathHandler(IOwner owner, IOwner killer, int damage, DamageType type, int hitCount);
    
    public delegate void GameMessageBoxHandler(GameDialogBoxType boxType, MessageBoxResponse response);
    public delegate void MenuMessageBoxHandler(MenuDialogBoxType boxType, MessageBoxResponse response);

    public delegate void PublicEventHandler(IPublicEvent publicEvent);
}
