﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using HelbreathWorld;
using HelbreathWorld.Game;

namespace PakEditor
{
    public partial class PakEditor : Form
    {
        private string spriteName;
        private string spriteFullPath;
        private SpriteFile spriteFile;
        private Image spriteImage;
        private bool fileChanged;
        private bool newFile;
        private bool fileReady;
        private bool isClosing;
        private bool isCreating;

        private System.Windows.Forms.Timer animationTimer;
        private int animationFrame;

        public PakEditor()
        {
            InitializeComponent();

            animationTimer = new System.Windows.Forms.Timer();
            animationTimer.Interval = 100;
            animationTimer.Tick += new EventHandler(AnimateSprite);
            animationTimer.Start();

            NewFile(null, null);
        }

        private void ListFiles()
        {
            FileList.Items.Clear();
            int index = 0;
            foreach (Sprite file in spriteFile.Sprites)
                if (true)
                {
                    FileList.Items.Add(new ListBoxItem(index, index + ", " + file.StartLocation.ToString()));
                    index++;
                }

            if (FileList.Items.Count > 0)
            {
                exportAllToolStripMenuItem.Enabled = true;
                replaceAllToolStripMenuItem.Enabled = true;
            }


            if (FileList.SelectedIndex != -1)
            {
                dataGridView1.Rows.Clear();
                index = 0;
                foreach (SpriteFrame frame in spriteFile[FileList.SelectedIndex].Frames)
                {
                    dataGridView1.Rows.Add(new string[] { index.ToString(), frame.Left.ToString(), frame.Top.ToString(), frame.Width.ToString(), frame.Height.ToString(), frame.PivotX.ToString(), frame.PivotY.ToString(), (frame.Width + frame.PivotX).ToString(), (frame.Height + frame.PivotY).ToString() });
                    index++;
                }

                dataGridView1.AllowUserToAddRows = true;
                exportSelectedToolStripMenuItem.Enabled = true;
                replaceSelectedToolStripMenuItem.Enabled = true;
            }
        }

        private void NewFile(object sender, EventArgs e)
        {
            if (fileChanged)
            {
                DialogResult result = MessageBox.Show("Do you want to save " + spriteName + "?", "Save File?", MessageBoxButtons.YesNo);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    SaveFile(sender, e);
                    NewFile(sender, e);
                    return;
                }
            }

            FileList.Items.Clear();
            pnlFileViewer.Controls.Clear();

            FileChanged();
            this.Text = "Helbreath Champions Sprite Editor - Untitled.spr*";
            spriteName = "Untitled.spr";
            spriteFile = new SpriteFile();
            newFile = true;
            fileReady = true;
            FileList.ContextMenuStrip = null;
            editToolStripMenuItem.Enabled = true;
            saveAsToolStripMenuItem.Enabled = true;
            exportSelectedToolStripMenuItem.Enabled = false;
            exportAllToolStripMenuItem.Enabled = false;
            replaceSelectedToolStripMenuItem.Enabled = false;
            replaceAllToolStripMenuItem.Enabled = false;

            dataGridView1.Rows.Clear();

        }

        private void ImportPakFile(object sender, EventArgs e)
        {
            NewFile(sender, e);

            LegacyPakFile pak = new LegacyPakFile(importLegacyPakFileDialog.FileName);

            spriteFile = SpriteFile.Convert(pak);

            ListFiles();
            editToolStripMenuItem.Enabled = true;
            FileChanged();
        }

        private void ImportPakFileDialog(object sender, EventArgs e)
        {
            if (fileChanged)
            {
                DialogResult result = MessageBox.Show("Do you want to save " + spriteName + "?", "Save File?", MessageBoxButtons.YesNo);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    SaveFile(sender, e);
                    ImportPakFileDialog(sender, e);
                    return;
                }
            }

            importLegacyPakFileDialog.ShowDialog();
        }

        private void LoadPakFileDialog(object sender, EventArgs e)
        {
            loadPakFileDialog.ShowDialog();
            savePakFileDialog.FileName = Path.GetFileName(loadPakFileDialog.FileName);
        }

        private void LoadFile(object sender, CancelEventArgs e)
        {
            try
            {
                spriteFile = new SpriteFile(loadPakFileDialog.FileName);
                ListFiles();
                this.Text = "Helbreath Champions Sprite Editor - " + Path.GetFileName(loadPakFileDialog.FileName);
                spriteName = Path.GetFileName(loadPakFileDialog.FileName);
                spriteFullPath = loadPakFileDialog.FileName;
                editToolStripMenuItem.Enabled = true;
                fileReady = true;
            }
            catch
            {
                MessageBox.Show("Unable to load this file, it may be corrupted.");
            }
        }

        private void FileChanged()
        {
            this.fileChanged = true;
            saveToolStripMenuItem.Enabled = true;
            saveAsToolStripMenuItem.Enabled = true;
            if (!this.Text.EndsWith("*")) this.Text += "*";
        }

        private void FileReady()
        {
            saveAsToolStripMenuItem.Enabled = true;
            editToolStripMenuItem.Enabled = true;
            pnlFileViewer.Controls.Clear();
            fileReady = true;
        }

        private void AddFileDialog(object sender, EventArgs e)
        {
            addFileDialog.ShowDialog();
        }

        private void AddFile(object sender, EventArgs e)
        {
            foreach (string fileName in addFileDialog.FileNames)
            {
                Sprite s = new Sprite();

                using (Bitmap image = (Bitmap)Image.FromFile(fileName))
                {
                    s.Width = image.Width;
                    s.Height = image.Height;
                    //image.MakeTransparent(image.GetPixel(0, 0));
                    using (MemoryStream stream = new MemoryStream())
                    {
                        image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        s.ImageData = stream.GetBuffer();
                    }
                }

                spriteFile.AddSprite(s);
            }

            ListFiles();
            FileChanged();
        }

        private void ViewFile(object sender, EventArgs e)
        {
            if (FileList.SelectedIndex < 0) return;
            pnlFileViewer.Controls.Clear();

            try
            {
                spriteImage = Utility.GetImage(spriteFile.Sprites[FileList.SelectedIndex].ImageData);

                // prepares the image object from the byte stream
                if (!spriteFile.Sprites[FileList.SelectedIndex].ImageLoaded)
                    spriteFile.Sprites[FileList.SelectedIndex].LoadImage();

                PictureBox spriteBox = new PictureBox();
                spriteBox.Name = "spriteBox";
                if (chkShowFrames.Checked)
                    spriteBox.Image = spriteFile.Sprites[FileList.SelectedIndex].ImageWithFrames;
                else spriteBox.Image = spriteImage;
                //spriteBox.Dock = DockStyle.Fill;
                spriteBox.Width = spriteFile.Sprites[FileList.SelectedIndex].Width;
                spriteBox.Height = spriteFile.Sprites[FileList.SelectedIndex].Height;
                spriteBox.BorderStyle = BorderStyle.FixedSingle;

                pnlFileViewer.Controls.Add(spriteBox);

                if (FileList.SelectedIndex != -1)
                {
                    dataGridView1.Rows.Clear();
                    int index = 0;
                    foreach (SpriteFrame frame in spriteFile[FileList.SelectedIndex].Frames)
                    {
                        dataGridView1.Rows.Add(new string[] { index.ToString(), frame.Left.ToString(), frame.Top.ToString(), frame.Width.ToString(), frame.Height.ToString(), frame.PivotX.ToString(), frame.PivotY.ToString(), (frame.Width + frame.PivotX).ToString(), (frame.Height + frame.PivotY).ToString() });
                        index++;
                    }

                    dataGridView1.AllowUserToAddRows = true;
                    exportSelectedToolStripMenuItem.Enabled = true;
                    replaceSelectedToolStripMenuItem.Enabled = true;
                }
            }
            catch
            {
                Label label = new Label();
                label.Text = "Unable to display this sprite.";
                label.Dock = DockStyle.Fill;

                pnlFileViewer.Controls.Add(label);
            }
        }

        private void ShowFrames(object sender, EventArgs e)
        {
            ViewFile(sender, e);
        }

        private void SavePakFileDialog(object sender, EventArgs e)
        {
            savePakFileDialog.ShowDialog();
        }

        private void SaveFile(object sender, EventArgs e)
        {
            if (newFile)
                savePakFileDialog.ShowDialog();
            else
            {
                
                spriteFile.Save(spriteFullPath);
                if (this.Text.EndsWith("*"))
                    this.Text = this.Text.Trim('*');

                saveToolStripMenuItem.Enabled = false;
                fileChanged = false;
            }
        }

        private void SaveAsFile(object sender, CancelEventArgs e)
        {
            newFile = false;

            spriteFile.Save(savePakFileDialog.FileName);

            spriteName = Path.GetFileName(savePakFileDialog.FileName);
            spriteFullPath = savePakFileDialog.FileName;

            this.Text = "Helbreath Champions Sprite Editor - " + spriteName;

            if (this.Text.EndsWith("*"))
                this.Text = this.Text.Trim('*');

            saveToolStripMenuItem.Enabled = false;
            fileChanged = false;
        }

        private void RemoveFile(object sender, EventArgs e)
        {
            if (FileList.SelectedIndex < 0) return;

            spriteFile.Sprites.RemoveAt(FileList.SelectedIndex);

            ListFiles();

            pnlFileViewer.Controls.Clear();
            if (FileList.Items.Count > 0) 
                ViewFile(sender, e);
        }

        private void RenameFile(object sender, EventArgs e)
        {
            int fileIndex = ((ListBoxItem)FileList.SelectedItem).Index;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (fileChanged)
            {
                isClosing = true;

                DialogResult result = MessageBox.Show("Do you want to save " + spriteName + "?", "Save File?", MessageBoxButtons.YesNo);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    e.Cancel = true;
                    SaveFile(null, null);
                    Exit(null, null);
                }
            }
            else base.OnClosing(e);
        }

        private void Exit(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadFileContextMenu(object sender, MouseEventArgs e)
        {
            if (fileReady)
                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    contextMenuStrip1.Show(FileList, e.Location);
                    addToolStripMenuItem.Enabled = true;    
                    if (FileList.Items.Count > 0)
                    {
                        FileList.SelectedIndex = FileList.IndexFromPoint(e.Location);

                        if (FileList.SelectedIndex >= 0)
                        {
                            removeToolStripMenuItem.Enabled = true;
                            toolStripMenuItem5.Enabled = true;
                        }
                        else
                        {
                            removeToolStripMenuItem.Enabled = false;
                            toolStripMenuItem5.Enabled = false;
                        }
                    }
                }
        }

        private void SortByName(object sender, EventArgs e)
        {

            ListFiles();
        }

        private void PakToSpr(object sender, EventArgs e)
        {
            loadLegacyPakFileDialog.ShowDialog();
        }

        private void LoadLegacyPakFile(object sender, CancelEventArgs e)
        {
            LegacyPakEditor editor = new LegacyPakEditor(loadLegacyPakFileDialog.FileName);
            editor.Show(this);
        }

        private void BatchLegacyConverter(object sender, EventArgs e)
        {
            loadBatchLegacyPakFileDialog.ShowDialog();
        }

        private AutoResetEvent[] autoEvent;

        private void ConvertBatchLegacyPakFiles(object sender, CancelEventArgs e)
        {
            if (loadBatchLegacyPakFileDialog.FileNames.Length <= 0) return;

            ThreadPool.QueueUserWorkItem(new WaitCallback(ConvertLegacyPakFile), loadBatchLegacyPakFileDialog.FileNames);           
        }

        private void ConvertLegacyPakFile(object files)
        {
            ToolStripLabel text = new ToolStripLabel();
            ToolStripProgressBar progress = new ToolStripProgressBar();
            progress.Maximum = loadBatchLegacyPakFileDialog.FileNames.Length;
            progress.Minimum = 0;

            statusStrip1.Invoke(new MethodInvoker(delegate() 
                {
                    statusStrip1.Items.Clear();
                    statusStrip1.Items.Add(progress);
                    statusStrip1.Items.Add(text);
                }));

            string logFile = Path.GetDirectoryName(Application.ExecutablePath) + "\\ConvertLog.txt";
            string logText = DateTime.Now.ToString() + ": " + "Starting Helbreath Legacy Pak Convertion to Helbreath Champions Sprite Format\r\n";
            logText += DateTime.Now.ToString() + ": " + loadBatchLegacyPakFileDialog.FileNames.Length + " files found.\r\n";
            logText += "----------------------------------------------------------------------------------------------------------\r\n";

            int complete = 0;
            int failed = 0;
            foreach (string filename in ((string[])files))
            {
                try
                {
                    LegacyPakFile legacyPak = new LegacyPakFile(filename);
                    SpriteFile.ConvertAndSave(legacyPak, filename.ToLower().Replace(".pak", ".spr"));
                    legacyPak = null;
                    complete++;
                    logText += DateTime.Now.ToString() + ": " + "Success: Converted " + filename + " to " + filename.ToLower().Replace(".pak", ".spr") + "\r\n";
                }
                catch (Exception ex)
                {
                    failed++;
                    logText += DateTime.Now.ToString() + ": " + "Failed: Converted " + filename + " to " + filename.ToLower().Replace(".pak", ".spr") + " with error: " + ex.ToString() + "\r\n";
                }
                progress.ProgressBar.Invoke(new MethodInvoker(delegate() { progress.Value++; }));
                Invoke(new MethodInvoker(delegate() { text.Text = "Converting... " + (complete + failed) + " / " + loadBatchLegacyPakFileDialog.FileNames.Length + " complete. " + failed + " failed."; }));
            }

            logText += DateTime.Now.ToString() + ": " + "Convertion Finished. " + loadBatchLegacyPakFileDialog.FileNames.Length + " converted. " + failed + " failed.\r\n";
            logText += "----------------------------------------------------------------------------------------------------------\r\n";

            using (StreamWriter writer = File.AppendText(logFile))
            {
                writer.Write(logText);
            }
        }

        private void AnimateSprite(object sender, EventArgs e)
        {
            if (FileList.SelectedIndex < 0) return;
            if (!chkAnimate.Checked) return;

            PictureBox spriteBox;

            if (pnlFileViewer.Controls.Count <= 0)
            {
                spriteBox = new PictureBox();
                spriteBox.Width = spriteFile.Sprites[FileList.SelectedIndex].AnimationWidth;
                spriteBox.Height = spriteFile.Sprites[FileList.SelectedIndex].AnimationHeight;
                spriteBox.Dock = DockStyle.Fill;
                pnlFileViewer.Controls.Add(spriteBox);
            }
            else
            {
                spriteBox = (PictureBox)pnlFileViewer.Controls[0];
                spriteBox.Width = spriteFile.Sprites[FileList.SelectedIndex].AnimationWidth;
                spriteBox.Height = spriteFile.Sprites[FileList.SelectedIndex].AnimationHeight;
            }

            // prepares the image object from the byte stream
            if (!spriteFile.Sprites[FileList.SelectedIndex].ImageLoaded)
                spriteFile.Sprites[FileList.SelectedIndex].LoadImage();

            if (chkAnimate.Checked && spriteFile.Sprites[FileList.SelectedIndex].Frames.Count > 0)
            {
                animationFrame++;
                if (animationFrame > spriteFile.Sprites[FileList.SelectedIndex].Frames.Count - 1) animationFrame = 0;

                spriteBox.Image = spriteFile.Sprites[FileList.SelectedIndex].GetFrame(animationFrame, chkShowFrames.Checked);
            }
        }

        private void AddFrame(object sender, DataGridViewRowEventArgs e)
        {
            if (FileList.SelectedIndex < 0) return;

            dataGridView1.Rows[e.Row.Index - 1].Cells[0].Value = (e.Row.Index-1).ToString();

            try
            {
                SpriteFrame frame = new SpriteFrame();
                spriteFile.Sprites[FileList.SelectedIndex].AddFrame(frame);
                FileChanged();
            }
            catch { }
        }

        private void AddFrame(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (FileList.SelectedIndex < 0) return;

            try
            {
                SpriteFrame frame = new SpriteFrame();
                if (dataGridView1.Rows[e.RowIndex].Cells[1].Value != null)
                    frame.Left = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
                if (dataGridView1.Rows[e.RowIndex].Cells[2].Value != null)
                    frame.Top = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
                if (dataGridView1.Rows[e.RowIndex].Cells[3].Value != null)
                    frame.Width = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString());
                if (dataGridView1.Rows[e.RowIndex].Cells[4].Value != null)
                    frame.Height = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString());
                if (dataGridView1.Rows[e.RowIndex].Cells[5].Value != null)
                    frame.PivotX = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString());
                if (dataGridView1.Rows[e.RowIndex].Cells[6].Value != null)
                    frame.PivotY = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString());

                dataGridView1.Rows[e.RowIndex].Cells[7].Value = (frame.Width + frame.PivotX).ToString();
                dataGridView1.Rows[e.RowIndex].Cells[8].Value = (frame.Height + frame.PivotY).ToString();

                spriteFile.Sprites[FileList.SelectedIndex].Frames.Add(frame);

                FileChanged();

                ViewFile(sender, e);
            }
            catch { }
        }

        private void EditFrame(object sender, DataGridViewCellEventArgs e)
        {
            if (FileList.SelectedIndex < 0) return;

            try
            {
                SpriteFrame frame = spriteFile.Sprites[FileList.SelectedIndex].Frames[e.RowIndex];
                if (dataGridView1.Rows[e.RowIndex].Cells[1].Value != null)
                    frame.Left = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
                if (dataGridView1.Rows[e.RowIndex].Cells[2].Value != null)
                    frame.Top = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
                if (dataGridView1.Rows[e.RowIndex].Cells[3].Value != null)
                    frame.Width = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString());
                if (dataGridView1.Rows[e.RowIndex].Cells[4].Value != null)
                    frame.Height = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString());
                if (dataGridView1.Rows[e.RowIndex].Cells[5].Value != null)
                    frame.PivotX = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString());
                if (dataGridView1.Rows[e.RowIndex].Cells[6].Value != null)
                    frame.PivotY = Int32.Parse(dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString());

                dataGridView1.Rows[e.RowIndex].Cells[7].Value = (frame.Width + frame.PivotX).ToString();
                dataGridView1.Rows[e.RowIndex].Cells[8].Value = (frame.Height + frame.PivotY).ToString();

                spriteFile.Sprites[FileList.SelectedIndex].Frames[e.RowIndex] = frame; // struct immutable, need to set it back
                FileChanged();

                ViewFile(sender, e);
            }
            catch { }
        }

        private enum ImageEditMode { Selected, All }
        private ImageEditMode imageEditMode;
        private void ExportSelectedDialog(object sender, EventArgs e)
        {
            imageEditMode = ImageEditMode.Selected;
            exportFileDialog.ShowDialog();
        }

        private void ExportAllDialog(object sender, EventArgs e)
        {
            imageEditMode = ImageEditMode.All;
            exportFileDialog.ShowDialog();
        }

        private void ExportFile(object sender, CancelEventArgs e)
        {
            try
            {
                switch (imageEditMode)
                {
                    case ImageEditMode.Selected:
                        if (FileList.SelectedIndex < 0) return;
                        Sprite s = spriteFile.Sprites[FileList.SelectedIndex];

                        if (!s.ImageLoaded) s.LoadImage();

                        s.Image.Save(exportFileDialog.FileName, System.Drawing.Imaging.ImageFormat.Png);

                        break;
                    case ImageEditMode.All:
                        int index = 0;
                        foreach (Sprite sprite in spriteFile.Sprites)
                        {
                            if (!sprite.ImageLoaded) sprite.LoadImage();

                            string name = Path.GetDirectoryName(exportFileDialog.FileName) 
                                            + "\\" +
                                          Path.GetFileNameWithoutExtension(exportFileDialog.FileName) 
                                            + "_" + index + 
                                          Path.GetExtension(exportFileDialog.FileName);
                            sprite.Image.Save(name, System.Drawing.Imaging.ImageFormat.Png);
                            index++;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a problem: " + ex.Message);
            }
        }

        private void ReplaceSelectedDialog(object sender, EventArgs e)
        {
            imageEditMode = ImageEditMode.Selected;
            replaceFileDialog.ShowDialog();
        }

        private void ReplaceAllDialog(object sender, EventArgs e)
        {
            imageEditMode = ImageEditMode.All;
            replaceFileDialog.ShowDialog();
        }

        private void ReplaceFile(object sender, CancelEventArgs e)
        {
            try
            {
                switch (imageEditMode)
                {
                    case ImageEditMode.Selected:
                        if (FileList.SelectedIndex < 0) return;

                        Sprite s = spriteFile.Sprites[FileList.SelectedIndex];

                        using (Bitmap img = (Bitmap)Image.FromFile(replaceFileDialog.FileName))
                        {
                            //img.MakeTransparent(img.GetPixel(0, 0));
                            using (MemoryStream stream = new MemoryStream())
                            {
                                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                                s.Width = img.Width;
                                s.Height = img.Height;
                                s.ImageData = stream.GetBuffer();
                            }
                        }

                        ViewFile(sender, e);
                        FileChanged();

                        break;
                    case ImageEditMode.All:
                        string prefix = Path.GetFileName(replaceFileDialog.FileName).Substring(0, Path.GetFileName(replaceFileDialog.FileName).IndexOf('_'));
                        int index = 0;
                        foreach (Sprite sprite in spriteFile.Sprites)
                        {
                            string name = Path.GetDirectoryName(replaceFileDialog.FileName)
                                            + "\\" +
                                          prefix
                                            + "_" + index +
                                          Path.GetExtension(replaceFileDialog.FileName);

                            using (Bitmap img = (Bitmap)Image.FromFile(name))
                            {
                                //img.MakeTransparent(Color.Black);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                                    sprite.Width = img.Width;
                                    sprite.Height = img.Height;
                                    sprite.ImageData = stream.GetBuffer();
                                }
                            }

                            index++;
                        }

                        if (FileList.SelectedIndex >= 0) ViewFile(sender, e);
                        FileChanged();
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a problem: " + ex.Message);
            }
        }

        private void ExportToPakDialog(object sender, EventArgs e)
        {
            exportLegacyPakFileDialog.ShowDialog();
        }

        private void ExportToPak(object sender, CancelEventArgs e)
        {
            
        }

        private void SetWhiteBackground(object sender, EventArgs e)
        {
            pnlFileViewer.BackColor = Color.White;
        }

        private void SetBlackBackground(object sender, EventArgs e)
        {
            pnlFileViewer.BackColor = Color.Black;
        }

        private void chkAnimate_CheckedChanged(object sender, EventArgs e)
        {
            if (spriteFile == null) return;
        }

        
    }

    public class ListBoxItem
    {
        public String Text;
        public int Index;

        public ListBoxItem(int pakIndex, string text)
        {
            this.Text = text;
            this.Index = pakIndex;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
