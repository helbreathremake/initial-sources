﻿namespace PakEditor
{
    partial class LegacyPakEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FileList = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkShowFrames = new System.Windows.Forms.CheckBox();
            this.chkAnimate = new System.Windows.Forms.CheckBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Left = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Top = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Width = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Height = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PivotX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PivotY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DifX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DifY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // FileList
            // 
            this.FileList.FormattingEnabled = true;
            this.FileList.Location = new System.Drawing.Point(12, 28);
            this.FileList.Name = "FileList";
            this.FileList.Size = new System.Drawing.Size(153, 498);
            this.FileList.TabIndex = 0;
            this.FileList.SelectedIndexChanged += new System.EventHandler(this.ShowSprite);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(601, 362);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(171, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(825, 499);
            this.panel1.TabIndex = 2;
            // 
            // chkShowFrames
            // 
            this.chkShowFrames.AutoSize = true;
            this.chkShowFrames.Location = new System.Drawing.Point(12, 532);
            this.chkShowFrames.Name = "chkShowFrames";
            this.chkShowFrames.Size = new System.Drawing.Size(90, 17);
            this.chkShowFrames.TabIndex = 2;
            this.chkShowFrames.Text = "Show Frames";
            this.chkShowFrames.UseVisualStyleBackColor = true;
            this.chkShowFrames.CheckedChanged += new System.EventHandler(this.ShowSprite);
            // 
            // chkAnimate
            // 
            this.chkAnimate.AutoSize = true;
            this.chkAnimate.Location = new System.Drawing.Point(101, 532);
            this.chkAnimate.Name = "chkAnimate";
            this.chkAnimate.Size = new System.Drawing.Size(64, 17);
            this.chkAnimate.TabIndex = 3;
            this.chkAnimate.Text = "Animate";
            this.chkAnimate.UseVisualStyleBackColor = true;
            this.chkAnimate.CheckedChanged += new System.EventHandler(this.ShowSprite);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Index,
            this.Left,
            this.Top,
            this.Width,
            this.Height,
            this.PivotX,
            this.PivotY,
            this.DifX,
            this.DifY});
            this.dataGridView1.Location = new System.Drawing.Point(3, 368);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(598, 131);
            this.dataGridView1.TabIndex = 4;
            // 
            // Index
            // 
            this.Index.HeaderText = "Frame";
            this.Index.MaxInputLength = 1;
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            // 
            // Left
            // 
            this.Left.HeaderText = "Left";
            this.Left.Name = "Left";
            this.Left.ReadOnly = true;
            // 
            // Top
            // 
            this.Top.HeaderText = "Top";
            this.Top.Name = "Top";
            this.Top.ReadOnly = true;
            // 
            // Width
            // 
            this.Width.HeaderText = "Width";
            this.Width.Name = "Width";
            this.Width.ReadOnly = true;
            // 
            // Height
            // 
            this.Height.HeaderText = "Height";
            this.Height.Name = "Height";
            this.Height.ReadOnly = true;
            // 
            // PivotX
            // 
            this.PivotX.HeaderText = "PivotX";
            this.PivotX.Name = "PivotX";
            this.PivotX.ReadOnly = true;
            // 
            // PivotY
            // 
            this.PivotY.HeaderText = "PivotY";
            this.PivotY.Name = "PivotY";
            this.PivotY.ReadOnly = true;
            // 
            // DifX
            // 
            this.DifX.HeaderText = "DifX";
            this.DifX.Name = "DifX";
            this.DifX.ReadOnly = true;
            // 
            // DifY
            // 
            this.DifY.HeaderText = "DifY";
            this.DifY.Name = "DifY";
            this.DifY.ReadOnly = true;
            // 
            // LegacyPakEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.chkAnimate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chkShowFrames);
            this.Controls.Add(this.FileList);
            this.Name = "LegacyPakEditor";
            this.Text = "Helbreath Pak Viewer";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox FileList;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkShowFrames;
        private System.Windows.Forms.CheckBox chkAnimate;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn Left;
        private System.Windows.Forms.DataGridViewTextBoxColumn Top;
        private System.Windows.Forms.DataGridViewTextBoxColumn Width;
        private System.Windows.Forms.DataGridViewTextBoxColumn Height;
        private System.Windows.Forms.DataGridViewTextBoxColumn PivotX;
        private System.Windows.Forms.DataGridViewTextBoxColumn PivotY;
        private System.Windows.Forms.DataGridViewTextBoxColumn DifX;
        private System.Windows.Forms.DataGridViewTextBoxColumn DifY;
    }
}