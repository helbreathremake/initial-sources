﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using HelbreathWorld;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Common.Assets.Objects;

namespace NpcLoader
{
    public partial class NpcLoader : Form
    {
        private List<Npc> npcConfig;

        public NpcLoader()
        {
            InitializeComponent();

            npcConfig = new List<Npc>();
            dataGridView1.Columns.Add("Name", "Name");
            dataGridView1.Columns.Add("Type", "Type");
            dataGridView1.Columns.Add("HitDice", "HitDice");
            dataGridView1.Columns.Add("MaxHP", "MaxHp");
            dataGridView1.Columns.Add("DR", "DR");
            dataGridView1.Columns.Add("HR", "HR");
            dataGridView1.Columns.Add("MinBrvy", "MinBrvy");
            dataGridView1.Columns.Add("ExpDice", "ExpDice");
            dataGridView1.Columns.Add("ADT", "ADT");
            dataGridView1.Columns.Add("ADR", "ADR");
            dataGridView1.Columns.Add("MaxDamage", "MaxDamage");
            dataGridView1.Columns.Add("Size", "Size");
            dataGridView1.Columns.Add("Side", "Side");
            dataGridView1.Columns.Add("ActionLimit", "ActionLimit");
            dataGridView1.Columns.Add("ATime", "ATime");
            dataGridView1.Columns.Add("MR", "MR");
            dataGridView1.Columns.Add("Magic", "Magic");
            dataGridView1.Columns.Add("Day", "Day");
            dataGridView1.Columns.Add("Chat", "Chat");
            dataGridView1.Columns.Add("Search", "Search");
            dataGridView1.Columns.Add("RegenTime", "RegenTime");
            dataGridView1.Columns.Add("Attr", "Attr");
            dataGridView1.Columns.Add("AbsM", "AbsM");
            dataGridView1.Columns.Add("MaxMana", "MaxMana");
            dataGridView1.Columns.Add("MagicR", "MagicR");
            dataGridView1.Columns.Add("AttkRange", "AttkRange");
            dataGridView1.Columns.Add("AreaSize", "AreaSize");
            dataGridView1.Columns.Add("ActionTime", "ActionTime");
            dataGridView1.Columns.Add("MaximumGold", "MaximumGold");
        }

        private void StartOpenFile(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void LoadFile(object sender, CancelEventArgs e)
        {
            string filename = openFileDialog1.FileName;

            StreamReader file = new StreamReader(filename);

            string line;
            while ((line = file.ReadLine()) != null)
            {
                if (line.StartsWith("Npc"))
                {
                    string[] tokens = line.Replace('\t', ' ').Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens.Length < 4) continue;
                    Npc npc = new Npc(tokens[2]);
                    npc.Type = Int32.Parse(tokens[3]);
                    npc.Size = (OwnerSize)Int32.Parse(tokens[11]);
                    npc.Side = (OwnerSide)Int32.Parse(tokens[12]);
                    npc.ActionTime = new TimeSpan(0,0, 0, 0, Int32.Parse(tokens[14]));
                    npc.SearchRange = Int32.Parse(tokens[19]);
                    npc.MaximumDeadTime = new TimeSpan(0, 0, 0, 0, Int32.Parse(tokens[20]));
                    npc.AttackRange = Int32.Parse(tokens[25]);
                    npc.BaseDamage = new Dice(Int32.Parse(tokens[9]), Int32.Parse(tokens[10]), 0);
                    npc.Dexterity = Int32.Parse(tokens[5]) / 2;
                    npc.Vitality = (((Int32.Parse(tokens[4])*8)+Int32.Parse(tokens[4]))/3);
                    switch (npc.NpcType)
                    {
                        case NpcType.CrusadeGrandMagicGenerator: npc.Magic = 0; break;
                        case NpcType.CrusadeCannonTower:
                        case NpcType.HeldenianCannonTower:
                        case NpcType.HeldenianBattleTank: npc.Magic = 2000; break;
                        default: npc.Magic = (Int32.Parse(tokens[23]) / 3); break;
                    }
                    npc.Experience = new Dice(Int32.Parse(tokens[8]), 4, Int32.Parse(tokens[8]));
                    npc.PhysicalAbsorption = (Int32.Parse(tokens[22]));
                    npc.MagicAbsorption = (Int32.Parse(tokens[22]));
                    npc.MaximumGold = (Int32.Parse(tokens[8]) * 25);

                    npcConfig.Add(npc);

                    dataGridView1.Rows.Add(
                        new object[]
                    {
                        npc.Name,
                        npc.Type,
                        0,
                        (Int32.Parse(tokens[4])*8)+Int32.Parse(tokens[4]),
                        0,0,0,0,
                        Int32.Parse(tokens[9]),Int32.Parse(tokens[10]),
                        (Int32.Parse(tokens[9])*Int32.Parse(tokens[10])),
                        npc.Size,
                        npc.Side,
                        0,0,0,0,0,0,0,0,0,0,0,0,
                        npc.AttackRange,
                        0,
                        npc.ActionTime,
                        npc.MaximumGold
                    }
                        );
                }
            }

            file.Close();

        }

        private void StartSaveFile(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void SaveFile(object sender, CancelEventArgs e)
        {
            string filename = saveFileDialog1.FileName;

            TextWriter file = new StreamWriter(filename);

            file.WriteLine("<Npcs>");

            foreach (Npc npc in npcConfig)
            {
                StringBuilder line = new StringBuilder();
                line.Append("   <Npc Name=\"" + npc.Name + "\" ");
                line.Append("Type=\"" + npc.Type + "\" ");
                line.Append("Size=\"" + Enum.GetName(typeof(OwnerSize), npc.Size) + "\" ");
                switch (npc.Side)
                {
                    case OwnerSide.Neutral: line.Append("Side=\"Neutral\" "); break;
                    default: line.Append("Side=\"" + Enum.GetName(typeof(OwnerSide), npc.Side) + "\" "); break;

                }
                line.Append("AttackRange=\"" + npc.AttackRange + "\" ");
                line.Append("SearchRange=\"" + npc.SearchRange + "\" ");
                line.Append("BaseExperienceThrow=\"" + npc.Experience.Die + "\" ");
                line.Append("BaseDamageThrow=\"" + npc.BaseDamage.Die + "\" ");
                line.Append("BaseDamageRange=\"" + npc.BaseDamage.Faces + "\" ");
                line.Append("Dexterity=\"" + npc.Dexterity + "\" ");
                line.Append("Vitality=\"" + npc.Vitality + "\" ");
                line.Append("Magic=\"" + npc.Magic + "\" ");
                line.Append("PhysicalAbsorption=\"" + npc.PhysicalAbsorption + "\" ");
                line.Append("MagicAbsorption=\"" + npc.MagicAbsorption + "\" ");
                line.Append("MaximumDeadTime=\"" + npc.MaximumDeadTime + "\" ");
                line.Append("ActionTime=\"" + npc.ActionTime + "\" ");
                line.Append("MaximumGold=\"" + npc.MaximumGold + "\" ");
                if (npc.Name.Equals("Dummy") || npc.Name.Contains("MS-") || npc.Name.Contains("DT-") || npc.Name.Contains("ESG-") ||
                    npc.Name.Contains("GMG-") || npc.Name.Equals("ManaStone") || npc.Name.Contains("gate") || npc.Name.Contains("Crop"))
                    line.Append("IsStationary=\"True\" ");
                else if (npc.Name.Contains("AGT-") || npc.Name.Contains("CGT-") || npc.Name.Contains("CT-") || npc.Name.Contains("AGC-"))
                    line.Append("IsStationaryAttack=\"True\" ");
                line.Append(" />");

                file.WriteLine(line.ToString());
            }

            file.WriteLine("</Npcs>");

            file.Close();
        }
    }
}
