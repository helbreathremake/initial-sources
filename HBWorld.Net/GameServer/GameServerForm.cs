﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using HelbreathWorld;
using HelbreathWorld.Common;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Server;

namespace GameServer
{
    public partial class GameServerForm : Form
    {
        private string installPath;
        private string logPath;
        private string news;

        private List<HelbreathWorld.Server.GameServer> gameServers;

        public GameServerForm()
        {
            InitializeComponent();

            gameServers = new List<HelbreathWorld.Server.GameServer>();

            if (LoadSettings())
            {
                foreach (HelbreathWorld.Server.GameServer gameServer in gameServers)
                {
                    ListBox txtLog = new ListBox();
                    txtLog.Name = "txtLog";
                    txtLog.Dock = DockStyle.Fill;
                    TabPage page = new TabPage(gameServer.Name);
                    page.Text = "Logging - " + gameServer.Name;
                    page.Name = gameServer.Name;
                    page.Controls.Add(txtLog);
                    tabControl1.TabPages.Add(page);

                    gameServer.MessageLogged += new HelbreathWorld.Server.GameServer.LogHandler(LogMessage);
                    gameServer.Start();
                }
                if (gameServers.Count > 0) lstGameServers.SelectedIndex = 0;
            }
            else
            {
                MessageBox.Show("Unable to read config.xml file");
            }
        }

        private bool LoadSettings()
        {
            try
            {
                installPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");
                logPath = installPath + @"\Logs\";

                // load news.txt file
                if (!File.Exists("news.txt")) File.WriteAllText("news.txt", "Welcome!");
                news = File.ReadAllText("news.txt");
                txtNews.Text = news;

                if (!Directory.Exists(logPath)) Directory.CreateDirectory(logPath);

                XmlTextReader gameServerReader = new XmlTextReader(installPath + @"\config.xml");

                string connectionString = "";
                while (gameServerReader.Read())
                    if (gameServerReader.IsStartElement())
                        switch (gameServerReader.Name)
                        {
                            case "Globals":
                                XmlReader globalsReader = gameServerReader.ReadSubtree();
                                while (globalsReader.Read())
                                    if (globalsReader.IsStartElement())
                                        switch (globalsReader.Name)
                                        {
                                            case "General":
                                                Globals.MaximumLevel = Int32.Parse(globalsReader["MaximumLevel"]);
                                                Globals.MaximumStat = Int32.Parse(globalsReader["MaximumStat"]);
                                                Globals.ExperienceMultiplier = Int32.Parse(globalsReader["ExperienceMultiplier"]);
                                                Globals.TravellerLimit = Int32.Parse(globalsReader["TravellerLimit"]);
                                                Globals.EnemyKillModifier = Int32.Parse(globalsReader["EnemyKillModifier"]);
                                                break;
                                            case "Timers":
                                                Globals.HungerTime = Int32.Parse(globalsReader["HungerTime"]);
                                                Globals.HPRegenTime = Int32.Parse(globalsReader["HPRegenTime"]);
                                                Globals.MPRegenTime = Int32.Parse(globalsReader["MPRegenTime"]);
                                                Globals.SPRegenTime = Int32.Parse(globalsReader["SPRegenTime"]);
                                                Globals.PoisonTime = Int32.Parse(globalsReader["PoisonTime"]);
                                                Globals.ExperienceRollTime = Int32.Parse(globalsReader["ExperienceRollTime"]);
                                                Globals.MobSpawnRate = Int32.Parse(globalsReader["MobSpawnRate"]);
                                                Globals.NpcSummonDuration = Int32.Parse(globalsReader["NpcSummonDuration"]);
                                                Globals.SpecialAbilityTime = Int32.Parse(globalsReader["SpecialAbilityTime"]);
                                                break;
                                            case "Items":
                                                Globals.PlayersDropItems = Boolean.Parse(globalsReader["PlayersDropItems"]);
                                                Globals.PrimaryDropRate = Int32.Parse(globalsReader["PrimaryDropRate"]);
                                                Globals.SecondaryDropRate = Int32.Parse(globalsReader["SecondaryDropRate"]);
                                                break;
                                            case "NewPlayerItem":
                                                if (Globals.NewPlayerItems == null) Globals.NewPlayerItems = new Dictionary<string,GenderType>();
                                                if (!Globals.NewPlayerItems.ContainsKey(globalsReader["Name"]))
                                                {
                                                    GenderType typeParser = GenderType.None;
                                                    if (Enum.TryParse<GenderType>(globalsReader["GenderLimit"], out typeParser))
                                                        Globals.NewPlayerItems.Add(globalsReader["Name"], typeParser);
                                                    else Globals.NewPlayerItems.Add(globalsReader["Name"], GenderType.None);
                                                }
                                                break;
                                            case "Titles": Globals.TitlesEnabled = Boolean.Parse(globalsReader["Enabled"]); break;
                                        }
                                break;
                            case "Servers":
                                connectionString = gameServerReader["ConnectionString"];
                                break;
                            case "GameServer":
                                HelbreathWorld.Server.GameServer gameServer
                                    = new HelbreathWorld.Server.GameServer(gameServerReader["Name"], Convert.ToInt32(gameServerReader["Port"]), gameServerReader["ExternalAddress"], gameServerReader["LoginServer"], Convert.ToInt32(gameServerReader["LoginServerPort"]), connectionString);

                                lstGameServers.Items.Add(gameServer.Name);

                                XmlReader mapReader = gameServerReader.ReadSubtree();
                                while (mapReader.Read())
                                    if (mapReader.IsStartElement())
                                        switch (mapReader.Name)
                                        {
                                            case "Map":
                                                gameServer.World.AddMap(gameServerReader["Name"], 
                                                                  gameServerReader["FriendlyName"], 
                                                                  "Maps/" + gameServerReader["File"], 
                                                                  (gameServerReader["Building"] != null && Convert.ToBoolean(gameServerReader["Building"])), 
                                                                  (gameServerReader["FightZone"] != null && Convert.ToBoolean(gameServerReader["FightZone"])),
                                                                  (gameServerReader["Building"] != null && gameServerReader["BuildingType"] != null && Convert.ToBoolean(gameServerReader["Building"])
                                                                    ? (BuildingType)Enum.Parse(typeof(BuildingType), gameServerReader["BuildingType"])
                                                                    : BuildingType.None),
                                                                    (gameServerReader["SafeMap"] != null) ? Convert.ToBoolean(gameServerReader["SafeMap"]) : false,
                                                                    (gameServerReader["LootEnabled"] != null) ? Convert.ToBoolean(gameServerReader["LootEnabled"]) : true,
                                                                    (gameServerReader["IsHuntZone"] != null) ? Convert.ToBoolean(gameServerReader["IsHuntZone"]) : false,
                                                                  mapReader);
                                                break;
                                        }
                                mapReader.Close();

                                gameServer.SetNews(news);

                                gameServers.Add(gameServer);
                                break;
                        }
                gameServerReader.Close();

                return true;
            }
            catch
            {
                return false;
            }
        }

        private void UpdateNews(object sender, EventArgs e)
        {
            this.news = txtNews.Text;
            File.WriteAllText("news.txt", news);

            foreach (HelbreathWorld.Server.GameServer gameServer in gameServers)
                gameServer.SetNews(news);

            MessageBox.Show("News Updated!", "Game Server Manager");
            btnUpdateNews.Enabled = false;
        }

        private void LogMessage(string name, string message, LogType logType)
        {
            switch (logType)
            {
                case LogType.Game: WriteToLoggingTab(name, message); break;
                case LogType.Chat: WriteToChatTab(message); break;
            }

            WriteToLogFile(message, logType);
        }

        private void WriteToChatTab(string message)
        {
            int i = tabControl1.TabPages["Chat"].Controls.Count;
            ListBox txtLog = (ListBox)tabControl1.TabPages["Chat"].Controls["lstChat"];
            if (txtLog.InvokeRequired)
                txtLog.Invoke(new UpdateListBoxDelegate(UpdateListBox), new object[] { lstChat, message });
            else UpdateListBox(lstChat, message);
        }

        private void WriteToLoggingTab(string name, string message)
        {
            if (tabControl1.TabPages.ContainsKey(name))
            {
                int i = tabControl1.TabPages[name].Controls.Count;
                ListBox txtLog = (ListBox)tabControl1.TabPages[name].Controls["txtLog"];
                if (txtLog.InvokeRequired)
                    txtLog.Invoke(new UpdateListBoxDelegate(UpdateListBox), new object[] { txtLog, message });
                else UpdateListBox(txtLog, message);
            }
        }

        delegate void UpdateListBoxDelegate(ListBox control, string message);
        private void UpdateListBox(ListBox control, string message)
        {
            control.Items.Add(String.Format("{0}-{1}:  {2}", DateTime.Now.Hour, DateTime.Now.Minute, message));
            control.SelectedIndex = control.Items.Count - 1;
            control.SelectedIndex = -1;
        }

        private void WriteToLogFile(string message, LogType logType)
        {
            String logPath;
            switch (logType)
            {
                case LogType.Game: logPath = this.logPath + "GameServer.log"; break;
                case LogType.Hack: logPath = this.logPath + "HackEvents.log"; break;
                case LogType.Item: logPath = this.logPath + "ItemEvents.log"; break;
                case LogType.Login: logPath = this.logPath + "Logins.log"; break;
                case LogType.Error: logPath = this.logPath + "GameServerError.log"; break;
                case LogType.Admin: logPath = this.logPath + "AdminCommands.log"; break;
                case LogType.Chat: logPath = this.logPath + "Chat.log"; break;
                case LogType.Map: logPath = this.logPath + "Maps.log"; break;
                case LogType.Test: logPath = this.logPath + "Test.log"; break;
                case LogType.Database: logPath = this.logPath + "Database.log"; break;
                case LogType.Events: logPath = this.logPath + "Events.log"; break;
                default: logPath = this.logPath + "GameServer.log"; break;
            }

            string log = String.Format("{0}-{1}-{2}-{3}-{4}-{5}:  ", DateTime.Now.Day, DateTime.Now.Month,
                                                                      DateTime.Now.Year, DateTime.Now.Hour,
                                                                      DateTime.Now.Minute, DateTime.Now.Second)
                                                                      + message;

            try
            {
                StreamWriter writer;

                if (File.Exists(logPath))
                {
                    FileInfo fi = new FileInfo(logPath);

                    if (fi.Length >= 1024000)
                    {
                        File.Copy(logPath, logPath + ".old", true);
                        File.Delete(logPath);

                        writer = File.CreateText(logPath);
                        writer.WriteLine(log);
                    }
                    else
                    {
                        writer = File.AppendText(logPath);
                        writer.WriteLine(log);
                    }
                }
                else
                {
                    writer = File.CreateText(logPath);
                    writer.WriteLine(log);
                }

                writer.Close();
            }
            catch (Exception)
            { 
                // TODO - logging
            }
        }

        private void Exit(object sender, EventArgs e)
        {
            bool found = false;
            foreach (HelbreathWorld.Server.GameServer gameServer in gameServers)
                if (gameServer.Running)
                {
                    MessageBox.Show("Game Server is Running.");
                    found = true;
                    break;
                }
            
            if (!found) this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            foreach (HelbreathWorld.Server.GameServer gameServer in gameServers)
                gameServer.Stop();

            base.OnClosed(e);
        }

        private void OnNewsModified(object sender, EventArgs e)
        {
            if (txtNews.Text.Equals(news))
                btnUpdateNews.Enabled = false;
            else btnUpdateNews.Enabled = true;
        }

        private void UpdateGameServerInfo(object sender, EventArgs e)
        {
            if (lstGameServers.SelectedIndex >= 0 && gameServers[lstGameServers.SelectedIndex] != null)
            {
                int index = lstGameServers.SelectedIndex;
                lblStatus.Text = (gameServers[index].Running) ? "Running" : "Stopped";
                lblStatus.ForeColor = (gameServers[index].Running) ? Color.Green : Color.Red;
                lblTotalNpcs.Text = gameServers[index].World.NpcCount.ToString();
                lblTotalMaps.Text = gameServers[index].World.Maps.Count.ToString();
                TimeSpan upTime = ((TimeSpan)(DateTime.Now - gameServers[index].StartTime));
                lblUpTime.Text = upTime.Days + "d " + upTime.Hours + "h " + upTime.Minutes + "m " + upTime.Seconds + "s";
                lblCurrentEvent.Text = (gameServers[index].World.CurrentEvent != null) ? gameServers[index].World.CurrentEvent.Type.ToString() : "None";
                lblLoginServer.Text = gameServers[index].LoginServerAddress;
                lblLoginPort.Text = gameServers[index].LoginPort;
                lblExternalAddress.Text = gameServers[index].ExternalAddress;
                lblPort.Text = gameServers[index].ExternalPort;
                lblTOD.Text = gameServers[index].World.TimeOfDay.ToString();

                DataTable players = gameServers[index].World.FormatPlayerList();
                lblPlayersOnline.Text = players.Rows.Count.ToString();
                dataGridView1.DataSource = players;
            }
        }

        private void PlayerSelected(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    playerMenu.Show(this.dataGridView1, new Point(e.RowIndex, e.ColumnIndex));
                }
            }
            catch 
            {
                // TODO why does this error sometimes?            
            }
        }

        private void SendChat(object sender, EventArgs e)
        {
            if (txtChatName.Text.Length > 4 && txtChatText.Text.Length > 2)
            {
                if (txtChatName.Text.Contains("[GM]"))
                {
                    foreach (HelbreathWorld.Server.GameServer game in gameServers)
                        if (game.Running)
                            game.World.ExternalChat(txtChatName.Text, txtChatText.Text, ChatType.GameMaster);

                    LogMessage("", txtChatName.Text + " [External GameMaster Chat]: " + txtChatText.Text, LogType.Chat);
                    txtChatText.Text = "";
                }
                else MessageBox.Show("Chat name must contain '[GM]'.", "Game Server Manager");
            }
        }

        private void SendChatKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendChat(sender, e);
        }

        private void ShowPlayerInfo(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows == null) return;
        }
    }
}