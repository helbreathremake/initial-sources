﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml;

using HelbreathWorld;
using HelbreathWorld.Data;
using HelbreathWorld.Common;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Common.Assets.Objects;
using HelbreathWorld.Common.Assets.Objects.Dynamic;
using HelbreathWorld.Common.Events;
using HelbreathWorld.Common.Assets.Quests;

namespace HelbreathWorld.Server
{
    public class GameServer
    {
        public World world; // stores information about the world

        private Scheduler scheduler;
       
        public delegate void LogHandler(string servername, string message, LogType logType);
        public event LogHandler MessageLogged;

        private DateTime startTime;
        private bool running;
        private int port;
        private string externalAddress;
        private int loginPort;
        private string loginAddress;

        private Queue<Command> messages;
        private Queue<Command> processQueue;
        public readonly object processQueueLock = new object();
        private object messageLock;
        private string name;
        private Thread mainThread;
        private ServerInfo server;
        private ClientInfo loginServerClient;

        private Connection connection;
        private CharacterData characterData;
        private GuildData guildData;
        private EventData eventData;

        /// <summary>
        /// Creates a new game server.
        /// </summary>
        /// <param name="name">Name of this game server.</param>
        /// <param name="port">Port on which the game server accepts connecions.</param>
        /// <param name="externalAddress">External (public) address for external users to connect. Usually the internet address.</param>
        /// <param name="loginServerAddress">Internal (private) address of the login server.</param>
        /// <param name="loginServerPort">Port on which the login server accepts connections from game servers.</param>
        /// <param name="connectionString">SQL connection string to connect to the database.</param>
        public GameServer(string name, int port, string externalAddress, string loginServerAddress, int loginServerPort, string connectionString)
        {
            world = new World();
            world.SendData += new SendDataHandler(OnSendData);
            world.MessageLogged += new Common.LogHandler(LogMessage);
            world.GuildCreated += new GuildHandler(OnGuildCreated);
            world.GuildDisbanded += new GuildHandler(OnGuildDisbanded);
            world.WorldEventEnded += new WorldEventHandler(OnWorldEventEnd);

            this.name = name;
            this.port = port;
            this.externalAddress = externalAddress;
            this.loginAddress = loginServerAddress;
            this.loginPort = loginServerPort;
            this.messageLock = new object();
            this.running = false;
            processQueue = new Queue<Command>();

            this.connection = new Connection(connectionString);
            this.connection.MessageLogged += new Connection.LogHandler(LogConnectionMessage);
            this.characterData = new CharacterData(connection);
            this.characterData.MessageLogged += new CharacterData.LogHandler(LogMessage);
            this.guildData = new GuildData(connection);
            this.guildData.MessageLogged += new GuildData.LogHandler(LogMessage);
            this.eventData = new EventData(connection);
                        
            World.ManufacturingConfiguration = new Dictionary<string, ManufactureItem>();
            World.AlchemyConfiguration = new Dictionary<string, AlchemyItem>();
            World.CraftingConfiguration = new Dictionary<string, CraftingItem>();
            World.SummonConfiguration = new Dictionary<int, string>();
            World.QuestConfiguration = new Dictionary<int, Quest>();
            World.ApocalypseConfiguration = new Apocalypse();
            World.CrusadeConfiguration = new Crusade();
            World.HeldenianConfiguration = new Heldenian();
            World.CaptureTheFlagConfiguration = new CaptureTheFlag();
            World.WorldEventResults = new Dictionary<WorldEventType, WorldEventResult>();

            Globals.ExperienceTable = new long[Globals.MaximumLevel + 21];
            for (int level = 1; level < Globals.ExperienceTable.Length; level++)
                Globals.ExperienceTable[level] = Globals.ExperienceTable[level - 1] + level * (50 + (level * (level / 17) * (level / 17)));

            Globals.SkillExperienceTable = new int[102];
            for (int level = 0; level < Globals.SkillExperienceTable.Length; level++)
                if (level < 1) Globals.SkillExperienceTable[level] = 1;
                else if (level <= 50) 
                     Globals.SkillExperienceTable[level] = Globals.SkillExperienceTable[level - 1] + level;
                else Globals.SkillExperienceTable[level] = Globals.SkillExperienceTable[level - 1] + ((level * level) / 10);

            scheduler = new Scheduler();
            scheduler.MessageLogged += new HelbreathWorld.LogHandler(LogMessage);
        }

        void OnWorldEventEnd(WorldEventResult result)
        {
            eventData.SaveEventResult(result);
        }

        public void SetNews(string news)
        {
            world.News = news;
        }

        public void MainLoop()
        {
            while (true)
            {
                try
                {
                    world.NpcProcess();
                    PlayerProcess(); // queuing method.
                    world.TimerProcess();
                    Thread.Sleep(100);
                    // TODO - processes should be on timers that execute these methods on the threadpool, instead of thread.sleep
                }
                catch (ThreadAbortException)
                { } // common
                catch (Exception ex)
                {
                    LogMessage("MainLoop error " + ex.ToString(), LogType.Error);
                }
            }
        }

        /// queuing method.
        public void PlayerProcess()
        {
            while (processQueue.Count > 0 && processQueue.Peek() != null)
            {
                Command command;
                lock (processQueueLock)
                {
                    command = processQueue.Dequeue();
                    PlayerProcess(Utility.Decrypt(command.Data), command.Identity, command.IPAddress);
                }
            }
        }

        /// <summary>
        /// Handles player movement, actions etc sent by client.
        /// </summary>
        /// <param name="data">Data sent by client.</param>
        /// <param name="clientID">Internal client ID.</param>
        public void PlayerProcess(byte[] data, int clientID, string ipAddress)
        {
            try
            {
                int typeTemp = (int)BitConverter.ToInt32(data, 0);
                CommandType type;

                if (Enum.TryParse<CommandType>(typeTemp.ToString(), out type))
                {
                    switch (type)
                    {
                        case CommandType.RequestInitPlayer:
                            string characterName = Encoding.ASCII.GetString(data, 6, 10).Trim('\0');
                            string username = Encoding.ASCII.GetString(data, 16, 10).Trim('\0');
                            string password = Encoding.ASCII.GetString(data, 26, 10).Trim('\0');
                            bool observer;
                            if (!bool.TryParse(data[27].ToString(), out observer)) observer = false;
                            string gameName = Encoding.ASCII.GetString(data, 28, 10).Trim('\0');

                            InitPlayer(clientID, characterName, username, password);
                            break;
                        default:
                            /*if (type == CommandType.Common)
                            {
                                CommandMessageType subType = (CommandMessageType)BitConverter.ToInt16(data, 4);
                                if (subType == CommandMessageType.EquipItem)
                                    LogMessage(type.ToString() + " - " + subType.ToString() + " Length: " + data.Length, LogType.Test);
                            }*/
                            world.PlayerProcess(data, clientID); 
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error reading message from client (" + ipAddress + "):  " + ex.ToString(), LogType.Error);
            }
        }

        public void InitPlayer(int clientID, string characterName, string username, string password)
        {
            if (!IsPlayerLoggedIn(characterName))
            {
                Character character = characterData.Load(characterName);
                characterData.LoadInventory(character);
                characterData.LoadWarehouse(character);
                character.ClientInfo = server[clientID];
                character.ClientID = clientID;
                if (character.TotalLogins == 0) world.GiveStarterItems(character);
                character.TotalLogins++;
                world.AddPlayer(character);
            }
            else LogMessage(clientID + " cannot connect, [" + characterName + "] is already logged in.", LogType.Game);
        }

        public bool IsPlayerLoggedIn(string characterName)
        {
            Character c;
            if (world.FindPlayer(characterName, out c)) return true;
            else return false;
        }

        /// <summary>
        /// Removes a character from the game server.
        /// </summary>
        /// <param name="clientID">ID of the character to remove.</param>
        public void RemovePlayer(int clientID)
        {
            if (world.Players.ContainsKey(clientID) && world.Players[clientID] != null)
            {
                Character character = world.Players[clientID];
                characterData.SaveCharacter(character);
                world.RemovePlayer(clientID);
            }
        }

        /// <summary>
        /// Initializes async server listener for incoming connections
        /// </summary>
        public void Start()
        {
            try
            {
                if (!LoadItemConfiguration())
                {
                    LogMessage("CRITICAL ERROR: Item configuration not loaded (items.xml). Game server not started.", LogType.Game);
                    return;
                }
                else LogMessage(World.ItemConfiguration.Count + " items loaded from items.xml", LogType.Game);

                if (!LoadNpcConfiguration())
                {
                    LogMessage("CRITICAL ERROR: Npc configuration not loaded (npc.xml). Game server not started.", LogType.Game);
                    return;
                }
                else LogMessage(World.NpcConfiguration.Count + " npc types loaded from npc.xml", LogType.Game);

                if (!LoadSkillsConfiguration())
                {
                    LogMessage("MINOR ERROR: Skills configuration not loaded (skills.xml). Game server not started.", LogType.Game);
                    ///return;
                }
                else LogMessage("Skill items loaded from skills.xml - Alchemy: " + World.AlchemyConfiguration.Count + "   Crafting: " + World.CraftingConfiguration.Count + "   Manufacturing: " + World.ManufacturingConfiguration.Count, LogType.Game);

                if (!LoadMagicConfiguration())
                {
                    LogMessage("CRITICAL ERROR: Magic configuration not loaded (magic.xml). Game server not started.", LogType.Game);
                    return;
                }
                else LogMessage(World.MagicConfiguration.Count + " magic types loaded from magic.xml", LogType.Game);

                if (!world.LoadMaps())
                {
                    LogMessage("CRITICAL ERROR: 1 or more maps failed to load. Game server not started.", LogType.Game);
                    return;
                }

                if (!LoadEventsConfiguration())
                {
                    LogMessage("CRITICAL ERROR: Failed to load World Events configuration (events.xml).", LogType.Game);
                    return;
                }

                if (!LoadGuilds())
                {
                    LogMessage("CRITICAL ERROR: Failed to load Guilds from the database.", LogType.Game);
                    return;
                }

                if (!LoadQuestConfiguration())
                {
                    LogMessage("CRITICAL ERROR: Failed to load Quests configuration (quests.xml).", LogType.Game);
                    return;
                }

                // connect to login server to register
                Socket socket = Sockets.CreateTCPSocket(loginAddress, loginPort);
                loginServerClient = new ClientInfo(socket, true);
                loginServerClient.OnReadBytes += new ConnectionReadBytes(ReadMessageFromLoginServer);

                StringBuilder mapList = new StringBuilder();
                mapList.Append("REGISTER|" + name + "|" + externalAddress + "|" + port + "|");
                foreach (Map map in world.Maps.Values)
                {
                    mapList.Append(map.Name);
                    mapList.Append("|");
                }
                loginServerClient.Send(mapList.ToString());

                server = new ServerInfo(port);
                server.ClientReady += new ClientEvent(ClientReady);
                server.DefaultEncryptionType = EncryptionType.None;
                // original HB uses custom encryption, emulated during Send() see HelbreathWorld.Utility.Encrypt

                messages = new Queue<Command>();
                mainThread = new Thread(new ThreadStart(MainLoop));
                mainThread.Start();


                scheduler.Start(world);
                LogMessage(string.Format("Task scheduler started. {0} tasks in schedule", scheduler.Tasks.Count), LogType.Game);

                running = true;
                startTime = DateTime.Now;
                LogMessage(string.Format("Game server started successfully. Listening to clients on port {0}", port), LogType.Game);
            }
            catch (Exception ex)
            {
                LogMessage("Error starting server: " + ex.ToString(), LogType.Error);
                LogMessage("CRITICAL ERROR: Error starting game server on port " + port + ". Game server not started. ", LogType.Game);
            }
        }

        public void Stop()
        {
            if (loginServerClient != null) loginServerClient.Close();
            if (server != null) server.Close();
            if (mainThread != null && mainThread.IsAlive) mainThread.Abort();
        }

        private bool LoadItemConfiguration()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\items.xml";

                World.ItemConfiguration = ItemHelper.LoadItems(configPath);

                return true;
            }
            catch (Exception ex)
            {
                LogMessage("Error loading items: " + ex.ToString(), LogType.Error);
                return false;
            }
        }

        private bool OnGuildCreated(Guild guild)
        {
            if (!guildData.GuildExists(guild.Name) &&
                 guildData.CreateGuild(guild))
                return true;
            else return false;
        }

        private bool OnGuildDisbanded(Guild guild)
        {
            if (guildData.GuildExists(guild.Name) &&
                 guildData.DeleteGuild(guild))
                return true;
            else return false;
        }

        private bool LoadGuilds()
        {
            try
            {
                world.Guilds = guildData.LoadGuilds();

                foreach (Guild guild in world.Guilds.Values)
                {
                    guild.World = world;
                    guildData.LoadGuildMembers(guild);
                }

                return true;
            }
            catch (Exception ex)
            {
                LogMessage("Error loading guilds: " + ex.ToString(), LogType.Error);
                return false;
            }
        }

        private bool LoadQuestConfiguration()
        {
            try
            {
                string installPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");

                XmlTextReader reader = new XmlTextReader(installPath + @"\quests.xml");

                while (reader.Read())
                    if (reader.IsStartElement())
                        if (reader.Name.Equals("Quest"))
                            if (!World.QuestConfiguration.ContainsKey(Int32.Parse(reader["Id"])))
                            {
                                Quest quest;
                                switch ((QuestType)Enum.Parse(typeof(QuestType), reader["Type"]))
                                {
                                    case QuestType.Slayer: quest = Quest.ParseXml(reader); break;
                                    default: quest = null; break;
                                }

                                if (quest != null) World.QuestConfiguration.Add(quest.Id, quest);
                            }
                reader.Close();

                return true;
            }
            catch (Exception ex)
            {
                LogMessage("Error loading quests: " + ex.ToString(), LogType.Error);
                return false;
            }
        }

        private bool LoadEventsConfiguration()
        {
            try
            {
                string installPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");

                XmlTextReader reader = new XmlTextReader(installPath + @"\events.xml");

                while (reader.Read())
                    if (reader.IsStartElement())
                        switch (reader.Name)
                        {
                            case "Crusade":
                                World.CrusadeConfiguration.ManaPerStrike = Int32.Parse(reader["ManaPerStrike"]);
                                World.CrusadeConfiguration.StructuresPerGuild = Int32.Parse(reader["StructuresPerGuild"]);
                                XmlReader crusadeReader = reader.ReadSubtree();
                                while (crusadeReader.Read())
                                    if (crusadeReader.IsStartElement())
                                        switch (crusadeReader.Name)
                                        {
                                            case "Structure":
                                                if (World.NpcConfiguration.ContainsKey(crusadeReader["Npc"]))
                                                    World.CrusadeConfiguration.Structures[World.NpcConfiguration[crusadeReader["Npc"]].Side].Add(new WorldEventStructure(crusadeReader["Map"], Int32.Parse(crusadeReader["X"]), Int32.Parse(crusadeReader["Y"]), crusadeReader["Npc"]));
                                                break;
                                            case "StrikePoint":
                                                CrusadeStrikePoint sp = new CrusadeStrikePoint(crusadeReader["Map"], Int32.Parse(crusadeReader["X"]), Int32.Parse(crusadeReader["Y"]), crusadeReader["RelatedMap"], Int32.Parse(crusadeReader["HitPoints"]));
                                                OwnerSide spside = OwnerSide.Neutral;
                                                if (crusadeReader["Map"].Equals(Globals.AresdenTownName)) spside = OwnerSide.Aresden;
                                                else if (crusadeReader["Map"].Equals(Globals.ElvineTownName)) spside = OwnerSide.Elvine;
                                                else if (crusadeReader["Map"].Equals(Globals.MiddlelandName)) spside = OwnerSide.Neutral;
                                                XmlReader strikePointReader = crusadeReader.ReadSubtree();
                                                while (strikePointReader.Read())
                                                    if (strikePointReader.IsStartElement() && strikePointReader.Name.Equals("Location"))
                                                        sp.EffectLocations.Add(new Location(Int32.Parse(strikePointReader["X"]), Int32.Parse(strikePointReader["Y"])));
                                                World.CrusadeConfiguration.StrikePoints[spside].Add(sp);
                                                break;
                                            case "Event":
                                                scheduler.AddTask(ScheduledTask.ParseXml("Crusade", TaskType.WorldEvent, crusadeReader));
                                                break;
                                        }
                                break;
                            case "Heldenian":
                                XmlReader heldenianReader = reader.ReadSubtree();
                                while (heldenianReader.Read())
                                    if (heldenianReader.IsStartElement())
                                        switch (heldenianReader.Name)
                                        {
                                            case "Structure":
                                                if (World.NpcConfiguration.ContainsKey(heldenianReader["Npc"]))
                                                    World.HeldenianConfiguration.BattleFieldStructures.Add(new WorldEventStructure(heldenianReader["Map"], Int32.Parse(heldenianReader["X"]), Int32.Parse(heldenianReader["Y"]), heldenianReader["Npc"]));
                                                break;
                                            case "Gate":
                                                World.HeldenianConfiguration.CastleGates.Add(new Location(heldenianReader["Map"], Int32.Parse(heldenianReader["X"]), Int32.Parse(heldenianReader["Y"])));
                                                break;
                                            case "WinningZone":
                                                World.HeldenianConfiguration.CastleWinningZone = new Location(heldenianReader["Map"], Int32.Parse(heldenianReader["X"]), Int32.Parse(heldenianReader["Y"]));
                                                break;
                                            case "Event":
                                                scheduler.AddTask(ScheduledTask.ParseXml("Heldenian", TaskType.WorldEvent, heldenianReader));
                                                break;
                                        }
                                break;
                            case "Apocalypse":
                                XmlReader apocalypseReader = reader.ReadSubtree();
                                while (apocalypseReader.Read())
                                    if (apocalypseReader.IsStartElement())
                                        switch (apocalypseReader.Name)
                                        {
                                            case "Portal":
                                                Portal portal = new Portal(new Location(apocalypseReader["Map"], Int32.Parse(apocalypseReader["X"]), Int32.Parse(apocalypseReader["Y"])), new Location(apocalypseReader["TargetMap"], -1, -1));
                                                portal.CurrentWorld = world;
                                                portal.PortalType = (PortalType)Enum.Parse(typeof(PortalType), apocalypseReader["PortalType"]);
                                                World.ApocalypseConfiguration.Portals.Add(portal.PortalLocation.MapName, portal);
                                                break;
                                            case "Event":
                                                scheduler.AddTask(ScheduledTask.ParseXml("Apocalypse", TaskType.WorldEvent, apocalypseReader));
                                                break;
                                        }
                                break;
                            case "CaptureTheFlag":
                                int intParser;
                                World.CaptureTheFlagConfiguration.NumberOfFlags = Int32.TryParse(reader["NumberOfFlags"], out intParser) ? Int32.Parse(reader["NumberOfFlags"]) : 3;
                                XmlReader ctfReader = reader.ReadSubtree();
                                while (ctfReader.Read())
                                    if (ctfReader.IsStartElement())
                                        switch (ctfReader.Name)
                                        {
                                            case "FlagLocation":
                                                switch (ctfReader["Map"].ToString())
                                                {
                                                    case "aresden":
                                                        World.CaptureTheFlagConfiguration.FlagLocations[(int)OwnerSide.Aresden] = new Location("aresden", Int32.Parse(ctfReader["X"]), Int32.Parse(ctfReader["Y"]));
                                                        break;
                                                    case "elvine":
                                                        World.CaptureTheFlagConfiguration.FlagLocations[(int)OwnerSide.Elvine] = new Location("elvine", Int32.Parse(ctfReader["X"]), Int32.Parse(ctfReader["Y"]));
                                                        break;
                                                }
                                                break;
                                            case "Event":
                                                scheduler.AddTask(ScheduledTask.ParseXml("CaptureTheFlag", TaskType.WorldEvent, ctfReader));
                                                break;
                                        }
                                break;
                        }

                try
                {
                    World.WorldEventResults = eventData.LoadEventResults();
                    return true;
                }
                catch (Exception ex)
                {
                    LogMessage("Error loading event results: " + ex.ToString(), LogType.Error);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error loading events: " + ex.ToString(), LogType.Error);
                return false;
            }
        }

        private bool LoadMagicConfiguration()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\magic.xml";

                World.MagicConfiguration = MagicHelper.LoadMagic(configPath);

                return true;
            }
            catch (Exception ex)
            {
                LogMessage("Error loading magic: " + ex.ToString(), LogType.Error);
                return false;
            }
        }

        private bool LoadNpcConfiguration()
        {
            
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\npc.xml";

                World.PrimaryLootConfiguration = new Dictionary<string, LootTable>();
                World.SecondaryLootConfiguration = new Dictionary<string, LootTable>();
                World.NpcConfiguration = NpcHelper.LoadNpcs(configPath, World.ItemConfiguration, out World.PrimaryLootConfiguration, out World.SecondaryLootConfiguration);

                return true;
            }
            catch (Exception ex)
            {
                LogMessage("Error loading npc: " + ex.ToString(), LogType.Game);
                return false;
            }
        }

        private bool LoadSkillsConfiguration()
        {
            try
            {
                string installPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");

                XmlTextReader reader = new XmlTextReader(installPath + @"\skills.xml");
                XmlReader itemReader;
                
                while (reader.Read())
                    if (reader.IsStartElement())
                        switch (reader.Name)
                        {
                            case "Manufacturing":
                                itemReader = reader.ReadSubtree();
                                while (itemReader.Read())
                                    if (itemReader.IsStartElement() && itemReader.Name.Equals("Item"))
                                    {
                                        if (World.ItemConfiguration.ContainsKey(reader["Name"]) &&
                                            !World.ManufacturingConfiguration.ContainsKey(reader["Name"]))
                                        {
                                            ManufactureItem manuItem = ManufactureItem.ParseXml(reader);
                                            World.ManufacturingConfiguration.Add(manuItem.Name, manuItem);
                                        }
                                    }
                                itemReader.Close();
                                break;
                            case "Alchemy":
                                itemReader = reader.ReadSubtree();
                                while (itemReader.Read())
                                    if (itemReader.IsStartElement() && itemReader.Name.Equals("Item"))
                                    {
                                        if (World.ItemConfiguration.ContainsKey(reader["Name"]) &&
                                            !World.AlchemyConfiguration.ContainsKey(reader["Name"]))
                                        {
                                            AlchemyItem alchItem = AlchemyItem.ParseXml(reader);
                                            World.AlchemyConfiguration.Add(alchItem.Name, alchItem);
                                        }
                                    }
                                itemReader.Close();
                                break;
                            case "Crafting":
                                itemReader = reader.ReadSubtree();
                                while (itemReader.Read())
                                    if (itemReader.IsStartElement() && itemReader.Name.Equals("Item") &&
                                            !World.CraftingConfiguration.ContainsKey(reader["Name"]))
                                    {
                                        if (World.ItemConfiguration.ContainsKey(reader["Name"]))
                                        {
                                            CraftingItem craftItem = CraftingItem.ParseXml(reader);
                                            World.CraftingConfiguration.Add(craftItem.Name, craftItem);
                                        }
                                    }
                                itemReader.Close();
                                break;
                            case "Magic":
                                itemReader = reader.ReadSubtree();
                                while (itemReader.Read())
                                    if (itemReader.IsStartElement() && itemReader.Name.Equals("Summon") &&
                                            !World.SummonConfiguration.ContainsKey(Int32.Parse(reader["Index"])))
                                    {
                                        if (World.NpcConfiguration.ContainsKey(reader["Mob"])) 
                                            World.SummonConfiguration.Add(Int32.Parse(reader["Index"]), reader["Mob"]);
                                    }
                                break;
                            default: break;
                        }
                reader.Close();

                return true;
            }
            catch (Exception ex)
            {
                LogMessage("Error loading skills: " + ex.ToString(), LogType.Error);
                return false;
            }
        }

        /// <summary>
        /// Sends data to a client.
        /// </summary>
        /// <param name="clientID">Internal ID of the client.</param>
        /// <param name="command">Byte array containing the data.</param>
        public void OnSendData(int clientID, byte[] command)
        {
            byte[] data = Utility.Encrypt(command, false);
            
            if (server[clientID] != null) server[clientID].Send(data);

            //LogMessage("Sent: " + Encoding.ASCII.GetString(command, 0, command.Length), LogType.Test);
        }

        #region NETWORK CONNECTIONS
        /// <summary>
        /// Establishes that a client has successfully connected.
        /// </summary>
        /// <param name="serv"></param>
        /// <param name="newClient"></param>
        /// <returns></returns>
        private bool ClientReady(ServerInfo serv, ClientInfo newClient)
        {
            newClient.MessageType = MessageType.Length;
            newClient.OnReadBytes += new ConnectionReadBytes(ReadMessageFromClient);
            newClient.OnClose += new ConnectionClosed(CloseMessage);
            newClient.OnMessageLogged += new HelbreathWorld.LogHandler(LogTestMessage);
            LogMessage("Client connected: " + newClient.ID + " (" + newClient.IPAddress + ")", LogType.Game);
            return true;
        }

        /// <summary>
        /// Handles disconnection of a client
        /// </summary>
        /// <param name="ci"></param>
        private void CloseMessage(ClientInfo ci)
        {
            LogMessage("Client disconnected: " + ci.ID + " (" + ci.IPAddress + ") - " + ci.CloseReason, LogType.Game);
            RemovePlayer(ci.ID);
        }

        /// <summary>
        /// Handles incoming messages from a client
        /// </summary>
        /// <param name="ci">ClientInfo object</param>
        /// <param name="data">Data received from the client</param>
        /// <param name="len">Length of data received</param>
        private void ReadMessageFromClient(ClientInfo ci, byte[] data, int len)
        {
            lock (processQueueLock)
            {            
                // find out if there is more than 1 packet here, if there is we need to split them up
                // and process them individually. this is by nature of the way TCP works using streams
                bool allPacketsProcessed = false;
                int pointer = 0; // packet start pointer
                while (!allPacketsProcessed)
                {
                    int size = BitConverter.ToInt16(data, pointer + 1); // data[1] and data[2] - size of the packet
                    if (size > data.Length) allPacketsProcessed = true;
                    else
                    {
                        byte[] packetData = new byte[size];
                        Buffer.BlockCopy(data, pointer, packetData, 0, size);

                        Command command = new Command(packetData, ci.ID);
                        processQueue.Enqueue(command);

                        // check if another header can be found. if so, check that its size > 0. if not, all packets processed
                        pointer += size + 3; // set the pointer to the index start of the next packet
                        if ((len >= pointer)
                            && BitConverter.ToInt16(data, pointer + 1) > 0)
                        { }
                        else allPacketsProcessed = true; // we're all good
                    }
                }
            }
        }

        private void ReadMessageFromLoginServer(ClientInfo ci, byte[] data, int len)
        {
            try
            {
                lock (messageLock)
                {

                    messages.Enqueue(new Command(Utility.Decrypt(data), ci.ID));
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error reading message from login server (" + ci.IPAddress + "):  " + ex.ToString(), LogType.Error);
            }
        }
        #endregion
        #region LOGGING
        public void LogMessage(string message)
        {
            if (MessageLogged != null) MessageLogged(name, message, LogType.Game);
        }

        public void LogMessage(string message, LogType logType)
        {
            if (MessageLogged != null) MessageLogged(name, message, logType);
        }

        public void LogConnectionMessage(string message)
        {
            if (MessageLogged != null) MessageLogged(name, message, LogType.Database);
        }

        public void LogTestMessage(string message)
        {
            if (MessageLogged != null) MessageLogged(name, message, LogType.Test);
        }
        #endregion
        #region PUBLIC PROPERTIES
        public World World
        {
            get { return world; }
            set { world = value; }
        }
        public Boolean Running
        {
            get { return running; }
        }

        public DateTime StartTime
        {
            get { return startTime; }
        }

        public String Name
        {
            get { return name; }
        }

        public String LoginServerAddress
        {
            get { return loginAddress; }
        }

        public String LoginPort
        {
            get { return loginPort.ToString(); }
        }

        public String ExternalAddress
        {
            get { return externalAddress; }
        }

        public String ExternalPort
        {
            get { return port.ToString(); }
        }
        #endregion
    }
}