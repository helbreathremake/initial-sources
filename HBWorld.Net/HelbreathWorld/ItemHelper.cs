﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

using HelbreathWorld.Common.Assets;

namespace HelbreathWorld.Common
{
    public static class ItemHelper
    {
        public static Dictionary<string, Item> LoadItems(string configFilePath)
        {

            Dictionary<string, Item> items = new Dictionary<string, Item>();

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    if (reader.Name.Equals("Item"))
                        if (!items.ContainsKey(reader["Name"]))
                        {
                            Item item = Item.ParseXml(reader);
                            items.Add(item.Name, item);
                        }
            reader.Close();

            return items;
        }
    }
}
