﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace HelbreathWorld.Common.Assets
{
    public class ManufactureItem
    {
        private string name;
        private int difficulty;
        private int skillLimit;
        private int maximumPurity;
        private Dictionary<string, int> ingredients;

        public ManufactureItem()
        {
            ingredients = new Dictionary<string, int>();
        }

        public static ManufactureItem ParseXml(XmlReader r)
        {
            ManufactureItem manuItem = new ManufactureItem();
            manuItem.Name = r["Name"];
            manuItem.Difficulty = Int32.Parse(r["Difficulty"]);
            manuItem.SkillLimit = Int32.Parse(r["SkillLevel"]);
            manuItem.MaximumPurity = Int32.Parse(r["MaximumPurity"]);
            XmlReader ingrediantReader = r.ReadSubtree();
            while (ingrediantReader.Read())
                if (ingrediantReader.IsStartElement() && ingrediantReader.Name.Equals("Ingrediant") && !manuItem.Ingredients.ContainsKey(r["Name"]))
                    manuItem.Ingredients.Add(r["Name"], Int32.Parse(r["Count"]));
            ingrediantReader.Close();

            return manuItem;
        }

        public string Name { get { return name; } set { name = value; } }
        public int Difficulty { get { return difficulty; } set { difficulty = value; } }
        public int SkillLimit { get { return skillLimit; } set { skillLimit = value; } }
        public int MaximumPurity { get { return maximumPurity; } set { maximumPurity = value; } }
        public Dictionary<string, int> Ingredients { get { return ingredients; } set { ingredients = value; } }
    } 
}
