﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace HelbreathWorld.Common.Assets
{
    public class AlchemyItem
    {
        private string name;
        private int difficulty;
        private int skillLimit;
        private Dictionary<string, int> ingredients;

        public AlchemyItem()
        {
            ingredients = new Dictionary<string, int>();
        }

        public static AlchemyItem ParseXml(XmlReader r)
        {
            AlchemyItem alchItem = new AlchemyItem();
            alchItem.Name = r["Name"];
            alchItem.Difficulty = Int32.Parse(r["Difficulty"]);
            alchItem.SkillLimit = Int32.Parse(r["SkillLevel"]);
            XmlReader ingrediantReader = r.ReadSubtree();
            while (ingrediantReader.Read())
                if (ingrediantReader.IsStartElement() && ingrediantReader.Name.Equals("Ingrediant") && !alchItem.Ingredients.ContainsKey(r["Name"]))
                    alchItem.Ingredients.Add(r["Name"], Int32.Parse(r["Count"]));
            ingrediantReader.Close();

            return alchItem;
        }

        public string Name { get { return name; } set { name = value; } }
        public int Difficulty { get { return difficulty; } set { difficulty = value; } }
        public int SkillLimit { get { return skillLimit; } set { skillLimit = value; } }
        public Dictionary<string, int> Ingredients { get { return ingredients; } set { ingredients = value; } }
    }   
}
