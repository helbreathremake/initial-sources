﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelbreathWorld.Common.Assets.Objects
{
    public class NpcHuman : Npc
    {
        private List<Item> equipment;

        private int type;
        private GenderType gender;
        private SkinType skin;
        private int hairStyle;
        private int hairColour;
        private int underwearColour;

        private int appearance1;
        private int appearance3;
        private int appearance4; 
        private int appearanceColour;

        private AdvancedAIBehaviour behaviour;
        private AdvancedAIClass aiClass;
        private AdvancedAIEquipment aiEquipment;
        private AdvancedAIState state;
        private AdvancedAIDifficulty difficulty;

        public NpcHuman(AdvancedAIDifficulty difficulty, AdvancedAIBehaviour behaviour, AdvancedAIClass aiClass, AdvancedAIEquipment aiEquipment)
        {
            this.difficulty = difficulty;
            this.behaviour = behaviour;
            this.aiClass = aiClass;
            this.aiEquipment = aiEquipment;

            // randomize equipment if Random is specified
            if (this.aiEquipment == AdvancedAIEquipment.Random)
                switch (Dice.Roll(1, 4))
                {
                    case 1: this.aiEquipment = AdvancedAIEquipment.Heavy; break;
                    case 2: this.aiEquipment = AdvancedAIEquipment.Light; break;
                    case 3: this.aiEquipment = AdvancedAIEquipment.Medium; break;
                    default: this.aiEquipment = AdvancedAIEquipment.Civilian; break;
                }

            // set up appearance values
            skin = (SkinType)0; // TODO randomize
            hairColour = 1; // TODO randomize
            hairStyle = 1; // TODO randomize
            underwearColour = 1; // TODO randomize
            gender = (Dice.Roll(1, 2) == 1) ? GenderType.Male : GenderType.Female;
            type = (gender == GenderType.Male) ? 1 : 4;
            type += ((int)skin) - 1;
            

            // inherited
            Size = OwnerSize.Small;
            ActionTime = new TimeSpan(0, 0, 1);

            appearance1 = Appearance2 = appearance3 = appearance4 = appearanceColour = 0;
            appearance1 = appearance1 | underwearColour;
            appearance1 = appearance1 | (hairStyle << 8);
            appearance1 = appearance1 | (hairColour << 4);

            GenerateStats();
            GenerateEquipment();
            GenerateAppearance();
        }

        private void GenerateStats()
        {
            // TODO difficulties
            switch (aiClass)
            {
                case AdvancedAIClass.Archer: break; // TODO
                case AdvancedAIClass.BattleMage: break; // TODO
                case AdvancedAIClass.Mage: Magic = Vitality = Globals.MaximumStat; break; // TODO ellaborate
                case AdvancedAIClass.Warrior: Strength = Vitality = Globals.MaximumStat; break; // TODO ellaborate
            }
        }

        private void GenerateEquipment()
        {
            equipment = new List<Item>(16);
            for (int i = 0; i < 15; i++) equipment.Add(null);

            //TODO some sort of festive fun with santa suit?
            //bool isFullSuit = (DateTime.Now.Month == 12 && aiEquipment == AdvancedAIEquipment.Civilian && Dice.Roll(1, 5) > 4) ? true : false;

            // TODO put these items in to config? hard coded and messy
            switch (aiEquipment)
            {
                case AdvancedAIEquipment.Civilian:
                    equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration["Shirt(M)"].Copy() : World.ItemConfiguration["Shirt(W)"].Copy();
                    equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration["Tunic(M)"].Copy() : (Dice.Roll(1, 2) == 1) ? World.ItemConfiguration["Bodice(W)"].Copy() : World.ItemConfiguration["LongBodice(W)"].Copy();
                    equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? (Dice.Roll(1, 2) == 1) ? World.ItemConfiguration["Trousers(M)"].Copy() : World.ItemConfiguration["KneeTrousers(M)"].Copy() : (Dice.Roll(1, 2) == 1) ? World.ItemConfiguration["Trousers(W)"].Copy() : World.ItemConfiguration["Skirt(W)"].Copy();
                    switch (Dice.Roll(1,3)) 
                    {
                        case 1: equipment[(int)EquipType.Feet] = World.ItemConfiguration["LongBoots"].Copy(); break;
                        case 2: equipment[(int)EquipType.Feet] = World.ItemConfiguration["Shoes"].Copy(); break;
                        default: break;
                    }

                    // chance to have small shield
                    equipment[(int)EquipType.LeftHand] = (Dice.Roll(1, 3) == 1) ? World.ItemConfiguration["WoodShield"].Copy() : null;

                    // randomize colours of clothes  // TODO - hard coded 15 colours, may change with new client
                    equipment[(int)EquipType.Arms].Colour = Dice.Roll(1, 15);
                    equipment[(int)EquipType.Body].Colour = Dice.Roll(1, 15);
                    equipment[(int)EquipType.Legs].Colour = Dice.Roll(1, 15);
                    if (equipment[(int)EquipType.Feet] != null) equipment[(int)EquipType.Feet].Colour = Dice.Roll(1, 15); 

                    break;
                // TODO - other types
            }
        }

        private void GenerateAppearance()
        {
            int temp;

            for (int i = 0; i < 15; i++)
                if (equipment[i] != null)
                {
                    Item item = equipment[i];

                    switch (item.EquipType)
                    {
                        case EquipType.RightHand:
                            temp = Appearance2;
                            temp = temp & 0xF00F;
                            temp = temp | (item.Appearance << 4);
                            Appearance2 = temp;

                            temp = appearanceColour;
                            temp = temp & 0x0FFFFFFF;
                            temp = temp | (item.Colour << 28);
                            appearanceColour = temp;

                            unchecked
                            {
                                int speed = item.Speed - ((Strength) / 13);
                                if (speed < 0) speed = 0;

                                temp = Status;
                                temp = temp & (int)0xFFFFFFF0;
                                temp = temp | speed;
                                Status = temp;
                            }
                            break;
                        case EquipType.LeftHand:
                            temp = Appearance2;
                            temp = temp & 0xFFF0;
                            temp = temp | (item.Appearance);
                            Appearance2 = temp;

                            unchecked
                            {
                                temp = appearanceColour;
                                temp = temp & (int)0xF0FFFFFF;
                                temp = temp | (item.Colour << 24);
                                appearanceColour = temp;
                            }
                            break;
                        case EquipType.DualHand:
                            temp = Appearance2;
                            temp = temp & 0xF00F;
                            temp = temp | (item.Appearance << 4);
                            Appearance2 = temp;

                            unchecked
                            {
                                temp = appearanceColour;
                                temp = temp & (int)0x0FFFFFFF;
                                temp = temp | (item.Colour << 28);
                                appearanceColour = temp;
                            }

                            unchecked
                            {
                                int speed = item.Speed - ((Strength) / 13);
                                if (speed < 0) speed = 0;

                                temp = Status;
                                temp = temp & (int)0xFFFFFFF0;
                                temp = temp | speed;
                                Status = temp;
                            }
                            break;
                        case EquipType.Body:
                            if (item.Appearance < 100)
                            {
                                temp = appearance3;
                                temp = temp & 0x0FFF;
                                temp = temp | (item.Appearance << 12);
                                appearance3 = temp;
                            }
                            else
                            {
                                temp = appearance3;
                                temp = temp & 0x0FFF;
                                temp = temp | ((item.Appearance - 100) << 12);
                                appearance3 = temp;

                                temp = appearance4;
                                temp = temp | 0x080;
                                appearance4 = temp;
                            }

                            unchecked
                            {
                                temp = appearanceColour;
                                temp = temp & (int)0xFF0FFFFF;
                                temp = temp | (item.Colour << 20);
                                appearanceColour = temp;
                            }
                            break;
                        case EquipType.Back:
                            temp = appearance4;
                            temp = temp & 0xF0FF;
                            temp = temp | (item.Appearance << 8);
                            appearance4 = temp;

                            unchecked
                            {
                                temp = appearanceColour;
                                temp = temp & (int)0xFFF0FFFF;
                                temp = temp | (item.Colour << 16);
                                appearanceColour = temp;
                            }
                            break;
                        case EquipType.Arms:
                            temp = appearance3;
                            temp = temp & 0xFFF0;
                            temp = temp | (item.Appearance);
                            appearance3 = temp;

                            unchecked
                            {
                                temp = appearanceColour;
                                temp = temp & (int)0xFFFF0FFF;
                                temp = temp | (item.Colour << 12);
                                appearanceColour = temp;
                            }
                            break;
                        case EquipType.Legs:
                            temp = appearance3;
                            temp = temp & 0xF0FF;
                            temp = temp | (item.Appearance << 8);
                            appearance3 = temp;

                            unchecked
                            {
                                temp = appearanceColour;
                                temp = temp & (int)0xFFFFF0FF;
                                temp = temp | (item.Colour << 8);
                                appearanceColour = temp;
                            }
                            break;
                        case EquipType.Feet:
                            temp = appearance4;
                            temp = temp & 0x0FFF;
                            temp = temp | (item.Appearance << 12);
                            appearance4 = temp;

                            unchecked
                            {
                                temp = appearanceColour;
                                temp = temp & (int)0xFFFFFF0F;
                                temp = temp | (item.Colour << 4);
                                appearanceColour = temp;
                            }
                            break;
                        case EquipType.Head:
                            temp = appearance3;
                            temp = temp & 0xFF0F;
                            temp = temp | (item.Appearance << 4);
                            appearance3 = temp;

                            unchecked
                            {
                                temp = appearanceColour;
                                temp = temp & (int)0xFFFFFFF0;
                                temp = temp | (item.Colour);
                                appearanceColour = temp;
                            }
                            break;
                        case EquipType.FullBody:
                            temp = appearance3;
                            temp = temp & 0x0FFF;
                            temp = temp | (item.Appearance << 12);
                            appearance3 = temp;

                            unchecked
                            {
                                temp = appearanceColour;
                                temp = temp & (int)0xFFF0FFFF;
                                appearanceColour = temp;
                            }
                            break;
                    }
                }
        }

        public int Appearance1 { get { return appearance1; } set { appearance1 = value; } }
        public int Appearance3 { get { return appearance3; } set { appearance3 = value; } }
        public int Appearance4 { get { return appearance4; } set { appearance4 = value; } }
        public int AppearanceColour { get { return appearanceColour; } set { appearanceColour = value; } }
        public AdvancedAIBehaviour Behaviour { get { return behaviour; } set { behaviour = value; } }
        public AdvancedAIClass Class { get { return aiClass; } set { aiClass = value; } }
        public AdvancedAIEquipment Equipment { get { return aiEquipment; } set { aiEquipment = value; } }
        public AdvancedAIState State { get { return state; } set { state = value; } }
    }
}
