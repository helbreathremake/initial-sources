﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelbreathWorld.Common.Assets.Objects.Dynamic
{
    class IceStorm : IDynamicObject
    {
        public event DynamicObjectHandler IceStormRemoved;

        private int id;
        private World currentWorld;
        private int x, y;
        private Item item;
        private int difficulty;

        private int mapX;
        private int mapY;
        private Map map;

        private DateTime creationTime;
        private TimeSpan lastTime;
        private int freezeChance;

        public IceStorm(int id)
        {
            this.id = id;
        }

        public int ID { get { return id; } }
        public DynamicObjectType Type { get { return DynamicObjectType.IceStorm; } }
        public World CurrentWorld { get { return currentWorld; } set { currentWorld = value; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return true; } }
        public TimeSpan LastTime { get { return lastTime; } set { lastTime = value; } }
        public int FreezeChance { get { return freezeChance; } set { freezeChance = value; } }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
            targetMap[targetY][targetX].SetDynamicObject(this);
            creationTime = DateTime.Now;
        }

        public void TimerProcess()
        {
            TimeSpan ts = DateTime.Now - creationTime;
            if (ts.TotalSeconds > lastTime.TotalSeconds) Remove();

            // TODO - hardcoded poop again - damage and 20 second freezing
            int damage = Dice.Roll(3, 3) + 5;

            // TODO - hardcoded range
            for (int y = mapY - 2; y <= mapY + 2; y++)
                for (int x = mapX - 2; x <= mapX + 2; x++)
                    if (map[y][x].IsOccupied)
                    {
                        map[y][x].Owner.TakeDamage(DamageType.Environment, damage, MotionDirection.None);

                        if (!map[y][x].Owner.EvadeIce())
                            map[y][x].Owner.SetMagicEffect(new Magic(MagicType.Ice, 20));
                    }
        }

        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (IceStormRemoved != null) IceStormRemoved(this);
        }
    }
}
