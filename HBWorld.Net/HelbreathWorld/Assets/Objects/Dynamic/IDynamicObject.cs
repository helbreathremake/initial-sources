﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelbreathWorld.Common.Assets.Objects.Dynamic
{
    public interface IDynamicObject
    {
        World CurrentWorld { get; set; }
        int ID { get; }
        DynamicObjectType Type { get; }
        Map CurrentMap { get; }
        MapTile CurrentLocation { get; }
        int X { get; }
        int Y { get; }
        bool IsTraversable { get; }

        void Init(Map targetMap, int targetX, int targetY);
        void TimerProcess();
        void Remove();
    }
}
