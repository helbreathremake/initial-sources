﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HelbreathWorld.Common;    

namespace HelbreathWorld.Common.Assets.Objects.Dynamic
{
    public class Fish : IDynamicObject
    {
        public event DynamicObjectHandler FishRemoved;

        private int id;
        private World currentWorld;
        private int x, y;
        private Item item;
        private int difficulty;
        private FishType type;

        private int mapX;
        private int mapY;
        private Map map;

        public Fish(int id)
        {
            this.id = id;
        }

        public int ID
        {
            get { return id; }
        }

        public DynamicObjectType Type
        {
            get
            {
                switch (type)
                {
                    default:
                    case FishType.Fish: return DynamicObjectType.Fish;
                    case FishType.Item: return DynamicObjectType.FishObject;
                }
            }
        }

        public FishType FishType
        {
            get { return type; }
        }

        public World CurrentWorld { get { return currentWorld; } set { currentWorld = value; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return false; } }

        public Item FishItem
        {
            get { return item; }
        }

        public int Difficulty
        {
            get { return difficulty; }
        }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
            targetMap[targetY][targetX].SetDynamicObject(this);
            GenerateFish();
        }

        public void TimerProcess()
        {
            return;
        }

        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (FishRemoved != null) FishRemoved(this);
        }

        private void GenerateFish()
        {
            type = FishType.Fish; // default type
            switch (Dice.Roll(1, 9))
            {
                case 1:
                    difficulty = Dice.Roll(1, 10, 5);
                    item = World.ItemConfiguration["RedCarp"].Copy();
                    break;
                case 2:
                    difficulty = Dice.Roll(1, 5, 15);
                    item = World.ItemConfiguration["GreenCarp"].Copy();
                    break;
                case 3:
                    difficulty = Dice.Roll(1, 10, 20);
                    item = World.ItemConfiguration["GoldCarp"].Copy();
                    break;
                case 4:
                    difficulty = 1;
                    item = World.ItemConfiguration["CrucianCarp"].Copy();
                    break;
                case 5:
                    difficulty = Dice.Roll(1, 15, 1);
                    item = World.ItemConfiguration["BlueSeaBream"].Copy();
                    break;
                case 6:
                    difficulty = Dice.Roll(1, 18, 1);
                    item = World.ItemConfiguration["Salmon"].Copy();
                    break;
                case 7:
                    difficulty = Dice.Roll(1, 12, 1);
                    item = World.ItemConfiguration["RedSeaBream"].Copy();
                    break;
                case 8:
                    difficulty = Dice.Roll(1, 10, 1);
                    item = World.ItemConfiguration["GrayMullet"].Copy();
                    break;
                case 9:
                    type = FishType.Item; // ensures that bubbles are shown by setting this fish's Type to DynamicObject.FishObject
                    switch (Dice.Roll(1, 150))
                    {
                        case 1:
                        case 2:
                        case 3:
                            difficulty = Dice.Roll(4, 4, 20);
                            item = World.ItemConfiguration["PowerGreenPotion"].Copy();
                            break;
                        case 10:
                        case 11:
                            difficulty = Dice.Roll(4, 4, 20);
                            item = World.ItemConfiguration["SuperGreenPotion"].Copy();
                            break;
                        case 20:
                            difficulty = Dice.Roll(4, 4, 5);
                            item = World.ItemConfiguration["Dagger+1"].Copy(); // TODO should be +2 but I dont have the item config!
                            break;
                        case 30:
                            difficulty = Dice.Roll(4, 4, 10);
                            item = World.ItemConfiguration["LongSword+2"].Copy();
                            break;
                        case 40:
                            difficulty = Dice.Roll(4, 4, 15);
                            item = World.ItemConfiguration["Scimitar+2"].Copy();
                            break;
                        case 50:
                            difficulty = Dice.Roll(4, 4, 35);
                            item = World.ItemConfiguration["Rapier+2"].Copy();
                            break;
                        case 60:
                            difficulty = Dice.Roll(4, 4, 40);
                            item = World.ItemConfiguration["Flameberge+2"].Copy();
                            break;
                        case 70:
                            difficulty = Dice.Roll(4, 4, 30);
                            item = World.ItemConfiguration["WarAxe+2"].Copy();
                            break;
                        case 90:
                            difficulty = Dice.Roll(4, 4, 30);
                            item = World.ItemConfiguration["Ruby"].Copy();
                            break;
                        case 96:
                            difficulty = Dice.Roll(4, 4, 30);
                            item = World.ItemConfiguration["Diamond"].Copy();
                            break;
                        default:
                            difficulty = Dice.Roll(1, 10);
                            item = World.ItemConfiguration["LongBoots"].Copy();
                            break;
                    }
                    break;
            }
        }
    }
}
