﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelbreathWorld.Common.Assets.Objects.Dynamic
{
    class Spikes : IDynamicObject
    {
        public event DynamicObjectHandler SpikeFieldRemoved;

        private int id;
        private World currentWorld;
        private int x, y;
        private Item item;
        private int difficulty;
        private DynamicObjectType type;

        private int mapX;
        private int mapY;
        private Map map;

        private DateTime creationTime;
        private TimeSpan lastTime;
        private Dice damage;

        public Spikes(int id)
        {
            this.id = id;
        }

        public int ID { get { return id; } }
        public DynamicObjectType Type { get { return DynamicObjectType.SpikeField; } }
        public World CurrentWorld { get { return currentWorld; } set { currentWorld = value; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return true; } }
        public TimeSpan LastTime { get { return lastTime; } set { lastTime = value; } }
        public Dice Damage { get { return damage; } set { damage = value; } }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
            targetMap[targetY][targetX].SetDynamicObject(this);

            creationTime = DateTime.Now;
        }

        public void TimerProcess()
        {
            TimeSpan ts = DateTime.Now - creationTime;
            if (ts.TotalSeconds > lastTime.TotalSeconds) Remove();
        }

        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (SpikeFieldRemoved != null) SpikeFieldRemoved(this);
        }
    }
}
