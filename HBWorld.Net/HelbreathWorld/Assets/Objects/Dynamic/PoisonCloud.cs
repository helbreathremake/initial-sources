﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelbreathWorld.Common.Assets.Objects.Dynamic
{
    class PoisonCloud : IDynamicObject
    {
        public event DynamicObjectHandler PoisonCloudRemoved;

        private int id;
        private World currentWorld;
        private int mapX;
        private int mapY;
        private Map map;

        private DateTime creationTime;
        private TimeSpan lastTime;
        private int poisonDamage;

        public PoisonCloud(int id)
        {
            this.id = id;
        }

        public int ID { get { return id; } }
        public DynamicObjectType Type { get { return DynamicObjectType.PoisonCloudBegin; } }
        public World CurrentWorld { get { return currentWorld; } set { currentWorld = value; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return true; } }
        public TimeSpan LastTime { get { return lastTime; } set { lastTime = value; } }
        public int PoisonDamage { get { return poisonDamage; } set { poisonDamage = value; } }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
            targetMap[targetY][targetX].SetDynamicObject(this);

            creationTime = DateTime.Now;
        }

        public void TimerProcess()
        {
            TimeSpan ts = DateTime.Now - creationTime;
            if (ts.TotalSeconds > lastTime.TotalSeconds) Remove();

            // TODO - more damage and duration hard coding
            int damage = (poisonDamage < 20) ? Dice.Roll(1, 6) : Dice.Roll(1, 8);
            if (CurrentLocation.IsOccupied)
            {
                CurrentLocation.Owner.TakeDamage(DamageType.Environment, damage, MotionDirection.None);

                if (!CurrentLocation.Owner.EvadeMagic(500, false)) // static. maybe make changable in configs?
                    if (!CurrentLocation.Owner.EvadePoison())
                        CurrentLocation.Owner.SetMagicEffect(new Magic(MagicType.Poison, 20, poisonDamage));
            }
        }

        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (PoisonCloudRemoved != null) PoisonCloudRemoved(this);
        }
    }
}
