﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HelbreathWorld.Common;

namespace HelbreathWorld.Common.Assets.Objects.Dynamic
{
    public class Mineral : IDynamicObject
    {
        public event DynamicObjectHandler MineralRemoved;

        private int id;
        private World currentWorld;
        private int x, y;
        private int remainingMaterials;
        private MineralType type;

        private int mapX;
        private int mapY;
        private Map map;

        public Mineral(int id, MineralType type)
        {
            this.id = id;
            this.type = type;
        }

        /// <summary>
        /// Initilizes this Mineral on the map.
        /// </summary>
        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;

            int x, y;

            if (targetMap.GetEmptyTile(targetX, targetY, out x, out y))
            {
                this.mapX = x;
                this.mapY = y;
                targetMap[y][x].SetDynamicObject(this);
            }

            this.remainingMaterials = TotalMaterials;
        }

        public void TimerProcess()
        {
            return;
        }

        /// <summary>
        /// Removes this Mineral from the map
        /// </summary>
        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (MineralRemoved != null) MineralRemoved(this);
        }

        public Item Mine()
        {
            Item mineral = null;
            switch (type)
            {
                case MineralType.RockEasy:
                    switch (Dice.Roll(1, 5))
                    {
                        case 1:
                        case 2:
                        case 3: mineral = World.ItemConfiguration["Coal"].Copy(); break;
                        case 4: mineral = World.ItemConfiguration["IronOre"].Copy(); break;
                        case 5: mineral = World.ItemConfiguration["BlondeStone"].Copy(); break;
                    }
                    break;
                case MineralType.RockMedium:
                    switch (Dice.Roll(1, 5))
                    {
                        case 1:
                        case 2:
                        case 3: mineral = World.ItemConfiguration["Coal"].Copy(); break;
                        case 4: mineral = World.ItemConfiguration["IronOre"].Copy(); break;
                        case 5:
                            switch (Dice.Roll(1, 3))
                            {
                                case 2: mineral = World.ItemConfiguration["SilverNugget"].Copy(); break;
                                default: mineral = World.ItemConfiguration["GoldNugget"].Copy(); break;
                            }
                            break;
                    }
                    break;
                case MineralType.RockHard:
                    switch (Dice.Roll(1, 6))
                    {
                        case 1: mineral = World.ItemConfiguration["Coal"].Copy(); break;
                        case 2:
                        case 3:
                        case 4:
                        case 5: mineral = World.ItemConfiguration["IronOre"].Copy(); break;
                        case 6:
                            switch (Dice.Roll(1, 8))
                            {
                                case 3:
                                    switch (Dice.Roll(1, 2))
                                    {
                                        case 1: mineral = World.ItemConfiguration["SilverNugget"].Copy(); break;
                                        default: mineral = World.ItemConfiguration["GoldNugget"].Copy(); break; // was IronOre - bug in old code?
                                    }
                                    break;
                                default: mineral = World.ItemConfiguration["IronOre"].Copy(); break;
                            }
                            break;
                    }
                    break;
                case MineralType.RockExtreme:
                    switch (Dice.Roll(1, 5))
                    {
                        case 1:
                        case 2: mineral = World.ItemConfiguration["Coal"].Copy(); break;
                        case 3:
                        case 4: mineral = World.ItemConfiguration["IronOre"].Copy(); break;
                        case 5:
                            switch (Dice.Roll(1, 5))
                            {
                                case 1:
                                case 2: mineral = World.ItemConfiguration["GoldNugget"].Copy(); break;
                                case 3:
                                case 4: mineral = World.ItemConfiguration["SilverNugget"].Copy(); break;
                                case 5: mineral = World.ItemConfiguration["Mithral"].Copy(); break;
                            }
                            break;
                    }
                    break;
                case MineralType.GemEasy:
                    switch (Dice.Roll(1, 18))
                    {
                        case 3: mineral = World.ItemConfiguration["Sapphire"].Copy(); break;
                        default: mineral = World.ItemConfiguration["Crystal"].Copy(); break;
                    }
                    break;
                case MineralType.GemHard:
                    switch (Dice.Roll(1, 5))
                    {
                        case 1:
                            switch (Dice.Roll(1, 6))
                            {
                                case 3: mineral = World.ItemConfiguration["Emerald"].Copy(); break;
                                default: mineral = World.ItemConfiguration["Crystal"].Copy(); break;
                            }
                            break;
                        case 2:
                            switch (Dice.Roll(1, 6))
                            {
                                case 3: mineral = World.ItemConfiguration["Sapphire"].Copy(); break;
                                default: mineral = World.ItemConfiguration["Crystal"].Copy(); break;
                            }
                            break;
                        case 3:
                            switch (Dice.Roll(1, 6))
                            {
                                case 3: mineral = World.ItemConfiguration["Ruby"].Copy(); break;
                                default: mineral = World.ItemConfiguration["Crystal"].Copy(); break;
                            }
                            break;
                        case 4: mineral = World.ItemConfiguration["Crystal"].Copy(); break;
                        case 5:
                            switch (Dice.Roll(1, 12))
                            {
                                case 3: mineral = World.ItemConfiguration["Diamond"].Copy(); break;
                                default: mineral = World.ItemConfiguration["Crystal"].Copy(); break;
                            }
                            break;
                    }
                    break;
            }

            if (mineral != null)
            {
                remainingMaterials--;
                if (remainingMaterials <= 0) Remove();
            }

            return mineral;
        }

        public int ID
        {
            get { return id; }
        }

        public DynamicObjectType Type
        {
            get
            {
                switch (type)
                {
                    default:
                    case MineralType.RockEasy: return DynamicObjectType.Rock;
                    case MineralType.RockMedium: return DynamicObjectType.Rock;
                    case MineralType.RockHard: return DynamicObjectType.Rock;
                    case MineralType.RockExtreme: return DynamicObjectType.Rock;
                    case MineralType.GemEasy: return DynamicObjectType.Gem;
                    case MineralType.GemHard: return DynamicObjectType.Gem;
                }
            }
        }

        public int Difficulty
        {
            get
            {
                switch (type)
                {
                    default:
                    case MineralType.RockEasy: return 10;
                    case MineralType.RockMedium: return 15;
                    case MineralType.RockHard: return 20;
                    case MineralType.RockExtreme: return 50;
                    case MineralType.GemEasy: return 70;
                    case MineralType.GemHard: return 90;
                }
            }
        }

        public int TotalMaterials
        {
            get
            {
                switch (type)
                {
                    default:
                    case MineralType.RockEasy: return 20;
                    case MineralType.RockMedium: return 15;
                    case MineralType.RockHard: return 10;
                    case MineralType.RockExtreme: return 8;
                    case MineralType.GemEasy: return 6;
                    case MineralType.GemHard: return 4;
                }
            }
        }

        public int RemainingMaterials
        {
            get { return remainingMaterials; }
            set { remainingMaterials = value; }
        }

        public World CurrentWorld { get { return currentWorld; } set { currentWorld = value; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return false; } }
    }
}
