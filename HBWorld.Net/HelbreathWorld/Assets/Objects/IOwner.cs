﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HelbreathWorld.Common.Assets.Objects.Dynamic;

namespace HelbreathWorld.Common.Assets.Objects
{
    public interface IOwner
    {
        World CurrentWorld { get; set; }
        int ID { get; }
        OwnerType OwnerType { get; }
        Dictionary<MagicType, MagicEffect> MagicEffects { get; }
        OwnerSize Size { get; }
        OwnerSide Side { get; }
        MotionDirection Direction { get; }
        Map CurrentMap { get; }
        MapTile CurrentLocation { get; }
        int X { get; }
        int Y { get; }
        bool IsDead { get; }
        bool IsInvisible { get; }
        string Name { get; }
        int HP { get; }
        int MP { get; }
        int MaxHP { get; }
        int MaxMP { get; }
        List<int> Summons { get; set; }

        void Die(IOwner killer, int damage, bool dropLoot);
        void DepleteMP(int points);
        void DepleteHP(int points);
        void DepleteSP(int points);
        void ReplenishMP(int points);
        void ReplenishHP(int points);
        void ReplenishSP(int points);
        bool SetMagicEffect(Magic magic);
        bool RemoveMagicEffect(MagicType type);
        int TakeDamage(DamageType type, int damage, MotionDirection flyDirection);
        int TakeDamage(DamageType type, int damage, MotionDirection flyDirection, bool notify);
        void TakeArmourDamage(int damage);
        bool PrepareMagic(int sourceX, int sourceY, MotionDirection direction);
        //void Attack(int destinationX, int destinationY, IOwner target, int attackType, bool isDash); - TODO move attack code out of World
        bool MeleeAttack(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, IOwner target, int attackType, bool isDash);
        bool MagicAttack(int hitX, int hitY, IOwner target, Magic spell, MagicTarget magicTarget);
        bool Idle(int sourceX, int sourceY, MotionDirection direction);
        bool Run(int sourceX, int sourceY, MotionDirection direction);
        bool Move(int sourceX, int sourceY, MotionDirection direction);
        void Init(Map targetMap, int targetX, int targetY);
        bool Remove();
        bool EvadeMelee(int attackerHitChance);
        bool EvadeMagic(int attackerHitChance, bool ignorePFM);
        bool EvadeIce();
        bool EvadePoison();
        bool IsWithinRange(IOwner other, int rangeX, int rangeY);
        bool IsWithinRange(IOwner other, int rangeX, int rangeY, int modifier);
        bool IsWithinRange(IDynamicObject other, int rangeX, int rangeY, int modifier);
        byte[] GetFullObjectData(Character requester);
        void Talk(string message);
    }
}
