﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;

using HelbreathWorld.Common;
using HelbreathWorld.Common.Assets.Objects.Dynamic;

namespace HelbreathWorld.Common.Assets.Objects
{
    public class Npc : IOwner, IDeadOwner
    {
        public event OwnerHandler StatusChanged;
        public event MotionHandler MotionChanged;
        public event DamageHandler DamageTaken;
        public event DeathHandler Killed;
        public event ItemHandler ItemDropped;
        public event LogHandler MessageLogged;

        private Dictionary<MagicType, MagicEffect> magicEffects;
        private int id;
        private int spawnId;

        private string name;
        private string friendlyName;
        private int type;
        private MotionDirection direction;
        private MotionTurn turn; // which direction does this npc turn when hitting an obstacle during path finding
        private MotionType action;
        private MovementType moveType;
        private int appearance2;
        private int status;
        private bool canDropLoot; // flag for secondary drops
        private bool isDead;
        private bool isFriendly; // for wh/ch/bs guys
        private bool isPacifist; // for non-attackers
        private DateTime deadTime;
        private OwnerSize size;
        private OwnerSide side;
        private Guild guild;

        private int strength;
        private int dexterity;
        private int vitality;
        private int magic;
        private string[] spells;

        private int hp;
        private int mp;
        private int maxHP;
        private int maxMP;
        private int maxGold;
        private Dice experience;
        private int remainingExperience;
        private int pa; // natural physical absorption
        private int ma; // natural magical absorption
        private MagicAttribute absorptionType; // natural magical absorption type
        private int attackRange;
        private int searchRange; // target search

        private int mapX;
        private int mapY;
        private Location waypoint;
        private World world;
        private Map map;

        private Dice baseDamage;

        private List<int> summons;
        private int summoner;
        private int targetID;
        private OwnerType targetType;

        private DateTime hpUpTime;    // hp regen
        private DateTime mpUpTime;    // mp regen
        private DateTime poisonTime;  // poison time
        private DateTime lastActionTime; // last action
        private DateTime summonTime; // time summoned
        private TimeSpan maximumDeadTime; // time before disappear
        private TimeSpan actionTime; // time between each action

        private int buildType; // TODO rename these maybe? bit unfriendly (properties also)
        private int buildPoints; // crops/sade structures etc
        private int buildLimit; // limit for crop yeild probability

        // Monster AI
        private int actionCount;
        private int failedActionCount; // number of times the last action failed

        public Npc()
        {
            this.name = "Unknown";
            Init();
        }

        public Npc(string name)
        {
            this.name = name;
            Init();
        }

        public Npc Copy()
        {
            Npc npc = new Npc(this.name);
            npc.FriendlyName = this.friendlyName;
            npc.Type = this.type;
            npc.Size = this.size;
            npc.Side = this.side;
            npc.BaseDamage = this.baseDamage;
            npc.AttackRange = this.attackRange;
            npc.SearchRange = this.searchRange;
            npc.Dexterity = this.dexterity;
            npc.Vitality = this.vitality;
            npc.Magic = this.magic;
            npc.Spells = this.spells;
            npc.Experience = this.experience;
            npc.RemainingExperience = this.remainingExperience;
            npc.HP = this.MaxHP;
            npc.MP = this.MaxMP;
            npc.PhysicalAbsorption = this.pa;
            npc.MagicAbsorption = this.ma;
            npc.MaximumDeadTime = this.maximumDeadTime;
            npc.ActionTime = this.actionTime.Add(new TimeSpan(0,0,0,0, Dice.Roll(1, 300))); // get a bit of randomness
            npc.MaximumGold = this.maxGold;
            npc.CurrentAction = this.action;
            npc.MoveType = this.moveType;

            return npc;
        }

        public static Npc ParseXml(XmlReader r)
        {
            Npc npc = new Npc(r["Name"]);
            npc.FriendlyName = (r["FriendlyName"] != null) ? r["FriendlyName"] : npc.Name;
            npc.Type = Int32.Parse(r["Type"]);
            OwnerSize ownerSize;
            if (Enum.TryParse<OwnerSize>(r["Size"], out ownerSize))
                npc.Size = ownerSize;
            OwnerSide ownerSide;
            if (Enum.TryParse<OwnerSide>(r["Side"], out ownerSide))
                npc.Side = ownerSide;
            npc.AttackRange = Int32.Parse(r["AttackRange"]);
            npc.SearchRange = Int32.Parse(r["SearchRange"]);
            npc.BaseDamage = new Dice(Int32.Parse(r["BaseDamageThrow"]), Int32.Parse(r["BaseDamageRange"]), 0);
            npc.Dexterity = Int32.Parse(r["Dexterity"]);
            npc.Vitality = Int32.Parse(r["Vitality"]);
            npc.Magic = Int32.Parse(r["Magic"]);
            npc.Spells = (r["Spells"] != null) ? r["Spells"].Split(',') : new string[0];
            npc.Experience = new Dice(Int32.Parse(r["BaseExperienceThrow"]), 4, Int32.Parse(r["BaseExperienceThrow"]));
            npc.MaximumDeadTime = TimeSpan.Parse(r["MaximumDeadTime"]);
            npc.ActionTime = TimeSpan.Parse(r["ActionTime"]);
            npc.PhysicalAbsorption = Int32.Parse(r["PhysicalAbsorption"]);
            npc.MagicAbsorption = Int32.Parse(r["MagicAbsorption"]);
            npc.MaximumGold = (r["MaximumGold"] != null) ? Int32.Parse(r["MaximumGold"]) : 0;

            if (r["IsStationary"] != null && Boolean.Parse(r["IsStationary"].ToString()))
            {
                npc.CurrentAction = MotionType.Idle;
                npc.MoveType = MovementType.None;
            }

            if (r["IsStationaryAttack"] != null && Boolean.Parse(r["IsStationaryAttack"].ToString()))
            {
                npc.CurrentAction = MotionType.AttackStationary;
                npc.MoveType = MovementType.None;
            }

            return npc;
        }

        private void Init()
        {
            this.direction = MotionDirection.South;
            this.action = MotionType.Undefined;
            this.moveType = MovementType.Undefined;
            this.isDead = false;
            this.waypoint = null;
            this.spawnId = -1;
            this.guild = Guild.None; // no guild by default
            this.targetType = OwnerType.None;
            this.summoner = -1;
            this.summons = new List<int>(Globals.MaximumSummons);
            this.turn = Dice.Roll(1, 100) < 50 ? MotionTurn.Right : MotionTurn.Left;
        }

        /// <summary>
        /// Initilizes this Npc on the map.
        /// </summary>
        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            magicEffects = new Dictionary<MagicType, MagicEffect>();

            int x, y;

            if (targetMap.GetEmptyTile(targetX, targetY, out x, out y))
            {
                this.mapX = x;
                this.mapY = y;
                targetMap[y][x].SetOwner(this);
                targetMap.Npcs.Add(id);
            }
            //else MessageLogged(string.Format("WARNING - NPC ({0}) cannot initialize at ({3} - {1},{2}), check spawn locations in config.", name, targetX, targetY, map.FriendlyName), LogType.Game);

            // if no action/movetype defined by configs, then set to Move/randomarea as default
            if (action == MotionType.Undefined) action = MotionType.Move;
            if (moveType == MovementType.Undefined) moveType = MovementType.RandomArea;
            lastActionTime = DateTime.Now;
            remainingExperience = experience.Roll();
        }

        public void Talk(string message)
        {

        }

        public bool SetMagicEffect(Magic magic)
        {
            if (magicEffects.ContainsKey(magic.Type)) return false;
            magicEffects.Add(magic.Type, new MagicEffect(magic, DateTime.Now.Add(magic.LastTime)));

            // flags and notify
            unchecked
            {
                switch (magic.Type)
                {
                    case MagicType.Berserk:
                        status = status | 0x00000020;
                        break;
                    case MagicType.Invisibility:
                        status = status | 0x00000010;
                        break;
                    case MagicType.IceLine: // uses Ice effect type
                    case MagicType.Ice:
                        status = status | 0x00000040;
                        break;
                    case MagicType.Protect:
                        switch (magic.Effect1)
                        {
                            case 1: status = status | 0x08000000; break; // arrow
                            case 2:
                            case 5: status = status | 0x04000000; break; // magic
                            case 3:
                            case 4: status = status | 0x02000000; break; // physical
                        }
                        break;
                    case MagicType.Poison:
                        status = status | 0x00000080;
                        poisonTime = DateTime.Now;
                        break;
                    case MagicType.Paralyze: break;
                }
            }

            if (StatusChanged != null) StatusChanged(this);
            return true;
        }

        public bool RemoveMagicEffect(MagicType type)
        {
            if (!magicEffects.ContainsKey(type)) return false;

            // flags and notify
            unchecked
            {
                switch (type)
                {
                    case MagicType.Berserk:
                        status = status & (int)0xFFFFFFDF;
                        break;
                    case MagicType.Invisibility:
                        status = status & (int)0xFFFFFFEF;
                        break;
                    case MagicType.Ice:
                        status = status & (int)0xFFFFFFBF;
                        break;
                    case MagicType.Protect:
                        switch (magicEffects[type].Magic.Effect1)
                        {
                            case 1: status = status & (int)0xF7FFFFFF; break; // arrows
                            case 2:
                            case 5: status = status & (int)0xFBFFFFFF; break; // magic
                            case 3:
                            case 4: status = status & (int)0xFDFFFFFF; break; // physical
                        }
                        break;
                    case MagicType.Poison:
                        status = status & (int)0xFFFFFF7F;
                        break;
                    case MagicType.Paralyze: break;
                }
            }

            magicEffects.Remove(type);
            if (StatusChanged != null) StatusChanged(this);
            return true;
        }

        /// <summary>
        /// Actions to be performed on a timer such as mp/hp regeneration etc.
        /// </summary>
        public void TimerProcess()
        {
            if (isDead) return;

            TimeSpan ts;

            // handle poison time
            if (magicEffects.ContainsKey(MagicType.Poison))
            {
                ts = DateTime.Now - poisonTime;
                if (ts.TotalSeconds >= Globals.PoisonTime)
                {
                    int damage = Dice.Roll(1, magicEffects[MagicType.Poison].Magic.Effect2);
                    DepleteHP((hp - damage < 1) ? hp - 1 : damage); // cant go below 1 hp
                    if (EvadePoison()) RemoveMagicEffect(MagicType.Poison);
                }
            }

            // handle magic effect removal if time has expired
            if (magicEffects.Count > 0)
                for (int i = 0; i < 50; i++)
                    if (magicEffects.ContainsKey((MagicType)i))
                    {
                        MagicEffect effect = magicEffects[(MagicType)i];
                        if (effect.Magic.Type != MagicType.Poison) // poison have their own timer
                        {
                            ts = DateTime.Now - effect.StartTime;
                            if (ts.TotalSeconds >= effect.Magic.LastTime.TotalSeconds) // use total seconds as some spells last over 59 seconds
                                RemoveMagicEffect(effect.Magic.Type);
                        }
                    }
        }

        public int TakeDamage(DamageType type, int damage, MotionDirection flyDirection) { return TakeDamage(null, type, damage, flyDirection, true); }
        public int TakeDamage(DamageType type, int damage, MotionDirection flyDirection, bool notify) { return TakeDamage(null, type, damage, flyDirection, notify); }
        public int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection) { return TakeDamage(attacker, type, damage, flyDirection, true); }
        public int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection, bool notify)
        {
            DepleteHP(damage);

            // remove paralyze for different damage types
            if (magicEffects.ContainsKey(MagicType.Paralyze))
                switch (type)
                {
                    case DamageType.Magic: if (magicEffects[MagicType.Paralyze].Magic.Effect1 == 2) RemoveMagicEffect(MagicType.Paralyze); break;
                    case DamageType.Ranged:
                    case DamageType.Melee: if (magicEffects[MagicType.Paralyze].Magic.Effect1 == 1) RemoveMagicEffect(MagicType.Paralyze); break;
                    case DamageType.Environment: RemoveMagicEffect(MagicType.Paralyze); break;
                }

            if (hp <= 0)
            {
                Die(attacker, damage, (!(IsSummoned || type == DamageType.Environment || (attacker != null && attacker.OwnerType == OwnerType.Npc))));
                if (!IsSummoned) return remainingExperience;
                else return 0;
            }
            else
            {
                if (notify) DamageTaken(this, type, damage, flyDirection);

                // TODO - counter attack

                if (!IsSummoned)
                {
                    if ((remainingExperience - damage) >= 0)
                    {
                        remainingExperience -= damage;
                        return remainingExperience;
                    }
                    else return 0;
                }
                else return 0;
            }
        }

        public void TakeArmourDamage(int damage)
        {
            // unused at the moment
        }

        public void DepleteMP(int points)
        {
            this.mp -= points;
            if (mp < 0) mp = 0;
        }
        public void ReplenishMP(int points)
        {
            mp += points;
            if (NpcType != NpcType.CrusadeGrandMagicGenerator && // grand magic generators maxMP is determined in crusade config (ManaPerStrike)
                mp > MaxMP) mp = MaxMP;
        }
        public void DepleteHP(int points)
        {
            hp -= points;
            if (hp < 0) hp = 0;
        }
        public void ReplenishHP(int points)
        {
            hp += points;
            if (hp > MaxHP) hp = MaxHP;
        }
        public void DepleteSP(int points)
        { }
        public void ReplenishSP(int points)
        { }

        public void Die(IOwner killer, int damage, bool dropLoot)
        {
            if (isDead) return; // avoid die-ception

            isDead = true;
            canDropLoot = dropLoot; // stored for npc removal, secondary drop
            action = MotionType.Dead;
            deadTime = DateTime.Now;          
            map[mapY][mapX].ClearOwner();
            map[mapY][mapX].SetDeadOwner(this);

            if (IsSpawned) map.MobSpawns[spawnId].Count--;
            if (IsSummoned && world.Players.ContainsKey(summoner)) world.Players[summoner].Summons.Remove(id);

            if (world.IsHeldenianBattlefield)
                if (NpcType == NpcType.HeldenianArrowTower || NpcType == NpcType.HeldenianCannonTower)
                    world.Heldenian.TowerDestroyed(side);

            Killed(this, killer, damage);
            
            if (dropLoot) DropLoot(LootDrop.Primary);
        }

        public void DropLoot(LootDrop drop)
        {
            if (IsSummoned) return; // summons dont drop items
            if (world.IsCrusade) return; // no drops in sade
            if (!map.LootEnabled) return; // b.i etc
            if ((world.IsHeldenianBattlefield || world.IsHeldenianSeige) && map.IsHeldenianMap) return; // no drops in heldenian on heldenian maps

            Dictionary<string, LootTable> lootTable;
            switch (drop)
            {
                case LootDrop.Primary: 
                    if (Dice.Roll(1, 100) >= Globals.PrimaryDropRate) return;
                    lootTable = World.PrimaryLootConfiguration;
                    // TODO - 35% drop rate if heldenian winner
                    break;
                case LootDrop.Secondary: 
                    if (Dice.Roll(1, 100) >= Globals.SecondaryDropRate) return;
                    lootTable = World.SecondaryLootConfiguration;
                    break;
                default: return;
            }
            if (!lootTable.ContainsKey(name)) return; // no loot table exists for this npc

            ItemRarity rarity = ItemRarity.VeryCommon;
            int rarityChance = Dice.Roll(1, 100000);
            if (rarityChance == 100000) rarity = ItemRarity.UltraRare; // 1/100000 - 0.001%
            else if (rarityChance >= 99500 && rarityChance < 100000) rarity = ItemRarity.VeryRare; // 500/100000 - 0.5%
            else if (rarityChance >= 98500 && rarityChance < 99500) rarity = ItemRarity.Rare; // 1000/1000000 - 1%
            else if (rarityChance >= 88500 && rarityChance < 98500) rarity = ItemRarity.Uncommon; // 10000/100000 - 10%
            else if (rarityChance >= 48500 && rarityChance < 88500) rarity = ItemRarity.Common; // 40000/100000 - 40%
            else rarity = ItemRarity.VeryCommon; // 48500/100000 - 48.5%

            Item loot = null;

            int result = Dice.Roll(1, 100);
            if (result <= 35 && drop != LootDrop.Secondary && maxGold > 0) // 35% gold
            {
                loot = World.ItemConfiguration["Gold"].Copy();
                loot.Count = Math.Max(Dice.Roll(1, maxGold), 1); 
            }
            else if (result > 35 && result <= 75 && drop != LootDrop.Secondary) // 40% item
            {
                if (lootTable[name].GetLoot(rarity).Count <= 0) return;
                loot = World.ItemConfiguration[lootTable[name].GetLoot(rarity)[Math.Max(Dice.Roll(1, lootTable[name].GetLoot(rarity).Count) - 1, 0)]].Copy();
                loot.GenerateAttribute();
            }
            else if (result > 75) // 25% weapon/armour or secondary drop
            {
                if (lootTable[name].GetLoot(rarity).Count <= 0) return;
                loot = World.ItemConfiguration[lootTable[name].GetLoot(rarity)[Math.Max(Dice.Roll(1, lootTable[name].GetLoot(rarity).Count)-1, 0)]].Copy();
                if (drop != LootDrop.Secondary) loot.GenerateAttribute();
            }

            if (loot != null) // drop it and notify
            {
                if (map[mapY][mapX].SetItem(loot)) ItemDropped(this, loot);
            }
        }

        public bool CastMagic(int destinationX, int destinationY, IOwner target, out int spellId)
        {
            Magic magic = null;
            spellId = -1;

            for (int i = 0; i < spells.Length; i++)
            {
                magic = world.GetSpellByName(spells[i]); // find spell by name from config - helper method
                if (magic != null && mp >= magic.ManaCost)
                {
                    spellId = magic.Index;
                    break; // find one
                }
            }

            if (magic != null) return true;
            else return false;
        }

        public bool MagicAttack(int hitX, int hitY, IOwner target, Magic spell, MagicTarget magicTarget)
        {
            int damage = 0;

            if (target == null) return false;
            if (target.IsDead) return false;
            if (CurrentLocation.IsSafe || target.CurrentLocation.IsSafe) return false;
            if (!CurrentMap.IsAttackEnabled) return false;
            if (world.IsHeldenianBattlefield && map.IsHeldenianMap && world.Heldenian.IsStarted == false) return false;

            // different effect values are used for the targeted object or for collatoral object in the "area of effect"
            if (magicTarget == MagicTarget.Single) damage = Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3);
            else if (magicTarget == MagicTarget.Area) damage = Dice.Roll(spell.Effect4, spell.Effect5, spell.Effect6);

            MotionDirection flyDirection = Utility.GetFlyDirection(hitX, hitY, target.X, target.Y);

            damage += (int)(((double)damage * (double)(((double)(magic) / 3.3f) / 100.0f)) + 0.5f); // 33% bonus from Magic stat
            if (CurrentMap.IsFightZone) damage = (int)((double)damage * 1.3);// fightzone 30% bonus

            switch (target.OwnerType)
            {
                case OwnerType.Player:
                    Character character = (Character)target;
                    if (character.IsAdmin) return false;
                    
                    // Magic absorption
                    damage -= character.BonusMagicAbsorption;

                    // VIT added absorption
                    damage -= Dice.Roll(1, (character.Vitality / 10) - 1);

                    if (damage == 0) return false;

                    if (character.MagicEffects.ContainsKey(MagicType.Protect) && character.MagicEffects[MagicType.Protect].Magic.Effect1 == 2)
                        damage /= 2;

                    character.TakeDamage(this, DamageType.Magic, damage, flyDirection);

                    break;
                case OwnerType.Npc:
                    Npc npc = (Npc)target;

                    if (npc.IsFriendly) return false;

                    // absorption
                    if (npc.MagicAbsorption > 0)
                        damage = damage - (int)(((double)damage / 100.0f) * (double)npc.PhysicalAbsorption);

                    if (damage == 0) return false;

                    // PFM
                    if (npc.MagicEffects.ContainsKey(MagicType.Protect) && npc.MagicEffects[MagicType.Protect].Magic.Effect1 == 2)
                        damage /= 2;

                    // deal the damage and store any experience received
                    npc.TakeDamage(this, DamageType.Magic, damage, flyDirection);
                    break;
            }

            return false;
        }

        public int GetMagicHitChance(Magic spell)
        {
            int level = 0; // virtual level, determined by size
            switch (size)
            {
                case OwnerSize.Small: level = 60; break;
                case OwnerSize.Large: level = 120; break;
            }

            int magicCircle = (spell.Index / 10) + 1;
            int magicLevel = (level/10);
            int hitChance = 100; // equivalent to 100% skill
            double temp1 = ((double)hitChance / 100.0f);
            double temp2 = ((double)(temp1 * (double)Globals.MagicCastingProbability[magicCircle]));
            hitChance = (int)temp2;
            
            if (magicCircle != magicLevel)
                if (magicCircle > magicLevel)
                {
                    temp1 = (double)((magicCircle - magicLevel) * Globals.MagicCastingLevelPenalty[magicCircle]);
                    temp2 = (double)((magicCircle - magicLevel) * 10);
                    double temp3 = ((double)level / temp2) * temp1;
                    hitChance -= Math.Abs(Math.Abs(magicCircle - magicLevel) * Globals.MagicCastingLevelPenalty[magicCircle] - (int)temp3);
                }
                else hitChance += 5 * Math.Abs(magicCircle - magicLevel);

            switch (CurrentMap.Weather)
            {
                case WeatherType.Rain: hitChance -= (hitChance / 24); break; // 4% reduction
                case WeatherType.RainMedium: hitChance -= (hitChance / 12); break; // 8% reduction
                case WeatherType.RainHeavy: hitChance -= (hitChance / 5); break; // 20% reduction
            }

            int result = Dice.Roll(1, 100);
            if (hitChance < result) return -1;

            if (magic > 50) hitChance += (magic) - 50;
            if (hitChance < 0) hitChance = 1;

            return hitChance;
        }

        public bool PrepareMagic(int sourceX, int sourceY, MotionDirection direction)
        {
            throw new NotImplementedException(); // TODO - HB world hero npcs used this. adds realistic delay
        }

        public bool MeleeAttack(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, IOwner target, int attackType, bool isDash)
        {
            if ((sourceX != this.mapX || sourceY != this.mapY)) return false;
            if (magicEffects.ContainsKey(MagicType.Paralyze)) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);

            this.direction = direction;

            if (target == null) return false;
            if (target.IsDead) return false;
            if (targetX != target.X || targetY != target.Y) return false;
            if (world.IsHeldenianBattlefield && map.IsHeldenianMap && world.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start

            int damage = baseDamage.Roll();
            int hitChance = HitChance;

            damage = damage <= 0 ? 1 : damage;
            hitChance += 50;

            if (target.EvadeMelee(hitChance)) return false;

            switch (target.OwnerType)
            {
                case OwnerType.Player:
                    Character character = (Character)target;
                    if (character.IsAdmin) return false; // cant hurt admin!

                    // vit damage absorption
                    damage -= Dice.Roll(1, (character.Vitality + character.VitalityBonus) / 10) - 1;

                    // bonus PA such as rings and gems etc.
                    if (character.BonusPhysicalAbsorption > 0)
                    {
                        if (character.BonusPhysicalAbsorption >= Globals.MaximumPhysicalAbsorption)
                            damage = damage - (int)(((double)damage / 100.0f) * (double)Globals.MaximumPhysicalAbsorption);
                        else damage = damage - (int)(((double)damage / 100.0f) * (double)character.BonusPhysicalAbsorption);
                    }

                    // roll dice to get chance to hit particular item location
                    EquipType hitLocation;
                    int hitLocationChance = Dice.Roll(1, 10000);
                    if ((hitLocationChance >= 5000) && (hitLocationChance < 7500)) hitLocation = EquipType.Legs; // 25%
                    else if ((hitLocationChance >= 7500) && (hitLocationChance < 9000)) hitLocation = EquipType.Arms; // 15%
                    else if ((hitLocationChance >= 9000) && (hitLocationChance <= 10000)) hitLocation = EquipType.Head; // 10%
                    else hitLocation = EquipType.Body; // 50%

                    switch (hitLocation) // handle item location physical absoprtion
                    {
                        case EquipType.Body:
                            if (character.BodyArmour != null)
                                damage = damage - (int)(((double)damage / 100.0f) * (double)character.BodyArmour.PhysicalAbsorption);
                            break;
                        case EquipType.Legs:
                            if (character.Leggings != null && character.Boots != null) // counts leggings and boots
                            {
                                int absorption = character.Leggings.PhysicalAbsorption + character.Boots.PhysicalAbsorption;
                                if (absorption > Globals.MaximumPhysicalAbsorption) absorption = Globals.MaximumPhysicalAbsorption;
                                damage = damage - (int)(((double)damage / 100.0f) * (double)absorption);
                            }
                            else if (character.Leggings != null) // if no boots equipped then check only leggings
                                damage = damage - (int)(((double)damage / 100.0f) * (double)character.Leggings.PhysicalAbsorption);
                            else if (character.Boots != null) // if no leggings equipped then check only boots
                                damage = damage - (int)(((double)damage / 100.0f) * (double)character.Boots.PhysicalAbsorption);
                            break;
                        case EquipType.Arms:
                            if (character.Hauberk != null)
                                damage = damage - (int)(((double)damage / 100.0f) * (double)character.Hauberk.PhysicalAbsorption);
                            break;
                        case EquipType.Head:
                            if (character.Helmet != null)
                                damage = damage - (int)(((double)damage / 100.0f) * (double)character.Helmet.PhysicalAbsorption);
                            break;
                    }

                    // additional absorption from shield
                    if (character.Shield != null)
                        if (Dice.Roll(1, 100) <= character.Skills[SkillType.Shield].Level)
                        {
                            character.Skills[SkillType.Shield].Experience++;

                            if (character.Shield.PhysicalAbsorption >= Globals.MaximumPhysicalAbsorption)
                                damage = damage - (int)(((double)damage / 100.0f) * (double)Globals.MaximumPhysicalAbsorption);
                            else damage = damage - (int)(((double)damage / 100.0f) * (double)character.Shield.PhysicalAbsorption);

                            character.TakeItemDamage(1, character.Shield, character.ShieldIndex);
                        }

                    if (character.SpecialAbilityEnabled)
                        switch (character.SpecialAbilityItem.SpecialAbilityType)
                        {
                            case ItemSpecialAbilityType.MerienArmour: break; // not applicable to monsters (no weapons to break!)
                            case ItemSpecialAbilityType.MerienShield: return false;
                        }

                    if (damage <= 1) damage = 1;

                    character.TakeDamage(this, DamageType.Melee, damage, direction);
                    return true;
                case OwnerType.Npc:
                    Npc npc = (Npc)target;

                    if (damage == 0) return false;
                    npc.TakeDamage(this, DamageType.Melee, damage, direction);
                    return true;
            }

            return false;
        }

        public bool Idle() { return Idle(mapX, mapY, direction); }
        public bool Idle(int sourceX, int sourceY, MotionDirection direction)
        {
            switch (NpcType)
            {
                case NpcType.CrusadeManaCollector:
                    if (!world.IsCrusade) return false;

                    if (IsBuilt)
                    {
                        actionCount++;
                        if (actionCount >= 3)
                        {
                            for (int y = sourceY - 5; y <= sourceY + 5; y++)
                                for (int x = sourceX - 5; x <= sourceX + 5; x++)
                                    if (map[y][x].IsOccupied)
                                        switch (map[y][x].Owner.OwnerType)
                                        {
                                            case OwnerType.Player:
                                                Character player = (Character)map[y][x].Owner;
                                                player.ReplenishMP(Dice.Roll(1, player.Magic + player.MagicBonus));
                                                break;
                                            case OwnerType.Npc:
                                                Npc npc = (Npc)map[y][x].Owner;
                                                if (npc.NpcType == NpcType.CrusadeManaStone)
                                                {
                                                    if (npc.ActionCount >= 3)
                                                        world.Crusade.CollectedMana[(int)side] += 3;
                                                    else world.Crusade.CollectedMana[(int)side] += npc.ActionCount;
                                                    npc.ActionCount = 0;
                                                }
                                                else npc.ReplenishMP(Dice.Roll(1, npc.Magic));
                                                break;
                                        }
                            actionCount = 0;
                        }
                    }
                    break;
                case NpcType.CrusadeManaStone:
                    if (!world.IsCrusade) return false;

                    actionCount++;
                    if (actionCount > 5) actionCount = 5;
                    break;
                case NpcType.CrusadeEnergyShield: break;
                case NpcType.CrusadeDetector:
                    if (!world.IsCrusade) return false;

                    if (IsBuilt)
                    {
                        actionCount++;
                        if (actionCount >= 3)
                        {
                            for (int y = sourceY - 10; y <= sourceY + 10; y++)
                                for (int x = sourceX - 10; x <= sourceX + 10; x++)
                                    if (map[y][x].IsOccupied && map[y][x].Owner.Side != side)
                                        map[y][x].Owner.RemoveMagicEffect(MagicType.Invisibility);

                            actionCount = 0;
                        }
                    }
                    break;
                case NpcType.CrusadeGrandMagicGenerator:
                    if (!world.IsCrusade) return false;

                    actionCount++;
                    if (actionCount >= 3)
                    {
                        if (world.Crusade.CollectedMana[(int)side] >= 15) // TODO - in config?
                        {
                            world.Crusade.CollectedMana[(int)side] = 0;

                            ReplenishMP(1);
                            MessageLogged(string.Format("{0} Grand Magic Generator mana: {1}", side.ToString(), mp), LogType.Events);
                            if (mp >= world.Crusade.ManaPerStrike)
                                switch (side)
                                {
                                    case OwnerSide.Elvine:
                                        if (world.Crusade.PrepareMeteorStrike(OwnerSide.Aresden))
                                        {
                                                foreach (Map map in world.Maps.Values)
                                                    for (int p = 0; p < map.Players.Count; p++)
                                                        if (world.Players.ContainsKey(map.Players[p]))
                                                        {
                                                            Character player = world.Players[map.Players[p]];
                                                            if (player.CurrentMap.Name.Equals(Globals.ElvineTownName))
                                                                player.Notify(CommandMessageType.NotifyCrusadeStrikeIncoming, 1);
                                                            else player.Notify(CommandMessageType.NotifyCrusadeStrikeIncoming, 2);
                                                        }

                                            DepleteMP(mp);
                                        }
                                        break;
                                    case OwnerSide.Aresden:
                                        if (world.Crusade.PrepareMeteorStrike(OwnerSide.Elvine))
                                        {
                                            foreach (Map map in world.Maps.Values)
                                                for (int p = 0; p < map.Players.Count; p++)
                                                    if (world.Players.ContainsKey(map.Players[p]))
                                                    {
                                                        Character player = world.Players[map.Players[p]];
                                                        if (player.CurrentMap.Name.Equals(Globals.AresdenTownName))
                                                            player.Notify(CommandMessageType.NotifyCrusadeStrikeIncoming, 3);
                                                        else player.Notify(CommandMessageType.NotifyCrusadeStrikeIncoming, 4);
                                                    }

                                            DepleteMP(mp);
                                        }
                                        break;
                                }
                        }
                        actionCount = 0;
                    }
                    break;
                default: return false;
            }

            return true;
        }

        public bool Run(int sourceX, int sourceY, MotionDirection direction)
        {
            throw new NotImplementedException();
        }

        public bool Move()
        {
            // if npc in attack mode
            if (action == MotionType.Attack)
            {
                switch (targetType)
                {
                    case OwnerType.Player:
                        if (!world.Players.ContainsKey(targetID) || map != world.Players[targetID].CurrentMap || world.Players[targetID].IsDead)
                        {
                            ClearTarget();
                            return false;
                        }
                        waypoint = new Location(world.Players[targetID].X, world.Players[targetID].Y); 
                        break;
                    case OwnerType.Npc:
                        if (!world.Npcs.ContainsKey(targetID) || map != world.Npcs[targetID].CurrentMap || world.Npcs[targetID].IsDead)
                        {
                            ClearTarget();
                            return false;
                        }
                        waypoint = new Location(world.Npcs[targetID].X, world.Npcs[targetID].Y); 
                        break;
                }
            }// if no waypoint, waypoint reached or action keeps failing, then get a new waypoint
            else if (waypoint == null || (waypoint.X == mapX && waypoint.Y == mapY) || (failedActionCount > 2))
                switch (moveType)
                {
                    case MovementType.None: return false; // static npcs such as shopkeeper
                    case MovementType.RandomArea:
                        if (isFriendly)
                        { if (IsSpawned && map.NpcSpawns[spawnId] != null) waypoint = map.NpcSpawns[spawnId].Zone.GetRandomLocation(); }
                        else
                        {
                            if (IsSpawned && map.MobSpawns[spawnId] != null) waypoint = map.MobSpawns[spawnId].Zone.GetRandomLocation();
                            else moveType = MovementType.Random; // if not spawned, set it to random
                        }
                        break;
                    case MovementType.Random:
                        waypoint = new Location(Dice.Roll(1, map.Width), Dice.Roll(1, map.Height));
                        break;
                    case MovementType.Guard:
                        break;
                    case MovementType.Follow:
                        if (IsSummoned && CurrentMap.Players.Contains(summoner))
                            waypoint = new Location(world.Players[summoner].X, world.Players[summoner].Y);
                        else moveType = MovementType.Random;
                        break;
                }

            // update waypoint if summoner has changed position
            if (action != MotionType.Attack && moveType == MovementType.Follow && IsSummoned && CurrentMap.Players.Contains(summoner))
            {
                if (waypoint.X != world.Players[summoner].X && waypoint.Y != world.Players[summoner].Y)
                    waypoint = new Location(world.Players[summoner].X, world.Players[summoner].Y);

                if (world.Players[summoner].IsWithinRange(this, 2, 2)) { return false; }
            }

            if (waypoint == null) { failedActionCount++; return false; }

            MotionDirection direction = map.GetNextDirection(mapX, mapY, waypoint.X, waypoint.Y, turn);

            bool ret = Move(mapX + Globals.MoveDirectionX[(int)direction], mapY + Globals.MoveDirectionY[(int)direction], direction);

            if (!ret)
            {
                failedActionCount++;
                return false;
            }
            else
            {
                failedActionCount = 0;
                return true;
            }
        }

        public bool Move(int destinationX, int destinationY, MotionDirection direction)
        {
            if (isDead) return false;
            if (!map[destinationY][destinationX].IsMoveable) return false;
            if (magicEffects.ContainsKey(MagicType.Paralyze)) return false;
            if (destinationX < 0 || destinationX > map.Width || destinationY < 0 || destinationY > map.Height) return false; // map boundaries

            if (!map[destinationY][destinationX].IsBlocked)
            {
                map[mapY][mapX].ClearOwner();
                this.direction = direction;
                mapX = destinationX;
                mapY = destinationY;
                map[destinationY][destinationX].SetOwner(this);

                return true;
            }
            else return false;
        }

        /// <summary>
        /// Rolls a dice to see if this Npc successfully evades the incoming attack or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Npc has successfully evaded or not.</returns>
        public bool EvadeMelee(int attackerHitChance)
        {
            int evadeChance = dexterity * 2;
            if (magicEffects.ContainsKey(MagicType.Protect))
                switch (magicEffects[MagicType.Protect].Magic.Effect1)
                {
                    case 3: evadeChance += 40; break; // defence shield
                    case 4: evadeChance += 100; break; // greater defence shield
                }

            int hitChance = (int)(((double)attackerHitChance / (double)evadeChance) * 50.0f);

            if (hitChance < Globals.MinimumHitChance) hitChance = Globals.MinimumHitChance;
            if (hitChance > Globals.MaximumHitChance) hitChance = Globals.MaximumHitChance;

            int result = Dice.Roll(1, 100);

            if (result <= hitChance) return false;
            else return true;
        }

        /// <summary>
        /// Rolls a dice to see if this Npc successfully evades a magic attack or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <param name="ignorePFM">Does this spell ignore PFM?</param>
        /// <returns>True or False to indicate that this Npc has successfully evaded or not.</returns>
        public bool EvadeMagic(int attackerHitChance, bool ignorePFM)
        {
            if (attackerHitChance == -1) return true; 

            int evadeChance = 0; // was magicresist in old configs. now calculated by their magic value
            if (magic > 50) evadeChance += (magic) - 50;

            if (magicEffects.ContainsKey(MagicType.Protect))
                if (magicEffects[MagicType.Protect].Magic.Effect1 == 5) return true; // AMP
                else if (MagicEffects[MagicType.Protect].Magic.Effect1 == 2 && !ignorePFM) return true; // PFM

            if (evadeChance < 1) evadeChance = 1;

            double temp1 = ((double)attackerHitChance / (double)evadeChance);
            double temp2 = ((double)(temp1 * 50.0f));
            int hitChance = (int)temp2;

            if (hitChance < Globals.MinimumHitChance) hitChance = Globals.MinimumHitChance;
            if (hitChance > Globals.MaximumHitChance) hitChance = Globals.MaximumHitChance;

            int result = Dice.Roll(1, 100);

            if (result <= hitChance) return false;
            else return true;
        }

        /// <summary>
        /// Rolls a dice to see if this Npc successfully evades the being frozen or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Npc has successfully evaded being frozen or not.</returns>
        public bool EvadeIce()
        {
            if (absorptionType == MagicAttribute.Water) return true; // ice/water monsters cant be frozen

            int evadeChance = 66; // TODO ice resistance previously determined by magicresistance - (magicresistance /3) (around 66% max chance)
            if (evadeChance < 1) evadeChance = 1;
            if (evadeChance > Globals.MaximumFreezeProtection) evadeChance = Globals.MaximumFreezeProtection;

            int result = Dice.Roll(1, 100);

            if (result <= evadeChance) return true;
            else return false;
        }

        /// <summary>
        /// Rolls a dice to see if this Npc successfully evades the being poisoned or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Npc has successfully evaded being poisoned or not.</returns>
        public bool EvadePoison()
        {
            int evadeChance = 10; // static according to old sources
            int result = Dice.Roll(1, 100);

            if (result <= evadeChance) return true;
            else return false;
        }

        public void ClearTarget()
        {
            if (moveType == MovementType.None)
                 action = MotionType.AttackStationary;
            else action = MotionType.Move;
        }

        public void SearchForTarget()
        {
            bool targetFound = false;
            int distance;

            for (int ix = mapX - searchRange; ix < mapX + searchRange * 2 + 1; ix++)
                for (int iy = mapY - searchRange; iy < mapY + searchRange * 2 + 1; iy++)
                {
                    if (targetFound) break;
                    if (!(iy < 0 || iy > map.Height || ix < 0 || ix > map.Width) && (map[iy][ix].IsOccupied))
                    {
                        IOwner owner = map[iy][ix].Owner;
                        if (owner.Side == side && ((owner.OwnerType == OwnerType.Player && !((Character)owner).IsCriminal) || owner.OwnerType == OwnerType.Npc)) break;
                        if (owner.OwnerType == OwnerType.Player && ((Character)owner).IsAdmin) break;
                        if (type == 21 && owner.Side == OwnerSide.Neutral) break; // special case for guards
                        if (owner.OwnerType == OwnerType.Npc && owner.Side == OwnerSide.Neutral) break; // doesnt attack neutral npcs
                        if (owner.IsInvisible) break;

                        if ((mapX - owner.X) >= (mapY - owner.Y))
                            distance = mapX - owner.X;
                        else distance = mapY - owner.Y;

                        if (distance < 100)
                        {
                            targetID = owner.ID;
                            targetType = owner.OwnerType;
                            targetFound = true;
                            break;
                        }
                    }
                }

            if (targetFound) action = MotionType.Attack;
        }

        public bool IsWithinRange(IOwner other, int rangeX, int rangeY) { return IsWithinRange(other, rangeX, rangeY, 0); }
        public bool IsWithinRange(IOwner other, int rangeX, int rangeY, int modifier)
        {
            if ((map == CurrentMap) &&
                (mapX >= other.X - rangeX - modifier) &&
                (mapX <= other.X + rangeX + modifier) &&
                (mapY >= other.Y - rangeY - modifier) &&
                (mapY <= other.Y + rangeY + modifier))
                return true;
            else return false;
        }

        public bool IsWithinRange(IDynamicObject other, int rangeX, int rangeY, int modifier)
        {
            if ((map == CurrentMap) &&
                (mapX >= other.X - rangeX - modifier) &&
                (mapX <= other.X + rangeX + modifier) &&
                (mapY >= other.Y - rangeY - modifier) &&
                (mapY <= other.Y + rangeY + modifier))
                return true;
            else return false;
        }

        /// <summary>
        /// Removes this NPC from the map
        /// </summary>
        public bool Remove()
        {
            if (!IsSummoned && canDropLoot) DropLoot(LootDrop.Secondary); // body parts, rares etc
            if (isDead) map[mapY][mapX].ClearDeadOwner();
            else map[mapY][mapX].ClearOwner();
            map.Npcs.Remove(id);

            return true;
        }

        public byte[] GetFullObjectData(Character requester)
        {
            byte[] data = new byte[22];

            Buffer.BlockCopy((id+10000).GetBytes(), 0, data, 0, 2);
            Buffer.BlockCopy(mapX.GetBytes(), 0, data, 2, 2);
            Buffer.BlockCopy(mapY.GetBytes(), 0, data, 4, 2);
            Buffer.BlockCopy(type.GetBytes(), 0, data, 6, 2);
            data[8] = (byte)((int)direction);
            Buffer.BlockCopy(name.GetBytes(5), 0, data, 9, 5);
            Buffer.BlockCopy(appearance2.GetBytes(), 0, data, 14, 2);
            int status = ((0x0FFFFFFF & this.status) | ((requester.GetNpcRelationship(this)) << 28));
            Buffer.BlockCopy((status).GetBytes(), 0, data, 16, 4);
            if (isDead)
                data[20] = 1;
            else data[20] = 0;

            return data;
        }

        public int ID
        {
            set { id = value; }
            get { return id; }
        }

        public int SpawnID
        {
            set { spawnId = value; }
            get { return spawnId; }
        }

        public string Name
        {
            set { name = value; }
            get { return name; }
        }
        public string FriendlyName { get { return friendlyName; } set { friendlyName = value; } }

        public MovementType MoveType
        {
            get { return moveType; }
            set {  moveType = value; }
        }

        public Guild Guild { get { return guild; } set { guild = value; } }
        public int Type { set { type = value; } get { return type; } }
        public NpcType NpcType { get { if (Enum.IsDefined(typeof(NpcType), type)) return (NpcType)type; else return NpcType.None; } }
        public OwnerType OwnerType { get { return OwnerType.Npc; } }
        public int Strength { get { return strength; } set { strength = value; } }
        public int Dexterity { set { dexterity = value; } get { return dexterity; } }
        public int Vitality { set { vitality = value; } get { return vitality; } }
        public int Magic { set { magic = value; } get { return magic; } }
        public string[] Spells { set { spells = value; } get { return spells; } }
        public int AttackRange { set { attackRange = value; } get { return attackRange; } }
        public int SearchRange { set { searchRange = value; } get { return searchRange; } }
        public OwnerSize Size { set { size = value; } get { return size; } }
        public OwnerSide Side { set { side = value; } get { return side; } }
        public MotionDirection Direction { get { return direction; } set { direction = value; } }
        public World CurrentWorld { set { world = value; } get { return world; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public int Status { get { return status; } set { status = value; } }
        public bool IsDead { get { return isDead; } }
        public DateTime DeadTime { get { return deadTime; } }

        public int Appearance2
        {
            set // changable with crusade structures/crops etc
            {
                appearance2 = value;
                if (StatusChanged != null) StatusChanged(this);
            }
            get { return appearance2; }
        }

        public int MagicAbsorption
        {
            get
            {
                if (ma > Globals.MaximumMagicalAbsorption)
                    return Globals.MaximumMagicalAbsorption;
                else return ma;
            }
            set { ma = value; }
        }

        public int PhysicalAbsorption
        {
            get {
                if (pa > Globals.MaximumPhysicalAbsorption)
                     return Globals.MaximumPhysicalAbsorption;
                else return pa; 
                }
            set { pa = value; }
        }

        public int HP
        {
            set { hp = value; }
            get { return hp; }
        }

        public int BuildType { set { buildType = value; } get { return buildType; } }
        public int BuildPoints { set { buildPoints = value; } get { return buildPoints; } }
        public int BuildLimit { set { buildLimit = value; } get { return buildLimit; } }

        /// <summary>
        /// Calculates the maximum hit points this Npc can have.
        /// </summary>
        public int MaxHP
        {
            get { return (vitality * 3) + (strength / 2); }
        }

        public int MP
        {
            set { mp = value; }
            get { return mp; }
        }

        /// <summary>
        /// Calculates the maximum magic points this Npc can have.
        /// </summary>
        public int MaxMP
        {
            get { return (magic * 3); }
        }

        public MotionType CurrentAction
        {
            get { return action; }
            set { action = value; }
        }

        public bool IsSpawned
        {
            get { return (spawnId != -1); }
        }

        public List<int> Summons { get { return summons; } set { summons = value; } }
        public bool IsSummoned { get { return (summoner != -1); } }
        public int Summoner
        {
            get { return summoner; }
            set { 
                  summoner = value;
                  summonTime = DateTime.Now;
                }
        }

        public Dice BaseDamage
        {
            set { baseDamage = value; }
            get { return baseDamage; }
        }

        public TimeSpan MaximumDeadTime
        {
            get { return maximumDeadTime; }
            set { maximumDeadTime = value; }
        }

        public Dice Experience
        {
            get { return experience; }
            set { experience = value; }
        }

        public int RemainingExperience
        {
            get { return remainingExperience; }
            set { remainingExperience = value; }
        }

        /// <summary>
        /// Chance for this Npc to hit the target. Based on dexterity * 2 + an additional 30%.
        /// </summary>
        public int HitChance
        {
            get { return (dexterity * 2)+(int)((double)(dexterity*2) *0.3); }
        }

        public Dictionary<MagicType, MagicEffect> MagicEffects
        {
            get { return magicEffects; }
        }

        public TimeSpan ActionTime
        {
            get { return actionTime; }
            set { actionTime = value; }
        }

        public DateTime LastActionTime
        {
            get { return lastActionTime; }
            set { lastActionTime = value; }
        }

        public DateTime SummonTime { get { return summonTime; } set { summonTime = value; } }
        public bool IsFriendly { get { return isFriendly; } set { isFriendly = value; } }
        public bool IsPacifist { get { return isPacifist; } set { isPacifist = value; } }
        public int TargetID { get { return targetID; } set { targetID = value; } }
        public OwnerType TargetType { get { return targetType; } set { targetType = value; } }
        public bool IsInvisible { get { return magicEffects.ContainsKey(MagicType.Invisibility); } }
        public bool IsBuilt { get { return buildPoints == 0; } }
        public int FailedActionCount { get { return failedActionCount; } set { failedActionCount = value; } }
        public int ActionCount { get { return actionCount; } set { actionCount = value; } }

        public int MaximumGold
        {
            get { return maxGold; }
            set { maxGold = value; }
        }
    }
}
