﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HelbreathWorld;
using HelbreathWorld.Common.Events;
using HelbreathWorld.Common.Assets.Objects.Dynamic;
using HelbreathWorld.Common.Assets.Quests;

namespace HelbreathWorld.Common.Assets.Objects
{
    public class Character : IOwner, IDeadOwner
    {
        public event GuildHandler GuildCreated;
        public event GuildHandler GuildDisbanded;

        public event OwnerHandler StatusChanged;
        public event MotionHandler MotionChanged;
        public event DamageHandler DamageTaken;
        public event DeathHandler Killed;
        public event ItemHandler ItemDropped;
        public event ItemHandler ItemPickedUp;
        public event LogHandler MessageLogged;

        private Dictionary<MagicType, MagicEffect> magicEffects;
        private List<Item> inventory;       // bag items
        private List<Item> warehouse;       // wh items
        private bool[] spellStatus;         // determines if a spell has been learned or not
        private int[] equipment;            // item indexes of items equipped in each slot (head, arms, body etc)

        private List<Quest> activeQuests;
        private List<int> summons;
        private Dictionary<SkillType, Skill> skills;
        private SkillType skillInUse;
        private DateTime skillTime;
        private int skillTargetX;
        private int skillTargetY;
        private Fish targetFish;
        private int targetFishChance;

        private int id;
        private string accountName;
        private ClientInfo clientInfo;
        private string databaseID; // database id
        private string name;
        private string profile;
        private OwnerSide town;
        private int adminLevel;
        private bool isSafeMode;
        private bool isCivilian;
        private TimeSpan muteTime;
        private int whisperID;

        private int enemyKills;
        private int criminalCount;
        private int rewardGold; // from quests etc

        private Guild guild;
        private int guildRank;
        private string loadedGuildName;

        private Party party;
        private int partyRequestID;

        private int hunger;
        private int level;
        private int strength, strengthBonus;
        private int dexterity, dexterityBonus;
        private int vitality, vitalityBonus;
        private int magic, magicBonus;
        private int intelligence, intelligenceBonus;
        private int charisma, charismaBonus;
        private long experience, experienceStored;
        private int contribution;
        private int reputation;
        private int hp, hpStock;
        private int mp;
        private int sp;
        private int criticals;
        private int criticalCharge;
        private int majestics;
        private int luck;              // percentage. used for chance of survival when at 0hp
        private int totalLogins;

        private int type;
        private GenderType gender;
        private SkinType skin;
        private int hairStyle;
        private int hairColour;
        private int underwearColour;

        private int appearance1;
        private int appearance2;
        private int appearance3;
        private int appearance4;
        private int appearanceColour;
        private MotionDirection direction;
        private bool isDead;
        private bool canFly;
        private bool showDamageEnabled;
        private int lastDamage;

        private int status;
        private object statusLock = new object(); // thread sync

        private int prevMapX;
        private int prevMapY;
        private int mapX;
        private int mapY;
        private World world;
        private Map map;
        private string loadedMapName; // loaded from database to find map reference

        private DateTime lastLogin;

        private DateTime statusTime; // magic effects etc
        private DateTime hungerTime; // hunger degen
        private DateTime hpUpTime;   // hp regen
        private DateTime mpUpTime;   // mp regen
        private DateTime spUpTime;   // sp regen
        private DateTime poisonTime; // poison time
        private DateTime experienceUpTime; // experience store purge
        private TimeSpan reputationTime; // time before can rep+ or rep- players

        private int specialAbilityItemIndex;
        private bool specialAbilityEnabled;
        private TimeSpan specialAbilityTime; // time before can activate
        private DateTime specialAbilityStartTime;

        // item bonuses
        private int bonusPhysicalDamage, bonusMagicDamage;
        private int bonusPhysicalAbsorption, bonusMagicAbsorption;
        private int bonusAirAbsorption, bonusEarthAbsorption, bonusFireAbsorption, bonusWaterAbsorption;
        private int bonusHitChance, bonusMagicResistance, bonusPoisonResistance, bonusPhysicalResistance;
        private int bonusManaSave;
        private int bonusHPRecovery, bonusSPRecovery, bonusMPRecovery;

        //world events
        private CrusadeDuty crusadeDuty;
        private int crusadeConstructionPoints;
        private int crusadeWarContribution;

        public Character()
        {
            inventory = new List<Item>(Globals.MaximumInventoryItems);
            warehouse = new List<Item>(Globals.MaximumWarehouseItems);
            magicEffects = new Dictionary<MagicType, MagicEffect>();
            summons = new List<int>(Globals.MaximumSummons);
            skills = new Dictionary<SkillType, Skill>(Globals.MaximumSkills);
            activeQuests = new List<Quest>();

            for (int i = 0; i < Globals.MaximumInventoryItems; i++) inventory.Add(null);
            for (int i = 0; i < Globals.MaximumWarehouseItems; i++) warehouse.Add(null);

            equipment = new int[15] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
            spellStatus = new bool[Globals.MaximumSpells];

            this.direction = MotionDirection.South;
            this.isDead = false;
            this.canFly = true;
            this.specialAbilityItemIndex = -1;
            this.whisperID = -1;
            this.skillInUse = SkillType.None;
            this.partyRequestID = -1;
        }

        /// <summary>
        /// Initilizes this Character on the map.
        /// </summary>
        public void Init() { Init(CurrentMap, mapX, mapY); }
        /// <summary>
        /// Initilizes this Character on the map.
        /// </summary>
        public void Init(Map targetMap, int targetX, int targetY)
        {
            if (hp <= 0) // handle death before log in (due to disconnect or log out while dead). similar to Restart()
            {
                hp = MaxHP;
                mp = MaxMP;
                sp = MaxSP;
                hunger = 100;
                targetX = targetY = -1; // default locations set in revival zone configs

                switch (town)
                {
                    case OwnerSide.Neutral: targetMap = world.Maps[Globals.TravellerRevivalZone]; break;
                    case OwnerSide.Aresden: targetMap = world.Maps[Globals.AresdenRevivalZone]; break;
                    case OwnerSide.Elvine: targetMap = world.Maps[Globals.ElvineRevivalZone]; break;
                }
            }

            if (targetX == -1 || targetY == -1) // handle default locations -1,-1
            {
                if (targetMap.DefaultLocations.Count > 0)
                {
                    Location location;
                    if (targetMap.DefaultLocations.Count == 1) location = targetMap.DefaultLocations[0];
                    else location = targetMap.DefaultLocations[Math.Max(Dice.Roll(1, targetMap.DefaultLocations.Count) - 1, 0)];
                    targetX = location.X;
                    targetY = location.Y;
                }
                else
                {
                    // send to BI if location cannot be determined. hardcoded failsafe
                    targetMap = world.Maps[Globals.BleedingIsleName];
                    targetX = 144;
                    targetY = 117;
                }
            }

            int x, y;
            if (targetMap.GetEmptyTile(targetX, targetY, out x, out y))
            {
                this.mapX = x;
                this.mapY = y;
                targetMap[y][x].SetOwner(this);
                targetMap.Players.Add(id);
            }

            status = ((0x0FFFFFFF & status) | (GetPlayerRelationship(null) << 28));

            Notify(CommandMessageType.NotifyCriticals, criticals);
            Notify(CommandMessageType.NotifyMajestics, majestics);

            if (specialAbilityTime.TotalSeconds <= 0) Notify(CommandMessageType.NotifySpecialAbilityEnabled); //specialAbilityTime = new TimeSpan(0, 0, 10);
            if (world.IsCrusade) Notify(CommandMessageType.NotifyCrusade, 1, 0, 0);
            if (world.IsHeldenianBattlefield)
            {
                if (world.Heldenian.IsStarted) Notify(CommandMessageType.NotifyHeldenianStarted);
                else Notify(CommandMessageType.NotifyHeldenian);
                if (map.Name.Equals(Globals.HeldenianBattleFieldName))
                    Notify(CommandMessageType.NotifyHeldenianStatistics, world.Heldenian.AresdenStructures, world.Heldenian.ElvineStructures, world.Heldenian.AresdenCasualties, world.Heldenian.ElvineCasualties);
            }
        }

        public bool Load(DataRow data)
        {
            databaseID = data["ID"].ToString();
            name = data["Name"].ToString();
            profile = data["Profile"].ToString();
            gender = (GenderType)((int)data["Gender"]);
            skin = (SkinType)((int)data["Skin"]);
            hairStyle = (int)data["HairStyle"];
            hairColour = (int)data["HairColour"];
            underwearColour = (int)data["UnderwearColour"];
            strength = (int)data["Strength"];
            vitality = (int)data["Vitality"];
            dexterity = (int)data["Dexterity"];
            intelligence = (int)data["Intelligence"];
            magic = (int)data["Magic"];
            charisma = (int)data["Charisma"];
            hp = (int)data["HP"];
            mp = (int)data["MP"];
            sp = (int)data["SP"];
            level = (int)data["Level"];
            experience = (int)data["Experience"];
            appearance1 = (int)data["Appearance1"];
            appearance2 = (int)data["Appearance2"];
            appearance3 = (int)data["Appearance3"];
            appearance4 = (int)data["Appearance4"];
            appearanceColour = (int)data["AppearanceColour"];
            loadedMapName = data["MapName"].ToString();
            mapX = (int)data["MapX"];
            mapY = (int)data["MapY"];
            lastLogin = (DateTime)data["LastLogin"];
            town = (OwnerSide)((int)data["Town"]);
            hunger = (int)data["Hunger"];
            criticals = (int)data["Criticals"];
            majestics = (int)data["Majestics"];
            specialAbilityTime = new TimeSpan(0, 0, (int)data["SpecialAbilityTime"]);
            muteTime = new TimeSpan(0, 0, (int)data["MuteTime"]);
            adminLevel = (int)data["AdminLevel"];
            loadedGuildName = data["Guild"].ToString();
            guildRank = (int)data["GuildRank"];
            totalLogins = (int)data["TotalLogins"];
            reputation = (int)data["Reputation"];
            contribution = (int)data["Contribution"];
            reputationTime = new TimeSpan(0, 0, (int)data["ReputationTime"]);
            enemyKills = (int)data["EnemyKills"];
            criminalCount = (int)data["CriminalCount"];

            for (int i = 0; i < Globals.MaximumSpells; i++)
                if (data["SpellStatus"].ToString()[i] == '1')
                    spellStatus[i] = true;
                else spellStatus[i] = false;


            string[] skillList = data["SkillStatus"].ToString().Split();
            for (int i = 0; i < Globals.MaximumSkills; i++)
                if (Enum.IsDefined(typeof(SkillType), i)) // check this skill is defined
                    if (!skills.ContainsKey((SkillType)i)) // check not already added
                    {
                        Skill s = new Skill((SkillType)i, Int32.Parse(skillList[i]));
                        s.LevelUp += new SkillHandler(OnSkillLevelUp);
                        skills.Add((SkillType)i, s);
                    }

            switch (gender)
            {
                case GenderType.Male: type = 1; break;
                case GenderType.Female: type = 4; break;
            }
            type += ((int)skin) - 1;

            statusTime = hungerTime = hpUpTime = mpUpTime = spUpTime = DateTime.Now;

            // cant have more than max
            if (hp > MaxHP) hp = MaxHP;
            if (mp > MaxMP) mp = MaxMP;
            if (sp > MaxSP) sp = MaxSP;
            if (criticals > MaxCriticals) criticals = MaxCriticals;

            return true;
        }

        public void Talk(string message)
        {
            // handle muted characters
            if (IsMuted)
            {
                world.SendClientMessage(this, "You are muted for " + MuteTime.Hours + "h " + MuteTime.Minutes + "m and " + MuteTime.Seconds + "s");
                return;
            }

            ChatType mode = ChatType.Local;
            if (IsWhispering) mode = ChatType.Whisper;

            switch (message[0])
            {
                case '#': mode = ChatType.Local; break; // overrides whisper
                case '$': mode = ChatType.Party; message = message.TrimStart('$'); break;
                case '@': mode = ChatType.Guild; message = message.TrimStart('@'); break;
                case '!': mode = (IsAdmin) ? ChatType.GameMaster : mode = ChatType.Global; message = message.TrimStart('!'); break;
                case '^': mode = ChatType.Guild; message = message.TrimStart('^'); break;
                case '~': mode = ChatType.Town; message = message.TrimStart('~'); break;
                case '/': mode = ChatType.Command; message = message.TrimStart('/');

                    string[] tokens = message.Split(' ');

                    Character c;
                    switch (tokens[0].ToLower())
                    {
                        case "testai": world.CreateAdvancedNpc(CurrentMap, X + 1, Y); break;
                        case "startapocalypse": if (IsAdmin) world.StartWorldEvent(WorldEventType.Apocalypse); break;
                        case "startcrusade": if (IsAdmin) world.StartWorldEvent(WorldEventType.Crusade); break;
                        case "startheldenian": if (IsAdmin) world.StartWorldEvent(WorldEventType.Heldenian); break;
                        case "startctf": if (IsAdmin) world.StartWorldEvent(WorldEventType.CaptureTheFlag); break;
                        case "endapocalypse":
                        case "endcrusade":
                        case "endheldenian":
                        case "endctf": if (IsAdmin) world.EndWorldEvent(OwnerSide.Neutral); break; // manual end - draw
                        case "setattackmode":
                            if (IsAdmin)
                                if (tokens.Length > 1)
                                {
                                    bool boolParser;
                                    if (Boolean.TryParse(tokens[1], out boolParser)) CurrentMap.IsAttackEnabled = boolParser;
                                     MessageLogged(Name + " - /setattackmode succeeded. " + CurrentMap.Name + " attack enabled: " + boolParser.ToString(), LogType.Admin);
                                }
                            break;
                        case "summon":
                            if (tokens.Length > 1)
                            {
                                string npcName = tokens[1];
                                int count = 1;
                                if (tokens.Length > 2) Int32.TryParse(tokens[2], out count);
                                if (count <= 0) count = 1;
                                if (count > 20) count = 20;

                                for (int i = 0; i < count; i++)
                                {
                                    if (!world.CreateNpc(npcName, CurrentMap, X + i, Y)){}
                                         MessageLogged(Name + " - /summon command failed. " + npcName + " does not exist.", LogType.Admin);
                                    //else  MessageLogged(owner.Name + " - /summon " + npcName + " succeeded.", LogType.Admin);
                                }
                            }
                            break;
                        case "createparty": CreateParty(); break;
                        case "createitem":
                            if (tokens.Length > 1)
                            {
                                string itemName = tokens[1];
                                uint attribute = 0;

                                if (World.ItemConfiguration.ContainsKey(itemName))
                                {
                                    int count = 1;
                                    if (tokens.Length > 2)
                                        if (World.ItemConfiguration[itemName].Type == ItemType.Arrow ||
                                            World.ItemConfiguration[itemName].Type == ItemType.Consume)
                                            Int32.TryParse(tokens[2], out count);
                                        else
                                        {
                                            if (tokens.Length > 5)
                                            {
                                                ItemSpecialWeaponPrimaryType pType; ItemSpecialWeaponSecondaryType sType;
                                                int pValue, sValue;
                                                if (Enum.TryParse<ItemSpecialWeaponPrimaryType>(tokens[2], out pType) &&
                                                    Enum.TryParse<ItemSpecialWeaponSecondaryType>(tokens[4], out sType) &&
                                                    Int32.TryParse(tokens[3], out pValue) &&
                                                    Int32.TryParse(tokens[5], out sValue))
                                                    attribute = Item.CalculateAttribute(pType, pValue, sType, sValue);
                                            }
                                            else UInt32.TryParse(tokens[2], out attribute);
                                        }
                                    // if consumable or arrow, second variable is count else second variable is attribute

                                    Item item = World.ItemConfiguration[itemName].Copy(count);
                                    item.Attribute = attribute;

                                    AddInventoryItem(item);
                                     MessageLogged(Name + " - /createitem " + itemName + " succeeded.", LogType.Admin);
                                }
                                 else MessageLogged(Name + " - /item command failed. " + itemName + " does not exist.", LogType.Admin);
                            }
                            break;
                        case "tp":
                        case "teleport":
                            if (tokens.Length > 1)
                                if (tokens.Length > 3 && world.Maps.ContainsKey(tokens[1]))
                                {
                                    Teleport(world.Maps[tokens[1]], Int32.Parse(tokens[2]), Int32.Parse(tokens[3]));
                                     MessageLogged(Name + " - /teleport " + tokens[1] + " (" + tokens[2] + "," + tokens[3] + ") succeeded.", LogType.Admin);
                                }
                                else if (world.Maps.ContainsKey(tokens[1]))
                                {
                                    Teleport(world.Maps[tokens[1]], -1, -1);
                                     MessageLogged(Name + " - /teleport " + tokens[1] + " (default location (-1,-1)) succeeded.", LogType.Admin);
                                }
                            break;
                        case "goto":
                            if (tokens.Length > 1)
                                if (world.FindPlayer(tokens[1].Trim(), out c))
                                {
                                    Teleport(c.CurrentMap, c.X, c.Y, MotionDirection.South);
                                     MessageLogged(Name + " - /goto " + tokens[1] + " (" + c.CurrentMap.Name + ": " + c.X + "," + c.Y + ") succeeded.", LogType.Admin);
                                }
                                else
                                {
                                    Notify(CommandMessageType.NotifyPlayerNotOnline, tokens[1]);
                                     MessageLogged(Name + " - /goto " + tokens[1] + " failed. Player not online.", LogType.Admin);
                                }
                            break;
                        case "summonplayer":
                        case "sp:":
                            if (tokens.Length > 1)
                                if (world.FindPlayer(tokens[1].Trim(), out c))
                                {
                                    c.Teleport(CurrentMap, X, Y, MotionDirection.South);
                                     MessageLogged(Name + " - /summonplayer " + tokens[1] + " (" + CurrentMap.Name + ": " + X + "," + Y + ") succeeded.", LogType.Admin);
                                }
                                else
                                {
                                    Notify(CommandMessageType.NotifyPlayerNotOnline, tokens[1]);
                                     MessageLogged(Name + " - /summonplayer " + tokens[1] + " failed. Player not online.", LogType.Admin);
                                }
                            break;
                        case "who":
                            int online = 0;
                            foreach (Map map in world.Maps.Values) online += map.Players.Count;

                            world.SendClientMessage(this, string.Format("Players Online: {0}", online));
                            break;
                        case "setberserk":
                            world.CastMagic(this, X, Y, World.MagicConfiguration[50]);
                            world.SendMagicEventToNearbyPlayers(this, CommandMessageType.CastMagic, X, Y, 50 + 100);
                            break;
                        case "rep+":
                            if (ReputationTime.Seconds > 0 || IsCriminal)
                                Notify(CommandMessageType.NotifyReputationFailed, ReputationTime.Seconds);
                            else if (Town == OwnerSide.Neutral)
                                Notify(CommandMessageType.NotifyReputationFailed, 0);
                            else if (tokens.Length > 1)
                                if (world.FindPlayer(tokens[1].Trim(), out c))
                                {
                                    c.Reputation = Math.Max(c.Reputation + 1, Globals.MaximumReputation);
                                    c.Notify(CommandMessageType.NotifyReputationSuccess, 1, c.Reputation, c.Name);
                                    ReputationTime = new TimeSpan(0, 0, 1200);
                                    Notify(CommandMessageType.NotifyReputationSuccess, 1, Reputation, c.Name);
                                }
                                else Notify(CommandMessageType.NotifyPlayerNotOnline, tokens[1]);
                            break;
                        case "rep-":
                            if (ReputationTime.Seconds > 0 || IsCriminal)
                                Notify(CommandMessageType.NotifyReputationFailed, ReputationTime.Seconds);
                            else if (Town == OwnerSide.Neutral)
                                Notify(CommandMessageType.NotifyReputationFailed, 0);
                            else if (tokens.Length > 1)
                                if (world.FindPlayer(tokens[1].Trim(), out c))
                                {
                                    c.Reputation = Math.Min(c.Reputation - 1, Globals.MinimumReputation);
                                    c.Notify(CommandMessageType.NotifyReputationSuccess, 0, c.Reputation, c.Name);
                                    ReputationTime = new TimeSpan(0, 0, 1200);
                                    Notify(CommandMessageType.NotifyReputationSuccess, 0, Reputation, c.Name);
                                }
                                else Notify(CommandMessageType.NotifyPlayerNotOnline, tokens[1]);
                            break;
                        case "getrep":
                            world.SendClientMessage(this, string.Format("Reputation: {0}", Reputation));
                            break;
                        case "setrep":
                            int rep;
                            if (tokens.Length > 1 && Int32.TryParse(tokens[1], out rep))
                                Reputation = Math.Max(Math.Min(rep, Globals.MaximumReputation), Globals.MinimumReputation);

                            world.SendClientMessage(this, string.Format("Reputation: {0}", Reputation));
                            break;
                        case "showdmg":
                            ShowDamageEnabled = !ShowDamageEnabled;
                            world.SendClientMessage(this, string.Format("Show Damage {0}.", (showDamageEnabled) ? "ON" : "OFF"));
                            break;
                        case "tgt":
                            if (tokens.Length > 1 && Summons.Count > 0)
                                if (world.FindPlayer(tokens[1].Trim(), out c))
                                    for (int s = 0; s < CurrentMap.Npcs.Count; s++)
                                        if (world.Npcs.ContainsKey(CurrentMap.Npcs[s]))
                                            if (world.Npcs[CurrentMap.Npcs[s]].Summoner == id)
                                            {
                                                world.Npcs[CurrentMap.Npcs[s]].TargetID = c.ID;
                                                world.Npcs[CurrentMap.Npcs[s]].TargetType = OwnerType.Player;
                                                world.Npcs[CurrentMap.Npcs[s]].CurrentAction = MotionType.Attack;
                                            }
                            break;
                        case "regen":
                            ReplenishHP(MaxHP);
                            ReplenishMP(MaxMP);
                            ReplenishSP(MaxSP);
                            EatFood(100);

                            Notify(CommandMessageType.NotifyHP, HP, MP);
                            Notify(CommandMessageType.NotifyMP, MP);
                            Notify(CommandMessageType.NotifySP, SP);
                            break;
                        case "clearmobs":
                            for (int n = 0; n < CurrentMap.Npcs.Count; n++)
                                if (world.Npcs.ContainsKey(CurrentMap.Npcs[n]))
                                    if (!world.Npcs[CurrentMap.Npcs[n]].IsFriendly) // dont kill friendly npcs like shopkeeper!
                                        world.Npcs[CurrentMap.Npcs[n]].Die(null, -1, false);
                            break;
                        case "mute":
                            if (tokens.Length > 2)
                                if (world.FindPlayer(tokens[1].Trim(), out c))
                                {
                                    c.MuteTime = new TimeSpan(0, Int32.Parse(tokens[2]), 0);
                                    world.SendClientMessage(c, "You are muted for " + c.MuteTime.Hours + "h " + c.MuteTime.Minutes + "m and " + c.MuteTime.Seconds + "s");
                                }
                            break;
                        case "unmute":
                            if (tokens.Length > 1)
                                if (world.FindPlayer(tokens[1].Trim(), out c))
                                    c.MuteTime = new TimeSpan(0, 0, 0);
                            break;
                        case "dc":
                        case "disconnect":
                            if (tokens.Length > 1)
                                if (world.FindPlayer(tokens[1].Trim(), out c))
                                    if (tokens.Length > 2) c.Disconnect(tokens[2]);
                                    else c.Disconnect("Not specified");
                            break;
                        case "to":
                            if (tokens.Length > 1)
                            {
                                if (world.FindPlayer(tokens[1].Trim(), out c) && this != c)
                                {

                                    WhisperIndex = c.ID;
                                    Notify(CommandMessageType.NotifyWhisperOn, c.Name);
                                }
                            }
                            else
                            {
                                WhisperIndex = -1;
                                Notify(CommandMessageType.NotifyWhisperOff, "");
                            }
                            break;
                        case "getskills":
                            for (int i = 0; i < Globals.MaximumSkills; i++)
                                if (Skills[(SkillType)i] != null)
                                {
                                    Skills[(SkillType)i].MaxOut();
                                    Notify(CommandMessageType.NotifyStudySkillSuccess, i, Skills[(SkillType)i].Level);
                                }
                            break;
                        case "getcrits":
                            criticals = MaxCriticals;
                            Notify(CommandMessageType.NotifyCriticals, criticals);
                            break;
                    }
                    return; // commands dont need to update players
            }

            if (mode != ChatType.Local && IsDead) return; // can only speak in local when dead

            // confuse language effect
            if (MagicEffects.ContainsKey(MagicType.Confuse) && MagicEffects[MagicType.Confuse].Magic.Effect1 == 1 && Dice.Roll(1, 3) != 2)
            {
                StringBuilder confusedText = new StringBuilder();
                for (int l = 0; l < message.Length; l++)
                    if (message[l] != ' ')
                        switch (Dice.Roll(1, 8))
                        {
                            case 1: confusedText.Append('!'); break;
                            case 2: confusedText.Append('#'); break;
                            case 3: confusedText.Append('%'); break;
                            case 4: confusedText.Append('$'); break;
                            case 5: confusedText.Append('^'); break;
                            default: confusedText.Append(message[l]); break;
                        }
                    else confusedText.Append(" ");
                message = confusedText.ToString();
            }

            // SP depletion
            switch (mode)
            {
                case ChatType.Global:
                case ChatType.Town:
                case ChatType.Guild:
                case ChatType.Party:
                    if (SP < 3) mode = ChatType.Local;
                    else DepleteSP(3);
                    break;
            }

            byte[] data = new byte[17 + message.Length];
            Buffer.BlockCopy(ClientID.GetBytes(), 0, data, 0, 2);
            Buffer.BlockCopy(X.GetBytes(), 0, data, 2, 2);
            Buffer.BlockCopy(Y.GetBytes(), 0, data, 4, 2);
            Buffer.BlockCopy(Name.GetBytes(10), 0, data, 6, 10);
            data[16] = (byte)((int)mode);
            Buffer.BlockCopy(message.GetBytes(message.Length), 0, data, 17, message.Length);

            switch (mode)
            {
                case ChatType.Whisper:
                    if (world.Players.ContainsKey(WhisperIndex))
                    {
                        world.Send(this, CommandType.Chat, CommandMessageType.Confirm, data);
                        world.Send(world.Players[WhisperIndex], CommandType.Chat, CommandMessageType.Confirm, data);
                    }
                    else
                    {
                        WhisperIndex = -1;
                        Notify(CommandMessageType.NotifyWhisperOff, "");
                    }
                    break;
                case ChatType.Local: // nearby chat - added +3 cell range
                    foreach (Map map in world.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (world.Players.ContainsKey(map.Players[p]))
                            {
                                Character player = world.Players[map.Players[p]];
                                if (player.ClientID == ClientID || player.IsWithinRange(this, 10, 7))
                                    world.Send(player, CommandType.Chat, CommandMessageType.Confirm, data);
                            }
                    break;
                case ChatType.Guild: // guild chat
                    if (Guild == Guild.None) break;
                    foreach (Map map in world.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (world.Players.ContainsKey(map.Players[p]))
                            {
                                Character player = world.Players[map.Players[p]];
                                if (player.ClientID == ClientID || player.Guild == Guild)
                                    world.Send(player, CommandType.Chat, CommandMessageType.Confirm, data);
                            }
                    break;
                case ChatType.Global: // global chat
                case ChatType.GameMaster:
                    foreach (Map map in world.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (world.Players.ContainsKey(map.Players[p]))
                            {
                                Character player = world.Players[map.Players[p]];
                                world.Send(player, CommandType.Chat, CommandMessageType.Confirm, data);
                            }
                    break;
                case ChatType.Town: // town chat
                    foreach (Map map in world.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (world.Players.ContainsKey(map.Players[p]))
                            {
                                Character player = world.Players[map.Players[p]];
                                if (player.Town == Town)
                                    world.Send(player, CommandType.Chat, CommandMessageType.Confirm, data);
                            }
                    break;
                case ChatType.Party: // party chat
                    if (HasParty)
                        for (int p = 0; p < Party.MemberCount; p++)
                            if (world.Players.ContainsKey(Party.Members[p]))
                                world.Send(world.Players[Party.Members[p]], CommandType.Chat, CommandMessageType.Confirm, data);
                    break;
            }

            // logging
            switch (mode)
            {
                case ChatType.Whisper:
                    if (world.Players.ContainsKey(WhisperIndex))
                         MessageLogged(Name + " [" + mode.ToString() + " >> " + world.Players[WhisperIndex].Name + "]: " + message, LogType.Chat);
                     else MessageLogged(Name + " [" + mode.ToString() + " >> Unknown]: " + message, LogType.Chat);
                    break;
                case ChatType.Guild:
                     MessageLogged(Name + " [" + Guild.Name + " " + mode.ToString() + " Chat]: " + message, LogType.Chat);
                    break;
                default:
                     MessageLogged(Name + " [" + mode.ToString() + " Chat]: " + message, LogType.Chat);
                    break;
            }
        }

        public void AcceptQuest()
        {
            // TODO - talktonpc (cityhall guy) gives "requestedquestid". accept it heres
        }

        public void TalkToNpc(Npc npc)
        {
            switch (npc.NpcType)
            {
                
            }
        }

        public void JoinGuild(Character guildMaster)
        {
            this.guild = guildMaster.Guild;
            guildRank = 10;

            Notify(CommandMessageType.JoinGuildApprove, guildRank, guild.Name);

            world.SendCommonEventToNearbyPlayers(this, CommandMessageType.ClearGuildName, 0, 0, 0, 0);

            // TODO send DEF_NOTIFY_NEWGUILDSMAN to all guild members
        }

        public void CreateParty()
        {
            party = new Party(id);
            party.World = world;

            world.Parties.Add(party);
        }

        public void JoinParty(Character character)
        {
            if (character == null) return;
            if (HasParty || IsCombatMode || this.Side != character.Side || character.PartyRequestID != -1)
            {
                Notify(CommandMessageType.NotifyParty, 7, 0, 0, 0);
                return;
            }

            character.Notify(CommandMessageType.NotifyPartyRequest, name);
            character.PartyRequestID = id;
        }

        public void LeaveParty()
        {
            // remove character from party
            if (HasParty)
            {
                party.RemoveMember(id);

                // notify all members of player leaving
                for (int p = 0; p < party.Members.Count; p++)
                    if (world.Players.ContainsKey(party.Members[p]))
                        world.Players[party.Members[p]].Notify(CommandMessageType.NotifyParty, 6, 1, name);

                // if last member in party, remove the party from the world
                if (party.MemberCount <= 0) world.Parties.Remove(party);
            }
        }

        public void HandlePartyRequest()
        {
            if (world.Players.ContainsKey(partyRequestID))
            {
                if (!HasParty)
                {
                    CreateParty();
                    // notify self of creation "player joined the party"
                    Notify(CommandMessageType.NotifyParty, 4, 1, name);
                }

                // add them to party, notify if party is full
                Character character = world.Players[partyRequestID];
                if (party.MemberCount < Globals.MaximumPartyMembers)
                {
                    party.AddMember(partyRequestID);
                    character.Party = party;
                }
                else
                {
                    Notify(CommandMessageType.NotifyParty, 7, 0, 0, 0);
                    return;
                }

                // notify all members of new player joining
                for (int p = 0; p < party.Members.Count; p++)
                    if (world.Players.ContainsKey(party.Members[p]))
                        world.Players[party.Members[p]].Notify(CommandMessageType.NotifyParty, 4, 1, character.Name);

                // notify new member of all existing members
                for (int p = 0; p < party.Members.Count; p++)
                    if (party.Members[p] != partyRequestID && world.Players.ContainsKey(party.Members[p]))
                        character.Notify(CommandMessageType.NotifyParty, 4, 1, world.Players[party.Members[p]].Name);
            }
            // clear request id to allow new requests
            partyRequestID = -1;
        }

        public void CreateGuild(string guildName)
        {
            if (world.IsCrusade) return;
            if (string.IsNullOrEmpty(guildName)) return;
            if (guildName.Equals(Guild.None.Name)) return;

            if (!HasGuild && level >= Globals.CreateGuildLevel && charisma >= Globals.CreateGuildCharisma)
            {
                if (Gold == null || Gold.Count < Globals.CreateGuildCost)
                {
                    Notify(CommandMessageType.NotifyNotEnoughGold, -1);
                    world.Send(this, CommandType.ResponseCreateGuild, CommandMessageType.Reject);
                }
                else
                {
                    Guild guild = new Guild(guildName, Guid.NewGuid().ToString());
                    guild.FoundedBy = name;
                    guild.FoundedOn = DateTime.Now;
                    if (GuildCreated(guild))
                    {
                        // take payment
                        Gold.Count -= Globals.CreateGuildCost;
                        if (Gold.Count <= 0) DepleteItem(GoldIndex, true);
                        else Notify(CommandMessageType.NotifyItemCount, GoldIndex, Gold.Count, 0);

                        // set player guild
                        this.guild = guild;
                        this.guildRank = 0;
                        world.Guilds.Add(guild.Name, guild);
                        RemoveMagicEffect(MagicType.CreateDynamic); // TODO replace this; dummy to update nearby players

                        world.Send(this, CommandType.ResponseCreateGuild, CommandMessageType.Confirm);
                    }
                    else world.Send(this, CommandType.ResponseCreateGuild, CommandMessageType.Reject);
                }
            }
            else world.Send(this, CommandType.ResponseCreateGuild, CommandMessageType.Reject);
        }

        public void DisbandGuild(string guildName)
        {
            if (world.IsCrusade) return;
            if (string.IsNullOrEmpty(guildName)) return;
            if (guildName.Equals(Guild.None.Name)) return;

            if (HasGuild && guildRank == 0 && guild.Name.Equals(guildName))
            {
                if (world.Guilds.ContainsKey(guildName) && GuildDisbanded(world.Guilds[guildName]))
                {
                    foreach (Map map in world.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (world.Players.ContainsKey(map.Players[p]) && world.Players[map.Players[p]].Guild.ID == world.Guilds[guildName].ID)
                            {
                                if (map.Players[p] != ClientID) world.Players[map.Players[p]].Notify(CommandMessageType.NotifyGuildDisbanded, world.Players[map.Players[p]].Town.ToString().ToLower());

                                world.Players[map.Players[p]].Guild = Guild.None;
                                world.Players[map.Players[p]].GuildRank = -1;

                                world.SendCommonEventToNearbyPlayers(world.Players[map.Players[p]], CommandMessageType.ClearGuildName, 0, 0, 0, 0);
                            }

                    world.Send(this, CommandType.ResponseDisbandGuild, CommandMessageType.Confirm);
                }
                else world.Send(this, CommandType.ResponseDisbandGuild, CommandMessageType.Reject);
            }
            else world.Send(this, CommandType.ResponseDisbandGuild, CommandMessageType.Reject);
        }

        public void SetCrusadeDuty(int duty)
        {
            if (Enum.TryParse<CrusadeDuty>(duty.ToString(), out crusadeDuty))
                switch (crusadeDuty)
                {
                    case CrusadeDuty.Commander:
                        if (guildRank != 0) crusadeDuty = CrusadeDuty.None;
                        else crusadeConstructionPoints = 3000; 
                        break;
                }

            Notify(CommandMessageType.NotifyCrusade, 1, 0, 0);
        }

        public bool SetMagicEffect(Magic magic)
        {
            if (magicEffects.ContainsKey(magic.Type)) return false;
            magicEffects.Add(magic.Type, new MagicEffect(magic, DateTime.Now));

            // flags and notify
            unchecked
            {
                switch (magic.Type)
                {
                    case MagicType.Berserk:
                        status = status | 0x00000020;
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect1);
                        break;
                    case MagicType.Invisibility:
                        status = status | 0x00000010;
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect1);
                        break;
                    case MagicType.IceLine: // uses Ice effect type
                    case MagicType.Ice:
                        status = status | 0x00000040;
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)MagicType.Ice, 1);
                        break;
                    case MagicType.Protect:
                        switch (magic.Effect1)
                        {
                            case 1: status = status | 0x08000000; break; // arrow
                            case 2:
                            case 5: status = status | 0x04000000; break; // magic
                            case 3:
                            case 4: status = status | 0x02000000; break; // physical
                        }
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect1);
                        break;
                    case MagicType.Poison:
                        status = status | 0x00000080;
                        poisonTime = DateTime.Now;
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect2);
                        break;
                    case MagicType.Paralyze:
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect1);
                        break;
                    case MagicType.Confuse:
                        switch (magic.Effect1)
                        {
                            case 3: // illusion
                            case 4: status = status | 0x01000000; break; // illusion movement
                        }
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect1);
                        break;
                }
            }

            if (StatusChanged != null) StatusChanged(this);
            return true;
        }

        public bool RemoveMagicEffect(MagicType type)
        {
            if (!magicEffects.ContainsKey(type)) return false;

            // flags and notify
            unchecked
            {
                switch (type)
                {
                    case MagicType.Berserk:
                        status = status & (int)0xFFFFFFDF;
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type);
                        break;
                    case MagicType.Invisibility:
                        status = status & (int)0xFFFFFFEF;
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type);
                        break;
                    case MagicType.Ice:
                        status = status & (int)0xFFFFFFBF;
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type);
                        break;
                    case MagicType.Protect:
                        switch (magicEffects[type].Magic.Effect1)
                        {
                            case 1: status = status & (int)0xF7FFFFFF; break; // arrows
                            case 2:
                            case 5: status = status & (int)0xFBFFFFFF; break; // magic
                            case 3:
                            case 4: status = status & (int)0xFDFFFFFF; break; // physical
                        }
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type, magicEffects[type].Magic.Effect1);
                        break;
                    case MagicType.Poison:
                        status = status & (int)0xFFFFFF7F;
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type);
                        break;
                    case MagicType.Paralyze:
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type);
                        break;
                    case MagicType.Confuse:
                        switch (magicEffects[type].Magic.Effect1)
                        {
                            case 3: // illusion
                            case 4: status = status & (int)0xFEFFFFFF; break; // illusion movement
                        }
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type, magicEffects[type].Magic.Effect1);
                        break;
                }
            }

            magicEffects.Remove(type);

            if (StatusChanged != null) StatusChanged(this);
            return true;
        }

        public void UseSpecialAbility()
        {
            if (specialAbilityTime.TotalSeconds > 0) return;
            if (specialAbilityEnabled) return;

            specialAbilityEnabled = true;
            specialAbilityStartTime = DateTime.Now;

            switch (inventory[specialAbilityItemIndex].SpecialAbilityType)
            {
                case ItemSpecialAbilityType.IceWeapon:
                case ItemSpecialAbilityType.MedusaWeapon:
                case ItemSpecialAbilityType.XelimaWeapon:
                    appearance4 = ((appearance4 & 0xFF0F) | 0x0010);
                    break;
                case ItemSpecialAbilityType.MerienArmour:
                case ItemSpecialAbilityType.MerienShield:
                case ItemSpecialAbilityType.Unknown:
                    appearance4 = ((appearance4 & 0xFF0F) | 0x0020);
                    break;
            }

            Notify(CommandMessageType.NotifySpecialAbilityStatus, 1, (int)inventory[specialAbilityItemIndex].SpecialAbilityType, inventory[specialAbilityItemIndex].SpecialEffect1);

            if (StatusChanged != null) StatusChanged(this);
        }

        /// <summary>
        /// Handles special abilities.
        /// </summary>
        public void ChargeSpecialAbility()
        {
            if (specialAbilityEnabled)
            {
                TimeSpan ts = DateTime.Now - specialAbilityStartTime;
                if (ts.TotalSeconds >= inventory[specialAbilityItemIndex].SpecialEffect1)
                {
                    Notify(CommandMessageType.NotifySpecialAbilityStatus, 3, 0, 0);
                    specialAbilityEnabled = false;
                    specialAbilityTime = new TimeSpan(0, 0, Globals.SpecialAbilityTime);

                    appearance4 = (appearance4 & 0xFF0F);

                    if (StatusChanged != null) StatusChanged(this);
                }
            }
            else
            {
                if (specialAbilityTime.TotalSeconds <= 0) return;

                specialAbilityTime = specialAbilityTime.Subtract(new TimeSpan(0, 0, 3)); // this happens on the timer every 3 seconds

                if (specialAbilityTime.TotalSeconds <= 0)
                {
                    specialAbilityTime = new TimeSpan(0, 0, 0);
                    Notify(CommandMessageType.NotifySpecialAbilityEnabled);
                }
            }
        }

        public void ChargeCritical()
        {
            criticalCharge++;

            if (criticalCharge > 14)
            {
                criticals++;
                if (criticals > MaxCriticals) criticals = MaxCriticals;
                Notify(CommandMessageType.NotifyCriticals, criticals);
                criticalCharge = 0;
            }
        }

        /// <summary>
        /// Handles setting of all stats. i.e after level up.
        /// </summary>
        /// <returns>True or False to indicate that stat change has succeeded or not.</returns>
        public bool ChangeStats(int strength, int vitality, int dexterity, int intelligence, int magic, int charisma)
        {
            if (this.strength + this.vitality + this.dexterity + this.intelligence + this.magic + this.charisma +
                strength + vitality + dexterity + intelligence + magic + charisma > TotalStatPoints)
            {
                Notify(CommandMessageType.NotifyStatChangeLevelUpFailed);
                return false;
            }

            if (this.strength + strength > Globals.MaximumStat || this.dexterity + dexterity > Globals.MaximumStat || this.vitality + vitality > Globals.MaximumStat ||
                this.magic + magic > Globals.MaximumStat || this.charisma + charisma > Globals.MaximumStat || this.intelligence + intelligence > Globals.MaximumStat)
            {
                Notify(CommandMessageType.NotifyStatChangeLevelUpFailed);
                return false;
            }

            this.strength += strength;
            this.dexterity += dexterity;
            this.vitality += vitality;
            this.intelligence += intelligence;
            this.magic += magic;
            this.charisma += charisma;

            // if strength changes, check swing speed and update status
            if (strength > 0 && Weapon != null)
            {
                unchecked
                {
                    int temp;
                    int speed = Weapon.Speed - ((this.strength + this.strengthBonus) / 13);
                    if (speed < 0) speed = 0;

                    temp = status;
                    temp = temp & (int)0xFFFFFFF0;
                    temp = temp | speed;
                    status = temp;
                    if (StatusChanged != null) StatusChanged(this);
                }
            }


            Notify(CommandMessageType.NotifyStatChangeLevelUpSuccess);

            return true;
        }

        /// <summary>
        /// Handles decrement of a particular stat by 1 using majestic points.
        /// </summary>
        /// <param name="stat">Enum type of the stat. These are hexidecimal values used by the client.</param>
        public void ChangeStat(Stat stat)
        {
            if (majestics <= 0)
            {
                Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
            }

            switch (stat)
            {
                case Stat.Strength:
                    if (strength - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    strength--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                case Stat.Dexterity:
                    if (dexterity - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    dexterity--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                case Stat.Intelligence:
                    if (intelligence - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    intelligence--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                case Stat.Magic:
                    if (magic - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    magic--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                case Stat.Vitality:
                    if (vitality - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    vitality--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                case Stat.Charisma:
                    if (charisma - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    charisma--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                default: return;
            }

            // stat changes may cause the player to have too many points:
            if (hp > MaxHP) { hp = MaxHP; Notify(CommandMessageType.NotifyHP, hp, mp); }
            if (mp > MaxMP) { mp = MaxMP; Notify(CommandMessageType.NotifyMP, mp); }
            if (sp > MaxSP) { sp = MaxSP; Notify(CommandMessageType.NotifySP, sp); }
        }

        /// <summary>
        /// Loads equipped items in to the correct slots (Arms, Body, Legs, Feet) etc.
        /// </summary>
        public void LoadEquipment()
        {
            for (int i = 0; i < Globals.MaximumInventoryItems; i++)
                if (inventory[i] != null && inventory[i].IsEquipped)
                    EquipItem(i);
        }

        public void Parse(byte[] data)
        {
            name = Encoding.ASCII.GetString(data, 6, 10).Trim('\0');
            gender = (GenderType)((int)data[66]);
            skin = (SkinType)((int)data[67]);
            hairStyle = (int)data[68];
            hairColour = (int)data[69];
            underwearColour = (int)data[70];
            strength = (int)data[71];
            vitality = (int)data[72];
            dexterity = (int)data[73];
            intelligence = (int)data[74];
            magic = (int)data[75];
            charisma = (int)data[76];

            appearance1 = appearance2 = appearance3 = appearance4 = appearanceColour = 0;
            appearance1 = appearance1 | underwearColour;
            appearance1 = appearance1 | (hairStyle << 8);
            appearance1 = appearance1 | (hairColour << 4);
        }

        /// <summary>
        /// Updates Equipment and changes the Appearance ready to be sent to the client.
        /// </summary>
        /// <param name="itemIndex">Index of the item in the Inventory list.</param>
        /// <returns>True or False to indicate successful Equip of the item.</returns>
        public bool EquipItem(int itemIndex)
        {
            lock (statusLock)
            {
                if (inventory[itemIndex] == null) return false;

                Item item = inventory[itemIndex];
                if (item.Type != ItemType.Equip) return false;
                if (item.EquipType == EquipType.None) return false;
                if (item.Endurance == 0) return false;
                if (item.LevelLimit > level) return false;
                if (item.GenderLimit != GenderType.None && item.GenderLimit != gender) return false;
                if (item.MinimumStrength > strength + strengthBonus) return false;
                if (item.MinimumDexterity > dexterity + dexterityBonus) return false;
                if (item.MinimumVitality > vitality + vitalityBonus) return false;
                if (item.MinimumIntelligence > intelligence + intelligenceBonus) return false;
                if (item.MinimumMagic > magic + magicBonus) return false;
                if (item.MinimumCharisma > charisma + charismaBonus) return false;
                if (!(item.BoundID.Equals(Globals.UnboundItem)))
                    if (!(item.BoundID.Equals(this.databaseID)))
                    {
                        world.SendClientMessage(this, "This item is bound to " + item.BoundName);
                        return false;
                    }

                switch (item.EquipType)
                {
                    case EquipType.LeftHand:
                    case EquipType.RightHand: // shield or weapon should unequip existing dual hand
                        if (equipment[(int)EquipType.DualHand] != -1) UnequipItem(equipment[(int)EquipType.DualHand]);
                        break;
                    case EquipType.DualHand: // dual hand should unequip existing left and right hands
                        if (equipment[(int)EquipType.LeftHand] != -1) UnequipItem(equipment[(int)EquipType.LeftHand]);
                        if (equipment[(int)EquipType.RightHand] != -1) UnequipItem(equipment[(int)EquipType.RightHand]);
                        break;
                    case EquipType.FullBody: // full body should unequip any other equipment slots
                        if (equipment[(int)EquipType.Head] != -1) UnequipItem(equipment[(int)EquipType.Head]);
                        if (equipment[(int)EquipType.Body] != -1) UnequipItem(equipment[(int)EquipType.Body]);
                        if (equipment[(int)EquipType.Arms] != -1) UnequipItem(equipment[(int)EquipType.Arms]);
                        if (equipment[(int)EquipType.Feet] != -1) UnequipItem(equipment[(int)EquipType.Feet]);
                        if (equipment[(int)EquipType.Legs] != -1) UnequipItem(equipment[(int)EquipType.Legs]);
                        if (equipment[(int)EquipType.Back] != -1) UnequipItem(equipment[(int)EquipType.Back]);
                        break;
                    case EquipType.Head:
                    case EquipType.Body:
                    case EquipType.Arms:
                    case EquipType.Feet:
                    case EquipType.Legs:
                    case EquipType.Back: // armour and cape should unequip full body costumes
                        if (equipment[(int)EquipType.FullBody] != -1) UnequipItem(equipment[(int)EquipType.FullBody]);
                        break;
                }
                // if there is something in the current slot then unequip
                if (equipment[(int)item.EquipType] != -1) UnequipItem(equipment[(int)item.EquipType]);

                inventory[itemIndex].IsEquipped = true;
                equipment[(int)item.EquipType] = itemIndex;

                int temp;
                switch (item.EquipType)
                {
                    case EquipType.RightHand:
                        temp = appearance2;
                        temp = temp & 0xF00F;
                        temp = temp | (item.Appearance << 4);
                        appearance2 = temp;

                        temp = appearanceColour;
                        temp = temp & 0x0FFFFFFF;
                        temp = temp | (item.Colour << 28);
                        appearanceColour = temp;

                        unchecked
                        {
                            int speed = item.Speed - ((strength + strengthBonus) / 13);
                            if (speed < 0) speed = 0;

                            temp = status;
                            temp = temp & (int)0xFFFFFFF0;
                            temp = temp | speed;
                            status = temp;
                        }
                        break;
                    case EquipType.LeftHand:
                        temp = appearance2;
                        temp = temp & 0xFFF0;
                        temp = temp | (item.Appearance);
                        appearance2 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xF0FFFFFF;
                            temp = temp | (item.Colour << 24);
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.DualHand:
                        temp = appearance2;
                        temp = temp & 0xF00F;
                        temp = temp | (item.Appearance << 4);
                        appearance2 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0x0FFFFFFF;
                            temp = temp | (item.Colour << 28);
                            appearanceColour = temp;
                        }

                        unchecked
                        {
                            int speed = item.Speed - ((strength + strengthBonus) / 13);
                            if (speed < 0) speed = 0;

                            temp = status;
                            temp = temp & (int)0xFFFFFFF0;
                            temp = temp | speed;
                            status = temp;
                        }
                        break;
                    case EquipType.Body:
                        if (item.Appearance < 100)
                        {
                            temp = appearance3;
                            temp = temp & 0x0FFF;
                            temp = temp | (item.Appearance << 12);
                            appearance3 = temp;
                        }
                        else
                        {
                            temp = appearance3;
                            temp = temp & 0x0FFF;
                            temp = temp | ((item.Appearance - 100) << 12);
                            appearance3 = temp;

                            temp = appearance4;
                            temp = temp | 0x080;
                            appearance4 = temp;
                        }

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFF0FFFFF;
                            temp = temp | (item.Colour << 20);
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Back:
                        temp = appearance4;
                        temp = temp & 0xF0FF;
                        temp = temp | (item.Appearance << 8);
                        appearance4 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFF0FFFF;
                            temp = temp | (item.Colour << 16);
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Arms:
                        temp = appearance3;
                        temp = temp & 0xFFF0;
                        temp = temp | (item.Appearance);
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFF0FFF;
                            temp = temp | (item.Colour << 12);
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Legs:
                        temp = appearance3;
                        temp = temp & 0xF0FF;
                        temp = temp | (item.Appearance << 8);
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFF0FF;
                            temp = temp | (item.Colour << 8);
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Feet:
                        temp = appearance4;
                        temp = temp & 0x0FFF;
                        temp = temp | (item.Appearance << 12);
                        appearance4 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFFF0F;
                            temp = temp | (item.Colour << 4);
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Head:
                        temp = appearance3;
                        temp = temp & 0xFF0F;
                        temp = temp | (item.Appearance << 4);
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFFFF0;
                            temp = temp | (item.Colour);
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.FullBody:
                        temp = appearance3;
                        temp = temp & 0x0FFF;
                        temp = temp | (item.Appearance << 12);
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFF0FFFF;
                            appearanceColour = temp;
                        }
                        break;
                }

                // weapon glows
                if (item.EffectType == ItemEffectType.AttackActivation)
                {
                    if (specialAbilityItemIndex != -1)
                    {
                        Notify(CommandMessageType.NotifyItemUnequipped, (int)inventory[specialAbilityItemIndex].EquipType, specialAbilityItemIndex);
                        UnequipItem(specialAbilityItemIndex);
                    }
                    appearance4 = appearance4 & 0xFFF3;
                    switch (item.SpecialAbilityType)
                    {
                        case ItemSpecialAbilityType.XelimaWeapon:
                            appearance4 = appearance4 | 0x0004;
                            break;
                        case ItemSpecialAbilityType.IceWeapon:
                            appearance4 = appearance4 | 0x000C;
                            break;
                        case ItemSpecialAbilityType.MedusaWeapon:
                            appearance4 = appearance4 | 0x0008;
                            break;
                    }
                    specialAbilityItemIndex = itemIndex;
                    Notify(CommandMessageType.NotifySpecialAbilityStatus, 2, (int)item.SpecialAbilityType, (int)specialAbilityTime.TotalSeconds);
                }

                // armour glows
                if (item.EffectType == ItemEffectType.DefenceActivation)
                {
                    if (specialAbilityItemIndex != -1)
                    {
                        Notify(CommandMessageType.NotifyItemUnequipped, (int)inventory[specialAbilityItemIndex].EquipType, specialAbilityItemIndex);
                        UnequipItem(specialAbilityItemIndex);
                    }
                    appearance4 = appearance4 & 0xFFFC;
                    switch (item.SpecialAbilityType)
                    {
                        case ItemSpecialAbilityType.MerienArmour:
                        case ItemSpecialAbilityType.MerienShield:
                        case ItemSpecialAbilityType.Unknown:
                            appearance4 = appearance4 | 0x0002;
                            break;
                    }
                    specialAbilityItemIndex = itemIndex;
                    Notify(CommandMessageType.NotifySpecialAbilityStatus, 2, (int)item.SpecialAbilityType, (int)specialAbilityTime.TotalSeconds);
                }

                if (item.EffectType == ItemEffectType.AddEffect)
                    switch (item.Effect1)
                    {
                        case 1: bonusMagicResistance += item.Effect2; break;
                        case 2: bonusManaSave += item.Effect2; break;
                        case 3: bonusPhysicalDamage += item.Effect2; break;
                        case 4: bonusPhysicalResistance += item.Effect2; break;
                        case 5: luck += item.Effect2; break;
                        case 6: bonusMagicDamage += item.Effect2; break;
                        case 7: bonusAirAbsorption += item.Effect2; break;
                        case 8: bonusEarthAbsorption += item.Effect2; break;
                        case 9: bonusFireAbsorption += item.Effect2; break;
                        case 10: bonusWaterAbsorption += item.Effect2; break;
                        case 11: bonusPoisonResistance += item.Effect2; break;
                        case 12: bonusHitChance += item.Effect2 + item.SpecialEffect2; break;
                        case 13: bonusHPRecovery += item.Effect2; break;
                        case 14: bonusHitChance += item.Effect2; break;
                        case 15: bonusMagicAbsorption += item.Effect2; break;
                        case 16: strengthBonus += item.Level; break;
                        case 17: dexterityBonus += item.Level; break;
                        case 18: intelligenceBonus += item.Level; break;
                        case 19: magicBonus += item.Level; break;
                        case 20: bonusPhysicalAbsorption += item.Effect2; break;
                    }

                // angels
                if (item.IsAngel)
                {
                    switch (item.Effect1)
                    {
                        case 16: status = status | 0x00001000; break;
                        case 17: status = status | 0x00002000; break;
                        case 18: status = status | 0x00004000; break;
                        case 19: status = status | 0x00008000; break;
                    }
                    if (item.Level > 5)
                    {
                        int stars = (item.Level / 3) * (item.Level / 5);
                        status = status | (stars << 8);
                    }
                    Notify(CommandMessageType.NotifyAngelStats, strengthBonus, intelligenceBonus, dexterityBonus, magicBonus);
                }

                // mana save
                if (item.EffectType == ItemEffectType.AttackManaSave) bonusManaSave += item.Effect4;

                switch (item.SpecialWeaponSecondaryType)
                {
                    case ItemSpecialWeaponSecondaryType.PhysicalResistance: bonusPhysicalResistance += item.SpecialWeaponSecondaryValue * 7; break;
                    case ItemSpecialWeaponSecondaryType.HittingProbability: bonusHitChance += item.SpecialWeaponSecondaryValue * 7; break;
                    case ItemSpecialWeaponSecondaryType.HPRecovery: bonusHPRecovery += item.SpecialWeaponSecondaryValue * 7; break;
                    case ItemSpecialWeaponSecondaryType.MPRecovery: bonusMPRecovery += item.SpecialWeaponSecondaryValue * 7; break;
                    case ItemSpecialWeaponSecondaryType.SPRecovery: bonusSPRecovery += item.SpecialWeaponSecondaryValue * 7; break;
                    case ItemSpecialWeaponSecondaryType.PoisonResistance: bonusPoisonResistance += item.SpecialWeaponSecondaryValue * 7; break;
                }

                // stat changes may cause the player to have too many points:
                if (hp > MaxHP) { hp = MaxHP; Notify(CommandMessageType.NotifyHP, hp, mp); }
                if (mp > MaxMP) { mp = MaxMP; Notify(CommandMessageType.NotifyMP, mp); }
                if (sp > MaxSP) { sp = MaxSP; Notify(CommandMessageType.NotifySP, sp); }

                if (StatusChanged != null) StatusChanged(this);
                return true;
            }
        }

        public bool GiveItem(int itemIndex, int count, int destinationX, int destinationY, int ownerID, string itemName)
        {
            if (isDead) return false;
            if (ownerID == 0) return false;
            if (CurrentMap[destinationY][destinationX].Owner == null) return false;
            if (inventory[itemIndex] == null || count < 1 || count > inventory[itemIndex].Count) return false;

            Item item = inventory[itemIndex];
            IOwner target = CurrentMap[destinationY][destinationX].Owner;
            switch (target.OwnerType)
            {
                case OwnerType.Npc:
                    if (target.ID != ownerID - 10000) return false;
                    if (target.Name.Equals(Globals.WarehouseKeeperName)) // TODO add NpcType="WarehouseKeeper" either to map config or npc config instead of global strings
                        SetWarehouseItem(itemIndex, count);

                    break;
                case OwnerType.Player:
                    Character character = (Character)target;
                    if (character.ID != ownerID) return false;

                    if (item.EffectType == ItemEffectType.GuildAdmission)
                    {
                        if (!world.IsCrusade && !HasGuild && town != OwnerSide.Neutral)
                        {
                            inventory[itemIndex] = null;
                            Notify(CommandMessageType.NotifyGiveItemAndErase, itemIndex, count, target.Name);
                            character.Notify(CommandMessageType.NotifyGuildAdmissionRequest, this.name);
                        }
                    }
                    else if (item.EffectType == ItemEffectType.GuildDismissal)
                    {
                        if (!world.IsCrusade && HasGuild && Guild == character.Guild && character.GuildRank == 0)
                        {
                            inventory[itemIndex] = null;
                            Notify(CommandMessageType.NotifyGiveItemAndErase, itemIndex, count, target.Name);
                            character.Notify(CommandMessageType.NotifyGuildDismissalRequest, this.name);
                        }
                    }
                    else
                    {
                        inventory[itemIndex] = null;
                        if (!character.isDead && character.AddInventoryItem(item))
                            Notify(CommandMessageType.NotifyGiveItemAndErase, itemIndex, count, target.Name);
                        else DropItem(itemIndex, count, item.Name);
                    }
                    break;
            }

            return true;
        }

        /// <summary>
        /// Updates Equipment and changes the Appearance ready to be sent to the client.
        /// </summary>
        /// <param name="itemIndex">Index of the item in the Inventory list.</param>
        /// <returns>True or False to indicate successful Un-Equip of the item.</returns>
        public bool UnequipItem(int itemIndex)
        {
            lock (statusLock)
            {
                if (inventory[itemIndex] == null) return false;

                Item item = inventory[itemIndex];
                if (!item.IsEquipped) return false;
                if (item.Type != ItemType.Equip) return false;
                if (item.EquipType == EquipType.None) return false;

                inventory[itemIndex].IsEquipped = false;
                equipment[(int)item.EquipType] = -1;

                int temp;
                switch (item.EquipType)
                {
                    case EquipType.RightHand:
                        temp = appearance2;
                        temp = temp & 0xF00F;
                        appearance2 = temp;

                        temp = appearanceColour;
                        temp = temp & 0x0FFFFFFF;
                        appearanceColour = temp;

                        unchecked // speed 0 when using fists
                        {
                            temp = status;
                            temp = temp & (int)0xFFFFFFF0;
                            temp = temp | 0;
                            status = temp;
                        }
                        break;
                    case EquipType.LeftHand:
                        temp = appearance2;
                        temp = temp & 0xFFF0;
                        appearance2 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xF0FFFFFF;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.DualHand:
                        temp = appearance2;
                        temp = temp & 0xF00F;
                        appearance2 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0x0FFFFFFF;
                            appearanceColour = temp;
                        }

                        unchecked // speed 0 when using fists
                        {
                            temp = status;
                            temp = temp & (int)0xFFFFFFF0;
                            temp = temp | 0;
                            status = temp;
                        }
                        break;
                    case EquipType.Body:
                        temp = appearance3;
                        temp = temp & 0x0FFF;
                        appearance3 = temp;

                        temp = appearance4;
                        temp = temp & 0xFF7F;
                        appearance4 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFF0FFFFF;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Back:
                        temp = appearance4;
                        temp = temp & 0xF0FF;
                        appearance4 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFF0FFFF;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Arms:
                        temp = appearance3;
                        temp = temp & 0xFFF0;
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFF0FFF;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Legs:
                        temp = appearance3;
                        temp = temp & 0xF0FF;
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFF0FF;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Feet:
                        temp = appearance4;
                        temp = temp & 0x0FFF;
                        appearance4 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFFF0F;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Head:
                        temp = appearance3;
                        temp = temp & 0xFF0F;
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFFFF0;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.FullBody:
                        temp = appearance3;
                        temp = temp & 0x0FFF;
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFF0FFFF;
                            appearanceColour = temp;
                        }
                        break;
                }

                // end activation if the item is unequipped
                if ((item.EffectType == ItemEffectType.AttackActivation || item.EffectType == ItemEffectType.DefenceActivation) && (itemIndex == specialAbilityItemIndex) && specialAbilityEnabled)
                {
                    Notify(CommandMessageType.NotifySpecialAbilityStatus, 4, 0, 0);
                    specialAbilityEnabled = false;
                    specialAbilityTime = new TimeSpan(0, 0, Globals.SpecialAbilityTime);

                    appearance4 = (appearance4 & 0xFF0F);
                }

                // remove weapon glows
                if (item.EffectType == ItemEffectType.AttackActivation)
                {
                    appearance4 = appearance4 & 0xFFF3;
                    if (itemIndex == specialAbilityItemIndex) specialAbilityItemIndex = -1;
                }
                // remove armour glows
                if (item.EffectType == ItemEffectType.DefenceActivation)
                {
                    appearance4 = appearance4 & 0xFFFC;
                    if (itemIndex == specialAbilityItemIndex) specialAbilityItemIndex = -1;
                }

                if (item.EffectType == ItemEffectType.AddEffect)
                    switch (item.Effect1)
                    {
                        case 1: bonusMagicResistance -= item.Effect2; break;
                        case 2: bonusManaSave -= item.Effect2; break;
                        case 3: bonusPhysicalDamage -= item.Effect2; break;
                        case 4: bonusPhysicalResistance -= item.Effect2; break;
                        case 5: luck -= item.Effect2; break;
                        case 6: bonusMagicDamage -= item.Effect2; break;
                        case 7: bonusAirAbsorption -= item.Effect2; break;
                        case 8: bonusEarthAbsorption -= item.Effect2; break;
                        case 9: bonusFireAbsorption -= item.Effect2; break;
                        case 10: bonusWaterAbsorption -= item.Effect2; break;
                        case 11: bonusPoisonResistance -= item.Effect2; break;
                        case 12: bonusHitChance -= item.Effect2 + item.SpecialEffect2; break;
                        case 13: bonusHPRecovery -= item.Effect2; break;
                        case 14: bonusHitChance -= item.Effect2; break;
                        case 15: bonusMagicAbsorption -= item.Effect2; break;
                        case 16: strengthBonus -= item.Level; break;
                        case 17: dexterityBonus -= item.Level; break;
                        case 18: intelligenceBonus -= item.Level; break;
                        case 19: magicBonus -= item.Level; break;
                        case 20: bonusPhysicalAbsorption -= item.Effect2; break;
                    }

                // angels
                if (item.IsAngel)
                {
                    unchecked { status = status & (int)0xFFFF00FF; }
                    Notify(CommandMessageType.NotifyAngelStats, strengthBonus, intelligenceBonus, dexterityBonus, magicBonus);
                }

                // mana save
                if (item.EffectType == ItemEffectType.AttackManaSave) bonusManaSave -= item.Effect4;

                switch (item.SpecialWeaponSecondaryType)
                {
                    case ItemSpecialWeaponSecondaryType.PhysicalResistance: bonusPhysicalResistance -= item.SpecialWeaponSecondaryValue * 7; break;
                    case ItemSpecialWeaponSecondaryType.HittingProbability: bonusHitChance -= item.SpecialWeaponSecondaryValue * 7; break;
                    case ItemSpecialWeaponSecondaryType.HPRecovery: bonusHPRecovery -= item.SpecialWeaponSecondaryValue * 7; break;
                    case ItemSpecialWeaponSecondaryType.MPRecovery: bonusMPRecovery -= item.SpecialWeaponSecondaryValue * 7; break;
                    case ItemSpecialWeaponSecondaryType.SPRecovery: bonusSPRecovery -= item.SpecialWeaponSecondaryValue * 7; break;
                    case ItemSpecialWeaponSecondaryType.PoisonResistance: bonusPoisonResistance -= item.SpecialWeaponSecondaryValue * 7; break;
                }

                // check that minimum stat requirements are still upheld (e.g after removal of strength/dexterity angels
                for (int i = 0; i < 15; i++)
                    if ((equipment[i] != -1) &&
                        (inventory[equipment[i]].MinimumStrength > strength + strengthBonus ||
                        inventory[equipment[i]].MinimumDexterity > dexterity + dexterityBonus ||
                        inventory[equipment[i]].MinimumVitality > vitality + vitalityBonus ||
                        inventory[equipment[i]].MinimumIntelligence > intelligence + intelligenceBonus ||
                        inventory[equipment[i]].MinimumMagic > magic + magicBonus ||
                        inventory[equipment[i]].MinimumCharisma > charisma + charismaBonus))
                        UnequipItem(equipment[i]);

                // stat changes may cause the player to have too many points:
                if (hp > MaxHP) { hp = MaxHP; Notify(CommandMessageType.NotifyHP, hp, mp); }
                if (mp > MaxMP) { mp = MaxMP; Notify(CommandMessageType.NotifyMP, mp); }
                if (sp > MaxSP) { sp = MaxSP; Notify(CommandMessageType.NotifySP, sp); }

                if (StatusChanged != null) StatusChanged(this);
                return true;
            }
        }

        public bool CastMagic(int sourceX, int sourceY, Magic magic) { throw new NotImplementedException(); }

        /// <summary>
        /// Character casts a spell at the destination specified. Checks requirements only. Magic effects are handled by GameServer.
        /// </summary>
        /// <param name="sourceX">X coordinate of character.</param>
        /// <param name="sourceY">Y coordinate of character.</param>
        /// <param name="magic">Magic type being cast.</param>
        /// <returns>True or False to indicate if CastMagic has failed or not.</returns>
        public bool CastMagic(int sourceX, int sourceY, Magic magic, out bool notifyFailed)
        {
            notifyFailed = false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (Shield != null) return false; // no shield casting
            if (Weapon != null && Weapon.RelatedSkill != SkillType.Staff) return false; // staff casting only
            if (magic == null) return false;
            if ((intelligence + intelligenceBonus) < magic.RequiredIntelligence) { notifyFailed = true; return false; }
            if (world.IsHeldenianBattlefield && map.IsHeldenianMap && world.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start
            int manaCost = Utility.GetManaCost(magic.ManaCost, bonusManaSave, isSafeMode);
            if (mp < manaCost) return false;

            if (CurrentMap.IsBuilding || CurrentMap.IsSafeMap || CurrentMap[sourceY][sourceX].IsSafe)
                switch (magic.Category)
                {
                    case MagicCategory.Offensive: return false;
                    default: break; // utility and defensive spells can be cast
                }

            // TODO check casting probability, include weather and hunger penalties and notify failed
            if (!spellStatus[magic.Index]) // not learned (packet edit?)
            {
                notifyFailed = true;
                return false;
            }

            skills[SkillType.Magic].Experience++;
            return true;
        }

        /// <summary>
        /// Character starts casting a spell.
        /// </summary>
        /// <param name="sourceX">X coordinate of character.</param>
        /// <param name="sourceY">Y coordinate of character.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <returns>True or False to indicate if PrepareMagic has failed or not.</returns>
        public bool PrepareMagic(int sourceX, int sourceY, MotionDirection direction)
        {
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (Shield != null) return false; // no shield casting
            if (Weapon != null && Weapon.RelatedSkill != SkillType.Staff) return false; // staff casting only
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);

            this.direction = direction;

            return true;
        }

        public int GetMagicHitChance(Magic spell)
        {
            int magicCircle = (spell.Index / 10) +1;
            int magicLevel = (level / 10);
            int hitChance = Math.Max(skills[SkillType.Magic].Level, 1);
            double temp1 = ((double)hitChance / 100.0f);
            double temp2 = ((double)(temp1 * (double)Globals.MagicCastingProbability[magicCircle]));
            hitChance = (int)temp2;

            if (intelligence + intelligenceBonus > 50) hitChance += (intelligence + intelligenceBonus) / 2;

            if (magicCircle != magicLevel)
                if (magicCircle > magicLevel)
                {
                    temp1 = (double)((magicCircle - magicLevel) * Globals.MagicCastingLevelPenalty[magicCircle]);
                    temp2 = (double)((magicCircle - magicLevel) * 10);
                    double temp3 = ((double)level / temp2) * temp1;
                    hitChance -= Math.Abs(Math.Abs(magicCircle - magicLevel) * Globals.MagicCastingLevelPenalty[magicCircle] - (int)temp3);
                }
                else hitChance += 5 * Math.Abs(magicCircle - magicLevel);

            switch (CurrentMap.Weather)
            {
                case WeatherType.Rain: hitChance -= (hitChance / 24); break; // 4% reduction
                case WeatherType.RainMedium: hitChance -= (hitChance / 12); break; // 8% reduction
                case WeatherType.RainHeavy: hitChance -= (hitChance / 5); break; // 20% reduction
            }
            if (Weapon != null)
                hitChance += Weapon.CastingProbabilityBonus;

            int result = Dice.Roll(1, 100);
            if (hitChance < result) return -1;

            if (magic + magicBonus > 50) hitChance += (magic + magicBonus) - 50;
            hitChance += bonusHitChance;
            if (hitChance < 0) hitChance = 1;

            return hitChance;
        }

        /// <summary>
        /// Character has casted offensive magic. Calculate damage and resistance of target.
        /// </summary>
        /// <param name="hitX">Actual hit location X.</param>
        /// <param name="hitY">Actual hit location Y.</param>
        /// <param name="target">Target object.</param>
        /// <param name="targetType">Target object's type.</param>
        /// <param name="spell">Magic type casted.</param>
        /// <returns>True or False to indicate damage has been dealt.</returns>
        public bool MagicAttack(int hitX, int hitY, IOwner target, Magic spell, MagicTarget magicTarget)
        {
            int experience = 0;
            int damage = 0;

            if (target == null) return false;
            if (target.IsDead) return false;
            if (CurrentLocation.IsSafe || target.CurrentLocation.IsSafe) return false; // safe zones
            if (!CurrentMap.IsAttackEnabled) return false;
            if (world.IsHeldenianBattlefield && map.IsHeldenianMap && world.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start

            // different effect values are used for the targeted object or for collatoral object in the "area of effect"
            if (magicTarget == MagicTarget.Single) damage = Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3);
            else if (magicTarget == MagicTarget.Area) damage = Dice.Roll(spell.Effect4, spell.Effect5, spell.Effect6);

            MotionDirection flyDirection = Utility.GetFlyDirection(hitX, hitY, target.X, target.Y);

            if (IsHero) damage += 3;

            // 20% damage increase for hunt zones (Middleland, Promiseland and Icebound by default)
            if (map.IsHuntZone) damage += damage / 5;

            if (Weapon != null)
                switch(Weapon.SpecialAbilityType)
                {
                    case ItemSpecialAbilityType.Berserk: damage = (int)((double)damage * 1.3); break; // zerk wand
                    case ItemSpecialAbilityType.Kloness: break; // TODO kloness wands
                }

            if (Necklace != null)
                switch (Necklace.SpecialAbilityType)
                {
                    case ItemSpecialAbilityType.Kloness: break; // TODO kloness necky
                }

            damage += (int)(((double)damage * (double)(((double)(magic + magicBonus) / 3.3f) / 100.0f)) + 0.5f); // 33% bonus from Magic stat
            damage += bonusMagicDamage; // Item bonuses
            if (CurrentMap.IsFightZone) damage = (int)((double)damage * 1.3);// fightzone 30% bonus
            if (world.IsCrusade && crusadeDuty == CrusadeDuty.Fighter) // crusade bonus - <= level 80 = + 70%,else +50%
            {
                if (level <= 80) damage = (int)((double)damage * 1.7);
                else damage = (int)((double)damage * 1.5);
            }
            // TODO - heldenian damage


            switch (target.OwnerType)
            {
                case OwnerType.Player:
                    Character character = (Character)target;
                    if (CurrentMap.IsBuilding || CurrentMap.IsSafeMap) return false;
                    if (character.IsAdmin) return false;
                    if (character.Town == OwnerSide.Neutral ||
                        town == OwnerSide.Neutral) return false; // cant hurt travs & travs cant hurt you

                    if (HasParty && character.HasParty && party.ID.Equals(character.Party.ID)) return false;

                    if (isSafeMode && target.Side == this.Side) return false; // safe mode

                    // TODO - elemental absorption necklaces etc
                    // TODO - ruby+emerald ring w/ endurance reduction e.g rings

                    // righteous weapons - damages negative rep players for each 10 negative rep points. max +10 damage
                    if (Weapon != null && Weapon.SpecialWeaponPrimaryType == ItemSpecialWeaponPrimaryType.Righteous && character.Reputation < 0)
                        damage += Math.Min(Math.Abs(character.Reputation) / 10, 10);

                    // Magic absorption
                    damage -= character.BonusMagicAbsorption;

                    // VIT added absorption
                    damage -= Dice.Roll(1, (character.Vitality / 10) - 1);

                    if (damage == 0) return false;

                    // PFM
                    if (character.MagicEffects.ContainsKey(MagicType.Protect) && character.MagicEffects[MagicType.Protect].Magic.Effect1 == 2)
                        damage /= 2;

                    experience += character.TakeDamage(this, DamageType.Magic, damage, flyDirection);
                    if (ShowDamageEnabled) world.SendClientMessage(this, string.Format("You did {0} damage to {1}.", damage, character.Name));
                    if (Weapon != null && Weapon.SpecialWeaponSecondaryType == ItemSpecialWeaponSecondaryType.Experience)
                        AddExp(experience + ((experience / 100) * (Weapon.SpecialWeaponSecondaryValue * 10)));
                    else AddExp(experience);
                    return true;
                case OwnerType.Npc:
                    Npc npc = (Npc)target;

                    if (npc.IsFriendly) return false;

                    // damage absorption
                    if (npc.MagicAbsorption > 0)
                        damage = damage - (int)(((double)damage / 100.0f) * (double)npc.PhysicalAbsorption);

                    if (damage == 0) return false;

                    // PFM
                    if (npc.MagicEffects.ContainsKey(MagicType.Protect) && npc.MagicEffects[MagicType.Protect].Magic.Effect1 == 2)
                        damage /= 2;

                    // deal the damage and store any experience received
                    experience += npc.TakeDamage(this, DamageType.Magic, damage, flyDirection);
                    if (ShowDamageEnabled) world.SendClientMessage(this, string.Format("You did {0} damage to {1}.", damage, npc.Name));
                    if (Weapon != null && Weapon.SpecialWeaponSecondaryType == ItemSpecialWeaponSecondaryType.Experience)
                        AddExp(experience + ((experience / 100) * (Weapon.SpecialWeaponSecondaryValue * 10)));
                    else AddExp(experience);
                    return true;
                default: return false;
            }
        }

        /// <summary>
        /// Character performs a melee Attack. Calculate damage and resistance of target.
        /// </summary>
        /// <param name="sourceX">X coordinate of attack operation.</param>
        /// <param name="sourceY">Y coordinate of attack operation.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <param name="targetX">X coordinate of the target location.</param>
        /// <param name="targetY">Y coordinate of the target location.</param>
        /// <param name="target">Target object.</param>
        /// <param name="targetType">Target object's type.</param>
        /// <returns>True or False to indicate if Attack has failed or not.</returns>
        public bool MeleeAttack(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, IOwner target, int attackType, bool isDash)
        {
            int experience = 0;
            int damage = 0;
            int hitChance = 0;
            bool isCritical = ((attackType >= 20) && criticals > 0);
            int distanceX = Math.Abs(sourceX - targetX);
            int distanceY = Math.Abs(sourceY - targetY);

            this.direction = direction; // should be set even if attack fails. to update clients

            if ((sourceX != this.mapX || sourceY != this.mapY) && !isDash && attackType < 20) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);
            if (!IsCombatMode) return false;
            if (target == null) return false;
            if (target.IsDead) return false;
            if (target.IsInvisible) return false;
            if (targetX != target.X || targetY != target.Y) return false; // target coordinates to not match the target object's location
            if (CurrentLocation.IsSafe || target.CurrentLocation.IsSafe) return false; // safe zones
            if (!CurrentMap.IsAttackEnabled) return false;
            if (world.IsHeldenianBattlefield && map.IsHeldenianMap && world.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start

            // if hungry or low sp, 10% chance of failed hit
            if (((hunger <= 10) || sp <= 0) && Dice.Roll(1, 10) == 5) return false;

            if (Weapon != null)
            {
                // weapon base damage. check if target is large, if so roll large damage. rolls small damage for players and small npcs
                if (target.Size == OwnerSize.Large) damage = Weapon.DamageLarge.Roll();
                else damage = Weapon.DamageSmall.Roll();

                switch (Weapon.RelatedSkill)
                {
                    case SkillType.Archery:
                        if (!HasArrows) return false; // cant use bow without arrows
                        if (distanceX > 9 || distanceY > 9) return false;

                        // around 8% bonus from strength
                        damage += Dice.Roll(1, (strength + strengthBonus) / 12);
                        hitChance = skills[Weapon.RelatedSkill].Level;
                        break;
                    case SkillType.Axe:
                    case SkillType.Fencing:
                    case SkillType.Hammer:
                    case SkillType.LongSword:
                    case SkillType.ShortSword:
                    case SkillType.Staff:
                        if (Weapon.HasExtendedRange && !isCritical && (distanceX > Weapon.Range+2 || distanceY > Weapon.Range+2)) return false;
                        else if (!Weapon.HasExtendedRange && !isCritical && (distanceX > Weapon.Range || distanceY > Weapon.Range)) return false;
                        if (Weapon.HasExtendedRange && isCritical && (distanceX > Weapon.CriticalRange+2 || distanceY > Weapon.CriticalRange+2)) return false;
                        else if (!Weapon.HasExtendedRange && isCritical && (distanceX > Weapon.CriticalRange || distanceY > Weapon.CriticalRange)) return false;
                        // 20% bonus from strength
                        damage += (int)(((double)damage * (double)(((double)(strength + strengthBonus) / 5.0f) / 100.0f)) + 0.5f);
                        hitChance = skills[Weapon.RelatedSkill].Level;
                        break;
                }

                TakeItemDamage(1, Weapon, WeaponIndex);
            }
            else
            {
                if (distanceX > 1 || distanceY > 1) return false;
                // around 8% bonus from strength - unarmed
                damage = Dice.Roll(1, (strength + strengthBonus) / 12);
                hitChance = skills[SkillType.Hand].Level;
            }
            damage = damage <= 0 ? 1 : damage;
            hitChance += 50;

            // TODO - CustomItemValue_Attack bonus - manufactured items

            // 20% damage bonuses for hunt maps (2ndmiddle, middleland, icebound by default)
            if (map.IsHuntZone) damage += damage / 5;

            // hero armour bonus
            if (IsHero) { damage += 5; hitChance += 100; }

            if (Weapon != null)
                switch (Weapon.SpecialAbilityType)
                {
                    case ItemSpecialAbilityType.Kloness: break; // TODO kloness weapons
                    case ItemSpecialAbilityType.Light: if (CurrentMap.TimeOfDay == TimeOfDay.Day) damage += 7; break;
                    case ItemSpecialAbilityType.Dark: if (CurrentMap.TimeOfDay == TimeOfDay.Night || CurrentMap.TimeOfDay == TimeOfDay.NightChristmas) damage += 7; break;
                }

            if (Necklace != null)
                switch (Necklace.SpecialAbilityType)
                {
                    case ItemSpecialAbilityType.Kloness: break; // TODO necky
                }

            // critical bonus (414 at max level :  180 * (1.8 + 0.5))
            if (isCritical)
            {
                damage += (int)(((double)damage * ((double)level / 100.0f)) + 0.5f);

                // weapon type critical bonuses
                if (Weapon != null)
                    switch (Weapon.RelatedSkill)
                    {
                        case SkillType.Archery: damage += (damage / 10); hitChance += 30; break;
                        case SkillType.ShortSword: damage *= 2; break;
                        case SkillType.LongSword: damage += (damage / 10); hitChance += 30; break;
                        case SkillType.Axe: damage += (damage / 5); break;
                        case SkillType.Hammer: damage += (damage / 5); hitChance += 20; break;
                        case SkillType.Staff: damage += (damage / 5); hitChance += 50; break;
                    }

                hitChance += 100;
                // TODO - hitchance bonus from CustomItemValue_Attack - manufactured items
                criticals--;
            }

            // dash bonus
            if (isDash)
            {
                // weapon type dashing bonuses
                if (Weapon != null)
                    switch (Weapon.RelatedSkill)
                    {
                        case SkillType.LongSword: damage += (damage / 10); break;
                        case SkillType.Axe: damage += (damage / 5); break;
                        case SkillType.Hammer: damage += (damage / 5); break;
                    }

                hitChance += 20;
            }

            // necklace hitChance bonuses
            hitChance += bonusHitChance;

            if (dexterity + dexterityBonus > 50) hitChance += (dexterity + dexterityBonus) - 50;

            // weather hit chance penalty for bows
            if (Weapon != null && Weapon.RelatedSkill == SkillType.Archery)
            {
                switch (CurrentMap.Weather)
                {
                    case WeatherType.Rain: hitChance -= (hitChance / 20); break;
                    case WeatherType.RainMedium: hitChance -= (hitChance / 10); break;
                    case WeatherType.RainHeavy: hitChance -= (hitChance / 4); break;
                }

                // arrow decrement
                Arrows.Count--;
                if (Arrows.Count <= 0) DepleteItem(ArrowsIndex);
                else Notify(CommandMessageType.NotifyItemCount, ArrowsIndex, Arrows.Count, 0);
            }

            // old code uses target's DR/2 but we dont have scope in EvadeMelee
            // so, this must be the last hitChance modifier to emulate old code
            // alternatively, we could pass in a bool such as isBackHit
            if (direction == target.Direction) hitChance *= 2;
            if (target.EvadeMelee(hitChance)) return false;

            // TODO - combobonus (consecutive damage weapon)
            if (IsBerserked && !isCritical) damage *= 2;// berserk damage bonus
            if (bonusPhysicalDamage > 0) damage += bonusPhysicalDamage;// damage bonuses from rings etc
            if (CurrentMap.IsFightZone) damage += (damage / 3);// 30% fightzone damage bonus
            if (world.IsCrusade && crusadeDuty == CrusadeDuty.Fighter) // crusade bonus (100% at level 1-80, 70% at level 80-100, 30% thereafter)
            {
                if (level <= 80) damage *= 2;
                else if (level <= 100) damage = (int)((double)damage * 1.7);
                else damage = (int)((double)damage * 1.3);
            }

            if (Weapon != null)
                switch (Weapon.SpecialWeaponPrimaryType)
                {
                    case ItemSpecialWeaponPrimaryType.Critical: if (isCritical) damage += Weapon.SpecialWeaponPrimaryValue; break; // bugged in old code. if last critical, wont trigger
                    case ItemSpecialWeaponPrimaryType.Poison: break; // TODO
                }

            // damage from critical-damage attribute
            if (Weapon != null && Weapon.SpecialWeaponPrimaryType == ItemSpecialWeaponPrimaryType.Critical) damage += Weapon.SpecialWeaponPrimaryValue;

            // if evade fails, handle absorption and experience gain separately depending on target type
            switch (target.OwnerType)
            {
                case OwnerType.Player:
                    Character character = (Character)target;
                    if (CurrentMap.IsBuilding || CurrentMap.IsSafeMap) return false;
                    if (character.IsAdmin) return false;
                    if (character.Town == OwnerSide.Neutral || town == OwnerSide.Neutral) return false; // cant hurt travs & travs cant hurt you

                    if (HasParty & character.HasParty && party.ID.Equals(character.Party.ID)) return false;

                
                    // righteous weapons - damages negative rep players for each 10 negative rep points. max +10 damage
                    if (Weapon != null && Weapon.SpecialWeaponPrimaryType == ItemSpecialWeaponPrimaryType.Righteous && character.Reputation < 0)
                        damage += Math.Min(Math.Abs(character.Reputation) / 10, 10);

                    // vit damage absorption
                    damage -= Dice.Roll(1, (character.vitality + character.vitalityBonus) / 10) - 1;

                    // bonus PA such as rings and gems etc.
                    if (character.BonusPhysicalAbsorption > 0)
                    {
                        if (character.BonusPhysicalAbsorption >= Globals.MaximumPhysicalAbsorption)
                            damage = damage - (int)(((double)damage / 100.0f) * (double)Globals.MaximumPhysicalAbsorption);
                        else damage = damage - (int)(((double)damage / 100.0f) * (double)character.BonusPhysicalAbsorption);
                    }

                    // endurance damage amount and hammer strip
                    int armourDamage = 1; bool stripAttempt = false;
                    if (Weapon != null)
                    {
                        switch (Weapon.RelatedSkill)
                        {
                            case SkillType.Hammer: armourDamage = 20; stripAttempt = true; break;
                            case SkillType.Axe: armourDamage = 3; break;
                            default: armourDamage = 1; break;
                        }
                        armourDamage += Weapon.StripBonus;
                    }

                    // roll dice to get chance to hit particular item location
                    EquipType hitLocation;
                    int hitLocationChance = Dice.Roll(1, 10000);
                    if ((hitLocationChance >= 5000) && (hitLocationChance < 7500)) hitLocation = EquipType.Legs; // 25%
                    else if ((hitLocationChance >= 7500) && (hitLocationChance < 9000)) hitLocation = EquipType.Arms; // 15%
                    else if ((hitLocationChance >= 9000) && (hitLocationChance <= 10000)) hitLocation = EquipType.Head; // 10%
                    else hitLocation = EquipType.Body; // 50%

                    switch (hitLocation) // handle item location physical absoprtion
                    {
                        case EquipType.Body:
                            if (character.BodyArmour != null)
                            {
                                damage = damage - (int)(((double)damage / 100.0f) * (double)character.BodyArmour.PhysicalAbsorption);
                                TakeItemDamage(armourDamage, BodyArmour, BodyArmourIndex, stripAttempt);
                            }
                            break;
                        case EquipType.Legs:
                            if (character.Leggings != null && character.Boots != null) // counts leggings and boots
                            {
                                int absorption = character.Leggings.PhysicalAbsorption + character.Boots.PhysicalAbsorption;
                                if (absorption > Globals.MaximumPhysicalAbsorption) absorption = Globals.MaximumPhysicalAbsorption;
                                damage = damage - (int)(((double)damage / 100.0f) * (double)absorption);
                                TakeItemDamage(armourDamage, Leggings, LeggingsIndex, stripAttempt);
                                TakeItemDamage(armourDamage, Boots, BootsIndex, stripAttempt);
                            }
                            else if (character.Leggings != null) // if no boots equipped then check only leggings
                            {
                                damage = damage - (int)(((double)damage / 100.0f) * (double)character.Leggings.PhysicalAbsorption);
                                TakeItemDamage(armourDamage, Leggings, LeggingsIndex, stripAttempt);
                            }
                            else if (character.Boots != null) // if no leggings equipped then check only boots
                            {
                                damage = damage - (int)(((double)damage / 100.0f) * (double)character.Boots.PhysicalAbsorption);
                                TakeItemDamage(armourDamage, Boots, BootsIndex, stripAttempt);
                            }
                            break;
                        case EquipType.Arms:
                            if (character.Hauberk != null)
                            {
                                damage = damage - (int)(((double)damage / 100.0f) * (double)character.Hauberk.PhysicalAbsorption);
                                TakeItemDamage(armourDamage, Hauberk, HauberkIndex, stripAttempt);
                            }
                            break;
                        case EquipType.Head:
                            if (character.Helmet != null)
                            {
                                damage = damage - (int)(((double)damage / 100.0f) * (double)character.Helmet.PhysicalAbsorption);
                                TakeItemDamage(armourDamage, Helmet, HelmetIndex, stripAttempt);
                            }
                            break;
                    }

                    // additional absorption from shield
                    if (character.Shield != null)
                        if (Dice.Roll(1, 100) <= character.Skills[SkillType.Shield].Level)
                        {
                            character.Skills[SkillType.Shield].Experience++;

                            if (character.Shield.PhysicalAbsorption >= Globals.MaximumPhysicalAbsorption)
                                damage = damage - (int)(((double)damage / 100.0f) * (double)Globals.MaximumPhysicalAbsorption);
                            else damage = damage - (int)(((double)damage / 100.0f) * (double)character.Shield.PhysicalAbsorption);

                            TakeItemDamage(1, Shield, ShieldIndex);
                        }

                    // defence activations
                    if (character.SpecialAbilityEnabled)
                        switch (character.SpecialAbilityItem.SpecialAbilityType)
                        {
                            case ItemSpecialAbilityType.MerienArmour:
                                if (Weapon != null)
                                {
                                    Weapon.Endurance = 0;
                                    Notify(CommandMessageType.NotifyItemBroken, ((int)Weapon.EquipType), equipment[((int)Weapon.EquipType)]);
                                    UnequipItem(WeaponIndex);
                                }
                                break;
                            case ItemSpecialAbilityType.MerienShield: return false;
                        }

                    // attack activations
                    if (specialAbilityEnabled && Weapon != null)
                        switch (Weapon.SpecialAbilityType)
                        {
                            case ItemSpecialAbilityType.XelimaWeapon: if ((character.HP / 2) > damage) damage = (character.HP / 2); break;
                            case ItemSpecialAbilityType.IceWeapon: character.SetMagicEffect(new Magic(MagicType.Ice, 30)); break;
                            case ItemSpecialAbilityType.MedusaWeapon: character.SetMagicEffect(new Magic(MagicType.Paralyze, 10, 2)); break;
                        }

                    if (isSafeMode)
                        if (target.Side == this.Side) return false;
                        else damage /= 2; // safe mode

                    if (guild == character.Guild) damage /= 2; // same guild

                    // deal the damage and store any experience received
                    if (damage <= 1) damage = 1;
                    experience += character.TakeDamage(this, DamageType.Melee, damage, direction);
                    if (ShowDamageEnabled) world.SendClientMessage(this, string.Format("You did {0} damage to {1}.", damage, character.Name)); // TODO - add hit location to showdmg
                    if (Weapon != null)
                    {
                        switch (Weapon.RelatedSkill)
                        {
                            case SkillType.Archery:
                            case SkillType.Axe:
                            case SkillType.Fencing:
                            case SkillType.Hammer:
                            case SkillType.ShortSword:
                            case SkillType.Staff:
                            case SkillType.LongSword:
                                skills[Weapon.RelatedSkill].Experience++;
                                break;
                        }

                        if (Weapon.SpecialWeaponSecondaryType == ItemSpecialWeaponSecondaryType.Experience)
                            AddExp(experience + ((experience / 100) * (Weapon.SpecialWeaponSecondaryValue * 10)));
                        else AddExp(experience);
                    }
                    else
                    {
                        skills[SkillType.Hand].Experience++;
                        AddExp(experience);
                    }
                    return true;
                case OwnerType.Npc:
                    Npc npc = (Npc)target;

                    if (npc.IsFriendly) return false;

                    if (world.IsCrusade)
                    {
                        if ((Side == OwnerSide.Neutral) || (Side == npc.Side && (npc.NpcType == NpcType.CrusadeEnergyShield || npc.NpcType == NpcType.CrusadeGrandMagicGenerator)))
                            return false; // cant hit ES or GMG if traveller or own side
                        // TODO crusade mode - hitting grand magic generator (type = 41) reduces mana stock. 500 hit points before mana stock down
                    }

                    // damage absorption
                    if (npc.PhysicalAbsorption > 0) damage = damage - (int)(((double)damage / 100.0f) * (double)npc.PhysicalAbsorption);

                    // TODO - perhaps "IsDemon" in config?
                    if (npc.Name.Equals("Demon") && Weapon != null &&
                        Weapon.SpecialAbilityType == ItemSpecialAbilityType.DemonSlayer)
                            damage += Dice.Roll(3, 2); // max +6 damage

                    // deal the damage and store any experience received
                    if (damage <= 0) damage = 0;
                    experience += npc.TakeDamage(this, DamageType.Melee, damage, direction);
                    if (ShowDamageEnabled) world.SendClientMessage(this, string.Format("You did {0} damage to {1}.", damage, npc.Name));
                    if (Weapon != null)
                    {
                        switch (Weapon.RelatedSkill)
                        {
                            case SkillType.Archery:
                            case SkillType.Axe:
                            case SkillType.Fencing:
                            case SkillType.Hammer:
                            case SkillType.ShortSword:
                            case SkillType.Staff:
                            case SkillType.LongSword:
                                skills[Weapon.RelatedSkill].Experience++;
                                break;
                        }

                        if (Weapon.SpecialWeaponSecondaryType == ItemSpecialWeaponSecondaryType.Experience)
                            AddExp(experience + ((experience / 100) * (Weapon.SpecialWeaponSecondaryValue * 10)));
                        else AddExp(experience);
                    }
                    else
                    {
                        skills[SkillType.Hand].Experience++;
                        AddExp(experience);
                    }
                    return true;
                default: return false;
            }
        }

        /// <summary>
        /// Character performs a mining action.
        /// </summary>
        /// <param name="sourceX">X coordinate of mining operation.</param>
        /// <param name="sourceY">Y coordinate of mining operation.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <param name="targetX">X coordinate of the target rock or gem.</param>
        /// <param name="targetY">Y coordinate of the target rock or gem.</param>
        /// <param name="target">Target rock or gem.</param>
        /// <returns>True or False to indicate a mineral was successfully mined.</returns>
        public bool Mine(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, Mineral target)
        {
            if (isDead) return false;
            if (!IsCombatMode) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);

            this.direction = direction; // should be set even if attack fails. to update nearby clients

            if (target == null) return false;
            if (target.RemainingMaterials <= 0) return false;
            if (targetX != target.X || targetY != target.Y) return false; // target coordinates to not match the target object's location

            int skillLevel = skills[SkillType.Mining].Level;
            skillLevel -= target.Difficulty; // reduces skill by difficulty
            if (skillLevel <= 0) skillLevel = 1;

            int result = Dice.Roll(1, 100);
            if (result < skillLevel)
            {
                Item mineral = target.Mine();
                if (mineral != null)
                {
                    // TODO - experience gain
                    if (CurrentLocation.SetItem(mineral)) ItemDropped(this, mineral);

                    skills[SkillType.Mining].Experience++;
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Character performs a mining action.
        /// </summary>
        /// <param name="sourceX">X coordinate of mining operation.</param>
        /// <param name="sourceY">Y coordinate of mining operation.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <param name="targetX">X coordinate of the target rock or gem.</param>
        /// <param name="targetY">Y coordinate of the target rock or gem.</param>
        /// <param name="target">Target rock or gem.</param>
        /// <returns>True or False to indicate a mineral was successfully mined.</returns>
        public bool Farm(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, Npc target)
        {
            if (!IsCombatMode) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);

            this.direction = direction; // should be set even if attack fails. to update nearby clients

            if (Weapon != null && Weapon.RelatedSkill != SkillType.Farming) return false;
            if (level < 20) return false; // travs cant farm
            if (target == null) return false;
            if (target.BuildPoints <= 0) return false;
            if (targetX != target.X || targetY != target.Y) return false;

            int skillLevel = skills[SkillType.Farming].Level;
            if (skillLevel < 20) return false; // buy manual first for 20%

            int result = Dice.Roll(1, 100);
            switch (target.BuildPoints)
            {
                case 1:
                case 8:
                case 18:
                    if (target.BuildPoints == 18)
                         target.Appearance2 = target.BuildType << 8 | 2; // get bigger
                    else target.Appearance2 = target.BuildType << 8 | 3; // and bigger
                    if (skillLevel <= target.BuildLimit + 10) 
                        skills[SkillType.Farming].Experience++;
                    if (Globals.FarmingDropTable[Math.Max(Math.Min((skillLevel - 20) / 10, 8), 0), Math.Max(Math.Min((target.BuildLimit - 20) / 10, 8), 0)] >= result)
                    {
                        Item vegetable = null; // this object is as clever as the original programmers
                        switch (target.BuildType)
                        {
                            case 1: vegetable = World.ItemConfiguration["WaterMelon"].Copy(); break;
                            case 2: vegetable = World.ItemConfiguration["Pumpkin"].Copy(); break;
                            case 3: vegetable = World.ItemConfiguration["Garlic"].Copy(); break;
                            case 4: vegetable = World.ItemConfiguration["Barley"].Copy(); break;
                            case 5: vegetable = World.ItemConfiguration["Carrot"].Copy(); break;
                            case 6: vegetable = World.ItemConfiguration["Radish"].Copy(); break;
                            case 7: vegetable = World.ItemConfiguration["Corn"].Copy(); break;
                            case 8: vegetable = World.ItemConfiguration["ChineseBellFlower"].Copy(); break;
                            case 9: vegetable = World.ItemConfiguration["Melone"].Copy(); break;
                            case 10: vegetable = World.ItemConfiguration["Tomato"].Copy(); break;
                            case 11: vegetable = World.ItemConfiguration["Grapes"].Copy(); break;
                            case 12: vegetable = World.ItemConfiguration["BlueGrapes"].Copy(); break;
                            case 13: vegetable = World.ItemConfiguration["Mushroom"].Copy(); break;
                            case 14: vegetable = World.ItemConfiguration["Ginseng"].Copy(); break;
                        }

                        if (vegetable != null)
                        {
                            // TODO - experience gain
                            if (CurrentLocation.SetItem(vegetable)) ItemDropped(this, vegetable);
                        }
                    }
                    break;
                default: break;
            }

            if (Globals.FarmingSkillTable[Math.Max(Math.Min((skillLevel - 20) / 10, 8), 0), Math.Max(Math.Min((target.BuildLimit - 20) / 10, 8), 0)] >= result
                        || target.BuildPoints == 1 || target.BuildPoints == 8 || target.BuildPoints == 18)
            {
                if (Weapon != null) TakeItemDamage(1, Weapon, WeaponIndex);

                target.BuildPoints--;
                if (target.BuildPoints < 0) target.BuildPoints = 0;
            }

            if (target.BuildPoints <= 1) target.Die(this, -1, false); // this is bugged in old code. you wouldnt gain skill % because the Npc would no longer exist after item drop. so I have moved it to the bottom

            return true;
        }

        /// <summary>
        /// Character performs a building action on structures such as those in crusade.
        /// </summary>
        /// <param name="sourceX">X coordinate of character.</param>
        /// <param name="sourceY">Y coordinate of character.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <param name="targetX">X coordinate of the target structure.</param>
        /// <param name="targetY">Y coordinate of the target structure.</param>
        /// <param name="target">Target structure.</param>
        /// <returns>True or False to indicate a building was successfully built.</returns>
        public bool Build(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, Npc target)
        {
            if (!IsCombatMode) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);

            this.direction = direction; // should be set even if attack fails. to update nearby clients

            if (Weapon != null && Weapon.RelatedSkill != SkillType.Mining) return false;
            if (level < 20) return false; // travs cant build
            if (target == null) return false;
            if (target.BuildPoints <= 0) return false;
            if (targetX != target.X || targetY != target.Y) return false;
            if (crusadeDuty != CrusadeDuty.Constructor && adminLevel == 0) return false;

            switch (target.NpcType)
            {
                case NpcType.CrusadeArrowTower:
                case NpcType.CrusadeCannonTower:
                case NpcType.CrusadeDetector:
                case NpcType.CrusadeManaCollector:
                    switch (target.BuildPoints)
                    {
                        case 1:
                            target.Appearance2 = 0;
                            switch (target.NpcType)
                            {
                                case NpcType.CrusadeArrowTower: crusadeConstructionPoints += 700; crusadeWarContribution += 700; break;
                                case NpcType.CrusadeCannonTower: crusadeConstructionPoints += 700; crusadeWarContribution += 700; break;
                                case NpcType.CrusadeDetector: crusadeConstructionPoints += 500; crusadeWarContribution += 500; break;
                                case NpcType.CrusadeManaCollector: crusadeConstructionPoints += 500; crusadeWarContribution += 500; break;
                            }
                            Notify(CommandMessageType.NotifyCrusadeStatistics, crusadeConstructionPoints, crusadeWarContribution, 0);

                            break;
                        case 5: target.Appearance2 = 1; break;
                        case 10: target.Appearance2 = 2; break;
                    }
                    break;
            }

            target.BuildPoints--;
            if (target.BuildPoints < 0) target.BuildPoints = 0;

            return true;
        }

        /// <summary>
        /// Character performs a manual Fishing action when fishing a dynamic object after clicking "Try Now" in the client.
        /// </summary>
        public void Fish()
        {
            if (isDead) return;
            if (targetFish == null) return;

            int result = Dice.Roll(1, 100);
            if (targetFishChance >= result)
            {
                experienceStored += Dice.Roll(targetFish.Difficulty, 6);
                skills[SkillType.Fishing].Experience += targetFish.Difficulty;

                if (targetFish.FishItem != null)
                {
                    if (CurrentLocation.SetItem(targetFish.FishItem)) ItemDropped(this, targetFish.FishItem);
                }
                targetFish.Remove();
                Notify(CommandMessageType.NotifyFishingSuccess);
            }
            else Notify(CommandMessageType.NotifyFishingFailed);
            skillInUse = SkillType.None;
            targetFish = null;
        }

        /// <summary>
        /// Character performs a manufacturing action.
        /// </summary>
        /// <param name="itemName">Name of the item to manufacture.</param>
        /// <param name="itemIndex">List of item indexes used in the manufacturing process.</param>
        public void Manufacture(string itemName, int[] itemIndex)
        {
            if (isDead) return;

            if (!World.ItemConfiguration.ContainsKey(itemName) ||
                !World.ManufacturingConfiguration.ContainsKey(itemName) ||
                 World.ManufacturingConfiguration[itemName].SkillLimit > skills[SkillType.Manufacturing].Level)
            {
                Notify(CommandMessageType.NotifyManufactureFailed);
                return;
            }

            int totalIngrediants = World.ManufacturingConfiguration[itemName].Ingredients.Count;
            int foundIngrediants = 0, purityValue = 0;

            // check that all ingrediants are available
            foreach (KeyValuePair<string, int> ingrediant in World.ManufacturingConfiguration[itemName].Ingredients)
                if (World.ItemConfiguration.ContainsKey(ingrediant.Key))
                    for (int i = 0; i < 6; i++)
                        if (itemIndex[i] != -1 && inventory[itemIndex[i]] != null && inventory[itemIndex[i]].Name.Equals(World.ItemConfiguration[ingrediant.Key].Name))
                            if (inventory[itemIndex[i]].IsStackable)
                            {
                                if (inventory[itemIndex[i]].Count >= ingrediant.Value)
                                {
                                    foundIngrediants++;
                                    purityValue += inventory[itemIndex[i]].SpecialEffect2 - (inventory[itemIndex[i]].SpecialEffect2 - skills[SkillType.Manufacturing].Level) / 2;
                                }
                            }
                            else
                            {
                                
                            }


            // not all ingrediants found
            if (foundIngrediants < totalIngrediants)
            {
                Notify(CommandMessageType.NotifyManufactureFailed);
                return;
            }

            // all ingrediants found perform manufacture of item based on skill %
            int result = Dice.Roll(1, 100);
            if (result > skills[SkillType.Manufacturing].Level)
            {
                Notify(CommandMessageType.NotifyManufactureFailed);
                return;
            }
            else
            {
                Item item = World.ItemConfiguration[itemName].Copy();
                item.Attribute = (item.Attribute & 0xFFFFFFFE) | 0x00000001; // "custom item"

                if (item.Type == ItemType.Material)
                    item.SpecialEffect2 = (skills[SkillType.Manufacturing].Level / 2) + (Dice.Roll(1, (skills[SkillType.Manufacturing].Level / 2) + 1) - 1);
                else
                {
                    item.Attribute = (item.Attribute & 0x0000FFFF) | (World.ItemConfiguration[itemName].Attribute << 16);

                    int resultValue = purityValue - World.ManufacturingConfiguration[itemName].Difficulty;
                    resultValue = Math.Min(Math.Max(0, resultValue), 200); // must be 0 - 200

                    item.SpecialEffect2 = (int)(((double)resultValue / (double)(100 - World.ManufacturingConfiguration[itemName].Difficulty)) * 100.0f);

                    int itemEndurance = item.MaximumEndurance + (int)((double)((double)item.SpecialEffect2 / 100.0f) * (double)item.MaximumEndurance);
                    itemEndurance = Math.Max(1, itemEndurance); // must be > 0

                    if (itemEndurance <= item.MaximumEndurance * 2)
                    {
                        item.MaximumEndurance = itemEndurance;
                        item.SpecialEffect1 = itemEndurance;
                        item.Endurance = itemEndurance;
                    }
                    else item.SpecialEffect1 = item.MaximumEndurance;

                    item.Colour = 2;

                    AddInventoryItem(item);

                    Notify(CommandMessageType.NotifyManufactureSuccess, (item.SpecialEffect2 >= 0) ? item.SpecialEffect2 : item.SpecialEffect2+10000, (int)item.Type);

                    skills[SkillType.Manufacturing].Experience++;

                    // TODO experience
                }
            }

            // remove ingrediants from inventory and check for depleted items TODO cleanup
            foreach (KeyValuePair<string, int> ingrediant in World.ManufacturingConfiguration[itemName].Ingredients)
                if (World.ItemConfiguration.ContainsKey(ingrediant.Key))
                    for (int i = 0; i < 6; i++)
                        if (itemIndex[i] != -1 && inventory[itemIndex[i]] != null &&
                            inventory[itemIndex[i]].Name.Equals(World.ItemConfiguration[ingrediant.Key].Name))
                        {
                            inventory[itemIndex[i]].Count -= ingrediant.Value;
                            if (inventory[itemIndex[i]].Count <= 0) DepleteItem(itemIndex[i]);
                            else Notify(CommandMessageType.NotifyItemCount, itemIndex[i], inventory[itemIndex[i]].Count, 0);
                        }
        }

        /// <summary>
        /// Character performs an alchemy action.
        /// </summary>
        /// <param name="itemIndex">List of item indexes used in the alchemy process.</param>
        public void Alchemy(int[] itemIndex)
        {

        }

        /// <summary>
        /// Character performs a crafting action.
        /// </summary>
        /// <param name="itemIndex">List of item indexes used in the crafting process.</param>
        public void Craft(int[] itemIndex)
        {
            if (isDead) return;
        }

        /// <summary>
        /// Character performs an slate action.
        /// </summary>
        /// <param name="itemIndex">List of item indexes used in the slate process.</param>
        public void Slate(int[] itemIndex)
        {
            if (isDead) return;
        }

        public void TakeArmourDamage(int damage)
        {
            if (BodyArmour != null) TakeItemDamage(damage, BodyArmour, BodyArmourIndex);
            if (Leggings != null) TakeItemDamage(damage, Leggings, LeggingsIndex);
            if (Boots != null) TakeItemDamage(damage, Boots, BootsIndex);
            if (Hauberk != null) TakeItemDamage(damage, Hauberk, HauberkIndex);
            if (Helmet != null) TakeItemDamage(damage, Helmet, HelmetIndex);
        }

        public void TakeItemDamage(int damage, Item item, int itemIndex, bool isStripAttempt = false)
        {
            item.Endurance -= damage;
            if (item.Endurance <= 0)
            {
                Notify(CommandMessageType.NotifyItemBroken, ((int)item.EquipType), equipment[((int)item.EquipType)]);
                UnequipItem(itemIndex);
            }
            else if (isStripAttempt && item.MaximumEndurance < Globals.MaximumEnduranceStripable)
            {
                int chance = Dice.Roll(1, 100);
                if (chance >= (int)(((double)item.Endurance / (double)item.MaximumEndurance) * 100.0f))
                {
                    int stripChance = Dice.Roll(1, 100);
                    if (stripChance < Math.Min(damage, Globals.MaximumStripChance)) UnequipItem(itemIndex);
                }
            }
        }

        public int TakeDamage(DamageType type, int damage, MotionDirection flyDirection) { return TakeDamage(null, type, damage, flyDirection, true); }
        public int TakeDamage(DamageType type, int damage, MotionDirection flyDirection, bool notify) { return TakeDamage(null, type, damage, flyDirection, notify); }
        public int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection) { return TakeDamage(attacker, type, damage, flyDirection, true); }
        public int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection, bool notify)
        {
            DepleteHP(damage);

            if (!(type == DamageType.Environment))
            {
                ChargeCritical();

                // remove paralyze for different damage types
                if (magicEffects.ContainsKey(MagicType.Paralyze))
                    switch (type)
                    {
                        case DamageType.Magic: if (magicEffects[MagicType.Paralyze].Magic.Effect1 == 2) RemoveMagicEffect(MagicType.Paralyze); break;
                        case DamageType.Ranged:
                        case DamageType.Melee: if (magicEffects[MagicType.Paralyze].Magic.Effect1 == 1) RemoveMagicEffect(MagicType.Paralyze); break;
                        case DamageType.Environment: RemoveMagicEffect(MagicType.Paralyze); break;
                    }
            }

            if (hp <= 0)
            {
                if (luck > 0 && Dice.Roll(1, 100) < luck)
                {
                    world.SendClientMessage(this, "You were lucky to survive a fatal blow!");
                    hp = 1;
                }
                else
                {
                    Die(attacker, damage, !(type == DamageType.Environment)); // if environmental, dont drop items
                    return 0;
                }
            }

            Notify(CommandMessageType.NotifyHP, hp, mp);
            lastDamage = damage;
            if (notify) DamageTaken(this, type, damage, flyDirection);
            // TODO how do we calculate exp?
            return 0;
        }

        public bool Fly(int sourceX, int sourceY, MotionDirection direction)
        {
            if (isDead) return false;

            if (!Enum.IsDefined(typeof(MotionDirection), direction)) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;

            int destinationX = mapX, destinationY = mapY;
            switch (direction)
            {
                case MotionDirection.North: destinationY = destinationY - 1; break;
                case MotionDirection.NorthEast: destinationX = destinationX + 1; destinationY = destinationY - 1; break;
                case MotionDirection.East: destinationX = destinationX + 1; break;
                case MotionDirection.SouthEast: destinationX = destinationX + 1; destinationY = destinationY + 1; break;
                case MotionDirection.South: destinationY = destinationY + 1; break;
                case MotionDirection.SouthWest: destinationX = destinationX - 1; destinationY = destinationY + 1; break;
                case MotionDirection.West: destinationX = destinationX - 1; break;
                case MotionDirection.NorthWest: destinationX = destinationX - 1; destinationY = destinationY - 1; break;
            }

            if (!map[destinationY][destinationX].IsMoveable) return false;
            if (destinationX < 0 || destinationX > map.Width || destinationY < 0 || destinationY > map.Height) return false; // map boundaries

            if (!map[destinationY][destinationX].IsBlocked)
            {
                if (world.IsApocalypse)
                {
                    Portal dynamicPortal = null;
                    if (world.Apocalypse.HasPortal(new Location(map.Name, destinationX, destinationY), out dynamicPortal))
                    {
                        Teleport(dynamicPortal.PortalTargetLocation, MotionDirection.South);
                        return true;
                    }
                }

                map[mapY][mapX].ClearOwner();
                this.direction = direction;
                prevMapX = mapX;
                prevMapY = mapY;
                mapX = destinationX;
                mapY = destinationY;
                map[destinationY][destinationX].SetOwner(this);

                return true;
            }
            else return false;
        }

        /// <summary>
        /// Reduces the character's mana and notifys the client of changes.
        /// </summary>
        /// <param name="mana">Amount of mana to deplete.</param>
        public void DepleteMP(int points)
        {
            this.mp -= points;
            if (mp < 0) mp = 0;
            Notify(CommandMessageType.NotifyMP, mp);
        }
        public void ReplenishMP(int points)
        {
            mp += points;
            if (mp > MaxMP) mp = MaxMP;
            Notify(CommandMessageType.NotifyMP, mp);
        }
        public void DepleteHP(int points)
        {
            hp -= points;
            if (hp < 0) hp = 0;
            Notify(CommandMessageType.NotifyHP, hp, mp);
        }
        public void ReplenishHP(int points)
        {
            hp += points;
            if (hp > MaxHP) hp = MaxHP;
            Notify(CommandMessageType.NotifyHP, hp, mp);
        }
        public void DepleteSP(int points)
        {
            sp -= points;
            if (sp < 0) sp = 0;
            Notify(CommandMessageType.NotifySP, sp);
        }
        public void ReplenishSP(int points)
        {
            sp += points;
            if (sp > MaxSP) sp = MaxSP;
            Notify(CommandMessageType.NotifySP, sp);
        }

        public void AddExp(int points, bool fromParty = false)
        {
            if (!fromParty)
            {
                // farm maps. over lvl 100 gets 10% exp. under lvl 100 gets 125% exp
                if (map.Name.Equals(Globals.ElvineFarmName) || map.Name.Equals(Globals.AresdenFarmName))
                    if (level >= 100) points /= 10; // TODO cleanup hardcoded level and exp modifier
                    else points = (int)((double)points * 1.25);
            }

            if (!fromParty && HasParty && points > 10)
            {
                for (int p = 0; p < Party.MemberCount; p++)
                    if (world.Players.ContainsKey(Party.Members[p]))
                        if (world.Players[Party.Members[p]].IsWithinRange(this, 20, 20)) // members must be close
                            world.Players[Party.Members[p]].AddExp((points / party.MemberCount) + 5, true);
                // TODO check same IP here to stop lame leveling
            }
            else
            {
                if ((status & 0x10000) != 0) points *= 3; // EXP slate
                experienceStored += points;
            }
        }

        public void EatFood(int points)
        {
            hunger += points;
            if (hunger > 100) hunger = 100;

            hpStock += points;
            if (hpStock > 500) hpStock = 500;
        }

        public void Die(IOwner killer, int damage, bool dropLoot)
        {
            if (isDead) return; // avoid die-ception

            isDead = true;
            map[mapY][mapX].ClearOwner();
            map[mapY][mapX].SetDeadOwner(this);

            for (int i = 0; i < 50; i++)
                if (MagicEffects.ContainsKey((MagicType)i))
                    RemoveMagicEffect((MagicType)i);

            Notify(CommandMessageType.NotifyDead, killer.Name);

            if (StatusChanged != null) StatusChanged(this);

            Killed(this, killer, damage);

            if (dropLoot) DropLoot();
        }

        /// <summary>
        /// Character is restarted after death, resetting Hp/Mp/Sp etc.
        /// </summary>
        public void Restart()
        {
            if (map[mapY][mapX].DeadOwner == this) map[mapY][mapX].ClearDeadOwner();
            isDead = false;
            hp = MaxHP;
            mp = MaxMP;
            sp = MaxSP;
            hunger = 100;
            if (IsCombatMode) ToggleCombatMode();
            direction = MotionDirection.South;

            switch (town)
            {
                case OwnerSide.Neutral: Teleport(world.Maps[Globals.TravellerRevivalZone], -1, -1); break;
                case OwnerSide.Aresden: Teleport(world.Maps[Globals.AresdenRevivalZone], -1, -1); break;
                case OwnerSide.Elvine: Teleport(world.Maps[Globals.ElvineRevivalZone], -1, -1); break;
            }
        }

        /// <summary>
        /// Character performs a Pick-Up to collect items at it's location.
        /// </summary>
        /// <param name="sourceX">X coordinate of pickup operation.</param>
        /// <param name="sourceY">Y coordinate of pickup operation.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <returns>True or False to indicate if Pick Up has failed or not. Reasons for failure include the tile not having items or inventory is full.</returns>
        public bool PickUp(int sourceX, int sourceY, MotionDirection direction)
        {
            Item nextItem = null;
            if (isDead) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (map[mapY][mapX].Items.Count <= 0) return false;
            if (InventoryCount >= Globals.MaximumInventoryItems)
            {
                Notify(CommandMessageType.NotifyInventoryFull);
                return false;
            }

            Item retrievedItem = map[mapY][mapX].Items.RemoveItem();

            if (!AddInventoryItem(retrievedItem))
                map[mapY][mapX].Items.AddItem(retrievedItem); // stick it back on
            else
            {

                if (map[mapY][mapX].Items.Count <= 0)
                    nextItem = Item.Empty();
                else
                {
                    Item next = map[mapY][mapX].Items.CheckItem();
                    if (next == null)
                        nextItem = Item.Empty();
                    else nextItem = next;
                }
            }

            ItemPickedUp(this, nextItem);

            return true;
        }

        /// <summary>
        /// Adds an item to the inventory.
        /// </summary>
        /// <param name="item">Item object to add.</param>
        /// <returns>True or False indicating that the item was successfully added or not.</returns>
        public bool AddInventoryItem(Item item) { int index; return AddInventoryItem(item, out index, true); }
        /// <summary>
        /// Adds an item to the inventory.
        /// </summary>
        /// <param name="item">Item object to add.</param>
        /// <param name="itemIndex">Outputs the index in the inventory that the new item is located.</param>
        /// <param name="notify">Specifies whether to notify the client or not.</param>
        /// <returns>True or False indicating that the item was successfully added or not.</returns>
        public bool AddInventoryItem(Item item, out int itemIndex, bool notify)
        {
            itemIndex = -1;
            if (item == null) return false;
            if (InventoryCount >= Globals.MaximumInventoryItems)
            {
                Notify(CommandMessageType.NotifyInventoryFull);
                return false;
            }

            if (item.IsStackable && inventory.Contains(item))
            {
                for (int i = 0; i < Globals.MaximumInventoryItems; i++)
                    if (inventory[i] != null && inventory[i].Equals(item))
                    {
                        inventory[i].Count += item.Count;
                        itemIndex = i;
                    }
            }
            else
            {
                bool added = false;
                for (int i = 0; i < Globals.MaximumInventoryItems; i++)
                    if (inventory[i] == null)
                    {
                        inventory[i] = item;
                        itemIndex = i;
                        added = true;
                        break;
                    }
                if (!added) inventory.Add(item); // something wrong here?
            }

            if (notify) Notify(CommandMessageType.NotifyItemObtained, item.GetObtainedData());
            return true;
        }

        public bool AddWarehouseItem(Item item) { int index; return AddWarehouseItem(item, 1, out index); }
        public bool AddWarehouseItem(Item item, int count) { int index; return AddWarehouseItem(item, count, out index); }
        public bool AddWarehouseItem(Item item, out int itemIndex) { return AddWarehouseItem(item, 1, out itemIndex); }
        public bool AddWarehouseItem(Item item, int count, out int itemIndex)
        {
            itemIndex = -1;
            if (item == null) return false;
            if (WarehouseCount >= Globals.MaximumWarehouseItems) return false;

            bool added = false;
            if (item.IsStackable)
            {
                for (int i = 0; i < Globals.MaximumWarehouseItems; i++)
                    if (warehouse[i] != null && warehouse[i].Equals(item)) // find existing
                    {
                        if (count >= item.Count)
                            warehouse[i].Count += item.Count;
                        else warehouse[i].Count += count;
                        added = true;
                        break;
                    }
                if (!added)
                {
                    for (int i = 0; i < Globals.MaximumWarehouseItems; i++)
                        if (warehouse[i] == null)
                        {
                            warehouse[i] = item;
                            itemIndex = i;
                            added = true;
                            break;
                        }
                    if (!added) warehouse.Add(item); // something wrong?
                }
            }
            else
            {
                for (int i = 0; i < Globals.MaximumWarehouseItems; i++)
                    if (warehouse[i] == null)
                    {
                        warehouse[i] = item;
                        itemIndex = i;
                        added = true;
                        break;
                    }
                if (!added) warehouse.Add(item); // something wrong?
            }

            return true;
        }

        public bool Idle(int sourceX, int sourceY, MotionDirection direction)
        {
            if (isDead) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;

            this.direction = direction;

            map[mapY][mapX].ClearOwner();
            map[sourceY][sourceX].SetOwner(this);

            return true;
        }

        /// <summary>
        /// Moves this character on the Map in the direction specified.
        /// </summary>
        /// <param name="sourceX">Player X Coordinate on the Map. Used to verify Client and Server coordinates are synchronized.</param>
        /// <param name="sourceY">Player Y Coordinate on the Map. Used to verify Client and Server coordinates are synchronized.</param>
        /// <param name="direction">Direction in which the character is to move.</param>
        /// <returns>True or False to indicate success or failure of this action.</returns>
        public bool Run(int sourceX, int sourceY, MotionDirection direction)
        {
            bool ret = Move(sourceX, sourceY, direction);

            if (ret) DepleteSP(1); // reduce staminar when running

            return ret;
        }

        /// <summary>
        /// Moves this character on the Map in the direction specified.
        /// </summary>
        /// <param name="sourceX">Player X Coordinate on the Map. Used to verify Client and Server coordinates are synchronized.</param>
        /// <param name="sourceY">Player Y Coordinate on the Map. Used to verify Client and Server coordinates are synchronized.</param>
        /// <param name="direction">Direction in which the character is to move.</param>
        /// <returns>True or False to indicate success or failure of this action.</returns>
        public bool Move(int sourceX, int sourceY, MotionDirection direction)
        {
            if (isDead) return false;
            if (!Enum.IsDefined(typeof(MotionDirection), direction)) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (magicEffects.ContainsKey(MagicType.Paralyze)) return false;
            //if (!IsWithinRange(this, 1, 1)) return false; // cant move more than 1 cell away at a time

            int destinationX = sourceX, destinationY = sourceY;
            switch (direction)
            {
                case MotionDirection.North: destinationY = sourceY - 1; break;
                case MotionDirection.NorthEast: destinationX = sourceX + 1; destinationY = sourceY - 1; break;
                case MotionDirection.East: destinationX = sourceX + 1; break;
                case MotionDirection.SouthEast: destinationX = sourceX + 1; destinationY = sourceY + 1; break;
                case MotionDirection.South: destinationY = sourceY + 1; break;
                case MotionDirection.SouthWest: destinationX = sourceX - 1; destinationY = sourceY + 1; break;
                case MotionDirection.West: destinationX = sourceX - 1; break;
                case MotionDirection.NorthWest: destinationX = sourceX - 1; destinationY = sourceY - 1; break;
            }

            if (!map[destinationY][destinationX].IsMoveable) return false;
            if (destinationX < 0 || destinationX > map.Width || destinationY < 0 || destinationY > map.Height) return false; // map boundaries

            if (!map[destinationY][destinationX].IsBlocked)
            {
                if (world.IsApocalypse)
                {
                    Portal dynamicPortal = null;
                    if (world.Apocalypse.HasPortal(new Location(map.Name, destinationX, destinationY), out dynamicPortal))
                    {
                        Teleport(dynamicPortal.PortalTargetLocation, MotionDirection.South);
                        return true;
                    }
                }

                map[mapY][mapX].ClearOwner();
                this.direction = direction;
                prevMapX = mapX;
                prevMapY = mapY;
                mapX = destinationX;
                mapY = destinationY;
                map[destinationY][destinationX].SetOwner(this);

                // spike field
                if (map[destinationY][destinationX].IsDynamicOccupied &&
                    map[destinationY][destinationX].DynamicObject.Type == DynamicObjectType.SpikeField)
                        TakeDamage(DamageType.Environment, ((Spikes)map[destinationY][destinationX].DynamicObject).Damage.Roll(), MotionDirection.None);

                return true;
            }
            else return false;
        }

        public void GetKillReward(Character target)
        {
            if (target == this) return; // no reward for killing self

            // TODO - map no penalty
            if (target.Side == this.Side || target.Side == OwnerSide.Neutral)
            {
                if (target.IsCriminal)
                {
                    target.CriminalCount--;
                    target.Notify(CommandMessageType.NotifyCriminalCount, target.CriminalCount);

                    if (!IsCriminal) //
                    {
                        rewardGold += target.Level * 3;
                        Notify(CommandMessageType.NotifyCriminalCaptured, target.CriminalCount, target.Level, target.Name);
                    }
                }
                else
                {
                    criminalCount++;
                    Notify(CommandMessageType.NotifyCriminalCount, criminalCount);
                    if (StatusChanged != null) StatusChanged(this);
                    reputation = Math.Min(Math.Max(reputation - 10, Globals.MinimumReputation), Globals.MaximumReputation);
                    // TODO send to jail, lock map
                }
            }
            else
            {
                if (world.IsCrusade)
                {
                    // TODO crusade reward - move to Npc.GetKillReward, give const pt to summoner
                }

                enemyKills += Globals.EnemyKillModifier;
                Notify(CommandMessageType.NotifyEnemyKills, enemyKills);

                experienceStored += (Dice.Roll(3, (3*target.Level)) + target.Level) /3;

                rewardGold += Dice.Roll(1, target.Level);
            }
            Notify(CommandMessageType.NotifyEnemyKillReward, target);
        }

        public void GetHeldenianFlag()
        {
            if (!world.IsHeldenianSeige) return;
            if (enemyKills < 10) return;

            switch (town)
            {
                case OwnerSide.Aresden: AddInventoryItem(World.ItemConfiguration["AresdenFlag"].Copy()); break;
                case OwnerSide.Elvine: AddInventoryItem(World.ItemConfiguration["ElvineFlag"].Copy()); break;
                default: return;
            }

            enemyKills -= 10;
            Notify(CommandMessageType.NotifyEnemyKills, enemyKills);
        }

        public void SummonCrusadeUnit(int type, int count, int x, int y)
        {
            if (map.IsBuilding || CurrentMap.IsSafeMap || map.IsAttackEnabled == false) return;
            if (!world.IsCrusade) return;
            if (guild.CrusadeStructures.Count >= World.CrusadeConfiguration.StructuresPerGuild)
            {
                Notify(CommandMessageType.NotifyCrusadeStructureLimitReached);
                return;
            }

            // todo check sufficient construction points
            // todo check guild construct point within 10 cells

            Npc summon = null;
            NpcType npcType = NpcType.None;
            if (Enum.TryParse<NpcType>(type.ToString(), out npcType))
                switch (npcType)
                {
                    case NpcType.CrusadeDetector:
                        switch (town)
                        {
                            case OwnerSide.Aresden: summon = World.NpcConfiguration["DT-Aresden"].Copy(); break;
                            case OwnerSide.Elvine: summon = World.NpcConfiguration["DT-Elvine"].Copy(); break;
                        }
                        summon.BuildPoints = 50;
                        break;
                    case NpcType.CrusadeManaCollector:
                        switch (town)
                        {
                            case OwnerSide.Aresden: summon = World.NpcConfiguration["MS-Aresden"].Copy(); break;
                            case OwnerSide.Elvine: summon = World.NpcConfiguration["MS-Elvine"].Copy(); break;
                        }
                        summon.BuildPoints = 40;
                        break;
                    case NpcType.CrusadeArrowTower:
                        switch (town)
                        {
                            case OwnerSide.Aresden: summon = World.NpcConfiguration["AGT-Aresden"].Copy(); break;
                            case OwnerSide.Elvine: summon = World.NpcConfiguration["AGT-Elvine"].Copy(); break;
                        }
                        summon.BuildPoints = 80;
                        break;
                    case NpcType.CrusadeCannonTower:
                        switch (town)
                        {
                            case OwnerSide.Aresden: summon = World.NpcConfiguration["CGT-Aresden"].Copy(); break;
                            case OwnerSide.Elvine: summon = World.NpcConfiguration["CGT-Elvine"].Copy(); break;
                        }
                        summon.BuildPoints = 90;
                        break;
                }
            
            if (summon != null)
            {
                summon.Appearance2 = 3; // starts in a "packed" state
                summon.Side = town;
                summon.CurrentWorld = world;
                guild.CrusadeStructures.Add(summon);
                world.CurrentEvent.NpcIDs.Add(world.InitNpc(summon, map, mapX, mapY, null));
            }
        }

        /// <summary>
        /// Character performs a Drop Item operation.
        /// </summary>
        /// <param name="itemIndex">Index of the item being dropped.</param>
        /// <param name="amount">Count of the item being dropped. Only affects stackable items if anything above 1.</param>
        /// <param name="itemName">Name of the item.</param>
        /// <returns>True or False to indicate item dropping succeeds or not.</returns>
        public bool DropItem(int itemIndex, int amount, string itemName)
        {
            if (isDead) return false;
            if (inventory[itemIndex] == null) return false;
            if (inventory[itemIndex].IsEquipped) UnequipItem(itemIndex);

            Item item = inventory[itemIndex];

            // if we are dropping less than the amount, split the stack and notify
            if (amount < item.Count)
            {
                Item splitItem = item.Copy(amount);
                if (map[mapY][mapX].SetItem(splitItem))
                {
                    item.Count = item.Count - amount; // remove amount from in-bag item then notify
                    Notify(CommandMessageType.NotifyItemCount, itemIndex, item.Count, 0); 
                    ItemDropped(this, splitItem); // notify the split amount is dropped
                    return true;
                }
                else return false;
            }
            else
            {
                if (map[mapY][mapX].SetItem(item))
                {
                    inventory[itemIndex] = null;
                    Notify(CommandMessageType.NotifyDropItemAndErase, itemIndex, item.Count);
                    ItemDropped(this, item);
                    return true;
                }
                else return false;
            }
        }

        public bool SetWarehouseItem(int itemIndex, int count)
        {
            int warehouseIndex = -1;
            if (inventory[itemIndex] == null) return false;
            if (WarehouseCount >= Globals.MaximumWarehouseItems)
            {
                Notify(CommandMessageType.NotifyWarehouseFull);
                return false;
            }

            Item item = inventory[itemIndex];

            UnequipItem(itemIndex);

            if (count >= item.Count)
            {
                AddWarehouseItem(item, count, out warehouseIndex);
                inventory[itemIndex] = null;
            }
            else
            {
                AddWarehouseItem(item, count, out warehouseIndex);
                inventory[itemIndex].Count -= count;
            }

            Notify(CommandMessageType.NotifyItemToWarehouse, item.GetInventoryToWarehouseData(warehouseIndex));
            Notify(CommandMessageType.NotifyGiveItemAndErase, itemIndex, count, Globals.WarehouseKeeperName);

            return true;
        }

        public bool RemoveWarehouseItem(int warehouseIndex, out int inventoryIndex)
        {
            inventoryIndex = -1;
            if (warehouse[warehouseIndex] == null) return false;
            if (InventoryCount >= Globals.MaximumInventoryItems)
            {
                Notify(CommandMessageType.NotifyInventoryFull);
                return false;
            }

            AddInventoryItem(warehouse[warehouseIndex], out inventoryIndex, false);
            warehouse[warehouseIndex] = null;

            // quickly sort
            for (int i = 0; i <= Globals.MaximumWarehouseItems - 2; i++)
                if ((warehouse[i + 1] != null) && (warehouse[i] == null))
                {
                    warehouse[i] = warehouse[i + 1];
                    warehouse[i + 1] = null;
                }

            return true;
        }

        public void DropLoot()
        {
            if (world.IsCrusade) return; // no drops in sade
            if (!Globals.PlayersDropItems) return;
            if (!map.LootEnabled) return;
            if ((world.IsHeldenianBattlefield || world.IsHeldenianSeige) && map.IsHeldenianMap) return; // no drops in heldenian on heldenian maps

            Item loot = null;

            // TODO zemstones
            // TODO criminals have more drops
            // TODO player drops

            if (loot != null) // drop it and notify
            {
                if (map[mapY][mapX].SetItem(loot)) ItemDropped(this, loot);
            }
        }

        public bool IsWithinRange(IOwner other, int rangeX, int rangeY) { return IsWithinRange(other, rangeX, rangeY, 0); }
        public bool IsWithinRange(IOwner other, int rangeX, int rangeY, int modifier)
        {
            if ((CurrentMap == other.CurrentMap) &&
                (mapX >= other.X - rangeX - modifier) &&
                (mapX <= other.X + rangeX + modifier) &&
                (mapY >= other.Y - rangeY - modifier) &&
                (mapY <= other.Y + rangeY + modifier))
                return true;
            else return false;
        }

        public bool IsWithinRange(IDynamicObject other, int rangeX, int rangeY, int modifier)
        {
            if ((CurrentMap == other.CurrentMap) &&
                (mapX >= other.X - rangeX - modifier) &&
                (mapX <= other.X + rangeX + modifier) &&
                (mapY >= other.Y - rangeY - modifier) &&
                (mapY <= other.Y + rangeY + modifier))
                return true;
            else return false;
        }

        /// <summary>
        /// Removes this character from the map
        /// </summary>
        public bool Remove()
        {
            if (isDead) map[mapY][mapX].ClearDeadOwner();
            else map[mapY][mapX].ClearOwner();

            // remove character from map id cache
            if (map.Players.Contains(id)) map.Players.Remove(id);

            LeaveParty();

            return true;
        }

        public void Disconnect(string reason)
        {
            clientInfo.Close(reason);
        }

        /// <summary>
        /// Teleports the character from the current location to a destination passed in as parameters and sets the character's direction.
        /// </summary>
        /// <param name="destinationMap">Destination map to teleport the character to.</param>
        /// <param name="destinationX">X coordinate of the destination.</param>
        /// <param name="destinationY">Y coordinate of the destination.</param>
        /// <param name="direction">Direction the character should fast after teleporting.</param>
        /// <param name="isRevive">Specifies whether this is a revival teleport.</param>
        /// <returns>Returns true or false to indicate teleporting has succeeded or not.</returns>
        public bool Teleport(Location location, MotionDirection direction) { return Teleport(world.Maps[location.MapName], location.X, location.Y, direction); }
        public bool Teleport(Map destinationMap, int destinationX, int destinationY) { return Teleport(destinationMap, destinationX, destinationY, this.direction); }
        public bool Teleport(Map destinationMap, int destinationX, int destinationY, MotionDirection direction)
        {
            if (isDead) return false;
            if (destinationX < 0 || destinationX > destinationMap.Width || destinationY < 0 || destinationY > destinationMap.Height)
                destinationX = destinationY = -1; // out of bounds, set to defaultLocation (-1,-1)

            if (destinationX == -1 || destinationY == -1)
            {
                if (destinationMap.DefaultLocations.Count > 0)
                {
                    Location location;
                    if (destinationMap.DefaultLocations.Count == 1) location = destinationMap.DefaultLocations[0];
                    else location = destinationMap.DefaultLocations[Math.Max(Dice.Roll(1, destinationMap.DefaultLocations.Count) - 1, 0)];
                    
                    if (location.X < 0 || location.X > destinationMap.Width || location.Y < 0 || location.Y > destinationMap.Height)
                        return false; // TODO log message - default locations are fucked!

                    if (CurrentLocation.Owner == this) CurrentLocation.ClearOwner();
                    if (CurrentLocation.DeadOwner == this) CurrentLocation.ClearDeadOwner();
                    this.map.Players.Remove(id);
                    world.SendLogEventToNearbyPlayers(this, CommandMessageType.Reject);
                    this.mapX = location.X;
                    this.mapY = location.Y;
                }
                else return false;
            }
            else
            {
                if (CurrentLocation.Owner == this) CurrentLocation.ClearOwner();
                if (CurrentLocation.DeadOwner == this) CurrentLocation.ClearDeadOwner();
                this.map.Players.Remove(id);
                world.SendLogEventToNearbyPlayers(this, CommandMessageType.Reject);
                this.mapX = destinationX;
                this.mapY = destinationY;
            }

            this.map = destinationMap;
            this.loadedMapName = this.map.Name;
            this.direction = direction;

            world.InitPlayerData(id);

            return true;
        }

        public void UseSkill(SkillType type)
        {
            switch (type)
            {
                case SkillType.PretendCorpse: break; // TODO
            }
        }

        /// <summary>
        /// Character uses a consumable item.
        /// </summary>
        /// <param name="itemIndex">Index of the item being used in the inventory.</param>
        /// <param name="destinationX">Map X Coordination.</param>
        /// <param name="destinationY">Map Y Coordination.</param>
        /// <param name="destinationItemIndex">Index of the target item.</param>
        public void UseItem(int itemIndex, int destinationX, int destinationY, int destinationItemIndex)
        {
            lock (statusLock)
            {
                if (isDead) return;
                if (inventory[itemIndex] == null) return;

                Item item = inventory[itemIndex];
                switch (item.Type)
                {
                    case ItemType.Eat:
                        switch (item.EffectType)
                        {
                            case ItemEffectType.Food: EatFood(Dice.Roll(item.Effect1, item.Effect2, item.Effect3)); break;
                            case ItemEffectType.HP: ReplenishHP(Dice.Roll(item.Effect1, item.Effect2, item.Effect3)); break;
                            case ItemEffectType.MP: ReplenishMP(Dice.Roll(item.Effect1, item.Effect2, item.Effect3)); break;
                            case ItemEffectType.SP: ReplenishSP(Dice.Roll(item.Effect1, item.Effect2, item.Effect3)); break;
                            case ItemEffectType.Unfreeze: RemoveMagicEffect(MagicType.Ice); break;
                        }
                        DepleteItem(itemIndex, true);
                        break;
                    case ItemType.Deplete:
                        bool confirmDeplete = false;
                        switch (item.EffectType)
                        {
                            case ItemEffectType.Magic: confirmDeplete = true; world.CastMagic(this, mapX, mapY, World.MagicConfiguration[item.Effect1], true); break;
                            case ItemEffectType.LearnMagic: confirmDeplete = LearnSpell(item.Effect1); break;
                            case ItemEffectType.LearnSkill: confirmDeplete = true; LearnSkill(item.Effect1, item.Effect2); break;
                            case ItemEffectType.Summon:
                                string npcName = "";
                                switch (item.Effect2) // TODO can this hardcoding be fixed?
                                {
                                    case 9: npcName = (town == OwnerSide.Aresden) ? "SOR-Aresden" : (town == OwnerSide.Elvine) ? "SOR-Elvine" : ""; break;
                                    case 10: npcName = (town == OwnerSide.Aresden) ? "ATK-Aresden" : (town == OwnerSide.Elvine) ? "ATK-Elvine" : ""; break;
                                    case 11: npcName = (town == OwnerSide.Aresden) ? "Elf-Aresden" : (town == OwnerSide.Elvine) ? "Elf-Elvine" : ""; break;
                                    case 12: npcName = (town == OwnerSide.Aresden) ? "DSK-Aresden" : (town == OwnerSide.Elvine) ? "DSK-Elvine" : ""; break;
                                    case 13: npcName = (town == OwnerSide.Aresden) ? "HBT-Aresden" : (town == OwnerSide.Elvine) ? "HBT-Elvine" : ""; break;
                                    case 14: npcName = (town == OwnerSide.Aresden) ? "Bar-Aresden" : (town == OwnerSide.Elvine) ? "Bar-Elvine" : ""; break;
                                    default: if (World.SummonConfiguration.ContainsKey(item.Effect1)) npcName = World.SummonConfiguration[item.Effect1]; break;
                                }
                                world.CreateNpc(npcName, map, mapX, mapY, this);
                                break;
                            default: break;
                        }
                        if (confirmDeplete) DepleteItem(itemIndex, true);
                        else Notify(CommandMessageType.NotifyItemCount, itemIndex, item.Count, 1);
                        // we use notify item count as a dummy because the client-side will release the item so it is no longer "disabled". we should creat its own command in new or updated clients. 1 = bIsItemResponse = true
                        break;
                    case ItemType.DepleteDestination:
                        Item destinationItem;
                        switch (item.EffectType)
                        {
                            case ItemEffectType.Flag: // has occupy status function and num flags unused
                                if (world.IsHeldenianSeige && map.Name.Equals(Globals.HeldenianCastleName) && guildRank == 0 &&
                                    destinationX == world.Heldenian.CastleWinningZone.X && destinationY == world.Heldenian.CastleWinningZone.Y &&
                                    world.Heldenian.CastleDefender != Side)
                                {
                                    world.EndWorldEvent(town);
                                    DepleteItem(itemIndex, true);
                                }
                                else Notify(CommandMessageType.NotifyHeldenianFlagFailed);
                                break;
                            case ItemEffectType.ConstructionKit: break; // TODO
                            case ItemEffectType.Dye:
                                if (destinationItemIndex == -1 || inventory[destinationItemIndex] == null) return;
                                destinationItem = inventory[destinationItemIndex];
                                // TODO put back
                                //if (destinationItem.Category == 11 || destinationItem.Category == 12) // TODO - again with the categories!? needed though
                                //{
                                    destinationItem.Colour = item.Effect1;
                                    Notify(CommandMessageType.NotifyItemColourChange, destinationItemIndex, destinationItem.Colour);
                                    DepleteItem(itemIndex, true);
                                //}
                                //else Notify(CommandMessageType.NotifyItemColourChange, destinationItemIndex, -1);
                                break;
                            case ItemEffectType.ArmourDye:
                                if (destinationItemIndex == -1 || inventory[destinationItemIndex] == null) return;
                                destinationItem = inventory[destinationItemIndex];
                                if (destinationItem.Category == 6) // TODO - again with the categories!? needed though
                                {
                                    destinationItem.Colour = item.Effect1;
                                    Notify(CommandMessageType.NotifyItemColourChange, destinationItemIndex, destinationItem.Colour);
                                    DepleteItem(itemIndex, true);
                                }
                                else Notify(CommandMessageType.NotifyItemColourChange, destinationItemIndex, -1);
                                break;
                            case ItemEffectType.FarmSeed:
                                // TODO map maximum crops

                                if (item.Effect2 > skills[SkillType.Farming].Level)
                                    Notify(CommandMessageType.NotifyFarmingSkillTooLow);
                                else if (map[destinationY][destinationX].IsBlocked || !map[destinationY][destinationX].IsFarm)
                                    Notify(CommandMessageType.NotifyFarmingInvalidLocation);
                                else
                                {
                                    Npc crop = World.NpcConfiguration["Crops"].Copy();
                                    crop.Side = town;
                                    crop.BuildType = item.Effect1;
                                    crop.BuildLimit = item.Effect2;
                                    crop.BuildPoints = 30; // TODO this previously used the "MinBravery" variable. we should add BuildPoints to the config
                                    crop.Appearance2 = item.Effect1 << 8 | 1;
                                    world.InitNpc(crop, map, mapX, mapY, this);

                                    DepleteItem(itemIndex, true);
                                }
                                break;
                            default: DepleteItem(itemIndex, true); break;
                        }
                        break;
                    case ItemType.Skill:
                        if (skillInUse == SkillType.None && item.Endurance > 0)
                        {
                            TakeItemDamage(1, item, itemIndex);

                            switch (item.RelatedSkill)
                            {
                                case SkillType.Fishing:
                                    skillInUse = SkillType.Fishing;
                                    skillTime = DateTime.Now; // triggers TimerProcess() then SkillProcess()
                                    skillTargetX = destinationX;
                                    skillTargetY = destinationY;
                                    break;
                            }
                        }
                        break;
                    default: DepleteItem(itemIndex, true); break;
                }
            }
        }

        public void LearnSkill(int skillIndex, int level)
        {
            if (skills[(SkillType)skillIndex] == null) return;

            // TODO - max of 700 levels? leveling down of skills?

            // only make changes and notify if the player is previously below requests level
            if (skills[(SkillType)skillIndex].Experience < Globals.SkillExperienceTable[level])
            {
                skills[(SkillType)skillIndex].Learn();
                Notify(CommandMessageType.NotifyStudySkillSuccess, skillIndex, level);
            }
        }

        public bool LearnSpell(string spellName, bool isPurchased) { foreach (Magic spell in World.MagicConfiguration.Values) if (spell.Name.Equals(spellName)) return LearnSpell(spell.Index, true); return false; }
        public bool LearnSpell(int spellIndex) { return LearnSpell(spellIndex, false); }
        public bool LearnSpell(int spellIndex, bool isPurchased)
        {
            if (CurrentMap.BuildingType != BuildingType.WizardTower && isPurchased) return false;

            Magic spell = World.MagicConfiguration[spellIndex];
            if ((spell == null) || (((intelligence + intelligenceBonus) < spell.RequiredIntelligence) && isPurchased)) // does not affect spell books since isPurchased is true (from wizard tower)
            {
                Notify(CommandMessageType.NotifyStudySpellFailed, spellIndex, spell.GoldCost, spell.RequiredIntelligence, spell.Name);
                return false;
            }

            if (isPurchased)
                if (Gold == null || Gold.Count < spell.GoldCost)
                {
                    Notify(CommandMessageType.NotifyStudySpellFailed, spellIndex, spell.GoldCost, spell.RequiredIntelligence, spell.Name);
                    return false;
                }
                else
                {
                    Gold.Count -= spell.GoldCost;
                    if (Gold.Count <= 0) DepleteItem(GoldIndex, true);
                    else Notify(CommandMessageType.NotifyItemCount, GoldIndex, Gold.Count, 0);
                }

            if (spellStatus[spellIndex] == true) return false; // TODO - friendly message, you already have this spell

            spellStatus[spellIndex] = true;
            Notify(CommandMessageType.NotifyStudySpellSuccess, spellIndex, 0, spell.Name);

            return true;
        }

        public void BuyItem(string itemName, int count)
        {
            if (isDead) return;

            if (CurrentMap.BuildingType != BuildingType.Shop &&
                CurrentMap.BuildingType != BuildingType.Blacksmith) return;

            if (InventoryCount >= Globals.MaximumInventoryItems)
            {
                Notify(CommandMessageType.NotifyInventoryFull);
                return;
            }

            Item item = null;

            // TODO arrows... sigh... somehow put Count in config? this is messy and hard coded
            switch (itemName)
            {
                case "10Arrows":    item = World.ItemConfiguration["Arrow"].Copy(10); break;
                case "100Arrows":   item = World.ItemConfiguration["Arrow"].Copy(100); break;
                default:            item = World.ItemConfiguration[itemName].Copy(); break;
            }
            if (item == null) return;
            if (!item.IsForSale) return;

            // non stackable items take 1 slot each. make sure we dont go over max items
            if (!item.IsStackable && (InventoryCount + count > Globals.MaximumInventoryItems))
                count = (Globals.MaximumInventoryItems - InventoryCount);

            int cost = 0;

            // crusade winners get 10% discount
            if (World.WorldEventResults.ContainsKey(WorldEventType.Crusade) && 
                World.WorldEventResults[WorldEventType.Crusade].Winner == town)
                 cost = ((int)((double)item.Price * 0.9) * count);
            else cost = item.Price * count;

            // heldenian losers get 100% penalty
            if (World.WorldEventResults.ContainsKey(WorldEventType.Heldenian) &&
                World.WorldEventResults[WorldEventType.Heldenian].Winner != town &&
                town != OwnerSide.Neutral)
                cost = (item.Price * 2) * count;

            // charisma discount
            if ((charisma + charismaBonus) > 10)
                cost -= (int)((double)cost * ((double)((charisma + charismaBonus) / 4) / 100.0f));

            if (Gold == null || Gold.Count < cost) // you are poor
                Notify(CommandMessageType.NotifyNotEnoughGold, -1); // -1 is the item index. since this is a new item, use -1
            else
            {
                Gold.Count -= cost;
                if (Gold.Count <= 0) DepleteItem(GoldIndex, true);
                else Notify(CommandMessageType.NotifyItemCount, GoldIndex, Gold.Count, 0);

                if (item.IsStackable)
                {
                    item.Count *= count; // we multiple incase bought item already has count (arrows)
                    AddInventoryItem(item);
                    Notify(CommandMessageType.NotifyItemPurchased, item.GetPurchasedData((short)cost));
                }
                else
                    for (int i = 0; i < count; i++)
                    {
                        AddInventoryItem(item);
                        Notify(CommandMessageType.NotifyItemPurchased, item.GetPurchasedData((short)cost));
                    }
            }
        }

        public void SellItem(int itemIndex, int ownerType, int count, string itemName)
        {
            if (isDead) return;
            if (inventory[itemIndex] == null) return;
            Item item = inventory[itemIndex];
            if (count <= 0 || item.Count < count) return;

            switch (ownerType) // bad way to do this... but must emulate for client. consider the way GiveItem works (checks destination and uses ownerID so no hard-coded values)
            {
                case 15: // ShopKeeper-W
                case 24: // Tom
                    int sellPrice = (Side == OwnerSide.Neutral) ? (item.Price / 4) * count : (item.Price / 2) * count;

                    // TODO stated items give higher price

                    if (Gold == null && InventoryCount + 1 < Globals.MaximumInventoryItems)
                        Notify(CommandMessageType.NotifySellItemFailed, itemIndex, 4, item.Name); // no space to take gold
                    else Notify(CommandMessageType.NotifySellItemQuote, itemIndex, item.Endurance, sellPrice, count, item.Name);
                    break;
            }
        }

        public void RepairItem(int itemIndex, int ownerType, string itemName)
        {
            if (isDead) return;
            if (inventory[itemIndex] == null) return;
            Item item = inventory[itemIndex];

            switch (ownerType) // bad way to do this... but must emulate for client. consider the way GiveItem works (checks destination and uses ownerID so no hard-coded values)
            {
                case 15: // ShopKeeper-W
                case 24: // Tom
                    int repairPrice = (item.Endurance <= 0) ? item.Price/2 : (int)((item.Price/2)-(int)((double)item.Price *(((double)item.Endurance/(double)item.MaximumEndurance)*0.5f)));

                    Notify(CommandMessageType.NotifyRepairItemQuote, itemIndex, item.Endurance, repairPrice, item.Name);
                    break;
            }
        }

        public void SellItemConfirm(int itemIndex, int count, string itemName)
        {
            if (isDead) return;
            if (inventory[itemIndex] == null) return;
            Item item = inventory[itemIndex];
            if (count <= 0 || item.Count < count) return;
            if (item.Endurance <= 0) return;

            int sellPrice = (Side == OwnerSide.Neutral) ? (item.Price / 4) * count : (item.Price / 2) * count;

            // TODO stated items give higher price

            Notify(CommandMessageType.NotifyItemSold, itemIndex);

            if (item.IsStackable)
            {
                item.Count--;
                if (item.Count <= 0) DepleteItem(itemIndex);
                else Notify(CommandMessageType.NotifyItemCount, itemIndex, item.Count, 0);
            }
            else DepleteItem(itemIndex);

            Item gold = World.ItemConfiguration["Gold"].Copy(sellPrice);
            AddInventoryItem(gold);
        }

        public void RepairItemConfirm(int itemIndex, string itemName)
        {
            if (isDead) return;
            if (inventory[itemIndex] == null) return;
            Item item = inventory[itemIndex];
            if (item.Endurance <= 0) return;

            int repairPrice = (item.Endurance <= 0) ? item.Price / 2 : (int)((item.Price / 2) - (int)((double)item.Price * (((double)item.Endurance / (double)item.MaximumEndurance) * 0.5f)));

            if (Gold == null || Gold.Count < repairPrice)
                Notify(CommandMessageType.NotifyNotEnoughGold, itemIndex);
            else
            {
                item.Endurance = item.MaximumEndurance;
                Notify(CommandMessageType.NotifyItemRepaired, itemIndex, item.Endurance);

                Gold.Count-= repairPrice;
                Notify(CommandMessageType.NotifyItemCount, GoldIndex, Gold.Count, 0);
            }

            
        }

        public bool BecomeCitizen(out string townName)
        {
            townName = "none";
            if (level < 5 || town != OwnerSide.Neutral) return false;

            if (CurrentMap.BuildingType == BuildingType.AresdenCityhall)
            {
                town = OwnerSide.Aresden;
                townName = "aresden";
            }
            else if (CurrentMap.BuildingType == BuildingType.ElvineCityhall)
            {
                town = OwnerSide.Elvine;
                townName = "elvine";
            }

            return true;
        }

        public void ToggleCombatMode()
        {
            int appearance = ((this.appearance2 & 0xF000) >> 12);

            if (!IsCombatMode) this.appearance2 = (0xF000 | appearance2);
            else this.appearance2 = (0x0FFF & appearance2);

            if (StatusChanged != null) StatusChanged(this);
        }

        public void ToggleSafeMode()
        {
            isSafeMode = !isSafeMode;
            Notify(CommandMessageType.NotifySafeMode, ((isSafeMode) ? 1 : 0));
        }

        public int GetPlayerRelationship(Character other)
        {
            int status = 0;

            if (criminalCount > 0) status = 8;
            if (town != HelbreathWorld.OwnerSide.Neutral) status = status | 4;
            if (town == HelbreathWorld.OwnerSide.Aresden) status = status | 2;
            if (isCivilian) status = status | 1; // m_bIsHunter in old source

            return status;
        }

        public int GetNpcRelationship(Npc other)
        {
            int status = 0x0000;

            switch (other.Side)
            {
                case OwnerSide.Neutral:
                    break;
                case OwnerSide.Wild:
                    status |= 0x0001 << 3;
                    break;
                case OwnerSide.Aresden:
                    status |= 0x0001 << 2;
                    status |= 0x0001 << 1;
                    break;
                case OwnerSide.Elvine:
                    status |= 0x0001 << 2;
                    break;
            }

            return status;
        }

        /// <summary>
        /// Actions to be performed on a timer such as hunger, mp/hp regeneration etc.
        /// </summary>
        public void TimerProcess()
        {
            if (isDead) return;

            TimeSpan ts;

            // handle Hunger
            ts = DateTime.Now - hungerTime;
            if (ts.TotalSeconds >= Globals.HungerTime)
            {
                hunger--;
                if (hunger <= 0) hunger = 0;
                if (hunger < 30) Notify(CommandMessageType.NotifyHunger, hunger);
                hungerTime = DateTime.Now;
            }

            // if starving, cant regen
            if (hunger > 10)
            {
                // handle Hp Regen
                if (hp < MaxHP)
                {
                    ts = DateTime.Now - hpUpTime;
                    if (ts.TotalSeconds >= Globals.HPRegenTime)
                    {
                        int regen = Dice.Roll(1, (vitality + vitalityBonus));
                        if (regen < (vitality + vitalityBonus) / 2) regen = (vitality + vitalityBonus) / 2;

                        if (Weapon != null && Weapon.SpecialAbilityType == ItemSpecialAbilityType.Blood)
                            regen -= (regen / 5);

                        regen += hpStock; // should blood not be calculated last? BUG?
                        if (bonusHPRecovery > 0) regen += (int)((double)(((double)bonusHPRecovery / 100.0f) * (double)regen));
                        ReplenishHP(regen);
                        hpUpTime = DateTime.Now;
                    }
                }

                // handle Mp Regen
                if (mp < MaxMP)
                {
                    ts = DateTime.Now - mpUpTime;
                    if (ts.TotalSeconds >= Globals.MPRegenTime)
                    {
                        int regen = Dice.Roll(1, (magic + magicBonus));
                        if (bonusMPRecovery > 0) regen = (int)(((double)bonusMPRecovery / 100.0f) * (double)regen);

                        ReplenishMP(regen);
                        mpUpTime = DateTime.Now;
                    }
                }

                // handle Sp Regen
                if (sp < MaxSP)
                {
                    ts = DateTime.Now - spUpTime;
                    if (ts.TotalSeconds >= Globals.SPRegenTime)
                    {
                        int regen = Dice.Roll(1, (vitality + vitalityBonus) / 3, 15);
                        if (bonusSPRecovery > 0) regen = (int)(((double)bonusSPRecovery / 100.0f) * (double)regen);
                        ReplenishSP(regen);
                        spUpTime = DateTime.Now;
                    }
                }
            }

            // handle poison time
            if (magicEffects.ContainsKey(MagicType.Poison))
            {
                ts = DateTime.Now - poisonTime;
                if (ts.TotalSeconds >= Globals.PoisonTime)
                {
                    int damage = Dice.Roll(1, magicEffects[MagicType.Poison].Magic.Effect2);
                    DepleteHP((hp - damage < 1) ? hp-1 : damage); // cant go below 1 hp
                    if (EvadePoison()) RemoveMagicEffect(MagicType.Poison);
                }
            }

            // handle skill time
            if (skillInUse != SkillType.None)
            {
                ts = DateTime.Now - skillTime;
                if (ts.Seconds >= Globals.SkillTime)
                    SkillProcess(skillInUse);
            }

            // handle increase of stored experience and level up
            if (experienceStored > 0)
            {
                ts = DateTime.Now - experienceUpTime;
                if (ts.TotalSeconds >= Globals.ExperienceRollTime)
                {
                    experience += (experienceStored * Globals.ExperienceMultiplier);

                    if (experience >= Globals.ExperienceTable[level + 1])
                    {
                        // handle level up. use loop incase there is enough experience to level up more than once.
                        while (experience >= Globals.ExperienceTable[level + 1])
                            if (level + 1 > Globals.MaximumLevel)
                            {
                                majestics++;
                                Notify(CommandMessageType.NotifyMajestics, majestics, 1);
                                experience = Globals.ExperienceTable[Globals.MaximumLevel];
                            }
                            else if (town == OwnerSide.Neutral && level + 1 > Globals.TravellerLimit)
                            {
                                Notify(CommandMessageType.NotifyTravellerLimit);
                                experience = Globals.ExperienceTable[Globals.TravellerLimit] - 1;
                                break;
                            }
                            else
                            {
                                level++;
                                Notify(CommandMessageType.NotifyLevelUp);
                            }
                    }

                    Notify(CommandMessageType.NotifyExperience);
                    experienceStored = 0;
                    experienceUpTime = DateTime.Now;
                }
            }

            // handle magic effect removal if time has expired
            if (magicEffects.Count > 0)
                for (int i = 0; i < 50; i++)
                    if (magicEffects.ContainsKey((MagicType)i))
                    {
                        MagicEffect effect = magicEffects[(MagicType)i];
                        if (effect.Magic.Type != MagicType.Poison) // poison have their own timer
                        {
                            ts = DateTime.Now - effect.StartTime;
                            if (ts.TotalSeconds >= effect.Magic.LastTime.TotalSeconds) // use total seconds as some spells last over 59 seconds
                                RemoveMagicEffect(effect.Magic.Type);
                        }
                    }

            // handle quest check for completion/notifications
            if (activeQuests.Count > 0)
                foreach (Quest quest in activeQuests)
                    switch (quest.Type)
                    {
                        case QuestType.Slayer: break;
                        default: break;
                    }

            ChargeCritical();
            ChargeSpecialAbility();
            if (muteTime.TotalSeconds > 0) muteTime = muteTime.Subtract(new TimeSpan(0, 0, 3));
            if (reputationTime.TotalSeconds > 0) reputationTime = reputationTime.Subtract(new TimeSpan(0, 0, 3));
        }

        /// <summary>
        /// Handles skills that need to process over time, such as fishing.
        /// </summary>
        /// <param name="skill"></param>
        private void SkillProcess(SkillType skill)
        {
            switch (skill)
            {
                case SkillType.Fishing:
                    int skillLevel = skills[SkillType.Fishing].Level;
                    int changeValue = Math.Max(skills[SkillType.Fishing].Level / 10, 1);

                    if (targetFish == null)
                        if (map[skillTargetY][skillTargetX].IsWater)
                        {
                            if (map[skillTargetY][skillTargetX].DynamicObject != null &&
                                map[skillTargetY][skillTargetX].DynamicObject.Type == DynamicObjectType.Fish) // rare fish/bubbles
                            {
                                Fish fish = (Fish)CurrentMap[skillTargetY][skillTargetX].DynamicObject;
                                targetFish = fish;
                                targetFishChance = 1;
                                Notify(CommandMessageType.NotifyFishingStarted, fish.FishItem.Price / 2, fish.FishItem.Sprite, fish.FishItem.SpriteFrame, fish.FishItem.Name);
                                Notify(CommandMessageType.NotifySkillEnd, 1);
                            }
                            else // standard fish - 100% success rate, low exp and skill exp
                            {
                                Item fish = World.ItemConfiguration["Fish"].Copy();
                                if (CurrentLocation.SetItem(fish)) ItemDropped(this, fish);
                                experienceStored += Dice.Roll(2, 5);
                                skills[SkillType.Fishing].Experience++;
                                Notify(CommandMessageType.NotifySkillEnd, 1);
                                skillInUse = SkillType.None;
                            }
                        }
                        else // no water, cant fish here
                        {
                            Notify(CommandMessageType.NotifySkillEnd, 0);
                            skillInUse = SkillType.None;
                        }

                    if (targetFish != null) // rare fish/bubbles at target
                    {
                        skillLevel = Math.Max(skillLevel - targetFish.Difficulty, 1);

                        int result = Dice.Roll(1, 100);
                        if (skillLevel > result)
                        {
                            targetFishChance = Math.Min(targetFishChance + changeValue, 99);
                            Notify(CommandMessageType.NotifyFishingChance, targetFishChance);
                        }
                        else
                        {
                            targetFishChance = Math.Max(targetFishChance - changeValue, 1);
                            Notify(CommandMessageType.NotifyFishingChance, targetFishChance);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Rolls a dice to see if this Character successfully evades the incoming attack or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Character has successfully evaded or not.</returns>
        public bool EvadeMelee(int attackerHitChance)
        {
            int evadeChance = (dexterity + dexterityBonus) * 2;
            evadeChance += bonusPhysicalResistance;
            if (magicEffects.ContainsKey(MagicType.Protect))
                switch (magicEffects[MagicType.Protect].Magic.Effect1)
                {
                    case 3: evadeChance += 40; break; // defence shield
                    case 4: evadeChance += 100; break; // greater defence shield
                }

            double temp1 = ((double)attackerHitChance / (double)evadeChance);
            double temp2 = ((double)(temp1 * 50.0f));
            int hitChance = (int)temp2;

            if (hitChance < Globals.MinimumHitChance) hitChance = Globals.MinimumHitChance;
            if (hitChance > Globals.MaximumHitChance) hitChance = Globals.MaximumHitChance;

            int result = Dice.Roll(1, 100);

            if (result <= hitChance) return false;
            else return true;
        }

        /// <summary>
        /// Rolls a dice to see if this Character successfully evades a magic attack or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <param name="ignorePFM">Does this spell ignore PFM?</param>
        /// <returns>True or False to indicate that this Character has successfully evaded or not.</returns>
        public bool EvadeMagic(int attackerHitChance, bool ignorePFM)
        {
            if (attackerHitChance == -1) return true; 

            int evadeChance = skills[SkillType.MagicResistance].Level;
            if (magic + magicBonus > 50) evadeChance += (magic + magicBonus) - 50;
            evadeChance += bonusMagicResistance;

            if (magicEffects.ContainsKey(MagicType.Protect))
                if (magicEffects[MagicType.Protect].Magic.Effect1 == 5) return true; // AMP
                else if (MagicEffects[MagicType.Protect].Magic.Effect1 == 2 && !ignorePFM) return true; // PFM

            if (evadeChance < 1) evadeChance = 1;

            double temp1 = ((double)attackerHitChance / (double)evadeChance);
            double temp2 = ((double)(temp1 * 50.0f));
            int hitChance = (int)temp2;

            if (hitChance < Globals.MinimumHitChance) hitChance = Globals.MinimumHitChance;
            if (hitChance > Globals.MaximumHitChance) hitChance = Globals.MaximumHitChance;

            int result = Dice.Roll(1, 100);

            if (result <= hitChance) return false;
            else
            {
                skills[SkillType.MagicResistance].Experience++;
                return true;
            }
        }

        /// <summary>
        /// Rolls a dice to see if this Character successfully evades the being frozen or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Character has successfully evaded being frozen or not.</returns>
        public bool EvadeIce()
        {
            int evadeChance = bonusWaterAbsorption * 2; // why x2 i dno.. maybe just set 50 and 100 in item configs? duh...
            if (evadeChance < 1) evadeChance = 1;
            if (evadeChance > Globals.MaximumFreezeProtection) evadeChance = Globals.MaximumFreezeProtection;

            int result = Dice.Roll(1, 100);

            if (result <= evadeChance) return true;
            else return false;
        }

        /// <summary>
        /// Rolls a dice to see if this Character successfully evades the being poisoned or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Character has successfully evaded being poisoned or not.</returns>
        public bool EvadePoison()
        {
            int evadeChance = skills[SkillType.PoisonResistance].Level + bonusPoisonResistance;
            if (evadeChance < 10) evadeChance = 10;
            if (evadeChance > Globals.MaximumPoisonProtection) evadeChance = Globals.MaximumPoisonProtection;

            int result = Dice.Roll(1, 100);

            if (result <= evadeChance) return true;
            else return false;
        }

        public void DepleteItem(int itemIndex) { DepleteItem(itemIndex, false); }
        public void DepleteItem(int itemIndex, bool isUseItemResult)
        {
            if (inventory[itemIndex].IsEquipped) UnequipItem(itemIndex);

            Notify(CommandMessageType.NotifyItemDepleted, itemIndex, ((isUseItemResult) ? 1 : 0));

            inventory[itemIndex] = null;
        }

        public void UpgradeItem(int itemIndex)
        {
            if (inventory[itemIndex] == null) return;

            Item item = inventory[itemIndex];
            int requiredMajesticPoints = 0, maximumLevel;

            switch (item.EffectType)
            {
                case ItemEffectType.Defence:
                case ItemEffectType.DefenceAntiMine:
                case ItemEffectType.DefenceActivation:
                    maximumLevel = 7;
                    break;
                case ItemEffectType.Attack:
                case ItemEffectType.AttackBow:
                case ItemEffectType.AttackDefence:
                case ItemEffectType.AttackManaSave:
                case ItemEffectType.AttackActivation:
                    maximumLevel = 15;
                    break;
                case ItemEffectType.AddEffect: maximumLevel = 10; break;
                default: Notify(CommandMessageType.NotifyItemUpgradeFailed, 2); return;
            }

            if (item.Level >= maximumLevel) { Notify(CommandMessageType.NotifyItemUpgradeFailed, 1); return; }

            // TODO - determine this based on the upgradeability of an item (set in configs maybe?)
            ItemUpgradeType type = ItemUpgradeType.Majestic;

            switch (type)
            {
                case ItemUpgradeType.Majestic:
                    if (!item.IsAngel && !item.IsUpgradeable)
                        Notify(CommandMessageType.NotifyItemUpgradeFailed, 2);
                    else if (majestics > 0)
                    {
                        if (item.IsAngel)
                            switch (item.Level)
                            {
                                case 0: requiredMajesticPoints = 10; break;
                                case 1: requiredMajesticPoints = 11; break;
                                case 2: requiredMajesticPoints = 13; break;
                                case 3: requiredMajesticPoints = 16; break;
                                case 4: requiredMajesticPoints = 20; break;
                                case 5: requiredMajesticPoints = 25; break;
                                case 6: requiredMajesticPoints = 31; break;
                                case 7: requiredMajesticPoints = 38; break;
                                case 8: requiredMajesticPoints = 46; break;
                                case 9: requiredMajesticPoints = 55; break;
                            }
                        else if (item.IsUpgradeable)
                            switch (item.EffectType)
                            {
                                case ItemEffectType.Defence:
                                case ItemEffectType.DefenceAntiMine:
                                case ItemEffectType.DefenceActivation:
                                case ItemEffectType.Attack:
                                case ItemEffectType.AttackBow:
                                case ItemEffectType.AttackDefence:
                                case ItemEffectType.AttackManaSave:
                                case ItemEffectType.AttackActivation:
                                    requiredMajesticPoints = (item.Level * (item.Level + 6) / 8) + 2;
                                    break;
                            }

                        if (majestics - requiredMajesticPoints >= 0)
                        {
                            majestics -= requiredMajesticPoints;
                            Notify(CommandMessageType.NotifyMajestics, majestics);

                            int newItemLevel;
                            if (item.IsAngel) newItemLevel = item.Level + 1; // angels get +1 per upgrade
                            else newItemLevel = Math.Min(item.Level + 2, maximumLevel); // other items are +2 per upgrade until maximum level

                            // TODO - fix this hard coded bullshit - upgrade to new item. possible UpgradeTransformLevel="" and UpgradeTransformItem=""
                            switch (item.Name)
                            {
                                case "DarkKnightFlameberge":
                                    if (item.Level == 0)
                                    {
                                        inventory[itemIndex] = World.ItemConfiguration["DarkKnightGiantSword"].Copy();
                                        item = inventory[itemIndex]; // points to new item
                                        item.BoundID = this.databaseID;
                                        item.BoundName = this.name;
                                    }
                                    break;
                                case "DarkKnightGiantSword":
                                    if (item.Level >= 6)
                                    {
                                        inventory[itemIndex] = World.ItemConfiguration["BlackKnightTemple"].Copy();
                                        item = inventory[itemIndex]; // points to new item
                                        item.BoundID = this.databaseID;
                                        item.BoundName = this.name;
                                    }
                                    break;
                                case "DarkMageMagicStaff":
                                    if (item.Level >= 4)
                                    {
                                        inventory[itemIndex] = World.ItemConfiguration["DarkMageMagicWand"].Copy();
                                        item = inventory[itemIndex]; // points to new item
                                        item.BoundID = this.databaseID;
                                        item.BoundName = this.name;
                                    }
                                    break;
                                case "DarkMageMagicWand":
                                    if (item.Level >= 6)
                                    {
                                        inventory[itemIndex] = World.ItemConfiguration["BlackMageTemple"].Copy();
                                        item = inventory[itemIndex]; // points to new item
                                        item.BoundID = this.databaseID;
                                        item.BoundName = this.name;
                                    }
                                    break;
                                case "BlackMageTemple":
                                case "BlackKnightTemple": 
                                    if (item.Level >= maximumLevel) item.Colour = 9; // gives it the glow
                                    break; 
                            }

                            item.Attribute = ((item.Attribute & 0x0FFFFFFF) | ((uint)(newItemLevel) << 28));
                            Notify(CommandMessageType.NotifyItemUpgrade, itemIndex);
                        }
                        else Notify(CommandMessageType.NotifyItemUpgradeFailed, 3);
                    }
                    else Notify(CommandMessageType.NotifyItemUpgradeFailed, 3); // 1 = item maxed, 2 = not possible, 3 = not enough majestic points
                    break;
                case ItemUpgradeType.Merien:
                case ItemUpgradeType.Xelima:
                    Notify(CommandMessageType.NotifyItemUpgradeFailed, 2);
                    // TODO stone upgrade
                    break;
                default: Notify(CommandMessageType.NotifyItemUpgradeFailed, 2); break;
            }
        }

        public byte[] GetStatsData()
        {
            byte[] data = new byte[110];

            Buffer.BlockCopy(BitConverter.GetBytes(hp), 0, data, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(mp), 0, data, 4, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(sp), 0, data, 8, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(0), 0, data, 12, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(0), 0, data, 16, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(level), 0, data, 20, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(strength), 0, data, 24, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(intelligence), 0, data, 28, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(vitality), 0, data, 32, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(dexterity), 0, data, 36, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(magic), 0, data, 40, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(charisma), 0, data, 44, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(AvailableLevelUpPoints), 0, data, 48, 2);
            data[50] = 0;
            data[51] = 0;
            data[52] = 0;
            data[53] = 0;
            data[54] = 0;
            Buffer.BlockCopy(BitConverter.GetBytes(experience), 0, data, 55, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(enemyKills), 0, data, 59, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(criminalCount), 0, data, 63, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(rewardGold), 0, data, 67, 4);
            Buffer.BlockCopy(Town.ToString().ToLower().GetBytes(10), 0, data, 71, 10);
            Buffer.BlockCopy(guild.Name.GetBytes(20), 0, data, 81, 20);
            Buffer.BlockCopy(BitConverter.GetBytes(guildRank), 0, data, 101, 4);
            data[105] = (byte)criticals;
            Buffer.BlockCopy(BitConverter.GetBytes(-1), 0, data, 106, 4);

            return data;
        }

        public void GetMapStatusData(int mode, string mapName)
        {
            int size = 0, structureCount = 0;
            switch (mode)
            {
                case 1:
                    if (Guild != Guild.None) Notify(CommandMessageType.NotifyCrusadeTeleportLocation);
                    break;
                case 3:
                    byte[] buffer = new byte[1024];
                    Buffer.BlockCopy(mapName.GetBytes(10), 0, buffer, size, 10);
                    size+=10;
                    Buffer.BlockCopy(0.GetBytes(), 0, buffer, size, 2);
                    size += 2;
                    buffer[size] = 0;
                    size++;
                    foreach (KeyValuePair<OwnerSide, List<WorldEventStructure>> structureList in world.Crusade.Structures)
                        foreach (WorldEventStructure structure in structureList.Value)
                        {
                            buffer[size] = (byte)World.NpcConfiguration[structure.NpcName].Type;
                            size++;
                            Buffer.BlockCopy(structure.Location.X.GetBytes(), 0, buffer, size, 2);
                            size += 2;
                            Buffer.BlockCopy(structure.Location.Y.GetBytes(), 0, buffer, size, 2);
                            size += 2;
                            buffer[size] = (byte)((int)structureList.Key);
                            size++;
                            structureCount++;
                        }

                    foreach (Guild guild in world.Guilds.Values)
                        foreach (Npc structure in guild.CrusadeStructures)
                        {
                            buffer[size] = (byte)structure.Type;
                            size++;
                            Buffer.BlockCopy(structure.X.GetBytes(), 0, buffer, size, 2);
                            size += 2;
                            Buffer.BlockCopy(structure.Y.GetBytes(), 0, buffer, size, 2);
                            size += 2;
                            buffer[size] = (byte)((int)structure.Side);
                            size++;
                            structureCount++;
                        }

                    buffer[12] = (byte)structureCount;

                    byte[] data = new byte[size];
                    Buffer.BlockCopy(buffer, 0, data, 0, size);

                    Notify(CommandMessageType.NotifyMapStatusNext, data);
                    break;
            }
        }

        public byte[] GetItemsData()
        {
            int size = 0, itemIndex = 0, inventoryCount = InventoryCount, warehouseCount = WarehouseCount;

            byte[] data = new byte[2 + (inventoryCount * 44) + (warehouseCount * 43) + 100 + 60];

            data[size] = (byte)inventoryCount;
            size++;

            if (inventoryCount > 0)
                foreach (Item item in inventory)
                {
                    if (item != null)
                    {
                        Buffer.BlockCopy(item.GetInventoryData(), 0, data, size, 44);
                        size += 44;
                    }
                    itemIndex++;
                }

            data[size] = (byte)warehouseCount;
            size++;

            if (warehouseCount > 0)
                foreach (Item item in warehouse) // does not include "equipped or not"
                    if (item != null)
                    {
                        Buffer.BlockCopy(item.GetWarehouseData(), 0, data, size, 43);
                        size += 43;
                    }

            // MAGIC LEARNED
            for (int i = 0; i < Globals.MaximumSpells; i++)
                data[size + i] = (byte)Convert.ToInt32(spellStatus[i]);
            size += Globals.MaximumSpells;

            // SKILL LEVELS
            for (int i = 0; i < Globals.MaximumSkills; i++)
                if (Enum.IsDefined(typeof(SkillType), i) && skills.ContainsKey((SkillType)i)) // check this skill exists
                    data[size + i] = (byte)skills[(SkillType)i].Level;
                else data[size + i] = 0;
            size += Globals.MaximumSkills;

            return data;
        }

        public byte[] GetInitData()
        {
            byte[] mapData = map.GetMapData(this, mapX - 10, mapY - 7);

            byte[] data = new byte[mapData.Length + 60];

            Buffer.BlockCopy(BitConverter.GetBytes((short)id), 0, data, 0, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)mapX - 14 - 5), 0, data, 2, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)mapY - 12 - 5), 0, data, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)type), 0, data, 6, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance1), 0, data, 8, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance2), 0, data, 10, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance3), 0, data, 12, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance4), 0, data, 14, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(appearanceColour), 0, data, 16, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data, 20, 4);
            Buffer.BlockCopy(loadedMapName.GetBytes(10), 0, data, 24, 10);
            Buffer.BlockCopy(loadedMapName.GetBytes(10), 0, data, 34, 10);
            data[44] = (byte)((int)map.TimeOfDay); //day/night
            data[45] = (byte)((int)map.Weather); //weather
            Buffer.BlockCopy(BitConverter.GetBytes(contribution), 0, data, 46, 4);
            data[50] = 0; //observer
            Buffer.BlockCopy(BitConverter.GetBytes(reputation), 0, data, 51, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(hp), 0, data, 55, 4);
            data[59] = 0; //discount (not sure what this is for yet)
            Buffer.BlockCopy(mapData, 0, data, 60, mapData.Length);
            return data;
        }

        public byte[] GetMotionData(MotionType type, CommandMessageType messageType) { return GetMotionData(type, messageType, this.mapX, this.mapY, this.direction); }
        public byte[] GetMotionData(MotionType type, CommandMessageType messageType, int sourceX, int sourceY, MotionDirection direction)
        {
            byte[] data = new byte[1];
            byte[] mapData;
            switch (messageType)
            {
                case CommandMessageType.ObjectConfirmMotion: break;
                case CommandMessageType.ObjectConfirmAttack: break;
                case CommandMessageType.ObjectConfirmMove:
                    switch (type)
                    {
                        case MotionType.Move:
                        case MotionType.Run:
                            mapData = map.GetMapDataPan(this, sourceX - 10, sourceY - 7, direction);

                            data = new byte[mapData.Length + 12];

                            Buffer.BlockCopy(BitConverter.GetBytes((short)(sourceX - 10)), 0, data, 0, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)(sourceY - 7)), 0, data, 2, 2);
                            data[4] = (byte)((int)direction);
                            if (type == MotionType.Run)
                                data[5] = (byte)1; // has staminar changed? IF...ELSE shorthand
                            else data[5] = (byte)0;
                            data[6] = (byte)0; // m_iOccupyStatus not sure what to do with this yet
                            Buffer.BlockCopy(BitConverter.GetBytes(hp), 0, data, 7, 4);
                            Buffer.BlockCopy(mapData, 0, data, 11, mapData.Length);
                            break;
                        case MotionType.Idle: break;
                    }
                    break;
                case CommandMessageType.ObjectRejectMove:
                    switch (type)
                    {
                        case MotionType.Move:
                        case MotionType.Run:
                            data = new byte[36];

                            Buffer.BlockCopy(BitConverter.GetBytes((ushort)id), 0, data, 0, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)mapX), 0, data, 2, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)mapY), 0, data, 4, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)this.type), 0, data, 6, 2);
                            data[8] = (byte)((int)direction);
                            Buffer.BlockCopy(name.GetBytes(10), 0, data, 9, 10);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance1), 0, data, 19, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance2), 0, data, 21, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance3), 0, data, 23, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance4), 0, data, 25, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes(appearanceColour), 0, data, 27, 4);
                            Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data, 31, 4);
                            break;
                        case MotionType.Idle:
                            data = new byte[36];

                            Buffer.BlockCopy(BitConverter.GetBytes((ushort)id), 0, data, 0, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)mapX), 0, data, 2, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)mapY), 0, data, 4, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)this.type), 0, data, 6, 2);
                            data[8] = (byte)((int)direction);
                            Buffer.BlockCopy(name.GetBytes(10), 0, data, 9, 10);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance1), 0, data, 19, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance2), 0, data, 21, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance3), 0, data, 23, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance4), 0, data, 25, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes(appearanceColour), 0, data, 27, 4);
                            Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data, 31, 4);
                            data[35] = 0; // dead or not?
                            break;
                    }
                    break;
            }

            return data;
        }

        public byte[] GetFullObjectData(Character requester)
        {
            byte[] data = new byte[36];

            Buffer.BlockCopy(id.GetBytes(), 0, data, 0, 2);
            Buffer.BlockCopy(mapX.GetBytes(), 0, data, 2, 2);
            Buffer.BlockCopy(mapY.GetBytes(), 0, data, 4, 2);
            Buffer.BlockCopy(type.GetBytes(), 0, data, 6, 2);
            data[8] = (byte)((int)direction);
            Buffer.BlockCopy(name.GetBytes(10), 0, data, 9, 10);
            Buffer.BlockCopy(appearance1.GetBytes(), 0, data, 19, 2);
            Buffer.BlockCopy(appearance2.GetBytes(), 0, data, 21, 2);
            Buffer.BlockCopy(appearance3.GetBytes(), 0, data, 23, 2);
            Buffer.BlockCopy(appearance4.GetBytes(), 0, data, 25, 2);
            Buffer.BlockCopy(appearanceColour.GetBytes(), 0, data, 27, 4);
            int status = ((0x0FFFFFFF & this.status) | (requester.GetPlayerRelationship(this) << 28));
            Buffer.BlockCopy(status.GetBytes(), 0, data, 31, 4);
            if (isDead)
                data[35] = 1;
            else data[35] = 0;

            return data;
        }

        public void Notify(CommandMessageType type) { Notify(type, 0, 0, 0, ""); }
        public void Notify(CommandMessageType type, string stringValue) { Notify(type, 0, 0, 0, stringValue); }
        public void Notify(CommandMessageType type, int value1) { Notify(type, value1, 0, 0, ""); }
        public void Notify(CommandMessageType type, int value1, string stringValue) { Notify(type, value1, 0, 0, stringValue); }
        public void Notify(CommandMessageType type, int value1, int value2) { Notify(type, value1, value2, 0, ""); }
        public void Notify(CommandMessageType type, int value1, int value2, int value3) { Notify(type, value1, value2, value3, ""); }
        public void Notify(CommandMessageType type, int value1, int value2, string stringValue) { Notify(type, value1, value2, 0, stringValue); }
        public void Notify(CommandMessageType type, int value1, int value2, int value3, string stringValue) { Notify(type, value1, value2, value3, 0, stringValue); }
        public void Notify(CommandMessageType type, int value1, int value2, int value3, int value4) { Notify(type, value1, value2, value3, value4, ""); }
        public void Notify(CommandMessageType type, int value1, int value2, int value3, int value4, string stringValue)
        {
            byte[] command;
            switch (type)
            {
                case CommandMessageType.NotifyCriminalCaptured:
                    command = new byte[35];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(10), 0, command, 10, 10);
                    Buffer.BlockCopy(rewardGold.GetBytes(), 0, command, 20, 4);
                    Buffer.BlockCopy(experience.GetBytes(), 0, command, 24, 4);
                    break;
                case CommandMessageType.NotifyParty:
                    switch (value1)
                    {
                        case 4:
                        case 6:
                            command = new byte[25];
                            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                            Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, command, 10, 2);
                            Buffer.BlockCopy(stringValue.GetBytes(10), 0, command, 12, 10);
                            break;
                        case 5:
                            command = new byte[15+value3*11];
                            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                            Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, command, 10, 2);
                            Buffer.BlockCopy(stringValue.GetBytes(value3*11), 0, command, 12, value3*11);
                            break;
                        default:
                            command = new byte[20];
                            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                            Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, command, 10, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)value4), 0, command, 12, 2);
                            break;
                    }
                    break;
                case CommandMessageType.NotifyPartyRequest:
                    command = new byte[10+stringValue.Length];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(stringValue.Length), 0, command, 6, stringValue.Length);
                    break;
                case CommandMessageType.NotifyItemBagPositions:
                    command = new byte[10 + (4 * Globals.MaximumInventoryItems)];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    for (int i = 0; i < Globals.MaximumInventoryItems; i++)
                        if (inventory[i] != null)
                        {
                            Buffer.BlockCopy(inventory[i].BagPosition.X.GetBytes(), 0, command, 6 + (i * 4), 2);
                            Buffer.BlockCopy(inventory[i].BagPosition.Y.GetBytes(), 0, command, 6 + (i * 4) + 2, 2);
                        }
                        else
                        {
                            Buffer.BlockCopy((40).GetBytes(), 0, command, 6 + (i * 4), 2);
                            Buffer.BlockCopy((30).GetBytes(), 0, command, 6 + (i * 4) + 2, 2);
                        }
                    break;
                case CommandMessageType.NotifyCrusadeTeleportLocation:
                    command = new byte[40];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(Guild.CrusadeTeleportLocation.X.GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy(Guild.CrusadeTeleportLocation.Y.GetBytes(), 0, command, 8, 2);
                    Buffer.BlockCopy(Guild.CrusadeTeleportLocation.MapName.GetBytes(10), 0, command, 10, 10);
                    Buffer.BlockCopy(Guild.CrusadeBuildLocation.X.GetBytes(), 0, command, 20, 2);
                    Buffer.BlockCopy(Guild.CrusadeBuildLocation.Y.GetBytes(), 0, command, 22, 2);
                    Buffer.BlockCopy(Guild.CrusadeBuildLocation.MapName.GetBytes(10), 0, command, 24, 10);
                    break;
                case CommandMessageType.NotifyDead:
                case CommandMessageType.NotifyPlayerNotOnline:
                    command = new byte[35];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 6, 20);
                    break;
                case CommandMessageType.JoinGuildApprove:
                    command = new byte[35];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 6, 20);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 26, 2);
                    break;
                case CommandMessageType.NotifyGuildAdmissionRequest:
                case CommandMessageType.NotifyGuildDismissalRequest:
                case CommandMessageType.NotifyWhisperOn:
                case CommandMessageType.NotifyWhisperOff:
                    command = new byte[25];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(10), 0, command, 6, 10);
                    break;
                case CommandMessageType.NotifyReputationSuccess:
                    command = new byte[30];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    command[6] = (byte)value1;
                    Buffer.BlockCopy(stringValue.GetBytes(10), 0, command, 7, 10);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 17, 4);
                    break;
                case CommandMessageType.NotifyGuildDisbanded:
                    command = new byte[45];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(loadedGuildName.GetBytes(20), 0, command, 7, 20);
                    Buffer.BlockCopy(stringValue.GetBytes(10), 0, command, 27, 10);
                    break;
                case CommandMessageType.NotifyStudySpellSuccess:
                    command = new byte[45];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    command[6] = (byte)value1;
                    Buffer.BlockCopy(stringValue.GetBytes(30), 0, command, 7, 30);
                    break;
                case CommandMessageType.NotifyStudySpellFailed:
                    command = new byte[50];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    command[6] = 0; // cFailCode - unused
                    command[7] = (byte)value1;
                    Buffer.BlockCopy(stringValue.GetBytes(30), 0, command, 8, 30);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 38, 4);
                    Buffer.BlockCopy(value3.GetBytes(), 0, command, 42, 4);
                    break;
                case CommandMessageType.NotifyStudySkillSuccess:
                    command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    command[6] = (byte)value1;
                    command[7] = (byte)value2;
                    break;
                case CommandMessageType.NotifyItemColourChange:
                case CommandMessageType.NotifySkill:
                case CommandMessageType.NotifySpecialAbilityStatus:
                case CommandMessageType.NotifyCrusadeStatistics:
                case CommandMessageType.NotifyFly:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, command, 10, 2);
                    break;
                case CommandMessageType.NotifyHeldenianStatistics:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, command, 10, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value4), 0, command, 12, 2);
                    break;
                case CommandMessageType.NotifyFishingStarted:
                    command = new byte[40];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, command, 10, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 12, 20);
                    break;
                case CommandMessageType.NotifyItemUpgrade:
                    command = new byte[50];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    command[8] = (byte)((int)inventory[value1].Type);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)inventory[value1].Endurance), 0, command, 9, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)inventory[value1].Sprite), 0, command, 11, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)inventory[value1].SpriteFrame), 0, command, 13, 2);
                    command[15] = (byte)((int)inventory[value1].Colour);
                    command[16] = (byte)((int)inventory[value1].SpecialEffect2);
                    Buffer.BlockCopy(BitConverter.GetBytes(inventory[value1].Attribute), 0, command, 17, 4);
                    Buffer.BlockCopy(inventory[value1].Name.GetBytes(20), 0, command, 21, 20);
                    break;
                case CommandMessageType.NotifyCriticals:
                case CommandMessageType.NotifyFishingChance:
                case CommandMessageType.NotifySkillEnd:
                case CommandMessageType.NotifyCrusadeStrikeIncoming:
                case CommandMessageType.NotifyHeldenianVictory:
                case CommandMessageType.NotifyReputationFailed:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(((short)value1).GetBytes(), 0, command, 6, 2);
                    break;
                case CommandMessageType.NotifyEnemyKills:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 4);
                    break;
                case CommandMessageType.NotifyCriminalCount:
                    command = new byte[45];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(experience.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(strength.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(vitality.GetBytes(), 0, command, 14, 4);
                    Buffer.BlockCopy(dexterity.GetBytes(), 0, command, 18, 4);
                    Buffer.BlockCopy(intelligence.GetBytes(), 0, command, 22, 4);
                    Buffer.BlockCopy(magic.GetBytes(), 0, command, 26, 4);
                    Buffer.BlockCopy(charisma.GetBytes(), 0, command, 30, 4);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 34, 4);
                    break;
                case CommandMessageType.NotifySpellsAndSkills:
                case CommandMessageType.NotifyStatChangeMajesticsSuccess:
                    command = new byte[180];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    for (int spell = 6; spell < 6 + Globals.MaximumSpells; spell++)
                        command[spell] = (byte)Convert.ToInt32(spellStatus[spell - 6]);
                    for (int skill = 6 + Globals.MaximumSpells; skill < 6 + Globals.MaximumSpells + Globals.MaximumSkills; skill++)
                        if (Enum.IsDefined(typeof(SkillType), skill - 6 - Globals.MaximumSpells) && skills.ContainsKey((SkillType)skill - 6 - Globals.MaximumSpells)) // check this skill exists
                            command[skill] = (byte)skills[(SkillType)skill - 6 - Globals.MaximumSpells].Level;
                        else command[skill] = 0;
                    break;
                case CommandMessageType.NotifyExperience:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(experience), 0, command, 6, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(reputation), 0, command, 10, 4);
                    break;
                case CommandMessageType.NotifyStatChangeLevelUpSuccess:
                case CommandMessageType.NotifyLevelUp:
                    command = new byte[40];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(level), 0, command, 6, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(strength), 0, command, 10, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(vitality), 0, command, 14, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(dexterity), 0, command, 18, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(intelligence), 0, command, 22, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(magic), 0, command, 26, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(charisma), 0, command, 30, 4);
                    break;
                case CommandMessageType.NotifyHP:
                case CommandMessageType.NotifyMP:
                case CommandMessageType.NotifySP:
                case CommandMessageType.NotifyHunger:
                case CommandMessageType.NotifyItemSold:
                case CommandMessageType.NotifyItemRepaired:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(value1), 0, command, 6, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, command, 10, 4);
                    break;
                case CommandMessageType.NotifyCrusade:
                    command = new byte[30];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(value1), 0, command, 6, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes((int)crusadeDuty), 0, command, 10, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, command, 14, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(value3), 0, command, 18, 4);
                    break;
                case CommandMessageType.NotifyTimeChange:
                case CommandMessageType.NotifyWeatherChange:
                case CommandMessageType.NotifySafeMode:
                case CommandMessageType.NotifyNotEnoughGold: // command[6] is item index
                    command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    command[6] = (byte)value1;
                    break;
                case CommandMessageType.NotifyDropItemAndErase:
                case CommandMessageType.NotifyItemUnequipped:
                case CommandMessageType.NotifyItemDepleted:
                case CommandMessageType.NotifyItemBroken:
                case CommandMessageType.NotifyItemCount:
                case CommandMessageType.NotifyMagicEffectOn:
                case CommandMessageType.NotifyMagicEffectOff:
                case CommandMessageType.NotifyItemUpgradeFailed:
                case CommandMessageType.NotifyManufactureSuccess:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, command, 8, 4);
                    command[12] = (byte)value3;
                    break;
                case CommandMessageType.NotifyGiveItemAndErase:
                    command = new byte[35];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, command, 8, 4);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 12, 20);
                    break;
                case CommandMessageType.NotifyGuildName:
                case CommandMessageType.NotifySellItemFailed:
                    command = new byte[35];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 10, 20);
                    break;
                case CommandMessageType.NotifyAngelStats:
                    command = new byte[30];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(value3.GetBytes(), 0, command, 14, 4);
                    Buffer.BlockCopy(value4.GetBytes(), 0, command, 18, 4);
                    break;
                case CommandMessageType.NotifySellItemQuote:
                case CommandMessageType.NotifyRepairItemQuote:
                    command = new byte[50];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(value3.GetBytes(), 0, command, 14, 4);
                    Buffer.BlockCopy(value4.GetBytes(), 0, command, 18, 4);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 22, 20);
                    break;
                case CommandMessageType.NotifyMajestics:
                    command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, command, 8, 4);
                    break;
                case CommandMessageType.NotifyStatChangeMajesticsFailed:
                case CommandMessageType.NotifyStatChangeLevelUpFailed:
                case CommandMessageType.NotifyInventoryFull:
                case CommandMessageType.NotifyWarehouseFull:
                case CommandMessageType.NotifySpecialAbilityEnabled:
                case CommandMessageType.NotifyFarmingSkillTooLow:
                case CommandMessageType.NotifyFarmingInvalidLocation:
                case CommandMessageType.NotifyFishingSuccess:
                case CommandMessageType.NotifyFishingFailed:
                case CommandMessageType.NotifyCrusadeMeteorStrike:
                case CommandMessageType.NotifyCrusadeStructureLimitReached:
                case CommandMessageType.NotifyHeldenian:
                case CommandMessageType.NotifyHeldenianStarted:
                case CommandMessageType.NotifyHeldenianEnded:
                case CommandMessageType.NotifyHeldenianFlagFailed:
                case CommandMessageType.NotifyApocalypseStart:
                case CommandMessageType.NotifyApocalypseEnd:
                case CommandMessageType.NotifyApocalypseGateClosed:
                case CommandMessageType.NotifyManufactureFailed:
                case CommandMessageType.NotifyTravellerLimit:
                default:
                    command = new byte[10];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    break;
            }

            byte[] data = Utility.Encrypt(command, false);
            if (clientInfo != null) clientInfo.Send(data);
        }

        public void Notify(CommandMessageType type, object dataObject)
        {
            byte[] command;
            switch (type)
            {
                case CommandMessageType.NotifyEnemyKillReward:
                    Character victim = (Character)dataObject;
                    command = new byte[50];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(experience.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(enemyKills.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(victim.Name.GetBytes(10), 0, command, 14, 10);
                    Buffer.BlockCopy(victim.Guild.Name.GetBytes(20), 0, command, 24, 20);
                    Buffer.BlockCopy(victim.GuildRank.GetBytes(), 0, command, 44, 2);
                    Buffer.BlockCopy(crusadeWarContribution.GetBytes(), 0, command, 46, 2);
                    break;
                case CommandMessageType.NotifyApocalypseGateOpen:
                    Location location = (Location)dataObject;
                    command = new byte[30];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(location.X.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(location.Y.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(location.MapName.GetBytes(10), 0, command, 14, 10);                
                    break;
                case CommandMessageType.NotifyCrusadeMeteorStrikeResult:
                    CrusadeStrikeResult result = (CrusadeStrikeResult)dataObject;
                    command = new byte[30 + (result.StrikePointsHP.Length*2)];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(result.StructuresDestroyed.GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy(result.StructuresDamaged.GetBytes(), 0, command, 8, 2);
                    Buffer.BlockCopy(result.Casualties.GetBytes(), 0, command, 10, 2);
                    Buffer.BlockCopy(result.MapName.GetBytes(10), 0, command, 12, 10);
                    Buffer.BlockCopy(result.RemainingStructures.GetBytes(), 0, command, 22, 2);

                    if (result.StrikePointsHP.Length > 0)
                    {
                        Buffer.BlockCopy(result.StrikePointsHP.Length.GetBytes(), 0, command, 24, 2);
                        for (int i = 0; i < result.StrikePointsHP.Length; i++)
                            Buffer.BlockCopy(result.StrikePointsHP[i].GetBytes(), 0, command, 26 + (i * 2), 2);
                    }
                    else Buffer.BlockCopy(0.GetBytes(), 0, command, 24, 2);

                    break;
                default:
                    command = new byte[10];
                    break;
            }

            byte[] data = Utility.Encrypt(command, false);
            if (clientInfo != null) clientInfo.Send(data);
        }

        public void Notify(CommandMessageType type, byte[] sendData)
        {
            byte[] command;
            switch (type)
            {
                case CommandMessageType.NotifyItemObtained:
                case CommandMessageType.NotifyItemToWarehouse:
                case CommandMessageType.NotifyMapStatusNext:
                case CommandMessageType.NotifyMapStatusLast:
                    command = new byte[sendData.Length + 15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(sendData, 0, command, 6, sendData.Length);
                    break;
                default:
                    command = new byte[10];
                    break;
            }

            byte[] data = Utility.Encrypt(command, false);
            if (clientInfo != null) clientInfo.Send(data);
        }

        public int ID { get { return id; } }
        public String DatabaseID { get { return databaseID; } }
        public String Name { get { return name; } }
        public String Profile { get { return profile; } }
        public int Reputation { get { return reputation; } set { reputation = value; } }
        public int Strength { get { return strength; } }
        public int Dexterity { get { return dexterity; } }
        public int Vitality { get { return vitality; } }
        public int Magic { get { return magic; } }
        public int Intelligence { get { return intelligence; } }
        public int Charisma { get { return charisma; } }
        public int StrengthBonus { get { return strengthBonus; } }
        public int DexterityBonus { get { return dexterityBonus; } }
        public int VitalityBonus { get { return vitalityBonus; } }
        public int MagicBonus { get { return magicBonus; } }
        public int IntelligenceBonus { get { return intelligenceBonus; } }
        public int CharismaBonus { get { return charismaBonus; } }
        public string LoadedGuildName { get { return loadedGuildName; } }
        public Guild Guild { get { return guild; } set { guild = value; } }
        public int GuildRank { get { return guildRank; } set { guildRank = value; } }
        public bool HasGuild { get { return (!guild.Equals(Guild.None)); } }
        public Party Party { get { return party; } set { party = value; } }
        public bool HasParty { get { return (party != null); } }
        public int PartyRequestID { get { return partyRequestID; } set { partyRequestID = value; } }
        public GenderType Gender { get { return gender; } }
        public SkinType Skin { get { return skin; } }
        public int HairColour { get { return hairColour; } }
        public int HairStyle { get { return hairStyle; } }
        public int UnderwearColour { get { return underwearColour; } }
        public int Appearance1 { get { return appearance1; } }
        public int Appearance2 { get { return appearance2; } }
        public int Appearance3 { get { return appearance3; } }
        public int Appearance4 { get { return appearance4; } }
        public int AppearanceColour { get { return appearanceColour; } }
        public int Level { get { return level; } }
        public long Experience { get { return experience; } }
        public long ExperienceStored { get { return experienceStored; } set { experienceStored = value; } }
        public int BonusManaSave { get { return bonusManaSave; } }
        public int BonusPhysicalAbsorption { get { return bonusPhysicalAbsorption; } }
        public int BonusMagicAbsorption { get { return bonusMagicAbsorption; } }
        public DateTime LastLogin { get { return lastLogin; } }
        public OwnerSide Town { get { return town; } }
        public OwnerSide Side { get { return town; } }
        public int Type { get { return type; } }
        public OwnerType OwnerType { get { return OwnerType.Player; } }
        public OwnerSize Size { get { return OwnerSize.Small; } }
        public int Status { get { return status; } }
        public Dictionary<MagicType, MagicEffect> MagicEffects { get { return magicEffects; } }
        public Boolean IsBerserked { get { return magicEffects.ContainsKey(MagicType.Berserk); } }
        public Boolean IsInvisible { get { return magicEffects.ContainsKey(MagicType.Invisibility); } }
        public Boolean IsSafeMode { get { return isSafeMode; } }
        public Boolean IsCivilian { get { return isCivilian; } }
        public Boolean IsCombatMode { get { return (this.appearance2 == (0xF000 | appearance2)); } }
        public Boolean IsDead { get { return isDead; } }
        public Boolean IsAdmin { get { return (adminLevel > 0); } }
        public int TotalLogins { get { return totalLogins; } set { totalLogins = value; } }
        public bool CanFly { get { return canFly; } set { canFly = value; } }
        public bool ShowDamageEnabled { get { return showDamageEnabled; } set { showDamageEnabled = value; } }
        public int LastDamage { get { return lastDamage; } set { lastDamage = value; } }

        /// <summary>
        /// The ID associated with the player's TCP socket connection.
        /// </summary>
        public int ClientID
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// ClientInfo object associated with the player's TCP socket connection.
        /// </summary>
        public ClientInfo ClientInfo
        {
            get { return clientInfo; }
            set { clientInfo = value; }
        }

        /// <summary>
        /// Gets the current direction in which this character is facing.
        /// </summary>
        public MotionDirection Direction
        {
            get { return direction; }
        }

        public MapTile CurrentLocation
        {
            get { return map[mapY][mapX]; }
        }

        public World CurrentWorld { set { world = value; } get { return world; } }
        public Map CurrentMap { set { map = value; } get { return map; } }

        public string LoadedMapName
        {
            get { return loadedMapName; }
        }

        /// <summary>
        /// Last X coordinate of this character's location.
        /// </summary>
        public int LastX
        {
            get { return prevMapX; }
        }

        /// <summary>
        /// Last Y coordinate of this character's location.
        /// </summary>
        public int LastY
        {
            get { return prevMapY; }
        }


        /// <summary>
        /// Current X coordinate of this character's location.
        /// </summary>
        public int X
        {
            get { return mapX; }
        }

        /// <summary>
        /// Current Y coordinate of this character's location.
        /// </summary>
        public int Y
        {
            get { return mapY; }
        }

        /// <summary>
        /// List of items in the character's bag.
        /// </summary>
        public List<Item> Inventory
        {
            get { return inventory; }
            set { inventory = value; }
        }

        /// <summary>
        /// List of items in the character's warehouse.
        /// </summary>
        public List<Item> Warehouse
        {
            get { return warehouse; }
            set { warehouse = value; }
        }


        /// <summary>
        /// Gets the character's current hit points.
        /// </summary>
        public int HP
        {
            get { return hp; }
        }

        /// <summary>
        /// Calculates the maximum hit points this character can have.
        /// </summary>
        public int MaxHP
        {
            get {   
                int value = ((vitality + vitalityBonus) * 3) + (level * 2) + ((strength + strengthBonus) / 2);

                // blood weapons reduce max hp by 20%
                if (Weapon != null && Weapon.SpecialAbilityType == ItemSpecialAbilityType.Blood)
                    value -= (value / 5);

                return value;
            }
        }

        /// <summary>
        /// Gets the character's current magic points.
        /// </summary>
        public int MP
        {
            get { return mp; }
        }

        /// <summary>
        /// Calculates the maximum magic points this character can have.
        /// </summary>
        public int MaxMP
        {
            get { return ((magic + magicBonus) * 2) + (level * 2) + ((intelligence + intelligenceBonus) / 2); }
        }

        /// <summary>
        /// Gets the character's current stamina points.
        /// </summary>
        public int SP
        {
            get { return sp; }
        }

        /// <summary>
        /// Calculates the maximum stamina points this character can have.
        /// </summary>
        public int MaxSP
        {
            get { return ((strength + strengthBonus) * 2) + (level * 2); }
        }

        public int Criticals { get { return criticals; } }
        public int MaxCriticals { get { return level / 10; } }
        public int Majestics { get { return majestics; } set { majestics = value; } }
        public List<int> Summons { get { return summons; } set { summons = value; } }
        public Dictionary<SkillType, Skill> Skills { get { return skills; } }
        public List<Quest> ActiveQuests { get { return activeQuests; } }
        public int[] Equipment { get { return equipment; } }
        public int CrusadeConstructionPoints { get { return crusadeConstructionPoints; } set { crusadeConstructionPoints = value; } }
        public int CrusadeWarContribution { get { return crusadeWarContribution; } set { crusadeWarContribution = value; } }
        public CrusadeDuty CrusadeDuty { get { return crusadeDuty; } set { crusadeDuty = value; } }

        /// <summary>
        /// Formats the spell status for saving in the database.
        /// </summary>
        public string SpellStatus
        {
            get
            {
                StringBuilder status = new StringBuilder();
                for (int i = 0; i < Globals.MaximumSpells; i++) status.Append(Convert.ToInt32(spellStatus[i]));
                return status.ToString();
            }
        }

        /// <summary>
        /// Formats the skill status for saving in the database.
        /// </summary>
        public string SkillStatus
        {
            get
            {
                StringBuilder status = new StringBuilder();
                for (int i = 0; i < Globals.MaximumSkills; i++)
                    if (Enum.IsDefined(typeof(SkillType), i) && skills.ContainsKey((SkillType)i)) // check this skill exists
                        status.Append(skills[(SkillType)i].Experience + " ");
                    else status.Append("0 ");

                return status.ToString();
            }
        }

        /// <summary>
        /// Gets the current hunger level of the character. Ranging from 0 to 100.
        /// </summary>
        public int Hunger
        {
            get { return hunger; }
        }

        /// <summary>
        /// Calculates the character's available level up points.
        /// </summary>
        public int AvailableLevelUpPoints
        {
            get { return ((level * 3) - ((strength + dexterity + intelligence + vitality + charisma + magic) - 70)); }
        }

        /// <summary>
        /// Calculates the character's total allowed stat points.
        /// </summary>
        public int TotalStatPoints
        {
            get { return (((level - 1) * 3) + 70); }
        }

        public Item Gold { get { return (GoldIndex != -1) ? inventory[GoldIndex] : null; } }
        public int GoldIndex
        {
            get
            {
                for (int i = 0; i < Globals.MaximumInventoryItems; i++)
                    if (inventory[i] != null && inventory[i].Name.Equals("Gold"))
                        return i;

                return -1;
            }
        }

        public bool HasArrows { get { return (Arrows != null); } }
        public Item Arrows { get { return (ArrowsIndex != -1) ? inventory[ArrowsIndex] : null; } }
        public int ArrowsIndex
        {
            get
            {
                for (int i = 0; i < Globals.MaximumInventoryItems; i++)
                    if (inventory[i] != null && inventory[i].Type == ItemType.Arrow)
                        return i;

                return -1;
            }
        }

        /// <summary>
        /// Quick access to the Weapon currently equipped.
        /// </summary>
        public Item Weapon { get { return (WeaponIndex != -1) ? inventory[WeaponIndex] : null; } }
        public int WeaponIndex
        {
            get
            {
                if (equipment[(int)EquipType.DualHand] != -1)
                    return equipment[(int)EquipType.DualHand];
                else if (equipment[(int)EquipType.RightHand] != -1)
                    return equipment[(int)EquipType.RightHand];
                else return -1;
            }
        }

        /// <summary>
        /// Quick access to the Shield currently equipped.
        /// </summary>
        public Item Shield { get { return (ShieldIndex != -1) ? inventory[ShieldIndex] : null; } }
        public int ShieldIndex
        {
            get
            {
                if (equipment[(int)EquipType.LeftHand] != -1)
                    return equipment[(int)EquipType.LeftHand];
                else return -1;
            }
        }

        /// <summary>
        /// Quick access to the Necklace currently equipped.
        /// </summary>
        public Item Necklace { get { return (NecklaceIndex != -1) ? inventory[NecklaceIndex] : null; } }
        public int NecklaceIndex
        {
            get
            {
                if (equipment[(int)EquipType.Neck] != -1)
                    return equipment[(int)EquipType.Neck];
                else return -1;
            }
        }

        /// <summary>
        /// Quick access to the Helmet currently equipped.
        /// </summary>
        public Item Helmet { get { return (HelmetIndex != -1) ? inventory[HelmetIndex] : null; } }
        public int HelmetIndex
        {
            get
            {
                if (equipment[(int)EquipType.Head] != -1)
                    return equipment[(int)EquipType.Head];
                else return -1;
            }
        }

        /// <summary>
        /// Quick access to the Body Armour currently equipped.
        /// </summary>
        public Item BodyArmour { get { return (BodyArmourIndex != -1) ? inventory[BodyArmourIndex] : null; } }
        public int BodyArmourIndex
        {
            get
            {
                if (equipment[(int)EquipType.Body] != -1)
                    return equipment[(int)EquipType.Body];
                else return -1;
            }
        }

        /// <summary>
        /// Quick access to the Hauberk currently equipped.
        /// </summary>
        public Item Hauberk { get { return (HauberkIndex != -1) ? inventory[HauberkIndex] : null; } }
        public int HauberkIndex
        {
            get
            {
                if (equipment[(int)EquipType.Arms] != -1)
                    return equipment[(int)EquipType.Arms];
                else return -1;
            }
        }

        /// <summary>
        /// Quick access to the Leggings currently equipped.
        /// </summary>
        public Item Leggings { get { return (LeggingsIndex != -1) ? inventory[LeggingsIndex] : null; } }
        public int LeggingsIndex
        {
            get
            {
                if (equipment[(int)EquipType.Legs] != -1)
                    return equipment[(int)EquipType.Legs];
                else return -1;
            }
        }

        /// <summary>
        /// Quick access to the Boots currently equipped.
        /// </summary>
        public Item Boots { get { return (BootsIndex != -1) ? inventory[BootsIndex] : null; } }
        public int BootsIndex
        {
            get
            {
                if (equipment[(int)EquipType.Feet] != -1)
                    return equipment[(int)EquipType.Feet];
                else return -1;
            }
        }

        /// <summary>
        /// Quick access to the Cape currently equipped.
        /// </summary>
        public Item Cape { get { return (CapeIndex != -1) ? inventory[CapeIndex] : null; } }
        public int CapeIndex
        {
            get
            {
                if (equipment[(int)EquipType.Back] != -1)
                    return equipment[(int)EquipType.Back];
                else return -1;
            }
        }

        /// <summary>
        /// Quick access to the Costume currently equipped.
        /// </summary>
        public Item Costume { get { return (CostumeIndex != -1) ? inventory[CostumeIndex] : null; } }
        public int CostumeIndex
        {
            get
            {
                if (equipment[(int)EquipType.FullBody] != -1)
                    return equipment[(int)EquipType.FullBody];
                else return -1;
            }
        }

        // TODO - differentiate between warr hero and mage hero
        public bool IsHero
        {
            get
            {
                return (((Helmet != null && Helmet.Name.StartsWith("aHero")) &&
                        (Hauberk != null && Hauberk.Name.StartsWith("aHero")) &&
                        (BodyArmour != null && BodyArmour.Name.StartsWith("aHero")) &&
                        (Leggings != null && Leggings.Name.StartsWith("aHero")))
                        ||
                        (Helmet != null && Helmet.Name.StartsWith("eHero")) &&
                        (Hauberk != null && Hauberk.Name.StartsWith("eHero")) &&
                        (BodyArmour != null && BodyArmour.Name.StartsWith("eHero")) &&
                        (Leggings != null && Leggings.Name.StartsWith("eHero")));
            }
        }

        public int InventoryCount
        {
            get
            {
                int count = 0;
                foreach (Item item in inventory)
                    if (item != null) count++;
                return count;
            }
        }

        public int WarehouseCount
        {
            get
            {
                int count = 0;
                foreach (Item item in warehouse)
                    if (item != null) count++;
                return count;
            }
        }

        public bool IsCriminal { get { return (criminalCount > 0); } }
        public int CriminalCount { set { criminalCount = value; } get { return criminalCount; } }
        public int EnemyKills { set { enemyKills = value; } get { return enemyKills; } }
        public TimeSpan SpecialAbilityTime { get { return specialAbilityTime; } }
        public bool SpecialAbilityEnabled { get { return specialAbilityEnabled; } }
        public Item SpecialAbilityItem { get { return inventory[specialAbilityItemIndex]; } }
        public TimeSpan ReputationTime { get { return reputationTime; } set { reputationTime = value; } }
        public TimeSpan MuteTime { get { return muteTime; } set { muteTime = value; } }
        public bool IsMuted { get { return muteTime.TotalSeconds > 0; } }
        public bool IsWhispering { get { return (whisperID != -1); } }
        public int WhisperIndex { get { return whisperID; } set { whisperID = value; } }

        private void OnSkillLevelUp(Skill skill)
        {
            // handles stat limiters
            switch (skill.Type)
            {
                case SkillType.Archery:
                case SkillType.Axe:
                case SkillType.Fencing:
                case SkillType.Hammer:
                case SkillType.LongSword:
                case SkillType.Shield:
                case SkillType.ShortSword:
                    if (dexterity * 2 < skill.Level)
                    {
                        skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
                        return;
                    }
                    break;
                case SkillType.Staff:
                case SkillType.Magic:
                    if (magic * 2 < skill.Level)
                    {
                        skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
                        return;
                    }
                    break;
                case SkillType.Mining:
                case SkillType.Manufacturing:
                case SkillType.Hand:
                    if (strength * 2 < skill.Level)
                    {
                        skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
                        return;
                    }
                    break;
                case SkillType.MagicResistance:
                    if (level * 2 < skill.Level)
                    {
                        skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
                        return;
                    }
                    break;
                case SkillType.PoisonResistance:
                    if (vitality * 2 < skill.Level)
                    {
                        skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
                        return;
                    }
                    break;
                case SkillType.Alchemy:
                case SkillType.PretendCorpse:
                    if (intelligence * 2 < skill.Level)
                    {
                        skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
                        return;
                    }
                    break;
            }

            Notify(CommandMessageType.NotifySkill, (int)skill.Type, skill.Level);
        }
    }
}