﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;

namespace HelbreathWorld.Common.Assets
{
    public class Item : IEquatable<Item>
    {
        private int count;
        private string name;
        private string friendlyName;
        private int sprite; // appearance in bag/on ground
        private int spriteFrame; // appearance in bag/on ground
        private int colour;
        private int appearance; // appearance on character
        private int levelLimit;
        private int speed;
        private int category;

        private ItemRarity rarity;
        private EquipType equipType;
        private ItemType type;
        private ItemEffectType effectType;
        private GenderType genderLimit;

        private int maximumEndurance;
        private int endurance;
        private int weight;
        private int price;
        private bool isUpgradeable;
        private bool isForSale;
        private bool hasExtendedRange;
        private uint attribute;
        private int effect1;
        private int effect2;
        private int effect3;
        private int effect4;
        private int effect5;
        private int effect6;
        private ItemSpecialAbilityType specialAbilityType;
        private int specialEffect1;
        private int specialEffect2;
        private int specialEffect3;
        private SkillType relatedSkill;
        private int stripBonus;

        private int minimumStrength;
        private int minimumIntelligence;
        private int minimumDexterity;
        private int minimumMagic;
        private int minimumVitality;
        private int minimumCharisma;

        // character bindings
        private string boundID;
        private string boundName;
        private bool isEquipped;
        private Location bagPosition;

        private DateTime dropTime;

        // client
        private int bagX;
        private int bagY;
        private int bagOldX;
        private int bagOldY;

        public Item()
        {

        }

        public Item(string itemName)
        {
            this.name = itemName;
        }

        public Item(int sprite, int spriteFrame, int colour)
        {
            this.sprite = sprite;
            this.spriteFrame = spriteFrame;
            this.colour = colour;
        }

        public void ParseData(byte[] data)
        {
            int index = 0;

            if (data.Length == 46) // when obtained
            {
                //data = 1 ?
                index++;
            }
            name = Encoding.ASCII.GetString(data, index, 20).Trim('\0');
            index += 20;
            count = BitConverter.ToInt32(data, index);
            index += 4;
            type = (ItemType)((int)data[index]);
            index++;
            equipType = (EquipType)((int)data[index]);
            index++;
            if (data.Length == 44) // during init
            {
                isEquipped = ((int)data[index] == 1) ? true : false;
                index++;
            }
            levelLimit = BitConverter.ToInt16(data, index);
            index += 2;
            genderLimit = (GenderType)((int)data[index]);
            index++;
            endurance = BitConverter.ToInt16(data, index);
            index += 2;
            weight = BitConverter.ToInt16(data, index);
            index += 2;
            sprite = BitConverter.ToInt16(data, index);
            index += 2;
            spriteFrame = BitConverter.ToInt16(data, index);
            index += 2;
            colour = (int)data[index];
            index++;
            specialEffect2 = (int)data[index];
            index++;
            attribute = BitConverter.ToUInt32(data, index);
            index += 4;
        }

        public static Item Parse(byte[] data)
        {
            Item i = new Item();
            i.ParseData(data);
            return i;
        }

        /// <summary>
        /// Creates an empty Item with Sprite, SpriteFrame and Colour set to 0. Used for emptying a cell on the client side.
        /// </summary>
        /// <returns>Item object with Sprite, SpriteFrame and Colour set to 0.</returns>
        public static Item Empty()
        {
            return new Item(0, 0, 0);
        }

        public Item Copy() { return Copy(1); }
        public Item Copy(int itemCount)
        {
            Item item = new Item(name);
            item.FriendlyName = friendlyName;
            item.count = itemCount;
            item.Type = type;
            item.EquipType = equipType;
            item.EffectType = effectType;
            item.Effect1 = effect1;
            item.Effect2 = effect2;
            item.Effect3 = effect3;
            item.Effect4 = effect4;
            item.Effect5 = effect5;
            item.Effect6 = effect6;
            item.Endurance = item.MaximumEndurance = maximumEndurance;
            item.SpecialAbilityType = specialAbilityType;
            item.Sprite = sprite;
            item.SpriteFrame = spriteFrame;
            item.Price = price;
            item.Weight = weight;
            item.Appearance = appearance;
            item.Speed = speed;
            item.LevelLimit = levelLimit;
            item.GenderLimit = genderLimit;
            item.SpecialEffect1 = specialEffect1;
            item.SpecialEffect2 = specialEffect2;
            item.RelatedSkill = relatedSkill;
            item.Category = category;
            item.Colour = colour;
            item.Rarity = rarity;
            item.IsForSale = isForSale;
            item.IsUpgradeable = isUpgradeable;
            item.HasExtendedRange = hasExtendedRange;

            // item bindings - new/copied items are unbound
            item.BoundID = Globals.UnboundItem;
            item.BoundName = "Unbound";
            item.bagPosition = new Location(40, 30);

            return item;
        }

        public static Item ParseXml(XmlReader r)
        {
            Item item = new Item(r["Name"]);
            item.FriendlyName = (r["FriendlyName"] != null) ? r["FriendlyName"] : item.Name;
            ItemType itemType;
            if (Enum.TryParse<ItemType>(r["Type"], out itemType))
                item.Type = itemType;
            EquipType equipType;
            if (Enum.TryParse<EquipType>(r["EquipType"], out equipType))
                item.EquipType = equipType;
            ItemEffectType effectType;
            if (Enum.TryParse<ItemEffectType>(r["EffectType"], out effectType))
                item.EffectType = effectType;
            ItemRarity rarity;
            if (Enum.TryParse<ItemRarity>(r["Rarity"], out rarity))
                item.Rarity = rarity;
            else item.Rarity = ItemRarity.None; // means it cannot drop. fail safe in case configs are incorrect and rares drop too common!
            item.Effect1 = Int32.Parse(r["Effect1"]);
            item.Effect2 = Int32.Parse(r["Effect2"]);
            item.Effect3 = Int32.Parse(r["Effect3"]);
            item.Effect4 = Int32.Parse(r["Effect4"]);
            item.Effect5 = Int32.Parse(r["Effect5"]);
            item.Effect6 = Int32.Parse(r["Effect6"]);
            item.MaximumEndurance = Int32.Parse(r["MaximumEndurance"]);
            ItemSpecialAbilityType specialAbilityType;
            if (Enum.TryParse<ItemSpecialAbilityType>(r["SpecialAbilityType"], out specialAbilityType))
                item.SpecialAbilityType = specialAbilityType;
            item.Sprite = Int32.Parse(r["Sprite"]);
            item.SpriteFrame = Int32.Parse(r["SpriteFrame"]);
            if (r["Price"] != null)
                item.Price = Int32.Parse(r["Price"]);
            else item.Price = 0;
            item.Appearance = Int32.Parse(r["Appearance"]);
            item.Speed = Int32.Parse(r["Speed"]);
            item.LevelLimit = Int32.Parse(r["LevelLimit"]);
            GenderType gender;
            if (Enum.TryParse<GenderType>(r["GenderLimit"], out gender))
                item.GenderLimit = gender;
            else item.GenderLimit = GenderType.None;
            item.SpecialEffect1 = Int32.Parse(r["SpecialEffect1"]);
            item.SpecialEffect2 = Int32.Parse(r["SpecialEffect2"]);
            SkillType skill;
            if (Enum.TryParse<SkillType>(r["RelatedSkill"], out skill))
                item.RelatedSkill = skill;
            else item.RelatedSkill = SkillType.None;
            item.Category = Int32.Parse(r["Category"]);
            item.Colour = Int32.Parse(r["Colour"]);
            item.IsForSale = (r["IsForSale"] != null) ? Boolean.Parse(r["IsForSale"]) : false;
            item.IsUpgradeable = (r["IsUpgradeable"] != null) ? Boolean.Parse(r["IsUpgradeable"]) : false;
            item.HasExtendedRange = (r["ExtendedRange"] != null) ? Boolean.Parse(r["ExtendedRange"]) : false;
            item.MinimumStrength = (r["MinimumStrength"] != null) ? (Int32.Parse(r["MinimumStrength"]) > Globals.MaximumStat + 10) ? Globals.MaximumStat + 10 : Int32.Parse(r["MinimumStrength"]) : 0;
            item.MinimumDexterity = (r["MinimumDexterity"] != null) ? Int32.Parse(r["MinimumDexterity"]) : 0;
            item.MinimumVitality = (r["MinimumVitality"] != null) ? Int32.Parse(r["MinimumVitality"]) : 0;
            item.MinimumMagic = (r["MinimumMagic"] != null) ? Int32.Parse(r["MinimumMagic"]) : 0;
            item.MinimumIntelligence = (r["MinimumIntelligence"] != null) ? Int32.Parse(r["MinimumIntelligence"]) : 0;
            item.MinimumCharisma = (r["MinimumCharisma"] != null) ? Int32.Parse(r["MinimumCharisma"]) : 0;
            item.Weight = (r["Weight"] != null) ? Int32.Parse(r["Weight"]) : Math.Min(item.MinimumStrength * 100, Globals.MaximumStat * 100);
            item.StripBonus = (r["StripBonus"] != null) ? Int32.Parse(r["StripBonus"]) : 0;

            return item;
        }

        public void GenerateAttribute()
        {
            int result, primaryValue, secondaryValue;
            ItemSpecialWeaponPrimaryType primaryType = ItemSpecialWeaponPrimaryType.None;
            ItemSpecialWeaponSecondaryType secondaryType = ItemSpecialWeaponSecondaryType.None;
            switch (effectType)
            {
                case ItemEffectType.Attack:
                    result = Dice.Roll(1, 10000);
                    if ((result >= 1) && (result <= 299)) {             primaryType = ItemSpecialWeaponPrimaryType.Light; colour = 2; }
                    else if ((result >= 300) && (result <= 999)) {      primaryType = ItemSpecialWeaponPrimaryType.Endurance; colour = 3; }
                    else if ((result >= 1000) && (result <= 2499)) {    primaryType = ItemSpecialWeaponPrimaryType.Critical; colour = 5; }
                    else if ((result >= 2500) && (result <= 4499)) {    primaryType = ItemSpecialWeaponPrimaryType.Agile; colour = 1; }
                    else if ((result >= 4500) && (result <= 6499)) {    primaryType = ItemSpecialWeaponPrimaryType.Righteous; colour = 7; }
                    else if ((result >= 6500) && (result <= 8099)) {    primaryType = ItemSpecialWeaponPrimaryType.Poison; colour = 4; }
                    else if ((result >= 8100) && (result <= 9699)) {    primaryType = ItemSpecialWeaponPrimaryType.Sharp; colour = 6; }
                    else if ((result >= 9700) && (result <= 10000)) {   primaryType = ItemSpecialWeaponPrimaryType.Ancient; colour = 8; }

                    result = Dice.Roll(1, 30000);
                    if ((result >= 1) && (result < 10000)) primaryValue = 1;  // 10000/29348 = 34%
                    else if ((result >= 10000) && (result < 17400)) primaryValue = 2;  // 6600/29348 = 22.4%
                    else if ((result >= 17400) && (result < 22400)) primaryValue = 3;  // 4356/29348 = 14.8%
                    else if ((result >= 22400) && (result < 25400)) primaryValue = 4;  // 2874/29348 = 9.7%
                    else if ((result >= 25400) && (result < 27400)) primaryValue = 5;  // 1897/29348 = 6.4%
                    else if ((result >= 27400) && (result < 28400)) primaryValue = 6;  // 1252/29348 = 4.2%
                    else if ((result >= 28400) && (result < 28900)) primaryValue = 7;  // 826/29348 = 2.8%
                    else if ((result >= 28900) && (result < 29300)) primaryValue = 8;  // 545/29348 = 1.85%
                    else if ((result >= 29300) && (result < 29700)) primaryValue = 9;  // 360/29348 = 1.2%
                    else if ((result >= 29700) && (result < 29880)) primaryValue = 10; // 237/29348 = 0.8%
                    else if ((result >= 29880) && (result < 29950)) primaryValue = 11; // 156/29348 = 0.5%
                    else if ((result >= 29950) && (result < 29990)) primaryValue = 12; // 103/29348 = 0.3%
                    else if ((result >= 29990) && (result <= 30000)) primaryValue = 13; // 68/29348 = 0.1%
                    else primaryValue = 1;

                    switch (primaryType)
                    {
                        case ItemSpecialWeaponPrimaryType.Critical: primaryValue = Math.Max(primaryValue, 5); break;
                        case ItemSpecialWeaponPrimaryType.Poison: primaryValue = Math.Max(primaryValue, 4); break;
                        case ItemSpecialWeaponPrimaryType.Light: primaryValue = Math.Max(primaryValue, 4); break;
                        case ItemSpecialWeaponPrimaryType.Endurance: primaryValue = Math.Max(primaryValue, 2); break;
                    }
                    // TODO - genlevel <= 2, max primary value is 7

                    attribute = (uint)(0 | (((int)primaryType) << 20) | (primaryValue << 16));

                    if (Dice.Roll(1, 100) <= 40) // 40% chance to get second stat
                    {
                        result = Dice.Roll(1, 10000);
                        if ((result >= 1) && (result <= 4999))          secondaryType = ItemSpecialWeaponSecondaryType.HittingProbability;
                        else if ((result >= 5000) && (result <= 8499))  secondaryType = ItemSpecialWeaponSecondaryType.ConsecutiveDamage;
                        else if ((result >= 8500) && (result <= 9499))  secondaryType = ItemSpecialWeaponSecondaryType.Gold;
                        else if ((result >= 9500) && (result <= 10000)) secondaryType = ItemSpecialWeaponSecondaryType.Experience;

                        result = Dice.Roll(1, 30000);
                        if ((result >= 1) && (result < 10000)) secondaryValue = 1;  // 10000/29348 = 34%
                        else if ((result >= 10000) && (result < 17400)) secondaryValue = 2;  // 6600/29348 = 22.4%
                        else if ((result >= 17400) && (result < 22400)) secondaryValue = 3;  // 4356/29348 = 14.8%
                        else if ((result >= 22400) && (result < 25400)) secondaryValue = 4;  // 2874/29348 = 9.7%
                        else if ((result >= 25400) && (result < 27400)) secondaryValue = 5;  // 1897/29348 = 6.4%
                        else if ((result >= 27400) && (result < 28400)) secondaryValue = 6;  // 1252/29348 = 4.2%
                        else if ((result >= 28400) && (result < 28900)) secondaryValue = 7;  // 826/29348 = 2.8%
                        else if ((result >= 28900) && (result < 29300)) secondaryValue = 8;  // 545/29348 = 1.85%
                        else if ((result >= 29300) && (result < 29700)) secondaryValue = 9;  // 360/29348 = 1.2%
                        else if ((result >= 29700) && (result < 29880)) secondaryValue = 10; // 237/29348 = 0.8%
                        else if ((result >= 29880) && (result < 29950)) secondaryValue = 11; // 156/29348 = 0.5%
                        else if ((result >= 29950) && (result < 29990)) secondaryValue = 12; // 103/29348 = 0.3%
                        else if ((result >= 29990) && (result <= 30000)) secondaryValue = 13; // 68/29348 = 0.1%
                        else secondaryValue = 1;

                        switch (secondaryType)
                        {
                            case ItemSpecialWeaponSecondaryType.HittingProbability: secondaryValue = Math.Max(secondaryValue, 3); break;
                            case ItemSpecialWeaponSecondaryType.ConsecutiveDamage:  secondaryValue = Math.Min(secondaryValue, 7); break;
                            case ItemSpecialWeaponSecondaryType.Experience:         secondaryValue = 2; break;
                            case ItemSpecialWeaponSecondaryType.Gold:               secondaryValue = 5; break;
                        }
                        // TODO - genlevel <= 2, max primary value is 7

                        attribute = (uint)(attribute | (((int)secondaryType) << 12) | (secondaryValue << 8));
                    }

                    break;
                case ItemEffectType.AttackManaSave:
                    primaryType = ItemSpecialWeaponPrimaryType.CastingProbability;
                    colour = 5;

                    result = Dice.Roll(1, 30000);
                    if ((result >= 1) && (result < 10000)) primaryValue = 1;  // 10000/29348 = 34%
                    else if ((result >= 10000) && (result < 17400)) primaryValue = 2;  // 6600/29348 = 22.4%
                    else if ((result >= 17400) && (result < 22400)) primaryValue = 3;  // 4356/29348 = 14.8%
                    else if ((result >= 22400) && (result < 25400)) primaryValue = 4;  // 2874/29348 = 9.7%
                    else if ((result >= 25400) && (result < 27400)) primaryValue = 5;  // 1897/29348 = 6.4%
                    else if ((result >= 27400) && (result < 28400)) primaryValue = 6;  // 1252/29348 = 4.2%
                    else if ((result >= 28400) && (result < 28900)) primaryValue = 7;  // 826/29348 = 2.8%
                    else if ((result >= 28900) && (result < 29300)) primaryValue = 8;  // 545/29348 = 1.85%
                    else if ((result >= 29300) && (result < 29700)) primaryValue = 9;  // 360/29348 = 1.2%
                    else if ((result >= 29700) && (result < 29880)) primaryValue = 10; // 237/29348 = 0.8%
                    else if ((result >= 29880) && (result < 29950)) primaryValue = 11; // 156/29348 = 0.5%
                    else if ((result >= 29950) && (result < 29990)) primaryValue = 12; // 103/29348 = 0.3%
                    else if ((result >= 29990) && (result <= 30000)) primaryValue = 13; // 68/29348 = 0.1%
                    else primaryValue = 1;

                    // TODO - genlevel <= 2, max primary value is 7

                    attribute = (uint)(0 | (((int)primaryType) << 20) | (primaryValue << 16));

                    if (Dice.Roll(1, 100) <= 40) // 40% chance to get second stat
                    {
                        result = Dice.Roll(1, 10000);
                        if ((result >= 1) && (result <= 4999))          secondaryType = ItemSpecialWeaponSecondaryType.HittingProbability;
                        else if ((result >= 5000) && (result <= 8499))  secondaryType = ItemSpecialWeaponSecondaryType.ConsecutiveDamage;
                        else if ((result >= 8500) && (result <= 9499))  secondaryType = ItemSpecialWeaponSecondaryType.Gold;
                        else if ((result >= 9500) && (result <= 10000)) secondaryType = ItemSpecialWeaponSecondaryType.Experience;

                        result = Dice.Roll(1, 30000);
                        if ((result >= 1) && (result < 10000)) secondaryValue = 1;  // 10000/29348 = 34%
                        else if ((result >= 10000) && (result < 17400)) secondaryValue = 2;  // 6600/29348 = 22.4%
                        else if ((result >= 17400) && (result < 22400)) secondaryValue = 3;  // 4356/29348 = 14.8%
                        else if ((result >= 22400) && (result < 25400)) secondaryValue = 4;  // 2874/29348 = 9.7%
                        else if ((result >= 25400) && (result < 27400)) secondaryValue = 5;  // 1897/29348 = 6.4%
                        else if ((result >= 27400) && (result < 28400)) secondaryValue = 6;  // 1252/29348 = 4.2%
                        else if ((result >= 28400) && (result < 28900)) secondaryValue = 7;  // 826/29348 = 2.8%
                        else if ((result >= 28900) && (result < 29300)) secondaryValue = 8;  // 545/29348 = 1.85%
                        else if ((result >= 29300) && (result < 29700)) secondaryValue = 9;  // 360/29348 = 1.2%
                        else if ((result >= 29700) && (result < 29880)) secondaryValue = 10; // 237/29348 = 0.8%
                        else if ((result >= 29880) && (result < 29950)) secondaryValue = 11; // 156/29348 = 0.5%
                        else if ((result >= 29950) && (result < 29990)) secondaryValue = 12; // 103/29348 = 0.3%
                        else if ((result >= 29990) && (result <= 30000)) secondaryValue = 13; // 68/29348 = 0.1%
                        else secondaryValue = 1;

                        switch (secondaryType)
                        {
                            case ItemSpecialWeaponSecondaryType.HittingProbability: secondaryValue = Math.Max(secondaryValue, 3); break;
                            case ItemSpecialWeaponSecondaryType.ConsecutiveDamage:  secondaryValue = Math.Min(secondaryValue, 7); break;
                            case ItemSpecialWeaponSecondaryType.Experience:         secondaryValue = 2; break;
                            case ItemSpecialWeaponSecondaryType.Gold:               secondaryValue = 5; break;
                        }
                        // TODO - genlevel <= 2, max primary value is 7

                        attribute = (uint)(attribute | (((int)secondaryType) << 12) | (secondaryValue << 8));
                    }
                    break;
                case ItemEffectType.Defence: 
                    result = Dice.Roll(1, 10000);
                    if ((result >= 1) && (result <= 5999)) {             primaryType = ItemSpecialWeaponPrimaryType.Endurance; }
                    else if ((result >= 6000) && (result <= 8999)) {     primaryType = ItemSpecialWeaponPrimaryType.Light; }
                    else if ((result >= 9000) && (result <= 9554)) {     primaryType = ItemSpecialWeaponPrimaryType.ManaConverting; }
                    else if ((result >= 9555) && (result <= 10000)) {    primaryType = ItemSpecialWeaponPrimaryType.CriticalChance; }

                    result = Dice.Roll(1, 30000);
                    if ((result >= 1) && (result < 10000)) primaryValue = 1;  // 10000/29348 = 34%
                    else if ((result >= 10000) && (result < 17400)) primaryValue = 2;  // 6600/29348 = 22.4%
                    else if ((result >= 17400) && (result < 22400)) primaryValue = 3;  // 4356/29348 = 14.8%
                    else if ((result >= 22400) && (result < 25400)) primaryValue = 4;  // 2874/29348 = 9.7%
                    else if ((result >= 25400) && (result < 27400)) primaryValue = 5;  // 1897/29348 = 6.4%
                    else if ((result >= 27400) && (result < 28400)) primaryValue = 6;  // 1252/29348 = 4.2%
                    else if ((result >= 28400) && (result < 28900)) primaryValue = 7;  // 826/29348 = 2.8%
                    else if ((result >= 28900) && (result < 29300)) primaryValue = 8;  // 545/29348 = 1.85%
                    else if ((result >= 29300) && (result < 29700)) primaryValue = 9;  // 360/29348 = 1.2%
                    else if ((result >= 29700) && (result < 29880)) primaryValue = 10; // 237/29348 = 0.8%
                    else if ((result >= 29880) && (result < 29950)) primaryValue = 11; // 156/29348 = 0.5%
                    else if ((result >= 29950) && (result < 29990)) primaryValue = 12; // 103/29348 = 0.3%
                    else if ((result >= 29990) && (result <= 30000)) primaryValue = 13; // 68/29348 = 0.1%
                    else primaryValue = 1;

                    switch (primaryType)
                    {
                        case ItemSpecialWeaponPrimaryType.ManaConverting:
                        case ItemSpecialWeaponPrimaryType.CriticalChance: primaryValue = Math.Max(((primaryValue+1)/2), 1); break;
                        case ItemSpecialWeaponPrimaryType.Light: primaryValue = Math.Max(primaryValue, 4); break;
                        case ItemSpecialWeaponPrimaryType.Endurance: primaryValue = Math.Max(primaryValue, 2); break;
                    }
                    // TODO - genlevel <= 2, max primary value is 7.
                    // TODO - genlevel <= 3, for mana and crit, max primary value is 2
                    // TODO - capes max light and endurance is value 1

                    attribute = (uint)(0 | (((int)primaryType) << 20) | (primaryValue << 16));

                    if (Dice.Roll(1, 100) <= 40) // 40% chance to get second stat
                    {
                        result = Dice.Roll(1, 10000);
                        if ((result >= 1) && (result <= 999)) secondaryType = ItemSpecialWeaponSecondaryType.PhysicalResistance;
                        else if ((result >= 1000) && (result <= 3999)) secondaryType = ItemSpecialWeaponSecondaryType.PoisonResistance;
                        else if ((result >= 4000) && (result <= 5499)) secondaryType = ItemSpecialWeaponSecondaryType.SPRecovery;
                        else if ((result >= 5500) && (result <= 6499)) secondaryType = ItemSpecialWeaponSecondaryType.HPRecovery;
                        else if ((result >= 6500) && (result <= 7499)) secondaryType = ItemSpecialWeaponSecondaryType.MPRecovery;
                        else if ((result >= 7500) && (result <= 9399)) secondaryType = ItemSpecialWeaponSecondaryType.MagicResistance;
                        else if ((result >= 9400) && (result <= 9799)) secondaryType = ItemSpecialWeaponSecondaryType.PhysicalAbsorption;
                        else if ((result >= 9800) && (result <= 10000)) secondaryType = ItemSpecialWeaponSecondaryType.MagicAbsorption;

                        result = Dice.Roll(1, 30000);
                        if ((result >= 1) && (result < 10000)) secondaryValue = 1;  // 10000/29348 = 34%
                        else if ((result >= 10000) && (result < 17400)) secondaryValue = 2;  // 6600/29348 = 22.4%
                        else if ((result >= 17400) && (result < 22400)) secondaryValue = 3;  // 4356/29348 = 14.8%
                        else if ((result >= 22400) && (result < 25400)) secondaryValue = 4;  // 2874/29348 = 9.7%
                        else if ((result >= 25400) && (result < 27400)) secondaryValue = 5;  // 1897/29348 = 6.4%
                        else if ((result >= 27400) && (result < 28400)) secondaryValue = 6;  // 1252/29348 = 4.2%
                        else if ((result >= 28400) && (result < 28900)) secondaryValue = 7;  // 826/29348 = 2.8%
                        else if ((result >= 28900) && (result < 29300)) secondaryValue = 8;  // 545/29348 = 1.85%
                        else if ((result >= 29300) && (result < 29700)) secondaryValue = 9;  // 360/29348 = 1.2%
                        else if ((result >= 29700) && (result < 29880)) secondaryValue = 10; // 237/29348 = 0.8%
                        else if ((result >= 29880) && (result < 29950)) secondaryValue = 11; // 156/29348 = 0.5%
                        else if ((result >= 29950) && (result < 29990)) secondaryValue = 12; // 103/29348 = 0.3%
                        else if ((result >= 29990) && (result <= 30000)) secondaryValue = 13; // 68/29348 = 0.1%
                        else secondaryValue = 1;

                        switch (secondaryType)
                        {
                            case ItemSpecialWeaponSecondaryType.PoisonResistance:
                            case ItemSpecialWeaponSecondaryType.PhysicalResistance:
                            case ItemSpecialWeaponSecondaryType.MagicResistance:
                            case ItemSpecialWeaponSecondaryType.PhysicalAbsorption:
                            case ItemSpecialWeaponSecondaryType.MagicAbsorption: secondaryValue = Math.Max(secondaryValue, 3); break;
                        }
                        // TODO - genlevel <= 2, max primary value is 7

                        attribute = (uint)(attribute | (((int)secondaryType) << 12) | (secondaryValue << 8));
                    }
                    break;
            }
        }

        public static uint CalculateAttribute(ItemSpecialWeaponPrimaryType primaryType, int primaryValue, ItemSpecialWeaponSecondaryType secondaryType, int secondaryValue)
        {
            uint attribute;

            # region Abbreviations
            switch (primaryType)
            {
                case ItemSpecialWeaponPrimaryType.CP: primaryType = ItemSpecialWeaponPrimaryType.CastingProbability; break;
                case ItemSpecialWeaponPrimaryType.C: primaryType = ItemSpecialWeaponPrimaryType.Critical; break;
                case ItemSpecialWeaponPrimaryType.CC: primaryType = ItemSpecialWeaponPrimaryType.CriticalChance; break;
                case ItemSpecialWeaponPrimaryType.MC: primaryType = ItemSpecialWeaponPrimaryType.ManaConverting; break;
                case ItemSpecialWeaponPrimaryType.P: primaryType = ItemSpecialWeaponPrimaryType.Poison; break;
            }

            switch (secondaryType)
            {
                case ItemSpecialWeaponSecondaryType.HP: secondaryType = ItemSpecialWeaponSecondaryType.HittingProbability; break;
                case ItemSpecialWeaponSecondaryType.HPR: secondaryType = ItemSpecialWeaponSecondaryType.HPRecovery; break;
                case ItemSpecialWeaponSecondaryType.MA: secondaryType = ItemSpecialWeaponSecondaryType.MagicAbsorption; break;
                case ItemSpecialWeaponSecondaryType.MR: secondaryType = ItemSpecialWeaponSecondaryType.MagicResistance; break;
                case ItemSpecialWeaponSecondaryType.MPR: secondaryType = ItemSpecialWeaponSecondaryType.MPRecovery; break;
                case ItemSpecialWeaponSecondaryType.PA: secondaryType = ItemSpecialWeaponSecondaryType.PhysicalAbsorption; break;
                case ItemSpecialWeaponSecondaryType.DR: secondaryType = ItemSpecialWeaponSecondaryType.PhysicalResistance; break;
                case ItemSpecialWeaponSecondaryType.PR: secondaryType = ItemSpecialWeaponSecondaryType.PoisonResistance; break;
            }
            #endregion

            switch (primaryType)
            {
                case ItemSpecialWeaponPrimaryType.Agile: primaryValue = 1; break;
                case ItemSpecialWeaponPrimaryType.Ancient: primaryValue = 0; break;
                case ItemSpecialWeaponPrimaryType.CastingProbability: primaryValue = Math.Min((primaryValue % 3 == 0) ? primaryValue / 3 : 1, 13); break;
                case ItemSpecialWeaponPrimaryType.Critical: primaryValue = Math.Min(primaryValue, 13) ; break;
                case ItemSpecialWeaponPrimaryType.CriticalChance: primaryValue = Math.Min(primaryValue, 7); break;
                case ItemSpecialWeaponPrimaryType.Endurance: primaryValue = Math.Min((primaryValue % 7 == 0) ? primaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponPrimaryType.Light: primaryValue = Math.Min((primaryValue % 7 == 0) ? primaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponPrimaryType.ManaConverting: primaryValue = Math.Min(primaryValue, 7); break;
                case ItemSpecialWeaponPrimaryType.None: primaryValue = 0; break;
                case ItemSpecialWeaponPrimaryType.Poison: primaryValue = Math.Min(primaryValue, 13); break;
                case ItemSpecialWeaponPrimaryType.Righteous: primaryValue = 0; break;
                case ItemSpecialWeaponPrimaryType.Sharp: primaryValue = 0; break;
            }

            switch (secondaryType)
            {
                case ItemSpecialWeaponSecondaryType.ConsecutiveDamage: secondaryValue = Math.Min(secondaryValue, 7); break;
                case ItemSpecialWeaponSecondaryType.Experience: secondaryValue = Math.Min((secondaryValue % 10 == 0) ? secondaryValue / 10 : 1, 2); break;
                case ItemSpecialWeaponSecondaryType.Gold: secondaryValue = Math.Min((secondaryValue % 10 == 0) ? secondaryValue / 10 : 1, 2); break;
                case ItemSpecialWeaponSecondaryType.HittingProbability: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.HPRecovery: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.MagicAbsorption: secondaryValue = Math.Min((secondaryValue % 3 == 0) ? secondaryValue / 2 : 1, 13); break;
                case ItemSpecialWeaponSecondaryType.MagicResistance: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.MPRecovery: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.None: secondaryValue = 0; break;
                case ItemSpecialWeaponSecondaryType.PhysicalAbsorption: secondaryValue = Math.Min((secondaryValue % 3 == 0) ? secondaryValue / 3 : 1, 13); break;
                case ItemSpecialWeaponSecondaryType.PhysicalResistance: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.PoisonResistance: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
                case ItemSpecialWeaponSecondaryType.SPRecovery: secondaryValue = Math.Min((secondaryValue % 7 == 0) ? secondaryValue / 7 : 2, 13); break;
            }

            attribute = (uint)(0 | (((int)primaryType) << 20) | (primaryValue << 16));
            attribute = (uint)(attribute | (((int)secondaryType) << 12) | (secondaryValue << 8));

            return attribute;
        }

        /// <summary>
        /// Gets the data format of the item which indicates to the client that it is in the bag.
        /// </summary>
        /// <returns>Data to send to Client - 44 bytes.</returns>
        public byte[] GetInventoryData()
        {
            byte[] data = new byte[44];
            int size = 0;

            Buffer.BlockCopy(name.GetBytes(20), 0, data, size, 20);
            size += 20;
            Buffer.BlockCopy(BitConverter.GetBytes(count), 0, data, size, 4);
            size += 4;
            data[size] = (byte)((int)type);
            size++;
            data[size] = (byte)((int)equipType);
            size++;
            data[size] = (byte)(isEquipped ? 1 : 0);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)levelLimit), 0, data, size, 2);
            size += 2;
            data[size] = (byte)((int)genderLimit);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)endurance), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)weight), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)sprite), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)spriteFrame), 0, data, size, 2);
            size += 2;
            data[size] = (byte)colour;
            size++;
            data[size] = (byte)specialEffect2;
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes(attribute), 0, data, size, 4);
            size += 4;

            return data;
        }

        /// <summary>
        /// Gets the data format of the item which indicates to the client that it is in the warehouse.
        /// </summary>
        /// <returns>Data to send to Client = 43 bytes.</returns>
        public byte[] GetWarehouseData()
        {
            byte[] data = new byte[43];
            int size = 0;

            Buffer.BlockCopy(name.GetBytes(20), 0, data, size, 20);
            size += 20;
            Buffer.BlockCopy(BitConverter.GetBytes(count), 0, data, size, 4);
            size += 4;
            data[size] = (byte)((int)type);
            size++;
            data[size] = (byte)((int)equipType);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)levelLimit), 0, data, size, 2);
            size += 2;
            data[size] = (byte)((int)genderLimit);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)endurance), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)weight), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)sprite), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)spriteFrame), 0, data, size, 2);
            size += 2;
            data[size] = (byte)colour;
            size++;
            data[size] = (byte)effect2;
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes(attribute), 0, data, size, 4);
            size += 4;

            return data;
        }

        /// <summary>
        /// Gets the data format of the item which indicates to the client that it has moved from inventory to warehouse.
        /// </summary>
        /// <returns>Data to send to Client = 51 bytes.</returns>
        public byte[] GetInventoryToWarehouseData(int index)
        {
            byte[] data = new byte[51];
            int size = 0;

            data[size] = (byte)index;
            size++;
            data[size] = (byte)1;
            size++;
            Buffer.BlockCopy(name.GetBytes(20), 0, data, size, 20);
            size += 20;
            Buffer.BlockCopy(count.GetBytes(), 0, data, size, 4);
            size += 4;
            data[size] = (byte)((int)type);
            size++;
            data[size] = (byte)((int)equipType);
            size++;
            data[size] = (byte)0; // not equipped
            size++;
            Buffer.BlockCopy(levelLimit.GetBytes(), 0, data, size, 2);
            size += 2;
            data[size] = (byte)((int)genderLimit);
            size++;
            Buffer.BlockCopy(endurance.GetBytes(), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(weight.GetBytes(), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(sprite.GetBytes(), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(spriteFrame.GetBytes(), 0, data, size, 2);
            size += 2;
            data[size] = (byte)colour;
            size++;
            Buffer.BlockCopy(effect2.GetBytes(), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes(attribute), 0, data, size, 4);
            size += 4;
            data[size] = (byte)specialEffect2;
            size++;
            data[size] = (byte)specialEffect3;
            size++;

            return data;
        }

        /// <summary>
        /// Gets the data format of the item which indicates to the client that it has just been obtained.
        /// </summary>
        /// <returns>Data to send to Client - 46 bytes.</returns>
        public byte[] GetObtainedData()
        {
            byte[] data = new byte[46];
            int size = 0;

            data[size] = (byte)1;
            size++;
            Buffer.BlockCopy(name.GetBytes(20), 0, data, size, 20);
            size += 20;
            Buffer.BlockCopy(BitConverter.GetBytes(count), 0, data, size, 4);
            size += 4;
            data[size] = (byte)((int)type);
            size++;
            data[size] = (byte)((int)equipType);
            size++;
            data[size] = (byte)0; // not equipped
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)levelLimit), 0, data, size, 2);
            size += 2;
            data[size] = (byte)((int)genderLimit);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)endurance), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)weight), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)sprite), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)spriteFrame), 0, data, size, 2);
            size += 2;
            data[size] = (byte)colour;
            size++;
            data[size] = (byte)specialEffect2;
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes(attribute), 0, data, size, 4);
            size += 4;

            return data;
        }

        /// <summary>
        /// Gets the data format of the item which indicates to the client that it has just been purchased from shop.
        /// </summary>
        /// <param name="price">The price paid for this item. Including discounts.</param>
        /// <returns>Data to send to Client - 43 bytes.</returns>
        public byte[] GetPurchasedData(short price)
        {
            byte[] data = new byte[43];
            int size = 0;

            data[size] = (byte)1;
            size++;
            Buffer.BlockCopy(name.GetBytes(20), 0, data, size, 20);
            size += 20;
            Buffer.BlockCopy(BitConverter.GetBytes(count), 0, data, size, 4);
            size += 4;
            data[size] = (byte)((int)type);
            size++;
            data[size] = (byte)((int)equipType);
            size++;
            data[size] = (byte)0; // not equipped
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)levelLimit), 0, data, size, 2);
            size += 2;
            data[size] = (byte)((int)genderLimit);
            size++;
            Buffer.BlockCopy(BitConverter.GetBytes((short)endurance), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)weight), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)sprite), 0, data, size, 2);
            size += 2;
            Buffer.BlockCopy(BitConverter.GetBytes((short)spriteFrame), 0, data, size, 2);
            size += 2;
            data[size] = (byte)colour;
            size++;
            Buffer.BlockCopy(price.GetBytes(), 0, data, size, 2);
            size += 2;

            return data;
        }


        public string GetName(int line)
        {
            switch (line)
            {
                case 0:
                    string prefix;
                    switch (effectType)
                    {
                        case ItemEffectType.Attack:
                        case ItemEffectType.AttackActivation:
                        case ItemEffectType.AttackBow:
                        case ItemEffectType.AttackDefence:
                        case ItemEffectType.AttackManaSave:
                        case ItemEffectType.AttackMaxHPDown:
                            switch (SpecialWeaponPrimaryType)
                            {
                                case ItemSpecialWeaponPrimaryType.Agile: prefix = "Agile"; break;
                                case ItemSpecialWeaponPrimaryType.Ancient: prefix = "Ancient"; break;
                                case ItemSpecialWeaponPrimaryType.CastingProbability: prefix = "Special"; break;
                                case ItemSpecialWeaponPrimaryType.Critical: prefix = "Critical"; break;
                                case ItemSpecialWeaponPrimaryType.CriticalChance: prefix = "Critical"; break;
                                case ItemSpecialWeaponPrimaryType.Endurance: prefix = "Strong"; break;
                                case ItemSpecialWeaponPrimaryType.Light: prefix = "Light"; break;
                                case ItemSpecialWeaponPrimaryType.ManaConverting: prefix = "Mana Converting"; break;
                                case ItemSpecialWeaponPrimaryType.Poison: prefix = "Poisoning"; break;
                                case ItemSpecialWeaponPrimaryType.Righteous: prefix = "Righteous"; break;
                                case ItemSpecialWeaponPrimaryType.Sharp: prefix = "Sharp"; break;
                                default: prefix = ""; break;
                            }
                            break;
                        default: prefix = ""; break;
                    }

                    return string.Format("{0} {1} {2}", ((count > 1) ? count.ToString() : ""), prefix, friendlyName).Trim();
                case 1:
                    switch (effectType)
                    {
                        case ItemEffectType.Attack:
                        case ItemEffectType.AttackActivation:
                        case ItemEffectType.AttackBow:
                        case ItemEffectType.AttackDefence:
                        case ItemEffectType.AttackManaSave:
                        case ItemEffectType.AttackMaxHPDown:
                            switch (SpecialWeaponPrimaryType)
                            {
                                case ItemSpecialWeaponPrimaryType.Agile: return ((speed - 1) / 13) + " Strength Full Swing";
                                case ItemSpecialWeaponPrimaryType.Ancient: return "+2 Dice Damage Added";
                                case ItemSpecialWeaponPrimaryType.CastingProbability: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 3) + " Casting Probability";
                                case ItemSpecialWeaponPrimaryType.Critical: return "+" + SpecialWeaponPrimaryValue + " Critical Damage";
                                case ItemSpecialWeaponPrimaryType.CriticalChance: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 3) + "% Critical Increase Change";
                                case ItemSpecialWeaponPrimaryType.Endurance: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 7) + "% Endurance";
                                case ItemSpecialWeaponPrimaryType.Light: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 7) + "% Light";
                                case ItemSpecialWeaponPrimaryType.ManaConverting: return "+" + SpecialWeaponPrimaryValue + "% Damage To Mana";
                                case ItemSpecialWeaponPrimaryType.Poison: return SpecialWeaponPrimaryValue*5 + " Poison Damage";
                                case ItemSpecialWeaponPrimaryType.Righteous: return "Added Reputation Damage";
                                case ItemSpecialWeaponPrimaryType.Sharp: return "+1 Dice Damage Added";
                                default: return string.Empty;
                            }
                        default: return string.Empty;
                    }
                case 2:

                    switch (SpecialWeaponSecondaryType)
                    {
                        case ItemSpecialWeaponSecondaryType.ConsecutiveDamage: return SpecialWeaponSecondaryValue + " Consecutive Damage";
                        case ItemSpecialWeaponSecondaryType.Experience: return "+20% Experience";
                        case ItemSpecialWeaponSecondaryType.Gold: return "+20% Gold Drop";
                        case ItemSpecialWeaponSecondaryType.HittingProbability: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 7) + " Hitting Probability";
                        case ItemSpecialWeaponSecondaryType.HPRecovery: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 7) + "% HP Recovery";
                        case ItemSpecialWeaponSecondaryType.MagicAbsorption: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 3) + "% Magic Absorption";
                        case ItemSpecialWeaponSecondaryType.MagicResistance: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 7) + "% Magic Resistance";
                        case ItemSpecialWeaponSecondaryType.MPRecovery: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 7) + "% MP Recovery";
                        case ItemSpecialWeaponSecondaryType.PhysicalAbsorption: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 3) + "% Physical Absorption";
                        case ItemSpecialWeaponSecondaryType.PhysicalResistance: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 7) + "% Physical Resistance";
                        case ItemSpecialWeaponSecondaryType.PoisonResistance: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 7) + "% Poison Resistance";
                        case ItemSpecialWeaponSecondaryType.SPRecovery: return "+" + ((Math.Max(SpecialWeaponSecondaryValue - 1, 1)) * 7) + "% SP Recovery";
                        default: return string.Empty;
                    }
                case 3:
                    switch (effectType)
                    {
                        case ItemEffectType.Attack:
                        case ItemEffectType.AttackActivation:
                        case ItemEffectType.AttackBow:
                        case ItemEffectType.AttackDefence:
                        case ItemEffectType.AttackManaSave:
                        case ItemEffectType.AttackMaxHPDown:
                            switch (SpecialAbilityType)
                            {
                                case ItemSpecialAbilityType.Blood: return "-20% Maximum HP";
                                case ItemSpecialAbilityType.Berserk: return "+30% Damage";
                                case ItemSpecialAbilityType.Dark: return "+2 Dice Damage At Night";
                                case ItemSpecialAbilityType.DemonSlayer: return "+6 Damage To Demons";
                                case ItemSpecialAbilityType.IceWeapon: return "Ice Activation";
                                case ItemSpecialAbilityType.Kloness: return "Added Reputation Damage";
                                case ItemSpecialAbilityType.Light: return "+2 Dice Damage At Day";
                                case ItemSpecialAbilityType.MedusaWeapon: return "Paralyze Activation";
                                case ItemSpecialAbilityType.MerienArmour: return "Weapon Break Activation";
                                case ItemSpecialAbilityType.MerienShield: return "Invulnerability Activation";
                                case ItemSpecialAbilityType.XelimaWeapon: return "Half Damage Activation";
                                default: return string.Empty;
                            }
                        default: return string.Empty;
                    }
                default:
                    return string.Empty;
            }           
        }

        public int MinimumStrength { get { return minimumStrength; } set { minimumStrength = value; } }
        public int MinimumDexterity { get { return minimumDexterity; } set { minimumDexterity = value; } }
        public int MinimumVitality { get { return minimumVitality; } set { minimumVitality = value; } }
        public int MinimumIntelligence { get { return minimumIntelligence; } set { minimumIntelligence = value; } }
        public int MinimumMagic { get { return minimumMagic; } set { minimumMagic = value; } }
        public int MinimumCharisma { get { return minimumCharisma; } set { minimumCharisma = value; } }

        public int Sprite
        {
            set { sprite = value; }
            get { return sprite; }
        }

        public int SpriteFrame
        {
            set { spriteFrame = value; }
            get { return spriteFrame; }
        }

        public int Colour
        {
            set { colour = value; }
            get { return colour; }
        }

        public string Name
        {
            set { name = value; }
            get { return name; }
        }
        public string FriendlyName { get { return friendlyName; } set { friendlyName = value; } }

        public int Count
        {
            set { count = value; }
            get { return count; }
        }

        public ItemType Type
        {
            set { type = value; }
            get { return type; }
        }

        public EquipType EquipType
        {
            set { equipType = value; }
            get { return equipType; }
        }

        public int LevelLimit
        {
            set { levelLimit = value; }
            get { return levelLimit; }
        }

        public GenderType GenderLimit
        {
            set { genderLimit = value; }
            get { return genderLimit; }
        }

        public int Endurance
        {
            set
            {
                endurance = value;

            }
            get { return endurance; }
        }

        public int Weight
        {
            set { weight = value; }
            get { return weight; }
        }

        public bool IsEquipped { set { isEquipped = value; } get { return isEquipped; } }
        public Location BagPosition { set { bagPosition = value; } get { return bagPosition; } }

        public int SpecialEffect1
        {
            set { specialEffect1 = value; }
            get { return specialEffect1; }
        }

        public int SpecialEffect2
        {
            set { specialEffect2 = value; }
            get { return specialEffect2; }
        }

        public int SpecialEffect3
        {
            set { specialEffect3 = value; }
            get { return specialEffect3; }
        }

        public uint Attribute
        {
            set
            {
                attribute = value;

                switch (SpecialWeaponPrimaryType)
                {
                    case ItemSpecialWeaponPrimaryType.Ancient: colour = 8; break;
                    case ItemSpecialWeaponPrimaryType.Sharp: colour = 6; break;
                    case ItemSpecialWeaponPrimaryType.Light: colour = 2; break;
                    case ItemSpecialWeaponPrimaryType.Endurance: colour = 3; break;
                    case ItemSpecialWeaponPrimaryType.Critical: colour = 5; break;
                    case ItemSpecialWeaponPrimaryType.Agile: colour = 1; break;
                    case ItemSpecialWeaponPrimaryType.Righteous: colour = 7; break;
                    case ItemSpecialWeaponPrimaryType.Poison: colour = 4; break;
                }
            }
            get { return attribute; }
        }

        public int Appearance
        {
            set { appearance = value; }
            get { return appearance; }
        }

        public int Speed
        {
            set { speed = value; }
            get
            {
                if (SpecialWeaponPrimaryType == ItemSpecialWeaponPrimaryType.Agile)
                    return speed - 1;
                else return speed;
            }
        }

        public int Price { get { return price; } set { price = value; } }
        public bool IsForSale { get { return isForSale; } set { isForSale = value; } }
        public bool HasExtendedRange { get { return hasExtendedRange; } set { hasExtendedRange = value; } }

        public int Effect1
        {
            get { return effect1; }
            set { effect1 = value; }
        }
        public int Effect2
        {
            get { return effect2; }
            set { effect2 = value; }
        }
        public int Effect3
        {
            get { return effect3; }
            set { effect3 = value; }
        }
        public int Effect4
        {
            get { return effect4; }
            set { effect4 = value; }
        }
        public int Effect5
        {
            get { return effect5; }
            set { effect5 = value; }
        }
        public int Effect6
        {
            get { return effect6; }
            set { effect6 = value; }
        }

        public int MaximumEndurance
        {
            get
            {
                if (SpecialWeaponPrimaryType == ItemSpecialWeaponPrimaryType.Endurance)
                    return maximumEndurance + ((maximumEndurance / 100) * (SpecialWeaponPrimaryValue*7));
                else return maximumEndurance;
            }
            set { maximumEndurance = value; }
        }

        public ItemRarity Rarity
        {
            set { rarity = value; }
            get { return rarity; }
        }

        public ItemEffectType EffectType
        {
            get { return effectType; }
            set { effectType = value; }
        }

        public ItemSpecialAbilityType SpecialAbilityType
        {
            get { return specialAbilityType; }
            set { specialAbilityType = value; }
        }

        public SkillType RelatedSkill { get { return relatedSkill; } set { relatedSkill = value; } }
        public int StripBonus { get { return stripBonus; } set { stripBonus = value; } }

        public int Category
        {
            get { return category; }
            set { category = value; }
        }

        public string BoundID
        {
            set { boundID = value; }
            get { return boundID; }
        }

        public string BoundName
        {
            set { boundName = value; }
            get { return boundName; }
        }

        public DateTime DropTime
        {
            get { return dropTime; }
            set { dropTime = value; }
        }

        public bool IsStackable
        {
            get { return (type == ItemType.Consume || type == ItemType.Arrow); }
        }

        public bool IsUpgradeable { get { return isUpgradeable; } set { isUpgradeable = value; } }
        public bool IsAngel { get { return (effectType == ItemEffectType.AddEffect && effect1 >= 16 && effect1 <= 19); } }
        public int Level { get { return (int)((attribute & 0xF0000000) >> 28); } }

        public int CastingProbabilityBonus
        {
            get
            {
                if (SpecialWeaponPrimaryType == ItemSpecialWeaponPrimaryType.CastingProbability)
                    return SpecialWeaponPrimaryValue * 3;
                else return 0;
            }
        }

        public int Range
        {
            get
            {
                switch (relatedSkill)
                {
                    case SkillType.Archery: return 9;
                    default: return 1;
                }
            }
        }

        public int CriticalRange
        {
            get
            {
                switch (relatedSkill)
                {
                    case SkillType.Archery: return 9;
                    case SkillType.Axe: return 2;
                    case SkillType.Fencing: return 4;
                    case SkillType.Hammer: return 2;
                    case SkillType.Hand: return 1;
                    case SkillType.LongSword: return 3;
                    case SkillType.ShortSword: return 2;
                    case SkillType.Staff: return 2;
                    default: return 1;
                }
            }
        }

        #region DEFENCE SPECIFIC VALUES
        public int PhysicalAbsorption
        {
            get
            {
                if (effectType == ItemEffectType.Defence ||
                    effectType == ItemEffectType.DefenceActivation)
                {
                    if (SpecialWeaponSecondaryType == ItemSpecialWeaponSecondaryType.PhysicalAbsorption)
                    {
                        if (SpecialWeaponSecondaryValue + effect1 > Globals.MaximumPhysicalAbsorption) return Globals.MaximumPhysicalAbsorption;
                        else return SpecialWeaponSecondaryValue + effect1;
                    }
                    else
                    {
                        if (effect1 > Globals.MaximumPhysicalAbsorption) return Globals.MaximumPhysicalAbsorption;
                        else return effect1;
                    }
                }
                else return 0;
            }
        }

        public int PhysicalResistance
        {
            get
            {
                if (effectType == ItemEffectType.Defence ||
                    effectType == ItemEffectType.DefenceAntiMine ||
                    effectType == ItemEffectType.DefenceActivation)
                {
                    if (SpecialWeaponSecondaryType == ItemSpecialWeaponSecondaryType.PhysicalResistance)
                         return effect2 + SpecialWeaponSecondaryValue * 7;
                    else return effect2;
                }
                else return 0;
            }
        }
        #endregion
        #region ATTACK SPECIFIC VALUES
        public Dice DamageSmall
        {
            get
            {
                if (effectType == ItemEffectType.Attack ||
                    effectType == ItemEffectType.AttackBow ||
                    effectType == ItemEffectType.AttackDefence ||
                    effectType == ItemEffectType.AttackManaSave ||
                    effectType == ItemEffectType.AttackActivation)
                {
                    if (SpecialWeaponPrimaryType == ItemSpecialWeaponPrimaryType.Ancient)
                        return new Dice(effect1 + 2, effect2 + 2, effect3);
                    else if (SpecialWeaponPrimaryType == ItemSpecialWeaponPrimaryType.Sharp)
                        return new Dice(effect1 + 1, effect2 + 1, effect3);
                    else return new Dice(effect1, effect2, effect3);
                }
                else return null;
            }
        }
        public Dice DamageLarge
        {
            get
            {
                if (effectType == ItemEffectType.Attack ||
                    effectType == ItemEffectType.AttackBow ||
                    effectType == ItemEffectType.AttackDefence ||
                    effectType == ItemEffectType.AttackManaSave ||
                    effectType == ItemEffectType.AttackActivation)
                {
                    if (SpecialWeaponPrimaryType == ItemSpecialWeaponPrimaryType.Ancient)
                        return new Dice(effect4 + 2, effect5 + 2, effect6);
                    else if (SpecialWeaponPrimaryType == ItemSpecialWeaponPrimaryType.Sharp)
                        return new Dice(effect4 + 1, effect5 + 1, effect6);
                    else return new Dice(effect4, effect5, effect6);
                }
                else return null;
            }
        }
        #endregion
        #region WEAPON ATTRIBUTE VALUES
        public ItemSpecialWeaponPrimaryType SpecialWeaponPrimaryType
        {
            get
            {
                if ((attribute & 0x00F00000) != 0)
                    return (ItemSpecialWeaponPrimaryType)((attribute & 0x00F00000) >> 20);
                else return ItemSpecialWeaponPrimaryType.None;
            }
        }
        public int SpecialWeaponPrimaryValue
        {
            get
            {
                if ((attribute & 0x000F0000) != 0)
                    return (int)((attribute & 0x000F0000) >> 16);
                else return 0;
            }
        }
        public ItemSpecialWeaponSecondaryType SpecialWeaponSecondaryType
        {
            get
            {
                if ((attribute & 0x0000F000) != 0)
                    return (ItemSpecialWeaponSecondaryType)((attribute & 0x0000F000) >> 12);
                else return ItemSpecialWeaponSecondaryType.None;
            }
        }
        public int SpecialWeaponSecondaryValue
        {
            get
            {
                if ((attribute & 0x0000F000) != 0)
                    return (int)((attribute & 0x00000F00) >> 8);
                else return 0;
            }
        }
        #endregion

        public bool Equals(Item other)
        {
            return (other.Name.Equals(this.name));
        }

        bool IEquatable<Item>.Equals(Item other)
        {
            return (other.Name.Equals(this.name));
        }

        public int BagX { get { return bagX; } set { bagX = value; } }
        public int BagY { get { return bagY; } set { bagY = value; } }
        public int BagOldX { get { return bagOldX; } set { bagOldX = value; } }
        public int BagOldY { get { return bagOldY; } set { bagOldY = value; } }
    }

    public class ItemStack
    {
        private List<Item> items;

        public ItemStack()
        {
            items = new List<Item>(Globals.MaximumItemsPerTile);
        }

        public Item CheckItem()
        {
            if (items.Count > 0)
                return items[items.Count - 1];
            else return null;
        }

        public void AddItem(Item item)
        {
            if (items.Count >= Globals.MaximumItemsPerTile) items.RemoveAt(0);
            
            items.Add(item);
        }

        public Item RemoveItem()
        {
            if (items.Count > 0)
            {
                Item item = items[items.Count - 1];
                items.RemoveAt(items.Count - 1);
                return item;
            }
            else return null;
        }

        public void ClearItem(int index)
        {
            if (items.Count > 0)
            {
                items.RemoveAt(index);
            }
        }

        public int Count { get { return items.Count; } }
        public Item this[int index] { get { return items[index]; } }
    }
}
