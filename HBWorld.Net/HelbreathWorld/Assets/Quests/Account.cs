﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HelbreathWorld.Common.Assets.Objects;

namespace HelbreathWorld.Common.Assets
{
    public class Account
    {
        private int clientID;    // identity of ClientInfo object to track socket
        private string id;       // identity of the account from the database
        private string name;

        private Dictionary<String, Character> characters;

        public Account()
        {
            
        }

        public bool Load(DataRow data)
        {
            if (data == null) return false;

            try
            {
                name = data["Name"].ToString();
                id = data["ID"].ToString();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool LoadCharacters(DataTable data)
        {
            characters = new Dictionary<String, Character>();

            if (data != null)
                foreach (DataRow row in data.Rows)
                {
                    Character c = new Character();
                    if (c.Load(row)) characters.Add(c.Name, c);
                }

            return true;
        }

        public byte[] GetLoginData()
        {
            byte[] data = new byte[26+(characters.Count*65)];

            Buffer.BlockCopy(BitConverter.GetBytes((short)3), 0, data, 0, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)82), 0, data, 2, 2);
            data[4] = (byte)0;
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 5, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 7, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 9, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 11, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 13, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 15, 2);
            data[17] = (byte)characters.Count;

            int size = 18;
            foreach (Character character in characters.Values)
            {
                Buffer.BlockCopy(character.Name.GetBytes(10), 0, data, size, 10);
                size += 10;
                data[size] = (byte)1;
                size++;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance1), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance2), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance3), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance4), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Gender), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Skin), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Level), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes(character.Experience), 0, data, size, 4);
                size += 4;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Strength), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Vitality), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Dexterity), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Intelligence), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Magic), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Charisma), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes(character.AppearanceColour), 0, data, size, 4);
                size += 4;
                Buffer.BlockCopy(BitConverter.GetBytes((short)1987), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)10), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)26), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)8), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)34), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LoadedMapName.GetBytes(10), 0, data, size, 10);
                size += 10;
            }

            
            Buffer.BlockCopy(BitConverter.GetBytes(35), 0, data, 18+(characters.Count*65), 4);
            Buffer.BlockCopy(BitConverter.GetBytes(35), 0, data, 22+(characters.Count*65), 4);

            return data;
        }

        public byte[] GetNewCharacterData(string characterName)
        {
            byte[] data = new byte[12 + (characters.Count * 65)];

            Buffer.BlockCopy(characterName.GetBytes(10), 0, data, 0, 10);

            data[10] = (byte)characters.Count;
            int size = 11;
            foreach (Character character in characters.Values)
            {
                Buffer.BlockCopy(character.Name.GetBytes(10), 0, data, size, 10);
                size += 10;
                data[size] = (byte)1;
                size++;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance1), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance2), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance3), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance4), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Gender), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Skin), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Level), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes(character.Experience), 0, data, size, 4);
                size += 4;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Strength), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Vitality), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Dexterity), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Intelligence), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Magic), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)character.Charisma), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes(character.AppearanceColour), 0, data, size, 4);
                size += 4;
                Buffer.BlockCopy(BitConverter.GetBytes((short)1987), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)10), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)26), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)8), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(BitConverter.GetBytes((short)34), 0, data, size, 2);
                size += 2;
                Buffer.BlockCopy(character.LoadedMapName.GetBytes(10), 0, data, size, 10);
                size += 10;
            }

            return data;
        }

        /// <summary>
        /// ClientInfo ID of this account from socket.
        /// </summary>
        public Int32 ClientID
        {
            get { return clientID; }
            set { clientID = value; }
        }

        public String Name
        {
            get { return name; }
        }

        /// <summary>
        /// Database ID of this account.
        /// </summary>
        public String ID
        {
            get { return id; }
        }

        public Character this[string characterName]
        {
            get { return characters[characterName]; }
        }
    }
}
