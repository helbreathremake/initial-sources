﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Common.Assets.Objects;
using HelbreathWorld.Common.Assets.Objects.Dynamic;

namespace HelbreathWorld.Common.Assets
{
    public class Map
    {
        public event LogHandler MessageLogged;
        public event MapHandler WeatherChanged;
        public event DynamicObjectHandler DynamicObjectCreated;
        public event DynamicObjectHandler DynamicObjectRemoved;

        private bool isLoaded;
        private string name;
        private string friendlyName;
        private string amdFileName;

        private int sizeX;
        private int sizeY;
        private int tileSize;
        private List<MapRow> rows;
        private List<Teleport> teleports;
        private List<MobSpawn> mobSpawns;
        private List<NpcSpawn> npcSpawns;
        private List<Zone> safeZones;
        private List<Location> mineralSpawns;
        private List<Location> fishSpawns;
        private List<Location> defaultLocations;

        private BuildingType buildingType;
        private WeatherType weatherType;
        private DateTime weatherStartTime;
        private TimeSpan weatherDuration;
        private TimeOfDay timeOfDay;
        private bool isBuilding;
        private bool isFightZone;
        private bool isAttackEnabled; // toggle - used by events and gms
        private bool isSafeMap; // static - specified in configs
        private bool lootEnabled; // disabling of loot drops e.g bleeding island
        private bool isHuntZone;

        private int npcIndex; // internal count kept for dynamic object id
        private int dynamicObjectIndex;  // internal count kept for dynamic object id
        private int currentMinerals;
        private int maximumMinerals;
        private int maximumMineralLevel;

        private int totalMobs;

        private DateTime itemClearTime;

        // Caching
        private List<int> players;
        private List<int> npcs;
        private List<int> dynamicObjects;

        public Map(string name)
        {
            this.amdFileName = "";
            this.name = name;
            this.friendlyName = "";

            Init();
        }

        public Map(string amdFileName, string name, string friendlyName)
        {
            this.amdFileName = amdFileName;
            this.name = name;
            this.friendlyName = friendlyName;

            Init();
        }

        private void Init()
        {
            rows = new List<MapRow>();
            teleports = new List<Teleport>();
            mobSpawns = new List<MobSpawn>();
            npcSpawns = new List<NpcSpawn>();
            mineralSpawns = new List<Location>();
            fishSpawns = new List<Location>();
            defaultLocations = new List<Location>();
            safeZones = new List<Zone>();
            weatherType = WeatherType.Clear;
            isLoaded = false;
            isAttackEnabled = true;
            itemClearTime = DateTime.Now;

            // caching
            players = new List<int>(Globals.MaximumMapPlayers);
            npcs = new List<int>(Globals.MaximumMapNpcs);
            dynamicObjects = new List<int>(Globals.MaximumMapDynamicObjects);
        }

        /// <summary>
        /// Loads the config of this map from the config.xml file.
        /// </summary>
        public void Parse(XmlReader mapReader)
        {
            XmlReader mapInfoReader = mapReader.ReadSubtree();
            while (mapInfoReader.Read())
                if (mapInfoReader.IsStartElement())
                    switch (mapInfoReader.Name)
                    {
                        case "Teleport":
                            teleports.Add(new Teleport(new Location(name, Convert.ToInt32(mapInfoReader["X"]), Convert.ToInt32(mapInfoReader["Y"])), new Location(mapInfoReader["DestinationMap"], Convert.ToInt32(mapInfoReader["DestinationX"]), Convert.ToInt32(mapInfoReader["DestinationY"])), (MotionDirection)Enum.Parse(typeof(MotionDirection), mapInfoReader["Direction"])));
                            break;
                        case "MobSpawn":
                            mobSpawns.Add(new MobSpawn(new Zone(this.name, Convert.ToInt32(mapInfoReader["Top"]), Convert.ToInt32(mapInfoReader["Left"]), Convert.ToInt32(mapInfoReader["Bottom"]), Convert.ToInt32(mapInfoReader["Right"])), mapInfoReader["Mob"], Convert.ToInt32(mapInfoReader["Count"])));
                            break;
                        case "Npc":
                            switch ((MovementType)Enum.Parse(typeof(MovementType), mapInfoReader["Movement"]))
                            {
                                case MovementType.RandomArea:
                                    npcSpawns.Add(new NpcSpawn(mapInfoReader["Name"], new Zone(this.name, Convert.ToInt32(mapInfoReader["Top"]), Convert.ToInt32(mapInfoReader["Left"]), Convert.ToInt32(mapInfoReader["Bottom"]), Convert.ToInt32(mapInfoReader["Right"])), (MovementType)Enum.Parse(typeof(MovementType), mapInfoReader["Movement"])));
                                    break;
                                default:
                                    npcSpawns.Add(new NpcSpawn(mapInfoReader["Name"], new Location(this.name, Convert.ToInt32(mapInfoReader["X"]), Convert.ToInt32(mapInfoReader["Y"])), (MovementType)Enum.Parse(typeof(MovementType), mapInfoReader["Movement"])));
                                    break;
                            }
                            break;
                        case "MineralSpawns":
                            maximumMinerals = Convert.ToInt32(mapInfoReader["Count"]);
                            maximumMineralLevel = Convert.ToInt32(mapInfoReader["MaximumLevel"]);
                            if (maximumMineralLevel <= 0) maximumMineralLevel = 1;
                            if (maximumMineralLevel > 6) maximumMineralLevel = 6;
                            break;
                        case "MineralSpawn":
                            mineralSpawns.Add(new Location(Convert.ToInt32(mapInfoReader["X"]), Convert.ToInt32(mapInfoReader["Y"])));
                            break;
                        case "FishSpawn":
                            fishSpawns.Add(new Location(Convert.ToInt32(mapInfoReader["X"]), Convert.ToInt32(mapInfoReader["Y"])));
                            break;
                        case "Location":
                            defaultLocations.Add(new Location(Convert.ToInt32(mapInfoReader["X"]), Convert.ToInt32(mapInfoReader["Y"])));
                            break;
                        case "SafeZone":
                            safeZones.Add(new Zone(this.name, Convert.ToInt32(mapInfoReader["Top"]), Convert.ToInt32(mapInfoReader["Left"]), Convert.ToInt32(mapInfoReader["Bottom"]), Convert.ToInt32(mapInfoReader["Right"])));
                            break;
                    }
            mapInfoReader.Close();
        }

        /// <summary>
        /// Loads the definition of this map from the .AMD file.
        /// </summary>
        /// <returns>Result of the load process.</returns>
        public MapLoadResult Load()
        {
            try
            {
                string installPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");
                FileStream reader = new FileStream(installPath + "\\" + amdFileName, FileMode.Open, FileAccess.Read);

                byte[] headerBuffer = new byte[256];
                reader.Read(headerBuffer, 0, 256);
                string[] header = Encoding.ASCII.GetString(headerBuffer).Replace('\0', ' ').Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                // loads the map header which defines the width, height and tile size of the map (in bytes)
                for (int i = 0; i < header.Length; i++)
                    switch (header[i])
                    {
                        case "MAPSIZEX":
                            this.sizeX = Int32.Parse(header[i + 2]);
                            break;
                        case "MAPSIZEY":
                            this.sizeY = Int32.Parse(header[i + 2]);
                            break;
                        case "TILESIZE":
                            this.tileSize = Int32.Parse(header[i + 2]);
                            break;
                    }

                // loads the tile information from the .amd file and creates a grid of maprow and maptile objects.
                int tileCount = 0;
                for (int y = 0; y < sizeY; y++)
                {
                    MapRow row = new MapRow();
                    for (int x = 0; x < sizeX; x++)
                    {
                        MapTile tile = new MapTile();

                        byte[] tileBuffer = new byte[tileSize];
                        reader.Read(tileBuffer, 0, tileSize);
                        if (tile.Parse(tileBuffer)) row.AddTile(tile);
                        tileCount++;
                    }
                    rows.Add(row);
                }

                // places the teleport locations on the tiles
                foreach (Teleport teleport in teleports) rows[teleport.Source.Y][teleport.Source.X].SetTeleport(teleport);     

                // sets tiles as safe zone tiles
                foreach (Zone safeZone in safeZones)
                    foreach (Location location in safeZone.GetAllLocations())
                        rows[location.Y][location.X].IsSafe = true;

                isLoaded = true;
                return MapLoadResult.Success;
            }
            catch (FileNotFoundException)
            {
                return MapLoadResult.MapNotFound;
            }
            catch (DirectoryNotFoundException)
            {
                return MapLoadResult.MapNotFound;
            }
            catch (UnauthorizedAccessException)
            {
                return MapLoadResult.MapNotAccessible;
            }
            catch
            {
                return MapLoadResult.Error;
            }
        }

        public Flag SetFlag(Location location, OwnerSide side)
        {
            Flag flag = new Flag(++dynamicObjectIndex);
            flag.Init(this, location.X, location.Y);
            flag.Side = side;
            flag.FlagRemoved += new DynamicObjectHandler(OnDynamicObjectRemoved);
            if (DynamicObjectCreated != null) DynamicObjectCreated(flag);
            return flag;
        }

        public void CreateDynamicObject(DynamicObjectType type, int x, int y) { CreateDynamicObject(type, x, y, new TimeSpan(), 0); }
        public void CreateDynamicObject(DynamicObjectType type, int x, int y, TimeSpan lastTime) { CreateDynamicObject(type, x, y, lastTime, 0); }
        public void CreateDynamicObject(DynamicObjectType type, int x, int y, TimeSpan lastTime, int effect)
        {
            if (this[y][x].IsDynamicOccupied) return;

            switch (type)
            {
                case DynamicObjectType.Rock:
                case DynamicObjectType.Gem:
                    Mineral mineral;
                    switch (Dice.Roll(1, maximumMineralLevel))
                    {
                        default:
                        case 1: mineral = new Mineral(++dynamicObjectIndex, MineralType.RockEasy); break;
                        case 2: mineral = new Mineral(++dynamicObjectIndex, MineralType.RockMedium); break;
                        case 3: mineral = new Mineral(++dynamicObjectIndex, MineralType.RockHard); break;
                        case 4: mineral = new Mineral(++dynamicObjectIndex, MineralType.RockExtreme); break;
                        case 5: mineral = new Mineral(++dynamicObjectIndex, MineralType.GemEasy); break;
                        case 6: mineral = new Mineral(++dynamicObjectIndex, MineralType.GemHard); break;
                    }
                    mineral.Init(this, x, y);
                    mineral.MineralRemoved += new DynamicObjectHandler(OnDynamicObjectRemoved);
                    if (DynamicObjectCreated != null) DynamicObjectCreated(mineral);
                    currentMinerals++;
                    dynamicObjects.Add(mineral.ID); // no need to register
                    break;
                case DynamicObjectType.Fish:
                case DynamicObjectType.FishObject:
                    Fish fish = new Fish(++dynamicObjectIndex);
                    fish.Init(this, x, y);
                    fish.FishRemoved += new DynamicObjectHandler(OnDynamicObjectRemoved);
                    if (DynamicObjectCreated != null) DynamicObjectCreated(fish);
                    dynamicObjects.Add(fish.ID);  // no need to register
                    break;
                case DynamicObjectType.Fire:
                case DynamicObjectType.Fire2:
                case DynamicObjectType.Fire3:
                    switch (weatherType) // rain causes the fires to die out quicker
                    {
                        case WeatherType.Rain: lastTime = new TimeSpan(0, 0, (int)(lastTime.TotalSeconds / 2)); break;
                        case WeatherType.RainMedium: lastTime = new TimeSpan(0, 0, (int)((lastTime.TotalSeconds / 2) - (lastTime.TotalSeconds / 3))); break;
                        case WeatherType.RainHeavy: lastTime = new TimeSpan(0, 0, (int)((lastTime.TotalSeconds / 3) - (lastTime.TotalSeconds / 4))); break;
                    }

                    Fire fire = new Fire(++dynamicObjectIndex);
                    fire.Init(this, x, y);
                    fire.Type = type;
                    fire.LastTime = lastTime;
                    fire.FireRemoved += new DynamicObjectHandler(OnDynamicObjectRemoved);
                    if (DynamicObjectCreated != null) DynamicObjectCreated(fire);
                    dynamicObjects.Add(fire.ID);
                    break;
                case DynamicObjectType.PoisonCloudBegin:
                    PoisonCloud cloud = new PoisonCloud(++dynamicObjectIndex);
                    cloud.Init(this, x, y);
                    cloud.PoisonDamage = effect;
                    cloud.LastTime = lastTime;
                    cloud.PoisonCloudRemoved += new DynamicObjectHandler(OnDynamicObjectRemoved);
                    if (DynamicObjectCreated != null) DynamicObjectCreated(cloud);
                    dynamicObjects.Add(cloud.ID);
                    break;
                case DynamicObjectType.SpikeField:
                    Spikes spike = new Spikes(++dynamicObjectIndex);
                    spike.Init(this, x, y);
                    spike.Damage = new Dice(2, 4, 0);
                    spike.LastTime = lastTime;
                    spike.SpikeFieldRemoved += new DynamicObjectHandler(OnDynamicObjectRemoved);
                    if (DynamicObjectCreated != null) DynamicObjectCreated(spike);
                    dynamicObjects.Add(spike.ID);
                    break;
                case DynamicObjectType.IceStorm:
                    IceStorm storm = new IceStorm(++dynamicObjectIndex);
                    storm.Init(this, x, y);
                    storm.FreezeChance = effect;
                    storm.LastTime = lastTime;
                    storm.IceStormRemoved += new DynamicObjectHandler(OnDynamicObjectRemoved);
                    if (DynamicObjectCreated != null) DynamicObjectCreated(storm);
                    dynamicObjects.Add(storm.ID);
                    break;
            }
            
        }

        /// <summary>
        /// Actions to be performed on a timer such as weather processing and mineral spawns.
        /// </summary>
        public void TimerProcess()
        {
            WeatherProcess();
            MineralProcess();
            FishProcess();
            ItemProcess();
        }

        private void ItemProcess()
        {
            TimeSpan ts = DateTime.Now - itemClearTime;
            if (ts.TotalSeconds >= Globals.ItemClearTime)
            {
                int itemsCleared = 0;
                for (int x = 0; x < Width; x++)
                    for (int y = 0; y < Height; y++)
                        if (rows[y][x].Items.Count > 0)
                            for (int i = 0; i < rows[y][x].Items.Count; i++)
                                if (rows[y][x].Items[i] != null)
                                    if ((DateTime.Now - rows[y][x].Items[i].DropTime).TotalSeconds >= Globals.ItemClearTime)
                                    {
                                        rows[y][x].Items.ClearItem(i);
                                        itemsCleared++;
                                    }

                if (itemsCleared > 0) LogMessage("[" + name + "] - Items cleared: " + itemsCleared);
                itemClearTime = DateTime.Now;
            }
        }

        private void MineralProcess()
        {
            if (isBuilding) return;

            if (mineralSpawns.Count > 0 && currentMinerals < maximumMinerals)
                if (Dice.Roll(1, 6) == 1) // randomize, stagger spawns
                    foreach (Location spawn in mineralSpawns)
                        if (spawn != null && !rows[spawn.Y][spawn.X].IsBlocked)
                        {
                            CreateDynamicObject(DynamicObjectType.Rock, spawn.X, spawn.Y);
                            break; // only create 1 per iteration
                        }
        }

        private void FishProcess()
        {
            if (isBuilding) return;

            if (fishSpawns.Count > 0)
                if (Dice.Roll(1, 6) == 1)
                    foreach (Location spawn in fishSpawns)
                        if (spawn != null && spawn.Y >= 0 && spawn.Y < Height && spawn.X >= 0 && spawn.X < Width &&
                            !rows[spawn.Y][spawn.X].IsDynamicOccupied && rows[spawn.Y][spawn.X].IsWater)
                        {
                            CreateDynamicObject(DynamicObjectType.Fish, spawn.X, spawn.Y);
                            break;
                        }
        }

        private void WeatherProcess()
        {
            if (isBuilding) return; // no weather inside buildings

            WeatherType currentWeather = weatherType;
            if (weatherType != WeatherType.Clear)
            {
                TimeSpan ts = DateTime.Now - weatherStartTime;
                if (ts > weatherDuration) weatherType = WeatherType.Clear;
            }
            else if (Dice.Roll(1, 300) == 13)
            {
                switch (Dice.Roll(1, 3))
                {
                    case 1: if (DateTime.Now.Month == 12) weatherType = WeatherType.Snow;
                        else weatherType = WeatherType.Rain;
                        break;
                    case 2: if (DateTime.Now.Month == 12) weatherType = WeatherType.SnowMedium;
                        else weatherType = WeatherType.RainMedium;
                        break;
                    case 3: if (DateTime.Now.Month == 12) weatherType = WeatherType.SnowHeavy;
                        else weatherType = WeatherType.RainHeavy;
                        break;
                }
                weatherStartTime = DateTime.Now;
                weatherDuration = new TimeSpan(0, 3 + Dice.Roll(1, 7), 0); // 3 - 10 minutes duration
            }

            if ((currentWeather != weatherType) && (WeatherChanged != null)) WeatherChanged(this); // notify game server
        }

        public MotionDirection GetNextDirection(int sourceX, int sourceY, int destinationX, int destinationY, MotionTurn turn)
        {
            int x, y, tempDirection, tempX, tempY;
            MotionDirection direction = MotionDirection.None;

            x = sourceX - destinationX;
            y = sourceY - destinationY;

            // source and target the same
            if ((x == 0) && (y == 0)) return MotionDirection.None;

            // get the general next direction
            if (x == 0)
            {
                if (y > 0) direction = MotionDirection.North;
                if (y < 0) direction = MotionDirection.South;
            }
            if (y == 0)
            {
                if (x > 0) direction = MotionDirection.West;
                if (x < 0) direction = MotionDirection.East;
            }
            if ((x > 0) && (y > 0)) direction = MotionDirection.NorthWest;
            if ((x < 0) && (y > 0)) direction = MotionDirection.NorthEast;
            if ((x > 0) && (y < 0)) direction = MotionDirection.SouthWest;
            if ((x < 0) && (y < 0)) direction = MotionDirection.SouthEast;

            // if the way is blocked, turn left or right around obstacle. an Npc's turn is randomized on instantiation
            switch (turn)
            {
                case MotionTurn.Right: // right
                    for (int i = (int)direction; i <= (int)direction + 7; i++)
                    {
                        tempDirection = i;
                        if (tempDirection > 8) tempDirection -= 8;
                        tempX = sourceX + Globals.MoveDirectionX[tempDirection];
                        tempY = sourceY + Globals.MoveDirectionY[tempDirection];
                        if (tempX >= 0 && tempX < Width && tempY >= 0 && tempY < Height && !rows[tempY][tempX].IsBlocked)
                            return (MotionDirection)tempDirection;
                        // TODO - GetBigMoveable (large monsters)
                    }
                    break;
                case MotionTurn.Left: // left
                    for (int i = (int)direction; i >= (int)direction - 7; i--)
                    {
                        tempDirection = i;
                        if (tempDirection < 1) tempDirection += 8;
                        tempX = sourceX + Globals.MoveDirectionX[tempDirection];
                        tempY = sourceY + Globals.MoveDirectionY[tempDirection];
                        if (tempX >= 0 && tempX < Width && tempY >= 0 && tempY < Height && !rows[tempY][tempX].IsBlocked) 
                            return (MotionDirection)tempDirection;
                    }
                    break;
            }

            return MotionDirection.None;
        }

        public byte[] GetMapDataPan(Character player, int sourceX, int sourceY, MotionDirection direction)
        {
            byte[] buffer = new byte[10240];

            int header, size = 0, tileCount = 0;
            int temp, tempStatus, status;
            int index = 0, x, y;
            while (true)
            {
                x = Globals.MoveLocationX[(int)direction, index];
                y = Globals.MoveLocationY[(int)direction, index];
                if ((x == -1) || (y == -1)) break;
                index++;

                MapTile tile = rows[sourceY + y][sourceX + x];
                if (tile.Owner != null || tile.DeadOwner != null || tile.Items.Count > 0 || tile.DynamicObject != null)
                {
                    tileCount++;
                    Buffer.BlockCopy(BitConverter.GetBytes((short)x), 0, buffer, size, 2);
                    size += 2;
                    Buffer.BlockCopy(BitConverter.GetBytes((short)y), 0, buffer, size, 2);
                    size += 2;

                    header = 0;
                    if (tile.Owner != null)
                        switch (tile.Owner.OwnerType)
                        {
                            case OwnerType.Player: header = header | 0x01; break;
                            case OwnerType.Npc: header = header | 0x01; break;
                        }

                    if (tile.DeadOwner != null)
                        switch (tile.DeadOwner.OwnerType)
                        {
                            case OwnerType.Player: header = header | 0x02; break;
                            //case OwnerType.Npc: header = header | 0x02; break; //TODO <- fix - causes client crash when dead npcs are updated on edge of screen.
                        }

                    if (tile.Items.Count > 0) header = header | 0x04;
                    if (tile.DynamicObject != null) header = header | 0x08;

                    buffer[size] = (byte)header;
                    size++;
                    if ((header & 0x01) != 0)
                        switch (tile.Owner.OwnerType)
                        {
                            case OwnerType.Player:
                                Character c = (Character)tile.Owner;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)c.ClientID), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)c.Type), 0, buffer, size, 2);
                                size += 2;
                                buffer[size] = (byte)((int)c.Direction);
                                size++;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)c.Appearance1), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)c.Appearance2), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)c.Appearance3), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)c.Appearance4), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes(c.AppearanceColour), 0, buffer, size, 4);
                                size += 4;

                                unchecked { tempStatus = player.Status & (int)0x0F0FFFF7F; }
                                if (c.Town != player.Town)
                                {
                                    if (player.IsAdmin) temp = player.Status;
                                    else if (c.ClientID != player.ClientID) temp = tempStatus;
                                    else temp = player.Status;
                                }
                                else temp = player.Status;

                                status = ((0x0FFFFFFF & temp) | (c.GetPlayerRelationship(player) << 28));
                                Buffer.BlockCopy(status.GetBytes(), 0, buffer, size, 4);
                                size += 4;

                                Buffer.BlockCopy(c.Name.GetBytes(10), 0, buffer, size, 10);
                                size += 10;

                                break;
                            case OwnerType.Npc:
                                Npc n = (Npc)tile.Owner;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)n.ID+10000), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)n.Type), 0, buffer, size, 2);
                                size += 2;
                                buffer[size] = (byte)((int)n.Direction);
                                size++;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)n.Appearance2), 0, buffer, size, 2);
                                size += 2;

                                status = (0x0FFFFFFF & n.Status | (player.GetNpcRelationship(n) << 28));
                                Buffer.BlockCopy(BitConverter.GetBytes(status), 0, buffer, size, 4);
                                size += 4;

                                Buffer.BlockCopy(n.Name.GetBytes(5), 0, buffer, size, 5);
                                size += 5;
                                break;
                        }

                    if ((header & 0x02) != 0)
                        switch (tile.DeadOwner.OwnerType)
                        {
                            case OwnerType.Player:
                                Character dc = (Character)tile.DeadOwner;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)dc.ClientID), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)dc.Type), 0, buffer, size, 2);
                                size += 2;
                                buffer[size] = (byte)((int)dc.Direction);
                                size++;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)dc.Appearance1), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)dc.Appearance2), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)dc.Appearance3), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)dc.Appearance4), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes(dc.AppearanceColour), 0, buffer, size, 4);
                                size += 4;

                                unchecked { tempStatus = player.Status & (int)0x0F0FFFF7F; }
                                if (dc.Town != player.Town)
                                {
                                    if (player.IsAdmin) temp = player.Status;
                                    else if (dc.ClientID != player.ClientID) temp = tempStatus;
                                    else temp = player.Status;
                                }
                                else temp = player.Status;

                                status = ((0x0FFFFFFF & temp) | (dc.GetPlayerRelationship(player) << 28));
                                Buffer.BlockCopy(status.GetBytes(), 0, buffer, size, 4);
                                size += 4;

                                Buffer.BlockCopy(dc.Name.GetBytes(10), 0, buffer, size, 10);
                                size += 10;

                                break;
                            case OwnerType.Npc:
                                Npc dn = (Npc)tile.DeadOwner;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)dn.ID+10000), 0, buffer, size, 2);
                                size += 2;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)dn.Type), 0, buffer, size, 2);
                                size += 2;
                                buffer[size] = (byte)((int)dn.Direction);
                                size++;
                                Buffer.BlockCopy(BitConverter.GetBytes((short)dn.Appearance2), 0, buffer, size, 2);
                                size += 2;

                                status = ((0x0FFFFFFF & dn.Status) | ( player.GetNpcRelationship(dn) << 28 )) ;
                                Buffer.BlockCopy(BitConverter.GetBytes(status), 0, buffer, size, 4);
                                size += 4;

                                Buffer.BlockCopy(dn.Name.GetBytes(5), 0, buffer, size, 5);
                                size += 5;
                                break;
                        }

                    if (tile.Items.Count > 0 && tile.Items.CheckItem() != null)
                    {
                        Item i = tile.Items.CheckItem();

                        Buffer.BlockCopy(BitConverter.GetBytes((short)i.Sprite), 0, buffer, size, 2);
                        size += 2;
                        Buffer.BlockCopy(BitConverter.GetBytes((short)i.SpriteFrame), 0, buffer, size, 2);
                        size += 2;
                        Buffer.BlockCopy(BitConverter.GetBytes((short)i.Colour), 0, buffer, size, 1);
                        size++;
                    }

                    if (tile.DynamicObject != null)
                    {
                        Buffer.BlockCopy(BitConverter.GetBytes((short)tile.DynamicObject.ID), 0, buffer, size, 2);
                        size += 2;
                        Buffer.BlockCopy(BitConverter.GetBytes((short)tile.DynamicObject.Type), 0, buffer, size, 2);
                        size += 2;
                    }
                }
            }

            byte[] data = new byte[size+2];
            Buffer.BlockCopy(BitConverter.GetBytes((short)tileCount), 0, data, 0, 2);
            Buffer.BlockCopy(buffer, 0, data, 2, size);

            return data;
        }

        public byte[] GetMapData(Character owner, int sourceX, int sourceY)
        {
            byte[] buffer = new byte[10240];

            int header, size = 0, tileCount = 0;
            int temp, tempStatus, status;
            for (int y = 0; y < 16; y++)
                for (int x = 0; x < 21; x++)
                {
                    MapTile tile = rows[sourceY + y][sourceX + x];
                    if (tile.Owner != null || tile.DeadOwner != null || tile.Items.Count > 0 || tile.DynamicObject != null)
                    {
                        tileCount++;
                        Buffer.BlockCopy(BitConverter.GetBytes((short)x), 0, buffer, size, 2);
                        size += 2;
                        Buffer.BlockCopy(BitConverter.GetBytes((short)y), 0, buffer, size, 2);
                        size += 2;

                        header = 0;
                        if (tile.Owner != null)
                            switch (tile.Owner.OwnerType)
                            {
                                case OwnerType.Player: header = header | 0x01; break;
                                case OwnerType.Npc:    header = header | 0x01; break;
                            }

                        if (tile.DeadOwner != null)
                            switch (tile.DeadOwner.OwnerType)
                            {
                                case OwnerType.Player: header = header | 0x02; break;
                                //case OwnerType.Npc:    header = header | 0x02; break;
                            }

                        if (tile.Items.Count > 0)       header = header | 0x04;
                        if (tile.DynamicObject != null) header = header | 0x08;

                        buffer[size] = (byte)header;
                        size ++;
                        if ((header & 0x01) != 0)
                            switch (tile.Owner.OwnerType)
                            {
                                case OwnerType.Player:
                                    Character c = (Character)tile.Owner;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)c.ClientID), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)c.Type), 0, buffer, size, 2);
                                    size += 2;
                                    buffer[size] = (byte)((int)c.Direction);
                                    size ++;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)c.Appearance1), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)c.Appearance2), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)c.Appearance3), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)c.Appearance4), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes(c.AppearanceColour), 0, buffer, size, 4);
                                    size += 4;

                                    unchecked { tempStatus = owner.Status & (int)0x0F0FFFF7F; }
                                    if (c.Town != owner.Town)
                                    {
                                        if (owner.IsAdmin) temp = owner.Status;
                                        else if (c.ClientID != owner.ClientID) temp = tempStatus;
                                        else temp = owner.Status;
                                    }
                                    else temp = owner.Status;

                                    status = ((0x0FFFFFFF & temp) | (c.GetPlayerRelationship(owner) << 28));
                                    Buffer.BlockCopy(status.GetBytes(), 0, buffer, size, 4);
                                    size += 4;

                                    Buffer.BlockCopy(c.Name.GetBytes(10), 0, buffer, size, 10);
                                    size += 10;

                                    break;
                                case OwnerType.Npc:
                                    Npc n = (Npc)tile.Owner;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)n.ID+10000), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)n.Type), 0, buffer, size, 2);
                                    size += 2;
                                    buffer[size] = (byte)((int)n.Direction);
                                    size ++;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)n.Appearance2), 0, buffer, size, 2);
                                    size += 2;

                                    status = ((0x0FFFFFFF & n.Status) | ( owner.GetNpcRelationship(n) << 28 )) ;
                                    Buffer.BlockCopy(BitConverter.GetBytes(status), 0, buffer, size, 4);
                                    size += 4;

                                    Buffer.BlockCopy(n.Name.GetBytes(5), 0, buffer, size, 5);
                                    size += 5;
                                    break;
                            }

                        if ((header & 0x02) != 0)
                            switch (tile.DeadOwner.OwnerType)
                            {
                                case OwnerType.Player:
                                    Character dc = (Character)tile.DeadOwner;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)dc.ClientID), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)dc.Type), 0, buffer, size, 2);
                                    size += 2;
                                    buffer[size] = (byte)((int)dc.Direction);
                                    size ++;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)dc.Appearance1), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)dc.Appearance2), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)dc.Appearance3), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)dc.Appearance4), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes(dc.AppearanceColour), 0, buffer, size, 4);
                                    size += 4;

                                    unchecked { tempStatus = owner.Status & (int)0x0F0FFFF7F; }
                                    if (dc.Town != owner.Town)
                                    {
                                        if (owner.IsAdmin) temp = owner.Status;
                                        else if (dc.ClientID != owner.ClientID) temp = tempStatus;
                                        else temp = owner.Status;
                                    }
                                    else temp = owner.Status;

                                    status = ((0x0FFFFFFF & temp) | (dc.GetPlayerRelationship(owner) << 28));
                                    Buffer.BlockCopy(status.GetBytes(), 0, buffer, size, 4);
                                    size += 4;

                                    Buffer.BlockCopy(dc.Name.GetBytes(10), 0, buffer, size, 10);
                                    size += 10;

                                    break;
                                case OwnerType.Npc:
                                    Npc dn = (Npc)tile.DeadOwner;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)dn.ID + 10000), 0, buffer, size, 2);
                                    size += 2;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)dn.Type), 0, buffer, size, 2);
                                    size += 2;
                                    buffer[size] = (byte)((int)dn.Direction);
                                    size ++;
                                    Buffer.BlockCopy(BitConverter.GetBytes((short)dn.Appearance2), 0, buffer, size, 2);
                                    size += 2;

                                    status = (0x0FFFFFFF & dn.Status | ( owner.GetNpcRelationship(dn) << 28 )) ;
                                    Buffer.BlockCopy(BitConverter.GetBytes(status), 0, buffer, size, 4);
                                    size += 4;

                                    Buffer.BlockCopy(dn.Name.GetBytes(5), 0, buffer, size, 5);
                                    size += 5;
                                    break;
                            }

                        if (tile.Items.Count > 0 && tile.Items.CheckItem() != null)
                        {
                            Item i = tile.Items.CheckItem();

                            Buffer.BlockCopy(BitConverter.GetBytes((short)i.Sprite), 0, buffer, size, 2);
                            size += 2;
                            Buffer.BlockCopy(BitConverter.GetBytes((short)i.SpriteFrame), 0, buffer, size, 2);
                            size += 2;
                            Buffer.BlockCopy(BitConverter.GetBytes((short)i.Colour), 0, buffer, size, 1);
                            size ++;
                        }

                        if (tile.DynamicObject != null)
                        {
                            Buffer.BlockCopy(BitConverter.GetBytes((short)tile.DynamicObject.ID), 0, buffer, size, 2);
                            size += 2;
                            Buffer.BlockCopy(BitConverter.GetBytes((short)tile.DynamicObject.Type), 0, buffer, size, 2);
                            size += 2;
                        }
                    }
                }

            byte[] data = new byte[size + 2];

            Buffer.BlockCopy(BitConverter.GetBytes((short)tileCount), 0, data, 0, 2);
            Buffer.BlockCopy(buffer, 0, data, 2, size);

            return data;
        }

        /// <summary>
        /// Gets X and Y coordinates of the nearest empty tile. If the source is not empty, a nearby location is selected starting on the tile to the east of the source, going clockwise in a spiral.
        /// </summary>
        /// <param name="sourceX">X coordinate of the requested tile.</param>
        /// <param name="sourceY">Y coordinate of the requested tile.</param>
        /// <param name="destinationX">X coordinate of the closest empty tile.</param>
        /// <param name="destinationY">Y coordinate of the closest empty tile.</param>
        /// <returns></returns>
        public bool GetEmptyTile(int sourceX, int sourceY, out int destinationX, out int destinationY)
        {
            destinationX = destinationY = -1;

            for (int i = 0; i < 25; i++)
            {
                int x = sourceX + Globals.EmptyPositionX[i];
                int y = sourceY + Globals.EmptyPositionY[i];
                if (y >= 0 && y < Height && 
                    x >= 0 && x < Width &&
                    rows[y] != null &&
                    rows[y][x] != null &&
                    rows[y][x].IsMoveable &&
                    !rows[y][x].IsBlocked &&
                    !rows[y][x].IsTeleport &&
                    !rows[y][x].IsWater)
                {
                    destinationX = sourceX + Globals.EmptyPositionX[i];
                    destinationY = sourceY + Globals.EmptyPositionY[i];

                    return true;
                }
            }

            return false;
        }

        public void AddRow(MapRow row)
        {
            rows.Add(row);
        }

        public MapRow this[int y]
        {
            get { return rows[y]; }
        }

        public List<MapRow> Rows { get { return rows; } }
        public int Height { get { return rows.Count; } }
        public int Width { get { return rows[0].Tiles.Count; } }
        public List<Teleport> Teleports { set { teleports = value; } get { return teleports; } }
        public List<MobSpawn> MobSpawns { set { mobSpawns = value; } get { return mobSpawns; } }
        public List<NpcSpawn> NpcSpawns { set { npcSpawns = value; } get { return npcSpawns; } }
        public List<Location> DefaultLocations { get { return defaultLocations; } }
        public List<Zone> SafeZones { get { return safeZones; } }

        public Boolean IsLoaded
        {
            get { return isLoaded; }
        }

        public String Name
        {
            get { return name; }
        }

        public String FriendlyName
        {
            get { return friendlyName; }
        }

        public String FileName
        {
            get { return amdFileName; }
        }

        public WeatherType Weather
        {
            get { return weatherType; }
        }

        public DateTime WeatherTime
        {
            get { return weatherStartTime; }
        }

        public TimeSpan WeatherDuration
        {
            get { return weatherDuration; }
        }

        public TimeOfDay TimeOfDay
        {
            get {
                if (IsBuilding) return HelbreathWorld.TimeOfDay.Day;
                else return timeOfDay; 
            }
            set { timeOfDay = value; }
        }

        public BuildingType BuildingType { get { return buildingType; } set { buildingType = value; } }
        public bool IsBuilding { get { return isBuilding; } set { isBuilding = value; } }
        public bool IsFightZone { get { return isFightZone; } set { isFightZone = value; } }
        public bool IsAttackEnabled { get { return isAttackEnabled; } set { isAttackEnabled = value; } }
        public bool IsSafeMap { get { return isSafeMap; } set { isSafeMap = value; } }
        public bool IsHeldenianMap { get { return name.Equals(Globals.HeldenianBattleFieldName) || name.Equals(Globals.HeldenianCastleName) || name.Equals(Globals.HeldenianRampartName); } } // TODO - maybe come from config instead of global strings?
        public bool IsHuntZone { get { return isHuntZone; } set { isHuntZone = value; } }
        public bool LootEnabled { get { return lootEnabled; } set { lootEnabled = value; } }
        public int TotalMobs { get { return totalMobs; } set { totalMobs = value; } }
        public int NpcIndex { get { return npcIndex; } set { npcIndex = value; } }

        // caching
        public List<int> Players { get { return players; } set { players = value; } }
        public List<int> Npcs { get { return npcs; } set { npcs = value; } }
        public List<int> DynamicObjects { get { return dynamicObjects; } set { dynamicObjects = value; } }

        public bool Equals(Map other)
        {
            return (other.Name.Equals(this.name));
        }

        private void OnDynamicObjectRemoved(IDynamicObject dynamicObject)
        {
            dynamicObjects.Remove(dynamicObject.ID);
            if (DynamicObjectRemoved != null) DynamicObjectRemoved(dynamicObject);
        }

        private void LogMessage(string message)
        {
            if (MessageLogged != null) MessageLogged(message, LogType.Map);
        }
    }

    public class MapRow
    {
        private List<MapTile> tiles;

        public MapRow()
        {
            tiles = new List<MapTile>();
        }

        public void AddTile(MapTile tile)
        {
            tiles.Add(tile);
        }

        public MapTile this[int x]
        {
            get { return tiles[x]; }
        }

        public List<MapTile> Tiles
        {
            get { return tiles; }
        }
    }

    public class MapTile
    {
        private readonly object tileLock = new object();

        private bool isTeleport;
        private bool isMoveAllowed;
        private bool isFarmingAllowed;
        private bool isWater;
        private bool isSafe;

        private IOwner owner;
        private IDeadOwner deadOwner;
        private IDynamicObject dynamicObject;

        private Teleport teleport;

        private ItemStack items;

        public MapTile()
        {
            items = new ItemStack();
        }

        public bool Parse(byte[] data)
        {
            try
            {
                if ((data[8] & 0x80) != 0)
                    isMoveAllowed = false;
                else isMoveAllowed = true;

                if ((data[8] & 0x40) != 0)
                     isTeleport = true;
                else isTeleport = false;

                if ((data[8] & 0x20) != 0)
                    isFarmingAllowed = true;
                else isFarmingAllowed = false;

                if ((int)data[0] == 19)
                    isWater = true;
                else isWater = false;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public Item GetItem()
        {
            if (items.Count > 0)
                return items.RemoveItem();
            else return null;
        }

        public bool SetItem(Item item)
        {
            if (items.Count >= 20)
                return false;
            else
            {
                item.DropTime = DateTime.Now;
                items.AddItem(item);
                return true;
            }
        }

        public void SetOwner(IOwner owner)
        {
            lock (tileLock)
            {
                this.owner = owner;
            }
        }

        public void SetDeadOwner(IDeadOwner deadOwner)
        {
            lock (tileLock)
            {
                this.deadOwner = deadOwner;
            }
        }

        public void SetDynamicObject(IDynamicObject dynamicObject)
        {
            lock (tileLock)
            {
                this.dynamicObject = dynamicObject;
            }
        }

        public void ClearOwner()
        {
            lock (tileLock)
            {
                this.owner = null;
            }
        }

        public void ClearDeadOwner()
        {
            lock (tileLock)
            {
                this.deadOwner = null;
            }
        }

        public void ClearDynamicObject()
        {
            lock (tileLock)
            {
                this.dynamicObject = null;
            }
        }

        public void SetTeleport(Teleport teleport)
        {
            this.teleport = teleport;
            this.isTeleport = true;
        }

        public IOwner Owner
        {
            get { return owner; }
        }

        public IDeadOwner DeadOwner
        {
            get { return deadOwner; }
        }

        public IDynamicObject DynamicObject
        {
            get { return dynamicObject; }
        }

        public ItemStack Items
        {
            get { return items; }
        }

        public bool IsMoveable { get { return isMoveAllowed; } }
        public bool IsWater { get { return isWater; } }
        public bool IsFarm { get { return isFarmingAllowed; } }

        /// <summary>
        /// Returns true if there is an owner here.
        /// </summary>
        public bool IsOccupied
        {
            get { return (owner != null); }
        }

        public bool IsDynamicOccupied
        {
            get { return (dynamicObject != null); }
        }

        /// <summary>
        /// Returns true if there is an owner or a non-traversable dynamic object such as a mining rock blocking.
        /// </summary>
        public bool IsBlocked
        {
            get { return (IsOccupied || ((dynamicObject != null) && (!dynamicObject.IsTraversable))); }
        }

        public bool IsTeleport
        {
            get { return isTeleport; }
        }

        public Teleport Teleport
        {
            get { return teleport; }
        }

        public bool IsSafe
        {
            set { isSafe = value; }
            get { return isSafe; }
        }
    }

    public struct Teleport
    {
        private Location source;
        private Location destination;
        private MotionDirection direction;

        public Teleport(Location source, Location destination, MotionDirection direction)
        {
            this.source = source;
            this.destination = destination;
            this.direction = direction;
        }

        public Location Source { get { return source; } }
        public Location Destination { get { return destination; } }
        public MotionDirection Direction { get { return direction; } }
    }

    public class MobSpawn
    {
        private Zone zone;
        private string npcName;
        private int total;
        private int count;

        public MobSpawn(Zone zone, string npcName, int total)
        {
            this.zone = zone;
            this.npcName = npcName;
            this.total = total;
        }

        public Zone Zone { get { return zone; } }
        public String NpcName { get { return npcName; } }
        public int Total { get { return total; } }
        public int Count { get { return count; } set { count = value; } }
    }

    public class NpcSpawn
    {
        private string name;
        private Location location;
        private Zone zone;
        private MovementType moveType;

        public NpcSpawn(string name, Location location, MovementType moveType)
        {
            this.name = name;
            this.location = location;
            this.moveType = moveType;
        }

        public NpcSpawn(string name, Zone zone, MovementType moveType)
        {
            this.name = name;
            this.zone = zone;
            this.moveType = moveType;
        }

        public string Name { get { return name; } }
        public MovementType MoveType { get { return moveType; } }
        public Location Location { get { return location; } }
        public Zone Zone { get { return zone; } }
    }

    public class Location
    {
        private int x;
        private int y;
        private string mapName;

        public Location(string mapName, int x, int y)
        {
            this.mapName = mapName;
            this.x = x;
            this.y = y;
        }

        public Location(int x, int y)
        {
            this.x = x;
            this.y = y;
            this.mapName = string.Empty;
        }

        public Location Copy()
        {
            return new Location(mapName, x, y);
        }

        public Location Copy(int offsetX, int offsetY)
        {
            return new Location(mapName, x + offsetX, y + offsetY);
        }

        public bool Equals(Location location)
        {
            return (this.x == location.X &&
                    this.y == location.Y &&
                    this.mapName.Equals(location.MapName));
        }

        public String MapName { get { return mapName; } }
        public int X { get { return x; } set { x = value; } }
        public int Y { get { return y; } set { y = value; } }
    }

    public class Zone
    {
        private int top,left,bottom,right;
        private string mapName;

        public Zone(string mapName, int top, int left, int bottom, int right)
        {
            this.mapName = mapName;
            this.top = top;
            this.left = left;
            this.bottom = bottom;
            this.right = right;
        }

        public Zone(int top, int left, int bottom, int right)
        {
            this.top = top;
            this.left = left;
            this.bottom = bottom;
            this.right = right;
        }

        public List<Location> GetAllLocations()
        {
            List<Location> locations = new List<Location>();
            for (int y = right; y < left; y++)
                for (int x = top; x < bottom; x++)
                    locations.Add(new Location(x, y));

            return locations;
        }

        public Location GetRandomLocation()
        {
            int rangeX = right - left;
            int rangeY = bottom - top;
            return new Location(mapName, left + Dice.Roll(1, rangeX), top + Dice.Roll(1, rangeY));
        }

        public String MapName { get { return mapName; } }
        public int Left { get { return left; } }
        public int Top { get { return top; } }
        public int Right { get { return right; } }
        public int Bottom { get { return bottom; } }
    }
}
