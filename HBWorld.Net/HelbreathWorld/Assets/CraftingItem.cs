﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace HelbreathWorld.Common.Assets
{
    public class CraftingItem
    {
        private string name;
        private int difficulty;
        private int skillLimit;
        private Dictionary<string, int> ingredients;

        public CraftingItem()
        {
            ingredients = new Dictionary<string, int>();
        }

        public static CraftingItem ParseXml(XmlReader r)
        {
            CraftingItem craftItem = new CraftingItem();
            craftItem.Name = r["Name"];
            craftItem.Difficulty = Int32.Parse(r["Difficulty"]);
            craftItem.SkillLimit = Int32.Parse(r["SkillLevel"]);
            XmlReader ingrediantReader = r.ReadSubtree();
            while (ingrediantReader.Read())
                if (ingrediantReader.IsStartElement() && ingrediantReader.Name.Equals("Ingrediant") && !craftItem.Ingredients.ContainsKey(r["Name"]))
                    craftItem.Ingredients.Add(r["Name"], Int32.Parse(r["Count"]));
            ingrediantReader.Close();
            return craftItem;
        }

        public string Name { get { return name; } set { name = value; } }
        public int Difficulty { get { return difficulty; } set { difficulty = value; } }
        public int SkillLimit { get { return skillLimit; } set { skillLimit = value; } }
        public Dictionary<string, int> Ingredients { get { return ingredients; } set { ingredients = value; } }
    }
}
