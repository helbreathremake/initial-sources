﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelbreathWorld.Common.Assets
{
    public class Party
    {
        private List<int> members;
        private int leaderID;
        private World world;

        private Guid id;

        public Party(int leaderID)
        {
            members = new List<int>();
            for (int i = 0; i < Globals.MaximumPartyMembers; i++)
                members.Add(-1);

            AddMember(leaderID);

            this.leaderID = leaderID;
            this.id = new Guid();
        }

        public bool AddMember(int playerID)
        {
            if (MemberCount < 12)
            {
                for (int i = 0; i < Globals.MaximumPartyMembers; i++)
                    if (members[i] == -1)
                    {
                        members[i] = playerID;
                        break;
                    }

                return true;
            }
            else return false;
        }

        public bool RemoveMember(int playerID)
        {
            for (int i = 0; i < members.Count; i++)
                if (members[i] == playerID)
                    members[i] = -1;

            // reallocate a leader
            if (playerID == leaderID)
                for (int i = 0; i < members.Count; i++)
                    if (members[i] != -1)
                        leaderID = members[i];

            return true;
        }

        public List<int> Members { get { return members; } set { members = value; } }
        public int MemberCount { get { int count = 0; for (int i = 0; i < Globals.MaximumPartyMembers; i++) if (members[i] != -1) count++; return count; } }
        public World World { get { return world; } set { world = value; } }
        public Guid ID { get { return id; } }
    }
}
