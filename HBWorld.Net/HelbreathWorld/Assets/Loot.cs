﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace HelbreathWorld.Common.Assets
{
    public class LootTable
    {
        private string npcName;

        private Dictionary<ItemRarity, StringCollection> tables; // list of items per rarity
        private Dictionary<ItemRarity, StringCollection> cache; // list cache based on maximum rarity

        public LootTable(string npcName)
        {
            this.npcName = npcName;

            tables = new Dictionary<ItemRarity, StringCollection>();
            tables.Add(ItemRarity.VeryCommon, new StringCollection());
            tables.Add(ItemRarity.Common, new StringCollection());
            tables.Add(ItemRarity.Uncommon, new StringCollection());
            tables.Add(ItemRarity.Rare, new StringCollection());
            tables.Add(ItemRarity.VeryRare, new StringCollection());
            tables.Add(ItemRarity.UltraRare, new StringCollection());

            cache = new Dictionary<ItemRarity, StringCollection>();
        }

        /// <summary>
        /// Generates a list of loot based on maximum rarity. For example if maximum rarity is Rare, it will also
        /// return Uncommon, Common and VeryCommon items. Caches lists on first call.
        /// </summary>
        /// <param name="maximumRarity">The maximum rarity of the items allowed in the list.</param>
        /// <returns>List of item names.</returns>
        public StringCollection GetLoot(ItemRarity maximumRarity)
        {
            if (!cache.ContainsKey(maximumRarity))
            {
                StringCollection itemsCache = new StringCollection();
                switch (maximumRarity)
                {
                    case ItemRarity.VeryCommon:
                        foreach (KeyValuePair<ItemRarity, StringCollection> list in tables)
                            if (list.Key != ItemRarity.UltraRare &&
                                list.Key != ItemRarity.VeryRare &&
                                list.Key != ItemRarity.Rare &&
                                list.Key != ItemRarity.Uncommon &&
                                list.Key != ItemRarity.Common)
                                foreach (string item in list.Value)
                                    itemsCache.Add(item);
                        break;
                    case ItemRarity.Common:
                        foreach (KeyValuePair<ItemRarity, StringCollection> list in tables)
                            if (list.Key != ItemRarity.UltraRare &&
                                list.Key != ItemRarity.VeryRare &&
                                list.Key != ItemRarity.Rare &&
                                list.Key != ItemRarity.Uncommon)
                                foreach (string item in list.Value)
                                    itemsCache.Add(item);
                        break;
                    case ItemRarity.Uncommon:
                        foreach (KeyValuePair<ItemRarity, StringCollection> list in tables)
                            if (list.Key != ItemRarity.UltraRare &&
                                list.Key != ItemRarity.VeryRare &&
                                list.Key != ItemRarity.Rare)
                                foreach (string item in list.Value)
                                    itemsCache.Add(item);
                        break;
                    case ItemRarity.Rare:
                        foreach (KeyValuePair<ItemRarity, StringCollection> list in tables)
                            if (list.Key != ItemRarity.UltraRare &&
                                list.Key != ItemRarity.VeryRare)
                                foreach (string item in list.Value)
                                    itemsCache.Add(item);
                        break;
                    case ItemRarity.VeryRare:
                        foreach (KeyValuePair<ItemRarity, StringCollection> list in tables)
                            if (list.Key != ItemRarity.UltraRare)
                                foreach (string item in list.Value)
                                    itemsCache.Add(item);
                        break;
                    case ItemRarity.UltraRare:
                        foreach (StringCollection list in tables.Values)
                            foreach (string item in list)
                                itemsCache.Add(item);
                        break;
                }
                cache.Add(maximumRarity, itemsCache);
            }

            return cache[maximumRarity];
        }

        public string NpcName { get { return npcName; } }
        public StringCollection this[ItemRarity rarity]
        {
            set { tables[rarity] = value; }
            get { return tables[rarity]; }
        }
    }
}
