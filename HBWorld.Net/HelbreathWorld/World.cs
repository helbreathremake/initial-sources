﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using HelbreathWorld.Common;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Common.Assets.Objects;
using HelbreathWorld.Common.Assets.Objects.Dynamic;
using HelbreathWorld.Common.Events;
using HelbreathWorld.Common.Assets.Quests;

namespace HelbreathWorld.Common
{
    public class World
    {
        // GAMESERVER EVENTS
        public event SendDataHandler SendData;
        public event LogHandler MessageLogged;
        public event GuildHandler GuildCreated;
        public event GuildHandler GuildDisbanded;
        public event WorldEventHandler WorldEventEnded;

        // CONFIGURATIONS
        public static Dictionary<string, Item> ItemConfiguration;
        public static Dictionary<string, Npc> NpcConfiguration;
        public static Dictionary<string, LootTable> PrimaryLootConfiguration;
        public static Dictionary<string, LootTable> SecondaryLootConfiguration;
        public static Dictionary<int, Magic> MagicConfiguration;
        public static Dictionary<string, ManufactureItem> ManufacturingConfiguration;
        public static Dictionary<string, AlchemyItem> AlchemyConfiguration;
        public static Dictionary<string, CraftingItem> CraftingConfiguration;
        public static Dictionary<int, string> SummonConfiguration;
        public static Dictionary<int, Quest> QuestConfiguration;
        public static Apocalypse ApocalypseConfiguration;
        public static Crusade CrusadeConfiguration;
        public static Heldenian HeldenianConfiguration;
        public static CaptureTheFlag CaptureTheFlagConfiguration;
        public static Dictionary<WorldEventType, WorldEventResult> WorldEventResults;
        public string News;

        // NPCS
        private bool[] npcIndexes; // available indexes. client limitations
        private Dictionary<int, Npc> npcs;
        public readonly object NpcLock = new object();
        private DateTime npcTimer;
        private DateTime npcActionTimer;
        private DateTime spawnTimer;

        // PLAYERS
        private Dictionary<int, Character> players;
        public readonly object PlayerLock = new object();
        private DateTime playerTimer;

        // GUILDS
        private Dictionary<string, Guild> guilds;

        // PARTIES
        private List<Party> parties;

        // DYNAMIC OBJECTS
        private Dictionary<string, IDynamicObject> dynamicObjects;
        private DateTime dynamicObjectTimer;

        // MAPS
        private Dictionary<string, Map> maps;
        private DateTime mapTimer;

        // EVENTS
        private IWorldEvent currentEvent;
        private DateTime eventTimer;
        
        // MISC
        private TimeOfDay timeOfDay;

        public World() 
        {
            maps = new Dictionary<string, Map>();
            npcs = new Dictionary<int, Npc>();
            players = new Dictionary<int, Character>();
            guilds = new Dictionary<string, Guild>();
            parties = new List<Party>();
            dynamicObjects = new Dictionary<string, IDynamicObject>();

            npcIndexes = new bool[20000];
            for (int index = 0; index < 20000; index++) npcIndexes[index] = false;

            playerTimer = npcTimer = npcActionTimer = spawnTimer = eventTimer = mapTimer = dynamicObjectTimer = DateTime.Now;
        }

        public void AddPlayer(Character character)
        {
            // check guild exists, if not, remove set guild to none.
            if (guilds.ContainsKey(character.LoadedGuildName))
                character.Guild = guilds[character.LoadedGuildName];
            else character.Guild = Guild.None; // TODO - problem log and/or notify player?
            character.CurrentWorld = this;

            // check map exists, if not send to bleeding isle
            if (maps.ContainsKey(character.LoadedMapName))
                character.CurrentMap = maps[character.LoadedMapName];
            else if (maps.ContainsKey(Globals.BleedingIsleName))
                character.CurrentMap = maps[Globals.BleedingIsleName];
            else return;

            character.StatusChanged += new OwnerHandler(OnStatusChanged);
            character.MotionChanged += new MotionHandler(OnMotionChanged);
            character.DamageTaken += new DamageHandler(OnDamageTaken);
            character.Killed += new DeathHandler(OnKilled);
            character.ItemDropped += new ItemHandler(OnItemDropped);
            character.ItemPickedUp += new ItemHandler(OnItemPickedUp);
            character.GuildCreated += new GuildHandler(GuildCreated);
            character.GuildDisbanded += new GuildHandler(GuildDisbanded);
            character.MessageLogged += new LogHandler(MessageLogged);
            lock (PlayerLock) { players.Add(character.ClientID, character); }
            MessageLogged("[" + character.Name + "] connected", LogType.Game);

            Send(character, CommandType.ResponseInitPlayer, CommandMessageType.Confirm);
        }

        public void RemovePlayer(int playerID)
        {
            if (players.ContainsKey(playerID))
            {
                SendLogEventToNearbyPlayers(players[playerID], CommandMessageType.Reject); // tells nearby players to remove this object from the map

                if (players.ContainsKey(players[playerID].WhisperIndex))
                {
                    players[players[playerID].WhisperIndex].WhisperIndex = -1;
                    players[players[playerID].WhisperIndex].Notify(CommandMessageType.NotifyWhisperOff, "");
                }

                players[playerID].Remove();

                lock (PlayerLock)
                {
                    players.Remove(playerID);
                }
            }
        }

        public void GiveStarterItems(Character player)
        {
            int itemCount = 0;

            foreach (KeyValuePair<string, GenderType> item in Globals.NewPlayerItems)
                if (ItemConfiguration.ContainsKey(item.Key))
                {
                    switch (item.Value)
                    {
                        case GenderType.None: player.Inventory[itemCount] = ItemConfiguration[item.Key].Copy(); break;
                        default: if (player.Gender == item.Value) player.Inventory[itemCount] = ItemConfiguration[item.Key].Copy(); break;
                    }
                    itemCount++;
                }
        }
       
        /// <summary>
        /// Handles player movement, actions etc sent by client.
        /// </summary>
        /// <param name="data">Data sent by client.</param>
        /// <param name="clientID">Internal client ID.</param>
        public void PlayerProcess(byte[] data, int clientID)
        {
            if (!players.ContainsKey(clientID))
            {
                LogMessage("World.PlayerProcess: Client not found: " + clientID, LogType.Game);
                return;
            }

            int sourceX, sourceY, itemIndex;
            string characterName, guildName;

            int typeTemp = (int)BitConverter.ToInt32(data, 0);
            CommandType type;

            if (Enum.TryParse<CommandType>(typeTemp.ToString(), out type))
                switch (type)
                {
                    case CommandType.RequestInitData: InitPlayerData(clientID); break;
                    case CommandType.RequestNews: Send(clientID, CommandType.ResponseNews, CommandMessageType.Reject, News.GetBytes(News.Length)); break; // dont check size anymore, always send
                    case CommandType.RequestRestart: if (players[clientID].IsDead) players[clientID].Restart(); break;
                    case CommandType.RequestFullObjectData:
                        int id = BitConverter.ToInt16(data, 4);

                        if (id < 10000 && (id != 0) && players.ContainsKey(id))
                            Send(clientID, CommandType.Motion, CommandMessageType.ObjectStop, players[id].GetFullObjectData(players[clientID]));
                        else if (id - 10000 != 0 && npcs.ContainsKey(id - 10000))
                            Send(clientID, CommandType.Motion, CommandMessageType.ObjectStop, npcs[id - 10000].GetFullObjectData(players[clientID]));
                        break;
                    case CommandType.RequestCreateGuild: players[clientID].CreateGuild(Encoding.ASCII.GetString(data, 36, 20).Trim('\0')); break;
                    case CommandType.RequestDisbandGuild: players[clientID].DisbandGuild(Encoding.ASCII.GetString(data, 36, 20).Trim('\0')); break;
                    case CommandType.RequestMotion:
                        MotionType motion;
                        if (Enum.TryParse<MotionType>(BitConverter.ToInt16(data, 4).ToString(), out motion))
                        {
                            sourceX = BitConverter.ToInt16(data, 6);
                            sourceY = BitConverter.ToInt16(data, 8);
                            MotionDirection direction = (MotionDirection)((int)data[10]);
                            int destinationX = BitConverter.ToInt16(data, 11); // for attacking only
                            int spellNumber = destinationX; // for casting only
                            int destinationY = BitConverter.ToInt16(data, 13); // for attacking only
                            int attackType = BitConverter.ToInt16(data, 15); // for attacking only

                            int targetID, clientTime;
                            switch (motion)
                            {
                                case MotionType.Attack:
                                case MotionType.Dash:
                                    targetID = BitConverter.ToInt16(data, 17);
                                    clientTime = BitConverter.ToInt32(data, 19);

                                    bool isDash = motion == MotionType.Dash ? true : false;
                                    bool successful = false;

                                    if (isDash && players[clientID].Move(sourceX, sourceY, direction))
                                        Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMove, players[clientID].GetMotionData(MotionType.Move, CommandMessageType.ObjectConfirmMove));

                                    if (targetID != 0)
                                    {
                                        IOwner target = targetID > 10000 ? (IOwner)npcs[targetID - 10000] : (IOwner)players[targetID];
                                        if (target.OwnerType == OwnerType.Npc)
                                            switch (((Npc)target).NpcType) // special cases for building and farming etc
                                            {
                                                case NpcType.Crops: // farming crops and attacking them
                                                    if (players[clientID].Weapon != null && players[clientID].Weapon.RelatedSkill == SkillType.Farming) // plants can be "killed" with conventional weapons
                                                        successful = players[clientID].Farm(sourceX, sourceY, direction, destinationX, destinationY, (Npc)target);
                                                    else successful = players[clientID].MeleeAttack(sourceX, sourceY, direction, destinationX, destinationY, target, attackType, isDash);
                                                    break;
                                                case NpcType.CrusadeArrowTower:
                                                case NpcType.CrusadeCannonTower:
                                                case NpcType.CrusadeDetector:
                                                case NpcType.CrusadeManaCollector: // building crusade towers and attacking them
                                                    if (target.Side == players[clientID].Side && ((Npc)target).BuildPoints > 0 && players[clientID].Weapon != null && players[clientID].Weapon.RelatedSkill == SkillType.Mining) // buildPoints = 0 means the structure is complete. imcomplete structures can be "killed" also
                                                        successful = players[clientID].Build(sourceX, sourceY, direction, destinationX, destinationY, (Npc)target);
                                                    else successful = players[clientID].MeleeAttack(sourceX, sourceY, direction, destinationX, destinationY, target, attackType, isDash);
                                                    break;
                                                default: successful = players[clientID].MeleeAttack(sourceX, sourceY, direction, destinationX, destinationY, target, attackType, isDash); break;
                                            }
                                        else successful = players[clientID].MeleeAttack(sourceX, sourceY, direction, destinationX, destinationY, target, attackType, isDash);
                                    }
                                    else if (players[clientID].CurrentMap[destinationY][destinationX].DynamicObject != null) // handle dynamic objects
                                    {
                                        IDynamicObject dynamicTarget = players[clientID].CurrentMap[destinationY][destinationX].DynamicObject;
                                        switch (players[clientID].CurrentMap[destinationY][destinationX].DynamicObject.Type)
                                        {
                                            case DynamicObjectType.Rock:
                                            case DynamicObjectType.Gem:
                                                successful = players[clientID].Mine(sourceX, sourceY, direction, destinationX, destinationY, (Mineral)dynamicTarget);
                                                break;
                                        }
                                    }

                                    Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmAttack, players[clientID].GetMotionData(MotionType.Attack, CommandMessageType.ObjectConfirmAttack));
                                    //if (successful) // this will stop the animation from being shown. re-think character.Attack return value
                                    //{
                                        if (isDash) SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectAttackDash, destinationX, destinationY, attackType);
                                        else SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectAttack, destinationX, destinationY, attackType);
                                    //} 

                                    break;
                                case MotionType.Move:
                                    clientTime = BitConverter.ToInt32(data, 17);
                                    if (players[clientID].Move(sourceX, sourceY, direction))
                                    {
                                        Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMove, players[clientID].GetMotionData(motion, CommandMessageType.ObjectConfirmMove));
                                        SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectMove);
                                    }
                                    else Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectRejectMove, players[clientID].GetMotionData(motion, CommandMessageType.ObjectRejectMove));
                                    break;
                                case MotionType.Run:
                                    clientTime = BitConverter.ToInt32(data, 17);
                                    if (players[clientID].Run(sourceX, sourceY, direction))
                                    {
                                        Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMove, players[clientID].GetMotionData(motion, CommandMessageType.ObjectConfirmMove));
                                        SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectRun);
                                    }
                                    else Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectRejectMove, players[clientID].GetMotionData(motion, CommandMessageType.ObjectRejectMove));
                                    break;
                                case MotionType.Idle:
                                    clientTime = BitConverter.ToInt32(data, 17);
                                    players[clientID].Idle(sourceX, sourceY, direction);

                                    Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMotion, players[clientID].GetMotionData(motion, CommandMessageType.ObjectConfirmMotion));
                                    SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectStop);
                                    break;
                                case MotionType.PickUp:
                                    clientTime = BitConverter.ToInt32(data, 17);
                                    players[clientID].PickUp(sourceX, sourceY, direction);

                                    Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMotion, players[clientID].GetMotionData(motion, CommandMessageType.ObjectConfirmMotion));
                                    SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectPickUp);
                                    break;
                                case MotionType.Magic:
                                    clientTime = BitConverter.ToInt32(data, 17);
                                    players[clientID].PrepareMagic(sourceX, sourceY, direction);
                                    
                                    if (!players[clientID].IsCombatMode) players[clientID].ToggleCombatMode();
                                    Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMotion, players[clientID].GetMotionData(motion, CommandMessageType.ObjectConfirmMotion));
                                    SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectMagic, spellNumber, 0, 0);
                                    break;
                                case MotionType.Fly:
                                    if (players[clientID].Fly(sourceX, sourceY, direction))
                                    {
                                        Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectConfirmMove, players[clientID].GetMotionData(MotionType.Move, CommandMessageType.ObjectConfirmMove)); // MotionType.Move to keep sync
                                        SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectTakeDamageAndFly, players[clientID].LastDamage, 0, 0);
                                    } Send(clientID, CommandType.ResponseMotion, CommandMessageType.ObjectRejectMove, players[clientID].GetMotionData(MotionType.Move, CommandMessageType.ObjectRejectMove));
                                    break;
                            }
                        }
                        break;
                    case CommandType.Common:
                        CommandMessageType subType;
                        if (Enum.TryParse<CommandMessageType>(BitConverter.ToInt16(data, 4).ToString(), out subType))
                        {
                            // these stay the same for all Common commands
                            sourceX = BitConverter.ToInt16(data, 6);
                            sourceY = BitConverter.ToInt16(data, 8);
                            MotionDirection direction = (MotionDirection)((int)data[10]);
                            switch (subType)
                            {
                                case CommandMessageType.Craft: players[clientID].Craft(new int[] { (int)data[31], (int)data[32], (int)data[33], (int)data[34], (int)data[35], (int)data[36] }); break;
                                case CommandMessageType.Alchemy: players[clientID].Alchemy(new int[] { (int)data[11], (int)data[12], (int)data[13], (int)data[14], (int)data[15], (int)data[16] }); break;
                                case CommandMessageType.Manufacture: players[clientID].Manufacture(Encoding.ASCII.GetString(data, 11, 20).Trim('\0'), new int[] { (int)data[31], (int)data[32], (int)data[33], (int)data[34], (int)data[35], (int)data[36] }); break;
                                case CommandMessageType.Slate: players[clientID].Slate(new int[] { (int)data[11], (int)data[12], (int)data[13], (int)data[14], (int)data[15], (int)data[16] }); break;
                                case CommandMessageType.ToggleCombatMode: players[clientID].ToggleCombatMode(); break;
                                case CommandMessageType.ToggleSafeMode: players[clientID].ToggleSafeMode(); break;
                                default:
                                    int value1 = BitConverter.ToInt32(data, 11);
                                    int value2 = BitConverter.ToInt32(data, 15);
                                    int value3 = BitConverter.ToInt32(data, 19);
                                    string stringValue = ""; int value4 = 0;
                                    Character c; Npc n;
                                    if (data.Length > 27) // what a mess.. stay the same... except in some cases it seems
                                    {
                                        //LogMessage(subType.ToString() + "Length: " + data.Length, LogType.Test);
                                        stringValue = Encoding.ASCII.GetString(data, 23, 30).Trim('\0');
                                        value4 = BitConverter.ToInt32(data, 53);
                                    }
                                    switch (subType)
                                    {
                                        case CommandMessageType.CrusadeSetDuty: players[clientID].SetCrusadeDuty(value1); break;
                                        case CommandMessageType.CrusadeSummon: players[clientID].SummonCrusadeUnit(value1, value2, value3, value4); break;
                                        case CommandMessageType.CrusadeSetTeleportLocation: players[clientID].Guild.CrusadeTeleportLocation = new Location(stringValue.Substring(0,10).Trim('\0') , value1, value2); break;
                                        case CommandMessageType.CrusadeSetBuildLocation: players[clientID].Guild.CrusadeBuildLocation = new Location(stringValue.Substring(0, 10).Trim('\0'), value1, value2); break;
                                        case CommandMessageType.CrusadeTeleport: 
                                            if (players[clientID].Guild.CrusadeTeleportLocation != null &&
                                                players[clientID].Teleport(players[clientID].Guild.CrusadeTeleportLocation, MotionDirection.South)) InitPlayerData(clientID);
                                            break;
                                        case CommandMessageType.MapStatus: players[clientID].GetMapStatusData(value1, stringValue.Substring(0, 10).Trim('\0')); break;
                                        case CommandMessageType.Fish: players[clientID].Fish(); break;
                                        case CommandMessageType.CastMagic: // TODO Character.CastMagic and GameServer.CastMagic merge? so this is on 1 line
                                            bool notifyFailed = false; // players see "failed" above head
                                            if ((World.MagicConfiguration.ContainsKey(value3 - 100)) &&
                                                (players[clientID].CastMagic(sourceX, sourceY, World.MagicConfiguration[value3 - 100], out notifyFailed)))
                                            {
                                                SendMagicEventToNearbyPlayers(players[clientID], CommandMessageType.CastMagic, value1, value2, value3);
                                                CastMagic(players[clientID], value1, value2, World.MagicConfiguration[value3 - 100]);
                                            }
                                            else if (notifyFailed) SendMotionEventToNearbyPlayers(players[clientID], CommandMessageType.ObjectTakeDamage); // failed above head if casting probability fails (hunger/weather etc)
                                            break;
                                        case CommandMessageType.GiveItem: players[clientID].GiveItem((int)data[10], value1, value2, value3, value4, stringValue); break;  // alternative use of data[10] as itemIndex
                                        case CommandMessageType.DropItem: players[clientID].DropItem(value1, value2, stringValue); break;
                                        case CommandMessageType.EquipItem: players[clientID].EquipItem(value1); break;
                                        case CommandMessageType.UnEquipItem: players[clientID].UnequipItem(value1); break;
                                        case CommandMessageType.UseItem: players[clientID].UseItem(value1, value2, value3, value4); break;
                                        case CommandMessageType.UseSkill: players[clientID].UseSkill((SkillType)value1); break; // value2 and value3 defined as parameters on HG but unused by client and hg
                                        case CommandMessageType.UpgradeItem: players[clientID].UpgradeItem(value1); break;
                                        case CommandMessageType.UseSpecialAbility: players[clientID].UseSpecialAbility(); break;
                                        case CommandMessageType.BuySpell: players[clientID].LearnSpell(stringValue, true); break;
                                        case CommandMessageType.BuyItem: players[clientID].BuyItem(stringValue.Substring(0, 20).Trim('\0'), value1); break;
                                        case CommandMessageType.SellItem: players[clientID].SellItem(value1, value2, value3, stringValue); break;
                                        case CommandMessageType.SellItemConfirm: players[clientID].SellItemConfirm(value1, value2, stringValue); break;
                                        case CommandMessageType.RepairItem: players[clientID].RepairItem(value1, value2, stringValue); break;
                                        case CommandMessageType.RepairItemConfirm: players[clientID].RepairItemConfirm(value1, stringValue); break;
                                        // to work with old client, uncomment this and comment the new one
                                        //case CommandMessageType.GetGuildName: if (players[value1] != null) players[clientID].Notify(CommandMessageType.NotifyGuildName, players[value1].GuildRank, value2, 0, players[value1].Guild.Name); break; // when someone hovers mouse over player, it is then cached (using cache id)
                                        case CommandMessageType.GetGuildName: if (players[value1] != null) players[clientID].Notify(CommandMessageType.NotifyGuildName, players[value1].GuildRank, value1, 0, players[value1].Guild.Name); break; // when someone hovers mouse over player, it is then cached (using object id)
                                        case CommandMessageType.EnsureMagicSkill: players[clientID].LearnSkill(4, Globals.MinimumSkillLevel); break; // weird way to ensure at least 20% magic.....
                                        case CommandMessageType.HeldenianFlag: players[clientID].GetHeldenianFlag(); break;
                                        case CommandMessageType.JoinGuildApprove: if (FindPlayer(stringValue.Substring(0, 10).Trim('\0'), out c)) c.JoinGuild(players[clientID]); break;
                                        case CommandMessageType.PartyRequest:
                                            switch ((PartyRequestType)value1)
                                            {
                                                case PartyRequestType.Join: if (FindPlayer(stringValue.Substring(0, 10).Trim('\0'), out c)) players[clientID].JoinParty(c); break;
                                                case PartyRequestType.Withdraw: players[clientID].LeaveParty(); break;
                                                case PartyRequestType.Dismiss: break;
                                            }
                                            break;
                                        case CommandMessageType.HandlePartyRequest: players[clientID].HandlePartyRequest(); break;
                                        case CommandMessageType.TalkToNpc: if (FindNpc(value1, out n)) players[clientID].TalkToNpc(n); break;
                                        case CommandMessageType.AcceptQuest: players[clientID].AcceptQuest(); break;
                                        case CommandMessageType.DismissGuildsmanApprove: /*TODO*/ break;
                                        case CommandMessageType.DismissGuildsmanReject: /*TODO*/ break;
                                    }
                                    break;
                            }
                        }
                        break;
                    case CommandType.SetItemPosition:
                        itemIndex = (int)data[6];
                        sourceX = BitConverter.ToInt16(data, 7);
                        sourceY = BitConverter.ToInt16(data, 9);
                        if (itemIndex >= 0 && itemIndex < Globals.MaximumInventoryItems && players[clientID].Inventory[itemIndex] != null)
                            players[clientID].Inventory[itemIndex].BagPosition = new Location(sourceX, sourceY);
                        break;
                    case CommandType.Chat:
                        sourceX = BitConverter.ToInt16(data, 6);
                        sourceY = BitConverter.ToInt16(data, 8);
                        characterName = Encoding.ASCII.GetString(data, 10, 10).Trim('\0');
                        string message = Encoding.ASCII.GetString(data, 21, (data.Length - 21)).Trim('\0');

                        // TODO - send to login server which will copy chat to other game servers
                        //loginServerClient.Send("CHAT|" + sourceX + "|" + sourceY + "|" + characterName + "|" + message + "|");
                        players[clientID].Talk(message);
                        break;
                    case CommandType.ChangeStatsLevelUp: players[clientID].ChangeStats((int)data[6], (int)data[7], (int)data[8], (int)data[9], (int)data[10], (int)data[11]); break;
                    case CommandType.ChangeStatsMajestics:
                        // Stat enum holds the hex values for each stat... copied from game.cpp of original source, Character.ChangeStat handles these in a switch
                        players[clientID].ChangeStat((Stat)data[6]);
                        players[clientID].ChangeStat((Stat)data[7]);
                        players[clientID].ChangeStat((Stat)data[8]);
                        break;
                    case CommandType.RequestTeleport:
                        // has the teleporter been defined? if not, set the player back to it's last location
                        if (!players[clientID].CurrentLocation.IsTeleport || string.IsNullOrEmpty(players[clientID].CurrentLocation.Teleport.Destination.MapName))
                             players[clientID].Teleport(players[clientID].CurrentMap, players[clientID].LastX, players[clientID].LastY);
                        else players[clientID].Teleport(maps[players[clientID].CurrentLocation.Teleport.Destination.MapName],
                                                            players[clientID].CurrentLocation.Teleport.Destination.X,
                                                            players[clientID].CurrentLocation.Teleport.Destination.Y,
                                                            players[clientID].CurrentLocation.Teleport.Direction);
                        break;
                    case CommandType.RequestHeldenianTeleport:
                        if (IsHeldenianBattlefield || IsHeldenianSeige)
                            switch (Heldenian.Mode)
                            {
                                case HeldenianType.Battlefield:
                                    switch (players[clientID].Side)
                                    {
                                        case OwnerSide.Aresden: players[clientID].Teleport(maps[Globals.HeldenianBattleFieldName], 68, 225); break;
                                        case OwnerSide.Elvine: players[clientID].Teleport(maps[Globals.HeldenianBattleFieldName], 202, 70); break;
                                    }
                                    break;
                                case HeldenianType.CastleSeige:
                                    switch (players[clientID].Side)
                                    {
                                        case OwnerSide.Aresden:
                                        case OwnerSide.Elvine:
                                            if (players[clientID].Side == Heldenian.CastleDefender)
                                                 players[clientID].Teleport(maps[Globals.HeldenianRampartName], 81, 42);
                                            else players[clientID].Teleport(maps[Globals.HeldenianRampartName], 156, 153);
                                            break;
                                    }                                        
                                    break;
                            }
                        break;
                    case CommandType.RequestCitizenship:
                        string townName;
                        byte[] command = new byte[35];
                        if (players[clientID].BecomeCitizen(out townName))
                        {
                            Buffer.BlockCopy(((short)1).GetBytes(), 0, command, 0, 2);
                            Buffer.BlockCopy(townName.GetBytes(10), 0, command, 2, 10);
                            Send(clientID, CommandType.ResponseCitizenship, CommandMessageType.Confirm, command); // dodgy but meh
                        }
                        else
                        {
                            Buffer.BlockCopy(((short)0).GetBytes(), 0, command, 0, 2);
                            Buffer.BlockCopy(townName.GetBytes(10), 0, command, 2, 10);
                            Send(clientID, CommandType.ResponseCitizenship, CommandMessageType.Confirm, command);
                        }
                        break;
                    case CommandType.RequestWarehouseItem:
                        itemIndex = (int)data[6];
                        int bagIndex;
                        if (players[clientID].RemoveWarehouseItem(itemIndex, out bagIndex))
                             Send(clientID, CommandType.ResponseWarehouseItem, CommandMessageType.Confirm, new byte[] { (byte)itemIndex, (byte)bagIndex });
                        else Send(clientID, CommandType.ResponseWarehouseItem, CommandMessageType.Reject);
                        break;
                    case CommandType.RequestAngel:
                        switch (BitConverter.ToInt16(data, 26))
                        {
                            case 1: players[clientID].AddInventoryItem(ItemConfiguration["AngelicPandent(STR)"].Copy()); break;
                            case 2: players[clientID].AddInventoryItem(ItemConfiguration["AngelicPandent(DEX)"].Copy()); break;
                            case 3: players[clientID].AddInventoryItem(ItemConfiguration["AngelicPandent(INT)"].Copy()); break;
                            case 4: players[clientID].AddInventoryItem(ItemConfiguration["AngelicPandent(MAG)"].Copy()); break;
                        }
                        players[clientID].Majestics -= 5;
                        players[clientID].Notify(CommandMessageType.NotifyMajestics, players[clientID].Majestics);
                        break;
                    case CommandType.RequestSellItemList:
                        for (int i = 0; i < 12; i++)
                            if (data.Length > 6 + (i * 2) + 1)
                                if (players[clientID].Inventory[data[6 + (i * 2)]] != null)
                                    players[clientID].SellItemConfirm(data[6 + (i * 2)], data[6 + (i * 2) + 1], players[clientID].Inventory[data[6 + (i * 2)]].Name);

                        break;
                    case CommandType.CheckConnection: break; // unused asynchronous now
                    default: MessageLogged("Unknown Command: " + string.Format("0x{0:x8}", typeTemp), LogType.Game); break;
                }
            else MessageLogged("Unknown Command: " + string.Format("0x{0:x8}", typeTemp), LogType.Game);
        }

        /// <summary>
        /// Finds an online player by name.
        /// </summary>
        /// <param name="name">Name of the online character to find.</param>
        /// <param name="character">Character object returned to caller.</param>
        /// <returns>True or False to indicate the player was found.</returns>
        public bool FindPlayer(string name, out Character character)
        {
            character = null;
            foreach (Map map in maps.Values)
                for (int p = 0; p < map.Players.Count; p++)
                    if (players.ContainsKey(map.Players[p]))
                    {
                        Character c = players[map.Players[p]];
                        if (c != null && c.Name.Equals(name))
                        {
                            character = c;
                            return true;
                        }
                    }

            return false;
        }

        public bool FindNpc(int type, out Npc npcObj)
        {
            npcObj = null;
            foreach (Map map in maps.Values)
                for (int n = 0; n < map.Npcs.Count; n++)
                    if (npcs.ContainsKey(map.Npcs[n]))
                    {
                        Npc npc = npcs[map.Npcs[n]];
                        if (npc != null && npc.Type == type)
                        {
                            npcObj = npc;
                            return true;
                        }
                    }

            return false;
        }

        public void InitPlayerData(int clientID)
        {
            Character character = players[clientID];
            character.Init();
            Send(character, CommandType.ResponseInitData, CommandMessageType.Confirm, character.GetInitData());
            Send(character, CommandType.ResponseInitDataPlayerStats, CommandMessageType.Confirm, character.GetStatsData());
            Send(character, CommandType.ResponseInitDataPlayerItems, CommandMessageType.Confirm, character.GetItemsData());
            character.Notify(CommandMessageType.NotifyItemBagPositions);
            SendLogEventToNearbyPlayers(players[clientID], CommandMessageType.Confirm); // tells nearby players to add this object to the map
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="destinationX"></param>
        /// <param name="destinationY"></param>
        /// <param name="spell"></param>
        /// <remarks>
        /// AREA hits send the hitX and hitY of the centre point, and not the owner's location. This ensures players fly away from the center point.
        /// </remarks>
        public void CastMagic(IOwner o, int destinationX, int destinationY, Magic spell, bool isScroll = false)
        {
            if (o.OwnerType == OwnerType.Npc)
            {
                Npc caster = (Npc)o;
                IOwner owner;

                switch (spell.Type)
                {
                    case MagicType.Paralyze:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (owner.CurrentLocation.IsSafe || caster.CurrentLocation.IsSafe) break; // cant para in safe or from safe
                            if (owner.Side == OwnerSide.Neutral) break; // cant para travellers
                            if (owner.Side == caster.Side) break; //  cant para own town

                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                owner.SetMagicEffect(spell);
                        }
                        break;
                    case MagicType.DamageSingle: // damages target
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                        }
                        break;
                    case MagicType.DamageSingleSPDown: // damages target and reduce SP
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                owner.DepleteSP(Dice.Roll(spell.Effect4, spell.Effect5, spell.Effect6));
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }
                        break;
                    case MagicType.DamageArea: // damages target and all surrounding area (2 hits on target)
                    case MagicType.Tremor:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                }
                        break;
                    case MagicType.DamageAreaNoSingle: // damages surrounding area of target, but not the target itself individually (1 hit on target)
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                }
                        break;
                    case MagicType.DamageAreaNoSingleSPDown: // as above but also reduces SP
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        owner.DepleteSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3));
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.ArmourBreak: // damages target, area and reduces endurance of armour
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                owner.TakeArmourDamage(spell.Effect7 * 2);
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        owner.TakeArmourDamage(spell.Effect7 * 2);
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.Ice: // damages target, area and freezes target
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                if (!owner.EvadeIce()) owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        if (!owner.EvadeIce()) owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.DamageLine:
                    case MagicType.DamageLineSPDown:
                    case MagicType.IceLine:
                        // Hit 1: "line of site". each "point" in the LOS should have a cross-shaped hit, except the last point (next to caster) 
                        // note that like AREA spells, the center point is sent to Character.MagicAttack(x, y... to ensure players are flown away from the center point
                        List<int[]> hitLocations = Utility.GetMagicLineLocations(caster.X, caster.Y, destinationX, destinationY);
                        foreach (int[] hitLocation in hitLocations)
                        {
                            int x = hitLocation[0];
                            int y = hitLocation[1];

                            if (caster.CurrentMap[y][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y][x + 1].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x + 1].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y + 1][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y + 1][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y][x - 1].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x - 1].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y - 1][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y - 1][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                        }

                        // Hit 2: Destination should hit like damagearea
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                            owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                        else if (spell.Type == MagicType.DamageLineSPDown)
                                            owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }

                        // Hit 3: Destination should also be hit like damagesingle
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                    owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                else if (spell.Type == MagicType.DamageLineSPDown)
                                    owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                            }
                        }



                        // Note/TODO: for DamageLineSPDown (earth-shock-wave), damage is done twice (???)
                        break;
                }
            }
            else if (o.OwnerType == OwnerType.Player)
            {
                Character caster = (Character)o;
                IOwner owner;

                switch (spell.Type)
                {
                    case MagicType.Scan:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                SendClientMessage(caster, owner.Name + " - HP: " + owner.HP + "/" + owner.MaxHP + "   MP: " + owner.MP + "/" + owner.MaxMP);
                        }
                        break;
                    case MagicType.Teleport: // TODO 10 second recall when hit. some locations cant recall from, plus jail
                        if (destinationX == caster.X && destinationY == caster.Y)
                            switch (caster.Side)
                            {
                                case OwnerSide.Aresden: caster.Teleport(maps[Globals.AresdenTownName], -1, -1); break;
                                case OwnerSide.Elvine: caster.Teleport(maps[Globals.ElvineTownName], -1, -1); break;
                                case OwnerSide.Neutral: caster.Teleport(maps[Globals.TravellerTownName], -1, -1); break;
                            }
                        break;
                    case MagicType.Possession:
                        if (!caster.CurrentMap[destinationY][destinationX].IsOccupied &&
                             caster.CurrentMap[destinationY][destinationX].Items.Count > 0)
                            if (caster.AddInventoryItem(caster.CurrentMap[destinationY][destinationX].GetItem()))
                            {
                                Item nextItem;
                                if (caster.CurrentMap[destinationY][destinationX].Items.Count <= 0)
                                    nextItem = Item.Empty();
                                else
                                {
                                    Item next = caster.CurrentMap[destinationY][destinationX].Items.CheckItem();
                                    if (next == null)
                                        nextItem = Item.Empty();
                                    else nextItem = next;
                                }
                                SendItemEventToNearbyPlayers(caster, CommandMessageType.SetItem, destinationX, destinationY, nextItem);
                            }
                        break;
                    case MagicType.Cancellation:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied
                            && caster.CurrentMap[destinationY][destinationX].Owner.OwnerType == OwnerType.Player)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            for (int i = 0; i < 50; i++)
                                if (owner.MagicEffects.ContainsKey((MagicType)i))
                                    switch ((MagicType)i)
                                    {
                                        case MagicType.Ice: break; // ignore these
                                        default: owner.RemoveMagicEffect((MagicType)i); break;
                                    }
                        }
                        break;
                    case MagicType.Summon:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied &&
                            caster.CurrentMap[destinationY][destinationX].Owner.OwnerType == OwnerType.Player)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (owner.Summons.Count <= caster.Skills[SkillType.Magic].Level / 20)
                            {
                                string npcName;
                                int result = Dice.Roll(1, caster.Skills[SkillType.Magic].Level / 10);
                                if (result < caster.Skills[SkillType.Magic].Level / 20) result = caster.Skills[SkillType.Magic].Level / 20;

                                if (World.SummonConfiguration.ContainsKey(result))
                                {
                                    npcName = World.SummonConfiguration[result];
                                    CreateNpc(npcName, owner.CurrentMap, owner.X, owner.Y, owner);
                                }
                            }
                        }
                        break;
                    case MagicType.CreateItem:
                        switch (spell.Effect1)
                        {
                            case 1: // food
                                Item food;
                                if (Dice.Roll(1, 2) == 1)
                                    food = World.ItemConfiguration["Meat"].Copy();
                                else food = World.ItemConfiguration["Baguette"].Copy();
                                if (caster.CurrentMap[destinationY][destinationX].SetItem(food))
                                    SendItemEventToNearbyPlayers(caster, CommandMessageType.DropItem, destinationX, destinationY, food);
                                break;
                        }
                        break;
                    case MagicType.Invisibility:
                        switch (spell.Effect1)
                        {
                            case 1: // invisibility
                                if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                                    caster.CurrentMap[destinationY][destinationX].Owner.SetMagicEffect(spell);
                                break;
                            case 2: // detect invisibility
                                for (int x = destinationX - 8; x <= destinationX + 8; x++)
                                    for (int y = destinationY - 8; y <= destinationY + 8; y++) // get everything in range
                                        if (caster.CurrentMap[y][x].IsOccupied)
                                            caster.CurrentMap[y][x].Owner.RemoveMagicEffect(spell.Type);
                                break;
                        }
                        break;
                    case MagicType.HPUpSingle:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                            caster.CurrentMap[destinationY][destinationX].Owner.ReplenishHP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3));
                        break;
                    case MagicType.SPUpSingle:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                            caster.CurrentMap[destinationY][destinationX].Owner.ReplenishSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3));
                        break;
                    case MagicType.SPDownSingle:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                            caster.CurrentMap[destinationY][destinationX].Owner.DepleteSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3));
                        break;
                    case MagicType.SPUpArea:
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                    caster.CurrentMap[y][x].Owner.ReplenishSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3));
                        break;
                    case MagicType.SPDownArea:
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                    caster.CurrentMap[y][x].Owner.DepleteSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3));
                        break;
                    case MagicType.Paralyze:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (owner.CurrentLocation.IsSafe || caster.CurrentLocation.IsSafe) break; // cant para in safe or from safe
                            if (owner.Side == OwnerSide.Neutral) break; // cant para travellers
                            if (owner.Side == caster.Side) break; //  cant para own town

                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                owner.SetMagicEffect(spell);
                        }
                        break;
                    case MagicType.Poison:
                        switch (spell.Effect1)
                        {
                            case 0: // cure
                                if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                                    caster.CurrentMap[destinationY][destinationX].Owner.RemoveMagicEffect(spell.Type);
                                break;
                            case 1: // poison
                                if (caster.CurrentMap[destinationY][destinationX].IsOccupied &&
                                    !caster.CurrentMap[destinationY][destinationX].Owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM) &&
                                    !caster.CurrentMap[destinationY][destinationX].Owner.EvadePoison())
                                    caster.CurrentMap[destinationY][destinationX].Owner.SetMagicEffect(spell);
                                break;
                        }
                        break;
                    case MagicType.Inhibition:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            if (caster.CurrentMap[destinationY][destinationX].Owner.OwnerType == OwnerType.Npc) break; // cant use these on NPC

                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                owner.SetMagicEffect(spell);
                        }
                        break;
                    case MagicType.Confuse:
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    if (caster.CurrentMap[y][x].Owner.OwnerType == OwnerType.Npc) break; // cant use these on NPC

                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        owner.SetMagicEffect(spell);
                                }
                        break;
                    case MagicType.Protect:
                    case MagicType.Berserk:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            //if (character.CurrentMap[destinationY][destinationX].OwnerType == OwnerType.Npc) break; // cant zerk or prot NPCs
                            owner.SetMagicEffect(spell);
                        }
                        break;
                    case MagicType.DamageSingle: // damages target
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                        }
                        break;
                    case MagicType.DamageSingleSPDown: // damages target and reduce SP
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                owner.DepleteSP(Dice.Roll(spell.Effect4, spell.Effect5, spell.Effect6));
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }
                        break;
                    case MagicType.DamageArea: // damages target and all surrounding area (2 hits on target)
                    case MagicType.Tremor:
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                }
                        break;
                    case MagicType.DamageAreaNoSingle: // damages surrounding area of target, but not the target itself individually (1 hit on target)
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                }
                        break;
                    case MagicType.DamageAreaNoSingleSPDown: // as above but also reduces SP
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        owner.DepleteSP(Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3));
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.ArmourBreak: // damages target, area and reduces endurance of armour
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                owner.TakeArmourDamage(spell.Effect7 * 2);
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        owner.TakeArmourDamage(spell.Effect7 * 2);
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.Ice: // damages target, area and freezes target
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                if (!owner.EvadeIce()) owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Single);
                            }
                        }

                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        if (!owner.EvadeIce()) owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }
                        break;
                    case MagicType.CreateDynamic:
                        if (IsCrusade) // cannot cast in towns or farms during crusade
                            if (caster.CurrentMap.Name.Equals(Globals.AresdenTownName) ||
                                caster.CurrentMap.Name.Equals(Globals.ElvineTownName) ||
                                caster.CurrentMap.Name.Equals(Globals.ElvineFarmName) ||
                                caster.CurrentMap.Name.Equals(Globals.AresdenFarmName))
                                return;

                        switch ((DynamicObjectType)spell.Effect7)
                        {
                            case DynamicObjectType.PoisonCloudBegin:
                            case DynamicObjectType.Fire:
                            case DynamicObjectType.SpikeField:
                                switch ((DynamicObjectShape)spell.Effect8)
                                {
                                    case DynamicObjectShape.Wall:
                                        int rangeX, rangeY;
                                        switch (Utility.GetNextDirection(caster.X, caster.Y, destinationX, destinationY))
                                        {
                                            default:
                                            case MotionDirection.North: rangeX = 1; rangeY = 0; break;
                                            case MotionDirection.NorthEast: rangeX = 1; rangeY = 1; break;
                                            case MotionDirection.East: rangeX = 0; rangeY = 0; break;
                                            case MotionDirection.SouthEast: rangeX = -1; rangeY = 1; break;
                                            case MotionDirection.South: rangeX = 1; rangeY = 0; break;
                                            case MotionDirection.SouthWest: rangeX = -1; rangeY = -1; break;
                                            case MotionDirection.West: rangeX = 0; rangeY = -1; break;
                                            case MotionDirection.NorthWest: rangeX = 1; rangeY = -1; break;
                                        }

                                        caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX, destinationY, spell.LastTime);

                                        for (int i = 1; i <= spell.Effect9; i++)
                                        {
                                            caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX + (rangeX * i), destinationY + (rangeY * i), spell.LastTime);
                                            // TODO check criminal action
                                            caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX - (rangeX * i), destinationY - (rangeY * i), spell.LastTime);
                                            // TODO check criminal action again
                                        }

                                        break;
                                    case DynamicObjectShape.Field:
                                        for (int y = destinationY - spell.Effect9; y <= destinationY + spell.Effect9; y++)
                                            for (int x = destinationX - spell.Effect9; x <= destinationX + spell.Effect9; x++)
                                            {
                                                caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, x, y, spell.LastTime);
                                                // TODO check criminal action
                                            }
                                        break;
                                }
                                break;
                            case DynamicObjectType.IceStorm:
                                caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX, destinationY, spell.LastTime, caster.Skills[SkillType.Magic].Level);
                                break;
                            default: break;
                        }

                        break;
                    case MagicType.Resurrection:
                        // TODO - resurrection code
                        break;
                    case MagicType.DamageLine:
                    case MagicType.DamageLineSPDown:
                    case MagicType.IceLine:
                        // Hit 1: "line of site". each "point" in the LOS should have a cross-shaped hit, except the last point (next to caster) 
                        // note that like AREA spells, the center point is sent to Character.MagicAttack(x, y... to ensure players are flown away from the center point
                        List<int[]> hitLocations = Utility.GetMagicLineLocations(caster.X, caster.Y, destinationX, destinationY);
                        foreach (int[] hitLocation in hitLocations)
                        {
                            int x = hitLocation[0];
                            int y = hitLocation[1];

                            if (caster.CurrentMap[y][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y][x + 1].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x + 1].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y + 1][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y + 1][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y][x - 1].IsOccupied)
                            {
                                owner = caster.CurrentMap[y][x - 1].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                            if (caster.CurrentMap[y - 1][x].IsOccupied)
                            {
                                owner = caster.CurrentMap[y - 1][x].Owner;
                                if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                {
                                    if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                        owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                    else if (spell.Type == MagicType.DamageLineSPDown)
                                        owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                    caster.MagicAttack(x, y, owner, spell, MagicTarget.Area);
                                }
                            }
                        }

                        // Hit 2: Destination should hit like damagearea
                        for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                            for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                if (caster.CurrentMap[y][x].IsOccupied)
                                {
                                    owner = caster.CurrentMap[y][x].Owner;
                                    if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                                    {
                                        if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                            owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                        else if (spell.Type == MagicType.DamageLineSPDown)
                                            owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                        caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                                    }
                                }

                        // Hit 3: Destination should also be hit like damagesingle
                        if (caster.CurrentMap[destinationY][destinationX].IsOccupied)
                        {
                            owner = caster.CurrentMap[destinationY][destinationX].Owner;
                            if (!owner.EvadeMagic(caster.GetMagicHitChance(spell), spell.IgnorePFM))
                            {
                                if (spell.Type == MagicType.IceLine && !owner.EvadeIce())
                                    owner.SetMagicEffect(new Magic(MagicType.Ice, spell.Effect7));
                                else if (spell.Type == MagicType.DamageLineSPDown)
                                    owner.DepleteSP(Dice.Roll(spell.Effect7, spell.Effect8, spell.Effect9));
                                caster.MagicAttack(destinationX, destinationY, owner, spell, MagicTarget.Area);
                            }
                        }



                        // Note/TODO: for DamageLineSPDown (earth-shock-wave), damage is done twice (???)
                        break;
                    case MagicType.Polymorph:
                        // TODO - do we really want polymorph? =\
                        break;
                }

                if (!isScroll)
                {
                    int manaCost = Utility.GetManaCost(spell.ManaCost, caster.BonusManaSave, caster.IsSafeMode);
                    caster.DepleteMP(manaCost);
                }
            }
        }

        public void ExternalChat(string name, string message, ChatType mode)
        {
            if (mode == ChatType.GameMaster && message.StartsWith("/"))
            {
                Character c;

                string[] tokens = message.TrimStart('/').Split(' ');
                switch (tokens[0].ToLower())
                {
                    case "startapocalypse": StartWorldEvent(WorldEventType.Apocalypse); break;
                    case "startcrusade": StartWorldEvent(WorldEventType.Crusade); break;
                    case "startheldenian": StartWorldEvent(WorldEventType.Heldenian); break;
                    case "startctf": StartWorldEvent(WorldEventType.CaptureTheFlag); break;
                    case "endapocalypse":
                    case "endcrusade":
                    case "endheldenian":
                    case "endctf": EndWorldEvent(OwnerSide.Neutral); break; // manual end - draw
                    case "mute":
                        if (tokens.Length > 2)
                            if (FindPlayer(tokens[1].Trim(), out c))
                            {
                                c.MuteTime = new TimeSpan(0, Int32.Parse(tokens[2]), 0);
                                SendClientMessage(c, "You are muted for " + c.MuteTime.Hours + "h " + c.MuteTime.Minutes + "m and " + c.MuteTime.Seconds + "s");
                            }
                        break;
                    case "unmute":
                        if (tokens.Length > 1)
                            if (FindPlayer(tokens[1].Trim(), out c))
                                c.MuteTime = new TimeSpan(0, 0, 0);
                        break;
                    case "dc":
                    case "disconnect":
                        if (tokens.Length > 1)
                            if (FindPlayer(tokens[1].Trim(), out c))
                                if (tokens.Length > 2) c.Disconnect(tokens[2]);
                                else c.Disconnect("Not specified");
                        break;
                }
            }

            byte[] data = new byte[17 + message.Length];
            Buffer.BlockCopy((0).GetBytes(), 0, data, 0, 2);
            Buffer.BlockCopy((-1).GetBytes(), 0, data, 2, 2);
            Buffer.BlockCopy((-1).GetBytes(), 0, data, 4, 2);
            Buffer.BlockCopy(name.GetBytes(10), 0, data, 6, 10);
            data[16] = (byte)((int)mode);
            Buffer.BlockCopy(message.GetBytes(message.Length), 0, data, 17, message.Length);
            foreach (Map map in maps.Values)
                for (int p = 0; p < map.Players.Count; p++)
                    if (players.ContainsKey(map.Players[p]))
                    {
                        Character player = players[map.Players[p]];
                        Send(player, CommandType.Chat, CommandMessageType.Confirm, data);
                    }
        }

        public void StartWorldEvent(WorldEventType eventType)
        {
            if (currentEvent != null) return;

            switch (eventType)
            {
                case WorldEventType.Crusade: currentEvent = World.CrusadeConfiguration.Copy(); break;
                case WorldEventType.Heldenian: currentEvent = World.HeldenianConfiguration.Copy(); break;
                case WorldEventType.Apocalypse: currentEvent = World.ApocalypseConfiguration.Copy(); break;
                case WorldEventType.CaptureTheFlag: currentEvent = World.CaptureTheFlagConfiguration.Copy(); break;
                case WorldEventType.KingOfTheHill:
                default: return;
            }
            currentEvent.CurrentWorld = this;
            currentEvent.Start();

            LogMessage(string.Format("{0} started.", eventType.ToString()), LogType.Events);
        }

        public void EndWorldEvent(OwnerSide side)
        {
            if (currentEvent == null) return;
            currentEvent.End(side);
            
            // update results
            if (WorldEventResults.ContainsKey(currentEvent.Result.Type))
                WorldEventResults[currentEvent.Result.Type] = currentEvent.Result;
            else WorldEventResults.Add(currentEvent.Result.Type, currentEvent.Result);

            // update datebase
            if (WorldEventEnded != null) WorldEventEnded(currentEvent.Result);
            LogMessage(string.Format("{0} ended.", currentEvent.Type.ToString()), LogType.Events);

            currentEvent = null;
        }

        /// <summary>
        /// Handles Timer-controlled functions.
        /// </summary>
        public void TimerProcess()
        {
            TimeSpan ts;

            // handles player timer for hp/mp/sp regen, hunger, poison, magic effects etc
            try
            {
                ts = DateTime.Now - playerTimer;
                if (ts.Seconds >= 3)
                {
                    foreach (Map map in maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (players.ContainsKey(map.Players[p]))
                                players[map.Players[p]].TimerProcess();

                    playerTimer = DateTime.Now;
                }
            }
            catch { LogMessage("TimerProcess Players error", LogType.Error); }

            // handles npc timer for hp/mp/sp regen, poison, magic effects etc
            try
            {
                ts = DateTime.Now - npcTimer;
                if (ts.Seconds >= 3)
                {
                    foreach (Map map in maps.Values)
                        for (int n = 0; n < map.Npcs.Count; n++)
                            if (npcs.ContainsKey(map.Npcs[n]))
                                npcs[map.Npcs[n]].TimerProcess();

                    npcTimer = DateTime.Now;
                }
            }
            catch { LogMessage("TimerProcess NPCs error", LogType.Error); }

            // handles world events e.g crusade, day/night mode and map effects e.g abby map thunder
            try
            {
                ts = DateTime.Now - eventTimer;
                if (ts.Seconds >= 2)
                {
                    CheckTimeOfDay();
                    if (currentEvent != null) currentEvent.TimerProcess();
                    eventTimer = DateTime.Now;
                }
            }
            catch { LogMessage("TimerProcess Events error", LogType.Error); }

            // handles map events such as weather, item clearing, mineral spawning
            try
            {
                ts = DateTime.Now - mapTimer;
                if (ts.Seconds >= 20)
                {
                    foreach (Map map in maps.Values) map.TimerProcess();
                    mapTimer = DateTime.Now;
                }
            }
            catch { LogMessage("TimerProcess Maps error", LogType.Error); }

            // handles registered dynamic objects that have timer processes, such as dynamic spells
            try
            {
                ts = DateTime.Now - dynamicObjectTimer;
                if (ts.Seconds >= 3)
                {
                    foreach (Map map in maps.Values)
                        for (int d = 0; d < map.DynamicObjects.Count; d++)
                            if (dynamicObjects.ContainsKey(map.Name + map.DynamicObjects[d]))
                                dynamicObjects[map.Name + map.DynamicObjects[d]].TimerProcess();

                    dynamicObjectTimer = DateTime.Now;
                }
            }
            catch { LogMessage("TimerProcess DynamicEvents error", LogType.Error); }
        }

        public bool CreateNpc(string npcName, Map map, int x, int y) { return CreateNpc(npcName, map, x, y, -1, null); }
        public bool CreateNpc(string npcName, Location location, int spawnID) { return CreateNpc(npcName, maps[location.MapName], location.X, location.Y, spawnID, null); }
        public bool CreateNpc(string npcName, Map map, int x, int y, IOwner summoner) { return CreateNpc(npcName, map, x, y, -1, summoner); }
        public bool CreateNpc(string npcName, Map map, int x, int y, int spawnID, IOwner summoner)
        {
            if (World.NpcConfiguration.ContainsKey(npcName))
            {
                Npc npc = World.NpcConfiguration[npcName].Copy();
                npc.CurrentWorld = this;
                npc.SpawnID = spawnID;

                int npcID = InitNpc(npc, map, x, y, summoner);

                if (npcID != -1)
                {
                    if (summoner != null)
                    {
                        npc.Summoner = summoner.ID;
                        npc.MoveType = MovementType.Follow;
                        summoner.Summons.Add(npcID);
                    }
                    return true;
                }
                else return false;
            }
            else return false;
        }

        public bool CreateAdvancedNpc(Map map, int x, int y)
        {
            //TODO testing purposes only. need to remove
            //if (World.NpcConfiguration.ContainsKey(npcName))
            if (true)
            {
                //Npc npc = World.NpcConfiguration[npcName].Copy();
                Npc npc = new NpcHuman(AdvancedAIDifficulty.Normal, AdvancedAIBehaviour.Berserker, AdvancedAIClass.Warrior, AdvancedAIEquipment.Civilian);
                npc.CurrentWorld = this;
                //npc.SpawnID = spawnID;

                int npcID = InitNpc(npc, map, x, y, null);

                if (npcID != -1)
                {
                    //if (summoner != null)
                    //{
                        //npc.Summoner = summoner.ID;
                        //npc.Side = summoner.Side;
                        npc.Side = OwnerSide.Neutral;
                        npc.MoveType = MovementType.Follow;
                        //summoner.Summons.Add(npcID);
                    //}
                    return true;
                }
                else return false;
            }
            else return false;
        }

        /// <summary>
        /// Gets an available Npc index. Client limitations means that available numbers are limited.
        /// </summary>
        /// <returns>Available Npc index</returns>
        public int GetNpcIndex()
        {
            for (int index = 0; index < 20000; index++)
                if (npcIndexes[index] == false)
                {
                    npcIndexes[index] = true;
                    return index;
                }
            return -1;
        }

        /// <summary>
        /// Clears an index when an Npc is removed so that it is available for a new Npc.
        /// </summary>
        /// <param name="index">Index to be removed</param>
        public void ClearNpcIndex(int index)
        {
            npcIndexes[index] = false;
        }

        /// <summary>
        /// Initialized an Npc object on to a map and sets the summoner if they exist.
        /// </summary>
        /// <param name="npc"></param>
        /// <param name="map"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="summoner"></param>
        /// <returns>ID of the Npc just created</returns>
        public int InitNpc(Npc npc, Location location) { return InitNpc(npc, maps[location.MapName], location.X, location.Y, null); }
        public int InitNpc(Npc npc, Location location, IOwner summoner) { return InitNpc(npc, maps[location.MapName], location.X, location.Y, summoner); }
        public int InitNpc(Npc npc, Map map, int x, int y, IOwner summoner)
        {
            if (npc == null || map == null) return -1;
            try
            {
                npc.ID = GetNpcIndex(); // gets available index
                if (npc.ID == -1) return -1; // -1 means no index available, cannot Init Npc
                if (summoner != null) npc.Side = summoner.Side;
                npc.MessageLogged += new LogHandler(MessageLogged);
                npc.Init(map, x, y);
                if (npc.Side == OwnerSide.Wild) map.TotalMobs++;
                npc.StatusChanged += new OwnerHandler(OnStatusChanged);
                npc.MotionChanged += new MotionHandler(OnMotionChanged);
                npc.DamageTaken += new DamageHandler(OnDamageTaken);
                npc.Killed += new DeathHandler(OnKilled);
                npc.ItemDropped += new ItemHandler(OnItemDropped);
                lock (NpcLock) { npcs.Add(npc.ID, npc); }
                SendLogEventToNearbyPlayers(npc, CommandMessageType.Confirm);

                return npc.ID;
            }
            catch (Exception ex)
            {
                LogMessage("Error in InitNpc: " + ex.ToString(), LogType.Error);
                return -1;
            }
        }

        /// <summary>
        /// Removes an NPC from the game server.
        /// </summary>
        /// <param name="npcID">ID of the Npc to remove.</param>
        public void RemoveNPC(int npcID)
        {
            if (npcs.ContainsKey(npcID) && npcs[npcID] != null)
            {
                Npc npc = npcs[npcID];
                SendLogEventToNearbyPlayers(npcs[npcID], CommandMessageType.Reject); // tells nearby players to remove this object from the map
                npc.Remove();
                if (npc.Side == OwnerSide.Wild) npc.CurrentMap.TotalMobs--;
                lock (NpcLock)
                {
                    npcs.Remove(npcID);
                }
                ClearNpcIndex(npcID);
            }
        }

        /// <summary>
        /// Handles Npc movement, actions and AI.
        /// </summary>
        public void NpcProcess()
        {
            TimeSpan ts;

            // TODO - can any of these be in the Map or Npc classes? use events (replace CreateNpc) to notify nearby players
            if (!IsCrusade) // no spawns during crusade
            {
                ts = DateTime.Now - spawnTimer;
                if (ts.Seconds >= Globals.MobSpawnRate)
                {
                    foreach (Map map in maps.Values)
                        if (!((IsHeldenianBattlefield || IsHeldenianSeige) && map.IsHeldenianMap)) // no spawns in heldenian maps during heldenian
                        {
                            for (int spawnID = 0; spawnID < map.MobSpawns.Count; spawnID++)
                            {
                                MobSpawn spawn = map.MobSpawns[spawnID];
                                if (spawn.Count < spawn.Total)
                                {
                                    CreateNpc(spawn.NpcName, spawn.Zone.GetRandomLocation(), spawnID);
                                    spawn.Count++;
                                    continue; // only one npc should spawn in each spawn location per iteration
                                }
                            }
                        }
                    spawnTimer = DateTime.Now;
                }
            }

            ts = DateTime.Now - npcActionTimer;
            if (ts.Milliseconds >= 50)
            {
                foreach (Map map in maps.Values)
                    for (int n = 0; n < map.Npcs.Count; n++)
                        if (npcs.ContainsKey(map.Npcs[n]))
                        {
                            Npc npc = npcs[map.Npcs[n]];
                            if (npc.IsSummoned)
                            {
                                ts = DateTime.Now - npc.SummonTime;
                                if (ts.Seconds >= Globals.NpcSummonDuration) npc.Die(null, -1, false);
                            }

                            ts = DateTime.Now - npc.LastActionTime;
                            if (ts.TotalMilliseconds >= npc.ActionTime.TotalMilliseconds)
                            {
                                npc.LastActionTime = DateTime.Now;
                                switch (npc.CurrentAction)
                                {
                                    case MotionType.Idle:
                                        if (npc.Idle())
                                            switch (npc.NpcType)
                                            {
                                                case NpcType.CrusadeManaCollector:
                                                case NpcType.CrusadeDetector:
                                                    SendMotionEventToNearbyPlayers(npcs[npc.ID], CommandMessageType.ObjectAttack, npc.X + 1, npc.Y + 1, 1);
                                                    break;
                                            }
                                        break;
                                    case MotionType.Dead:
                                        if (npc.IsDead)
                                        {
                                            ts = DateTime.Now - npc.DeadTime;
                                            if (ts.Seconds >= npc.MaximumDeadTime.Seconds) RemoveNPC(npc.ID);
                                        }
                                        break;
                                    case MotionType.Move:
                                        if (npc.Move()) SendMotionEventToNearbyPlayers(npcs[npc.ID], CommandMessageType.ObjectMove);
                                        if (!npc.IsFriendly && !npc.IsPacifist && !(npc.Side == OwnerSide.Neutral)) npc.SearchForTarget();
                                        break;
                                    case MotionType.AttackStationary:
                                        if (!npc.IsFriendly && !npc.IsPacifist && !(npc.Side == OwnerSide.Neutral)) npc.SearchForTarget();
                                        break;
                                    case MotionType.Attack:
                                        int rangeX, rangeY, spell; MotionDirection direction;
                                        // TODO - use the interface instead of separate code for individual target types?
                                        switch (npc.TargetType)
                                        {
                                            case OwnerType.Player:
                                                if (!players.ContainsKey(npc.TargetID) || players[npc.TargetID] == null || players[npc.TargetID].IsInvisible || players[npc.TargetID].CurrentMap != npc.CurrentMap) { npc.ClearTarget(); break; }
                                                Character player = players[npc.TargetID];
                                                rangeX = Math.Abs(npc.X - player.X); rangeY = Math.Abs(npc.Y - player.Y);
                                                if (player.IsDead || npc.FailedActionCount > 20) { npc.ClearTarget(); break; }
                                                //LogMessage("Magic: " + npc.Magic + "   Spells: " + npc.Spells.Length + "   Mp: " + npc.MP, LogType.Game);
                                                if (npc.Magic > 0 && npc.Spells.Length > 0 &&
                                                    (rangeX >= 0 && rangeX <= 9) && (rangeY >= 0 && rangeY <= 7) &&
                                                    npc.CastMagic(player.X, player.Y, player, out spell))
                                                {
                                                    direction = Utility.GetNextDirection(npc.X, npc.Y, player.X, player.Y);
                                                    npc.Direction = direction;
                                                    SendMagicEventToNearbyPlayers(npcs[npc.ID], CommandMessageType.CastMagic, player.X, player.Y, spell+100);
                                                    SendMotionEventToNearbyPlayers(npcs[npc.ID], CommandMessageType.ObjectAttack, player.X, player.Y, 1);
                                                    if (!player.EvadeMagic(npc.GetMagicHitChance(World.MagicConfiguration[spell]), World.MagicConfiguration[spell].IgnorePFM))
                                                        CastMagic(npc, player.X, player.Y, World.MagicConfiguration[spell]);
                                                    npc.DepleteMP(World.MagicConfiguration[spell].ManaCost);
                                                    npc.FailedActionCount = 0;
                                                }
                                                else if ((rangeX >= 0 && rangeX <= npc.AttackRange) && (rangeY >= 0 && rangeY <= npc.AttackRange))
                                                {
                                                    direction = Utility.GetNextDirection(npc.X, npc.Y, player.X, player.Y);
                                                    SendMotionEventToNearbyPlayers(npcs[npc.ID], CommandMessageType.ObjectAttack, player.X, player.Y, 1);
                                                    npc.MeleeAttack(npc.X, npc.Y, direction, player.X, player.Y, player, 1, false);
                                                    npc.FailedActionCount = 0;
                                                }
                                                else
                                                {
                                                    if (npc.MoveType == MovementType.None) { npc.ClearTarget(); break; }
                                                    if (npc.Move()) SendMotionEventToNearbyPlayers(npcs[npc.ID], CommandMessageType.ObjectMove);
                                                    npc.FailedActionCount++; // failure is not an option!
                                                }
                                                //LogMessage("SpellId: " + spell, LogType.Game);
                                                break;
                                            case OwnerType.Npc:
                                                if (!npcs.ContainsKey(npc.TargetID) || npcs[npc.TargetID] == null || npcs[npc.TargetID].IsInvisible || npcs[npc.TargetID].CurrentMap != npc.CurrentMap) { npc.ClearTarget(); break; }
                                                Npc target = npcs[npc.TargetID];
                                                rangeX = Math.Abs(npc.X - target.X); rangeY = Math.Abs(npc.Y - target.Y);
                                                if (target.IsDead || npc.FailedActionCount > 20) { npc.ClearTarget(); break; }
                                                if (npc.Magic > 0 && npc.Spells.Length > 0 &&
                                                    (rangeX >= 0 && rangeX <= 9) && (rangeY >= 0 && rangeY <= 7) &&
                                                    npc.CastMagic(target.X, target.Y, target, out spell))
                                                {
                                                    direction = Utility.GetNextDirection(npc.X, npc.Y, target.X, target.Y);
                                                    npc.Direction = direction;
                                                    SendMagicEventToNearbyPlayers(npcs[npc.ID], CommandMessageType.CastMagic, target.X, target.Y, spell + 100);
                                                    SendMotionEventToNearbyPlayers(npcs[npc.ID], CommandMessageType.ObjectAttack, target.X, target.Y, 1);
                                                    if (!target.EvadeMagic(npc.GetMagicHitChance(World.MagicConfiguration[spell]), World.MagicConfiguration[spell].IgnorePFM))
                                                        CastMagic(npc, target.X, target.Y, World.MagicConfiguration[spell]);
                                                    npc.DepleteMP(World.MagicConfiguration[spell].ManaCost);
                                                    npc.FailedActionCount = 0;
                                                }
                                                else if ((rangeX >= 0 && rangeX <= npc.AttackRange) && (rangeY >= 0 && rangeY <= npc.AttackRange))
                                                {
                                                    direction = Utility.GetNextDirection(npc.X, npc.Y, target.X, target.Y);
                                                    SendMotionEventToNearbyPlayers(npcs[npc.ID], CommandMessageType.ObjectAttack, target.X, target.Y, 1);
                                                    npc.MeleeAttack(npc.X, npc.Y, direction, target.X, target.Y, target, 1, false);
                                                    npc.FailedActionCount = 0;
                                                }
                                                else
                                                {
                                                    if (npc.MoveType == MovementType.None) { npc.ClearTarget(); break; }
                                                    if (npc.Move()) SendMotionEventToNearbyPlayers(npcs[npc.ID], CommandMessageType.ObjectMove);
                                                    npc.FailedActionCount++; // failure is not an option!
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                        }
                npcActionTimer = DateTime.Now;
            }
        }

        /// <summary>
        /// Checks the time of day and, and sets dark or lightness of a map. Players are notified if a change happens. Buildings are ignored.
        /// </summary>
        private void CheckTimeOfDay()
        {
            TimeOfDay tod;
            if ((DateTime.Now.Hour >= 20) || (DateTime.Now.Hour < 6))
                if (DateTime.Now.Month == 12)
                    tod = TimeOfDay.NightChristmas;
                else tod = TimeOfDay.Night;
            else tod = TimeOfDay.Day;

            if (tod != timeOfDay)
            {
                foreach (Map map in maps.Values)
                {
                    if (!map.IsBuilding) map.TimeOfDay = tod;
                    for (int p = 0; p < map.Players.Count; p++)
                        if (players.ContainsKey(map.Players[p]))
                        {
                            Character player = players[map.Players[p]];
                            if (!player.CurrentMap.IsBuilding) player.Notify(CommandMessageType.NotifyTimeChange, (int)tod);
                        }
                }
                timeOfDay = tod;
            }
        }

        public void SendClientMessage(Character owner, string message)
        {
            if (message.Length > 70) message.Substring(0, 69);

            byte[] data = new byte[17 + message.Length];
            Buffer.BlockCopy(((short)0).GetBytes(), 0, data, 0, 2);
            Buffer.BlockCopy(((short)0).GetBytes(), 0, data, 2, 2);
            Buffer.BlockCopy(((short)0).GetBytes(), 0, data, 4, 2);
            Buffer.BlockCopy(("Server").GetBytes(10), 0, data, 6, 10);
            data[16] = 10;
            Buffer.BlockCopy(message.GetBytes(message.Length), 0, data, 17, message.Length);

            Send(owner, CommandType.Chat, CommandMessageType.Confirm, data);
        }

        public void SendItemEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, Item item) { SendCommonEventToNearbyPlayers(owner, messageType, item.Sprite, item.SpriteFrame, item.Colour, 0); }
        public void SendItemEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, int destinationX, int destinationY, Item item) { SendCommonEventToNearbyPlayers(owner, messageType, item.Sprite, item.SpriteFrame, item.Colour, destinationX, destinationY); }
        public void SendMagicEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, int destinationX, int destinationY, int magicType) { SendCommonEventToNearbyPlayers(owner, messageType, destinationX, destinationY, magicType, 0); }
        public void SendCommonEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, int value1, int value2, int value3, int value4)
        {
            byte[] data;

            data = new byte[12];
            Buffer.BlockCopy(BitConverter.GetBytes((short)owner.X), 0, data, 0, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)owner.Y), 0, data, 2, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, data, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, data, 6, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, data, 8, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)value4), 0, data, 10, 2);

            for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                if (players.ContainsKey(owner.CurrentMap.Players[p]))
                {
                    Character player = players[owner.CurrentMap.Players[p]];
                    if ((owner.OwnerType == OwnerType.Player &&
                        player.ClientID == owner.ID) ||
                             player.IsWithinRange(owner, 10, 8))
                        Send(player, CommandType.CommonEvent, messageType, data);
                }
        }

        public void SendCommonEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, int value1, int value2, int value3, int destinationX, int destinationY)
        {
            byte[] data;

            if (owner.OwnerType == OwnerType.Player)
            {
                Character character = (Character)owner;
                data = new byte[12];
                Buffer.BlockCopy(BitConverter.GetBytes((short)destinationX), 0, data, 0, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)destinationY), 0, data, 2, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, data, 4, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, data, 6, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, data, 8, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 10, 2);

                for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                    if (players.ContainsKey(owner.CurrentMap.Players[p]))
                    {
                        Character player = players[owner.CurrentMap.Players[p]];
                        if (player.ClientID == character.ClientID ||
                                 player.IsWithinRange(character, 10, 8))
                            Send(player, CommandType.CommonEvent, messageType, data);
                    }
            }
            else if (owner.OwnerType == OwnerType.Npc)
            {
                Npc npc = (Npc)owner;
                data = new byte[12];
                Buffer.BlockCopy(BitConverter.GetBytes((short)destinationX), 0, data, 0, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)destinationY), 0, data, 2, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, data, 4, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, data, 6, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, data, 8, 2);
                Buffer.BlockCopy(BitConverter.GetBytes((short)0), 0, data, 10, 2);

                for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                    if (players.ContainsKey(owner.CurrentMap.Players[p]))
                    {
                        Character player = players[owner.CurrentMap.Players[p]];
                        if (player.IsWithinRange(npc, 10, 8))
                            Send(player, CommandType.CommonEvent, messageType, data);
                    }
            }
        }

        public void SendLogEventToNearbyPlayers(IOwner owner, CommandMessageType messageType) { SendEventToNearbyPlayers(owner, CommandType.LogEvent, messageType, 0, 0, 0); }
        public void SendMotionEventToNearbyPlayers(IOwner owner, CommandMessageType messageType) { SendEventToNearbyPlayers(owner, CommandType.Motion, messageType, 0, 0, 0); }
        public void SendMotionEventToNearbyPlayers(IOwner owner, CommandMessageType messageType, int value1, int value2, int value3) { SendEventToNearbyPlayers(owner, CommandType.Motion, messageType, value1, value2, value3); }
        public void SendEventToNearbyPlayers(IOwner owner, CommandType commandType, CommandMessageType messageType, int value1, int value2, int value3)
        {
            byte[] data;
            bool notifyOwner = false; // sends data to the owner of the motion event when true
            int rangeModifier = 0; // adds additional range to "nearby" 
            int tempStatus; // holds status and changes depending on the nearby player. allies/foes receive different data accordingly

            switch (messageType)
            {
                case CommandMessageType.Confirm:
                case CommandMessageType.Reject:
                case CommandMessageType.ObjectMove:
                case CommandMessageType.ObjectRun:
                case CommandMessageType.ObjectAttackDash:
                case CommandMessageType.ObjectTakeDamageAndFly:
                case CommandMessageType.ObjectDying:
                    rangeModifier = 1;
                    break;
            }

            if (owner.OwnerType == OwnerType.Player)
            {
                switch (messageType)
                {
                    case CommandMessageType.ObjectNullAction:
                    case CommandMessageType.ObjectTakeDamage:
                    case CommandMessageType.ObjectDying:
                        notifyOwner = true;
                        break;
                }

                Character character = (Character)owner;
                switch (messageType)
                {
                    case CommandMessageType.ObjectNullAction:
                    case CommandMessageType.Confirm:
                    case CommandMessageType.Reject:
                        data = new byte[36];
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.ClientID), 0, data, 0, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.X), 0, data, 2, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Y), 0, data, 4, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Type), 0, data, 6, 2);
                        data[8] = (byte)((int)character.Direction);
                        Buffer.BlockCopy(character.Name.GetBytes(10), 0, data, 9, 10);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance1), 0, data, 19, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance2), 0, data, 21, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance3), 0, data, 23, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance4), 0, data, 25, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes(character.AppearanceColour), 0, data, 27, 4);
                        unchecked { tempStatus = character.Status & (int)0x0F0FFFF7F; } // temp status, see notes below when data is copied to byte array
                        if (messageType == CommandMessageType.ObjectNullAction)
                        { if (character.IsDead) data[35] = 1; else data[35] = 0; }
                        else data[35] = 0;

                        for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                            if (players.ContainsKey(owner.CurrentMap.Players[p]))
                            {
                                Character player = players[owner.CurrentMap.Players[p]];
                                if ((notifyOwner && player.ClientID == character.ClientID) ||
                                         (player.IsWithinRange(character, 10, 8, rangeModifier) && player.ClientID != character.ClientID))
                                {
                                    // make sure allies/foes get correct data accordingly
                                    int temp;
                                    if (player.Town != character.Town)
                                    {
                                        if (character.IsAdmin) temp = character.Status;
                                        else if (player.ClientID != character.ClientID) temp = tempStatus;
                                        else temp = character.Status;
                                    }
                                    else temp = character.Status;

                                    int status = ((0x0FFFFFFF & temp) | (character.GetPlayerRelationship(player) << 28));
                                    Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data, 31, 4);

                                    Send(player, commandType, messageType, data);
                                }
                            }
                        break;
                    case CommandMessageType.ObjectMagic:
                    case CommandMessageType.ObjectTakeDamage:
                    case CommandMessageType.ObjectTakeDamageAndFly:
                        data = new byte[10];
                        Buffer.BlockCopy(BitConverter.GetBytes((ushort)character.ClientID + 30000), 0, data, 0, 2);
                        data[2] = (byte)((int)character.Direction);
                        data[3] = (byte)value1;
                        data[4] = (byte)0; // not sure what this is for yet
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.X), 0, data, 5, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Y), 0, data, 7, 2);

                        for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                            if (players.ContainsKey(owner.CurrentMap.Players[p]))
                            {
                                Character player = players[owner.CurrentMap.Players[p]];
                                if ((notifyOwner && player.ClientID == character.ClientID) ||
                                         (player.IsWithinRange(character, 10, 8, rangeModifier) && player.ClientID != character.ClientID))
                                    Send(player, commandType, messageType, data);
                            }
                        break;
                    case CommandMessageType.ObjectDying:
                        data = new byte[10];
                        Buffer.BlockCopy(BitConverter.GetBytes((ushort)(character.ClientID + 30000)), 0, data, 0, 2);
                        data[2] = (byte)((int)character.Direction);
                        data[3] = (byte)value1; // damage
                        data[4] = (byte)value2; // attacker weapon
                        Buffer.BlockCopy(BitConverter.GetBytes(character.X), 0, data, 5, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes(character.Y), 0, data, 7, 2);

                        for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                            if (players.ContainsKey(owner.CurrentMap.Players[p]))
                            {
                                Character player = players[owner.CurrentMap.Players[p]];
                                if ((notifyOwner && player.ClientID == character.ClientID) ||
                                         (player.IsWithinRange(character, 10, 8, rangeModifier) && player.ClientID != character.ClientID))
                                    Send(player, commandType, messageType, data);
                            }
                        break;
                    case CommandMessageType.ObjectAttack:
                    case CommandMessageType.ObjectAttackDash:
                        data = new byte[8];
                        Buffer.BlockCopy(BitConverter.GetBytes((ushort)character.ClientID + 30000), 0, data, 0, 2);
                        data[2] = (byte)((int)character.Direction);
                        data[3] = (byte)(value1 - character.X);
                        data[4] = (byte)(value2 - character.Y);
                        Buffer.BlockCopy(BitConverter.GetBytes(value3), 0, data, 5, 2);

                        for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                            if (players.ContainsKey(owner.CurrentMap.Players[p]))
                            {
                                Character player = players[owner.CurrentMap.Players[p]];
                                if ((notifyOwner && player.ClientID == character.ClientID) ||
                                         (player.IsWithinRange(character, 10, 8, rangeModifier) && player.ClientID != character.ClientID))
                                    Send(player, commandType, messageType, data);
                            }
                        break;
                    default:
                        byte[] data1 = new byte[9];
                        Buffer.BlockCopy(BitConverter.GetBytes((ushort)character.ClientID + 30000), 0, data1, 0, 2);
                        data1[2] = (byte)((int)character.Direction);
                        data1[3] = (byte)value1;
                        data1[4] = (byte)0; // not sure what this is for yet
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.X), 0, data1, 5, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Y), 0, data1, 7, 2);

                        byte[] data2 = new byte[36];
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.ClientID), 0, data2, 0, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.X), 0, data2, 2, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Y), 0, data2, 4, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Type), 0, data2, 6, 2);
                        data2[8] = (byte)((int)character.Direction);
                        Buffer.BlockCopy(character.Name.GetBytes(10), 0, data2, 9, 10);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance1), 0, data2, 19, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance2), 0, data2, 21, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance3), 0, data2, 23, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)character.Appearance4), 0, data2, 25, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes(character.AppearanceColour), 0, data2, 27, 4);
                        unchecked { tempStatus = character.Status & (int)0x0F0FFFF7F; } // temp status, see notes below when data is copied to byte array
                        if (messageType == CommandMessageType.ObjectNullAction)
                        { if (character.IsDead) data2[35] = 1; else data2[35] = 0; }
                        else data2[35] = 0;

                        for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                            if (players.ContainsKey(owner.CurrentMap.Players[p]))
                            {
                                Character player = players[owner.CurrentMap.Players[p]];
                                if ((player.IsWithinRange(character, 10, 8, rangeModifier) && player.ClientID != character.ClientID) ||
                                     (notifyOwner && player.ClientID == character.ClientID))
                                    if (player.IsWithinRange(character, 9, 7, rangeModifier))
                                        Send(player, commandType, messageType, data1);
                                    else
                                    {
                                        int temp;
                                        if (character.Town != player.Town)
                                        {
                                            if (character.IsAdmin) temp = character.Status;
                                            else if (player.ClientID != character.ClientID) temp = tempStatus;
                                            else temp = character.Status;
                                        }
                                        else temp = character.Status;

                                        int status = ((0x0FFFFFFF & temp) | (character.GetPlayerRelationship(player) << 28));
                                        Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data2, 31, 4);

                                        Send(player, commandType, messageType, data2);
                                    }
                            }
                        break;
                }
            }
            else if (owner.OwnerType == OwnerType.Npc)
            {
                Npc npc = (Npc)owner;
                switch (messageType)
                {
                    case CommandMessageType.ObjectNullAction:
                    case CommandMessageType.Confirm:
                    case CommandMessageType.Reject:
                        data = new byte[21];
                        Buffer.BlockCopy(BitConverter.GetBytes((ushort)(npc.ID + 10000)), 0, data, 0, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)npc.X), 0, data, 2, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)npc.Y), 0, data, 4, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)npc.Type), 0, data, 6, 2);
                        data[8] = (byte)((int)npc.Direction);
                        Buffer.BlockCopy(npc.Name.GetBytes(5), 0, data, 9, 5);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)npc.Appearance2), 0, data, 14, 2);

                        if (messageType == CommandMessageType.ObjectNullAction)
                        { if (npc.IsDead) data[20] = 1; else data[20] = 0; }
                        else data[20] = 0;

                        for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                            if (players.ContainsKey(owner.CurrentMap.Players[p]))
                            {
                                Character player = players[owner.CurrentMap.Players[p]];
                                if (player.IsWithinRange(npc, 10, 8, rangeModifier))
                                {
                                    int status = ((0x0FFFFFFF & npc.Status) | (player.GetNpcRelationship(npc) << 28));
                                    Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data, 16, 4);

                                    Send(player, commandType, messageType, data);
                                }
                            }
                        break;
                    case CommandMessageType.ObjectTakeDamage:
                    case CommandMessageType.ObjectTakeDamageAndFly:
                        data = new byte[10];
                        Buffer.BlockCopy(BitConverter.GetBytes((ushort)(npc.ID + 40000)), 0, data, 0, 2);
                        data[2] = (byte)((int)npc.Direction);
                        data[3] = (byte)value1;
                        data[4] = (byte)0; // not sure what this is for yet
                        Buffer.BlockCopy(BitConverter.GetBytes(npc.X), 0, data, 5, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes(npc.Y), 0, data, 7, 2);

                        for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                            if (players.ContainsKey(owner.CurrentMap.Players[p]))
                            {
                                Character player = players[owner.CurrentMap.Players[p]];
                                if (player.IsWithinRange(npc, 10, 8, rangeModifier))
                                    Send(player, commandType, messageType, data);
                            }
                        break;
                    case CommandMessageType.ObjectDying:
                        data = new byte[10];
                        Buffer.BlockCopy(BitConverter.GetBytes((ushort)(npc.ID + 40000)), 0, data, 0, 2);
                        data[2] = (byte)((int)npc.Direction);
                        data[3] = (byte)value1; // damage
                        data[4] = (byte)value2; // attacker weapon
                        Buffer.BlockCopy(BitConverter.GetBytes(npc.X), 0, data, 5, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes(npc.Y), 0, data, 7, 2);

                        for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                            if (players.ContainsKey(owner.CurrentMap.Players[p]))
                            {
                                Character player = players[owner.CurrentMap.Players[p]];
                                if (player.IsWithinRange(npc, 10, 8, rangeModifier))
                                    Send(player, commandType, messageType, data);
                            }
                        break;
                    case CommandMessageType.ObjectAttack:
                    case CommandMessageType.ObjectAttackDash:
                        data = new byte[8];
                        Buffer.BlockCopy(BitConverter.GetBytes((ushort)(npc.ID + 40000)), 0, data, 0, 2);
                        data[2] = (byte)((int)npc.Direction);
                        data[3] = (byte)(value1 - npc.X);
                        data[4] = (byte)(value2 - npc.Y);
                        Buffer.BlockCopy(BitConverter.GetBytes(value3), 0, data, 5, 2);

                        for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                            if (players.ContainsKey(owner.CurrentMap.Players[p]))
                            {
                                Character player = players[owner.CurrentMap.Players[p]];
                                if (player.IsWithinRange(npc, 10, 8, rangeModifier))
                                    Send(player, commandType, messageType, data);
                            }                        
                        break;
                    default: // returns 2 differnt data sets depending on range... why? messy again
                        byte[] data1 = new byte[10];
                        Buffer.BlockCopy(BitConverter.GetBytes((ushort)(npc.ID + 40000)), 0, data1, 0, 2);
                        data1[2] = (byte)((int)npc.Direction);
                        data1[3] = (byte)value1;
                        data1[4] = (byte)value2; // not sure what this is for yet
                        Buffer.BlockCopy(BitConverter.GetBytes((short)npc.X), 0, data1, 5, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)npc.Y), 0, data1, 7, 2);

                        byte[] data2 = new byte[21];
                        Buffer.BlockCopy(BitConverter.GetBytes((ushort)(npc.ID + 10000)), 0, data2, 0, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)npc.X), 0, data2, 2, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)npc.Y), 0, data2, 4, 2);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)npc.Type), 0, data2, 6, 2);
                        data2[8] = (byte)((int)npc.Direction);
                        Buffer.BlockCopy(npc.Name.GetBytes(5), 0, data2, 9, 5);
                        Buffer.BlockCopy(BitConverter.GetBytes((short)npc.Appearance2), 0, data2, 14, 2);

                        if (messageType == CommandMessageType.ObjectNullAction)
                        { if (npc.IsDead) data2[20] = 1; else data2[20] = 0; }
                        else data2[20] = 0;

                        for (int p = 0; p < owner.CurrentMap.Players.Count; p++)
                            if (players.ContainsKey(owner.CurrentMap.Players[p]))
                            {
                                Character player = players[owner.CurrentMap.Players[p]];
                                if (player.IsWithinRange(npc, 10, 8, rangeModifier))
                                    if (player.IsWithinRange(npc, 9, 7, rangeModifier))
                                        Send(player, commandType, messageType, data1);
                                    else
                                    {
                                        int status = ((0x0FFFFFFF & npc.Status) | (player.GetNpcRelationship(npc) << 28));
                                        Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data2, 16, 4);

                                        Send(player, commandType, messageType, data2);
                                    }
                            }
                        break;
                }
            }
        }

        public void SendEventToNearbyPlayers(CommandType commandType, CommandMessageType messageType, Map map, int x, int y, int value1, int value2, int value3, int value4)
        {
            byte[] data;

            data = new byte[15];
            Buffer.BlockCopy(BitConverter.GetBytes(x), 0, data, 0, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(y), 0, data, 2, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(value1), 0, data, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, data, 6, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(value3), 0, data, 8, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(value4), 0, data, 10, 2);

            for (int p = 0; p < map.Players.Count; p++)
                if (players.ContainsKey(map.Players[p]))
                {
                    Character player = players[map.Players[p]];
                    if ((player.CurrentMap == map) &&
                             (player.X >= x - 10) &&
                             (player.X <= x + 10) &&
                             (player.Y >= y - 8) &&
                             (player.Y <= y + 8))
                        Send(player, commandType, messageType, data);
                }
        }

        public void Send(Character character, CommandType type, CommandMessageType messageType) { Send(character.ClientID, type, messageType, null); }
        public void Send(Character character, CommandType type, CommandMessageType messageType, byte[] sendData) { Send(character.ClientID, type, messageType, sendData); }
        public void Send(int clientID, CommandType type, CommandMessageType messageType) { Send(clientID, type, messageType, null); }
        public void Send(int clientID, CommandType commandType, CommandMessageType messageType, byte[] sendData)
        {
            byte[] data;

            switch (commandType)
            {
                case CommandType.ResponseInitPlayer:
                case CommandType.ResponseInitData:
                case CommandType.ResponseInitDataPlayerStats:
                case CommandType.ResponseInitDataPlayerItems:
                case CommandType.ResponseMotion:
                case CommandType.ResponseWarehouseItem:
                case CommandType.ResponseNews:
                case CommandType.ResponseCreateGuild:
                case CommandType.ResponseDisbandGuild:
                case CommandType.CommonEvent:
                case CommandType.Motion:
                case CommandType.LogEvent:
                case CommandType.DynamicObjectEvent:
                    if (sendData != null) // has payload?
                    {
                        data = new byte[sendData.Length + 15];
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(commandType), 0, data, 0, 4);
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(messageType), 0, data, 4, 2);
                        Buffer.BlockCopy(sendData, 0, data, 6, sendData.Length);
                        SendData(clientID, data);
                    }
                    else // no payload, usually a "Confirm" message
                    {
                        data = new byte[15];
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(commandType), 0, data, 0, 4);
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(messageType), 0, data, 4, 2);
                        SendData(clientID, data);
                    }
                    break;
                case CommandType.Chat:
                case CommandType.ResponseCitizenship:
                    data = new byte[sendData.Length + 15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(commandType), 0, data, 0, 4);
                    Buffer.BlockCopy(sendData, 0, data, 4, sendData.Length);
                    SendData(clientID, data);
                    break;
            }
        }

        private void OnMapWeatherChanged(Map map)
        {
            for (int p = 0; p < map.Players.Count; p++)
                if (players.ContainsKey(map.Players[p]))
                    players[map.Players[p]].Notify(CommandMessageType.NotifyWeatherChange, (int)map.Weather);
        }
        private void OnMapDynamicObjectCreated(IDynamicObject dynamicObject)
        {
            switch (dynamicObject.Type)
            {
                case DynamicObjectType.Fish:
                case DynamicObjectType.FishObject:
                case DynamicObjectType.Rock:
                case DynamicObjectType.Gem: // do not register any objects that dont implement the timer
                    break;
                default: dynamicObjects.Add(dynamicObject.CurrentMap.Name + dynamicObject.ID, dynamicObject); break;
            }
            
            SendEventToNearbyPlayers(CommandType.DynamicObjectEvent, CommandMessageType.Confirm, dynamicObject.CurrentMap, dynamicObject.X, dynamicObject.Y, (int)dynamicObject.Type, dynamicObject.ID, 0, 0);
        }

        private void OnMapDynamicObjectRemoved(IDynamicObject dynamicObject)
        {
            SendEventToNearbyPlayers(CommandType.DynamicObjectEvent, CommandMessageType.Reject, dynamicObject.CurrentMap, dynamicObject.X, dynamicObject.Y, (int)dynamicObject.Type, dynamicObject.ID, 0, 0);
        }

        private void OnDamageTaken(IOwner owner, DamageType type, int damage, MotionDirection flyDirection)
        {
            if (owner.OwnerType == OwnerType.Player) 
            {
                Character player = (Character)owner;
                switch (type)// players can be flown. different fly thresholds depending on DamageType
                {
                    case DamageType.Magic:
                        if (player.CurrentMap.IsFightZone && damage >= Globals.MagicFlyDamageFightzone)
                            player.Notify(CommandMessageType.NotifyFly, (int)flyDirection, damage);
                        else if (damage >= Globals.MagicFlyDamage)
                            player.Notify(CommandMessageType.NotifyFly, (int)flyDirection, damage);
                        else SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectTakeDamage, damage, 0, 0);
                        break;
                    case DamageType.Melee:
                    case DamageType.Ranged:
                        if (player.CurrentMap.IsFightZone && damage >= Globals.MeleeFlyDamageFightzone)
                            player.Notify(CommandMessageType.NotifyFly, (int)flyDirection, damage);
                        else if (damage >= Globals.MeleeFlyDamage)
                            player.Notify(CommandMessageType.NotifyFly, (int)flyDirection, damage);
                        else SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectTakeDamage, damage, 0, 0);
                        break;
                    case DamageType.Environment: break; // spike/fire/pc doesn't stun
                }
            }
            else SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectTakeDamage, damage, 0, 0);
        }

        private void OnKilled(IOwner owner, IOwner killer, int damage)
        {
            int attackerWeapon = (owner.OwnerType == OwnerType.Player) ? ((((Character)owner).Appearance2 & 0x0FF0) >> 4) : 1;
            SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectDying, damage, attackerWeapon, 0);

            // player kill reward
            if (owner.OwnerType == OwnerType.Player &&
                killer != null && killer.OwnerType == OwnerType.Player)
                ((Character)killer).GetKillReward((Character)owner);
        }

        private void OnItemDropped(IOwner owner, Item item)
        {
            SendItemEventToNearbyPlayers(owner, CommandMessageType.DropItem, item);
        }

        private void OnItemPickedUp(IOwner owner, Item item)
        {
            SendItemEventToNearbyPlayers(owner, CommandMessageType.SetItem, item);
        }

        private void OnMotionChanged(IOwner owner, CommandMessageType motionType, int destinationX, int destinationY)
        {
            // TODO instead of doing this in PlayerProcess() - make use of events
        }

        private void OnStatusChanged(IOwner owner)
        {
            SendMotionEventToNearbyPlayers(owner, CommandMessageType.ObjectNullAction);
        }

        /// <summary>
        /// Adds a map to the list of maps for this game server.
        /// </summary>
        /// <param name="name">Internal name of the map. Sent between client and server.</param>
        /// <param name="friendlyName">Friendly name of the map. For readability.</param>
        /// <param name="amdFileName">File name of the map including directory. e.g Maps/map.amd</param>
        /// <param name="isBuilding">Specifies whether this is a building for day/night cycles and weather to ignore.</param>
        /// <param name="isFightZone">Specifies whether this is a fightzone.</param>
        /// <param name="mapReader">An open xml reader with the config for the maps.</param>
        public void AddMap(string name, string friendlyName, string amdFileName, bool isBuilding, bool isFightZone, BuildingType type, bool isSafeMap, bool lootEnabled, bool isHuntZone, XmlReader mapReader)
        {
            Map map = new Map(amdFileName, name, friendlyName);
            map.Parse(mapReader);
            map.IsBuilding = (type != BuildingType.None) ? true : isBuilding; // if building type is specified, it is a building. otherwise, check Building="True" from config
            map.IsSafeMap = isSafeMap;
            map.IsFightZone = isFightZone;
            map.LootEnabled = lootEnabled;
            map.IsHuntZone = isHuntZone;
            map.BuildingType = type;
            map.WeatherChanged += new MapHandler(OnMapWeatherChanged);
            map.DynamicObjectCreated += new DynamicObjectHandler(OnMapDynamicObjectCreated);
            map.DynamicObjectRemoved += new DynamicObjectHandler(OnMapDynamicObjectRemoved);
            map.MessageLogged += new LogHandler(LogMessage);
            maps.Add(name, map);
        }

        public void LogMessage(string message, LogType type)
        {
            if (MessageLogged != null) MessageLogged(message, type);
        }

        /// <summary>
        /// Loads map data from the amd files specified in config.xml. Error messages returned based on Map.Load result.
        /// </summary>
        /// <returns>True indicates all maps loaded. False indicates at least 1 failure.</returns>
        public bool LoadMaps()
        {
            int failues = 0;
            foreach (Map map in maps.Values)
                if (!map.IsLoaded)
                    switch (map.Load())
                    {
                        case MapLoadResult.Success:
                            for (int spawnID = 0; spawnID < map.NpcSpawns.Count; spawnID++)
                            {
                                NpcSpawn spawn = map.NpcSpawns[spawnID];
                                Npc npc = World.NpcConfiguration[spawn.Name].Copy();
                                npc.MoveType = spawn.MoveType;
                                npc.IsFriendly = true; // the differnce between an Npc and a Mob
                                npc.SpawnID = spawnID;
                                if (spawn.MoveType == MovementType.RandomArea)
                                     InitNpc(npc, spawn.Zone.GetRandomLocation(), null);
                                else InitNpc(npc, spawn.Location, null);
                            }

                            MessageLogged(map.FriendlyName + " map loaded [" + map.Name + "]", LogType.Game);
                            break;
                        case MapLoadResult.MapNotFound:
                            MessageLogged(map.FriendlyName + " not loaded... unable to find " + map.FileName, LogType.Game);
                            failues++;
                            break;
                        case MapLoadResult.MapNotAccessible:
                            MessageLogged(map.FriendlyName + " not loaded... " + map.FileName + " access denied. Check file permissions", LogType.Game);
                            failues++;
                            break;
                        case MapLoadResult.Error:
                            MessageLogged(map.FriendlyName + " not loaded... unknown error.", LogType.Game);
                            failues++;
                            break;
                    }

            CheckTimeOfDay();

            if (failues == 0) return true;
            else return false;
        }

        public DataTable FormatPlayerList()
        {
            DataTable playerData = new DataTable();
            playerData.Columns.Add("Name");
            playerData.Columns.Add("Town");
            playerData.Columns.Add("Level");
            playerData.Columns.Add("Guild");
            playerData.Columns.Add("Map");

            try
            {
                foreach (Map map in maps.Values)
                    for (int p = 0; p < map.Players.Count; p++)
                        if (players.ContainsKey(map.Players[p]))
                            playerData.Rows.Add(new object[] 
                                            { players[map.Players[p]].Name,
                                              players[map.Players[p]].Town.ToString(),
                                              players[map.Players[p]].Level,
                                              players[map.Players[p]].Guild.Name,
                                              map.FriendlyName });
            }
            catch (Exception ex)
            {
                LogMessage("Error formating list for GUI controls: " + ex.ToString(), LogType.Error);
            }

            return playerData;
        }

        public Magic GetSpellByName(string name)
        {
            Magic spell = null;

            for (int i = 0; i < World.MagicConfiguration.Count; i++)
                if (World.MagicConfiguration.ContainsKey(i))
                {
                    //LogMessage("Searching: " + name + "   against:  " + World.MagicConfiguration[i].Name, LogType.Game);
                    if (World.MagicConfiguration[i].Name.Trim().Equals(name.Trim()))
                    {
                        spell = World.MagicConfiguration[i];
                        break;
                    }
            }

            return spell;
        }

        public int NpcCount
        {
            get
            {
                int count = 0;
                foreach (Map map in maps.Values)
                    count += map.Npcs.Count;
                return count;
            }
        }

        public Dictionary<int, Character> Players { get { return players; } set { players = value; } }
        public Dictionary<int, Npc> Npcs { get { return npcs; } set { npcs = value; } }
        public Dictionary<string, IDynamicObject> DynamicObjects { get { return dynamicObjects; } set { dynamicObjects = value; } }
        public Dictionary<string, Map> Maps { get { return maps; } set { maps = value; } }
        public Dictionary<string, Guild> Guilds { get { return guilds; } set { guilds = value; } }
        public List<Party> Parties { get { return parties; } set { parties = value; } }
        public IWorldEvent CurrentEvent { get { return currentEvent; } }
        public bool IsApocalypse { get { return (currentEvent != null && currentEvent.Type == WorldEventType.Apocalypse); } }
        public Apocalypse Apocalypse { get { return ((Apocalypse)currentEvent); } }
        public bool IsCrusade { get { return (currentEvent != null && currentEvent.Type == WorldEventType.Crusade); } }
        public Crusade Crusade { get { return ((Crusade)currentEvent); } }
        public bool IsHeldenianBattlefield { get { return (currentEvent != null && currentEvent.Type == WorldEventType.Heldenian && ((Heldenian)currentEvent).Mode == HeldenianType.Battlefield); } }
        public Heldenian Heldenian { get { return ((Heldenian)currentEvent); } }
        public bool IsHeldenianSeige { get { return (currentEvent != null && currentEvent.Type == WorldEventType.Heldenian && ((Heldenian)currentEvent).Mode == HeldenianType.CastleSeige); } }
        public CaptureTheFlag CaptureTheFlag { get { return ((CaptureTheFlag)currentEvent); } }
        public bool IsCaptureTheFlag { get { return ((currentEvent != null) && currentEvent.Type == WorldEventType.CaptureTheFlag); } }
        public TimeOfDay TimeOfDay { get { return timeOfDay; } }
    }
}
