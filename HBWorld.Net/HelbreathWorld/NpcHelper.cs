﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Common.Assets.Objects;

namespace HelbreathWorld.Common
{
    public static class NpcHelper
    {
        public static Dictionary<string, Npc> LoadNpcs(string configFilePath, Dictionary<string, Item> itemConfiguration, out Dictionary<string,LootTable> primaryLoot, out Dictionary<string, LootTable> secondaryLoot)
        {
            Dictionary<string, Npc> npcs = new Dictionary<string, Npc>();
            primaryLoot = new Dictionary<string, LootTable>();
            secondaryLoot = new Dictionary<string, LootTable>();

            string npcName = "";
            string lootName = ""; // reduce size of try blocks?

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    if (reader.Name.Equals("Npc"))
                        if (!npcs.ContainsKey(reader["Name"]))
                        {
                            Npc npc = Npc.ParseXml(reader);
                            npcs.Add(npc.Name, npc);
                            npcName = npc.Name; // for logging

                            LootTable drops = new LootTable(npc.Name); // primary drops
                            LootTable secDrops = new LootTable(npc.Name); // secondary drops
                            XmlReader lootReader = reader.ReadSubtree();
                            while (lootReader.Read())
                                if (lootReader.IsStartElement())
                                    switch (lootReader.Name)
                                    {
                                        case "Item":
                                            lootName = lootReader["Name"];
                                            LootDrop drop;
                                            if (Enum.TryParse<LootDrop>(lootReader["Drop"], out drop))
                                                switch (drop)
                                                {
                                                    case LootDrop.Primary:
                                                        if (itemConfiguration[lootReader["Name"]].Rarity != ItemRarity.None)
                                                            drops[itemConfiguration[lootReader["Name"]].Rarity].Add(lootReader["Name"]);
                                                        break;
                                                    case LootDrop.Secondary:
                                                        if (itemConfiguration[lootReader["Name"]].Rarity != ItemRarity.None)
                                                            secDrops[itemConfiguration[lootReader["Name"]].Rarity].Add(lootReader["Name"]);
                                                        break;
                                                }
                                            break;
                                    }
                            primaryLoot.Add(npc.Name, drops);
                            secondaryLoot.Add(npc.Name, secDrops);
                        }

            reader.Close();

            return npcs;
        }

        public static Dictionary<string, Npc> LoadNpcs(string configFilePath)
        {
            Dictionary<string, Npc> npcs = new Dictionary<string, Npc>();

            string npcName = "";

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    if (reader.Name.Equals("Npc"))
                        if (!npcs.ContainsKey(reader["Name"]))
                        {
                            Npc npc = Npc.ParseXml(reader);
                            npcs.Add(npc.Name, npc);
                            npcName = npc.Name; // for logging
                        }

            reader.Close();

            return npcs;
        }
    }
}
