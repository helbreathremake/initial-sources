﻿using System;
using System.Collections.Generic;

namespace HelbreathWorld
{
    public static class Globals
    {
        // general
        public static int MaximumLevel = 180;
        public static int MaximumStat = 200;
        public static long[] ExperienceTable; // calculated during game server start up based on maximum level
        public static int ExperienceMultiplier = 1; // default, set in configs
        public static int[] SkillExperienceTable; // calculated during game server start up
        public static int TravellerLimit = 19;
        public static int EnemyKillModifier = 1;
        public static bool PlayersDropItems = true;
        public static Dictionary<string, GenderType> NewPlayerItems; // collected from config
        public readonly static int CreateGuildCost = 2000;
        public readonly static int CreateGuildLevel = 100;
        public readonly static int CreateGuildCharisma = 20;
        public readonly static int MaximumSkills = 60;
        public readonly static int MaximumSpells = 100;
        public readonly static int MaximumInventoryItems = 50;
        public readonly static int MaximumWarehouseItems = 100;
        public readonly static int MaximumGuildMembers = 50;
        public readonly static int MaximumPartyMembers = 8;
        public readonly static int MaximumReputation = 10000;
        public readonly static int MinimumReputation = -10000;
        public readonly static int MagicFlyDamage = 50;
        public readonly static int MagicFlyDamageFightzone = 80;
        public readonly static int MeleeFlyDamage = 40;
        public readonly static int MeleeFlyDamageFightzone = 80;
        public readonly static int MaximumMapPlayers = 200;
        public readonly static int MaximumMapNpcs = 500;
        public readonly static int MaximumMapDynamicObjects = 100;
        public readonly static int MaximumItemsPerTile = 12;
        public readonly static int MaximumSummons = 5;
        public readonly static int MaximumWarContribution = 30000;
        public readonly static int MaximumEnduranceStripable = 2000;

        // percentages
        public static int PrimaryDropRate = 65;
        public static int SecondaryDropRate = 50;
        public readonly static int MinimumSkillLevel = 20;
        public readonly static int MinimumHitChance = 20;
        public readonly static int MaximumHitChance = 90;
        public readonly static int MaximumPhysicalAbsorption = 80;
        public readonly static int MaximumMagicalAbsorption = 80;
        public readonly static int MaximumManaSave = 80;
        public readonly static int MaximumFreezeProtection = 100;
        public readonly static int MaximumPoisonProtection = 100;
        public readonly static int MaximumLuck = 50;
        public readonly static int MaximumStripChance = 50;

        // seconds
        public static int HungerTime = 60;
        public static int HPRegenTime = 15;
        public static int MPRegenTime = 20;
        public static int SPRegenTime = 10;
        public static int PoisonTime = 12;
        public static int ExperienceRollTime = 10;
        public static int MobSpawnRate = 3;
        public static int NpcSummonDuration = 90;
        public static int SpecialAbilityTime = 1200;
        public readonly static int SkillTime = 6;
        public readonly static int ItemClearTime = 300;

        // addons
        public static bool TitlesEnabled = false;

        // bytes
        public readonly static int MaximumDataBuffer = 10240;

        // strings
        public readonly static string WarehouseKeeperName = "Howard";
        public readonly static string TravellerTownName = "default";
        public readonly static string AresdenTownName = "aresden";
        public readonly static string ElvineTownName = "elvine";
        public readonly static string MiddlelandName = "middleland";
        public readonly static string HeldenianBattleFieldName = "BtField";
        public readonly static string HeldenianRampartName = "HRampart";
        public readonly static string HeldenianCastleName = "GodH";
        public readonly static string TravellerRevivalZone = "default";
        public readonly static string AresdenRevivalZone = "resurr1";
        public readonly static string AresdenFarmName = "arefarm";
        public readonly static string ElvineFarmName = "elvfarm";
        public readonly static string ElvineRevivalZone = "resurr1";
        public readonly static string BleedingIsleName = "bisle";
        public readonly static string UnboundItem = "00000000-0000-0000-0000-000000000000";

        // casting probability modifiers based on magic circle 1-10
        public readonly static int[] MagicCastingProbability  = { 0, 300, 250, 200, 150, 100, 80, 70, 60, 50, 40 };
        public readonly static int[] MagicCastingLevelPenalty = { 0, 5, 5, 8, 8, 10, 14, 28, 32, 36, 40 };

        // character movement modifiers
        public readonly static int[] MoveDirectionX = { 0, 0, 1, 1, 1, 0, -1, -1, -1 };
        public readonly static int[] MoveDirectionY = { 0, -1, -1, 0, 1, 1, 1, 0, -1 };
        public readonly static int[] EmptyPositionX = { 0, 1, 1, 0, -1, -1, -1, 0, 1, 2, 2, 2, 2, 1, 0, -1, -2, -2, -2, -2, -2, -1, 0, 1, 2 };
        public readonly static int[] EmptyPositionY = { 0, 0, 1, 1, 1, 0, -1, -1, -1, -1, 0, 1, 2, 2, 2, 2, 2, 1, 0, -1, -2, -2, -2, -2, -2 };
        public readonly static int[,] MoveLocationX = {
	                // 0
	                {0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0},
	                // 1
	                {0,1,2,3,4,5,6,7,8,9,
	                 10,11,12,13,14,15,16,17,18,19,
	                 20,-1,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0},
	                //2
	                {0,1,2,3,4,5,6,7,8,9,
	                 10,11,12,13,14,15,16,17,18,19,
	                 20,20,20,20,20,20,20,20,20,20,
	                 20,20,20,20,20,20,-1},
	                //3
	                {20,20,20,20,20,20,20,20,20,20,
	                 20,20,20,20,20,20,-1,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0},
	                //4
	                {20,20,20,20,20,20,20,20,20,20,
	                 20,20,20,20,20,20,19,18,17,16,
	                 15,14,13,12,11,10,9,8,7,6,
	                 5,4,3,2,1,0,-1},
	                //5
	                {0,1,2,3,4,5,6,7,8,9,
	                 10,11,12,13,14,15,16,17,18,19,
	                 20,-1,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0},
	                //6
	                {0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,1,2,3,4,
	                 5,6,7,8,9,10,11,12,13,14,
	                 15,16,17,18,19,20,-1},
	                //7
	                {0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,-1,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0},
	                //8
	                {0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,1,2,3,4,
	                 5,6,7,8,9,10,11,12,13,14,
	                 15,16,17,18,19,20,-1}
                };
        public readonly static int[,] MoveLocationY = {
	                // 0
	                {0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0},
	                //1
	                {0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,-1,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0},
	                //2
	                {0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,1,2,3,4,5,6,7,8,9,
	                 10,11,12,13,14,15,-1},
	                //3
	                {0,1,2,3,4,5,6,7,8,9,
	                 10,11,12,13,14,15,-1,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0},
	                //4
	                {0,1,2,3,4,5,6,7,8,9,
	                 10,11,12,13,14,15,15,15,15,15,
	                 15,15,15,15,15,15,15,15,15,15,
	                 15,15,15,15,15,15,-1},
	                //5
	                {15,15,15,15,15,15,15,15,15,15,
	                 15,15,15,15,15,15,15,15,15,15,
	                 15,-1,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0},
	                //6
	                {0,1,2,3,4,5,6,7,8,9,
	                 10,11,12,13,14,15,15,15,15,15,
	                 15,15,15,15,15,15,15,15,15,15,
	                 15,15,15,15,15,15,-1},
	                //7
	                {0,1,2,3,4,5,6,7,8,9,
	                 10,11,12,13,14,15,-1,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,0},
	                // 8
	                {15,14,13,12,11,10,9,8,7,6,
	                 5,4,3,2,1,0,0,0,0,0,
	                 0,0,0,0,0,0,0,0,0,0,
	                 0,0,0,0,0,0,-1}
                };

        public readonly static int[,] FarmingSkillTable = 
                {
                    {42, 34, 27, 21, 16, 12,  9,  7,  6},  //20
                    {43, 40, 33, 27, 22, 18, 15, 13, 10},  //30
                    {44, 41, 38, 32, 27, 23, 20, 18, 13},  //40
                    {45, 42, 39, 36, 31, 27, 24, 22, 15},  //50
                    {46, 43, 40, 37, 34, 30, 27, 25, 16},  //60
                    {47, 44, 41, 38, 35, 32, 29, 27, 20},  //70
                    {48, 45, 42, 39, 36, 33, 30, 28, 23},  //80
                    {49, 46, 43, 40, 37, 34, 31, 28, 25},  //90
                    {50, 47, 44, 41, 38, 35, 32, 29, 26}  //100
                };

        public readonly static int[,] FarmingDropTable = 
                {
                   {40,  0,  0,  0,  0,  0,  0,  0,  0},  //20
                   {41, 38,  0,  0,  0,  0,  0,  0,  0},  //30
                   {43, 40, 36,  0,  0,  0,  0,  0,  0},  //40
                   {46, 42, 38, 35,  0,  0,  0,  0,  0},  //50
                   {50, 45, 41, 37, 33,  0,  0,  0,  0},  //60
                   {55, 49, 44, 40, 35, 31,  0,  0,  0},  //70
                   {61, 54, 48, 43, 38, 33, 30,  0,  0},  //80
                   {68, 60, 53, 47, 42, 37, 32, 28,  0},  //90
                   {76, 67, 59, 52, 46, 41, 35, 29, 24}  //100
                };


        // client
        public static readonly bool Debug = true;
        public static readonly bool DebugOwner = true;
        public static readonly int CellWidth = 32;
        public static readonly int CellHeight = 32;
        public static int OffScreenCells = 5;
        public static readonly int CriticalDamage = 200;
        public static readonly int LargeDamage = 40;
        public static readonly int MediumDamage = 12;
    }
}
