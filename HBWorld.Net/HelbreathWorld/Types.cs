﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelbreathWorld
{
    public enum ItemSpecialWeaponPrimaryType
    {
        None = 0,
        Critical = 1,
        Poison = 2,
        Righteous = 3,
        Agile = 5,
        Light = 6,
        Sharp = 7,
        Endurance = 8,
        Ancient = 9,
        CastingProbability = 10,
        ManaConverting = 11,
        CriticalChance = 12,

        //abbreviations for createitem command
        C,
        P,
        CP,
        MC,
        CC
    }

    public enum ItemSpecialWeaponSecondaryType
    {
        None = 0,
        PoisonResistance = 1,
        HittingProbability = 2,
        PhysicalResistance = 3,
        HPRecovery = 4,
        SPRecovery = 5,
        MPRecovery = 6,
        MagicResistance = 7,
        PhysicalAbsorption = 8,
        MagicAbsorption = 9, 
        ConsecutiveDamage = 10,
        Experience = 11,
        Gold = 12,

        //abbreviations for createitem command
        DR,
        HP,
        HPR,
        SPR,
        MPR,
        MR,
        PA,
        MA,
        PR
    }

    public enum ItemUpgradeType
    {
        None,
        Majestic,
        Xelima,
        Merien
    }

    public enum SkillType
    {
        None = -1,
        Mining = 0,
        Fishing = 1,
        Farming = 2,
        MagicResistance = 3,
        Magic = 4,
        Hand = 5,
        Archery = 6,
        ShortSword = 7,
        LongSword = 8,
        Fencing = 9,
        Axe = 10,
        Shield = 11,
        Alchemy = 12,
        Manufacturing = 13,
        Hammer = 14,
        Crafting = 15,
        PretendCorpse = 19,
        Staff = 21,
        PoisonResistance = 23
    }

    public enum OwnerSide
    {
        None = -1,
        Neutral = 0,
        Aresden = 1,
        Elvine = 2,
        Wild = 10
    }

    public enum MovementType
    {
        Undefined,
        None,
        Guard,
        Random,
        RandomArea,
        Follow
    }

    public enum MotionType
    {
        Undefined = -1,
        Idle = 0,
        Move = 1,
        Run = 2,
        Attack =3,       
        Magic = 4,
        PickUp = 5,
        TakeDamage = 6,
        Fly = 7,
        Dash = 8,
        Bow = 9,
        Die = 10,
        AttackStationary = 15, // special case for towers
        BowAttack = 15, // special case for attacking with bow
        None = 100,
        Dead = 101,
        DeadFadeOut = 16, // special case for fading out spirte after death
    }

    public enum AdvancedAIDifficulty
    {
        Easy,
        Normal,
        Hard,
        Heroic,
        Godly
    }

    public enum AdvancedAIEquipment
    {
        Random,
        Civilian,  //clothes
        Light,     //regular
        Medium,    //knight/darkknight
        Heavy      //hero/godly
    }

    public enum AdvancedAIClass
    {
        Warrior,    //plates,wings,horns,2h or 1h+shield
        Mage,       //robes,hats, staves/wands + traps
        BattleMage, //plates,hats,hoses,battlestaffs + traps
        Archer      //leathers,caps(hoods),bows + traps
    }

    public enum AdvancedAIBehaviour
    {
        Defender,   // tanking, shields, short chasing, self preservation
        Supporter,  // buffing, ally target duplicating, escaping, paralysing
        Healer,     // ally preservation, self preservation, healing, curing, cancelling
        Berserker   // damage dealing, berserking, target bias, long chasing
    }

    public enum AdvancedAIState
    {
        AllySelection,        // find allies WithinRange( ) -cache
        ThreatDetection,      // find enemies WithinRage() - cache
        SelfPreservation,     // check own hp, compensate
        AllyPreservation,     // check ally hp, compensate, summon/buff use
        RequestingAssistance, // call ally to target threat
        TargetChasing,        // debilitating, running/chasing
        TargetSelection,      // targetting bias based on target protection type
        UsingSkill            // pretend corpse etc
    }

    public enum MotionDirection
    {
        None = 0,
        North = 1,
        NorthEast = 2,
        East = 3,
        SouthEast = 4,
        South = 5,
        SouthWest = 6,
        West = 7,
        NorthWest = 8
    }

    public enum MotionTurn
    {
        Right = 1,
        Left = 2
    }

    public enum DynamicObjectType
    {
        Fire = 1,
        Fish = 2,
        FishObject = 3,
        Rock = 4,
        Gem = 5,
        AresdenFlag = 6,
        ElvineFlag = 7,
        IceStorm = 8,
        SpikeField = 9,
        PoisonCloudBegin = 10,
        PoisonCloudLoop = 11,
        PoisonCloudEnd = 12,
        Fire2 = 13,
        Fire3 = 14,
        Portal = 20
    }

    public enum DynamicObjectShape
    {
        Wall = 1,
        Field = 2
    }

    public enum OwnerType
    {
        None,
        Player = 0,
        Npc = 1
    }

    public enum OwnerSize
    {
        Small = 0,
        Large = 1
    }

    public enum EquipType
    {
        None = 0,
        Head = 1,
        Arms = 3,
        Body = 2,
        Legs = 4,
        Feet = 5,
        Neck = 6,
        LeftFinger = 11,
        RightFinger = 10,
        LeftHand = 7,
        RightHand = 8,
        DualHand = 9,
        Back = 12,
        FullBody = 13
    }

    public enum ItemCategory
    {
        None = 0,
        Weapon = 1,
        Bow = 3,
        Arrow = 4,
        Shield = 5,
        Armour = 6,
        Stave = 8,
        Clothing = 11,
        Accessory = 12, // (boots, shoes, capes and dark mage robes?)
        Accessory2 = 13, // (hero robes and capes + regular robes?)
        Outfit = 15, // i.e santasuit
        Commodity = 21, //seed bags, potions, tools, gold packets
        Loot = 31, // tablet pieces, monster parts, fish,
        Depletable = 42, // manuals, potions, flags, maps, lottery tickets, guild tickets, scrolls, dyes
        Fishing = 43, // fishing rod
        Valuable = 46 // wares, rings, necklaces, gems, ingots, ores, flowerpot + bouquette
    }

    public enum ItemRarity
    {
        None = 0, // if no rarity specified, it will never drop
        VeryCommon = 1,
        Common = 2,
        Uncommon = 3,
        Rare = 4,
        VeryRare = 5,
        UltraRare = 6
    }

    public enum LootDrop
    {
        Primary,
        Secondary
    }

    public enum ItemType
    {
        None = 0,
        Equip = 1,
        Apply = 2,
        Deplete = 3,
        Install = 4,
        Consume = 5,
        Arrow = 6,
        Eat = 7,
        Skill = 8,
        Perm = 9,
        EnableDialogBox = 10, // ?
        DepleteDestination = 11,
        Material = 12
    }

    public enum ItemEffectType
    {
        None = 0,
        Attack = 1,
        Defence = 2,
        AttackBow = 3,
        HP = 4,
        MP = 5,
        SP = 6,
        Food = 7,
        Get = 8,
        LearnSkill = 9,
        ShowLocation = 10,
        Magic = 11,
        ChangeAttribute = 12,
        AttackManaSave = 13,
        AddEffect = 14,
        MagicAbsorption = 15,
        Flag = 16,
        Dye = 17,
        LearnMagic = 18,
        AttackMaxHPDown = 19, // obsolete. blood weapons are Attack with SpecialEffectType="Blood"
        AttackDefence = 20,
        MaterialAttribute = 21, // ??
        UnlimitedSP = 22,
        Lottery = 23,
        AttackActivation = 24, // activation only, since some special ability types are passive (Kloness, Blood, Berserk etc)
        DefenceActivation = 25,
        Zemstone = 26,
        ConstructionKit = 27,
        Unfreeze = 28,
        DefenceAntiMine = 29, // ??
        FarmSeed = 30,
        Slate = 31,
        ArmourDye = 32,
        GuildAdmission = 33,
        GuildDismissal = 34,
        Summon = 35 //
    }

    public enum ItemSpecialAbilityType
    {
        None = 0,
        XelimaWeapon = 1,
        IceWeapon = 2,
        MedusaWeapon = 3,
        DemonSlayer = 7,
        Dark = 8, // dark executor
        Light = 9, // lighting blade
        Blood = 10,
        Berserk = 11,
        Kloness = 15,
        MerienArmour = 50,
        Unknown = 51, // ??
        MerienShield = 52,
    }



    public enum NpcType
    {
        None,
        CrusadeArrowTower = 36,
        CrusadeCannonTower = 37,
        CrusadeManaCollector = 38,
        CrusadeDetector = 39,
        CrusadeEnergyShield = 40,
        CrusadeGrandMagicGenerator = 41,
        CrusadeManaStone = 42,
        HeldenianWarBeetle = 43,
        HeldenianGodHandKnight = 44,
        HeldenianGodHandKnightMounted = 45,
        HeldenianTempleKnight = 46,
        HeldenianBattleGolem = 47,
        HeldenianCatapult = 51,
        HeldenianBattleTank = 86,
        HeldenianCannonTower = 87,
        HeldenianArrowTower = 89,
        HeldenianGate = 91,
        Crops = 64
    }

    public enum MagicCategory
    {
        Utility = 0,
        Offensive = 1,
        Defensive = 2
    }

    public enum MagicType
    {
        DamageSingle = 1,
        HPUpSingle = 2,
        DamageArea = 3,
        SPDownSingle = 4,
        SPDownArea = 5,
        SPUpSingle = 6,
        SPUpArea = 7,
        Teleport = 8,
        Summon = 9,
        CreateItem = 10,
        Protect = 11,
        Paralyze = 12,
        Invisibility = 13,
        CreateDynamic = 14, // firefield etc
        Possession = 15,
        Confuse = 16,
        Poison = 17,
        Berserk = 18,
        DamageLine = 19,
        Polymorph = 20,
        DamageAreaNoSingle = 21,
        Tremor = 22,
        Ice = 23,
        DamageSingleSPDown = 25,
        IceLine = 26, // TODO should be 27?
        DamageAreaNoSingleSPDown = 27, // earthworm, should be 25?
        ArmourBreak = 28, // TODO should be 26?
        Cancellation = 29, // TODO should be 28?
        DamageLineSPDown = 30,
        Inhibition = 31, // TODO should be 29? mass magic missile uses 31
        Resurrection = 32,
        Scan = 33
    }

    public enum MagicAttribute
    {
        None = 0,
        Earth = 1,
        Air = 2,
        Fire = 3,
        Water = 4,
        Holy = 5,
        Unholy = 6
    }

    public enum MagicTarget
    {
        /// <summary>
        /// Uses effect 1,2,3 in the magic configuration.
        /// </summary>
        Single,
        /// <summary>
        /// Uses effect 4,5,6 in the magic configuration.
        /// </summary>
        Area,
        /// <summary>
        /// Uses effect 7,8,9 in the magic configuration.
        /// </summary>
        Dynamic
    }

    public enum WeatherType
    {
        Clear = 0,
        Rain = 1,
        RainMedium = 2,
        RainHeavy = 3,
        Snow = 4,
        SnowMedium = 5,
        SnowHeavy = 6
    }

    public enum TimeOfDay
    {
        Day = 1,
        Night = 2,
        NightChristmas = 3
    }

    public enum GenderType
    {
        None = 0,
        Male = 1,
        Female = 2
    }

    public enum SkinType
    {
        White = 0,
        Black = 1,
        Yellow = 2
    }

    public enum MineralType
    {
        RockEasy = 1,
        RockMedium = 2,
        RockHard = 3,
        RockExtreme = 4,
        GemEasy = 5,
        GemHard = 6
    }

    public enum FishType
    {
        Fish,
        Item
    }

    public enum BuildingType
    {
        None,
        Warehouse,
        Shop,
        Blacksmith,
        AresdenCityhall,
        ElvineCityhall,
        Church,
        Guildhall,
        WizardTower,
        CommandHall
    }

    public enum WorldEventType
    {
        Crusade,
        Heldenian,
        Apocalypse,
        CaptureTheFlag,
        KingOfTheHill
    }

    public enum CrusadeDuty
    {
        None = 0,
        Fighter = 1,
        Constructor = 2,
        Commander = 3
    }

    public enum HeldenianType
    {
        Battlefield = 1,
        CastleSeige = 2
    }

    public enum PortalType
    {
        Apocalypse,
        ClearAllMobs
    }

    public enum DamageType
    {
        Melee = 0,
        Magic = 1,
        Ranged = 2,
        Environment = 5
    }

    public enum ChatType
    {
        None = -2,
        Command = -1,
        Local = 0,
        Guild = 1,
        Global = 2,
        Town = 3,
        Party = 4,
        GameMaster = 10,
        Whisper = 20
    }

    // TODO - attack type. e.g. > 20 is "critical attack".
    public enum WeaponType
    {
        None,
        Hand,
        Dagger,
        ShortSword,
        Fencing,
        StormBlade,
        LongSword,
        Axe,
        Hammer,
        Wand,
        LongBow,
        ShortBow,
        BattleStaff,
        Throwing
    }

    public enum MarketOrderType
    {
        Buy = 0,
        Sell = 1
    }

    public enum PartyRequestType
    {
        Withdraw = 0,
        Join = 1,
        Dismiss = 2
    }

    public enum QuestType
    {
        Slayer,
        Assassination,
        Delivery,
        Escort,
        Guard,
        GoToLocation,
        BuildStructure,
        SupplyBuildStructure,
        StrategicStrike,
        SendToBattle,
        PlaceFlag
    }

    public enum QuestRewardType
    {
        Gold,
        Item,
        Majestics,
        Contribution
    }

    public enum GameState
    {
        Loading,
        MainMenu,
        AccountCreation,
        Login,
        CharacterSelect,
        Playing,
        Waiting
    }

    public enum GameMouseState
    {
        Normal = 0,
        Pickup = 101, // animation
        Pickup1 = 1,
        Pickup2 = 2,
        Aggressive = 3,
        Magic = 4,
        MagicAggressive = 5,
        Highlight = 6,
        HighlightAggressive = 7,
        Loading = 8,
        Selection = 102, // animation
        Selection1 = 9,
        Selection2 = 10,
        Selection3 = 11
    }

    public enum AnimationType
    {
        Idle = 0,
        IdleAttack = 1,
        Walking = 2,
        WalkingAttack = 3,
        Running = 4,
        Bowing = 5,
        Attacking = 6,
        Archery = 7,
        Casting = 8,
        Pickup = 9,
        TakeDamage = 10,
        Dying = 11,
        Effect = 12,
        None = 15
    }

    public enum AnimationDirection
    {
        North = 0,
        NorthEast = 1,
        East = 2,
        SouthEast = 3,
        South = 4,
        SouthWest = 5,
        West = 6,
        NorthWest = 7,
        None = 15
    }
    public enum SpriteType
    {
        Human,
        Monster,
        Equipment,
        EquipmentPack,
        HairAndUndies,
        Shields,
        Swords,
        Bows,
        Tiles,
        Effect,
        Interface
    }

    public enum FontType
    {
        Dialog,
        General,
        MenuItem,
        MenuItemHighlight,
        Friendly,
        Enemy,
        Neutral,
        DamageSmall,
        DamageMedium,
        DamageLarge,
        Chat,
        Magic
    }

    public enum GameFontType
    {
        Bold,
        Regular,
        Small
    }

    public enum MenuItemType
    {
        Image,
        Text,
        Input
    }

    public enum GraphicsDetail
    {
        Low = 0,
        Medium = 1,
        High = 2
    }

    public enum UserInterfaceType
    {
        Classic,
        World
    }

    public enum DialogBoxType
    {
        None,
        IconPanel,
        Character,
        Inventory,
        Magic
    }
}
