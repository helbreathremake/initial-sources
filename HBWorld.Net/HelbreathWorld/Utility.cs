﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace HelbreathWorld
{
    public static class Utility
    {
        public static int GetManaCost(int manaCost, int manaSave, bool isSafeMode)
        {
            if (manaSave > Globals.MaximumManaSave) manaSave = Globals.MaximumManaSave;

            int cost = (int)((double)manaCost - (((double)manaSave / 100.0f) * (double)manaCost));
            if (cost <= 0) cost = 1;
            if (isSafeMode) cost += (cost / 2) - (cost / 10);
            return cost;
        }

        public static List<int[]> GetMagicLineLocations(int sourceX, int sourceY, int targetX, int targetY)
        {
            List<int[]> hitLocations = new List<int[]>();

            if (sourceX == targetX && sourceY == targetY)
            {
                hitLocations.Add(new int[2] { sourceX, sourceY });
                return hitLocations;
            }

            int rangeX = targetX - sourceX;
            int rangeY = targetY - sourceY;
            int incrementX, incrementY;

            if (rangeX >= 0) incrementX = 1;
            else
            {
                incrementX = -1;
                rangeX = -rangeX;
            }

            if (rangeY >= 0) incrementY = 1;
            else
            {
                incrementY = -1;
                rangeY = -rangeY;
            }

            for (int hit = 2; hit < 10; hit++)
            {
                int error = 0, count = 0;
                int resultX = sourceX, resultY = sourceY;

                if (rangeX > rangeY)
                {
                    for (int index = 0; index <= rangeX; index++)
                    {
                        error += rangeY;
                        if (error > rangeX)
                        {
                            error -= rangeX;
                            resultY += incrementY;
                        }
                        resultX += incrementX;
                        count++;
                        if (count >= hit) break;
                    }
                }
                else
                {
                    for (int index = 0; index <= rangeY; index++)
                    {
                        error += rangeX;
                        if (error > rangeY)
                        {
                            error -= rangeY;
                            resultX += incrementX;
                        }
                        resultY += incrementY;
                        count++;
                        if (count >= hit) break;
                    }
                }

                hitLocations.Add(new int[2] { resultX, resultY });
            }

            return hitLocations;
        }

        public static MotionDirection GetFlyDirection(int sourceX, int sourceY, int targetX, int targetY)
        {
            if (sourceX > targetX)
            {
                if (sourceY > targetY) return MotionDirection.NorthWest;
                else if (sourceY == targetY) return MotionDirection.West;
                else if (sourceY < targetY) return MotionDirection.SouthWest;
            }
            else if (sourceX == targetX)
            {
                if (sourceY > targetY) return MotionDirection.North;
                else if (sourceY == targetY) return MotionDirection.None;
                else if (sourceY < targetX) return MotionDirection.South;
            }
            else if (sourceX < targetX)
            {
                if (sourceY > targetY) return MotionDirection.NorthEast;
                else if (sourceY == targetY) return MotionDirection.East;
                else if (sourceY < targetY) return MotionDirection.SouthEast;
            }
            
            return MotionDirection.None;
        }

        public static MotionDirection GetNextDirection(int sourceX, int sourceY, int destinationX, int destinationY)
        {
            int x, y;
            MotionDirection direction = MotionDirection.None;

            x = sourceX - destinationX;
            y = sourceY - destinationY;

            if ((x == 0) && (y == 0)) direction = MotionDirection.None;

            if (x == 0)
            {
                if (y > 0) direction = MotionDirection.North;
                if (y < 0) direction = MotionDirection.South;
            }
            if (y == 0)
            {
                if (x > 0) direction = MotionDirection.West;
                if (x < 0) direction = MotionDirection.East;
            }
            if ((x > 0) && (y > 0)) direction = MotionDirection.NorthWest;
            if ((x < 0) && (y > 0)) direction = MotionDirection.NorthEast;
            if ((x > 0) && (y < 0)) direction = MotionDirection.SouthWest;
            if ((x < 0) && (y < 0)) direction = MotionDirection.SouthEast;

            return direction;
        }

        public static int GetRange(int appearance2, bool critical) { return GetRange(GetWeaponType(appearance2), critical); }
        public static int GetRange(WeaponType weaponType, bool critical)
        {
            switch (weaponType)
            {
                default:
                case WeaponType.None: return 0;
                case WeaponType.Hand: return 1;
                case WeaponType.Dagger: return 1;
                case WeaponType.ShortSword: return 1;
                case WeaponType.Fencing: return critical ? 5 : 1;
                case WeaponType.LongSword: return critical ? 3 : 1;
                case WeaponType.StormBlade: return critical ? 5 : 3;
                case WeaponType.Axe: return critical ? 2 : 1;
                case WeaponType.Hammer: return critical ? 2 : 1;
                case WeaponType.Wand: return critical ? 2 : 1;
                case WeaponType.BattleStaff: return critical ? 4 : 3;
                case WeaponType.LongBow: return critical ? 9 : 8;
                case WeaponType.ShortBow: return critical ? 6 : 5;
                case WeaponType.Throwing: return critical ? 6 : 5;
            }
        }

        public static bool IsSelected(int mouseX, int mouseY, int left, int top, int right, int bottom)
        {
            if (mouseX > left && mouseX < right &&
                        mouseY > top && mouseY < bottom)
            {
                return true;
            }
            return false;
        }

        public static WeaponType GetWeaponType(int appearance2)
        {
            int type = ((appearance2 & 0x0FF0) >> 4);

            if (type == 0) return WeaponType.Hand;
            else if (type >= 1 && type < 3) return WeaponType.ShortSword;
            else if (type >= 3 && type < 20)
            {
                if (type == 7 || type == 18) return WeaponType.Fencing;
                else return WeaponType.LongSword;
            }
            else if (type >= 20 && type < 29) return WeaponType.Axe;
            else if (type >= 30 && type < 33) return WeaponType.Hammer;
            else if (type >= 34 && type < 40) return WeaponType.Wand;
            else if (type >= 40 && type < 41) return WeaponType.ShortBow;
            else if (type >= 41 && type < 50) return WeaponType.LongBow;
            else if (type == 29 || type == 33) return WeaponType.LongSword;
            else if (type >= 50 && type < 60) return WeaponType.BattleStaff;
            else if (type >= 0 && type < 70) return WeaponType.Throwing;
            else return WeaponType.None;
        }

        public static string ByteArrayToString(byte[] ba)
        {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }

        public static string ByteArrayToString(byte[] ba, int startIndex, int length)
        {
            byte[] na = new byte[length];
            Buffer.BlockCopy(ba, startIndex, na, 0, length);
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }

        /// <summary>
        /// Legacy Encryption algorithm used by existing client
        /// </summary>
        /// <param name="message">Message to be sent to client.</param>
        /// <param name="encryptionKey">Character used as encryption key. NULL if no encryption.</param>
        /// <returns>Byte array of encrypted data to be sent to client.</returns>
        public static byte[] Encrypt(byte[] originalData, bool useEncryption)
        {
            if (originalData.Length > short.MaxValue) return null; // max short size
            int size = originalData.Length;
            if (size + 3 > Globals.MaximumDataBuffer) return null; // max buffer size

            char encryptionKey;
            if (useEncryption)
            {
                Random rnd = new Random();
                encryptionKey = (char)rnd.Next(1, 255);
            }
            else encryptionKey = (char)0;

            byte[] data = new byte[size + 3];

            data[0] = (byte)encryptionKey;
            Buffer.BlockCopy(BitConverter.GetBytes(size), 0, data, 1, 2); // data[1] and data[2]
            Buffer.BlockCopy(originalData, 0, data, 3, size);
            if (encryptionKey != 0)
                for (int i = 0; i < size; i++)
                {
                    data[3 + i] += (byte)(i ^ encryptionKey);
                    data[3 + i] = (byte)(data[3 + i] ^ (encryptionKey ^ (size - i)));
                }

            return data;
        }

        /// <summary>
        /// Legacy Decryption algorithm used by existing client
        /// </summary>
        /// <param name="data">Data received from client.</param>
        /// <returns>Byte array of decrypted data received from client.</returns>
        public static byte[] Decrypt(byte[] data)
        {
            char key = (char)data[0];
            int size = BitConverter.ToInt16(data, 1);
            size -= 3;

            if (size > Globals.MaximumDataBuffer) size = Globals.MaximumDataBuffer; // max buffer

            if (key != 0)
                for (int i = 0; i < size; i++)
                {
                    data[3 + i] = (byte)(data[3 + i] ^ (key ^ ((size) - i)));
                    data[3 + i] -= (byte)(i ^ key);
                }

            byte[] ret = new byte[size];
            Buffer.BlockCopy(data, 3, ret, 0, size);
            return ret;
        }

        public static byte[] GetCommandTypeBytes(CommandType type)
        {
            return BitConverter.GetBytes((int)type);
        }

        public static byte[] GetCommandTypeBytes(CommandMessageType type)
        {
            return BitConverter.GetBytes((int)type);
        }

        public static byte[] GetCommandTypeBytes(MotionType type)
        {
            return BitConverter.GetBytes((int)type);
        }

        public static Image GetImage(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            Bitmap temp = new Bitmap(stream);
            Bitmap bitmap = new Bitmap(temp);
            temp.Dispose();
            temp = null;
            stream.Close();
            stream = null;

            return (Image)bitmap;
        }

        public static string GetText(byte[] data)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetString(data);
        }
    }

    public static class Extensions
    {

        public static byte[] GetBytes(this int value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetBytes(this short value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetBytes(this bool value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetBytes(this long value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetBytes(this string value, int maxWidth)
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            return enc.GetBytes(value.PadRight(maxWidth, '\0'));
        }
    }
}
