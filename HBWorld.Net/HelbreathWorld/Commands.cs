﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelbreathWorld
{
    /// <summary>
    /// Legacy Helbreath messages according to NetMessages.h. 
    /// HandleCommand will match these as Integers at runtime, so no need to convert.
    /// </summary>
    public enum CommandType
    {
        // LOGIN SERVER
        RequestLogin = 0x0FC94201,
        CreateAccount = 0x0FC94202,
        CreateCharacter = 0x0FC94204,
        RequestEnterGame = 0x0FC94205,
        ChangePassword = 0x0FC94210,       
        Confirm = 0x0F14,
        Reject = 0x0F15,
        PasswordIncorrect = 0x0F16,
        NewAccountCreated = 0x0F18,
        NewAccountFailed =  0x0F19,
        NewAccountAlreadyExists = 0x0F1A,
        NewCharacterCreated = 0x0F1C,
        NewCharacterFailed = 0x0F1D,
        NewCharacterAlreadyExists = 0x0F1E,
        EnterGameFailed = 0x0F21,
        EnterGameSucceed = 0x0F22,
        EnterGameAlreadyLoggedIn = 0x0F20,
        LoginUnavailable = 0x0F32,
        ChangePasswordSucceed = 0x0A00,
        ChangePasswordFailed = 0x0A01,

        // GAME SERVER
        Chat = 0x03203204,
        CheckConnection = 0x03203203,
        SetItemPosition = 0x180ACE0A,
        CommonEvent = 0x0FA314DB,
        Common = 0x0FA314DC,
        Motion = 0x0FA314D7,
        LogEvent = 0x0FA314D8,
        Notify = 0x0FA314D0,
        DynamicObjectEvent = 0x12A01001,
        RequestInitPlayer = 0x05040205,
        ResponseInitPlayer = 0x05040206,
        RequestInitData = 0x05080404,
        ResponseInitData = 0x05080405,
        ResponseInitDataPlayerStats = 0x0FA40000,
        ResponseInitDataPlayerItems = 0x0FA314DD,
        RequestMotion = 0x0FA314D5,
        ResponseMotion = 0x0FA314D6,
        RequestFullObjectData = 0x0DF40000,
        ChangeStatsLevelUp = 0x11A01000,
        ChangeStatsMajestics = 0x11A01001,
        RequestTeleport = 0x0EA03201,
        RequestCitizenship = 0x0FC9420E,
        ResponseCitizenship = 0x0FC9420F,
        RequestWarehouseItem = 0x0DF30751,
        ResponseWarehouseItem = 0x0DF30752,
        RequestRestart = 0x28010EEE,
        RequestHeldenianTeleport = 0x0EA03206,
        ResponseHeldenianTeleport = 0x0EA03207,
        RequestAngel = 0x0FC9421E,
        RequestNews = 0x220B2F00,
        ResponseNews = 0x220B2F01,
        RequestCreateGuild = 0x0FC94208,
        ResponseCreateGuild = 0x0FC94209,
        RequestDisbandGuild = 0x0FC9420A,
        ResponseDisbandGuild = 0x0FC9420B,
        RequestSellItemList = 0x2900AD30
    }

    public enum CommandMessageType
    {
        Confirm = 0x0F14,
        Reject = 0x0F15,
        ToggleCombatMode = 0x0A0B,
        ToggleSafeMode = 0x0A18,

        // Common
        UseItem = 0x0A11,
        UseSkill = 0x0A12,
        GiveItem = 0x0A05,
        DropItem = 0x0A01,
        SetItem  = 0x0A0C,
        EquipItem = 0x0A02,
        UnEquipItem = 0x0A0A,
        CastMagic   = 0x0A0D,
        UpgradeItem = 0x0A58,
        UseSpecialAbility = 0x0A40,
        BuySpell = 0x0A0E,
        BuyItem = 0x0A04,
        SellItem = 0x0A13,
        SellItemConfirm = 0x0A15,
        RepairItem = 0x0A14,
        RepairItemConfirm = 0x0A16,
        GetGuildName = 0x0A59,
        EnsureMagicSkill = 0x0A24,
        Fish = 0x0A17,
        Craft = 0x0A28,
        Manufacture = 0x0A23,
        Alchemy = 0x0A19,
        Slate = 0x0A61,
        MapStatus = 0x0A52,
        CrusadeTeleport = 0x0A55,
        CrusadeSetDuty = 0x0A51,
        CrusadeSetTeleportLocation = 0x0A54,
        CrusadeSetBuildLocation = 0x0A57,
        CrusadeSummon = 0x0A56,
        HeldenianFlag = 0x0A1C,
        ClearGuildName = 0x0A25,
        JoinGuildApprove = 0x0A06,
        HandlePartyRequest = 0x0A30,
        PartyRequest = 0x0A31,
        PartyResponse = 0x0A32,
        AcceptQuest = 0x0A22,
        TalkToNpc = 0x0A1A,
        DismissGuildsmanApprove = 0x0A08,
        DismissGuildsmanReject = 0x0A09,

        // Notifications
        NotifyFly = 0x0B74,
        NotifyHP = 0x0B07,
        NotifyMP = 0x0B14,
        NotifySP = 0x0B15,
        NotifyAngelStats = 0x0BF2,
        NotifySkill = 0x0B23,
        NotifySkillEnd = 0x0B2A,
        NotifyStudySpellSuccess = 0x0B10,
        NotifyStudySpellFailed = 0x0B11,
        NotifyStudySkillSuccess = 0x0B12,
        NotifyStudySkillFailed = 0x0B13, // unused
        NotifyWhisperOn = 0x0B35,
        NotifyWhisperOff = 0x0B36,
        NotifyExperience = 0x0B0A,
        NotifyLevelUp = 0x0B16,
        NotifyMajestics = 0x0BA4,
        NotifyEnemyKills = 0x0B5A,
        NotifyEnemyKillReward = 0x0B1C,
        NotifyCriminalCount = 0x0B1A,
        NotifyCriticals = 0x0B52,
        NotifySpellsAndSkills = 0x0BF6,
        NotifyDropItemAndErase = 0x0B1F,
        NotifyGiveItemAndErase = 0x0B1D,
        NotifyNotEnoughGold = 0x0B08,
        NotifyItemUnequipped = 0x0B5C,
        NotifyItemObtained = 0x0B01,
        NotifyItemPurchased = 0x0B06,
        NotifyItemDepleted = 0x0B20,
        NotifyItemBroken = 0x0B17,
        NotifyItemToWarehouse = 0x0B19,
        NotifyItemCount = 0x0B25,
        NotifyItemColourChange = 0x0B65,
        NotifyItemBagPositions = 0x0B5B,
        NotifyInventoryFull = 0x0B05,
        NotifyWarehouseFull = 0x0B26,
        NotifyHunger = 0x0B39,
        NotifyStatChangeLevelUpSuccess = 0x0BB3,
        NotifyStatChangeLevelUpFailed = 0x0BB4,
        NotifyStatChangeMajesticsSuccess = 0x0BB5,
        NotifyStatChangeMajesticsFailed = 0x0BB6,
        NotifyMagicEffectOn = 0x0B27,
        NotifyMagicEffectOff = 0x0B28,
        NotifyTimeChange = 0x0B41,
        NotifyWeatherChange = 0x0B4D,
        NotifySafeMode = 0x0B51,
        NotifyAngelUpgrade = 0x0A9C, // N/A for current client
        NotifyItemUpgrade = 0x0BA5,
        NotifyItemUpgradeFailed = 0x0BA8,
        NotifySpecialAbilityEnabled = 0x0B92,
        NotifySpecialAbilityStatus = 0x0B93,
        NotifyDead = 0x0B09,
        NotifyGuildName = 0x0BA6,
        NotifyFarmingSkillTooLow = 0x0BB1,
        NotifyFarmingInvalidLocation = 0x0BB2,
        NotifyFishingStarted = 0x0B47,
        NotifyFishingChance = 0x0B48,
        NotifyFishingSuccess = 0x0B4A,
        NotifyFishingFailed = 0x0B4B,
        NotifyManufactureSuccess = 0x0B70,
        NotifyManufactureFailed = 0x0B71,
        NotifyApocalypseStart = 0x0BD2,
        NotifyApocalypseEnd = 0x0BD3,
        NotifyApocalypseGateOpen = 0x0BD4,
        NotifyApocalypseGateClosed = 0x0BD5,
        NotifyAbaddonKilled = 0x0BD6,
        NotifyCrusade = 0x0B94,
        NotifyCrusadeStrikeIncoming = 0x0B9B,
        NotifyCrusadeStrikeHit = 0x0B9C,
        NotifyCrusadeStatistics = 0x0B9F,
        NotifyCrusadeTeleportLocation = 0x0BA0,
        NotifyCrusadeMeteorStrike = 0x0B9C,
        NotifyCrusadeMeteorStrikeResult = 0x0B9D,
        NotifyCrusadeStructureLimitReached = 0x0B9E,
        NotifyMapStatusNext = 0x0B97,
        NotifyMapStatusLast = 0x0B98,
        NotifyHeldenian = 0x0BE6,
        NotifyHeldenianStatistics = 0x0BEC,
        NotifyHeldenianStarted = 0x0BEA,
        NotifyHeldenianEnded = 0x0BE7,
        NotifyHeldenianVictory = 0x0BEB,
        NotifyHeldenianFlagFailed = 0x0B5D,
        NotifyTravellerLimit = 0x0B38,
        NotifyPlayerNotOnline = 0x0B34,
        NotifyReputationFailed = 0x0B44,
        NotifyReputationSuccess = 0x0B45,
        NotifyGuildDisbanded = 0x0B0B,
        NotifyGuildAdmissionRequest = 0x0B02,
        NotifyGuildDismissalRequest = 0x0B03,
        NotifyParty = 0x0BA2,
        NotifyPartyRequest = 0x0B81,
        NotifyCriminalCaptured = 0x0B1B,
        NotifySellItemFailed = 0x0B2C,
        NotifySellItemQuote = 0x0B2D,
        NotifyItemSold = 0x0B31,
        NotifyRepairItemFailed = 0x0B2E,
        NotifyRepairItemQuote = 0x0B2F,
        NotifyItemRepaired = 0x0B30,

        // Object Actions
        ObjectStop = 0,
        ObjectMove = 1,
        ObjectRun = 2,
        ObjectAttack = 3,
        ObjectMagic = 4,
        ObjectPickUp = 5,
        ObjectTakeDamage = 6,
        ObjectTakeDamageAndFly = 7,
        ObjectAttackDash = 8,
        ObjectDying = 10,
        OwnerStop = 94,
        OwnerMove = 54,
        OwnerRun = 63,
        OwnerAttack = 22,
        OwnerMagic = 41,
        OwnerPickUp = 88,
        OwnerTakeDamage = 23,
        OwnerTakeDamageAndFly = 14,
        OwnerAttackDash = 32,
        OwnerDying = 45,
        ObjectNullAction = 100,
        ObjectDead = 101,
        ObjectConfirmMove = 1001,
        ObjectRejectMove = 1010,
        ObjectConfirmMotion = 1020,
        ObjectConfirmAttack = 1030,
        ObjectRejectMotion = 1040
    }

    public enum Stat
    {
        Strength = 0x01,
        Dexterity = 0x02,
        Intelligence = 0x03,
        Vitality = 0x04,
        Magic = 0x05,
        Charisma = 0x06
    }

    public class Command
    {
        private Byte[] data;
        //private String identity;
        private int identity;
        private string ipAddress;

        public Command(Byte[] data, int identity)
        {
            this.data = data;
            this.identity = identity;
        }

        public Command(Byte[] data, int identity, string ipAddress)
        {
            this.data = data;
            this.identity = identity;
            this.ipAddress = ipAddress;
        }

        public Byte[] Data
        {
            get { return data; }
        }

        public Int32 Identity
        {
            get { return identity; }
        }

        public String IPAddress
        {
            get { return ipAddress; }
        }
    }
}
