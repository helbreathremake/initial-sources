﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelbreathWorld
{
    public enum LogType
    {
        Login,
        Game,
        Item,
        Hack,
        Error,
        Chat,
        Admin,
        Map,
        Test,
        Database,
        Events
    }
}
