﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Windows.Forms;

using HelbreathWorld;
using HelbreathWorld.Server;

namespace LoginServer
{
    public partial class LoginServerForm : Form
    {
        private string installPath;
        private string logPath;

        private int clientPort, gamePort;
        private string connectionString;
        private ListBox txtLoginLog;
        private TabControl tabControl1;
        private TabPage tabPage2;
        private TabPage tabPage1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ListBox listAccounts;
        private GroupBox groupBox2;
        private ListBox listCharacters;
        private GroupBox groupBox1;
        private Label lblCreatedOn;
        private Label lblName;
        private SplitContainer splitContainer1;
        private SplitContainer splitContainer2;
        private Label lblCharacterChar;
        private Label lblCharacterMagic;
        private Label lblCharacterInt;
        private Label lblCharacterVit;
        private Label lblCharacterDex;
        private Label lblCharacterStrength;
        private Label lblCharacterName;
        private TabPage tabPage3;
        private GroupBox groupBox3;
        private Label lblStatus;
        private Label label11;
        private IContainer components;
        private Timer timer1;
        private Label lblUpTime;
        private Label label4;

        private HelbreathWorld.Server.LoginServer loginServer;

        public LoginServerForm()
        {
            InitializeComponent();

            if (LoadSettings())
            {
                loginServer = new HelbreathWorld.Server.LoginServer(clientPort, gamePort, connectionString);
                loginServer.MessageLogged += new HelbreathWorld.Server.LoginServer.LogHandler(LogMessage);
                loginServer.Start();

                LoadAccounts();
            }
            else
            {
                MessageBox.Show("Unable to read config.xml file");
            }
        }
        private void SelectCharacter(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(string.Format("SELECT * FROM Characters WHERE Name='{0}'", listCharacters.SelectedItem.ToString()), connection))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            lblCharacterName.Text = string.Format("Name: {0}", reader["Name"]);
                            lblCharacterStrength.Text = string.Format("STR: {0}", reader["Strength"]);
                            lblCharacterDex.Text = string.Format("DEX: {0}", reader["Dexterity"]);
                            lblCharacterInt.Text = string.Format("INT: {0}", reader["Intelligence"]);
                            lblCharacterMagic.Text = string.Format("MAG: {0}", reader["Magic"]);
                            lblCharacterVit.Text = string.Format("VIT: {0}", reader["Vitality"]);
                            lblCharacterChar.Text = string.Format("AGI: {0}", reader["Charisma"]);
                        }
                        reader.Close();
                    }
                }
            }
            catch
            { }
        }

        private void SelectAccount(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(string.Format("SELECT Name,CreatedOn FROM Accounts WHERE Name = '{0}'", listAccounts.SelectedItem.ToString()), connection))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                            while (reader.Read())
                            {
                                lblName.Text = string.Format("Name: {0}", reader["Name"]);
                                lblCreatedOn.Text = string.Format("Created On: {0}", reader["CreatedOn"]);
                            }
                        reader.Close();
                    }

                    using (SqlCommand command = new SqlCommand(string.Format("SELECT Name FROM Characters WHERE AccountID = (SELECT ID FROM Accounts WHERE Name = '{0}')", listAccounts.SelectedItem.ToString()), connection))
                    {
                        listCharacters.Items.Clear();
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                            while (reader.Read())
                            {
                                listCharacters.Items.Add(reader["Name"]);
                            }
                        reader.Close();
                    }
                    if (listCharacters.Items.Count > 0) listCharacters.SelectedIndex = 0;
                }
            }
            catch
            { }
        }

        private void LoadAccounts()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("SELECT Name FROM Accounts ORDER BY Name ASC", connection))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                            while (reader.Read())
                            {
                                listAccounts.Items.Add(reader["Name"]);
                            }
                        reader.Close();
                    }
                }
                if (listAccounts.Items.Count > 0) listAccounts.SelectedIndex = 0;
            }
            catch
            { }
        }

        private bool LoadSettings()
        {
            try
            {
                installPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");
                logPath = installPath + @"\Logs\";

                if (!Directory.Exists(logPath)) Directory.CreateDirectory(logPath);

                XmlTextReader reader = new XmlTextReader(installPath + @"\config.xml");

                while (reader.Read())
                    if (reader.IsStartElement())
                        switch (reader.Name)
                        {
                            case "Servers":
                                connectionString = reader["ConnectionString"];
                                break;
                            case "LoginServer": 
                                clientPort = Convert.ToInt32(reader["ClientPort"]);
                                gamePort = Convert.ToInt32(reader["GameServerPort"]);
                                break;
                        }
                reader.Close();

                return true;
            }
            catch
            {
                return false;
            }
        }

        private void LogMessage(string message, LogType logType)
        {
            switch (logType)
            {
                case LogType.Login: WriteToScreen(message); break;
            }

            WriteToLogFile(message, logType);
        }

        private void WriteToScreen(string message)
        {
            if (txtLoginLog.InvokeRequired)
                this.txtLoginLog.Invoke(new UpdateTextBoxDelegate(UpdateTextBox), message);
            else UpdateTextBox(message);
        }

        delegate void UpdateTextBoxDelegate(string message);
        private void UpdateTextBox(string message)
        {
            this.txtLoginLog.Items.Add(String.Format("{0}-{1}:  {2}", DateTime.Now.Hour, DateTime.Now.Minute, message));
        }

        private void WriteToLogFile(string message, LogType logType)
        {
            String logPath;

            switch (logType)
            {
                case LogType.Game:          logPath = this.logPath + "GameServer.log"; break;
                case LogType.Hack:          logPath = this.logPath + "HackEvents.log"; break;
                case LogType.Item:          logPath = this.logPath + "ItemEvents.log"; break;
                case LogType.Login:         logPath = this.logPath + "Logins.log"; break;
                case LogType.Error:         logPath = this.logPath + "LoginServerError.log"; break;
                case LogType.Database:      logPath = this.logPath + "Database.log"; break;
                default:                    logPath = this.logPath + "GameServer.log"; break;
            }

            string log = String.Format("{0}-{1}-{2}-{3}-{4}-{5}:  ", DateTime.Now.Day, DateTime.Now.Month,
                                                                      DateTime.Now.Year, DateTime.Now.Hour,
                                                                      DateTime.Now.Minute, DateTime.Now.Second)
                                                                      + message;

            try
            {
                StreamWriter writer;

                if (File.Exists(logPath))
                {
                    FileInfo fi = new FileInfo(logPath);

                    if (fi.Length >= 1024000)
                    {
                        File.Copy(logPath, logPath + ".old", true);
                        File.Delete(logPath);

                        writer = File.CreateText(logPath);
                        writer.WriteLine(log);
                    }
                    else
                    {
                        writer = File.AppendText(logPath);
                        writer.WriteLine(log);
                    }
                }
                else
                {
                    writer = File.CreateText(logPath);
                    writer.WriteLine(log);
                }

                writer.Close();
            }
            catch //(Exception ex) TODO - what happens when logging fails? =(
            { }
        }

        private void Exit(object sender, EventArgs e)
        {
            if (loginServer.Running)
            {
                MessageBox.Show("Login Server is Running.");
            }
            else this.Close();
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginServerForm));
            this.txtLoginLog = new System.Windows.Forms.ListBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listAccounts = new System.Windows.Forms.ListBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblCreatedOn = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblCharacterChar = new System.Windows.Forms.Label();
            this.lblCharacterMagic = new System.Windows.Forms.Label();
            this.lblCharacterInt = new System.Windows.Forms.Label();
            this.lblCharacterVit = new System.Windows.Forms.Label();
            this.lblCharacterDex = new System.Windows.Forms.Label();
            this.lblCharacterStrength = new System.Windows.Forms.Label();
            this.lblCharacterName = new System.Windows.Forms.Label();
            this.listCharacters = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblUpTime = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtLoginLog
            // 
            this.txtLoginLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLoginLog.FormattingEnabled = true;
            this.txtLoginLog.Location = new System.Drawing.Point(3, 3);
            this.txtLoginLog.Name = "txtLoginLog";
            this.txtLoginLog.Size = new System.Drawing.Size(770, 506);
            this.txtLoginLog.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(784, 538);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(776, 512);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Status";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblUpTime);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.lblStatus);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Location = new System.Drawing.Point(8, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(760, 152);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "General";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(66, 19);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(52, 13);
            this.lblStatus.TabIndex = 24;
            this.lblStatus.Text = "Starting...";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Status:";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(776, 512);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Accounts";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listAccounts);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(770, 506);
            this.splitContainer1.SplitterDistance = 150;
            this.splitContainer1.TabIndex = 3;
            // 
            // listAccounts
            // 
            this.listAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listAccounts.FormattingEnabled = true;
            this.listAccounts.Location = new System.Drawing.Point(0, 0);
            this.listAccounts.Name = "listAccounts";
            this.listAccounts.Size = new System.Drawing.Size(150, 506);
            this.listAccounts.TabIndex = 0;
            this.listAccounts.SelectedIndexChanged += new System.EventHandler(this.SelectAccount);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer2.Size = new System.Drawing.Size(616, 506);
            this.splitContainer2.SplitterDistance = 161;
            this.splitContainer2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblCreatedOn);
            this.groupBox1.Controls.Add(this.lblName);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(616, 161);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General";
            // 
            // lblCreatedOn
            // 
            this.lblCreatedOn.AutoSize = true;
            this.lblCreatedOn.Location = new System.Drawing.Point(6, 38);
            this.lblCreatedOn.Name = "lblCreatedOn";
            this.lblCreatedOn.Size = new System.Drawing.Size(87, 13);
            this.lblCreatedOn.TabIndex = 1;
            this.lblCreatedOn.Text = "Created On: N/A";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(6, 16);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(61, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name: N/A";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblCharacterChar);
            this.groupBox2.Controls.Add(this.lblCharacterMagic);
            this.groupBox2.Controls.Add(this.lblCharacterInt);
            this.groupBox2.Controls.Add(this.lblCharacterVit);
            this.groupBox2.Controls.Add(this.lblCharacterDex);
            this.groupBox2.Controls.Add(this.lblCharacterStrength);
            this.groupBox2.Controls.Add(this.lblCharacterName);
            this.groupBox2.Controls.Add(this.listCharacters);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(616, 341);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Characters";
            // 
            // lblCharacterChar
            // 
            this.lblCharacterChar.AutoSize = true;
            this.lblCharacterChar.Location = new System.Drawing.Point(263, 83);
            this.lblCharacterChar.Name = "lblCharacterChar";
            this.lblCharacterChar.Size = new System.Drawing.Size(33, 13);
            this.lblCharacterChar.TabIndex = 8;
            this.lblCharacterChar.Text = "CHR:";
            // 
            // lblCharacterMagic
            // 
            this.lblCharacterMagic.AutoSize = true;
            this.lblCharacterMagic.Location = new System.Drawing.Point(263, 61);
            this.lblCharacterMagic.Name = "lblCharacterMagic";
            this.lblCharacterMagic.Size = new System.Drawing.Size(34, 13);
            this.lblCharacterMagic.TabIndex = 7;
            this.lblCharacterMagic.Text = "MAG:";
            // 
            // lblCharacterInt
            // 
            this.lblCharacterInt.AutoSize = true;
            this.lblCharacterInt.Location = new System.Drawing.Point(263, 39);
            this.lblCharacterInt.Name = "lblCharacterInt";
            this.lblCharacterInt.Size = new System.Drawing.Size(28, 13);
            this.lblCharacterInt.TabIndex = 6;
            this.lblCharacterInt.Text = "INT:";
            // 
            // lblCharacterVit
            // 
            this.lblCharacterVit.AutoSize = true;
            this.lblCharacterVit.Location = new System.Drawing.Point(135, 83);
            this.lblCharacterVit.Name = "lblCharacterVit";
            this.lblCharacterVit.Size = new System.Drawing.Size(27, 13);
            this.lblCharacterVit.TabIndex = 5;
            this.lblCharacterVit.Text = "VIT:";
            // 
            // lblCharacterDex
            // 
            this.lblCharacterDex.AutoSize = true;
            this.lblCharacterDex.Location = new System.Drawing.Point(135, 61);
            this.lblCharacterDex.Name = "lblCharacterDex";
            this.lblCharacterDex.Size = new System.Drawing.Size(32, 13);
            this.lblCharacterDex.TabIndex = 4;
            this.lblCharacterDex.Text = "DEX:";
            // 
            // lblCharacterStrength
            // 
            this.lblCharacterStrength.AutoSize = true;
            this.lblCharacterStrength.Location = new System.Drawing.Point(135, 39);
            this.lblCharacterStrength.Name = "lblCharacterStrength";
            this.lblCharacterStrength.Size = new System.Drawing.Size(32, 13);
            this.lblCharacterStrength.TabIndex = 3;
            this.lblCharacterStrength.Text = "STR:";
            // 
            // lblCharacterName
            // 
            this.lblCharacterName.AutoSize = true;
            this.lblCharacterName.Location = new System.Drawing.Point(135, 16);
            this.lblCharacterName.Name = "lblCharacterName";
            this.lblCharacterName.Size = new System.Drawing.Size(61, 13);
            this.lblCharacterName.TabIndex = 2;
            this.lblCharacterName.Text = "Name: N/A";
            // 
            // listCharacters
            // 
            this.listCharacters.FormattingEnabled = true;
            this.listCharacters.Location = new System.Drawing.Point(9, 19);
            this.listCharacters.Name = "listCharacters";
            this.listCharacters.Size = new System.Drawing.Size(120, 95);
            this.listCharacters.TabIndex = 0;
            this.listCharacters.SelectedIndexChanged += new System.EventHandler(this.SelectCharacter);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtLoginLog);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(776, 512);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Logging";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.Exit);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.UpdateLoginServerInfo);
            // 
            // lblUpTime
            // 
            this.lblUpTime.AutoSize = true;
            this.lblUpTime.Location = new System.Drawing.Point(66, 41);
            this.lblUpTime.Name = "lblUpTime";
            this.lblUpTime.Size = new System.Drawing.Size(13, 13);
            this.lblUpTime.TabIndex = 26;
            this.lblUpTime.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Up Time:";
            // 
            // LoginServerForm
            // 
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(800, 600);
            this.Name = "LoginServerForm";
            this.Text = "Helbreath Champions Login Server by Vamp";
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void UpdateLoginServerInfo(object sender, EventArgs e)
        {
            lblStatus.Text = (loginServer.Running) ? "Running" : "Stopped";
            lblStatus.ForeColor = (loginServer.Running) ? Color.Green : Color.Red;
            TimeSpan upTime = ((TimeSpan)(DateTime.Now - loginServer.StartTime));
            lblUpTime.Text = upTime.Days + "d " + upTime.Hours + "h " + upTime.Minutes + "m " + upTime.Seconds + "s";
        }
    }
}
