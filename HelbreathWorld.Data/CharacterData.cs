﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;

using HelbreathWorld.Common;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Common.Assets.Objects;

namespace HelbreathWorld.Data
{
    public class CharacterData
    {
        public delegate void LogHandler(string message);
        public event LogHandler MessageLogged;

        private Connection connection;

        public CharacterData(Connection connection)
        {
            this.connection = connection;
        }

        public Dictionary<string, Character> LoadAllForLogin(string accountId)
        {
            Dictionary<string, Character> list = new Dictionary<string, Character>();

            SqlCommand command = new SqlCommand("SELECT * FROM Characters WHERE AccountID = @AccountID");
            command.Parameters.AddWithValue("@AccountID", accountId);

            DataTable table = connection.ExecuteSQLTable(command);

            if (table != null)
                foreach (DataRow row in table.Rows)
                {
                    Character c = Load(row["Name"].ToString());
                    list.Add(c.Name, c);
                    LoadInventory(c);
                }

            return list;
        }

        public Character Load(string characterName)
        {
            Character c = new Character();
            SqlCommand command = new SqlCommand("SELECT * FROM Characters WHERE lower(Name) = lower(@Name)");
            command.Parameters.AddWithValue("@Name", characterName);

            c.Load(connection.ExecuteSQLTable(command).Rows[0]);

            return c;
        }

        /// <summary>
        /// Loads the inventory of a character. Loads the default data from item configs, then adds sets the unique data from the database.
        /// </summary>
        /// <param name="character">Character object to load the inventory for.</param>
        public void LoadInventory(Character character)
        {

            try
            {
                SqlCommand command = new SqlCommand("SELECT [Index], BoundCharacterID, BoundName = ISNULL((SELECT Name FROM Characters WHERE CharacterID = [BoundCharacterID]), 'Unknown'), Count, ItemId, Endurance, Colour, SpecialEffect1, SpecialEffect2, SpecialEffect3, BagPositionX, BagPositionY, Stats, IsEquipped, Quality, Level, SetNumber, ColorType, ID FROM Inventory WHERE CharacterID = @CharacterID");
                command.Parameters.AddWithValue("@CharacterID", character.DatabaseID);
                DataTable itemList = connection.ExecuteSQLTable(command);


                if (itemList != null && itemList.Rows.Count > 0)
                    foreach (DataRow itemData in itemList.Rows)
                    {
                        Item item = World.ItemConfiguration[(int)itemData["ItemId"]].Copy();
                        item.BoundID = itemData["BoundCharacterID"].ToString();
                        item.BoundName = itemData["BoundName"].ToString();
                        item.Count = (int)itemData["Count"];
                        item.Endurance = (int)itemData["Endurance"];
                        item.Colour = (int)itemData["Colour"];
                        item.SpecialEffect1 = (int)itemData["SpecialEffect1"];
                        item.SpecialEffect2 = (int)itemData["SpecialEffect2"];
                        item.SpecialEffect3 = (int)itemData["SpecialEffect3"];
                        item.BagPosition = new Location((int)itemData["BagPositionX"], (int)itemData["BagPositionY"]);
                        item.IsEquipped = (bool)itemData["IsEquipped"];
                        item.Quality = (ItemQuality)(int)itemData["Quality"];
                        item.Level = (int)itemData["Level"];
                        item.SetNumber = (int)itemData["SetNumber"];
                        item.ColorType = (GameColor)itemData["ColorType"];
                        item.SpecificID = (Guid)itemData["ID"];


                        string[] stats = itemData["Stats"].ToString().Split(' ');
                        for (int i = 0; i < stats.Length/2; i++)
                            item.Stats.Add((ItemStat)Int32.Parse(stats[i * 2]), Int32.Parse(stats[(i * 2) + 1]));

                        character.Inventory[(int)itemData["Index"]] = item; // intenvoryV2 uses slot index so that we can rearrage bag freely

                        if (item.IsEquipped) //duplicate item into equipment slot
                        {
                            switch (item.EquipType)
                            {
                                case EquipType.DualHand: character.Inventory[(int)EquipType.RightHand] = item; break;
                                case EquipType.FullBody: character.Inventory[(int)EquipType.Body] = item; break;
                                default: character.Inventory[(int)item.EquipType] = item; break;
                            }
                        }    
                    }

                character.Inventory.Update();
                // cant have more than max. moved here because these are affected by inventory (needs to run after Update())
                if (character.HP > character.MaxHP) character.HP = character.MaxHP;
                if (character.MP > character.MaxMP) character.MP = character.MaxMP;
                if (character.SP > character.MaxSP) character.SP = character.MaxSP;
                if (character.Criticals > character.MaxCriticals) character.Criticals = character.MaxCriticals;
            }
            catch (Exception ex)
            {
                LogMessage("Error in loading inventory for (" + character.Name + ") - " + ex.Message);
            }
        }

        /// <summary>
        /// Loads the inventory of a character. Loads the default data from item configs, then adds sets the unique data from the database.
        /// </summary>
        /// <param name="character">Character object to load the inventory for.</param>
        public void LoadWarehouse(Character character)
        {
            try
            {
                SqlCommand command = new SqlCommand("SELECT [Index], BoundCharacterID, BoundName = (SELECT Name FROM Characters WHERE CharacterID = [BoundCharacterID]), Count, ItemId, Endurance, Colour, ColorType, SpecialEffect1, SpecialEffect2, SpecialEffect3, Stats, Quality, Level, ID FROM Warehouse WHERE CharacterID = @CharacterID ORDER BY [Index]");
                command.Parameters.AddWithValue("@CharacterID", character.DatabaseID);
                DataTable itemList = connection.ExecuteSQLTable(command);

                if (itemList != null && itemList.Rows.Count >0)
                    foreach (DataRow itemData in itemList.Rows)
                    {
                        Item item = World.ItemConfiguration[(int)itemData["ItemId"]].Copy();
                        item.BoundID = itemData["BoundCharacterID"].ToString();
                        item.BoundName = itemData["BoundName"].ToString();
                        item.Count = (int)itemData["Count"];
                        item.Endurance = (int)itemData["Endurance"];
                        item.Colour = (int)itemData["Colour"];
                        item.ColorType = (GameColor)itemData["ColorType"];
                        item.SpecialEffect1 = (int)itemData["SpecialEffect1"];
                        item.SpecialEffect2 = (int)itemData["SpecialEffect2"];
                        item.SpecialEffect3 = (int)itemData["SpecialEffect3"];
                        item.Quality = (ItemQuality)(int)itemData["Quality"];
                        item.Level = (int)itemData["Level"];
                        item.SpecificID = (Guid)itemData["ID"];

                        string[] stats = itemData["Stats"].ToString().Split(' ');
                        for (int i = 0; i < stats.Length / 2; i++)
                            item.Stats.Add((ItemStat)Int32.Parse(stats[i * 2]), Int32.Parse(stats[(i * 2) + 1]));

                        character.Warehouse[(int)itemData["Index"]] = item;
                    }
            }
            catch (Exception ex)
            {
                LogMessage("Error loading warehouse for (" + character.Name + ") - " + ex.Message);
            }
        }

        public void LoadTitles(Character character)
        {
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM CharacterTitles WHERE CharacterID = @CharacterID");
                command.Parameters.AddWithValue("@CharacterID", character.DatabaseID);

                DataTable titleList = connection.ExecuteSQLTable(command);

                if (titleList != null && titleList.Rows.Count > 0)
                {
                    DataRow title = titleList.Rows[0];
                    character.Titles[TitleType.DistanceTravelled] = (int)title["DistanceTravelled"];
                    character.Titles[TitleType.ItemsCollected] = (int)title["ItemsCollected"];
                    character.Titles[TitleType.RareItemsCollected] = (int)title["RareItemsCollected"];
                    character.Titles[TitleType.MonstersKilled] = (int)title["MonstersKilled"];
                    character.Titles[TitleType.PlayersKilled] = (int)title["PlayersKilled"];
                    character.Titles[TitleType.MonsterDeaths] = (int)title["MonsterDeaths"];
                    character.Titles[TitleType.PlayerDeaths] = (int)title["PlayerDeaths"];
                    character.Titles[TitleType.MonsterDamageDealt] = (int)title["MonsterDamageDealt"];
                    character.Titles[TitleType.PlayerDamageDealt] = (int)title["PlayerDamageDealt"];
                    character.Titles[TitleType.QuestsCompleted] = (int)title["QuestsCompleted"];
                    character.Titles[TitleType.HighestMagicDamage] = (int)title["HighestMagicDamage"];
                    character.Titles[TitleType.HighestMeleeDamage] = (int)title["HighestMeleeDamage"];
                    character.Titles[TitleType.HighestValueItem] = (int)title["HighestValueItem"];
                    character.Titles[TitleType.PublicEventsGold] = (int)title["PublicEventsGold"];
                    character.Titles[TitleType.PublicEventsSilver] = (int)title["PublicEventsSilver"];
                    character.Titles[TitleType.PublicEventsBronze] = (int)title["PublicEventsBronze"];

                }
                else
                {
                    // creates title list if doesnt exist
                    SqlCommand newTitles = new SqlCommand("INSERT INTO CharacterTitles (CharacterId) VALUES (@CharacterID);");
                    newTitles.Parameters.AddWithValue("@CharacterID", character.DatabaseID);
                    connection.ExecuteSQLNoReturn(newTitles);
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error loading titles for (" + character.Name + ") - " + ex.Message);
            }
        }

        public bool CharacterExists(string name)
        {
            SqlCommand command = new SqlCommand("SELECT Count(ID) FROM Characters WHERE lower(Name) = lower(@Name)");
            command.Parameters.AddWithValue("@Name", name);

            string value = connection.ExecuteSQLScalar(command);

            int intParser;
            if (Int32.TryParse(value, out intParser))
                if (intParser > 0) return true;
                else return false;
            else return false;
        }

        public bool DeleteCharacter(string accountID, string characterName)
        {
            SqlCommand command = new SqlCommand("DELETE FROM Characters WHERE Name = @Name AND AccountID = (SELECT ID FROM Accounts WHERE Name = @AccountName);");
            command.Parameters.AddWithValue("@Name", characterName);
            command.Parameters.AddWithValue("@AccountName", accountID);

            return connection.ExecuteSQLNoReturn(command);
        }

        public bool CreateCharacter(string accountID, Character character)
        {
            SqlCommand command = new SqlCommand("INSERT INTO Characters (ID, AccountID, Name, Profile, Level, Strength, Dexterity," +
                                                 "Vitality, Magic, Intelligence, Charisma, Experience, Gender, Skin, HairStyle, HairColour," + 
                                                 "UnderwearColour, Appearance1, Appearance2, Appearance3, Appearance4, AppearanceColour, " +
                                                 "Town, MapName, MapX, MapY, LastLogin, HP, MP, SP, Hunger, Criticals, Majestics, SpecialAbilityTime," +
                                                 "SkillStatus, SpellStatus, TotalLogins, ReputationTime, EnemyKills, CriminalCount, MuteTime) " +

                                                "VALUES (newid(), @AccountID, @Name, '', 1, @Strength, @Dexterity, @Vitality," + 
                                                "@Magic, @Intelligence, @Charisma, 0, @Gender, @Skin, @HairStyle, @HairColour," +
                                                "@UnderwearColour, @Appearance1, @Appearance2, @Appearance3, @Appearance4, @AppearanceColour," +
                                                "@Town, @MapName, @MapX, @MapY, GETDATE(), @HP, @MP, @SP, 100, 0, 0, 0," +
                                                "@SkillStatus, @SpellStatus, 0, 1200, 0, 0, 0)");
            command.Parameters.AddWithValue("@AccountID", accountID);
            command.Parameters.AddWithValue("@Name", character.Name);
            command.Parameters.AddWithValue("@Strength", character.Strength);
            command.Parameters.AddWithValue("@Dexterity", character.Dexterity);
            command.Parameters.AddWithValue("@Vitality", character.Vitality);
            command.Parameters.AddWithValue("@Magic", character.Magic);
            command.Parameters.AddWithValue("@Intelligence", character.Intelligence);
            command.Parameters.AddWithValue("@Charisma", character.Agility);
            command.Parameters.AddWithValue("@Gender", (int)character.Gender);
            command.Parameters.AddWithValue("@Skin", character.Skin);
            command.Parameters.AddWithValue("@HairStyle", character.HairStyle);
            command.Parameters.AddWithValue("@HairColour", character.HairColour);
            command.Parameters.AddWithValue("@UnderwearColour", character.UnderwearColour);
            command.Parameters.AddWithValue("@Appearance1", character.Appearance1);
            command.Parameters.AddWithValue("@Appearance2", character.Appearance2);
            command.Parameters.AddWithValue("@Appearance3", character.Appearance3);
            command.Parameters.AddWithValue("@Appearance4", character.Appearance4);
            command.Parameters.AddWithValue("@AppearanceColour", character.AppearanceColour);
            command.Parameters.AddWithValue("@Town", character.Town);
            command.Parameters.AddWithValue("@HP", character.MaxHP);
            command.Parameters.AddWithValue("@MP", character.MaxMP);
            command.Parameters.AddWithValue("@SP", character.MaxSP);
            command.Parameters.AddWithValue("@SkillStatus", "101 101 101 101 101 101 101 101 101 101 101 101 101 101 101 101 0 0 0 101 0 101 0 101 101 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0");
            command.Parameters.AddWithValue("@SpellStatus", character.SpellStatus); //"1110000000111110000011111111101111111111111111111111011111111111111111111111111111110000001110111000"
            command.Parameters.AddWithValue("@MapName", "cityhall_2");
            command.Parameters.AddWithValue("@MapX", 40);
            command.Parameters.AddWithValue("@MapY", 50);

            if (connection.ExecuteSQLNoReturn(command)) return true;
            else return false;
        }

        public bool SaveCharacter(Character character)
        {
            SqlCommand command = new SqlCommand("UPDATE Characters SET Name = @Name, Profile = @Profile, Town = @Town, Level = @Level," +
                                                "Strength = @Strength, Dexterity = @Dexterity, Vitality = @Vitality, Magic = @Magic," +
                                                "Intelligence = @Intelligence, Charisma = @Charisma, Experience = @Experience," +
                                                "Gender = @Gender, Skin = @Skin, HairStyle = @HairStyle, HairColour = @HairColour, " +
                                                "UnderwearColour = @UnderwearColour, Appearance1 = @Appearance1, Appearance2 = @Appearance2," +
                                                "Appearance3 = @Appearance3, Appearance4 = @Appearance4, AppearanceColour = @AppearanceColour," +
                                                "MapName = @MapName, MapX = @MapX, MapY = @MapY, HP = @HP, MP = @MP, SP = @SP, " +
                                                "Hunger = @Hunger, Criticals = @Criticals, Majestics = @Majestics, SpecialAbilityTime = @SpecialAbilityTime, SkillStatus = @SkillStatus, SpellStatus = @SpellStatus, " +
                                                "ReputationTime = @ReputationTime, TotalLogins = @TotalLogins, EnemyKills = @EnemyKills, CriminalCount = @CriminalCount, " +
                                                "Guild = @Guild, GuildRank = @GuildRank, MuteTime = @MuteTime, Reputation = @Reputation, Contribution = @Contribution, Gold = @Gold, " +
                                                "RebirthLevel = @RebirthLevel, GladiatorPoints = @GladiatorPoints " +
                                                "WHERE ID = @ID");
            command.Parameters.AddWithValue("@ID", character.DatabaseID);
            command.Parameters.AddWithValue("@Name", character.Name);
            command.Parameters.AddWithValue("@Profile", character.Profile);
            command.Parameters.AddWithValue("@Town", (int)character.Town);
            command.Parameters.AddWithValue("@Level", character.Level);
            command.Parameters.AddWithValue("@Strength", character.Strength);
            command.Parameters.AddWithValue("@Dexterity", character.Dexterity);
            command.Parameters.AddWithValue("@Vitality", character.Vitality);
            command.Parameters.AddWithValue("@Magic", character.Magic);
            command.Parameters.AddWithValue("@Intelligence", character.Intelligence);
            command.Parameters.AddWithValue("@Charisma", character.Agility);
            command.Parameters.AddWithValue("@Experience", character.Experience);
            command.Parameters.AddWithValue("@Gender", (int)character.Gender);
            command.Parameters.AddWithValue("@Skin", (int)character.Skin);
            command.Parameters.AddWithValue("@HairStyle", character.HairStyle);
            command.Parameters.AddWithValue("@HairColour", character.HairColour);
            command.Parameters.AddWithValue("@UnderwearColour", character.UnderwearColour);
            command.Parameters.AddWithValue("@Appearance1", character.Appearance1);
            command.Parameters.AddWithValue("@Appearance2", character.Appearance2);
            command.Parameters.AddWithValue("@Appearance3", character.Appearance3);
            command.Parameters.AddWithValue("@Appearance4", character.Appearance4);
            command.Parameters.AddWithValue("@AppearanceColour", character.AppearanceColour);
            command.Parameters.AddWithValue("@MapName", character.CurrentMap.Name);
            command.Parameters.AddWithValue("@MapX", character.X);
            command.Parameters.AddWithValue("@MapY", character.Y);
            command.Parameters.AddWithValue("@HP", character.HP);
            command.Parameters.AddWithValue("@MP", character.MP);
            command.Parameters.AddWithValue("@SP", character.SP);
            command.Parameters.AddWithValue("@Hunger", character.Hunger);
            command.Parameters.AddWithValue("@Criticals", character.Criticals);
            command.Parameters.AddWithValue("@Majestics", character.Majestics);
            command.Parameters.AddWithValue("@SpecialAbilityTime", character.SpecialAbilityTime.TotalSeconds);
            command.Parameters.AddWithValue("@SkillStatus", character.SkillStatus);
            command.Parameters.AddWithValue("@SpellStatus", character.SpellStatus);
            command.Parameters.AddWithValue("@ReputationTime", character.ReputationTime.TotalSeconds);
            command.Parameters.AddWithValue("@TotalLogins", character.TotalLogins);
            command.Parameters.AddWithValue("@EnemyKills", character.EnemyKills);
            command.Parameters.AddWithValue("@CriminalCount", character.CriminalCount);
            command.Parameters.AddWithValue("@Guild", character.Guild.Name);
            command.Parameters.AddWithValue("@GuildRank", character.GuildRank);
            command.Parameters.AddWithValue("@MuteTime", character.MuteTime.TotalSeconds);
            command.Parameters.AddWithValue("@Contribution", character.CrusadeWarContribution);
            command.Parameters.AddWithValue("@Reputation", character.Reputation);
            command.Parameters.AddWithValue("@Gold", character.Gold);
            command.Parameters.AddWithValue("@RebirthLevel", character.RebirthLevel);
            command.Parameters.AddWithValue("@GladiatorPoints", character.GladiatorPoints);

            if (connection.ExecuteSQLNoReturn(command))
            {
                SaveTitles(character);

                ClearInventory(character.DatabaseID);
                if (character.InventoryCount > 0)
                    SaveInventory(character.DatabaseID, character.Inventory.List);

                ClearWarehouse(character.DatabaseID);
                if (character.WarehouseCount > 0)
                    SaveWarehouse(character.DatabaseID, character.Warehouse);

                return true;
            }
            else return false;
        }

        public void SaveTitles(Character character)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE CharacterTitles SET DistanceTravelled = @DistanceTravelled, ItemsCollected = @ItemsCollected, RareItemsCollected = @RareItemsCollected, MonstersKilled = @MonstersKilled, PlayersKilled=@PlayersKilled, MonsterDeaths = @MonsterDeaths, PlayerDeaths = @PlayerDeaths, MonsterDamageDealt = @MonsterDamageDealt, PlayerDamageDealt = @PlayerDamageDealt, QuestsCompleted = @QuestsCompleted, HighestMeleeDamage = @HighestMeleeDamage, HighestMagicDamage = @HighestMagicDamage, HighestValueItem = @HighestValueItem, PublicEventsGold = @PublicEventsGold, PublicEventsSilver = @PublicEventsSilver, PublicEventsBronze = @PublicEventsBronze WHERE CharacterID = @CharacterID;");
                command.Parameters.AddWithValue("@CharacterID", character.DatabaseID);
                command.Parameters.AddWithValue("@DistanceTravelled", character.Titles[TitleType.DistanceTravelled]);
                command.Parameters.AddWithValue("@ItemsCollected", character.Titles[TitleType.ItemsCollected]);
                command.Parameters.AddWithValue("@RareItemsCollected", character.Titles[TitleType.RareItemsCollected]);
                command.Parameters.AddWithValue("@MonstersKilled", character.Titles[TitleType.MonstersKilled]);
                command.Parameters.AddWithValue("@PlayersKilled", character.Titles[TitleType.PlayersKilled]);
                command.Parameters.AddWithValue("@MonsterDeaths", character.Titles[TitleType.MonsterDeaths]);
                command.Parameters.AddWithValue("@PlayerDeaths", character.Titles[TitleType.PlayerDeaths]);
                command.Parameters.AddWithValue("@MonsterDamageDealt", character.Titles[TitleType.MonsterDamageDealt]);
                command.Parameters.AddWithValue("@PlayerDamageDealt", character.Titles[TitleType.PlayerDamageDealt]);
                command.Parameters.AddWithValue("@QuestsCompleted", character.Titles[TitleType.QuestsCompleted]);
                command.Parameters.AddWithValue("@HighestMeleeDamage", character.Titles[TitleType.HighestMeleeDamage]);
                command.Parameters.AddWithValue("@HighestMagicDamage", character.Titles[TitleType.HighestMagicDamage]);
                command.Parameters.AddWithValue("@HighestValueItem", character.Titles[TitleType.HighestValueItem]);
                command.Parameters.AddWithValue("@PublicEventsGold", character.Titles[TitleType.PublicEventsGold]);
                command.Parameters.AddWithValue("@PublicEventsSilver", character.Titles[TitleType.PublicEventsSilver]);
                command.Parameters.AddWithValue("@PublicEventsBronze", character.Titles[TitleType.PublicEventsBronze]);
                connection.ExecuteSQLNoReturn(command);
            }
            catch (Exception ex)
            {
                LogMessage("Error in Save Titles: " + ex.ToString());
            }
        }

        public void ClearInventory(string characterID)
        {
            SqlCommand command = new SqlCommand("DELETE FROM Inventory WHERE CharacterID = @CharacterID");
            command.Parameters.AddWithValue("@CharacterID", characterID);
            connection.ExecuteSQLNoReturn(command);
        }

        public bool SaveInventory(string characterID, List<Item> items)
        {
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append("INSERT INTO Inventory (ID, [Index], CharacterID, BoundCharacterID, Count, ItemId, Endurance, Colour, SpecialEffect1, SpecialEffect2, SpecialEffect3, BagPositionX, BagPositionY, Stats, IsEquipped, Quality, Level, SetNumber, ColorType)");

                Item item;
                for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++) //Only go through inventory items so no duplicates
                {
                    if (items[i] != null)

                    {
                        item = items[i];

                        query.Append(" SELECT ");
                        query.Append("'" + item.SpecificID.ToString() + "', ");
                        query.Append(i + ", '");
                        query.Append(characterID + "', ");
                        query.Append("'" + item.BoundID + "', ");
                        query.Append(item.Count + ", '");
                        query.Append(item.ItemId + "', ");
                        query.Append(item.Endurance + ", ");
                        query.Append(item.Colour + ", ");
                        query.Append(item.SpecialEffect1 + ", ");
                        query.Append(item.SpecialEffect2 + ", ");
                        query.Append(item.SpecialEffect3 + ", ");
                        query.Append(item.BagPosition.X + ", ");
                        query.Append(item.BagPosition.Y + ", ");
                        string stats = string.Empty;
                        foreach (KeyValuePair<ItemStat, int> stat in item.Stats)
                            stats += (int)stat.Key + " " + stat.Value + " ";
                        stats = stats.Trim();
                        query.Append("'" + stats + "', ");
                        query.Append(((item.IsEquipped) ? 1 : 0) + ", ");
                        query.Append((int)item.Quality + ", ");
                        query.Append((int)item.Level + ", ");
                        query.Append((int)item.SetNumber + ", ");
                        query.Append((int)item.ColorType);


                        if (i != Globals.MaximumTotalItems - 1) // dont do this on last entry otherwise syntax error
                            query.Append(" UNION ALL ");
                    }
                }
                string q = query.ToString();
                if (q.EndsWith("UNION ALL ")) q = q.Remove(q.LastIndexOf("UNION"));

                SqlCommand command = new SqlCommand(q);
                connection.ExecuteSQLNoReturn(command);

                return true;
            }
            catch (Exception ex)
            {
                LogMessage("Error in Save Inventory: " + ex.ToString());
                return false;
            }
        }

        public void ClearWarehouse(string characterID) //TODO switch this to used AccountID so all characters share the same warehouse
        {
            SqlCommand command = new SqlCommand("DELETE FROM Warehouse WHERE CharacterID = @CharacterID");
            command.Parameters.AddWithValue("@CharacterID", characterID);
            connection.ExecuteSQLNoReturn(command);
        }

        public bool SaveWarehouse(string characterID, List<Item> items)
        {
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append("INSERT INTO Warehouse (ID, [Index], CharacterID, BoundCharacterID, Count, ItemId, Endurance, Colour, SpecialEffect1, SpecialEffect2, SpecialEffect3, Stats, Quality, Level, ColorType)");

                int index = 0; // used to keep correct order of items.
                foreach (Item item in items)
                {
                    if (item != null)
                    {
                        query.Append(" SELECT ");
                        query.Append("'" + item.SpecificID.ToString() + "', ");
                        query.Append(index + ", '");
                        query.Append(characterID + "', ");
                        query.Append("'" + item.BoundID + "', ");
                        query.Append(item.Count + ", '");
                        query.Append(item.ItemId + "', ");
                        query.Append(item.Endurance + ", ");
                        query.Append(item.Colour + ", ");
                        query.Append(item.SpecialEffect1 + ", ");
                        query.Append(item.SpecialEffect2 + ", ");
                        query.Append(item.SpecialEffect3 + ", ");
                        string stats = string.Empty;
                        foreach (KeyValuePair<ItemStat, int> stat in item.Stats)
                            stats += (int)stat.Key + " " + stat.Value + " ";
                        stats = stats.Trim();
                        query.Append("'" + stats + "', ");
                        query.Append((int)item.Quality + ", ");
                        query.Append((int)item.Level + ", ");
                        query.Append((int)item.ColorType);

                        if (index != items.Count) // dont do this on last entry otherwise syntax error
                            query.Append(" UNION ALL ");
                    }
                    index++;
                }
                string q = query.ToString();
                if (q.EndsWith("UNION ALL ")) q = q.Remove(q.LastIndexOf("UNION"));

                SqlCommand command = new SqlCommand(q);
                connection.ExecuteSQLNoReturn(command);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateLastLogin(string characterName)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE Characters SET LastLogin = GETDATE() WHERE Name = @CharacterName");
                command.Parameters.AddWithValue("@CharacterName", characterName);
                connection.ExecuteSQLNoReturn(command);

                return true;
            }
            catch
            {
                return false;
            }
        }

        #region LOGGING
        public void LogMessage(string message)
        {
            if (MessageLogged != null) MessageLogged(message);
        }
        #endregion
    }
}
