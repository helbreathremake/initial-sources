﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace HelbreathWorld.Game
{
    public class PackagedFile
    {
        public bool DataLoaded;
        public string Name;
        public int Length;
        public byte[] Data;
    }

    public class PakFile
    {
        public delegate void PakFileHandler();
        public event PakFileHandler FileChanged;

        private string fileName;
        private Dictionary<int, PackagedFile> files;

        public Dictionary<int, PackagedFile> FileList { get { return files; } }

        public PakFile()
        {
            this.fileName = "Untitled.pak";
            files = new Dictionary<int, PackagedFile>();
        }

        public PakFile(string fileName)
        {
            this.fileName = fileName;

            files = new Dictionary<int, PackagedFile>();
        }

        public bool Load()
        {
            try
            {
                byte[] data = File.ReadAllBytes(fileName);

                // disable slow GZip
                /*MemoryStream outStream = new MemoryStream();
                using (MemoryStream stream = new MemoryStream(data))
                using (GZipStream zipStream = new GZipStream(stream, CompressionMode.Decompress))
                {
                    byte[] buffer = new byte[0x400];
                    int count = zipStream.Read(buffer, 0, buffer.Length);
                    while (count != 0)
                    {
                        outStream.Write(buffer, 0, count);
                        count = zipStream.Read(buffer, 0, buffer.Length);
                    }

                    data = new byte[data.Length];
                    zipStream.Read(data, 0, data.Length);
                }
                data = outStream.ToArray();*/

                int size = 0;
                int fileCount = BitConverter.ToInt16(data, size);
                size+=2;

                for (int i = 0; i < fileCount; i++)
                {
                    PackagedFile file = new PackagedFile();
                    file.Name = Encoding.UTF8.GetString(data, size, 50).Trim().Replace("\0", "");
                    size += 50;
                    file.Length = BitConverter.ToInt32(data, size);
                    size += 4;
                    file.Data = new byte[file.Length];
                    files.Add(i, file);
                }

                for (int i = 0; i < fileCount; i++)
                {
                    Buffer.BlockCopy(data, size, files[i].Data, 0, files[i].Length);
                    size += files[i].Length;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Save() { return SaveAs(this.fileName); }
        public bool SaveAs(string fileName)
        {
            try
            {
                long maxSize = 0;
                foreach (PackagedFile file in files.Values) maxSize += file.Length;

                int size = 0;
                long fileSize = maxSize + 2 + (files.Count * 54);
                byte[] buffer = new byte[fileSize];
                

                Buffer.BlockCopy(BitConverter.GetBytes((short)files.Count), 0, buffer, size, 2);
                size+=2;

                foreach (PackagedFile file in files.Values)
                {
                    Buffer.BlockCopy(file.Name.GetBytes(50), 0, buffer, size, 50);
                    size += 50;
                    Buffer.BlockCopy(BitConverter.GetBytes((int)file.Length), 0, buffer, size, 4);
                    size += 4;
                }

                foreach (PackagedFile file in files.Values)
                {
                    Buffer.BlockCopy(file.Data, 0, buffer, size, file.Length);
                    size += file.Length;
                }

                File.WriteAllBytes(fileName, buffer);

                /* disable slow GZip
                using (MemoryStream inFile = new MemoryStream(buffer))
                {
                    using (FileStream outFile = File.Create(fileName))
                    {
                        using (GZipStream Compress = new GZipStream(outFile,
                                CompressionMode.Compress))
                        {

                            byte[] buff = new byte[409600];
                            int numRead;
                            while ((numRead = inFile.Read(buff, 0, buff.Length)) != 0)
                            {
                                Compress.Write(buff, 0, numRead);
                            }
                        }
                    }
                } */

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AddFile(string fileName)
        {
            try
            {
                PackagedFile file = new PackagedFile();
                file.Name = fileName.Substring(fileName.LastIndexOf('\\') + 1);
                file.Length = (int)new FileInfo(fileName).Length;
                file.Data = File.ReadAllBytes(fileName);

                files.Add(files.Count, file);

                if (FileChanged != null) FileChanged();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void SortFilesByName()
        {
            List<KeyValuePair<int, PackagedFile>> results =
                new List<KeyValuePair<int, PackagedFile>>(files);
            results.Sort(
              delegate(
                KeyValuePair<int, PackagedFile> first,
                KeyValuePair<int, PackagedFile> second)
                  {
                      return first.Value.Name.CompareTo(second.Value.Name);
                  }
              );
            
            files = new Dictionary<int,PackagedFile>();
            foreach (KeyValuePair<int, PackagedFile> resukt in results)
                files.Add(resukt.Key, resukt.Value);

            if (FileChanged != null) FileChanged();
        }

        public bool RemoveFile(int index)
        {
            files.Remove(index);

            if (FileChanged != null) FileChanged();

            return true;
        }

        public Dictionary<int, PackagedFile>.ValueCollection Files
        {
            get { return files.Values; }
        }

        public PackagedFile this[string fileName]
        {
            get
            {
                foreach (PackagedFile file in files.Values)
                    if (file.Name.Equals(fileName)) return file;
                return null;
            }
        }

        public String Name
        {
            set { this.fileName = value; }
            get
            {
                if (this.fileName.Contains('\\'))
                    return this.fileName.Substring(fileName.LastIndexOf('\\')+1);
                else return this.fileName;
            }
        }
    }
}
