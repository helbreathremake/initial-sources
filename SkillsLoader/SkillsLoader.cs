﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using HelbreathWorld;
using HelbreathWorld.Common.Assets;

namespace SkillsLoader
{
    public partial class SkillsLoader : Form
    {
        public static Dictionary<int, Item> ItemConfiguration;
        private List<AlchemyItem> alchemyConfig;
        private List<ManufactureItem> manuConfig;
        private List<CraftingItem> craftConfig;

        public SkillsLoader()
        {
            InitializeComponent();

            ItemConfiguration = new Dictionary<int, Item>();

            alchemyConfig = new List<AlchemyItem>();
            manuConfig = new List<ManufactureItem>();
            craftConfig = new List<CraftingItem>();

            dataGridView1.Columns.Add("ItemName", "ItemName");
            dataGridView1.Columns.Add("SkillType", "SkillType");
            dataGridView1.Columns.Add("SkillLimit", "SkillLimit");
            dataGridView1.Columns.Add("Difficulty", "Difficulty");
            dataGridView1.Columns.Add("Ingrediants", "Ingrediants");
        }

        private void ItemOpenFile(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
        }

        private void LoadItemList(object sender, CancelEventArgs e)
        {
            try
            {
                string filename = openFileDialog2.FileName;

                StreamReader file = new StreamReader(filename);

                string line;
                int itemId = 1;
                while ((line = file.ReadLine()) != null)
                {
                    if (line.StartsWith("Item"))
                    {
                        int index;
                        string[] tokens = line.Replace('\t', ' ').Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if (tokens.Length < 4) continue;
                        Item item = new Item();
                        index = Int32.Parse(tokens[2]);
                        item.ItemId = itemId;
                        item.Type = (ItemType)Convert.ToInt32(tokens[4]);
                        item.EquipType = (EquipType)Convert.ToInt32(tokens[5]);
                        item.EffectType = (ItemEffectType)Convert.ToInt32(tokens[6]);
                        item.Effect1 = Convert.ToInt32(tokens[7]);
                        item.Effect2 = Convert.ToInt32(tokens[8]);
                        item.Effect3 = Convert.ToInt32(tokens[9]);
                        item.Effect4 = Convert.ToInt32(tokens[10]);
                        item.Effect5 = Convert.ToInt32(tokens[11]);
                        item.Effect6 = Convert.ToInt32(tokens[12]);
                        item.BaseMaximumEndurance = Convert.ToInt32(tokens[13]);
                        if (tokens[3].Equals("DemonSlayer"))
                            item.SpecialAbilityType = ItemSpecialAbilityType.DemonSlayer;
                        else if (tokens[3].Contains("Kloness"))
                            item.SpecialAbilityType = ItemSpecialAbilityType.Kloness;
                        else if (tokens[3].Equals("DarkExecutor"))
                            item.SpecialAbilityType = ItemSpecialAbilityType.Dark;
                        else if (tokens[3].Equals("LightingBlade"))
                            item.SpecialAbilityType = ItemSpecialAbilityType.Light;
                        else if (tokens[3].StartsWith("Berserk"))
                            item.SpecialAbilityType = ItemSpecialAbilityType.Berserk;
                        else item.SpecialAbilityType = (ItemSpecialAbilityType)Convert.ToInt32(tokens[14]);
                        if (item.EffectType == ItemEffectType.AttackMaxHPDown)
                        {
                            item.EffectType = ItemEffectType.Attack;
                            item.SpecialAbilityType = ItemSpecialAbilityType.Blood;
                        }
                        item.Sprite = Convert.ToInt32(tokens[15]);
                        item.SpriteFrame = Convert.ToInt32(tokens[16]);
                        item.Price = Math.Abs(Convert.ToInt32(tokens[17]));
                        item.Weight = Convert.ToInt32(tokens[18]);
                        item.Appearance = Convert.ToInt32(tokens[19]);
                        item.Speed = Convert.ToInt32(tokens[20]);
                        item.LevelLimit = Convert.ToInt32(tokens[21]);
                        item.GenderLimit = (GenderType)Convert.ToInt32(tokens[22]);
                        item.SpecialEffect1 = Convert.ToInt32(tokens[23]);
                        item.SpecialEffect2 = Convert.ToInt32(tokens[24]);
                        switch (Convert.ToInt32(tokens[25]))
                        {
                            case 0:
                                if (tokens[3].Equals("PickAxe"))
                                    item.RelatedSkill = SkillType.Mining;
                                else if (tokens[3].Equals("Hoe"))
                                    item.RelatedSkill = SkillType.Farming;
                                else item.RelatedSkill = SkillType.None;
                                break;
                            default: item.RelatedSkill = (SkillType)Convert.ToInt32(tokens[25]); break;
                        }
                        //item.Category = Convert.ToInt32(tokens[26]);
                        //item.Colour = Convert.ToInt32(tokens[27]);
                        item.Rarity = GetRarity(tokens[3]);
                        item.IsForSale = (Convert.ToInt32(tokens[17]) > 0);

                        ItemConfiguration.Add(index, item);

                        itemId++;
                    }
                }


                file.Close();

                label1.Text = "Items Loaded: " + SkillsLoader.ItemConfiguration.Count;
                if (SkillsLoader.ItemConfiguration.Count > 0) button1.Enabled = button2.Enabled = button3.Enabled = true;
                return;
            }
            catch
            {
                return;
            }
        }

        private ItemRarity GetRarity(string itemName)
        {
            switch (itemName)
            {
                case "Gold":
                case "GreenPotion":
                case "RedPotion":
                case "BluePotion":
                case "BigGreenPotion":
                case "BigRedPotion":
                case "BigBluePotion":
                case "RedCandy":
                case "BlueCandy":
                case "GreenCandy":
                case "Dagger":
                case "ShortSword":
                case "LightAxe":
                case "Rapier":
                case "MagicWand(MS0)":
                case "WoodShield":
                case "LeatherArmor(M)":
                case "LeatherArmor(W)":
                    return ItemRarity.VeryCommon;
                case "PowerGreenPotion":
                case "MainGauche":
                case "SexonAxe":
                case "Tomahoc":
                case "Sabre":
                case "Esterk":
                case "Scimitar":
                case "Falchion":
                case "MagicWand(MS10)":
                case "TargeShield":
                case "ScaleMail(M)":
                case "ScaleMail(W)":
                case "SlimeJelly":
                case "AntLeg":
                case "AntFeeler":
                case "SnakeMeat":
                case "SnakeSkin":
                case "SnakeTeeth":
                case "SnakeTongue":
                case "OrcMeat":
                case "OrcLeather":
                case "OrcTeeth":
                case "ScorpionPincers":
                case "ScorpionMeat":
                case "ScorpionSting":
                case "ScorpionSkin":
                case "SkeletonBones":
                case "LumpOfCalay":
                case "StoneGolemPiece":
                    return ItemRarity.Common;
                case "LongSword":
                case "DoubleAxe":
                case "GreatSword":
                case "Claymore":
                case "MagicWand(MS20)":
                case "Hauberk(M)":
                case "Hauberk(W)":
                case "ChainHose(M)":
                case "ChainHose(W)":
                case "BlondeShield":
                case "IronShield":
                case "ChainMail(M)":
                case "ChainMail(W)":
                case "LagiShield":
                case "Wizard-Cap(M)":
                case "Wizard-Cap(W)":
                case "Helm(M)":
                case "Helm(W)":
                case "HelboundHeart":
                case "HelboundLeather":
                case "HelboundTail":
                case "HelboundTeeth":
                case "HelboundClaw":
                case "HelboundTongue":
                case "CyclopsEye":
                case "CyclopsHandEdge":
                case "CyclopsHeart":
                case "CyclopsMeat":
                case "CyclopsLeather":
                case "TrollHeart":
                case "TrollMeat":
                case "TrollLeather":
                case "TrollClaw":
                case "OgreHair":
                case "OgreHeart":
                case "OgreMeat":
                case "OgreLeather":
                case "OgreTeeth":
                case "OgreClaw":
                    return ItemRarity.Uncommon;
                case "AcientTablet":
                case "AcientTablet(LU)":
                case "AcientTablet(LD)":
                case "AcientTablet(RU)":
                case "AcientTablet(RD)":
                case "StoneOfXelima":
                case "StoneOfMerien":
                case "ZemstoneofSacrifice":
                case "WarAxe":
                case "Flameberge":
                case "Hammer":
                case "PlateMail(M)":
                case "PlateMail(W)":
                case "Wings-Helm(M)":
                case "Wings-Helm(W)":
                case "Wizard-Hat(M)":
                case "Wizard-Hat(W)":
                case "KnightShield":
                case "TowerShield":
                case "Full-Helm(M)":
                case "Full-Helm(W)":
                case "Cape":
                case "MagicNecklace(RM10)":
                case "MagicWand(M.Shield)":
                case "MagicNecklace(MS10)":
                case "MagicNecklace(DF+10)":
                case "MagicNecklace(DM+1)":
                case "Flameberge+3(LLF)":
                case "BloodRapier":
                case "BloodSword":
                case "BloodAxe":
                case "MagicWand(MS30-LLF)":
                case "LuckyGoldRing":
                case "SapphireRing":
                case "PlatinumRing":
                case "RingofOgrePower":
                case "ResurWand(MS.10)":
                case "BloodyShockW.Manual":
                case "DarkElf-Bow":
                case "DemonHeart":
                case "DemonMeat":
                case "DemonLeather":
                case "DemonEye":
                case "UnicornHeart":
                case "UnicornHorn":
                case "UnicornMeat":
                case "UnicornLeather":
                case "WerewolfTail":
                case "WerewolfHeart":
                case "WerewolfMeat":
                case "WerewolfLeather":
                case "WerewolfTeeth":
                case "WerewolfClaw":
                case "WerewolfNail":
                    return ItemRarity.Rare;
                case "BattleAxe":
                case "Flameberge+1":
                case "GiantSword":
                case "GiantHammer":
                case "Horned-Helm(M)":
                case "Horned-Helm(W)":
                case "RubyRing":
                case "EmeraldRing":
                case "RingofWizard":
                case "RingofMage":
                case "KnecklaceOfIcePro":
                case "KnecklaceOfLightPro":
                case "KnecklaceOfFirePro":
                case "KnecklaceOfPoisonPro":
                case "SwordofIceElemental":
                case "RingofGrandMage":
                case "RingofDemonPower":
                case "Excaliber":
                case "KnecklaceOfStoneGol":
                case "LightingBlade":
                case "4BladeGoldenAxe(LLF)":
                case "IceStormManual":
                case "NecklaceOfLiche":
                case "ResurWand(MS.20)":
                case "MassFireStrikeManual":
                case "DemonSlayer":
                case "BerserkWand(MS.10)":
                case "RingofDragonpower":
                case "DarkExecutor":
                case "GiantBattleHammer":
                    return ItemRarity.VeryRare;
                case "KnecklaceOfSufferent":
                case "KnecklaceOfAirEle":
                case "MerienShield":
                case "KneclaceOfIceEle":
                case "RingoftheXelima":
                case "XelimaRapier":
                case "XelimaBlade":
                case "XelimaAxe":
                case "CancelManual":
                case "SwordOfMedusa":
                case "KnecklaceOfMedusa":
                case "MerienPlateMailM":
                case "MerienPlateMailW":
                case "KneclaceOfMerien":
                case "E.S.W.Manual":
                case "KlonessBlade":
                case "KlonessAxe":
                case "KlonessEsterk":
                case "NecklaceOfKloness":
                case "KlonessWand(MS.20)":
                case "KlonessWand(MS.10)":
                case "RingofArcMage":
                case "BerserkWand(MS.20)":
                case "KnecklaceOfXelima":
                case "RingoftheAbaddon":
                case "I.M.CManual":
                case "StormBringer":
                    return ItemRarity.UltraRare;
                default: return ItemRarity.None;
            }
        }

        private void BuildItemOpenFile(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void LoadBuildItemFile(object sender, CancelEventArgs e)
        {
            string filename = openFileDialog1.FileName;

            StreamReader file = new StreamReader(filename);

            string line;
            while ((line = file.ReadLine()) != null)
            {
                if (line.StartsWith("BuildItem"))
                {
                    string[] tokens = line.Replace('\t', ' ').Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens.Length < 4) continue;
                    ManufactureItem item = new ManufactureItem();
                    item.Name = tokens[2];
                    item.SkillLimit = Int32.Parse(tokens[3]);
                    item.Difficulty = Int32.Parse(tokens[23]);

                    // TODO - fix for itemName to itemId change
                    /*for (int i = 4; i < 22; i += 3)
                        if (Int32.Parse(tokens[i]) != -1 && Int32.Parse(tokens[i + 1]) > 0)
                            if (SkillsLoader.ItemConfiguration.ContainsKey(Int32.Parse(tokens[i])))
                            {
                                item.MaximumPurity += Int32.Parse(tokens[i + 2])*100;
                                if (item.Ingredients.ContainsKey(SkillsLoader.ItemConfiguration[Int32.Parse(tokens[i])].Name))
                                    item.Ingredients[SkillsLoader.ItemConfiguration[Int32.Parse(tokens[i])].Name] += Int32.Parse(tokens[i + 1]);
                                else
                                    item.Ingredients.Add(SkillsLoader.ItemConfiguration[Int32.Parse(tokens[i])].Name, Int32.Parse(tokens[i + 1]));
                            }
                     */

                    manuConfig.Add(item);

                    dataGridView1.Rows.Add(
                        new object[]
                    {
                        item.Name,
                        SkillType.Manufacturing,
                        item.SkillLimit,
                        item.Difficulty,
                        item.Ingredients.Count
                    }
                        );
                }
            }

            file.Close();

        }

        private void LoadPotionFile(object sender, CancelEventArgs e)
        {
            try
            {
                string filename = openFileDialog3.FileName;

                StreamReader file = new StreamReader(filename);

                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (line.StartsWith("potion"))
                    {
                        string[] tokens = line.Replace('\t', ' ').Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if (tokens.Length < 4) continue;
                        AlchemyItem item = new AlchemyItem();
                        item.Name = tokens[3];
                        item.SkillLimit = Int32.Parse(tokens[16]);
                        item.Difficulty = Int32.Parse(tokens[17]);
                        // TODO fix for itemName changed to itemId
                        /*for (int i = 4; i < 16; i += 2)
                            if (Int32.Parse(tokens[i]) != -1 && Int32.Parse(tokens[i + 1]) > 0)
                                if (SkillsLoader.ItemConfiguration.ContainsKey(Int32.Parse(tokens[i])))
                                    if (item.Ingredients.ContainsKey(SkillsLoader.ItemConfiguration[Int32.Parse(tokens[i])].ItemId))
                                        item.Ingredients[SkillsLoader.ItemConfiguration[Int32.Parse(tokens[i])].ItemId] += Int32.Parse(tokens[i + 1]);
                                    else
                                        item.Ingredients.Add(SkillsLoader.ItemConfiguration[Int32.Parse(tokens[i])].ItemId, Int32.Parse(tokens[i + 1]));
                         */

                        alchemyConfig.Add(item);

                        dataGridView1.Rows.Add(
                            new object[]
                    {
                        item.Name,
                        SkillType.Alchemy,
                        item.SkillLimit,
                        item.Difficulty,
                        item.Ingredients.Count
                    }
                            );
                    }
                }

                file.Close();
            }
            catch (Exception ex)
            {

            }
        }

        private void PotionOpenFile(object sender, EventArgs e)
        {
            openFileDialog3.ShowDialog();
        }

        private void CraftingOpenFile(object sender, EventArgs e)
        {
            openFileDialog4.ShowDialog();
        }

        private void LoadCraftingFile(object sender, CancelEventArgs e)
        {
            try
            {
                string filename = openFileDialog4.FileName;

                StreamReader file = new StreamReader(filename);

                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (line.StartsWith("crafting"))
                    {
                        string[] tokens = line.Replace('\t', ' ').Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if (tokens.Length < 4) continue;
                        CraftingItem item = new CraftingItem();
                        item.Name = tokens[3];
                        item.SkillLimit = Int32.Parse(tokens[16]);
                        item.Difficulty = Int32.Parse(tokens[17]);
                        // TODO - fix for itemName changed to itemId
                        /*for (int i = 4; i < 16; i += 2)
                            if (Int32.Parse(tokens[i]) != -1 && Int32.Parse(tokens[i + 1]) > 0)
                                if (SkillsLoader.ItemConfiguration.ContainsKey(Int32.Parse(tokens[i])))
                                    if (item.Ingredients.ContainsKey(SkillsLoader.ItemConfiguration[Int32.Parse(tokens[i])].ItemId))
                                        item.Ingredients[SkillsLoader.ItemConfiguration[Int32.Parse(tokens[i])].ItemId] += Int32.Parse(tokens[i + 1]);
                                    else
                                        item.Ingredients.Add(SkillsLoader.ItemConfiguration[Int32.Parse(tokens[i])].ItemId, Int32.Parse(tokens[i + 1]));
                         * */

                        craftConfig.Add(item);

                        dataGridView1.Rows.Add(
                            new object[]
                    {
                        item.Name,
                        SkillType.Crafting,
                        item.SkillLimit,
                        item.Difficulty,
                        item.Ingredients.Count
                    }
                            );
                    }
                }

                file.Close();
            }
            catch (Exception ex)
            {

            }
        }

        private void StartSaveFile(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void SaveFile(object sender, CancelEventArgs e)
        {
            /*string filename = saveFileDialog1.FileName;

            TextWriter file = new StreamWriter(filename);

            file.WriteLine("<Skills>");
            file.WriteLine("   <Manufacturing>");
            file.WriteLine("      <Items>");

            foreach (ManufactureItem item in manuConfig)
            {
                StringBuilder line = new StringBuilder();
                line.Append("         <Item Name=\"" + item.Name + "\" ");
                line.Append("SkillLevel=\"" + item.SkillLimit + "\" ");
                line.Append("Difficulty=\"" + item.Difficulty + "\" ");
                line.Append("MaximumPurity=\"" + item.MaximumPurity + "\" ");
                line.Append(">");
                file.WriteLine(line.ToString());

                file.WriteLine("            <Ingrediants>");
                foreach (KeyValuePair<string, int> ingrediant in item.Ingredients)
                {
                    line = new StringBuilder();
                    line.Append("               <Ingrediant Name=\"" + ingrediant.Key + "\" ");
                    line.Append("Count=\"" + ingrediant.Value + "\" ");
                    line.Append("/>");
                    file.WriteLine(line.ToString());
                }
                file.WriteLine("            </Ingrediants>");
                file.WriteLine("         </Item>");
            }
            file.WriteLine("      </Items>");
            file.WriteLine("   </Manufacturing>");
            /////////////////////////////////////////////////////////////
            file.WriteLine("   <Alchemy>");
            file.WriteLine("      <Items>");
            foreach (AlchemyItem item in alchemyConfig)
            {
                StringBuilder line = new StringBuilder();
                line.Append("         <Item Name=\"" + item.Name + "\" ");
                line.Append("SkillLevel=\"" + item.SkillLimit + "\" ");
                line.Append("Difficulty=\"" + item.Difficulty + "\" ");
                line.Append(">");
                file.WriteLine(line.ToString());

                file.WriteLine("            <Ingrediants>");
                foreach (KeyValuePair<string, int> ingrediant in item.Ingredients)
                {
                    line = new StringBuilder();
                    line.Append("               <Ingrediant Name=\"" + ingrediant.Key + "\" ");
                    line.Append("Count=\"" + ingrediant.Value + "\" ");
                    line.Append("/>");
                    file.WriteLine(line.ToString());
                }
                file.WriteLine("            </Ingrediants>");
                file.WriteLine("         </Item>");
            }
            file.WriteLine("      </Items>");
            file.WriteLine("   </Alchemy>");
            /////////////////////////////////////////////////////////////
            file.WriteLine("   <Crafting>");
            file.WriteLine("      <Items>");
            foreach (CraftingItem item in craftConfig)
            {
                StringBuilder line = new StringBuilder();
                line.Append("         <Item Name=\"" + item.Name + "\" ");
                line.Append("SkillLevel=\"" + item.SkillLimit + "\" ");
                line.Append("Difficulty=\"" + item.Difficulty + "\" ");
                line.Append(">");
                file.WriteLine(line.ToString());

                file.WriteLine("            <Ingrediants>");
                foreach (KeyValuePair<string, int> ingrediant in item.Ingredients)
                {
                    line = new StringBuilder();
                    line.Append("               <Ingrediant Name=\"" + ingrediant.Key + "\" ");
                    line.Append("Count=\"" + ingrediant.Value + "\" ");
                    line.Append("/>");
                    file.WriteLine(line.ToString());
                }
                file.WriteLine("            </Ingrediants>");
                file.WriteLine("         </Item>");
            }
            file.WriteLine("      </Items>");
            file.WriteLine("   </Crafting>");

            file.WriteLine("</Skills>");

            file.Close();*/
        }
    }
}
