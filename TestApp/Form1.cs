﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using HelbreathWorld;
using HelbreathWorld.Game.Assets;

namespace TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            for (int i = 20; i <= 100; i++)
                drpSkill.Items.Add(i + "%");
            drpSkill.SelectedIndex = drpSkill.Items.Count-1;

            for (int i = 0; i <= 100; i++)
                drpBonus.Items.Add(i + "%");
            drpBonus.SelectedIndex = 0;

            drpWeather.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Add("Circle", "Circle");
            dataGridView1.Columns.Add("CP", "CP");

            for (int i = 1; i <= 10; i++)
            {
                int cp = Utility.GetCastingProbability(Int32.Parse(drpSkill.SelectedItem.ToString().Replace("%","")), i, Int32.Parse(txtLevel.Text), Int32.Parse(textBox1.Text), (WeatherType)Enum.Parse(typeof(WeatherType), drpWeather.SelectedItem.ToString()), Int32.Parse(drpBonus.SelectedItem.ToString().Replace("%","")), 1);
                dataGridView1.Rows.Add(new object[] { i,Math.Min(cp,100) + "%" }); 
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
                panel1.BackColor = colorDialog1.Color;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (colorDialog2.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
                panel2.BackColor = colorDialog2.Color;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            panel3.BackColor = Mix(panel1.BackColor, panel2.BackColor);
        }

        private Color Mix(Color c1, Color c2)
        {

            int outR = (c1.R + c2.R) >> 1;

            int outG = (c1.G + c2.G) >> 1;

            int outB = (c1.B + c2.B) >> 1;

            Color c = Color.FromArgb(outR, outG, outB);

            return c;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            foreach (string s in Directory.GetFiles(@"C:\Jeremiah\HBChampions\HBWorld.Net\Client"))
            {
                textBox2.AppendText("  <file name=\"" + Path.GetFileName(s) + "\" force=\"true\" />");
                textBox2.AppendText(Environment.NewLine);
            }
            foreach (string s in Directory.GetFiles(@"C:\Jeremiah\HBChampions\HBWorld.Net\Client\Configs"))
            {
                textBox2.AppendText("  <file name=\"Configs/" + Path.GetFileName(s) + "\" force=\"true\" />");
                textBox2.AppendText(Environment.NewLine);
            }
            foreach (string s in Directory.GetFiles(@"C:\Jeremiah\HBChampions\HBWorld.Net\Client\Content"))
            {
                textBox2.AppendText("  <file name=\"Content/" + Path.GetFileName(s) + "\" force=\"true\" />");
                textBox2.AppendText(Environment.NewLine);
            }
            foreach (string s in Directory.GetFiles(@"C:\Jeremiah\HBChampions\HBWorld.Net\Client\Content\Fonts"))
            {
                textBox2.AppendText("  <file name=\"Content/Fonts/" + Path.GetFileName(s) + "\" force=\"true\" />");
                textBox2.AppendText(Environment.NewLine);
            }
            foreach (string s in Directory.GetFiles(@"C:\Jeremiah\HBChampions\HBWorld.Net\Client\Content\Music"))
            {
                textBox2.AppendText("  <file name=\"Content/Music/" + Path.GetFileName(s) + "\" force=\"true\" />");
                textBox2.AppendText(Environment.NewLine);
            }
            foreach (string s in Directory.GetFiles(@"C:\Jeremiah\HBChampions\HBWorld.Net\Client\Content\Sounds"))
            {
                textBox2.AppendText("  <file name=\"Content/Sounds/" + Path.GetFileName(s) + "\" force=\"true\" />");
                textBox2.AppendText(Environment.NewLine);
            }
            foreach (string s in Directory.GetFiles(@"C:\Jeremiah\HBChampions\HBWorld.Net\Client\Maps"))
            {
                textBox2.AppendText("  <file name=\"Maps/" + Path.GetFileName(s) + "\" force=\"true\" />");
                textBox2.AppendText(Environment.NewLine);
            }
            foreach (string s in Directory.GetFiles(@"C:\Jeremiah\HBChampions\HBWorld.Net\Client\Sprites"))
            {
                textBox2.AppendText("  <file name=\"Sprites/" + Path.GetFileName(s) + "\" force=\"true\" />");
                textBox2.AppendText(Environment.NewLine);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox3.Text = panel1.BackColor.ToArgb().ToString();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                int x = Int32.Parse(textBox4.Text);
                int y = Int32.Parse(textBox5.Text);

                double newX = x;
                double newY = y;

                int resOld = comboBox2.SelectedIndex;
                int resNew = comboBox1.SelectedIndex;

                double pcX, pcY;
                switch (resOld)
                {
                    case 0: // 640x480
                        pcX = ((double)x / 640.0f) * 100.0f;
                        pcY = ((double)y / 480.0f) * 100.0f;
                        switch (resNew)
                        {
                            case 0: // 640x480
                                break;
                            case 1: // 800x600
                                newX = (800.0f / 100.0f) * pcX;
                                newY = (600.0f / 100.0f) * pcY;
                                break;
                            case 2: // 1024x768
                                newX = (1024.0f / 100.0f) * pcX;
                                newY = (768.0f / 100.0f) * pcY;
                                break;
                        }
                        break;
                    case 1: // 800x600
                        pcX = ((double)x / 800.0f) * 100.0f;
                        pcY = ((double)y / 600.0f) * 100.0f;
                        switch (resNew)
                        {
                            case 0: // 640x480
                                newX = (640.0f / 100.0f) * pcX;
                                newY = (480.0f / 100.0f) * pcY;
                                break;
                            case 1: // 800x600
                                break;
                            case 2: // 1024x768
                                newX = (1024.0f / 100.0f) * pcX;
                                newY = (768.0f / 100.0f) * pcY;
                                break;
                        }
                        break;
                    case 2: // 1024x768
                        pcX = ((double)x / 1024.0f) * 100.0f;
                        pcY = ((double)y / 768.0f) * 100.0f;
                        switch (resNew)
                        {
                            case 0: // 640x480
                                newX = (640.0f / 100.0f) * pcX;
                                newY = (480.0f / 100.0f) * pcY;
                                break;
                            case 1: // 800x600
                                newX = (800.0f / 100.0f) * pcX;
                                newY = (600.0f / 100.0f) * pcY;
                                break;
                            case 2: // 1024x768
                                break;
                        }
                        break;
                }

                listBox1.Items.Add(x + "," + y + " = " + newX + "," + newY);
            }
            catch { }
        }
    }
}
