﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.Effects
{
    public interface IGameEffect
    {
        Location Pivot { get; set; }
        bool Draw(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int pivotX, int pivotY);
        bool DrawLights(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int pivotX, int pivotY);
        void Update();
        bool IsComplete { get; }
    }
}
