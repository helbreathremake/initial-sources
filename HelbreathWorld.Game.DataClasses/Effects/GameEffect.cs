﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.Effects
{
    public class GameEffect : IGameEffect
    {
        private DrawEffectType drawEffect;

        private Location source; // start location of the effect (can be screen location or cell location depending on effect)
        private Location destination; // destination location of effect
        private Location current; // tracks moving effects
        private Location random; // random offsets for effect locations

        private int typeModifier; // legacy use. helps reduce amount of code for similar effects
        private int sprite; // sprite number for this effect
        private int spriteLights; // sprite number for lights
        private int spriteLightsFrame; // frame for lights (if static)
        private int spriteFrame; // sprite frame for this effect
        private Color color;
        bool staticFrame; // specifies whether the spriteFrame is static, or should animate (using animationFrame)
        private int animationFrame;
        private int maximumFrames;
        private int frameSpeed;
        private DateTime animationTime;
        private string soundKey;
        private MotionDirection direction;
        int acceleration; // for falling objects

        // e.g lightning
        private int subEffectsCount;
        private List<IGameEffect> subEffects = new List<IGameEffect>();

        public GameEffect(DrawEffectType type, Location source, Location destination, int typeModifier = 0, int accelerationModifier = 0)
        {
            this.staticFrame = false; // forces the use of spriteFrame instead of animationFrame when maximumFrames > 0
            this.drawEffect = type;
            this.source = source;
            this.destination = (destination != null ? destination : source.Copy());

            direction = Utility.GetNextDirection(source.X, source.Y, this.destination.X, this.destination.Y);

            this.color = Color.White;
            this.sprite = this.spriteLights = this.spriteLightsFrame = -1;

            switch (drawEffect)
            {
                case DrawEffectType.Hit:
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    maximumFrames = 2;
                    frameSpeed = 10;
                    sprite = 8;
                    break;
                case DrawEffectType.Miss:
                    current = new Location(source.X, source.Y + (Dice.Roll(1,5)- 2));
                    maximumFrames = 4;
                    frameSpeed = 100;
                    sprite = 11;
                    break;
                case DrawEffectType.Arrow:
                    current = new Location(source.X * 32, (source.Y * 32) - 40);
                    maximumFrames = 0;
                    frameSpeed = 10;
                    soundKey = "C4";
                    sprite = 7;
                    break;
                case DrawEffectType.Critical:
                    staticFrame = true;
                    current = new Location(source.X * 32, (source.Y * 32) - 40);
                    frameSpeed = 10;
                    spriteFrame = 1;
                    sprite = 8;
                    break;
                case DrawEffectType.Burst:
                    current = source.Copy();
                    random = new Location(Dice.Roll(1, 12) - 6, -8 - (Dice.Roll(1, 6)));
                    maximumFrames = 14;
                    frameSpeed = 30;
                    sprite = 11;
                    break;
                case DrawEffectType.DustKick:
                    current = new Location((source.X * 32) + Dice.Roll(1, 5), (source.Y * 32) + Dice.Roll(1, 5));
                    maximumFrames = 4;
                    frameSpeed = 100;
                    sprite = 11;
                    break;
                case DrawEffectType.GoldDrop:
                    current = new Location(source.X*32, source.Y*32);
                    maximumFrames = 12;
                    frameSpeed = 100;
                    sprite = 1;
                    soundKey = "E12";
                    break;
                case DrawEffectType.AdvancedMagic:
                    current = new Location(source.X * 32, source.Y * 32);
                    maximumFrames = 16;
                    frameSpeed = 80;
                    spriteLights = 30;
                    break;
                case DrawEffectType.LightningMagic:
                case DrawEffectType.LightningMagicReverse:
                    current = new Location(source.X * 32, source.Y * 32);
                    maximumFrames = 17;
                    frameSpeed = 30;
                    sprite = 16;
                    break;
                case DrawEffectType.Kinesis:
                case DrawEffectType.KinesisReverse:
                    current = new Location(source.X * 32, source.Y * 32 + 40);
                    maximumFrames = 20;
                    frameSpeed = 10; // 15
                    sprite = 75;
                    color = color * 0.75f;
                    break;
                case DrawEffectType.MagicMissile:
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    frameSpeed = 20;
                    soundKey = "E1";
                    break;
                case DrawEffectType.Burst2:
                    current = source.Copy();
                    maximumFrames = 4;
                    frameSpeed = 30;
                    sprite = 11;
                    break;
                case DrawEffectType.MagicMissileExplosion:
                    current = source.Copy();
                    maximumFrames = 5;
                    frameSpeed = 50;
                    sprite = 6;
                    soundKey = "E3";
                    break;
                case DrawEffectType.MagicMissileLarge:
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    frameSpeed = 20;
                    sprite = 98;
                    soundKey = "E1";
                    break;
                case DrawEffectType.MagicMissileLargeExplosion:
                    current = source.Copy(-30, -18);
                    maximumFrames = 18;
                    frameSpeed = 40;
                    sprite = 6;
                    soundKey = "E4";
                    break;
                case DrawEffectType.MagicMissileLargeExplosion2:
                    current = source.Copy();
                    maximumFrames = 15;
                    frameSpeed = 40;
                    sprite = 97;
                    soundKey = "E4";
                    break;
                case DrawEffectType.EnergyBolt:
                    staticFrame = true;
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    sprite = spriteFrame = 0;
                    frameSpeed = 20;
                    soundKey = "E1";
                    break;
                case DrawEffectType.EnergyBoltLarge:
                    current = source.Copy();
                    maximumFrames = 20;
                    soundKey = "E1";
                    break;
                case DrawEffectType.EnergyBoltExplosion:
                case DrawEffectType.EnergyArrowExplosion:
                    current = source.Copy();
                    maximumFrames = 14;
                    frameSpeed = 10;
                    sprite = 6;
                    soundKey = "E2";
                    break;
                case DrawEffectType.FireBurst:
                    current = source.Copy();
                    random = new Location(Dice.Roll(1, 16) - 8, Dice.Roll(1, 12) - 4);
                    maximumFrames = 10;
                    frameSpeed = 30;
                    sprite = 11;
                    break;
                case DrawEffectType.FireBall:
                case DrawEffectType.FireStrike:
                case DrawEffectType.FireStrikeLarge:
                    staticFrame = true;
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    sprite = 5;
                    frameSpeed = 20;
                    soundKey = "E1";
                    break;
                case DrawEffectType.FireBallExplosion:
                    maximumFrames = 11;
                    frameSpeed = 10;
                    sprite = 3;
                    soundKey = "E4";
                    break;
                case DrawEffectType.FireBallExplosionLarge:
                    maximumFrames = 9;
                    frameSpeed = 40;
                    sprite = 14;
                    soundKey = "E4";
                    break;
                case DrawEffectType.FireBallExplosionLarge2:
                    maximumFrames = 8;
                    frameSpeed = 40;
                    sprite = 15;
                    soundKey = "E4";
                    break;
                case DrawEffectType.EnergyArrow:
                    staticFrame = true;
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    frameSpeed = 20;
                    sprite = 10;
                    soundKey = "E1";
                    break;
                case DrawEffectType.EnergyArrowLarge:
                    maximumFrames = 3;
                    frameSpeed = 130;
                    break;
                case DrawEffectType.Lightning:
                    if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                        subEffects.Add(new LightningBolt(new Location(destination.X * 32, (destination.Y * 32)-800), new Location((destination.X + Dice.Roll(1, 3) - 1) * 32, (destination.Y + Dice.Roll(1, 3) - 1) * 32)) { FadeOutRate = 0.03f });
                    else
                    {
                        current = new Location(source.X * 32, source.Y * 32 - 50);
                        random = new Location(5 - Dice.Roll(1, 10), 5 - Dice.Roll(1, 10));
                        maximumFrames = (type == DrawEffectType.LightningBolt ? 7 : 10);
                        frameSpeed = 10;
                        sprite = 10;
                        soundKey = "E40";
                    }
                    break;
                case DrawEffectType.LightningBolt:
                    if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                        subEffects.Add(new LightningBolt(new Location(source.X * 32, (source.Y * 32) - 40), new Location((destination.X + Dice.Roll(1, 3) - 1) * 32, (destination.Y + Dice.Roll(1, 3) - 1) * 32)) { FadeOutRate = 0.03f });
                    else
                    {
                        current = new Location(source.X * 32, source.Y * 32 - 50);
                        random = new Location(5 - Dice.Roll(1, 10), 5 - Dice.Roll(1, 10));
                        maximumFrames = (type == DrawEffectType.LightningBolt ? 7 : 10);
                        frameSpeed = 10;
                        sprite = 10;
                        soundKey = "E40";
                    }
                    break;
                case DrawEffectType.LightningStrike:
                    maximumFrames = 5;
                    frameSpeed = 120;
                    break;
                case DrawEffectType.ChillWind:
                case DrawEffectType.ChillWindLarge:
                    maximumFrames = 2;
                    frameSpeed = 10;
                    break;
                case DrawEffectType.Wind:
                    maximumFrames = 15;
                    frameSpeed = 30;
                    sprite = 20;
                    soundKey = "E45";
                    break;
                case DrawEffectType.WindLarge:
                    maximumFrames = 14;
                    frameSpeed = 30;
                    sprite = 29;
                    soundKey = "E45";
                    break;
                case DrawEffectType.EnergyStrike:
                    maximumFrames = 7;
                    frameSpeed = 80;
                    break;
                case DrawEffectType.EnergyStrikeTrail:
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    frameSpeed = 20;
                    break;
                case DrawEffectType.EnergyStrikeExplosion:
                    current = source.Copy();
                    maximumFrames = 10;
                    frameSpeed = 50;
                    sprite = 18;
                    break;
                case DrawEffectType.GhostSpiral:
                    maximumFrames = 46;
                    frameSpeed = 20;
                    break;
                case DrawEffectType.Ghost1:
                    frameSpeed = 20;
                    sprite = 154;
                    maximumFrames = 12;
                    break;
                case DrawEffectType.Ghost2:
                    frameSpeed = 20;
                    sprite = 155;
                    maximumFrames = 12;
                    break;
                case DrawEffectType.Ghost3:
                    frameSpeed = 20;
                    sprite = 156;
                    maximumFrames = 12;
                    break;
                case DrawEffectType.Ghost4:
                    frameSpeed = 20;
                    sprite = 157;
                    maximumFrames = 12;
                    break;
                case DrawEffectType.Ghost5:
                    frameSpeed = 20;
                    sprite = 158;
                    maximumFrames = 12;
                    spriteLights = 0;
                    spriteLightsFrame = 1;
                    break;
                case DrawEffectType.Ghost6:
                    frameSpeed = 20;
                    sprite = 159;
                    maximumFrames = 12;
                    break;
                case DrawEffectType.Blizzard:
                    maximumFrames = 7;
                    frameSpeed = 80;
                    break;
                case DrawEffectType.BlizzardShard:
                    current = new Location(source.X * 32, source.Y * 32);
                    frameSpeed = 20;
                    break;
                case DrawEffectType.BlizzardShard2:
                    maximumFrames = 15;
                    frameSpeed = 20;
                    sprite = 51;
                    soundKey = "E47";
                    break;
                case DrawEffectType.IceCloud:
                    staticFrame = true;
                    current = source.Copy();
                    maximumFrames = 9;
                    frameSpeed = 80;
                    sprite = 28;
                    break;
                case DrawEffectType.BlizzardShardTrail2:
                    staticFrame = true;
                    acceleration = 20;
                    current = source.Copy(0, -220);
                    maximumFrames = 12;
                    frameSpeed = 20;
                    sprite = 47;
                    soundKey = "E46";
                    break;
                case DrawEffectType.BlizzardShardTrail3:
                    staticFrame = true;
                    acceleration = 20;
                    current = source.Copy(0, -220);
                    maximumFrames = 12;
                    frameSpeed = 20;
                    sprite = 48;
                    soundKey = "E46";
                    break;
                case DrawEffectType.Ice:
                    current = source.Copy();
                    maximumFrames = 12;
                    frameSpeed = 50;
                    sprite = 22;
                    soundKey = "E47";
                    break;
                case DrawEffectType.IceStrike:
                    maximumFrames = 2;
                    frameSpeed = 10;
                    break;
                case DrawEffectType.IceStrikeLarge:
                    maximumFrames = 5;
                    frameSpeed = 20;
                    break;
                case DrawEffectType.Icicle:
                    current = source.Copy(0, -220); // icicles start too high???
                    maximumFrames = 14;
                    frameSpeed = 20;
                    acceleration = 20;
                    sprite = 21;
                    soundKey = "46";
                    break;
                case DrawEffectType.EarthwormStrike:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 1;
                    frameSpeed = 10;
                    soundKey = "E4";
                    break;
                case DrawEffectType.Worm:
                    current = source.Copy();
                    maximumFrames = 17;
                    frameSpeed = 30;
                    soundKey = "E4";
                    break;
                case DrawEffectType.BloodyShockWave:
                    maximumFrames = 7;
                    frameSpeed = 80;
                    break;
                case DrawEffectType.BloodyShockWaveTrail:
                    current = new Location(source.X * 32, source.Y * 32 -40);
                    frameSpeed = 20;
                    soundKey = "E1";
                    break;
                case DrawEffectType.BloodyShock:
                    current = source.Copy();
                    maximumFrames = 16;
                    frameSpeed = 20;
                    sprite = 19;
                    break;
                case DrawEffectType.EarthShockWave:
                    current = new Location(source.X * 32, source.Y * 32);
                    maximumFrames = 30;
                    frameSpeed = 20;
                    break;
                case DrawEffectType.EarthShock:
                    current = source.Copy();
                    acceleration = 20;
                    maximumFrames = 30;
                    frameSpeed = 25;
                    break;
                case DrawEffectType.Meteor:
                    current = new Location(destination.X * 32 + 300, destination.Y * 32 - 460);
                    maximumFrames = 10;
                    frameSpeed = 25;
                    sprite = 31;
                    break;
                case DrawEffectType.MeteorTrail:
                    current = new Location(source.X * 32 + 300, source.Y * 32 - 460);
                    maximumFrames = 10;
                    frameSpeed = 50;
                    sprite = 31;
                    break;
                case DrawEffectType.MeteorExplosion:
                    current = source.Copy();
                    maximumFrames = 16;
                    frameSpeed = 20;
                    sprite = 32;
                    soundKey = "E4";
                    break;
                case DrawEffectType.MeteorExplosion2:
                    current = source.Copy();
                    maximumFrames = 16;
                    frameSpeed = 10;
                    sprite = 33;
                    break;
                case DrawEffectType.AbaddonMeteor:
                    current = new Location((source.X * 32)+40, (source.Y - 1) * 32); // -32 pixel
                    maximumFrames = 15;
                    frameSpeed = 15;
                    sprite = 133;
                    break;
                case DrawEffectType.AbaddonMeteor2:
                    current = new Location((source.X * 32)-35, (source.Y - 1) * 32); // -32 pixel
                    maximumFrames = 15;
                    frameSpeed = 15;
                    sprite = 134;
                    break;
                case DrawEffectType.AbaddonMeteor3:
                    current = new Location((source.X * 32)+10, (source.Y - 1) * 32); // -32 pixel
                    maximumFrames = 15;
                    frameSpeed = 15;
                    sprite = 135;
                    break;
                case DrawEffectType.AbaddonMeteorExplosion:
                    current = source.Copy();
                    maximumFrames = 18;
                    frameSpeed = 25;
                    sprite = 136;
                    break;
                case DrawEffectType.Debuff:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 13;
                    frameSpeed = 70;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Firework1:
                    maximumFrames = 11;
                    frameSpeed = 30;
                    sprite = 42;
                    soundKey = "E42";
                    break;
                case DrawEffectType.Firework2:
                    maximumFrames = 11;
                    frameSpeed = 30;
                    sprite = 43;
                    soundKey = "E42";
                    break;
                case DrawEffectType.Flash:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 13;
                    frameSpeed = 120;
                    sprite = 4;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Stars:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 12;
                    frameSpeed = 80;
                    sprite = 52;
                    soundKey = "E5";
                    break;
                case DrawEffectType.SpiralDown:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 14;
                    frameSpeed = 80;
                    sprite = 49;
                    soundKey = "E5";
                    break;
                case DrawEffectType.SpiralUp:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 14;
                    frameSpeed = 80;
                    sprite = 50;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Defence:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 12;
                    frameSpeed = 120;
                    sprite = 62;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Defence2:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 12;
                    frameSpeed = 120;
                    sprite = 63;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Protect:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 15;
                    frameSpeed = 80;
                    spriteLights = 24;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Bubble:
                    current = new Location(source.X * 32, source.Y * 32);
                    maximumFrames = 21;
                    frameSpeed = 70;
                    sprite = 53;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Twist:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 15;
                    frameSpeed = 80;
                    sprite = 25;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Aura:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 11;
                    frameSpeed = 100;
                    spriteLights = 58;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Cancellation:
                    current = new Location(destination.X * 32+50, destination.Y * 32+85);
                    maximumFrames = 23;
                    frameSpeed = 60;
                    sprite = 90;
                    soundKey = "E5";
                    break;
                case DrawEffectType.IllusionMovement:
                case DrawEffectType.Illusion:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 11;
                    frameSpeed = 100;
                    sprite = 60;
                    spriteLights = 59;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Inhibition:
                    current = new Location(destination.X * 32, destination.Y * 32 + 40);
                    maximumFrames = 11;
                    frameSpeed = 100;
                    sprite = 94;
                    soundKey = "E5";
                    break;
                case DrawEffectType.Resurrect:
                    current = new Location(destination.X * 32, destination.Y * 32);
                    maximumFrames = 30;
                    frameSpeed = 40;
                    sprite = 99;
                    break;
                case DrawEffectType.HeroMage:
                    current = new Location(source.X * 32 + 50, source.Y * 32 + 57);
                    maximumFrames = 30;
                    frameSpeed = 40;
                    sprite = 87;
                    break;
                case DrawEffectType.HeroWarrior:
                    current = new Location(source.X * 32 + 65, source.Y * 32 + 68);
                    maximumFrames = 19;
                    frameSpeed = 18;
                    sprite = 88;
                    break;
                case DrawEffectType.RainSplash:
                    current = source.Copy();
                    maximumFrames = 4;
                    frameSpeed = 100;
                    sprite = 11;
                    break;
                case DrawEffectType.EnergyStaff:
                    staticFrame = true;
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    sprite = spriteFrame = 0;
                    frameSpeed = 20;
                    soundKey = "E1";
                    break;
                case DrawEffectType.FireStaff:
                    staticFrame = true;
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    sprite = 5;
                    frameSpeed = 20;
                    soundKey = "E1";
                    break;
                case DrawEffectType.IceStaff:
                    staticFrame = true;
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    sprite = 21;
                    maximumFrames = 8;
                    frameSpeed = 20;
                    soundKey = "E1";
                    break;
                case DrawEffectType.EarthStaff:
                    staticFrame = true;
                    current = new Location(source.X * 32, source.Y * 32 - 40);
                    maximumFrames = 8;
                    frameSpeed = 20;
                    soundKey = "E1";
                    break;
                case DrawEffectType.MeteorStaff:
                    staticFrame = true;
                    current = new Location(destination.X * 32, destination.Y * 32 - 40);
                    maximumFrames = 1;
                    frameSpeed = 20;
                    soundKey = "E1";
                    break;
                case DrawEffectType.StormBlade:
                    current = new Location(source.X * 32, source.Y * 32);
                    maximumFrames = 27;
                    frameSpeed = 40;
                    sprite = 100;
                    break;
                case DrawEffectType.Twinkle:
                    current = new Location((source.X * 32) + Dice.Roll(1, 20) - 10, (source.Y * 32) - 40 + (Dice.Roll(1, 50) - 5));
                    maximumFrames = 10;
                    frameSpeed = 15;
                    sprite = 28;
                    break;
                case DrawEffectType.AngelTwinkle:
                    current = new Location((source.X * 32) + Dice.Roll(1, 15) + 10, (source.Y * 32) - 40 + (Dice.Roll(1, 30) - 50));
                    maximumFrames = 10;
                    frameSpeed = 15;
                    sprite = 28;
                    break;
            }

            this.typeModifier += typeModifier;
            this.acceleration += accelerationModifier;

            if (current == null) current = source.Copy();

            if (Cache.GameSettings.SoundsOn && !string.IsNullOrEmpty(soundKey))
            {
                AudioHelper.PlaySound(soundKey, 1.0f, 0.5f); // TODO - distance and pan
            }
        }

        public void Update()
        {
            // Animate
            //if (maximumFrames > 0)
            //{
                if ((DateTime.Now - animationTime).Milliseconds > frameSpeed)
                {

                    int[] point;
                    switch (drawEffect)
                    {
                        case DrawEffectType.Arrow:
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, (destination.Y * 32)-40, 25); // was 70
                            current.X = point[0];
                            current.Y = point[1];

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - (destination.Y * 32) - 40) <= 2)
                                Remove();
                            break;
                        case DrawEffectType.Critical:
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, (destination.Y * 32) -40, 50); // was 70
                            current.X = point[0];
                            current.Y = point[1];

                            for (int i = 0; i < 4; i++)
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - (destination.Y * 32)-40) <= 2)
                                Remove();
                            break;
                        case DrawEffectType.MagicMissile:
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, destination.Y * 32, 50);
                            current.X = point[0];
                            current.Y = point[1];
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - destination.Y * 32) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.MagicMissileExplosion, new Location(destination.X*32, destination.Y*32), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.MagicMissileLarge:
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, destination.Y * 32, 50);
                            current.X = point[0];
                            current.Y = point[1];
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) -10), null), this.Pivot.X, this.Pivot.Y);

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - destination.Y * 32) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.MagicMissileLargeExplosion, new Location(destination.X * 32 + 22, destination.Y * 32 - 15), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.MagicMissileLargeExplosion2, new Location(destination.X * 32 - 22, destination.Y * 32 - 7), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.MagicMissileLargeExplosion2, new Location(destination.X * 32 + 30, destination.Y * 32 - 22), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.MagicMissileLargeExplosion2, new Location(destination.X * 32 + 12, destination.Y * 32 + 22), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.EnergyBolt:
                            spriteFrame = Dice.Roll(1, 4) + 2; // randomize
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, destination.Y * 32, 50);
                            current.X = point[0];
                            current.Y = point[1];
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - destination.Y * 32) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EnergyBoltExplosion, new Location(destination.X * 32, destination.Y * 32), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.EnergyBoltLarge:
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EnergyBolt, source.Copy(), destination.Copy(-1,-1)), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EnergyBolt, source.Copy(), destination.Copy(1, -1)), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EnergyBolt, source.Copy(), destination.Copy(1, 1)), this.Pivot.X, this.Pivot.Y);

                            // not sure what this is for so removed it. trails are done during individual EnergyBold effect
                            //Cache.GameState.AddEffect(new GameEffect(DrawEffectType.MagicMissileTrail, current.Copy(Dice.Roll(1,20)-10, Dice.Roll(1,20)-10),null));
                            Remove();
                            break;
                        case DrawEffectType.FireBall:
                            spriteFrame = (((int)direction) - 1) * 4 + Dice.Roll(1, 4); // get correct direction and then randomize
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, destination.Y * 32, 50);
                            current.X = point[0];
                            current.Y = point[1];

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - destination.Y * 32) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.FireBallExplosion, new Location(destination.X * 32, destination.Y * 32), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.FireStrike:
                            spriteFrame = (((int)direction) - 1) * 4 + Dice.Roll(1, 4); // get correct direction and then randomize
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, destination.Y * 32, 50);
                            current.X = point[0];
                            current.Y = point[1];

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - destination.Y * 32) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.FireBallExplosion, new Location(destination.X * 32 - 30, destination.Y * 32 - 15), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.FireBallExplosion, new Location(destination.X * 32 + 35, destination.Y * 32 - 30), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.FireBallExplosion, new Location(destination.X * 32 + 20, destination.Y * 32 + 30), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.FireBallExplosion, new Location(destination.X * 32, destination.Y * 32), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.FireStrikeLarge:
                            spriteFrame = (((int)direction) - 1) * 4 + Dice.Roll(1, 4); // get correct direction and then randomize
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, destination.Y * 32, 50);
                            current.X = point[0];
                            current.Y = point[1];

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - destination.Y * 32) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.FireBallExplosionLarge, new Location(destination.X * 32, destination.Y * 32), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.FireBallExplosionLarge2, new Location(destination.X * 32 - 30, destination.Y * 32 - 15), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.FireBallExplosionLarge2, new Location(destination.X * 32 + 35, destination.Y * 32 - 30), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.FireBallExplosionLarge2, new Location(destination.X * 32 + 20, destination.Y * 32 + 30), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.EnergyArrow:
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, destination.Y * 32, 50);
                            current.X = point[0];
                            current.Y = point[1];
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, new Location((current.X + Dice.Roll(1, 10) / 32), (current.Y + Dice.Roll(1, 10) / 32)), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, new Location((current.X + Dice.Roll(1, 10) / 32), (current.Y + Dice.Roll(1, 10) / 32)), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, new Location((current.X + Dice.Roll(1, 10) / 32), (current.Y + Dice.Roll(1, 10) / 32)), null), this.Pivot.X, this.Pivot.Y);

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - destination.Y * 32) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EnergyArrowExplosion, new Location(destination.X * 32, destination.Y * 32), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.EnergyArrowLarge:
                            if (animationFrame >= maximumFrames) Remove();
                            else
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EnergyArrow, source.Copy(), destination.Copy()), this.Pivot.X, this.Pivot.Y);
                            break;
                        case DrawEffectType.Lightning:
                        case DrawEffectType.LightningBolt:
                            if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                            {
                                if (subEffects.Count <= 0) Remove(); // remove this effect if all subeffects are complete
                                foreach (var subEffect in subEffects) subEffect.Update();
                                subEffects = subEffects.Where(x => !x.IsComplete).ToList();
                            }
                            else
                            {
                                if (animationFrame >= maximumFrames)
                                {
                                    Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EnergyArrowExplosion, new Location(destination.X * 32, destination.Y * 32), null), this.Pivot.X, this.Pivot.Y);
                                    Remove();
                                }
                                else random = new Location(5 - Dice.Roll(1, 10), 5 - Dice.Roll(1, 10));
                            }
                            break;
                        case DrawEffectType.LightningStrike:
                            if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                            {
                                // add subeffects per frame
                                if (subEffectsCount < maximumFrames)
                                {
                                    subEffects.Add(new LightningBolt(new Location(source.X * 32, (source.Y * 32) - 40), new Location((destination.X + Dice.Roll(1, 3) - 1) * 32, (destination.Y + Dice.Roll(1, 3) - 1) * 32)));
                                    subEffectsCount++;
                                }
                                else if (subEffects.Count <= 0) Remove(); // remove this effect if all subeffects are complete

                                // update subeffects
                                foreach (var subEffect in subEffects) subEffect.Update();
                                subEffects = subEffects.Where(x => !x.IsComplete).ToList();
                            }
                            else
                            {
                                if (animationFrame >= maximumFrames) Remove();
                                else Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.LightningBolt, source.Copy(), destination.Copy(Dice.Roll(1, 3) - 1, Dice.Roll(1, 3) - 1)), this.Pivot.X, this.Pivot.Y);
                            }
                            break;
                        case DrawEffectType.EnergyStrike:
                            if (animationFrame >= maximumFrames) Remove();
                            else
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EnergyStrikeTrail, source.Copy(), destination.Copy(Dice.Roll(1, 3) - 1, Dice.Roll(1, 3) - 1)), this.Pivot.X, this.Pivot.Y);
                            break;
                        case DrawEffectType.EnergyStrikeTrail:
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, destination.Y * 32, 50);
                            current.X = point[0];
                            current.Y = point[1];
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, new Location((current.X + Dice.Roll(1, 10) / 32), (current.Y + Dice.Roll(1, 10) / 32)), null), this.Pivot.X, this.Pivot.Y);

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - destination.Y * 32) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EnergyStrikeExplosion, new Location(destination.X *32, destination.Y*32), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst, new Location((current.X + Dice.Roll(1, 40) - 20), (current.Y + Dice.Roll(1, 40) - 20)), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst, new Location((current.X + Dice.Roll(1, 40) - 20), (current.Y + Dice.Roll(1, 40) - 20)), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst, new Location((current.X + Dice.Roll(1, 40) - 20), (current.Y + Dice.Roll(1, 40) - 20)), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst, new Location((current.X + Dice.Roll(1, 40) - 20), (current.Y + Dice.Roll(1, 40) - 20)), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst, new Location((current.X + Dice.Roll(1, 40) - 20), (current.Y + Dice.Roll(1, 40) - 20)), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.ChillWind:
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Wind, new Location(destination.X * 32, destination.Y * 32), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Wind, new Location(destination.X * 32 - 30, destination.Y * 32 - 15), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Wind, new Location(destination.X * 32 + 35, destination.Y * 32 - 30), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Wind, new Location(destination.X * 32 + 20, destination.Y * 32 + 30), null), this.Pivot.X, this.Pivot.Y);
                            Remove();
                            break;
                        case DrawEffectType.ChillWindLarge:
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.WindLarge, new Location(destination.X * 32, destination.Y * 32), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.WindLarge, new Location(destination.X * 32 -30, destination.Y * 32 -15), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.WindLarge, new Location(destination.X * 32 +35, destination.Y * 32 -30), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.WindLarge, new Location(destination.X * 32 +20, destination.Y * 32 + 30), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.WindLarge, new Location(destination.X * 32 + Dice.Roll(1,100)-50, destination.Y * 32), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.WindLarge, new Location(destination.X * 32 + Dice.Roll(1, 100) - 50, destination.Y * 32 + Dice.Roll(1,70)-35), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.WindLarge, new Location(destination.X * 32 + Dice.Roll(1, 100) - 50, destination.Y * 32 + Dice.Roll(1, 70) - 35), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.WindLarge, new Location(destination.X * 32 + Dice.Roll(1, 100) - 50, destination.Y * 32 + Dice.Roll(1, 70) - 35), null), this.Pivot.X, this.Pivot.Y);
                            Remove();
                            break;
                        case DrawEffectType.GhostSpiral:
                            if (animationFrame >= maximumFrames) Remove();
                            else if (animationFrame % 2 == 0) // generate a ghost every every 2 frames
                            {
                                List<int[]> locations = Utility.GetMagicSpiralLocations(source.X, source.Y);
                                int x = locations[animationFrame][0] * 32;
                                int y = locations[animationFrame][1] * 32;
                                switch (Dice.Roll(1, 6))
                                {
                                    case 1: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Ghost1, new Location(x + Dice.Roll(1, 100) - 50, y + Dice.Roll(1, 100) - 50), null), this.Pivot.X, this.Pivot.Y); break;
                                    case 2: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Ghost2, new Location(x + Dice.Roll(1, 100) - 50, y + Dice.Roll(1, 100) - 50), null), this.Pivot.X, this.Pivot.Y); break;
                                    case 3: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Ghost3, new Location(x + Dice.Roll(1, 100) - 50, y + Dice.Roll(1, 100) - 50), null), this.Pivot.X, this.Pivot.Y); break;
                                    case 4: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Ghost4, new Location(x + Dice.Roll(1, 100) - 50, y + Dice.Roll(1, 100) - 50), null), this.Pivot.X, this.Pivot.Y); break;
                                    case 5: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Ghost5, new Location(x + Dice.Roll(1, 100) - 50, y + Dice.Roll(1, 100) - 50), null), this.Pivot.X, this.Pivot.Y); break;
                                    case 6: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Ghost6, new Location(x + Dice.Roll(1, 100) - 50, y + Dice.Roll(1, 100) - 50), null), this.Pivot.X, this.Pivot.Y); break;
                                }
                            }
                            break;
                        case DrawEffectType.Blizzard:
                            if (animationFrame >= maximumFrames) Remove();
                            else
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.BlizzardShard, current.Copy(), new Location((destination.X * 32) + Dice.Roll(1, 100) - 50, (destination.Y * 32) + Dice.Roll(1, 100) - 50)), this.Pivot.X, this.Pivot.Y);
                            break;
                        case DrawEffectType.BlizzardShard:
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X, destination.Y, 50);
                            current.X = point[0];
                            current.Y = point[1];
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.IceCloud, current.Copy(Dice.Roll(1, 30) - 15, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.BlizzardShardTrail2, current.Copy(Dice.Roll(1, 30) - 15, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);

                            if (Math.Abs(current.X - destination.X) <= 2 || Math.Abs(current.Y - destination.Y) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.BlizzardShardTrail3, current.Copy(), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.BlizzardShardTrail2:
                        case DrawEffectType.BlizzardShardTrail3:
                            if (animationFrame >= 7)
                            {
                                current.X--;
                                current.Y += acceleration;
                                acceleration += 4;
                            }
                            if (animationFrame >= maximumFrames)
                            {
                                if (drawEffect == DrawEffectType.BlizzardShardTrail3)
                                     Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.BlizzardShard2, current.Copy(), null), this.Pivot.X, this.Pivot.Y);
                                else Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Ice, current.Copy(), null), this.Pivot.X, this.Pivot.Y);

                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Miss, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Miss, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Miss, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);

                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.IceCloud, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.IceCloud, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);

                                Remove();
                            }
                            break;
                        case DrawEffectType.IceStrike:
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Icicle, new Location(destination.X * 32, destination.Y * 32), null, 41), this.Pivot.X, this.Pivot.Y);
                            for (int i = 0; i < 14; i++)
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Icicle, new Location(destination.X * 32 + Dice.Roll(1, 100) - 50 + 10, destination.Y * 32 + Dice.Roll(1, 90) - 45 + 10), null, 41 + Dice.Roll(1, 3), (-1 * i - 1)), this.Pivot.X, this.Pivot.Y);

                            for (int i = 0; i < 6; i++)
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Icicle, new Location(destination.X * 32 + Dice.Roll(1, 100) - 50 + 10, destination.Y * 32 + Dice.Roll(1, 90) - 45 + 10), null, 45 + Dice.Roll(1, 2), (-1 * i - 1 - 10)), this.Pivot.X, this.Pivot.Y);

                            Remove();
                            break;
                        case DrawEffectType.IceStrikeLarge:
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Icicle, new Location(destination.X * 32, destination.Y * 32), null, 44), this.Pivot.X, this.Pivot.Y);
                            for (int i = 0; i < 16; i++)
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Icicle, new Location(destination.X * 32 + Dice.Roll(1, 110) - 55 + 10, destination.Y * 32 + Dice.Roll(1, 100) - 50), null, 44, (-1 * Dice.Roll(1, 3))), this.Pivot.X, this.Pivot.Y);

                            for (int i = 0; i < 16; i++)
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Icicle, new Location(destination.X * 32 + Dice.Roll(1, 110) - 55 + 10, destination.Y * 32 + Dice.Roll(1, 100) - 50), null, 44, (-1 * i - 1)), this.Pivot.X, this.Pivot.Y);

                            for (int i = 0; i < 8; i++)
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Icicle, new Location(destination.X * 32 + Dice.Roll(1, 100) - 50 + 10, destination.Y * 32 + Dice.Roll(1, 90) - 45), null, 45 + Dice.Roll(1, 2), (-1 * i - 1 - 10)), this.Pivot.X, this.Pivot.Y);

                            Remove();
                            break;
                        case DrawEffectType.Icicle:
                            if (animationFrame >= 7)
                            {
                                current.X--;
                                current.Y += acceleration;
                                acceleration += 4; // fixed from +=1 otherwise icicles didnt fall far enough before exploding
                            }
                            if (animationFrame >= maximumFrames)
                            {
                                if ((typeModifier == 45 || typeModifier == 46))
                                {
                                    Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Ice, current.Copy(), null), this.Pivot.X, this.Pivot.Y);

                                    Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Miss, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                                    Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Miss, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                                    Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Miss, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);

                                    Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.IceCloud, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                                    Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.IceCloud, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                                }

                                Remove();
                            }
                            break;
                        case DrawEffectType.EarthwormStrike:
                            if (animationFrame == 0)
                            {
                                for (int i = 0; i < 14; i++)
                                    Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Miss, current.Copy(Dice.Roll(1, 120) - 60, Dice.Roll(1, 80) - 40), null), this.Pivot.X, this.Pivot.Y);
                            }

                            if (animationFrame >= maximumFrames)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Worm, new Location(destination.X * 32, destination.Y*32), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.BloodyShockWave:
                            if (animationFrame >= maximumFrames) Remove();
                            else if (animationFrame % 2 == 0)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.BloodyShockWaveTrail, source.Copy(), new Location((destination.X * 32) + 30 - Dice.Roll(1, 60), (destination.Y * 32) + 30 - Dice.Roll(1, 60))), this.Pivot.X, this.Pivot.Y);
                            }
                            break;
                        case DrawEffectType.BloodyShockWaveTrail:
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X, destination.Y, 50);
                            current.X = point[0];
                            current.Y = point[1];
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.BloodyShock, current.Copy(Dice.Roll(1, 30) - 15, Dice.Roll(1, 30) - 15), null), this.Pivot.X, this.Pivot.Y);

                            if (Math.Abs(current.X - destination.X) <= 2 || Math.Abs(current.Y - destination.Y) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.BloodyShock, new Location(destination.X, destination.Y), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.EarthShockWave:
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X*32, destination.Y*32, 40);
                            current.X = point[0];
                            current.Y = point[1];
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EarthShock, current.Copy(Dice.Roll(1, 30) - 15, Dice.Roll(1, 30) - 15), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EarthShock, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);

                            if (animationFrame >= maximumFrames)
                            {
                                Remove();
                            }
                            break;
                        case DrawEffectType.Meteor:
                            if (animationFrame >= maximumFrames)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.MeteorExplosion2, current.Copy(), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.MeteorExplosion, current.Copy(), null), this.Pivot.X, this.Pivot.Y);
                                for (int i = 0; i < 5; i++)
                                    Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.FireBurst, current.Copy(Dice.Roll(1, 10) - 5, Dice.Roll(1, 10) - 5), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            else
                            {
                                current.X -= 30;
                                current.Y += 46;
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.MeteorTrail, source.Copy(), null), this.Pivot.X, this.Pivot.Y);
                            }
                            break;
                        case DrawEffectType.AbaddonMeteor:
                            if (animationFrame >= maximumFrames)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.AbaddonMeteorExplosion, new Location((destination.X * 32) + 80, (destination.Y * 32) + 95), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.AbaddonMeteor2:
                            if (animationFrame >= maximumFrames)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.AbaddonMeteorExplosion, new Location((destination.X * 32) + 80, (destination.Y * 32) + 95), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.AbaddonMeteor3:
                            if (animationFrame >= maximumFrames)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.AbaddonMeteorExplosion, new Location((destination.X * 32) + 80, (destination.Y * 32) + 95), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.Fireworks:
                            for (int i = 0; i < 4; i++)
                                switch (Dice.Roll(1, 2))
                                {
                                    case 1: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Firework1, new Location(destination.X * 32, destination.Y*32), null), this.Pivot.X, this.Pivot.Y); break;
                                    case 2: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Firework2, new Location(destination.X * 32, destination.Y*32), null), this.Pivot.X, this.Pivot.Y); break;
                                }
                            Remove();
                            break;
                        case DrawEffectType.EnergyStaff:
                            spriteFrame = Dice.Roll(1, 4) + 2; // randomize
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, (destination.Y * 32) - 40, 30);
                            current.X = point[0];
                            current.Y = point[1];
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Burst2, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - (destination.Y * 32) - 40) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.EnergyStrikeExplosion, new Location(destination.X * 32, (destination.Y * 32) - 40), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.FireStaff:
                            spriteFrame = (((int)direction) - 1) * 4 + Dice.Roll(1, 4); // get correct direction and then randomize
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, (destination.Y * 32) - 40, 30);
                            current.X = point[0];
                            current.Y = point[1];

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - (destination.Y * 32) - 40) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.MeteorExplosion2, new Location(destination.X * 32, (destination.Y * 32) - 40), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.IceStaff:
                            spriteFrame = 24 + (Dice.Roll(1, 8) - 1);
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, (destination.Y * 32) - 40, 30);
                            current.X = point[0];
                            current.Y = point[1];

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - (destination.Y * 32) - 40) <= 2)
                            {
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Ice, current.Copy(Dice.Roll(1, 20) - 10, Dice.Roll(1, 20) - 10), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.IceCloud, current.Copy(Dice.Roll(1, 40) - 20, Dice.Roll(1, 40) - 20), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.IceCloud, current.Copy(Dice.Roll(1, 40) - 20, Dice.Roll(1, 40) - 20), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.IceCloud, current.Copy(Dice.Roll(1, 40) - 20, Dice.Roll(1, 40) - 20), null), this.Pivot.X, this.Pivot.Y);
                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.IceCloud, current.Copy(Dice.Roll(1, 40) - 20, Dice.Roll(1, 40) - 20), null), this.Pivot.X, this.Pivot.Y);
                                Remove();
                            }
                            break;
                        case DrawEffectType.EarthStaff:
                            spriteFrame = animationFrame;
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, (destination.Y * 32) - 40, 30);
                            current.X = point[0];
                            current.Y = point[1];

                            if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - (destination.Y * 32) - 40) <= 2)
                            {
                                Remove();
                            }
                            break;
                        case DrawEffectType.MeteorStaff:
                            switch (Dice.Roll(1,3))
                            {
                                case 1: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.AbaddonMeteor, destination.Copy(), null), this.Pivot.X, this.Pivot.Y); break;
                                case 2: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.AbaddonMeteor2, destination.Copy(), null), this.Pivot.X, this.Pivot.Y); break;
                                case 3: Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.AbaddonMeteor3, destination.Copy(), null), this.Pivot.X, this.Pivot.Y); break;
                            }
                            Remove();
                            break;
                        case DrawEffectType.StormBlade:
                            point = GameUtility.GetPoint(current.X, current.Y, destination.X*32, destination.Y*32, 8);
                            current.X = point[0];
                            current.Y = point[1];

                            //if (Math.Abs(current.X - destination.X * 32) <= 2 || Math.Abs(current.Y - destination.Y * 32) <= 2)
                            if (animationFrame >= maximumFrames) Remove();
                            break;
                        default: if (animationFrame >= maximumFrames) Remove(); break;
                    }

                    if (animationFrame + 1 <= maximumFrames)
                        animationFrame++;
                    else animationFrame = 0;

                    animationTime = DateTime.Now;
                }
            //}
            //else animationFrame = spriteFrame; // do not animate if 0 frames specified
        }

        /// <summary>
        /// Draws lighting for the effect.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="display"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="pivotX"></param>
        /// <param name="pivotY"></param>
        /// <returns></returns>
        public bool DrawLights(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int pivotX, int pivotY)
        {
            int offsetX = (pivotX - this.Pivot.X)*32;
            int offsetY = (pivotY - this.Pivot.Y)*32;

            switch (drawEffect)
            {
                default:
                    if (spriteLights != -1)
                        SpriteHelper.DrawEffect(spriteBatch, spriteLights, (spriteLightsFrame != -1 ? spriteLightsFrame : animationFrame), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
            }

            return false;
        }

        /// <summary>
        /// DrawItemPopup sprites for the effect.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="display"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="pivotX"></param>
        /// <param name="pivotY"></param>
        /// <returns></returns>
        public bool Draw(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int pivotX, int pivotY)
        {
            int[] point;
            int frame;
            int offsetX = (pivotX - this.Pivot.X)*32;
            int offsetY = (pivotY - this.Pivot.Y)*32;

            switch (drawEffect)
            {
                case DrawEffectType.LightningMagicReverse:
                case DrawEffectType.KinesisReverse:
                    spriteFrame = (maximumFrames - animationFrame);
                    SpriteHelper.DrawEffect(spriteBatch, sprite, spriteFrame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.Miss:
                case DrawEffectType.DustKick:
                    SpriteHelper.DrawEffect(spriteBatch, sprite, 28 + animationFrame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.GoldDrop: break; // apparently this was removed in HB 1.5! only has 3 frames, play from frames 9 - 12
                case DrawEffectType.Arrow:
                    spriteFrame = (((int)direction)-1)*2;
                    SpriteHelper.DrawEffect(spriteBatch, sprite, spriteFrame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.Burst:
                    SpriteHelper.DrawEffect(spriteBatch, sprite, Dice.Roll(1, 5), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.Burst2:
                    spriteFrame = 4 - animationFrame;
                    SpriteHelper.DrawEffect(spriteBatch, sprite, spriteFrame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.FireBurst:
                    frame = Dice.Roll(1, 6) + 10;
                    if (animationFrame < 4)
                        SpriteHelper.DrawEffect(spriteBatch, sprite, frame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    SpriteHelper.DrawEffect(spriteBatch, sprite, frame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.EnergyArrow:
                    for (int i = 0; i < 5; i++)
                    {
                        point = GameUtility.GetPoint(current.X, current.Y, destination.X * 32, destination.Y * 32, i * 15);
                        SpriteHelper.DrawEffect(spriteBatch, sprite, (((int)direction) - 1) * 4 + Dice.Roll(1, 4), point[0] + 16, point[1] + 16, -offsetX - x, -offsetY - y, color);
                    }
                    break;
                case DrawEffectType.Lightning:
                    if (Cache.GameSettings.DetailMode != GraphicsDetail.High)
                    {
                        DrawLightning(spriteBatch, display, new Location(destination.X * 32 + 16, (destination.Y * 32) - 800 + 16), new Location(destination.X * 32 + 16, destination.Y * 32 + 16), random.Copy(), 1);
                        DrawLightning(spriteBatch, display, new Location(destination.X * 32 + 16, (destination.Y * 32) - 800 + 16), new Location(destination.X * 32 + 16, destination.Y * 32 + 16), random.Copy(4, 2), 2);
                        DrawLightning(spriteBatch, display, new Location(destination.X * 32 + 16, (destination.Y * 32) - 800 + 16), new Location(destination.X * 32 + 16, destination.Y * 32 + 16), random.Copy(-2, -2), 2);
                    }
                    break;
                case DrawEffectType.LightningBolt:
                    if (Cache.GameSettings.DetailMode != GraphicsDetail.High)
                    {
                        DrawLightning(spriteBatch, display, new Location(source.X * 32 + 16, source.Y * 32 + 16), new Location(destination.X * 32 + 16, destination.Y * 32 + 16), random.Copy(), 1);
                        DrawLightning(spriteBatch, display, new Location(source.X * 32 + 16, source.Y * 32 + 16), new Location(destination.X * 32 + 16, destination.Y * 32 + 16), random.Copy(4, 2), 2);
                        DrawLightning(spriteBatch, display, new Location(source.X * 32 + 16, source.Y * 32 + 16), new Location(destination.X * 32 + 16, destination.Y * 32 + 16), random.Copy(-2, -2), 2);
                    }
                    break;
                case DrawEffectType.BlizzardShard2:
                    if (animationFrame <= 8)
                        SpriteHelper.DrawEffect(spriteBatch, sprite, animationFrame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    else
                    {
                        float fade = (1.0f * (float)((float)animationFrame / 8.0f));
                        SpriteHelper.DrawEffect(spriteBatch, sprite, 8, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color * fade);
                    }
                    break;
                case DrawEffectType.IceCloud:
                    spriteFrame = animationFrame + 11;
                    SpriteHelper.DrawEffect(spriteBatch, sprite, (staticFrame ? spriteFrame : animationFrame), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color * 0.25f);
                    break;
                case DrawEffectType.BlizzardShardTrail2:
                case DrawEffectType.BlizzardShardTrail3:
                    SpriteHelper.DrawEffect(spriteBatch, sprite, (staticFrame ? spriteFrame : animationFrame), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);

                    if (animationFrame > 0)
                        if (animationFrame < 7)
                        {
                            float fade = (1.0f * (float)((float)animationFrame / 6.0f));
                            SpriteHelper.DrawEffect(spriteBatch, sprite, animationFrame + 1, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color * fade);
                        }
                        else if (animationFrame >= 8) 
                        {
                            int tempFrame = animationFrame % 8;
                            SpriteHelper.DrawEffect(spriteBatch, sprite, tempFrame + 1, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                        }
                    break;
                case DrawEffectType.Ice:
                    if (animationFrame <= 6)
                        SpriteHelper.DrawEffect(spriteBatch, sprite, animationFrame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    else
                    {
                        float fade = (1.0f * (float)((float)animationFrame / 6.0f));
                        SpriteHelper.DrawEffect(spriteBatch, sprite, 6, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color * fade);
                    }
                    break;
                case DrawEffectType.Icicle:
                    SpriteHelper.DrawEffect(spriteBatch, sprite, 48, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);

                    frame = animationFrame;
                    if ((8 * (typeModifier - 41) + animationFrame) < (8 * (typeModifier - 41) + 7))
                    {
                        frame = -8 * (6 - animationFrame);
                        SpriteHelper.DrawEffect(spriteBatch, sprite, 8 * (typeModifier - 41) + frame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    }
                    else
                    {
                        if ((animationFrame - 5) >= 8) frame = ((animationFrame - 5) - 8) + 5;
                        SpriteHelper.DrawEffect(spriteBatch, sprite, 8 * (typeModifier - 41) + (frame - 5), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    }
                    break;
                case DrawEffectType.Worm:
                    if (animationFrame <= 11)
                    {
                        SpriteHelper.DrawEffect(spriteBatch, 40, (staticFrame ? spriteFrame : animationFrame), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                        SpriteHelper.DrawEffect(spriteBatch, 41, (staticFrame ? spriteFrame : animationFrame), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color * 0.5f);
                        SpriteHelper.DrawEffect(spriteBatch, 44, (staticFrame ? spriteFrame : animationFrame), current.X -2 + 16, current.Y -3 + 16, -offsetX - x, -offsetY - y, color);
                        SpriteHelper.DrawEffect(spriteBatch, 44, (staticFrame ? spriteFrame : animationFrame), current.X -4 + 16, current.Y -3 + 16, -offsetX - x, -offsetY - y, color);
                    }
                    else
                        switch (animationFrame)
                        {
                            case 12:
                            case 13:
                            case 14: 
                                SpriteHelper.DrawEffect(spriteBatch, 40, 11, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                                break;
                            case 15: SpriteHelper.DrawEffect(spriteBatch, 40, 11, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color * 0.7f); break;
                            case 16: SpriteHelper.DrawEffect(spriteBatch, 40, 11, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color * 0.5f); break;
                            case 17: SpriteHelper.DrawEffect(spriteBatch, 40, 11, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color * 0.25f); break;
                        }
                    break;
                case DrawEffectType.EarthShockWave:
                    SpriteHelper.DrawEffect(spriteBatch, 91, (staticFrame ? spriteFrame : animationFrame), (destination.X * 32) + 16, (destination.Y * 32) + 16, -offsetX - x, -offsetY - y, color);
                    SpriteHelper.DrawEffect(spriteBatch, 92, (staticFrame ? spriteFrame : animationFrame), (destination.X * 32) + 16, (destination.Y * 32) + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.EarthShock:
                    SpriteHelper.DrawEffect(spriteBatch, 91, (staticFrame ? spriteFrame : animationFrame), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    SpriteHelper.DrawEffect(spriteBatch, 92, (staticFrame ? spriteFrame : animationFrame), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.Meteor:
                    if (animationFrame >= 0)
                    {
                        if (animationFrame > 4) frame = animationFrame / 4; else frame = animationFrame;
                        SpriteHelper.DrawEffect(spriteBatch, sprite, 15 + frame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                        SpriteHelper.DrawEffect(spriteBatch, sprite, frame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    }
                    break;
                case DrawEffectType.MeteorTrail:
                    frame = animationFrame;
                    if (animationFrame >= 0)
                    {
                        frame--;
                        SpriteHelper.DrawEffect(spriteBatch, sprite, 20+ frame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    }
                    break;
                case DrawEffectType.Debuff:
                    SpriteHelper.DrawEffect(spriteBatch, 55, (staticFrame ? spriteFrame : animationFrame), current.X + 16, current.Y + 45 + 16, -offsetX - x, -offsetY - y, color);
                    SpriteHelper.DrawEffect(spriteBatch, 54, (staticFrame ? spriteFrame : animationFrame), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.IllusionMovement:
                case DrawEffectType.Illusion:
                case DrawEffectType.Inhibition:
                    spriteFrame = (animationFrame -5)*(-3);
                    SpriteHelper.DrawEffect(spriteBatch, sprite, spriteFrame, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.RainSplash:
                    SpriteHelper.DrawEffect(spriteBatch, sprite, animationFrame + 20, current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
                case DrawEffectType.StormBlade:
                    SpriteHelper.DrawEffect(spriteBatch, sprite, (staticFrame ? spriteFrame : animationFrame), current.X + 70 + 16, current.Y + 70 + 16, -offsetX - x, -offsetY - y, color);
                    break;
                default:
                    if (sprite != -1)
                        SpriteHelper.DrawEffect(spriteBatch, sprite, (staticFrame ? spriteFrame : animationFrame), current.X + 16, current.Y + 16, -offsetX - x, -offsetY - y, color);
                    break;
            }

            // draw sub effects
            foreach (var subEffect in subEffects) subEffect.Draw(spriteBatch, display, x, y, offsetX, offsetY);

            return false;
        }

        private void DrawLightning(SpriteBatch spriteBatch, GameDisplay display, Location source, Location destination, Location random, int type)
        {
            int j, pX1, pY1, iX1, iY1, tX, tY;
            MotionDirection direction;
            int sX = pX1 = iX1 = tX = source.X;
            int sY = pY1 = iY1 = tY = source.Y;
            int rX = random.X;
            int rY = random.Y;
            int dX = destination.X;
            int dY = destination.Y;
            Color colour = Color.White * 0.5f;
            Color colour1 = new Color(100, 100, 200) * 0.25f;
            Color colour2 = new Color(3, 80, 150) * 0.25f;
            Color colour3 = Color.White * 0.5f;
            Color colour4 = new Color(83, 104, 120) * 0.25f;

            for (j = 0; j < 100; j++)
            {
                switch (type)
                {
                    case 1:
                        SpriteHelper.DrawLine(spriteBatch, pX1, pY1, iX1, iY1, colour);
                        SpriteHelper.DrawLine(spriteBatch, pX1 - 1, pY1, iX1 - 1, iY1, colour1);
                        SpriteHelper.DrawLine(spriteBatch, pX1 + 1, pY1, iX1 + 1, iY1, colour1);
                        SpriteHelper.DrawLine(spriteBatch, pX1, pY1 - 1, iX1, iY1 - 1, colour1);
                        SpriteHelper.DrawLine(spriteBatch, pX1, pY1 + 1, iX1, iY1 + 1, colour1);

                        SpriteHelper.DrawLine(spriteBatch, pX1 - 2, pY1, iX1 - 2, iY1, colour2);
                        SpriteHelper.DrawLine(spriteBatch, pX1 + 2, pY1, iX1 + 2, iY1, colour2);
                        SpriteHelper.DrawLine(spriteBatch, pX1, pY1 - 2, iX1, iY1 - 2, colour2);
                        SpriteHelper.DrawLine(spriteBatch, pX1, pY1 + 2, iX1, iY1 + 2, colour2);

                        SpriteHelper.DrawLine(spriteBatch, pX1 - 1, pY1 - 1, iX1 - 1, iY1 - 1, colour3);
                        SpriteHelper.DrawLine(spriteBatch, pX1 + 1, pY1 - 1, iX1 + 1, iY1 - 1, colour3);
                        SpriteHelper.DrawLine(spriteBatch, pX1 + 1, pY1 - 1, iX1 + 1, iY1 - 1, colour3);
                        SpriteHelper.DrawLine(spriteBatch, pX1 - 1, pY1 + 1, iX1 - 1, iY1 + 1, colour3);
                        break;

                    case 2:
                        //DrawLine2(pX1, pY1, iX1, iY1, wR4, wG4, wB4);
                        break;
                }


                int[] point = GameUtility.GetPoint(sX, sY, destination.X, destination.Y, j * 10);
                tX = point[0];
                tY = point[1];
                pX1 = iX1;
                pY1 = iY1;
                direction = Utility.GetNextDirection(iX1, iY1, tX, tY);
                switch (direction)
                {
                    case MotionDirection.North: rY -= 5; break;
                    case MotionDirection.NorthEast: rY -= 5; rX += 5; break;
                    case MotionDirection.East: rX += 5; break;
                    case MotionDirection.SouthEast: rX += 5; rY += 5; break;
                    case MotionDirection.South: rY += 5; break;
                    case MotionDirection.SouthWest: rX -= 5; rY += 5; break;
                    case MotionDirection.West: rX -= 5; break;
                    case MotionDirection.NorthWest: rX -= 5; rY -= 5; break;
                }
                if (rX < -20) rX = -20;
                if (rX > 20) rX = 20;
                if (rY < -20) rY = -20;
                if (rY > 20) rY = 20;
                iX1 = iX1 + rX;
                iY1 = iY1 + rY;
                if ((Math.Abs(tX - dX) < 5) && (Math.Abs(tY - dY) < 5)) break;
            }

            if (type == 1)
                SpriteHelper.DrawEffect(spriteBatch, 6, Dice.Roll(1, 2), iX1, iY1, 0, 0, Color.White);
        }

        public void Remove()
        {
            subEffects.Clear();
            Cache.DefaultState.Effects.Remove(this);
        }

        public Color Color { get { return color; } set { color = value; } }
        public Location Source { get { return source; } set { source = value; } }
        public Location Destination { get { return destination; } set { destination = value; } }
        public Location Pivot { get; set; }
        public bool IsComplete { get { return true; } }
    }
}
