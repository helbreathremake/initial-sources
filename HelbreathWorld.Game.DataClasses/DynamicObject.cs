﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;

namespace HelbreathWorld.Game.Assets
{
    public class DynamicObject
    {
        private int objectId;
        private DynamicObjectType type;

        private int maximumFrames;
        private int animationFrame;
        private DateTime animationTime;

        private MotionDirection direction;
        private Location offset; // offset for fish etc
        private Location random; // random for fish etc

        public DynamicObject(int objectId, DynamicObjectType type)
        {
            this.objectId = objectId;
            this.type = type;
            this.animationFrame = Dice.Roll(1, 5); // randomize

            switch (type)
            {
                case DynamicObjectType.Fire:
                case DynamicObjectType.FireBow: maximumFrames = 24; break;
                case DynamicObjectType.FireCrusade: maximumFrames = 27; break;
                case DynamicObjectType.SpikeField: maximumFrames = 13; break;
                case DynamicObjectType.IceStorm: maximumFrames = 10; break;
                case DynamicObjectType.Fish: 
                    maximumFrames = 15;
                    random = new Location(Dice.Roll(1, 40) - 20, Dice.Roll(1, 40) - 20);
                    offset = new Location(Dice.Roll(1, 10) - 5, Dice.Roll(1, 10) - 5);
                    break;
                case DynamicObjectType.PoisonCloudBegin:
                case DynamicObjectType.PoisonCloudLoop:
                case DynamicObjectType.PoisonCloudEnd: maximumFrames = 8; break;
                case DynamicObjectType.AresdenFlag:
                case DynamicObjectType.ElvineFlag: maximumFrames = 4; break;
            }
        }

        public void Init(int x, int y)
        {
            if (Map != null)
            {
                X = x;
                Y = y;
                Map[y][x].DynamicObject = this;

                Cache.DynamicObjects.Add(objectId, this);
            }
        }

        public void Remove()
        {
            // handle removal based on type
            switch (type)
            {
                case DynamicObjectType.PoisonCloudLoop:  // poison cloud has a "dissapating" effect before removal
                    type = DynamicObjectType.PoisonCloudEnd;
                    animationFrame = 0;
                    break;
                case DynamicObjectType.Fire:
                case DynamicObjectType.FireBow:
                    /* TODO - add fire effect
                        m_pGame->bAddNewEffect(15, (m_sPivotX+dX)*32, (m_sPivotY+dY)*32, NULL, NULL, 0, 0);
			            m_pGame->bAddNewEffect(15, (m_sPivotX+dX)*32 +(10-(rand()%20)), (m_sPivotY+dY)*32  +(20-(rand()%40)), NULL, NULL, 0, 0);
			            m_pGame->bAddNewEffect(15, (m_sPivotX+dX)*32 +(10-(rand()%20)), (m_sPivotY+dY)*32  +(20-(rand()%40)), NULL, NULL, 0, 0);
			            m_pGame->bAddNewEffect(15, (m_sPivotX+dX)*32 +(10-(rand()%20)), (m_sPivotY+dY)*32  +(20-(rand()%40)), NULL, NULL, 0, 0);
                     */
                    Map[Y][X].DynamicObject = null;
                    Cache.DynamicObjects.Remove(objectId);
                    break;
                default:
                    Map[Y][X].DynamicObject = null;
                    Cache.DynamicObjects.Remove(objectId);
                    break;
            }
        }

        public void Update()
        {
            if ((DateTime.Now - animationTime).Milliseconds > (90 + Dice.Roll(1,20))) // add a bit of random
            {
                animationFrame++;

                switch (type)
                {
                    case DynamicObjectType.Fire:
                    case DynamicObjectType.FireBow:
                    case DynamicObjectType.FireCrusade:
                        if (animationFrame == 1) AudioHelper.PlaySound("E9");
                        break;
                    case DynamicObjectType.PoisonCloudBegin:
                        if (animationFrame >= 8)
                        {
                            type = DynamicObjectType.PoisonCloudLoop;
                            animationFrame = Dice.Roll(1, 8);
                        }
                        break;
                    case DynamicObjectType.PoisonCloudLoop: break;
                    case DynamicObjectType.PoisonCloudEnd: 
                        if (animationFrame >= 8) Remove(); 
                        break;
                    case DynamicObjectType.Fish:
                        if (animationFrame >= maximumFrames) direction = (MotionDirection)Dice.Roll(1, 8);
                        direction = Utility.GetNextDirection(random.X, random.Y, 0, 0);
                        switch (direction)
                        {
                            case MotionDirection.North: offset.Y -= 2; break;
                            case MotionDirection.NorthEast: offset.Y -= 2; offset.X += 2; break;
                            case MotionDirection.East: offset.X += 2; break;
                            case MotionDirection.SouthEast: offset.X += 2; offset.Y += 2; break;
                            case MotionDirection.South: offset.Y += 2; break;
                            case MotionDirection.SouthWest: offset.X -= 2; offset.Y += 2; break;
                            case MotionDirection.West: offset.X -= 2; break;
                            case MotionDirection.NorthWest: offset.X -= 2; offset.Y -= 2; break;
                        }

                        offset.Y = (int)MathHelper.Clamp(offset.Y, -12, 12);
                        offset.X = (int)MathHelper.Clamp(offset.X, -12, 12);

                        random.X += offset.X;
                        random.Y += offset.Y;

                        if (Dice.Roll(1, 15) == 1)
                        { } // add effect 13
                        break;
                }

                if (animationFrame >= maximumFrames) animationFrame = 0;

                animationTime = DateTime.Now;
            }
        }

        public bool Draw(SpriteBatch spriteBatch, int x, int y, int playerOffsetX, int playerOffsetY)
        {
            bool isSelected = false;

            switch (type)
            {
                case DynamicObjectType.Fire:
                case DynamicObjectType.FireBow:
                    switch (Dice.Roll(1, 3))
                    {
                        case 0: SpriteHelper.DrawEffect(spriteBatch, 0, 1, x, y, playerOffsetX, playerOffsetY, Color.White * 0.25f); break; // light
                        case 1: SpriteHelper.DrawEffect(spriteBatch, 0, 1, x, y, playerOffsetX, playerOffsetY, Color.White * 0.5f); break; // light
                        case 2: SpriteHelper.DrawEffect(spriteBatch, 0, 1, x, y, playerOffsetX, playerOffsetY, Color.White * 0.7f); break; // light
                    }
                    SpriteHelper.DrawEffect(spriteBatch, 9, animationFrame / 3, x, y, playerOffsetX, playerOffsetY, Color.White * 0.7f);
                    break;
                case DynamicObjectType.SpikeField:
                    SpriteHelper.DrawEffect(spriteBatch, 17, animationFrame, x, y, playerOffsetX, playerOffsetY, Color.White * 0.7f);
                    break;
                case DynamicObjectType.IceStorm:
                    SpriteHelper.DrawEffect(spriteBatch, 0, 1, x, y, playerOffsetX, playerOffsetY, Color.White * 0.5f); // light
                    SpriteHelper.DrawEffect(spriteBatch, 13, animationFrame, x, y, playerOffsetX, playerOffsetY, Color.White * 0.7f);
                    break;
                case DynamicObjectType.PoisonCloudBegin:
                    SpriteHelper.DrawEffect(spriteBatch, 23, animationFrame, x + (Dice.Roll(1,2)), y + (Dice.Roll(1,2)), playerOffsetX, playerOffsetY, Color.White * 0.5f);
                    break;
                case DynamicObjectType.PoisonCloudLoop:
                    SpriteHelper.DrawEffect(spriteBatch, 23, animationFrame+8, x + (Dice.Roll(1,2)), y + (Dice.Roll(1,2)), playerOffsetX, playerOffsetY, Color.White * 0.5f);
                    break;
                case DynamicObjectType.PoisonCloudEnd:
                    SpriteHelper.DrawEffect(spriteBatch, 23, animationFrame + 16, x + (Dice.Roll(1, 2)), y + (Dice.Roll(1, 2)), playerOffsetX, playerOffsetY, Color.White * 0.5f);
                    break;
                case DynamicObjectType.Rock:
                    SpriteHelper.DrawDynamicObject(spriteBatch, (int)SpriteId.DynamicObject + 1, 0, x, y, playerOffsetX, playerOffsetY, Color.White, out isSelected);
                    break;
                case DynamicObjectType.Gem:
                    SpriteHelper.DrawDynamicObject(spriteBatch, (int)SpriteId.DynamicObject + 1, 1, x, y, playerOffsetX, playerOffsetY, Color.White, out isSelected);
                    break;
                case DynamicObjectType.Fish:
                    SpriteHelper.DrawDynamicObject(spriteBatch, (int)SpriteId.DynamicObject, ((((int)direction)-1)*4) + Dice.Roll(1,4)-1, x + offset.X, y + offset.Y, playerOffsetX, playerOffsetY, Color.White * 0.75f, out isSelected);
                    break;
            }

            return isSelected;
        }

        public Map Map;
        public int X;
        public int Y;
        public int ObjectId { get { return objectId; } set { objectId = value; } }
        public DynamicObjectType Type { get { return type; } }
        public bool IsTraversable
        {
            get
            {
                switch (Type)
                {
                    case DynamicObjectType.Rock:
                    case DynamicObjectType.Gem:
                        return false;
                    default: return true;
                }
            }
        }
    }
}
