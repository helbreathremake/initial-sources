﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace HelbreathWorld.Game.Assets
{
    public class AudioHelper
    {
        private static float GetDistance(int width, int height, int sourceX, int sourceY, int x, int y)
        {
            int rangeX = Math.Abs(x - sourceX);
            int rangeY = Math.Abs(y - sourceY);

            // get average distance and maximum distance
            float distance = (rangeX + rangeY)/2;
            float maxDistance = (width + height) / 2;

            if (distance > 0f && maxDistance > 0f)
                // calculator distance as percentage of 1.0f, maximum 100%
                return Math.Max(distance / maxDistance, 100.0f);
            else return 0f;
        }

        public static void PlaySound(string key, float soundVolume = 1.0f, float distance = 0.0f)
        {
            if (string.IsNullOrEmpty(key)) return;
            if (!Cache.GameSettings.SoundsOn) return;
            if (!Cache.SoundEffects.ContainsKey(key)) return;

            float volume = MathHelper.Clamp(Cache.GameSettings.SoundVolume / 100.0f, 0.0f, 1.0f);
            volume = MathHelper.Clamp(volume * soundVolume, 0.0f, 1.0f);
            volume = MathHelper.Clamp(volume - (volume * distance), 0.0f, 1.0f);

            Cache.SoundEffects[key].Play(volume, 0.0f, 0.0f);
        }

        public static void PlaySound(string key, int distanceX, int distanceY)
        {
            if (string.IsNullOrEmpty(key)) return;
            if (!Cache.GameSettings.SoundsOn) return;
            if (!Cache.SoundEffects.ContainsKey(key)) return;

            // volume is based on distance from player center
            float volume = MathHelper.Clamp(Cache.GameSettings.SoundVolume / 100.0f, 0.0f, 1.0f);

            // pan is based on distance from player center (x coord only)
            float pan = 0.0f;

            switch (Cache.DefaultState.Display.Resolution)
            {
                case Resolution.Classic:
                    // 3-4 cell average range before silence
                    volume += (((float)(Math.Abs(distanceX) + Math.Abs(distanceY)) / 2.0f) * -0.2f);
                    pan = distanceX / 8.0f; // 8 cells would be full left pan
                    break;
                case Resolution.Standard:
                    // 7-8 cell average range before silence
                    volume += (((float)(Math.Abs(distanceX) + Math.Abs(distanceY)) / 2.0f) * -0.1f);
                    pan = distanceX / 16.0f; // 16 cells would be full left pan
                    break;
                case Resolution.Large:
                    // 10-11 cell average range before silence
                    volume += (((float)(Math.Abs(distanceX) + Math.Abs(distanceY)) / 2.0f) * -0.075f);
                    pan = distanceX / 22.0f; // 22 cells would be full left pan
                    break;
            }

            if (volume > 0.2f)
            {
                Cache.SoundEffects[key].Play(volume, 0.0f, pan);
            }
        }

        public static void PlayMusic(string name)
        {
            if (!Cache.GameSettings.MusicOn) return;

            if (Cache.Music.ContainsKey(name))
            {
                if (Cache.BackgroundMusic != null &&
                !Cache.GameSettings.CurrentBGM.Equals(name) &&
                Cache.BackgroundMusic.State == SoundState.Playing)
                    StopMusic();

                Cache.BackgroundMusic = Cache.Music[name].CreateInstance();
                Cache.BackgroundMusic.IsLooped = true;
                Cache.BackgroundMusic.Play();
                Cache.GameSettings.CurrentBGM = name;
            }
        }

        public static void StopMusic()
        {
            if (Cache.BackgroundMusic != null)
            {
                if (!Cache.BackgroundMusic.IsDisposed)
                {
                    Cache.BackgroundMusic.Stop(true);
                    Cache.BackgroundMusic.Dispose();
                }
            }
        }
    }
}
