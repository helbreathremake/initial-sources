﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Game.Assets.UI;

namespace HelbreathWorld.Game.Assets
{
    public class GameKeyboard
    {
        private KeyboardState KeyState;
        private KeyboardState PrevKeyState;
        private List<Keys> newlyPressedKeys = new List<Keys>(); // Keys just pressed
        private List<Keys> heldKeys = new List<Keys>(); //Currently held down keys
        private List<Keys> heldKeyCheck = new List<Keys>(); // checks if pressed key has been processed
        private Keys[] oldPressedKeys;
        private Keys[] newPressedKeys;

        public KeyboardState CurrentKeyboardState
        {
            get { return KeyState; }
        }

        public KeyboardState PreviousKeyboardState
        {
            get { return PrevKeyState; }
        }
        public IList<Keys> NewlyPressedKeys
        {
            get { return newlyPressedKeys; }
        }

        public IList<Keys> HeldKeys
        {
            get { return heldKeys; }
        }

        public IList<Keys> HeldKeyCheck
        {
            get { return heldKeyCheck; }
        }

        public delegate void KeyEvent(Keys keys);
        public event KeyEvent OnKeyUp;
        public event KeyEvent OnKeyDown;

        private bool escPressed;
        private bool f1Pressed;
        private bool f2Pressed;
        private bool f3Pressed;
        private bool f4Pressed;
        private bool f5Pressed;
        private bool f6Pressed;
        private bool f7Pressed;
        private bool f8Pressed;
        private bool f9Pressed;
        private bool f11Pressed;
        private bool f12Pressed;
        private bool printScnPressed;
        private bool scrollLockPressed;
        private bool pauseBreakPressed;

        private bool tildePressed;
        private bool onePressed;
        private bool twoPressed;
        private bool threePressed;
        private bool fourPressed;
        private bool fivePressed;
        private bool sixPressed;
        private bool sevenPressed;
        private bool eightPressed;
        private bool ninePressed;
        private bool zeroPressed;
        private bool minusPressed;
        private bool plusPressed;
        private bool backPressed;

        private bool insertPressed;
        private bool homePressed;
        private bool pageUpPressed;
        private bool deletePressed;
        private bool endPressed;
        private bool pageDownPressed;

        private bool aPressed;
        private bool bPressed;
        private bool cPressed;
        private bool dPressed;
        private bool ePressed;
        private bool fPressed;
        private bool gPressed;
        private bool hPressed;
        private bool iPressed;
        private bool jPressed;
        private bool kPressed;
        private bool lPressed;
        private bool mPressed;
        private bool nPressed;
        private bool oPressed;
        private bool pPressed;
        private bool qPressed;
        private bool rPressed;
        private bool sPressed;
        private bool tPressed;
        private bool uPressed;
        private bool vPressed;
        private bool wPressed;
        private bool xPressed;
        private bool yPressed;
        private bool zPressed;

        private bool bracketOpenPressed;
        private bool bracketClosePressed;
        private bool semicolonPressed;
        private bool quotePressed;
        private bool commaPressed;
        private bool periodPressed;
        private bool questionPressed;

        private bool tabPressed;
        private bool capsLockPressed;
        private bool leftShiftPressed;
        private bool leftCtrlPressed;
        private bool leftAltPressed;
        private bool spacePressed;
        private bool rightAltPressed;
        private bool rightCtrlPressed;
        private bool rightShiftPressed;
        private bool enterPressed;
        private bool oempipePressed;

        private bool upPressed;
        private bool leftPressed;
        private bool downPressed;
        private bool rightPressed;

        private bool numPadZeroPressed;
        private bool numPadOnePressed;
        private bool numPadTwoPressed;
        private bool numPadThreePressed;
        private bool numPadFourPressed;
        private bool numPadFivePressed;
        private bool numPadSixPressed;
        private bool numPadSevenPressed;
        private bool numPadEightPressed;
        private bool numPadNinePressed;

        private bool numLockPressed;
        private bool dividePressed;
        private bool multiplyPressed;
        private bool subtractPressed;
        private bool addPressed;
        private bool decimalPressed;

        private Dictionary<Keys, bool> keyCheck;

        private KeyboardDispatcher dispatcher;

        public GameKeyboard()
        {
            keyCheck = new Dictionary<Keys, bool>();

            keyCheck.Add(Keys.Escape, false);
            keyCheck.Add(Keys.F1, false);
            keyCheck.Add(Keys.F2, false);
            keyCheck.Add(Keys.F3, false);
            keyCheck.Add(Keys.F4, false);
            keyCheck.Add(Keys.F5, false);
            keyCheck.Add(Keys.F6, false);
            keyCheck.Add(Keys.F7, false);
            keyCheck.Add(Keys.F8, false);
            keyCheck.Add(Keys.F9, false);
            keyCheck.Add(Keys.F11, false);
            keyCheck.Add(Keys.F12, false);
            keyCheck.Add(Keys.PrintScreen, false);
            keyCheck.Add(Keys.Scroll, false);
            keyCheck.Add(Keys.Pause, false);

            keyCheck.Add(Keys.OemTilde, false);
            keyCheck.Add(Keys.D1, false);
            keyCheck.Add(Keys.D2, false);
            keyCheck.Add(Keys.D3, false);
            keyCheck.Add(Keys.D4, false);
            keyCheck.Add(Keys.D5, false);
            keyCheck.Add(Keys.D6, false);
            keyCheck.Add(Keys.D7, false);
            keyCheck.Add(Keys.D8, false);
            keyCheck.Add(Keys.D9, false);
            keyCheck.Add(Keys.D0, false);
            keyCheck.Add(Keys.OemMinus, false);
            keyCheck.Add(Keys.OemPlus, false);
            keyCheck.Add(Keys.Back, false);

            keyCheck.Add(Keys.Insert, false);
            keyCheck.Add(Keys.Home, false);
            keyCheck.Add(Keys.PageUp, false);
            keyCheck.Add(Keys.Delete, false);
            keyCheck.Add(Keys.End, false);
            keyCheck.Add(Keys.PageDown, false);

            keyCheck.Add(Keys.A, false);
            keyCheck.Add(Keys.B, false);
            keyCheck.Add(Keys.C, false);
            keyCheck.Add(Keys.D, false);
            keyCheck.Add(Keys.E, false);
            keyCheck.Add(Keys.F, false);
            keyCheck.Add(Keys.G, false);
            keyCheck.Add(Keys.H, false);
            keyCheck.Add(Keys.I, false);
            keyCheck.Add(Keys.J, false);
            keyCheck.Add(Keys.K, false);
            keyCheck.Add(Keys.L, false);
            keyCheck.Add(Keys.M, false);
            keyCheck.Add(Keys.N, false);
            keyCheck.Add(Keys.O, false);
            keyCheck.Add(Keys.P, false);
            keyCheck.Add(Keys.Q, false);
            keyCheck.Add(Keys.R, false);
            keyCheck.Add(Keys.S, false);
            keyCheck.Add(Keys.T, false);
            keyCheck.Add(Keys.U, false);
            keyCheck.Add(Keys.V, false);
            keyCheck.Add(Keys.W, false);
            keyCheck.Add(Keys.X, false);
            keyCheck.Add(Keys.Y, false);
            keyCheck.Add(Keys.Z, false);

            keyCheck.Add(Keys.OemOpenBrackets, false);
            keyCheck.Add(Keys.OemCloseBrackets, false);
            keyCheck.Add(Keys.OemSemicolon, false);
            keyCheck.Add(Keys.OemQuotes, false);
            keyCheck.Add(Keys.OemComma, false);
            keyCheck.Add(Keys.OemPeriod, false);
            keyCheck.Add(Keys.OemQuestion, false);

            keyCheck.Add(Keys.Tab, false);
            keyCheck.Add(Keys.CapsLock, false);
            keyCheck.Add(Keys.LeftShift, false);
            keyCheck.Add(Keys.LeftControl, false);
            keyCheck.Add(Keys.LeftAlt, false);
            keyCheck.Add(Keys.Space, false);
            keyCheck.Add(Keys.RightAlt, false);
            keyCheck.Add(Keys.RightControl, false);
            keyCheck.Add(Keys.RightShift, false);
            keyCheck.Add(Keys.Enter, false);
            keyCheck.Add(Keys.OemPipe, false);

            keyCheck.Add(Keys.Up, false);
            keyCheck.Add(Keys.Left, false);
            keyCheck.Add(Keys.Down, false);
            keyCheck.Add(Keys.Right, false);

            keyCheck.Add(Keys.NumPad0, false);
            keyCheck.Add(Keys.NumPad1, false);
            keyCheck.Add(Keys.NumPad2, false);
            keyCheck.Add(Keys.NumPad3, false);
            keyCheck.Add(Keys.NumPad4, false);
            keyCheck.Add(Keys.NumPad5, false);
            keyCheck.Add(Keys.NumPad6, false);
            keyCheck.Add(Keys.NumPad7, false);
            keyCheck.Add(Keys.NumPad8, false);
            keyCheck.Add(Keys.NumPad9, false);

            keyCheck.Add(Keys.NumLock, false);
            keyCheck.Add(Keys.Divide, false);
            keyCheck.Add(Keys.Multiply, false);
            keyCheck.Add(Keys.Subtract, false);
            keyCheck.Add(Keys.Add, false);
            keyCheck.Add(Keys.Decimal, false);
        }

        public void Update(KeyboardState state)
        {
            PrevKeyState = KeyState;
            KeyState = state;

            oldPressedKeys = newPressedKeys;
            newPressedKeys = KeyState.GetPressedKeys();

            newlyPressedKeys.Clear();
            heldKeys.Clear();

             //compare old and new keys for keys that are now down
            foreach (Keys key in newPressedKeys)
            {
                if (oldPressedKeys != null && oldPressedKeys.Contains(key))
                    heldKeys.Add(key);
                else
                    newlyPressedKeys.Add(key);
            }
           

            //check if any held keys have now come up and remove the key check
            //for (int i = 0; i < heldKeyCheck.Count; i++) // cant use foreach enumeration because collection changes in HandleKeyboard can cause exception
            //    if (!heldKeys.Contains(heldKeyCheck[i]))
            //        heldKeyCheck.Remove(heldKeyCheck[i]);
        

            if (state.IsKeyDown(Keys.Escape)) escPressed = true;
            else if (state.IsKeyUp(Keys.Escape)) escPressed = false;

            if (state.IsKeyDown(Keys.F1)) f1Pressed = true;
            else if (state.IsKeyUp(Keys.F1)) f1Pressed = false;

            if (state.IsKeyDown(Keys.F2)) f2Pressed = true;
            else if (state.IsKeyUp(Keys.F2)) f2Pressed = false;

            if (state.IsKeyDown(Keys.F3)) f3Pressed = true;
            else if (state.IsKeyUp(Keys.F3)) f3Pressed = false;

            if (state.IsKeyDown(Keys.F4)) f4Pressed = true;
            else if (state.IsKeyUp(Keys.F4)) f4Pressed = false;

            if (state.IsKeyDown(Keys.F5)) f5Pressed = true;
            else if (state.IsKeyUp(Keys.F5)) f5Pressed = false;

            if (state.IsKeyDown(Keys.F6)) f6Pressed = true;
            else if (state.IsKeyUp(Keys.F6)) f6Pressed = false;

            if (state.IsKeyDown(Keys.F7)) f7Pressed = true;
            else if (state.IsKeyUp(Keys.F7)) f7Pressed = false;

            if (state.IsKeyDown(Keys.F8)) f8Pressed = true;
            else if (state.IsKeyUp(Keys.F8)) f8Pressed = false;

            if (state.IsKeyDown(Keys.F9)) f9Pressed = true;
            else if (state.IsKeyUp(Keys.F9)) f9Pressed = false;

            if (state.IsKeyDown(Keys.F11)) f11Pressed = true;
            else if (state.IsKeyUp(Keys.F11)) f11Pressed = false;

            if (state.IsKeyDown(Keys.F12)) f12Pressed = true;
            else if (state.IsKeyUp(Keys.F12)) f12Pressed = false;

            if (state.IsKeyDown(Keys.PrintScreen)) printScnPressed = true;
            else if (state.IsKeyUp(Keys.PrintScreen)) printScnPressed = false;

            if (state.IsKeyDown(Keys.Scroll)) scrollLockPressed = true;
            else if (state.IsKeyUp(Keys.Scroll)) scrollLockPressed = false;

            if (state.IsKeyDown(Keys.Pause)) pauseBreakPressed = true;
            else if (state.IsKeyUp(Keys.Pause)) pauseBreakPressed = false;

            if (state.IsKeyDown(Keys.OemTilde)) tildePressed = true;
            else if (state.IsKeyUp(Keys.OemTilde)) tildePressed = false;

            if (state.IsKeyDown(Keys.D1)) onePressed = true;
            else if (state.IsKeyUp(Keys.D1)) onePressed = false;

            if (state.IsKeyDown(Keys.D2)) twoPressed = true;
            else if (state.IsKeyUp(Keys.D2)) twoPressed = false;

            if (state.IsKeyDown(Keys.D3)) threePressed = true;
            else if (state.IsKeyUp(Keys.D3)) threePressed = false;

            if (state.IsKeyDown(Keys.D4)) fourPressed = true;
            else if (state.IsKeyUp(Keys.D4)) fourPressed = false;

            if (state.IsKeyDown(Keys.D5)) fivePressed = true;
            else if (state.IsKeyUp(Keys.D5)) fivePressed = false;

            if (state.IsKeyDown(Keys.D6)) sixPressed = true;
            else if (state.IsKeyUp(Keys.D6)) sixPressed = false;

            if (state.IsKeyDown(Keys.D7)) sevenPressed = true;
            else if (state.IsKeyUp(Keys.D7)) sevenPressed = false;

            if (state.IsKeyDown(Keys.D8)) eightPressed = true;
            else if (state.IsKeyUp(Keys.D8)) eightPressed = false;

            if (state.IsKeyDown(Keys.D9)) ninePressed = true;
            else if (state.IsKeyUp(Keys.D9)) ninePressed = false;

            if (state.IsKeyDown(Keys.D0)) zeroPressed = true;
            else if (state.IsKeyUp(Keys.D0)) zeroPressed = false;

            if (state.IsKeyDown(Keys.OemMinus)) minusPressed = true;
            else if (state.IsKeyUp(Keys.OemMinus)) minusPressed = false;

            if (state.IsKeyDown(Keys.OemPlus)) plusPressed = true;
            else if (state.IsKeyUp(Keys.OemPlus)) plusPressed = false;

            if (state.IsKeyDown(Keys.Back)) backPressed = true;
            else if (state.IsKeyUp(Keys.Back)) backPressed = false;

            if (state.IsKeyDown(Keys.Insert)) insertPressed = true;
            else if (state.IsKeyUp(Keys.Insert)) insertPressed = false;

            if (state.IsKeyDown(Keys.Home)) homePressed = true;
            else if (state.IsKeyUp(Keys.Home)) homePressed = false;

            if (state.IsKeyDown(Keys.PageUp)) pageUpPressed = true;
            else if (state.IsKeyUp(Keys.PageUp)) pageUpPressed = false;

            if (state.IsKeyDown(Keys.Delete)) deletePressed = true;
            else if (state.IsKeyUp(Keys.Delete)) deletePressed = false;

            if (state.IsKeyDown(Keys.End)) endPressed = true;
            else if (state.IsKeyUp(Keys.End)) endPressed = false;

            if (state.IsKeyDown(Keys.PageDown)) pageDownPressed = true;
            else if (state.IsKeyUp(Keys.PageDown)) pageDownPressed = false;

            if (state.IsKeyDown(Keys.A)) aPressed = true;
            else if (state.IsKeyUp(Keys.A)) aPressed = false;

            if (state.IsKeyDown(Keys.B)) bPressed = true;
            else if (state.IsKeyUp(Keys.B)) bPressed = false;

            if (state.IsKeyDown(Keys.C)) cPressed = true;
            else if (state.IsKeyUp(Keys.C)) cPressed = false;

            if (state.IsKeyDown(Keys.D)) dPressed = true;
            else if (state.IsKeyUp(Keys.D)) dPressed = false;

            if (state.IsKeyDown(Keys.E)) ePressed = true;
            else if (state.IsKeyUp(Keys.E)) ePressed = false;

            if (state.IsKeyDown(Keys.F)) fPressed = true;
            else if (state.IsKeyUp(Keys.F)) fPressed = false;

            if (state.IsKeyDown(Keys.G)) gPressed = true;
            else if (state.IsKeyUp(Keys.G)) gPressed = false;

            if (state.IsKeyDown(Keys.H)) hPressed = true;
            else if (state.IsKeyUp(Keys.H)) hPressed = false;

            if (state.IsKeyDown(Keys.I)) iPressed = true;
            else if (state.IsKeyUp(Keys.I)) iPressed = false;

            if (state.IsKeyDown(Keys.J)) jPressed = true;
            else if (state.IsKeyUp(Keys.J)) jPressed = false;

            if (state.IsKeyDown(Keys.K)) kPressed = true;
            else if (state.IsKeyUp(Keys.K)) kPressed = false;

            if (state.IsKeyDown(Keys.L)) lPressed = true;
            else if (state.IsKeyUp(Keys.L)) lPressed = false;

            if (state.IsKeyDown(Keys.M)) mPressed = true;
            else if (state.IsKeyUp(Keys.M)) mPressed = false;

            if (state.IsKeyDown(Keys.N)) nPressed = true;
            else if (state.IsKeyUp(Keys.N)) nPressed = false;

            if (state.IsKeyDown(Keys.O)) oPressed = true;
            else if (state.IsKeyUp(Keys.O)) oPressed = false;

            if (state.IsKeyDown(Keys.P)) pPressed = true;
            else if (state.IsKeyUp(Keys.P)) pPressed = false;

            if (state.IsKeyDown(Keys.Q)) qPressed = true;
            else if (state.IsKeyUp(Keys.Q)) qPressed = false;

            if (state.IsKeyDown(Keys.R)) rPressed = true;
            else if (state.IsKeyUp(Keys.R)) rPressed = false;

            if (state.IsKeyDown(Keys.S)) sPressed = true;
            else if (state.IsKeyUp(Keys.S)) sPressed = false;

            if (state.IsKeyDown(Keys.T)) tPressed = true;
            else if (state.IsKeyUp(Keys.T)) tPressed = false;

            if (state.IsKeyDown(Keys.U)) uPressed = true;
            else if (state.IsKeyUp(Keys.U)) uPressed = false;

            if (state.IsKeyDown(Keys.V)) vPressed = true;
            else if (state.IsKeyUp(Keys.V)) vPressed = false;

            if (state.IsKeyDown(Keys.W)) wPressed = true;
            else if (state.IsKeyUp(Keys.W)) wPressed = false;

            if (state.IsKeyDown(Keys.X)) xPressed = true;
            else if (state.IsKeyUp(Keys.X)) xPressed = false;

            if (state.IsKeyDown(Keys.Y)) yPressed = true;
            else if (state.IsKeyUp(Keys.Y)) yPressed = false;

            if (state.IsKeyDown(Keys.Z)) zPressed = true;
            else if (state.IsKeyUp(Keys.Z)) zPressed = false;

            if (state.IsKeyDown(Keys.OemOpenBrackets)) bracketOpenPressed = true;
            else if (state.IsKeyUp(Keys.OemOpenBrackets)) bracketOpenPressed = false;

            if (state.IsKeyDown(Keys.OemCloseBrackets)) bracketClosePressed = true;
            else if (state.IsKeyUp(Keys.OemCloseBrackets)) bracketClosePressed = false;

            if (state.IsKeyDown(Keys.OemSemicolon)) semicolonPressed = true;
            else if (state.IsKeyUp(Keys.OemSemicolon)) semicolonPressed = false;

            if (state.IsKeyDown(Keys.OemQuotes)) quotePressed = true;
            else if (state.IsKeyUp(Keys.OemQuotes)) quotePressed = false;

            if (state.IsKeyDown(Keys.OemComma)) commaPressed = true;
            else if (state.IsKeyUp(Keys.OemComma)) commaPressed = false;

            if (state.IsKeyDown(Keys.OemPeriod)) periodPressed = true;
            else if (state.IsKeyUp(Keys.OemPeriod)) periodPressed = false;

            if (state.IsKeyDown(Keys.OemQuestion)) questionPressed = true;
            else if (state.IsKeyUp(Keys.OemQuestion)) questionPressed = false;

            if (state.IsKeyDown(Keys.Tab)) tabPressed = true;
            else if (state.IsKeyUp(Keys.Tab)) tabPressed = false;

            if (state.IsKeyDown(Keys.CapsLock)) capsLockPressed = true;
            else if (state.IsKeyUp(Keys.CapsLock)) capsLockPressed = false;

            if (state.IsKeyDown(Keys.LeftShift)) leftShiftPressed = true;
            else if (state.IsKeyUp(Keys.LeftShift)) leftShiftPressed = false;

            if (state.IsKeyDown(Keys.LeftControl)) leftCtrlPressed = true;
            else if (state.IsKeyUp(Keys.LeftControl)) leftCtrlPressed = false;

            if (state.IsKeyDown(Keys.LeftAlt)) leftAltPressed = true;
            else if (state.IsKeyUp(Keys.LeftAlt)) leftAltPressed = false;

            if (state.IsKeyDown(Keys.Space)) spacePressed = true;
            else if (state.IsKeyUp(Keys.Space)) spacePressed = false;

            if (state.IsKeyDown(Keys.RightAlt)) rightAltPressed = true;
            else if (state.IsKeyUp(Keys.RightAlt)) rightAltPressed = false;

            if (state.IsKeyDown(Keys.RightControl)) rightCtrlPressed = true;
            else if (state.IsKeyUp(Keys.RightControl)) rightCtrlPressed = false;

            if (state.IsKeyDown(Keys.RightShift)) rightShiftPressed = true;
            else if (state.IsKeyUp(Keys.RightShift)) rightShiftPressed = false;

            if (state.IsKeyDown(Keys.Enter)) enterPressed = true;
            else if (state.IsKeyUp(Keys.Enter)) enterPressed = false;

            if (state.IsKeyDown(Keys.OemPipe)) oempipePressed = true;
            else if (state.IsKeyUp(Keys.OemPipe)) oempipePressed = false;

            if (state.IsKeyDown(Keys.Up)) upPressed = true;
            else if (state.IsKeyUp(Keys.Up)) upPressed = false;

            if (state.IsKeyDown(Keys.Left)) leftPressed = true;
            else if (state.IsKeyUp(Keys.Left)) leftPressed = false;

            if (state.IsKeyDown(Keys.Down)) downPressed = true;
            else if (state.IsKeyUp(Keys.Down)) downPressed = false;

            if (state.IsKeyDown(Keys.Right)) rightPressed = true;
            else if (state.IsKeyUp(Keys.Right)) rightPressed = false;

            if (state.IsKeyDown(Keys.NumPad0)) numPadZeroPressed = true;
            else if (state.IsKeyUp(Keys.NumPad0)) numPadZeroPressed = false;

            if (state.IsKeyDown(Keys.NumPad1)) numPadOnePressed = true;
            else if (state.IsKeyUp(Keys.NumPad1)) numPadOnePressed = false;

            if (state.IsKeyDown(Keys.NumPad2)) numPadTwoPressed = true;
            else if (state.IsKeyUp(Keys.NumPad2)) numPadTwoPressed = false;

            if (state.IsKeyDown(Keys.NumPad3)) numPadThreePressed = true;
            else if (state.IsKeyUp(Keys.NumPad3)) numPadThreePressed = false;

            if (state.IsKeyDown(Keys.NumPad4)) numPadFourPressed = true;
            else if (state.IsKeyUp(Keys.NumPad4)) numPadFourPressed = false;

            if (state.IsKeyDown(Keys.NumPad5)) numPadFivePressed = true;
            else if (state.IsKeyUp(Keys.NumPad5)) numPadFivePressed = false;

            if (state.IsKeyDown(Keys.NumPad6)) numPadSixPressed = true;
            else if (state.IsKeyUp(Keys.NumPad6)) numPadSixPressed = false;

            if (state.IsKeyDown(Keys.NumPad7)) numPadSevenPressed = true;
            else if (state.IsKeyUp(Keys.NumPad7)) numPadSevenPressed = false;

            if (state.IsKeyDown(Keys.NumPad8)) numPadEightPressed = true;
            else if (state.IsKeyUp(Keys.NumPad8)) numPadEightPressed = false;

            if (state.IsKeyDown(Keys.NumPad9)) numPadNinePressed = true;
            else if (state.IsKeyUp(Keys.NumPad9)) numPadNinePressed = false;

            if (state.IsKeyDown(Keys.NumLock)) numLockPressed = true;
            else if (state.IsKeyUp(Keys.NumLock)) numLockPressed = false;

            if (state.IsKeyDown(Keys.Divide)) dividePressed = true;
            else if (state.IsKeyUp(Keys.Divide)) dividePressed = false;

            if (state.IsKeyDown(Keys.Multiply)) multiplyPressed = true;
            else if (state.IsKeyUp(Keys.Multiply)) multiplyPressed = false;

            if (state.IsKeyDown(Keys.Subtract)) subtractPressed = true;
            else if (state.IsKeyUp(Keys.Subtract)) subtractPressed = false;

            if (state.IsKeyDown(Keys.Add)) addPressed = true;
            else if (state.IsKeyUp(Keys.Add)) addPressed = false;

            if (state.IsKeyDown(Keys.Decimal)) decimalPressed = true;
            else if (state.IsKeyUp(Keys.Decimal)) decimalPressed = false;

        }

        public bool Esc { get { return escPressed; } set { escPressed = value; } }
        public bool F1 { get { return f1Pressed; } set { f1Pressed = value; } }
        public bool F2 { get { return f2Pressed; } set { f2Pressed = value; } }
        public bool F3 { get { return f3Pressed; } set { f3Pressed = value; } }
        public bool F4 { get { return f4Pressed; } set { f4Pressed = value; } }
        public bool F5 { get { return f5Pressed; } set { f5Pressed = value; } }
        public bool F6 { get { return f6Pressed; } set { f6Pressed = value; } }
        public bool F7 { get { return f7Pressed; } set { f7Pressed = value; } }
        public bool F8 { get { return f8Pressed; } set { f8Pressed = value; } }
        public bool F9 { get { return f9Pressed; } set { f9Pressed = value; } }
        public bool F11 { get { return f11Pressed; } set { f11Pressed = value; } }
        public bool F12 { get { return f12Pressed; } set { f12Pressed = value; } }
        public bool PrintScn { get { return printScnPressed; } set { printScnPressed = value; } }
        public bool ScrollLock { get { return scrollLockPressed; } set { scrollLockPressed = value; } }
        public bool PauseBreak { get { return pauseBreakPressed; } set { pauseBreakPressed = value; } }

        public bool Tilde { get { return tildePressed; } set { tildePressed = value; } }
        public bool One { get { return onePressed; } set { onePressed = value; } }
        public bool Two { get { return twoPressed; } set { twoPressed = value; } }
        public bool Three { get { return threePressed; } set { threePressed = value; } }
        public bool Four { get { return fourPressed; } set { fourPressed = value; } }
        public bool Five { get { return fivePressed; } set { fivePressed = value; } }
        public bool Six { get { return sixPressed; } set { sixPressed = value; } }
        public bool Seven { get { return sevenPressed; } set { sevenPressed = value; } }
        public bool Eight { get { return eightPressed; } set { eightPressed = value; } }
        public bool Nine { get { return ninePressed; } set { ninePressed = value; } }
        public bool Zero { get { return zeroPressed; } set { zeroPressed = value; } }
        public bool Minus { get { return minusPressed; } set { minusPressed = value; } }
        public bool Plus { get { return plusPressed; } set { plusPressed = value; } }
        public bool Back { get { return backPressed; } set { backPressed = value; } }

        public bool Insert { get { return insertPressed; } set { insertPressed = value; } }
        public bool Home { get { return homePressed; } set { homePressed = value; } }
        public bool PageUp { get { return pageUpPressed; } set { pageUpPressed = value; } }
        public bool Delete { get { return deletePressed; } set { deletePressed = value; } }
        public bool End { get { return endPressed; } set { endPressed = value; } }
        public bool PageDown { get { return pageDownPressed; } set { pageDownPressed = value; } }

        public bool A { get { return aPressed; } set { aPressed = value; } }
        public bool B { get { return bPressed; } set { bPressed = value; } }
        public bool C { get { return cPressed; } set { cPressed = value; } }
        public bool D { get { return dPressed; } set { dPressed = value; } }
        public bool E { get { return ePressed; } set { ePressed = value; } }
        public bool F { get { return fPressed; } set { fPressed = value; } }
        public bool G { get { return gPressed; } set { gPressed = value; } }
        public bool H { get { return hPressed; } set { hPressed = value; } }
        public bool I { get { return iPressed; } set { iPressed = value; } }
        public bool J { get { return jPressed; } set { jPressed = value; } }
        public bool K { get { return kPressed; } set { kPressed = value; } }
        public bool L { get { return lPressed; } set { lPressed = value; } }
        public bool M { get { return mPressed; } set { mPressed = value; } }
        public bool N { get { return nPressed; } set { nPressed = value; } }
        public bool O { get { return oPressed; } set { oPressed = value; } }
        public bool P { get { return pPressed; } set { pPressed = value; } }
        public bool Q { get { return qPressed; } set { qPressed = value; } }
        public bool R { get { return rPressed; } set { rPressed = value; } }
        public bool S { get { return sPressed; } set { sPressed = value; } }
        public bool T { get { return tPressed; } set { tPressed = value; } }
        public bool U { get { return uPressed; } set { uPressed = value; } }
        public bool V { get { return vPressed; } set { vPressed = value; } }
        public bool W { get { return wPressed; } set { wPressed = value; } }
        public bool X { get { return xPressed; } set { xPressed = value; } }
        public bool Y { get { return yPressed; } set { yPressed = value; } }
        public bool Z { get { return zPressed; } set { zPressed = value; } }

        public bool BracketOpen { get { return bracketOpenPressed; } set { bracketOpenPressed = value; } }
        public bool BracketClose { get { return bracketClosePressed; } set { bracketClosePressed = value; } }
        public bool Semicolon { get { return semicolonPressed; } set { semicolonPressed = value; } }
        public bool Quote { get { return quotePressed; } set { quotePressed = value; } }
        public bool Comma { get { return commaPressed; } set { commaPressed = value; } }
        public bool Period { get { return periodPressed; } set { periodPressed = value; } }
        public bool Question { get { return questionPressed; } set { questionPressed = value; } }

        public bool Tab { get { return tabPressed; } set { tabPressed = value; } }
        public bool CapsLock { get { return capsLockPressed; } set { capsLockPressed = value; } }
        public bool LeftShift { get { return leftShiftPressed; } set { leftShiftPressed = value; } }
        public bool LeftCtrl { get { return leftCtrlPressed; } set { leftCtrlPressed = value; } }
        public bool LeftAlt { get { return leftAltPressed; } set { leftAltPressed = value; } }
        public bool Space { get { return spacePressed; } set { spacePressed = value; } }
        public bool RightAlt { get { return rightAltPressed; } set { rightAltPressed = value; } }
        public bool RightCtrl { get { return rightCtrlPressed; } set { rightCtrlPressed = value; } }
        public bool RightShift { get { return rightShiftPressed; } set { rightShiftPressed = value; } }
        public bool Enter { get { return enterPressed; } set { enterPressed = value; } }
        public bool Oempipe { get { return oempipePressed; } set { oempipePressed = value; } }

        public bool Up { get { return upPressed; } set { upPressed = value; } }
        public bool Left { get { return leftPressed; } set { leftPressed = value; } }
        public bool Down { get { return downPressed; } set { downPressed = value; } }
        public bool Right { get { return rightPressed; } set { rightPressed = value; } }

        public bool NumPadZero { get { return numPadZeroPressed; } set { numPadZeroPressed = value; } }
        public bool NumPadOne { get { return numPadOnePressed; } set { numPadOnePressed = value; } }
        public bool NumPadTwo { get { return numPadTwoPressed; } set { numPadTwoPressed = value; } }
        public bool NumPadThree { get { return numPadThreePressed; } set { numPadThreePressed = value; } }
        public bool NumPadFour { get { return numPadFourPressed; } set { numPadFourPressed = value; } }
        public bool NumPadFive { get { return numPadFivePressed; } set { numPadFivePressed = value; } }
        public bool NumPadSix { get { return numPadSixPressed; } set { numPadSixPressed = value; } }
        public bool NumPadSeven { get { return numPadSevenPressed; } set { numPadSevenPressed = value; } }
        public bool NumPadEight { get { return numPadEightPressed; } set { numPadEightPressed = value; } }
        public bool NumPadNine { get { return numPadNinePressed; } set { numPadNinePressed = value; } }

        public bool NumLock { get { return numLockPressed; } set { numLockPressed = value; } }
        public bool Divide { get { return dividePressed; } set { dividePressed = value; } }
        public bool Multiply { get { return multiplyPressed; } set { multiplyPressed = value; } }
        public bool Subtract { get { return subtractPressed; } set { subtractPressed = value; } }
        public bool Add { get { return addPressed; } set { addPressed = value; } }
        public bool Decimal { get { return decimalPressed; } set { decimalPressed = value; } }

        public Dictionary<Keys, bool> KeyCheck { get { return keyCheck; } set { keyCheck = value; } }
        public KeyboardDispatcher Dispatcher { get { return dispatcher; } set { dispatcher = value; } }

        public bool IsNewKeyRelease(Keys key)
        {
            return (CurrentKeyboardState.IsKeyUp(key) && PreviousKeyboardState.IsKeyDown(key));
        }

        public bool IsNewKeyPress(Keys key)
        {
            return (CurrentKeyboardState.IsKeyDown(key) && PreviousKeyboardState.IsKeyUp(key));
        }

        public bool IsKeyPressed(Keys key)
        {
            return CurrentKeyboardState.IsKeyDown(key);
        }
    }
}

//Left this here incase we need it later for some reason

//    //{
//        /* PRESS ESC */
//        if (Display.Keyboard.Esc) Display.Keyboard.KeyCheck[Keys.Escape] = true;
//        if (!Display.Keyboard.Esc && Display.Keyboard.KeyCheck[Keys.Escape])
//        {

//            Display.Keyboard.KeyCheck[Keys.Escape] = false;
//        }
//        /* PRESS F1 */
//        if (Display.Keyboard.F1) Display.Keyboard.KeyCheck[Keys.F1] = true;
//        if (!Display.Keyboard.F1 && Display.Keyboard.KeyCheck[Keys.F1])
//        {
//            string key = "F1";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F1] = false;
//        }
//        /* PRESS F2 */
//        if (Display.Keyboard.F2) Display.Keyboard.KeyCheck[Keys.F2] = true;
//        if (!Display.Keyboard.F2 && Display.Keyboard.KeyCheck[Keys.F2])
//        {
//            string key = "F2";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F2] = false;
//        }
//        /* PRESS F3 */
//        if (Display.Keyboard.F3) Display.Keyboard.KeyCheck[Keys.F3] = true;
//        if (!Display.Keyboard.F3 && Display.Keyboard.KeyCheck[Keys.F3])
//        {
//            string key = "F3";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F3] = false;
//        }
//        /* PRESS F4 */
//        if (Display.Keyboard.F4) Display.Keyboard.KeyCheck[Keys.F4] = true;
//        if (!Display.Keyboard.F4 && Display.Keyboard.KeyCheck[Keys.F4])
//        {
//            string key = "F4";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F5] = false;
//        }
//        /* PRESS F5 */
//        if (Display.Keyboard.F5) Display.Keyboard.KeyCheck[Keys.F5] = true;
//        if (!Display.Keyboard.F5 && Display.Keyboard.KeyCheck[Keys.F5])
//        {
//            string key = "F5";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F5] = false;
//        }
//        /* PRESS F6 */
//        if (Display.Keyboard.F6) Display.Keyboard.KeyCheck[Keys.F6] = true;
//        if (!Display.Keyboard.F6 && Display.Keyboard.KeyCheck[Keys.F6])
//        {
//            string key = "F6";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F6] = false;
//        }
//        /* PRESS F7 */
//        if (Display.Keyboard.F7) Display.Keyboard.KeyCheck[Keys.F7] = true;
//        if (!Display.Keyboard.F7 && Display.Keyboard.KeyCheck[Keys.F7])
//        {
//            string key = "F7";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F7] = false;
//        }
//        /* PRESS F8 */
//        if (Display.Keyboard.F8) Display.Keyboard.KeyCheck[Keys.F8] = true;
//        if (!Display.Keyboard.F8 && Display.Keyboard.KeyCheck[Keys.F8])
//        {
//            string key = "F8";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F8] = false;
//        }
//        /* PRESS F9 */
//        if (Display.Keyboard.F9) Display.Keyboard.KeyCheck[Keys.F9] = true;
//        if (!Display.Keyboard.F9 && Display.Keyboard.KeyCheck[Keys.F9])
//        {
//            string key = "F9";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F9] = false;
//        }
//        /* PRESS F11 */
//        if (Display.Keyboard.F11) Display.Keyboard.KeyCheck[Keys.F11] = true;
//        if (!Display.Keyboard.F11 && Display.Keyboard.KeyCheck[Keys.F11])
//        {
//            string key = "F11";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F11] = false;
//        }
//        /* PRESS F5 */
//        if (Display.Keyboard.F12) Display.Keyboard.KeyCheck[Keys.F12] = true;
//        if (!Display.Keyboard.F12 && Display.Keyboard.KeyCheck[Keys.F12])
//        {
//            string key = "F12";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F12] = false;
//        }
//        /* PRESS PRINTSCN */
//        if (Display.Keyboard.PrintScn) Display.Keyboard.KeyCheck[Keys.PrintScreen] = true;
//        if (!Display.Keyboard.PrintScn && Display.Keyboard.KeyCheck[Keys.PrintScreen])
//        {
//            string key = "PRINTSCN";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.PrintScreen] = false;
//        }
//        /* PRESS SCROLLLOCK */
//        if (Display.Keyboard.ScrollLock) Display.Keyboard.KeyCheck[Keys.Scroll] = true;
//        if (!Display.Keyboard.ScrollLock && Display.Keyboard.KeyCheck[Keys.Scroll])
//        {
//            string key = "SCROLLLOCK";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Scroll] = false;
//        }
//        /* PRESS PAUSEBREAK */
//        if (Display.Keyboard.PauseBreak) Display.Keyboard.KeyCheck[Keys.Pause] = true;
//        if (!Display.Keyboard.PauseBreak && Display.Keyboard.KeyCheck[Keys.Pause])
//        {
//            string key = "PAUSEBREAK";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Pause] = false;
//        }

//        /* PRESS TILDE */
//        if (Display.Keyboard.Tilde) Display.Keyboard.KeyCheck[Keys.OemTilde] = true;
//        if (!Display.Keyboard.Tilde && Display.Keyboard.KeyCheck[Keys.OemTilde])
//        {
//            string key = "TILDE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemTilde] = false;
//        }
//        /* PRESS ONE */
//        if (Display.Keyboard.One) Display.Keyboard.KeyCheck[Keys.D1] = true;
//        if (!Display.Keyboard.One && Display.Keyboard.KeyCheck[Keys.D1])
//        {
//            string key = "ONE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D1] = false;
//        }
//        /* PRESS TWO */
//        if (Display.Keyboard.Two) Display.Keyboard.KeyCheck[Keys.D2] = true;
//        if (!Display.Keyboard.Two && Display.Keyboard.KeyCheck[Keys.D2])
//        {
//            string key = "TWO";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D2] = false;
//        }
//        /* PRESS THREE */
//        if (Display.Keyboard.Three) Display.Keyboard.KeyCheck[Keys.D3] = true;
//        if (!Display.Keyboard.Three && Display.Keyboard.KeyCheck[Keys.D3])
//        {
//            string key = "THREE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D3] = false;
//        }
//        /* PRESS FOUR */
//        if (Display.Keyboard.Four) Display.Keyboard.KeyCheck[Keys.D4] = true;
//        if (!Display.Keyboard.Four && Display.Keyboard.KeyCheck[Keys.D4])
//        {
//            string key = "FOUR";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D4] = false;
//        }
//        /* PRESS FIVE */
//        if (Display.Keyboard.Five) Display.Keyboard.KeyCheck[Keys.D5] = true;
//        if (!Display.Keyboard.Five && Display.Keyboard.KeyCheck[Keys.D5])
//        {
//            string key = "FIVE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D5] = false;
//        }
//        /* PRESS SIX */
//        if (Display.Keyboard.Six) Display.Keyboard.KeyCheck[Keys.D6] = true;
//        if (!Display.Keyboard.Six && Display.Keyboard.KeyCheck[Keys.D6])
//        {
//            string key = "SIX";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D6] = false;
//        }
//        /* PRESS SEVEN */
//        if (Display.Keyboard.Seven) Display.Keyboard.KeyCheck[Keys.D7] = true;
//        if (!Display.Keyboard.Seven && Display.Keyboard.KeyCheck[Keys.D7])
//        {
//            string key = "SEVEN";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D7] = false;
//        }
//        /* PRESS EIGHT */
//        if (Display.Keyboard.Eight) Display.Keyboard.KeyCheck[Keys.D8] = true;
//        if (!Display.Keyboard.Eight && Display.Keyboard.KeyCheck[Keys.D8])
//        {
//            string key = "EIGHT";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D8] = false;
//        }
//        /* PRESS NINE */
//        if (Display.Keyboard.Nine) Display.Keyboard.KeyCheck[Keys.D9] = true;
//        if (!Display.Keyboard.Nine && Display.Keyboard.KeyCheck[Keys.D9])
//        {
//            string key = "NINE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D9] = false;
//        }
//        /* PRESS ZERO */
//        if (Display.Keyboard.Zero) Display.Keyboard.KeyCheck[Keys.D0] = true;
//        if (!Display.Keyboard.Zero && Display.Keyboard.KeyCheck[Keys.D0])
//        {
//            string key = "ZERO";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D0] = false;
//        }
//        /* PRESS MINUS */
//        if (Display.Keyboard.Minus) Display.Keyboard.KeyCheck[Keys.OemMinus] = true;
//        if (!Display.Keyboard.Minus && Display.Keyboard.KeyCheck[Keys.OemMinus])
//        {
//            string key = "MINUS";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemMinus] = false;
//        }

//        /* PRESS PLUS */
//        if (Display.Keyboard.Plus) Display.Keyboard.KeyCheck[Keys.OemPlus] = true;
//        if (!Display.Keyboard.Plus && Display.Keyboard.KeyCheck[Keys.OemPlus])
//        {
//            string key = "PLUS";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemPlus] = false;
//        }

//        /* PRESS BACK */
//        if (Display.Keyboard.Back) Display.Keyboard.KeyCheck[Keys.Back] = true;
//        if (!Display.Keyboard.Back && Display.Keyboard.KeyCheck[Keys.Back])
//        {
//            string key = "BACK";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Back] = false;
//        }
//        /* PRESS INSERT*/
//        if (Display.Keyboard.Insert) Display.Keyboard.KeyCheck[Keys.Insert] = true;
//        if (!Display.Keyboard.Insert && Display.Keyboard.KeyCheck[Keys.Insert])
//        {
//            string key = "INSERT";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Insert] = false;
//        }
//        /* PRESS HOME*/
//        if (Display.Keyboard.Home) Display.Keyboard.KeyCheck[Keys.Home] = true;
//        if (!Display.Keyboard.Home && Display.Keyboard.KeyCheck[Keys.Home])
//        {
//            string key = "HOME";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Home] = false;
//        }
//        /* PRESS PAGEUP*/
//        if (Display.Keyboard.PageUp) Display.Keyboard.KeyCheck[Keys.PageUp] = true;
//        if (!Display.Keyboard.PageUp && Display.Keyboard.KeyCheck[Keys.PageUp])
//        {
//            string key = "PAGEUP";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.PageUp] = false;
//        }
//        /* PRESS DELETE*/
//        if (Display.Keyboard.Delete) Display.Keyboard.KeyCheck[Keys.Delete] = true;
//        if (!Display.Keyboard.Delete && Display.Keyboard.KeyCheck[Keys.Delete])
//        {
//            string key = "DELETE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Delete] = false;
//        }
//        /* PRESS END*/
//        if (Display.Keyboard.End) Display.Keyboard.KeyCheck[Keys.End] = true;
//        if (!Display.Keyboard.End && Display.Keyboard.KeyCheck[Keys.End])
//        {
//            string key = "END";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.End] = false;
//        }
//        /* PRESS PAGEDOWN */
//        if (Display.Keyboard.PageDown) Display.Keyboard.KeyCheck[Keys.PageDown] = true;
//        if (!Display.Keyboard.PageDown && Display.Keyboard.KeyCheck[Keys.PageDown])
//        {
//            string key = "PAGEDOWN";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.PageDown] = false;
//        }
//        /* PRESS A */
//        if (Display.Keyboard.A) Display.Keyboard.KeyCheck[Keys.A] = true;
//        if (!Display.Keyboard.A && Display.Keyboard.KeyCheck[Keys.A])
//        {
//            string key = "A";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.A] = false;
//        }
//        /* PRESS B */
//        if (Display.Keyboard.B) Display.Keyboard.KeyCheck[Keys.B] = true;
//        if (!Display.Keyboard.B && Display.Keyboard.KeyCheck[Keys.B])
//        {
//            string key = "B";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.B] = false;
//        }
//        /* PRESS C */
//        if (Display.Keyboard.C) Display.Keyboard.KeyCheck[Keys.C] = true;
//        if (!Display.Keyboard.C && Display.Keyboard.KeyCheck[Keys.C])
//        {
//            string key = "C";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.C] = false;
//        }
//        /* PRESS D */
//        if (Display.Keyboard.D) Display.Keyboard.KeyCheck[Keys.D] = true;
//        if (!Display.Keyboard.D && Display.Keyboard.KeyCheck[Keys.D])
//        {
//            string key = "D";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.D] = false;
//        }
//        /* PRESS E */
//        if (Display.Keyboard.E) Display.Keyboard.KeyCheck[Keys.E] = true;
//        if (!Display.Keyboard.E && Display.Keyboard.KeyCheck[Keys.E])
//        {
//            string key = "E";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.E] = false;
//        }
//        /* PRESS F */
//        if (Display.Keyboard.F) Display.Keyboard.KeyCheck[Keys.F] = true;
//        if (!Display.Keyboard.F && Display.Keyboard.KeyCheck[Keys.F])
//        {
//            string key = "F";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.F] = false;
//        }
//        /* PRESS G */
//        if (Display.Keyboard.G) Display.Keyboard.KeyCheck[Keys.G] = true;
//        if (!Display.Keyboard.G && Display.Keyboard.KeyCheck[Keys.G])
//        {
//            string key = "G";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.G] = false;
//        }
//        /* PRESS H */
//        if (Display.Keyboard.H) Display.Keyboard.KeyCheck[Keys.H] = true;
//        if (!Display.Keyboard.H && Display.Keyboard.KeyCheck[Keys.H])
//        {
//            string key = "H";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.H] = false;
//        }
//        /* PRESS I */
//        if (Display.Keyboard.I) Display.Keyboard.KeyCheck[Keys.I] = true;
//        if (!Display.Keyboard.I && Display.Keyboard.KeyCheck[Keys.I])
//        {
//            string key = "I";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.I] = false;
//        }
//        /* PRESS J */
//        if (Display.Keyboard.J) Display.Keyboard.KeyCheck[Keys.J] = true;
//        if (!Display.Keyboard.J && Display.Keyboard.KeyCheck[Keys.J])
//        {
//            string key = "J";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.J] = false;
//        }
//        /* PRESS K */
//        if (Display.Keyboard.K) Display.Keyboard.KeyCheck[Keys.K] = true;
//        if (!Display.Keyboard.K && Display.Keyboard.KeyCheck[Keys.K])
//        {
//            string key = "K";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.K] = false;
//        }
//        /* PRESS L */
//        if (Display.Keyboard.L) Display.Keyboard.KeyCheck[Keys.L] = true;
//        if (!Display.Keyboard.L && Display.Keyboard.KeyCheck[Keys.L])
//        {
//            string key = "L";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.L] = false;
//        }
//        /* PRESS M */
//        if (Display.Keyboard.M) Display.Keyboard.KeyCheck[Keys.M] = true;
//        if (!Display.Keyboard.M && Display.Keyboard.KeyCheck[Keys.M])
//        {
//            string key = "M";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.M] = false;
//        }
//        /* PRESS N */
//        if (Display.Keyboard.N) Display.Keyboard.KeyCheck[Keys.N] = true;
//        if (!Display.Keyboard.N && Display.Keyboard.KeyCheck[Keys.N])
//        {
//            string key = "N";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.N] = false;
//        }
//        /* PRESS O */
//        if (Display.Keyboard.O) Display.Keyboard.KeyCheck[Keys.O] = true;
//        if (!Display.Keyboard.O && Display.Keyboard.KeyCheck[Keys.O])
//        {
//            string key = "O";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.O] = false;
//        }
//        /* PRESS P */
//        if (Display.Keyboard.P) Display.Keyboard.KeyCheck[Keys.P] = true;
//        if (!Display.Keyboard.P && Display.Keyboard.KeyCheck[Keys.P])
//        {
//            string key = "P";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.P] = false;
//        }
//        /* PRESS Q */
//        if (Display.Keyboard.Q) Display.Keyboard.KeyCheck[Keys.Q] = true;
//        if (!Display.Keyboard.Q && Display.Keyboard.KeyCheck[Keys.Q])
//        {
//            string key = "Q";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Q] = false;
//        }
//        /* PRESS R */
//        if (Display.Keyboard.R) Display.Keyboard.KeyCheck[Keys.R] = true;
//        if (!Display.Keyboard.R && Display.Keyboard.KeyCheck[Keys.R])
//        {
//            string key = "R";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.R] = false;
//        }
//        /* PRESS S */
//        if (Display.Keyboard.S) Display.Keyboard.KeyCheck[Keys.S] = true;
//        if (!Display.Keyboard.S && Display.Keyboard.KeyCheck[Keys.S])
//        {
//            string key = "S";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.S] = false;
//        }
//        /* PRESS T */
//        if (Display.Keyboard.T) Display.Keyboard.KeyCheck[Keys.T] = true;
//        if (!Display.Keyboard.T && Display.Keyboard.KeyCheck[Keys.T])
//        {
//            string key = "T";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.T] = false;
//        }
//        /* PRESS U */
//        if (Display.Keyboard.U) Display.Keyboard.KeyCheck[Keys.U] = true;
//        if (!Display.Keyboard.U && Display.Keyboard.KeyCheck[Keys.U])
//        {
//            string key = "U";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.U] = false;
//        }
//        /* PRESS V */
//        if (Display.Keyboard.V) Display.Keyboard.KeyCheck[Keys.V] = true;
//        if (!Display.Keyboard.V && Display.Keyboard.KeyCheck[Keys.V])
//        {
//            string key = "V";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.V] = false;
//        }
//        /* PRESS W */
//        if (Display.Keyboard.W) Display.Keyboard.KeyCheck[Keys.W] = true;
//        if (!Display.Keyboard.W && Display.Keyboard.KeyCheck[Keys.W])
//        {
//            string key = "W";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.W] = false;
//        }
//        /* PRESS X */
//        if (Display.Keyboard.X) Display.Keyboard.KeyCheck[Keys.X] = true;
//        if (!Display.Keyboard.X && Display.Keyboard.KeyCheck[Keys.X])
//        {
//            string key = "X";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.X] = false;
//        }
//        /* PRESS Y*/
//        if (Display.Keyboard.Y) Display.Keyboard.KeyCheck[Keys.Y] = true;
//        if (!Display.Keyboard.Y && Display.Keyboard.KeyCheck[Keys.Y])
//        {
//            string key = "Y";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Y] = false;
//        }
//        /* PRESS Z */
//        if (Display.Keyboard.Z) Display.Keyboard.KeyCheck[Keys.Z] = true;
//        if (!Display.Keyboard.Z && Display.Keyboard.KeyCheck[Keys.Z])
//        {
//            string key = "Z";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Z] = false;
//        }

//        /* PRESS  OEMOPENBRACKETS */
//        if (Display.Keyboard.BracketOpen) Display.Keyboard.KeyCheck[Keys.OemOpenBrackets] = true;
//        if (!Display.Keyboard.BracketOpen && Display.Keyboard.KeyCheck[Keys.OemOpenBrackets])
//        {
//            string key = "OEMOPENBRACKETS";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemOpenBrackets] = false;
//        }
//        /* PRESS OEMCLOSEBRACKETS */
//        if (Display.Keyboard.BracketClose) Display.Keyboard.KeyCheck[Keys.OemCloseBrackets] = true;
//        if (!Display.Keyboard.BracketClose && Display.Keyboard.KeyCheck[Keys.OemCloseBrackets])
//        {
//            string key = "OEMCLOSEBRACKETS";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemCloseBrackets] = false;
//        }
//        /* PRESS SEMICOLON */
//        if (Display.Keyboard.Semicolon) Display.Keyboard.KeyCheck[Keys.OemSemicolon] = true;
//        if (!Display.Keyboard.Semicolon && Display.Keyboard.KeyCheck[Keys.OemSemicolon])
//        {
//            string key = "SEMICOLON";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemSemicolon] = false;
//        }
//        /* PRESS QUOTE */
//        if (Display.Keyboard.Quote) Display.Keyboard.KeyCheck[Keys.OemQuotes] = true;
//        if (!Display.Keyboard.Quote && Display.Keyboard.KeyCheck[Keys.OemQuotes])
//        {
//            string key = "QUOTE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemQuotes] = false;
//        }
//        /* PRESS COMMA */
//        if (Display.Keyboard.Comma) Display.Keyboard.KeyCheck[Keys.OemComma] = true;
//        if (!Display.Keyboard.Comma && Display.Keyboard.KeyCheck[Keys.OemComma])
//        {
//            string key = "COMMA";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemComma] = false;
//        }
//        /* PRESS PERIOD */
//        if (Display.Keyboard.Period) Display.Keyboard.KeyCheck[Keys.OemPeriod] = true;
//        if (!Display.Keyboard.Period && Display.Keyboard.KeyCheck[Keys.OemPeriod])
//        {
//            string key = "PERIOD";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemPeriod] = false;
//        }
//        /* PRESS QUESTION */
//        if (Display.Keyboard.Question) Display.Keyboard.KeyCheck[Keys.OemQuestion] = true;
//        if (!Display.Keyboard.Question && Display.Keyboard.KeyCheck[Keys.OemQuestion])
//        {
//            string key = "QUESTION";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemQuestion] = false;
//        }
//        /* PRESS TAB */
//        if (Display.Keyboard.Tab) Display.Keyboard.KeyCheck[Keys.Tab] = true;
//        if (!Display.Keyboard.Tab && Display.Keyboard.KeyCheck[Keys.Tab])
//        {
//            string key = "TAB";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Tab] = false;
//        }
//        /* PRESS CAPSLOCK */
//        if (Display.Keyboard.CapsLock) Display.Keyboard.KeyCheck[Keys.CapsLock] = true;
//        if (!Display.Keyboard.CapsLock && Display.Keyboard.KeyCheck[Keys.CapsLock])
//        {
//            string key = "CAPSLOCK";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.CapsLock] = false;
//        }
//        /* PRESS LEFTSHIFT */
//        if (Display.Keyboard.LeftShift) Display.Keyboard.KeyCheck[Keys.LeftShift] = true;
//        if (!Display.Keyboard.LeftShift && Display.Keyboard.KeyCheck[Keys.LeftShift])
//        {
//            string key = "LEFTSHIFT";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.LeftShift] = false;
//        }
//        /* PRESS LEFTCTRL */
//        if (Display.Keyboard.LeftCtrl) Display.Keyboard.KeyCheck[Keys.LeftControl] = true;
//        if (!Display.Keyboard.LeftCtrl && Display.Keyboard.KeyCheck[Keys.LeftControl])
//        {
//            string key = "LEFTCTRL";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.LeftControl] = false;
//        }
//        /* PRESS LEFTALT */
//        if (Display.Keyboard.LeftAlt) Display.Keyboard.KeyCheck[Keys.LeftAlt] = true;
//        if (!Display.Keyboard.LeftAlt && Display.Keyboard.KeyCheck[Keys.LeftAlt])
//        {
//            string key = "LEFTALT";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.LeftAlt] = false;
//        }
//        /* PRESS SPACE */
//        if (Display.Keyboard.Space) Display.Keyboard.KeyCheck[Keys.Space] = true;
//        if (!Display.Keyboard.Space && Display.Keyboard.KeyCheck[Keys.Space])
//        {
//            string key = "SPACE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Space] = false;
//        }
//        /* PRESS RIGHTALT */
//        if (Display.Keyboard.RightAlt) Display.Keyboard.KeyCheck[Keys.RightAlt] = true;
//        if (!Display.Keyboard.RightAlt && Display.Keyboard.KeyCheck[Keys.RightAlt])
//        {
//            string key = "RIGHTALT";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.RightAlt] = false;
//        }
//        /* PRESS RIGHTCTRL */
//        if (Display.Keyboard.RightCtrl) Display.Keyboard.KeyCheck[Keys.RightControl] = true;
//        if (!Display.Keyboard.RightCtrl && Display.Keyboard.KeyCheck[Keys.RightControl])
//        {
//            string key = "RIGHTCTRL";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.RightControl] = false;
//        }
//        /* PRESS RIGHTSHIFT */
//        if (Display.Keyboard.RightShift) Display.Keyboard.KeyCheck[Keys.RightShift] = true;
//        if (!Display.Keyboard.RightShift && Display.Keyboard.KeyCheck[Keys.RightShift])
//        {
//            string key = "RIGHTSHIFT";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.RightShift] = false;
//        }
//        /* PRESS ENTER */
//        if (Display.Keyboard.Enter) Display.Keyboard.KeyCheck[Keys.Enter] = true;
//        if (!Display.Keyboard.Enter & Display.Keyboard.KeyCheck[Keys.Enter])
//        {
//            string key = "ENTER";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Enter] = false;
//        }
//        /* PRESS OEMPIPE */
//        if (Display.Keyboard.Oempipe) Display.Keyboard.KeyCheck[Keys.OemPipe] = true;
//        if (!Display.Keyboard.Oempipe && Display.Keyboard.KeyCheck[Keys.OemPipe])
//        {
//            string key = "OEMPIPE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.OemPipe] = false;
//        }
//        /* PRESS  UP */
//        if (Display.Keyboard.Up) Display.Keyboard.KeyCheck[Keys.Up] = true;
//        if (!Display.Keyboard.Up && Display.Keyboard.KeyCheck[Keys.Up])
//        {
//            string key = "UP";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Up] = false;
//        }
//        /* PRESS  LEFT */
//        if (Display.Keyboard.Left) Display.Keyboard.KeyCheck[Keys.Left] = true;
//        if (!Display.Keyboard.Left && Display.Keyboard.KeyCheck[Keys.Left])
//        {
//            string key = "LEFT";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Left] = false;
//        }
//        /* PRESS DOWN */
//        if (Display.Keyboard.Down) Display.Keyboard.KeyCheck[Keys.Down] = true;
//        if (!Display.Keyboard.Down && Display.Keyboard.KeyCheck[Keys.Down])
//        {
//            string key = "DOWN";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Down] = false;
//        }
//        /* PRESS RIGHT */
//        if (Display.Keyboard.Right) Display.Keyboard.KeyCheck[Keys.Right] = true;
//        if (!Display.Keyboard.Right && Display.Keyboard.KeyCheck[Keys.Right])
//        {
//            string key = "RIGHT";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Right] = false;
//        }
//        /* PRESS NUMPADZERO */
//        if (Display.Keyboard.NumPadZero) Display.Keyboard.KeyCheck[Keys.NumPad0] = true;
//        if (!Display.Keyboard.NumPadZero && Display.Keyboard.KeyCheck[Keys.NumPad0])
//        {
//            string key = "NUMPADZERO";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumPad0] = false;
//        }
//        /* PRESS NUMPADONE */
//        if (Display.Keyboard.NumPadOne) Display.Keyboard.KeyCheck[Keys.NumPad1] = true;
//        if (!Display.Keyboard.NumPadOne && Display.Keyboard.KeyCheck[Keys.NumPad1])
//        {
//            string key = "NUMPADONE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumPad1] = false;
//        }
//        /* PRESS NUMPADTWO */
//        if (Display.Keyboard.NumPadTwo) Display.Keyboard.KeyCheck[Keys.NumPad2] = true;
//        if (!Display.Keyboard.NumPadTwo && Display.Keyboard.KeyCheck[Keys.NumPad2])
//        {
//            string key = "NUMPADTWO";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumPad2] = false;
//        }
//        /* PRESS NUMPADTHREE */
//        if (Display.Keyboard.NumPadThree) Display.Keyboard.KeyCheck[Keys.NumPad3] = true;
//        if (!Display.Keyboard.NumPadThree && Display.Keyboard.KeyCheck[Keys.NumPad3])
//        {
//            string key = "NUMPADTHREE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumPad3] = false;
//        }
//        /* PRESS NUMPADFOUR */
//        if (Display.Keyboard.NumPadFour) Display.Keyboard.KeyCheck[Keys.NumPad4] = true;
//        if (!Display.Keyboard.NumPadFour && Display.Keyboard.KeyCheck[Keys.NumPad4])
//        {
//            string key = "NUMPADFOUR";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumPad4] = false;
//        }
//        /* PRESS NUMPADFIVE */
//        if (Display.Keyboard.NumPadFive) Display.Keyboard.KeyCheck[Keys.NumPad5] = true;
//        if (!Display.Keyboard.NumPadFive && Display.Keyboard.KeyCheck[Keys.NumPad5])
//        {
//            string key = "NUMPADFIVE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumPad5] = false;
//        }
//        /* PRESS NUMPADSIX */
//        if (Display.Keyboard.NumPadSix) Display.Keyboard.KeyCheck[Keys.NumPad6] = true;
//        if (!Display.Keyboard.NumPadSix && Display.Keyboard.KeyCheck[Keys.NumPad6])
//        {
//            string key = "NUMPADSIX";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumPad6] = false;
//        }
//        /* PRESS NUMPADSEVEN */
//        if (Display.Keyboard.NumPadSeven) Display.Keyboard.KeyCheck[Keys.NumPad7] = true;
//        if (!Display.Keyboard.NumPadSeven && Display.Keyboard.KeyCheck[Keys.NumPad7])
//        {
//            string key = "NUMPADSEVEN";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumPad7] = false;
//        }
//        /* PRESS NUMPADEIGHT */
//        if (Display.Keyboard.NumPadEight) Display.Keyboard.KeyCheck[Keys.NumPad8] = true;
//        if (!Display.Keyboard.NumPadEight && Display.Keyboard.KeyCheck[Keys.NumPad8])
//        {
//            string key = "NUMPADEIGHT";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumPad8] = false;
//        }
//        /* PRESS  NUMPADNINE*/
//        if (Display.Keyboard.NumPadNine) Display.Keyboard.KeyCheck[Keys.NumPad9] = true;
//        if (!Display.Keyboard.NumPadNine && Display.Keyboard.KeyCheck[Keys.NumPad9])
//        {
//            string key = "NUMPADNINE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumPad9] = false;
//        }
//        /* PRESS NUMLOCK */
//        if (Display.Keyboard.NumLock) Display.Keyboard.KeyCheck[Keys.NumLock] = true;
//        if (!Display.Keyboard.NumLock && Display.Keyboard.KeyCheck[Keys.NumLock])
//        {
//            string key = "NUMLOCK";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.NumLock] = false;
//        }
//        /* PRESS DIVDE */
//        if (Display.Keyboard.Divide) Display.Keyboard.KeyCheck[Keys.Divide] = true;
//        if (!Display.Keyboard.Divide && Display.Keyboard.KeyCheck[Keys.Divide])
//        {
//            string key = "DIVIDE";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Divide] = false;
//        }
//        /* PRESS MULTIPLY */
//        if (Display.Keyboard.Multiply) Display.Keyboard.KeyCheck[Keys.Multiply] = true;
//        if (!Display.Keyboard.Multiply && Display.Keyboard.KeyCheck[Keys.Multiply])
//        {
//            string key = "MULTIPLY";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Multiply] = false;
//        }
//        /* PRESS SUBTRACT */
//        if (Display.Keyboard.Subtract) Display.Keyboard.KeyCheck[Keys.Subtract] = true;
//        if (!Display.Keyboard.Subtract && Display.Keyboard.KeyCheck[Keys.Subtract])
//        {
//            string key = "SUBTRACT";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Subtract] = false;
//        }
//        /* PRESS ADD */
//        if (Display.Keyboard.Add) Display.Keyboard.KeyCheck[Keys.Add] = true;
//        if (!Display.Keyboard.Add && Display.Keyboard.KeyCheck[Keys.Add])
//        {
//            string key = "ADD";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Add] = false;
//        }
//        /* PRESS DECIMAL */
//        if (Display.Keyboard.Decimal) Display.Keyboard.KeyCheck[Keys.Decimal] = true;
//        if (!Display.Keyboard.Decimal && Display.Keyboard.KeyCheck[Keys.Decimal])
//        {
//            string key = "DECIMAL";
//            DoHotKeyAction(key);
//            Display.Keyboard.KeyCheck[Keys.Decimal] = false;
//        }
//   // } 
//}

