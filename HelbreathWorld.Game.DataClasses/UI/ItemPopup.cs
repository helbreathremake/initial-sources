﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.UI
{
    public static class ItemPopup
    {
        public static void DrawMagicPopup(SpriteBatch spriteBatch, int magicIndex, Vector2 drawLocation, int pivotX, int pivotY)
        {
            int sprite = (int)SpriteId.DialogsV2 + 2;
            float transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);
            Player player = ((MainGame)Cache.DefaultState).Player;

            // check for additional description lines before deciding if it needs an extended popup
            string baseMagicName = Cache.MagicConfiguration[magicIndex].Name;

            AnimationFrame popupFrame = Cache.Interface[sprite].Frames[27];

            int offsetX = 0; int offsetY = 0;
            if (pivotX + drawLocation.X + 18 + popupFrame.Width > Cache.DefaultState.Display.ResolutionWidth) offsetX -= popupFrame.Width + 26;
            if (pivotY + drawLocation.Y + 17 + popupFrame.Height > Cache.DefaultState.Display.ResolutionHeight) offsetY -= popupFrame.Height;
            Vector2 popupLocation = new Vector2(pivotX + drawLocation.X + 18 + offsetX, pivotY + drawLocation.Y + 17 + offsetY);

            spriteBatch.Draw(Cache.Interface[sprite].Texture, popupLocation, popupFrame.GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, baseMagicName, FontType.GeneralSize10, new Vector2(popupLocation.X + 10, popupLocation.Y + 15), 162, Cache.Colors[GameColor.Orange]);

            //int probability = Math.Min(100, Utility.GetCastingProbability(player.Skills[(int)SkillType.Magic], Cache.MagicConfiguration[magicIndex].Level, player.Level, player.Intelligence, (((MainGame)Cache.DefaultState).Weather != null ? ((MainGame)Cache.DefaultState).Weather.Type : WeatherType.Clear), player.Inventory.CastProbabilityBonus, player.SP));
            //SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Mana: {0}    CP: {1}%", player.RequiredMana(magicIndex), probability), FontType.DialogsV2Smaller, new Vector2(popupLocation.X + 10, popupLocation.Y + 9 + 17), 162, Color.White);
        }
        /// <summary>
        /// Draws an Item popup to display item information
        /// </summary>
        /// <param name="spriteBatch">The current sprite batch to draw to</param>
        /// <param name="item">The item object to display information about</param>
        /// <param name="drawLocation">The coordinates to draw the popup</param>
        /// <param name="pivotX">The X coordinate of the dialog box this popup is attached to</param>
        /// <param name="pivotY">The Y coordinate of the dialog box this popup is attached to</param>
        public static void DrawItemPopup(SpriteBatch spriteBatch, Item item, Vector2 drawLocation, int pivotX, int pivotY) //TODO add magic icons?
        {
            int sprite = (int)SpriteId.DialogsV2 + 2;
            float transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);

            // check for additional description lines before deciding if it needs an extended popup
            string baseDescription = GetBaseDescription(item);
            string baseStats = GetBaseStats(item);
            string specialDescription = GetSpecialDescription(item);

            // determine whether a short box or a long box
            bool extendedPopup = (item.Stats.Count + 
                (item.SpecialAbilityType != ItemSpecialAbilityType.None ? 1 : 0) +
                (!string.IsNullOrEmpty(baseDescription) ? 1 : 0) +
                (!string.IsNullOrEmpty(specialDescription) ? 1 : 0) +
                ((item.Level > 0 || item.Count > 1) ? 1 : 0) +
                (item.DisplayGroup != ItemDisplayGroup.Ungrouped ? 1 : 0) > 3);
            AnimationFrame popupFrame = extendedPopup ? Cache.Interface[sprite].Frames[6] : Cache.Interface[sprite].Frames[10];

            /* DETERMINE POPUP DIRECTION (ENSURES IT DOESN'T GO OFF-SCREEN) */
            int offsetX = 0; int offsetY = 0;
            if (pivotX + drawLocation.X + 18 + popupFrame.Width > Cache.DefaultState.Display.ResolutionWidth) offsetX -= popupFrame.Width;
            if (pivotY + drawLocation.Y + 17 + popupFrame.Height > Cache.DefaultState.Display.ResolutionHeight) offsetY -= popupFrame.Height;
            Vector2 popupLocation = new Vector2(pivotX + drawLocation.X + 18 + offsetX, pivotY + drawLocation.Y + 17 + offsetY);

            spriteBatch.Draw(Cache.Interface[sprite].Texture, popupLocation, popupFrame.GetRectangle(), Color.White * transparency);
         
            GameColor itemTextColor = GameColor.None;
            switch (item.Quality)
            {
                case ItemQuality.None: itemTextColor = GameColor.Normal; break;//Color Orage //TODO expand on this for items that don't get quality level
                case ItemQuality.Flimsy: itemTextColor = GameColor.Flimsy; break;//Color White
                case ItemQuality.Sturdy: itemTextColor = GameColor.Sturdy; break;//Color Green
                case ItemQuality.Reinforced: itemTextColor = GameColor.Reinforced; break;//Color Blue
                case ItemQuality.Studded: itemTextColor = GameColor.Studded; break;//Color Purple
            }

            //Name - Show quality via text color
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, item.FriendlyName, FontType.GeneralSize10, new Vector2(popupLocation.X + 10, popupLocation.Y + 15), 162, Cache.Colors[itemTextColor]);

            int drawLine = 0;

            //Quality
            if (item.Quality != ItemQuality.None && !string.IsNullOrEmpty(item.Quality.ToString()))
            {
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Quality: " + item.Quality.ToString(), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[itemTextColor]);
            }
              
            // Base Stats
            if (!string.IsNullOrEmpty(baseStats))
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, baseStats, FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White);

            // level or count
            if (item.Count > 1)
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Quantity: {0}", item.Count), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White);
            else if (item.Level > 0)
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Item Level: {0}", item.Level), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White);

            // base description
            if (!string.IsNullOrEmpty(baseDescription))
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, baseDescription, FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White);

            // special description
            if (!string.IsNullOrEmpty(specialDescription))
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, specialDescription, FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White);

            foreach (KeyValuePair<ItemStat, int> stat in item.Stats)
                switch (stat.Key)
                {
                    case ItemStat.Critical: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Critical Damage", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Critical]); break;
                    case ItemStat.Poison: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Poison Damage", stat.Value * 5), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Poison]); break;
                    case ItemStat.Sharp: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+1 Dice Damage", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Sharp]); break;
                    case ItemStat.Ancient: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+2 Dice Damage", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Ancient]); break;
                    case ItemStat.Legendary: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+3 Dice Damage", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Legendary]); break;
                    case ItemStat.Righteous: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Reputation Damage", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Legendary]); break;
                    case ItemStat.CastingProbability: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Cast Probability", stat.Value * 3), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.ManaConverting: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("{0}% Damage to Mana", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.CriticalChance: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Critical Increase Chance", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.Endurance: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Endurance", stat.Value * 5), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.HittingProbability: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Hit Chance", stat.Value * 5), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Light]); break;
                    case ItemStat.ComboDamage: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Combo Damage", stat.Value * 5), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Light]); break;
                    case ItemStat.Gold: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Gold", stat.Value * 3), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Golden]); break;
                    case ItemStat.Experience: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Experience", stat.Value * 3), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Light]); break;
                    case ItemStat.PhysicalResistance: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Evade Chance", stat.Value * 5), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Light]); break;
                    case ItemStat.PhysicalAbsorption: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Physical Absorption", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Absorption]); break;
                    case ItemStat.MagicResistance: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Magic Evade Chance", stat.Value * 5), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Light]); break;
                    case ItemStat.MagicAbsorption: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Magical Absorption", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Absorption]); break;
                    case ItemStat.EarthAbsorption: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Earth Absorption", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Absorption]); break;
                    case ItemStat.AirAbsorption: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Air Absorption", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Absorption]); break;
                    case ItemStat.FireAbsorption: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Fire Absorption", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Absorption]); break;
                    case ItemStat.WaterAbsorption: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Water Absorption", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Absorption]); break;
                    case ItemStat.PoisonResistance: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Poison Evade Chance", stat.Value * 5), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Light]); break;
                    case ItemStat.SPRecovery: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% SP Recovery", stat.Value * 5), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Staminar]); break;
                    case ItemStat.HPRecovery: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% HP Recovery", stat.Value * 5), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Health]); break;
                    case ItemStat.MPRecovery: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% MP Recovery", stat.Value * 5), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Mana]); break;
                    case ItemStat.HPIncrease: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Max HP +{0}%", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Health]); break;
                    case ItemStat.MPIncrease: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Max MP +{0}%", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Mana]); break;
                    case ItemStat.SPIncrease: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Max SP +{0}%", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Staminar]); break;
                    case ItemStat.MaxCrits: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Max Crits +{0}", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Crits]); break;
                    case ItemStat.Vampiric: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Steals {0}% HP", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Vampire]); break;
                    case ItemStat.Zealous: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Steals {0}% MP", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Zealot]); break;
                    case ItemStat.Exhaustive: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Reduces SP by {0}", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Exhaust]); break;
                    case ItemStat.DamageBonus: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Extra Damage", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Light]); break;
                    case ItemStat.Piercing: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("{0}% Armour Piercing", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Piercing]); break;
                    case ItemStat.Agile: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Agile", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.ManaSave: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Mana Save", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.Luck: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Luck", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.Strength: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Strength", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.Dexterity: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Dexterity", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.Magic: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Magic", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.Intelligence: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Intelligence", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.Vitality: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Vitality", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.Agility: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0} Agility", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.CompleteMagicProtection: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Complete Magic Absorption"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.CompleteFireProtection: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Complete Fire Absorption"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.CompleteAirProtection: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Complete Air Absorption"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.CompleteEarthProtection: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Complete Earth Absorption"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.CompleteKinesisProtection: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Complete Kinesis Absorption"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.CompleteUnholyProtection: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Complete Unholy Absorption"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.CompleteWaterProtection: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Complete Water Absorption"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.CompleteSpiritProtection: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Complete Spirit Absorption"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.UnholyAbsorption: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Unholy Absorption", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.SpiritAbsorption: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Spirit Absorption", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.KinesisAbsorption: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+{0}% Kinesis Absorption", stat.Value), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                    case ItemStat.MagicAbsorptionDepleteEndurance: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Absorption Depletes Endurance"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;                                      
                }

            // activations
            switch (item.SpecialAbilityType)
            {
                //Active
                case ItemSpecialAbilityType.IceWeapon: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Ice Activation"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                case ItemSpecialAbilityType.MedusaWeapon: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Paralyze Activation"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                case ItemSpecialAbilityType.XelimaWeapon: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Half Damage Activation"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                case ItemSpecialAbilityType.MerienArmour: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Destory Weapon Activation"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                case ItemSpecialAbilityType.MerienShield: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Invincible Activation"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;            
                
                //Passive
                case ItemSpecialAbilityType.Blood: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("-20% Maximum HP"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Blood]); break;
                case ItemSpecialAbilityType.Berserk: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+30% Magic Damage"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Berserk]); break;
                case ItemSpecialAbilityType.Dark: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+2 Dice Damage At Night"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Shadow]); break;
                case ItemSpecialAbilityType.Light: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+2 Dice Damage At Day"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Righteous]); break;
                case ItemSpecialAbilityType.DemonSlayer: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("+6 Damage To Demons"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Blood]); break;
                case ItemSpecialAbilityType.Kloness: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Added Reputation Damage"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Cache.Colors[GameColor.Righteous]); break;
                case ItemSpecialAbilityType.BowLine: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Destory Weapon Activation"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                case ItemSpecialAbilityType.Storm: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Storm Activation"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
                case ItemSpecialAbilityType.Unknown: SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Unknown Activation"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White); break;
            }

            // Bound
            if (item.BoundID != Globals.UnboundItem)
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Bounded Item"), FontType.DialogsSmallSize8, new Vector2(popupLocation.X + 10, popupLocation.Y + 20 + (++drawLine * 15)), 162, Color.White);

            if (extendedPopup)
            {
                if (item.Endurance == 0) { SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("End: {0}/{1}", item.Endurance, item.MaximumEndurance), FontType.DialogsSmallerSize7, new Vector2(popupLocation.X + 10, popupLocation.Y + 165), 152, Color.Red); }
                else { SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("End: {0}/{1}", item.Endurance, item.MaximumEndurance), FontType.DialogsSmallerSize7, new Vector2(popupLocation.X + 10, popupLocation.Y + 165), 152, Color.White); }

                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(popupLocation.X + 10, popupLocation.Y + 162), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[5].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", item.GetGoldValue(true).ToString()), FontType.DialogsSmallerSize7, new Vector2(popupLocation.X + 25, popupLocation.Y + 165), Color.White);
            }
            else
            {
                if (item.Endurance == 0) { SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("End: {0}/{1}", item.Endurance, item.MaximumEndurance), FontType.DialogsSmallerSize7, new Vector2(popupLocation.X + 10, popupLocation.Y + 84), 152, Color.Red); }
                else { SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("End: {0}/{1}", item.Endurance, item.MaximumEndurance), FontType.DialogsSmallerSize7, new Vector2(popupLocation.X + 10, popupLocation.Y + 84), 152, Color.White); }

                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(popupLocation.X + 10, popupLocation.Y + 81), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[5].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", item.GetGoldValue(true).ToString()), FontType.DialogsSmallerSize7, new Vector2(popupLocation.X + 25, popupLocation.Y + 84), Color.White);
            }
        }

        private static string GetBaseStats(Item item)
        {
            string stats = string.Empty;

            // check for damage bonus stat
            int damageBonus = 0;
            foreach (KeyValuePair<ItemStat, int> stat in item.Stats)
                if (stat.Key == ItemStat.DamageBonus)
                    damageBonus = stat.Value;

            switch (item.Category)
            {
                case ItemCategory.Armour:
                case ItemCategory.Robes:
                case ItemCategory.Hats:
                case ItemCategory.Clothes:
                case ItemCategory.Shields:
                    stats = "PA: " + item.PhysicalAbsorption + "   DR: " + item.PhysicalResistance + (item.MagicAbsorption > 0 ? "   MA: " + item.MagicAbsorption : "");
                    break;
                case ItemCategory.Weapons:
                case ItemCategory.Staffs:
                case ItemCategory.Bows:
                    Dice damageSmall = item.DamageSmall;
                    if (damageSmall != null)
                        stats = string.Format("Dmg: {0}d{1}+{2}", damageSmall.Die, damageSmall.Faces, damageSmall.Bonus + damageBonus);
                    break;
                case ItemCategory.BattleStaffs:
                    Dice damageSmallBS = item.DamageSmall;
                    if (damageSmallBS != null)
                        stats = string.Format("Dmg: {0}d{1}+{2}", damageSmallBS.Die, damageSmallBS.Faces, damageSmallBS.Bonus + damageBonus);
                    break;         
            }

            return stats;
        }

        private static string GetBaseDescription(Item item)
        {
            string description = string.Empty;

            switch (item.EffectType)
            {
                case ItemEffectType.Food: description = string.Format("Replenish {0}~{1} Hunger", item.Effect1 + item.Effect3, (item.Effect1 * item.Effect2) + item.Effect3); break;
                case ItemEffectType.HP: description = string.Format("Replenish {0}~{1} HP", item.Effect1 + item.Effect3, (item.Effect1 * item.Effect2) + item.Effect3); break;
                case ItemEffectType.MP: description = string.Format("Replenish {0}~{1} MP", item.Effect1 + item.Effect3, (item.Effect1 * item.Effect2) + item.Effect3); break;
                case ItemEffectType.SP: description = string.Format("Replenish {0}~{1} SP", item.Effect1 + item.Effect3, (item.Effect1 * item.Effect2) + item.Effect3); break;
                case ItemEffectType.AddEffect:
                    switch (item.Effect1)
                    {
                        case 1: description = string.Format("+{0}% Magic Evade Chance", item.Effect2); break;
                        case 2: description = string.Format("+{0}% Mana Save", item.Effect2); break;
                        case 3: description = string.Format("+{0} Physical Damage", item.Effect2); break;
                        case 4: description = string.Format("+{0}% Evade Chance", item.Effect2); break;
                        case 5: description = string.Format("+{0}% Luck", item.Effect2); break;
                        case 6: description = string.Format("+{0} Magical Damage", item.Effect2); break;
                        case 7: description = string.Format("+{0}% Air Absorption", item.Effect2); break;
                        case 8: description = string.Format("+{0}% Earth Absorption", item.Effect2); break;
                        case 9: description = string.Format("+{0}% Fire Absorption", item.Effect2); break;
                        case 10: description = string.Format("+{0}% Water Absorption", item.Effect2); break;
                        case 11: description = string.Format("+{0}% Poison Evade Chance", item.Effect2); break;
                        case 12: description = string.Format("+{0}% Hit Chance", item.Effect2 + item.SpecialEffect2); break;
                        case 13: description = string.Format("+{0}% Health Recovery", item.Effect2); break;
                        case 14: description = string.Format("+{0}% Hit Chance", item.Effect2); break;
                        case 15: description = string.Format("+{0}% Magical Absorption", item.Effect2); break;
                        case 16: description = string.Format("+{0} Strength", item.Level); break;
                        case 17: description = string.Format("+{0} Dexterity", item.Level); break;
                        case 18: description = string.Format("+{0} Intelligence", item.Level); break;
                        case 19: description = string.Format("+{0} Magic", item.Level); break;
                        case 20: description = string.Format("+{0}% Physical Absorption", item.Effect2); break;
                    }
                    break;
                case ItemEffectType.Angel:
                    switch ((AngelType)item.Effect1)
                    {
                        case AngelType.Str: description = string.Format("+{0} Strength", item.Level); break;
                        case AngelType.Dex: description = string.Format("+{0} Dexterity", item.Level); break;
                        case AngelType.Mag: description = string.Format("+{0} Magic", item.Level); break;
                        case AngelType.Int: description = string.Format("+{0} Intelligence", item.Level); break;
                        case AngelType.Vit: description = string.Format("+{0} Vitality", item.Level); break;
                        case AngelType.Agi: description = string.Format("+{0} Agility", item.Level); break;
                    }
                    break;
            }

            return description;
        }

        private static string GetSpecialDescription(Item item)
        {
            string description = string.Empty;

            switch (item.Category)
            {
                case ItemCategory.Armour:
                case ItemCategory.Robes:
                case ItemCategory.Hats:
                case ItemCategory.Clothes:
                case ItemCategory.Shields:
                    if (item.Category == ItemCategory.Hats) description = string.Format("+5% Cast Probability"); 
                    if (item.Category == ItemCategory.Robes) description = string.Format("+20% Cast Probability"); 
                    break;
                case ItemCategory.Weapons:
                case ItemCategory.Staffs:
                case ItemCategory.Bows:
                    if (item.EffectType == ItemEffectType.AttackManaSave && item.Effect4 > 0)
                        description = string.Format("+{0}% Mana Save", item.Effect4);
                    break;
                case ItemCategory.BattleStaffs:
                    description = string.Format("{0} Damage", ((MagicAttribute)item.Effect4).ToString());
                    break;
                case ItemCategory.Upgrades:
                    if (item.EffectType == ItemEffectType.Xelima) description = string.Format("Weapon Upgrade Ingredient");
                    if (item.EffectType == ItemEffectType.Merien) description = string.Format("Armour Upgrade Ingredient");
                    break;
            }

            switch (item.Set) //TODO Expand this
            {
                //Hero
                case ItemSet.Hero: description = string.Format("Set: +{0} TODO", 5); break;
                case ItemSet.HeroWarrior: description = string.Format("Set: +{0} Physical Damage", 5); break;
                case ItemSet.HeroMage: description = string.Format("Set: +{0} Magical Damage", 3); break;
                case ItemSet.HeroBattleMage: description = string.Format("Set: +{0} Battle Staff Damage", 5); break;
                case ItemSet.HeroArcher: description = string.Format("Set: +{0} Arrow Damage", 5); break;

                //Godly
                case ItemSet.Godly: string.Format("Set: +{0} TODO", 5); break;
                case ItemSet.GodlyWarrior: string.Format("Set: +{0} Physical Damage", 5); break;
                case ItemSet.GodlyMage: string.Format("Set: +{0} Magical Damage", 3); break;
                case ItemSet.GodlyBattleMage: description = string.Format("Set: +{0} Battle Staff Damage", 5); break;
                case ItemSet.GodlyArcher: description = string.Format("Set: +{0} Arrow Damage", 5); break;            
            }

            return description;
        }
    }
}
