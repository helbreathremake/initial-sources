﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HelbreathWorld.Game.Assets
{
    public class GameFont
    {
        private Texture2D texture;

        private List<Rectangle> frames;
        private Dictionary<char, Rectangle> bold;
        private Dictionary<char, Rectangle> regular;
        private Dictionary<char, Rectangle> small;

        public GameFont(Texture2D texture)
        {
            this.texture = texture;

            bold = new Dictionary<char, Rectangle>();
            regular = new Dictionary<char, Rectangle>();
            small = new Dictionary<char, Rectangle>();
        }

        public Vector2 MeasureString(string message, GameFontType type)
        {
            switch (type)
            {
                case GameFontType.Bold: return MeasureBoldString(message);
                case GameFontType.Regular: return MeasureRegularString(message);
                case GameFontType.Small: return MeasureSmallString(message);
                default: return MeasureRegularString(message);
            }
        }

        public Vector2 MeasureBoldString(string message)
        {
            int height = 0; int width = 0;
            foreach (char c in message)
            {
                if (bold[c].Height > height) height = bold[c].Height; // get tallest sprite

                width += bold[c].Width;
                width += 5; // 5 pixels between characters
            }
            width -= 5; // ignores last letter

            return new Vector2(width, height);
        }
        public Vector2 MeasureRegularString(string message)
        {
            int height = 0; int width = 0;
            foreach (char c in message)
            {
                if (regular[c].Height > height) height = regular[c].Height; // get tallest sprite

                width += regular[c].Width;
                width += 5; // 5 pixels between characters
            }
            width -= 5; // ignores last letter

            return new Vector2(width, height);
        }
        public Vector2 MeasureSmallString(string message)
        {
            int height = 0; int width = 0;
            foreach (char c in message)
            {
                if (small[c].Height > height) height = small[c].Height; // get tallest sprite

                width += small[c].Width;
                width += 5; // 5 pixels between characters
            }
            width -= 5; // ignores last letter

            return new Vector2(width, height);
        }

        public void InitCharacters(List<SpriteFrame> frames)
        {
            int index = 0;
            bold.Add(' ', new Rectangle(frames[index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('!', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('"', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('#', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('$', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('%', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('&', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('\'', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('(', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add(')', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('*', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('+', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add(',', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('-', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('.', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('/', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('0', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('1', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('2', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('3', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('4', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('5', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('6', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('7', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('8', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('9', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add(':', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add(';', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('<', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('=', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('>', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('?', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('@', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('A', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('B', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('C', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('D', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('E', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('F', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('G', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('H', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('I', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('J', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('K', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('L', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('M', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('N', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('O', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('P', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('Q', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('R', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('S', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('T', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('U', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('V', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('W', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('X', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('Y', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('Z', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('[', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('\\', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add(']', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('^', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('_', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('`', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('a', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('b', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('c', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('d', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('e', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('f', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('g', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('h', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('i', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('j', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('k', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('l', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('m', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('n', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('o', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('p', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('q', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('r', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('s', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('t', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('u', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('v', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('w', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('x', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('y', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('z', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('{', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('|', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('}', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            bold.Add('~', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));

            regular.Add(' ', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('!', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('"', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('#', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('$', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('%', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('&', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('\'', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('(', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add(')', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('*', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('+', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add(',', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('-', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('.', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('/', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('0', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('1', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('2', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('3', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('4', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('5', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('6', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('7', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('8', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('9', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add(':', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add(';', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('<', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('=', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('>', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('?', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('@', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('A', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('B', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('C', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('D', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('E', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('F', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('G', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('H', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('I', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('J', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('K', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('L', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('M', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('N', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('O', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('P', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('Q', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('R', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('S', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('T', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('U', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('V', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('W', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('X', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('Y', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('Z', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('[', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('\\', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add(']', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('^', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('_', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('`', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('a', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('b', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('c', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('d', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('e', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('f', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('g', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('h', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('i', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('j', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('k', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('l', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('m', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('n', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('o', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('p', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('q', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('r', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('s', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('t', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('u', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('v', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('w', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('x', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('y', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('z', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('{', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('|', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('}', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            regular.Add('~', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));

            small.Add(' ', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('!', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('"', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('#', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('$', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('%', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('&', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('\'', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('(', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add(')', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('*', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('+', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add(',', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('-', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('.', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('/', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('0', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('1', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('2', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('3', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('4', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('5', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('6', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('7', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('8', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('9', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add(':', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add(';', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('<', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('=', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('>', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('?', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('@', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('A', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('B', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('C', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('D', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('E', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('F', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('G', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('H', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('I', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('J', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('K', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('L', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('M', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('N', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('O', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('P', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('Q', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('R', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('S', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('T', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('U', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('V', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('W', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('X', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('Y', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('Z', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('[', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('\\', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add(']', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('^', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('_', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('`', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('a', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('b', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('c', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('d', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('e', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('f', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('g', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('h', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('i', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('j', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('k', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('l', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('m', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('n', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('o', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('p', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('q', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('r', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('s', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('t', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('u', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('v', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('w', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('x', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('y', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('z', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('{', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('|', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('}', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));
            small.Add('~', new Rectangle(frames[++index].Left, frames[index].Top, frames[index].Width, frames[index].Height));            
        }

        public Texture2D Texture { get { return texture; } }
        public List<Rectangle> Frames { get { return frames; } set { frames = value; } }
        public Dictionary<char, Rectangle> Bold { get { return bold; } }
        public Dictionary<char, Rectangle> Regular { get { return regular; } }
        public Dictionary<char, Rectangle> Small { get { return small; } }
    }
}
