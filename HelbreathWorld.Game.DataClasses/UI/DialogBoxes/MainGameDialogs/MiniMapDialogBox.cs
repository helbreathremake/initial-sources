﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.UI
{
    public class MiniMapDialogBox : IGameDialogBox
    {        
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items

        AnimationFrame frame;
              
        GameDialogBoxConfiguration config;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.MiniMap; } }

        public MiniMapDialogBox()
        {    
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);   
        }

        public MiniMapDialogBox(GameDialogBoxConfiguration config)
        {    
            this.config = config;
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {       
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            Player player = ((MainGame)Cache.DefaultState).Player;
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;

            int x = config.X;
            int y = config.Y;
            


            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;
       
            //draw extra 
            int drawX, drawY, zoomMapPivotX, zoomMapPivotY, highlightPlayerId = -1;
            GameColor color = GameColor.None;
            switch (player.Side)
            {
                case OwnerSide.Aresden: color = GameColor.Aresden; break;
                case OwnerSide.Elvine: color = GameColor.Elvine; break;
                case OwnerSide.Neutral: color = GameColor.Neutral; break;
                case OwnerSide.Wild: color = GameColor.Wild; break;
                case OwnerSide.None:
                default: break;
            }

            switch (Cache.GameSettings.MiniMapZoomLevel)
            {
                case 2:
                    if (player.Map.ZoomMap == null) break;
            
                    frame = new AnimationFrame(0, 0, player.Map.Width, player.Map.Height, 0, 0); // create dummy frame so mouse clicks are aware of the bounds
            
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "[full map]", FontType.DialogsSmallSize8, new Vector2(x+3, (y > 213 ? (y - 17) : (y + frame.Height + 4))), Cache.Colors[GameColor.Orange]);
                    spriteBatch.Draw(player.Map.ZoomMap, new Vector2(x, y), new Rectangle(0, 0, player.Map.Width, player.Map.Height), Color.White * transparency);
            
                    drawX = player.X;
                    drawY = player.Y;

                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceMiniMapMarker].Texture, new Vector2(x + drawX, y + drawY), Cache.Interface[(int)SpriteId.InterfaceCrusade].Frames[0].GetRectangle(), Cache.Colors[color]);
            
                    // draw party member locations
                    if (player.HasParty)
                        for (int i = 0; i < player.Party.MemberCount; i++)
                            if (player.Party.MemberInfo.ContainsKey(player.Party.Members[i]) && player.Party.Members[i] != player.ObjectId && player.Party.MemberInfo[player.Party.Members[i]].MapName.Equals(player.MapName)) // must be on same map
                            {
                                // draw party member location
                                drawX = player.Party.MemberInfo[player.Party.Members[i]].X;
                                drawY = player.Party.MemberInfo[player.Party.Members[i]].Y;
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceMiniMapMarker].Texture, new Vector2(x + drawX, y + drawY), Cache.Interface[(int)SpriteId.InterfaceCrusade].Frames[0].GetRectangle(), Cache.Colors[GameColor.Party]);
                                if (Utility.IsSelected(mouseX, mouseY, x + drawX, y + drawY, x + drawX + 6, y + drawY + 6))
                                    highlightPlayerId = player.Party.Members[i];
                            }
            
                    if (((MainGame)Cache.DefaultState).PublicEventState != PublicEventState.None)
                    {
                        drawX = ((MainGame)Cache.DefaultState).PublicEventX - (Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[12].Width / 2);
                        drawY = ((MainGame)Cache.DefaultState).PublicEventY - (Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[12].Height / 2);
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + drawX, y + drawY), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[12].GetRectangle(), Color.White);
                    }
                    break;
                case 1:
                    if (player.Map.ZoomMap == null) break;
            
                    frame = new AnimationFrame(0, 0, 128, 128, 0, 0); // create dummy frame so mouse clicks are aware of the bounds
            
                    zoomMapPivotX = Math.Min(Math.Max(player.X - (frame.Width / 2), 0), player.Map.Width - 128);
                    zoomMapPivotY = Math.Min(Math.Max(player.Y - (frame.Height / 2), 0), player.Map.Height - 128);
            
                    // draw [zoom map] text
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "[zoom map]", FontType.DialogsSmallSize8, new Vector2(x+3, (y > 213 ? (y - 17) : (y + frame.Height + 4))), Cache.Colors[GameColor.Orange]);
                    spriteBatch.Draw(player.Map.ZoomMap, new Vector2(x, y), new Rectangle(zoomMapPivotX, zoomMapPivotY, frame.Width, frame.Height), Color.White * transparency);
            
                    drawX = player.X - zoomMapPivotX;
                    drawY = player.Y - zoomMapPivotY;

                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceMiniMapMarker].Texture, new Vector2(x + drawX, y + drawY), Cache.Interface[(int)SpriteId.InterfaceCrusade].Frames[0].GetRectangle(), Cache.Colors[color]);
            
                    // draw party member locations
                    if (player.HasParty)
                        for (int i = 0; i < player.Party.MemberCount; i++)
                            if (player.Party.MemberInfo.ContainsKey(player.Party.Members[i]) && player.Party.Members[i] != player.ObjectId && player.Party.MemberInfo[player.Party.Members[i]].MapName.Equals(player.MapName)) // must be on same map
                            {
                                // draw party member location
                                drawX = player.Party.MemberInfo[player.Party.Members[i]].X - zoomMapPivotX;
                                drawY = player.Party.MemberInfo[player.Party.Members[i]].Y - zoomMapPivotY;
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceMiniMapMarker].Texture, new Vector2(x + drawX, y + drawY), Cache.Interface[(int)SpriteId.InterfaceCrusade].Frames[0].GetRectangle(), Cache.Colors[GameColor.Party]);
                                if (Utility.IsSelected(mouseX, mouseY, x + drawX, y + drawY, x + drawX + 6, y + drawY + 6))
                                    highlightPlayerId = player.Party.Members[i];
                            }
            
                    if (((MainGame)Cache.DefaultState).PublicEventState != PublicEventState.None)
                    {
                        drawX = ((MainGame)Cache.DefaultState).PublicEventX - zoomMapPivotX - (Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[12].Width / 2);
                        drawY = ((MainGame)Cache.DefaultState).PublicEventY - zoomMapPivotY - (Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[12].Height / 2);
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + drawX, y + drawY), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[12].GetRectangle(), Color.White);
                    }
                    break;
                case 0:
                    if (player.Map.MiniMap == null) break;
            
                    frame = new AnimationFrame(0, 0, 128, 128, 0, 0); // create dummy frame so mouse clicks are aware of the bounds
            
                    // draw [mini map] text
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "[mini map]", FontType.DialogsSmallSize8, new Vector2(x +3, (y > 213 ? (y - 17) : (y + frame.Height + 4))), Cache.Colors[GameColor.Orange]);
                    spriteBatch.Draw(player.Map.MiniMap, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);
            
                    // draw player location (player location * (scale factor of map to minimap))
                    drawX = (int)((double)player.X * ((double)frame.Width / (double)player.Map.Width));
                    drawY = (int)((double)player.Y * ((double)frame.Height / (double)player.Map.Height));

                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceMiniMapMarker].Texture, new Vector2(x + drawX, y + drawY), Cache.Interface[(int)SpriteId.InterfaceCrusade].Frames[0].GetRectangle(), Cache.Colors[color]);
            
                    // draw party member locations
                    if (player.HasParty)
                        for (int i = 0; i < player.Party.MemberCount; i++)
                            if (player.Party.MemberInfo.ContainsKey(player.Party.Members[i]) && player.Party.Members[i] != player.ObjectId && player.Party.MemberInfo[player.Party.Members[i]].MapName.Equals(player.MapName)) // must be on same map
                            {
                                // draw party member location
                                drawX = (int)((double)player.Party.MemberInfo[player.Party.Members[i]].X * ((double)frame.Width / (double)player.Map.Width));
                                drawY = (int)((double)player.Party.MemberInfo[player.Party.Members[i]].Y * ((double)frame.Height / (double)player.Map.Height));
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceMiniMapMarker].Texture, new Vector2(x + drawX, y + drawY), Cache.Interface[(int)SpriteId.InterfaceCrusade].Frames[0].GetRectangle(), Cache.Colors[GameColor.Party]);
                                if (Utility.IsSelected(mouseX, mouseY, x + drawX, y + drawY, x + drawX + 6, y + drawY + 6))
                                    highlightPlayerId = player.Party.Members[i];
                            }
            
                    if (((MainGame)Cache.DefaultState).PublicEventState != PublicEventState.None)
                    {
                        drawX = (int)(((double)((MainGame)Cache.DefaultState).PublicEventX - (Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[12].Width / 2)) * ((double)frame.Width / (double)player.Map.Width));
                        drawY = (int)(((double)((MainGame)Cache.DefaultState).PublicEventY - (Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[12].Height / 2)) * ((double)frame.Height / (double)player.Map.Height));
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + drawX, y + drawY), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[12].GetRectangle(), Color.White);
                    }
                    break;
            }
            
            if (highlightPlayerId != -1 && player.Party.MemberInfo.ContainsKey(highlightPlayerId))
            {
                SpriteHelper.DrawTextWithShadow(spriteBatch, player.Party.MemberInfo[highlightPlayerId].Name + " (" + player.Party.MemberInfo[highlightPlayerId].X + "," + player.Party.MemberInfo[highlightPlayerId].Y + ")", FontType.DialogsSmallerSize7, new Vector2(mouseX + 5, mouseY + 25), Cache.Colors[GameColor.Party]);
            }
            else if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
            {
                switch (Type)
                {
                    case GameDialogBoxType.MiniMap:
                        int mapX, mapY;
                        switch (Cache.GameSettings.MiniMapZoomLevel)
                        {
                            case 2:
                                mapX = ((int)Cache.DefaultState.Display.Mouse.X - x);
                                mapY = ((int)Cache.DefaultState.Display.Mouse.Y - y);
                                break;
                            case 1:
                                mapX = (player.X - (frame.Width / 2)) + ((int)Cache.DefaultState.Display.Mouse.X - x);
                                mapY = (player.Y - (frame.Height / 2)) + ((int)Cache.DefaultState.Display.Mouse.Y - y);
                                break;
                            case 0:
                            default:
                                mapX = (int)(((double)Cache.DefaultState.Display.Mouse.X - x) / ((double)frame.Width / (double)player.Map.Width));
                                mapY = (int)(((double)Cache.DefaultState.Display.Mouse.Y - y) / ((double)frame.Height / (double)player.Map.Height));
                                break;
                        }
                        SpriteHelper.DrawTextWithShadow(spriteBatch, mapX + "," + mapY, FontType.DialogsSmallerSize7, new Vector2(mouseX + 5, mouseY + 25), Cache.Colors[GameColor.Orange]);
                        break;
                }
                return true;
            }

            if (config.X < 0) config.X = 0;
            if (config.Y < 0) config.Y = 0;
            if (config.X > Cache.DefaultState.Display.ResolutionWidth - frame.Width) config.X = Cache.DefaultState.Display.ResolutionWidth - frame.Width;
            if (config.Y > Cache.DefaultState.Display.ResolutionHeight - frame.Height) config.Y = Cache.DefaultState.Display.ResolutionHeight - frame.Height;

            //figure out what this is used for
            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
                return true;
            else return false;                                                         
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {      
            config.X += x;
            config.Y += y;
            if (config.X < 0) config.X = 0;
            if (config.Y < 0) config.Y = 0;
            if (config.X > Cache.DefaultState.Display.ResolutionWidth - frame.Width) config.X = Cache.DefaultState.Display.ResolutionWidth - frame.Width;
            if (config.Y > Cache.DefaultState.Display.ResolutionHeight - frame.Height) config.Y = Cache.DefaultState.Display.ResolutionHeight - frame.Height;
        }

        public void Show()
        {
                       
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {                   
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
                      
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}

           