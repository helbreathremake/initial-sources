﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.UI
{
    public class CharacterV2DialogBox : IGameDialogBox
    {
        // IDIALOG
        private int clickedItemIndex; // clicked item
        private int selectedItemIndex; // mouse over item
        private Vector2 selectedItemLocation;
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedItemIndex; // chosen item (non volatile)
        private Vector2 highlightedItemLocation; // chose item location (non volatile)
        private GameDialogBoxConfiguration config;

        Player player;

        private int clickedX = 0; 
        private int clickedY = 0; 
        private float itemTransparency;

        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }

        public GameDialogBoxType Type { get { return GameDialogBoxType.CharacterV2; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }


        int mouseX;
        int mouseY;
        int X;
        int Y;
        int sprite = (int)SpriteId.DialogsV2 + 2;
        AnimationFrame frame;
        float transparency;
        int slotId;

        // INVENTORY DIALOG
        private Dictionary<EquipType, Vector2> equipmentLocations;

        public CharacterV2DialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            this.highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            SetPagination();

            equipmentLocations = new Dictionary<EquipType, Vector2>();
            equipmentLocations.Add(EquipType.Head, new Vector2(188, 35));
            equipmentLocations.Add(EquipType.Body, new Vector2(188, 70));
            equipmentLocations.Add(EquipType.FullBody, new Vector2(188, 70));
            equipmentLocations.Add(EquipType.Arms, new Vector2(188, 105));
            equipmentLocations.Add(EquipType.Legs, new Vector2(188, 140));
            equipmentLocations.Add(EquipType.Back, new Vector2(188, 175));
            equipmentLocations.Add(EquipType.Feet, new Vector2(188, 210));
            equipmentLocations.Add(EquipType.Neck, new Vector2(18, 35));
            equipmentLocations.Add(EquipType.LeftFinger, new Vector2(18, 70));
            equipmentLocations.Add(EquipType.RightFinger, new Vector2(18, 105));
            equipmentLocations.Add(EquipType.LeftHand, new Vector2(18, 140));
            equipmentLocations.Add(EquipType.DualHand, new Vector2(18, 175));
            equipmentLocations.Add(EquipType.RightHand, new Vector2(18, 175));
            equipmentLocations.Add(EquipType.Utility, new Vector2(18, 210));

            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);
        }


        public CharacterV2DialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            this.highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            SetPagination();

            equipmentLocations = new Dictionary<EquipType, Vector2>();
            equipmentLocations.Add(EquipType.Head, new Vector2(188, 35));
            equipmentLocations.Add(EquipType.Body, new Vector2(188, 70));
            equipmentLocations.Add(EquipType.FullBody, new Vector2(188, 70));
            equipmentLocations.Add(EquipType.Arms, new Vector2(188, 105));
            equipmentLocations.Add(EquipType.Legs, new Vector2(188, 140));
            equipmentLocations.Add(EquipType.Back, new Vector2(188, 175));
            equipmentLocations.Add(EquipType.Feet, new Vector2(188, 210));
            equipmentLocations.Add(EquipType.Neck, new Vector2(18, 35));
            equipmentLocations.Add(EquipType.LeftFinger, new Vector2(18, 70));
            equipmentLocations.Add(EquipType.RightFinger, new Vector2(18, 105));
            equipmentLocations.Add(EquipType.LeftHand, new Vector2(18, 140));
            equipmentLocations.Add(EquipType.DualHand, new Vector2(18, 175));
            equipmentLocations.Add(EquipType.RightHand, new Vector2(18, 175));
            equipmentLocations.Add(EquipType.Utility, new Vector2(18, 210));

            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            frame = null;

            switch (config.State)
            {
                case DialogBoxState.Normal:
                    highlightedItemIndex = -1;
                    frame = Cache.Interface[sprite].Frames[0];
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);

                    /* PLAYER VISUAL ON DIALOG */
                    //player.DrawItemPopup(spriteBatch, Cache.GameState.Display, x + 125, y + 175, -player.OffsetX, -player.OffsetY);

                    /* EQUIPMENT SLOT HIGHLIGHTS */
                    if ((X + 188 < mouseX) && (X + 188 + 37 > mouseX) && (Y + 35 < mouseY) && (Y + 35 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 31), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 0; highlightedItemLocation = new Vector2(191, 40);
                    }
                    else if ((X + 188 < mouseX) && (X + 188 + 37 > mouseX) && (Y + 70 < mouseY) && (Y + 70 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 66), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 1; highlightedItemLocation = new Vector2(191, 75);
                    }
                    else if ((X + 188 < mouseX) && (X + 188 + 37 > mouseX) && (Y + 105 < mouseY) && (Y + 105 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 101), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 2; highlightedItemLocation = new Vector2(191, 110);
                    }
                    else if ((X + 188 < mouseX) && (X + 188 + 37 > mouseX) && (Y + 140 < mouseY) && (Y + 140 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 136), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 3; highlightedItemLocation = new Vector2(191, 145);
                    }
                    else if ((X + 188 < mouseX) && (X + 188 + 37 > mouseX) && (Y + 175 < mouseY) && (Y + 175 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 171), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 4; highlightedItemLocation = new Vector2(191, 180);
                    }
                    else if ((X + 188 < mouseX) && (X + 188 + 37 > mouseX) && (Y + 210 < mouseY) && (Y + 210 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 206), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 5; highlightedItemLocation = new Vector2(191, 215);
                    }
                    else if ((X + 18 < mouseX) && (X + 18 + 37 > mouseX) && (Y + 35 < mouseY) && (Y + 35 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 31), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 6; highlightedItemLocation = new Vector2(21, 40);
                    }
                    else if ((X + 18 < mouseX) && (X + 18 + 37 > mouseX) && (Y + 70 < mouseY) && (Y + 70 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 66), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 7; highlightedItemLocation = new Vector2(21, 75);
                    }
                    else if ((X + 18 < mouseX) && (X + 18 + 37 > mouseX) && (Y + 105 < mouseY) && (Y + 105 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 101), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 8; highlightedItemLocation = new Vector2(21, 110);
                    }
                    else if ((X + 18 < mouseX) && (X + 18 + 37 > mouseX) && (Y + 140 < mouseY) && (Y + 140 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 136), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 9; highlightedItemLocation = new Vector2(21, 145);
                    }
                    else if ((X + 18 < mouseX) && (X + 18 + 37 > mouseX) && (Y + 175 < mouseY) && (Y + 175 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 171), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 10; highlightedItemLocation = new Vector2(21, 180);
                    }
                    else if ((X + 18 < mouseX) && (X + 18 + 37 > mouseX) && (Y + 210 < mouseY) && (Y + 210 + 34 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 206), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        highlightedItemIndex = 11; highlightedItemLocation = new Vector2(21, 215);
                    }

                    /* EQUIPMENT SELECTION AND SET HIGHLIGHTS */
                    //if (selectedSlotIds.Contains(0)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 31), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[0] != null && player.Inventory[0].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 31), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(1)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 66), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[1] != null && player.Inventory[1].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 66), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(2)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 101), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[2] != null && player.Inventory[2].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 101), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(3)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 136), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[3] != null && player.Inventory[3].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 136), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(4)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 171), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[4] != null && player.Inventory[4].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 171), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(5)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 206), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[5] != null && player.Inventory[5].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 185, Y + 206), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(6)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 31), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[6] != null && player.Inventory[6].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 31), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(7)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 66), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[7] != null && player.Inventory[7].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 66), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(8)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 101), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[8] != null && player.Inventory[8].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 101), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(9)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 136), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[9] != null && player.Inventory[9].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 136), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(10)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 171), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[10] != null && player.Inventory[10].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 171), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (selectedSlotIds.Contains(11)) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 206), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                    //if (player.Inventory[11] != null && player.Inventory[11].SetNumber > 0) spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 15, Y + 206), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);

                    /* PAGINATION */
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 215, Y + 459), Cache.Interface[sprite].Frames[9].GetRectangle(), Color.White * transparency);
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Page: " + config.Page + "/" + config.MaxPages, FontType.DialogsSmallerSize7, new Vector2(X + 189, Y + 458), 25, Cache.Colors[GameColor.Orange]);

                    /* PAGINATION SCROLL HIGHLIGHT*/
                    if ((Y + 459 < mouseY) && (Y + 459 + 7 > mouseY))
                    {
                        if ((X + 215 < mouseX) && (X + 215 + 11 > mouseX)) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 212, Y + 456), Cache.Interface[sprite].Frames[25].GetRectangle(), Color.White * transparency); }
                        if ((X + 215 + 11 < mouseX) && (X + 215 + 11 + 11 > mouseX)) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 212 + 11, Y + 456), Cache.Interface[sprite].Frames[25].GetRectangle(), Color.White * transparency); }
                    }

                    /* MAXIMIZE BUTTON */
                    if ((X + 222 < mouseX) && (X + 233 > mouseX) && (Y + 14 < mouseY) && (Y + 22 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 222 - 3, Y + 14 - 3), Cache.Interface[sprite].Frames[26].GetRectangle(), Color.White * transparency);
                    }

                    /* ITEM COUNTER */
                    //spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 215, y + 459), Cache.Interface[sprite].Frames[9].GetRectangle(), Color.White * transparency);
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Items: " + player.InventoryCount + "/" + Globals.MaximumInventoryItems, FontType.DialogsSmallerSize7, new Vector2(X + 119, Y + 458), 25, Cache.Colors[GameColor.Orange]); //x was 189

                    /* CHARACTER BRIEF */
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Lvl.{0}", player.Level), FontType.DialogsSmallSize8, new Vector2(X + 59, Y + 55), 134, Color.White);
                    if (player.GuildRank >= 0)
                    {
                        string guild = player.GuildName; // +((player.GuildRank == 0) ? " Guildmaster" : " Guildsman");
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, guild, FontType.DialogsSmallSize8, new Vector2(X + 59, Y + 70), 134, Color.White);
                    }

                    /* REBIRTH LEVEL */
                    if (player.PlayType == PlayType.PvE && player.RebirthLevel > 0)
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 102, Y + 90), Cache.Interface[sprite].Frames[13].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, player.RebirthLevel.ToString(), FontType.DisplayNameSize13Spacing1, new Vector2(X + 103, Y + 92), 45, Color.White);
                    }

                    if (player.PlayType == PlayType.PvE)
                    {
                        /* GOLD */
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 6, Y + 454), Cache.Interface[sprite].Frames[5].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", player.Gold), FontType.DialogsSmallerSize7, new Vector2(X + 21, Y + 458), Cache.Colors[GameColor.Orange]);
                    }
                    else if (player.PlayType == PlayType.PvP)
                    {
                        /* GLADIATOR POINTS */
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 6, Y + 454), Cache.Interface[sprite].Frames[12].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", player.GladiatorPoints), FontType.DialogsSmallerSize7, new Vector2(X + 21, Y + 458), Cache.Colors[GameColor.Orange]);
                    }

                    /* SET 1-5 */
                    // SET 1
                    if ((X + 17 < mouseX) && (X + 52 > mouseX) && (Y + 254 < mouseY) && (Y + 268 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 1", FontType.DialogsSmallerSize7, new Vector2(X + 20, Y + 259), 34, Cache.Colors[GameColor.Friendly]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 1", FontType.DialogsSmallerSize7, new Vector2(X + 20, Y + 259), 34, Color.White);

                    // SET 2
                    if ((X + 60 < mouseX) && (X + 95 > mouseX) && (Y + 254 < mouseY) && (Y + 268 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 2", FontType.DialogsSmallerSize7, new Vector2(X + 63, Y + 259), 34, Cache.Colors[GameColor.Friendly]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 2", FontType.DialogsSmallerSize7, new Vector2(X + 63, Y + 259), 34, Color.White);

                    // SET 3
                    if ((X + 103 < mouseX) && (X + 138 > mouseX) && (Y + 254 < mouseY) && (Y + 268 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 3", FontType.DialogsSmallerSize7, new Vector2(X + 106, Y + 259), 34, Cache.Colors[GameColor.Friendly]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 3", FontType.DialogsSmallerSize7, new Vector2(X + 106, Y + 259), 34, Color.White);

                    // SET 4
                    if ((X + 146 < mouseX) && (X + 181 > mouseX) && (Y + 254 < mouseY) && (Y + 268 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 4", FontType.DialogsSmallerSize7, new Vector2(X + 149, Y + 259), 34, Cache.Colors[GameColor.Friendly]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 4", FontType.DialogsSmallerSize7, new Vector2(X + 149, Y + 259), 34, Color.White);

                    // SET 5
                    if ((X + 189 < mouseX) && (X + 224 > mouseX) && (Y + 254 < mouseY) && (Y + 268 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 5", FontType.DialogsSmallerSize7, new Vector2(X + 192, Y + 259), 34, Cache.Colors[GameColor.Friendly]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 5", FontType.DialogsSmallerSize7, new Vector2(X + 192, Y + 259), 34, Color.White);

                    //CREATE SET
                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 59, Y + 225), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                    if ((X + 59 < mouseX) && (X + 116 > mouseX) && (Y + 225 < mouseY) && (Y + 247 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Create Set", FontType.DialogsSmallerSize7, new Vector2(X + 72, Y + 232), 34, Cache.Colors[GameColor.Orange]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Create Set", FontType.DialogsSmallerSize7, new Vector2(X + 72, Y + 232), 34, Color.White);

                    //PARTY
                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 59, Y + 185), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                    if ((X + 59 < mouseX) && (X + 116 > mouseX) && (Y + 185 < mouseY) && (Y + 207 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Party", FontType.DialogsSmallerSize7, new Vector2(X + 72, Y + 192), 34, Cache.Colors[GameColor.Orange]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Party", FontType.DialogsSmallerSize7, new Vector2(X + 72, Y + 192), 34, Color.White);

                    //CLEAR SET
                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 123, Y + 225), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                    if ((X + 123 < mouseX) && (X + 182 > mouseX) && (Y + 225 < mouseY) && (Y + 247 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Clear Set", FontType.DialogsSmallerSize7, new Vector2(X + 136, Y + 232), 34, Cache.Colors[GameColor.Orange]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Clear Set", FontType.DialogsSmallerSize7, new Vector2(X + 136, Y + 232), 34, Color.White);

                    /* GRID HIGHLIGHTS */
                    slotId = Globals.MaximumEquipment + ((config.Page-1)*30); // start at 12 or 42
                    for (int slotY = 0; slotY < 5; slotY++)
                        for (int slotX = 0; slotX < 6; slotX++)
                        {
                            if ((X + 6 + (slotX * 38) < mouseX) && (X + 6 + (slotX * 38) + 37 > mouseX) && (Y + 281 + (slotY * 35) < mouseY) && (Y + 281 + (slotY * 35) + 34 > mouseY))
                            {
                                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 6 + (slotX * 38), Y + 281 + (slotY * 35)), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                                highlightedItemIndex = slotId;
                                highlightedItemLocation = new Vector2(9 + (slotX * 38), 286 + (slotY * 35));
                            }

                            if (player.Inventory[slotId] != null && (player.Inventory[slotId].IsUpgradeItem || player.Inventory[slotId].IsUpgradeIngredient))
                            {
                                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 6 + (slotX * 38), Y + 281 + (slotY * 35)), Cache.Interface[sprite].Frames[11].GetRectangle(), Color.White * transparency);
                            }
                            //if (SelectedSlotIds.Contains(slotId))
                            //   spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 6 + (slotX * 38), Y + 281 + (slotY * 35)), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                            //if (player.Inventory[slotId] != null && player.Inventory[slotId].SetNumber > 0)
                            //{
                            //    spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(X + 6 + (slotX * 38), Y + 281 + (slotY * 35)), Cache.Interface[sprite].Frames[2].GetRectangle(), Color.White * transparency);
                            //}
                            slotId++;
                        }
                    if (highlightedItemIndex != -1 && player.Inventory[highlightedItemIndex] != null) { selectedItemIndex = highlightedItemIndex; selectedItemLocation = highlightedItemLocation; }
                    else selectedItemIndex = -1;

                    if (player.Inventory.Count > 0)
                    {                     
                        int slotX = 0; int slotY = 0;
                        for (int itemIndex = Globals.MaximumEquipment + ((config.Page - 1) * 30); itemIndex < Globals.MaximumEquipment + ((config.Page - 1) * 30) + 30; itemIndex++)
                        {
                            if (player.Inventory[itemIndex] != null)
                            {
                                Item item = player.Inventory[itemIndex];

                                // checks merchant dialog is open and the item is in the sell list
                                bool forSale = (((IGameState)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant].Config.Visible && ((MerchantDialogBox)((IGameState)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant]).SellList.Contains(itemIndex));
                                bool forTrade = (((IGameState)Cache.DefaultState).DialogBoxes[GameDialogBoxType.TradeV2].Config.Visible && ((TradeV2DialogBox)((IGameState)Cache.DefaultState).DialogBoxes[GameDialogBoxType.TradeV2]).TradeList.Contains(itemIndex));

                                // get sprite frame
                                Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Frames[item.SpriteFrame].GetRectangle();
                                // get draw location offset from center of the slot
                                Rectangle draw = new Rectangle(X + 9 + (slotX * 38) + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                               Y + 286 + (slotY * 35) + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                               f.Width,
                                                               f.Height
                                                               );

                                itemTransparency = 1.0f;
                                if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex == itemIndex)
                                {
                                    clickedX = slotX;
                                    clickedY = slotY;
                                    itemTransparency = 0.6f;
                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 9 + (slotX * 38) + 5, Y + 286 + (slotY * 35) + 5, 27, 24), f, Color.Black * 0.5f);
                                }

                                if (item.IsEquipped) { spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 9 + (slotX * 38) + 5, Y + 286 + (slotY * 35) + 5, 27, 24), f, Cache.Colors[GameColor.Friendly] * 0.1f); }
                                else if (item.IsBroken) { spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 9 + (slotX * 38) + 5, Y + 286 + (slotY * 35) + 5, 27, 24), f, Cache.Colors[GameColor.Enemy] * 0.1f); }

                                if (f.Width > 31 || f.Height > 31)
                                    spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, ((item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]) * itemTransparency);
                                else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Vector2(draw.X, draw.Y), f, ((item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]) * itemTransparency);

                                if (forSale) SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "SELL", FontType.DialogsSmallerSize7, new Vector2(X + 9 + (slotX * 38), Y + 300 + (slotY * 35)), 40, Cache.Colors[GameColor.Enemy]);
                                if (forTrade) SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "TRADE", FontType.DialogsSmallerSize7, new Vector2(X + 9 + (slotX * 38), Y + 300 + (slotY * 35)), 40, Cache.Colors[GameColor.Enemy]);

                                if (item.Count > 1)
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("x{0}", item.Count.ToString()), FontType.DialogsSmallerSize7, new Vector2(X + 9 + (slotX * 38), Y + 281 + (slotY * 35) + 24), 32, Cache.Colors[GameColor.Orange]);
                                if (item.SetNumber > 0)
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", item.SetNumber.ToString()), FontType.DialogsSmallerSize7, new Vector2(X + 9 + (slotX * 38) + 6, Y + (slotY * 35) + 281 + 8), Cache.Colors[GameColor.Orange]);
                            }
                            slotX++;
                            if (slotX > 5) { slotX = 0; slotY++; }
                        }
                    }

                    /* EQUIPMENT ITEMS */
                    for (int itemIndex = 0; itemIndex < Globals.MaximumEquipment; itemIndex++)
                    {
                        if (player.Inventory[itemIndex] != null && player.Inventory[itemIndex].IsEquipped)
                        {
                            Item item = player.Inventory[itemIndex];

                            bool forSale = (((IGameState)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant].Config.Visible && ((MerchantDialogBox)((IGameState)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant]).SellList.Contains(itemIndex));
                            bool forTrade = (((IGameState)Cache.DefaultState)).DialogBoxes[GameDialogBoxType.TradeV2].Config.Visible && (((TradeV2DialogBox)((IGameState)Cache.DefaultState).DialogBoxes[GameDialogBoxType.TradeV2]).TradeList.Contains(itemIndex));

                            Vector2 location = Vector2.Add(equipmentLocations[item.EquipType], new Vector2(X, Y));

                            // get sprite frame
                            Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Frames[item.SpriteFrame].GetRectangle();
                            // get draw location offset from center of the slot
                            Rectangle draw = new Rectangle((int)location.X + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                           (int)location.Y + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                           f.Width, f.Height);

                            itemTransparency = 1.0f;
                            if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex == itemIndex)
                            {
                                clickedX = (int)location.X; 
                                clickedY = (int)location.Y; 
                                itemTransparency = 0.6f; 
                                spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle((int)location.X + 5, (int)location.Y + 5, 27, 24), f, Color.Black * 0.5f);
                            }

                            if (f.Width > 31 || f.Height > 31)
                                spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, ((item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]) * itemTransparency);
                            else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Vector2(draw.X, draw.Y), f, ((item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]) * itemTransparency);

                            if (forSale) SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "SELL", FontType.DialogsSmallerSize7, new Vector2(location.X, location.Y + 14), 40, Cache.Colors[GameColor.Enemy]);
                            if (forTrade) SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "TRADE", FontType.DialogsSmallerSize7, new Vector2(location.X, location.Y + 14), 40, Cache.Colors[GameColor.Enemy]);

                            if (item.Count > 1)
                                SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("x{0}", item.Count.ToString()), FontType.DialogsSmallerSize7, new Vector2(location.X, location.Y - 5 + 24), 32, Cache.Colors[GameColor.Orange]);
                            if (item.SetNumber > 0)
                                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", item.SetNumber.ToString()), FontType.DialogsSmallerSize7, new Vector2(location.X + 6, location.Y + 3), Cache.Colors[GameColor.Orange]);
                        }
                    }

                    /* TITLE */
                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 48, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, player.Name, FontType.DisplayNameSize13Spacing1, new Vector2(X + 58, Y - 9), 140, Cache.Colors[GameColor.Orange]);
                    break;
                case DialogBoxState.Compact:
                    highlightedItemIndex = -1;
                    frame = Cache.Interface[sprite].Frames[1];
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);

                    /* SET 1-5 */
                    if ((X + 35 < mouseX) && (X + 70 > mouseX) && (Y + 10 < mouseY) && (Y + 24 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 1", FontType.DialogsSmallerSize7, new Vector2(X + 38, Y + 15), 34, Cache.Colors[GameColor.Friendly]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 1", FontType.DialogsSmallerSize7, new Vector2(X + 38, Y + 15), 34, Color.White);

                    // SET 2
                    if ((X + 70 < mouseX) && (X + 105 > mouseX) && (Y + 10 < mouseY) && (Y + 24 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 2", FontType.DialogsSmallerSize7, new Vector2(X + 73, Y + 15), 34, Cache.Colors[GameColor.Friendly]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 2", FontType.DialogsSmallerSize7, new Vector2(X + 73, Y + 15), 34, Color.White);

                    // SET 3
                    if ((X + 105 < mouseX) && (X + 140 > mouseX) && (Y + 10 < mouseY) && (Y + 24 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 3", FontType.DialogsSmallerSize7, new Vector2(X + 108, Y + 15), 34, Cache.Colors[GameColor.Friendly]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 3", FontType.DialogsSmallerSize7, new Vector2(X + 108, Y + 15), 34, Color.White);

                    // SET 4
                    if ((X + 140 < mouseX) && (X + 175 > mouseX) && (Y + 10 < mouseY) && (Y + 24 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 4", FontType.DialogsSmallerSize7, new Vector2(X + 143, Y + 15), 34, Cache.Colors[GameColor.Friendly]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 4", FontType.DialogsSmallerSize7, new Vector2(X + 143, Y + 15), 34, Color.White);

                    // SET 5
                    if ((X + 175 < mouseX) && (X + 210 > mouseX) && (Y + 10 < mouseY) && (Y + 24 > mouseY))
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 5", FontType.DialogsSmallerSize7, new Vector2(X + 178, Y + 15), 34, Cache.Colors[GameColor.Friendly]);
                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 5", FontType.DialogsSmallerSize7, new Vector2(X + 178, Y + 15), 34, Color.White);

                    /* MINIMIZE BUTTON HIGHLIGHT */
                    if ((X + 222 < mouseX) && (X + 233 > mouseX) && (Y + 14 < mouseY) && (Y + 22 > mouseY))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 222 - 3, Y + 14 - 3), Cache.Interface[sprite].Frames[26].GetRectangle(), Color.White * transparency);
                    }

                    /* PAGINATION */
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 218, Y + 201), Cache.Interface[sprite].Frames[9].GetRectangle(), Color.White * transparency);
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Page: " + config.Page + "/" + config.MaxPages, FontType.DialogsSmallerSize7, new Vector2(X + 191, Y + 200), 25, Cache.Colors[GameColor.Orange]);

                    /* PAGINATION SCROLL HIGHLIGHT*/
                    if ((Y + 201 < mouseY) && (Y + 201 + 7 > mouseY))
                    {
                        if ((X + 218 < mouseX) && (X + +218 + 11 > mouseX)) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 215, Y + 198), Cache.Interface[sprite].Frames[25].GetRectangle(), Color.White * transparency); }
                        if ((X + 218 + 11 < mouseX) && (X + +218 + 11 + 11 > mouseX)) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 215 + 11, Y + 198), Cache.Interface[sprite].Frames[25].GetRectangle(), Color.White * transparency); }
                    }

                    /* ITEM COUNTER */
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 218, Y + 201), Cache.Interface[sprite].Frames[9].GetRectangle(), Color.White * transparency);
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Items: " + player.InventoryCount + "/" + Globals.MaximumInventoryItems, FontType.DialogsSmallerSize7, new Vector2(X + 121, Y + 200), 25, Cache.Colors[GameColor.Orange]); // x was + 191

                    if (player.PlayType == PlayType.PvE)
                    {
                        /* GOLD */
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 6, Y + 196), Cache.Interface[sprite].Frames[5].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", player.Gold), FontType.DialogsSmallerSize7, new Vector2(X + 21, Y + 200), Cache.Colors[GameColor.Orange]);
                    }
                    else if (player.PlayType == PlayType.PvP)
                    {
                        /* GLADIATOR POINTS */
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 6, Y + 196), Cache.Interface[sprite].Frames[12].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", player.GladiatorPoints), FontType.DialogsSmallerSize7, new Vector2(X + 21, Y + 200), Cache.Colors[GameColor.Orange]);
                    }

                    /* GRID HIGHLIGHTS */
                    slotId = Globals.MaximumEquipment + ((config.Page-1)*30); // start at 12 or 42
                    for (int slotY = 0; slotY < 5; slotY++)
                        for (int slotX = 0; slotX < 6; slotX++)
                        {
                            if ((X + 6 + (slotX * 38) < mouseX) && (X + 6 + (slotX * 38) + 37 > mouseX) && (Y + 23 + (slotY * 35) < mouseY) && (Y + 23 + (slotY * 35) + 34 > mouseY))
                            {
                                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 6 + (slotX * 38), Y + 23 + (slotY * 35)), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                                highlightedItemIndex = slotId;
                                highlightedItemLocation = new Vector2(9 + (slotX * 38), 28 + (slotY * 35));
                            }

                            if (player.Inventory[slotId] != null && (player.Inventory[slotId].IsUpgradeItem || player.Inventory[slotId].IsUpgradeIngredient))
                            {
                                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 6 + (slotX * 38), Y + 23 + (slotY * 35)), Cache.Interface[sprite].Frames[11].GetRectangle(), Color.White * transparency);
                            }

                            slotId++;
                        }
                    if (highlightedItemIndex != -1 && player.Inventory[highlightedItemIndex] != null) { selectedItemIndex = highlightedItemIndex; selectedItemLocation = highlightedItemLocation; }
                    else selectedItemIndex = -1;

                    if (player.Inventory.Count > 0)
                    {
                        int slotX = 0; int slotY = 0;
                        for (int itemIndex = Globals.MaximumEquipment + ((config.Page - 1) * 30); itemIndex < Globals.MaximumEquipment + ((config.Page - 1) * 30) + 30; itemIndex++)
                        {
                            if (player.Inventory[itemIndex] != null)
                            {
                                Item item = player.Inventory[itemIndex];

                                bool forSale = (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant].Config.Visible && ((MerchantDialogBox)((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant]).SellList.Contains(itemIndex));
                                bool forTrade = (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.TradeV2].Config.Visible && ((TradeV2DialogBox)((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.TradeV2]).TradeList.Contains(itemIndex));

                                // get sprite frame
                                Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Frames[item.SpriteFrame].GetRectangle();
                                // get draw location offset from center of the slot
                                Rectangle draw = new Rectangle(X + 9 + (slotX * 38) + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                               Y + 28 + (slotY * 35) + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                               f.Width,
                                                               f.Height
                                                               );

                                itemTransparency = 1.0f;
                                if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex == itemIndex)
                                {
                                    clickedX = slotX;
                                    clickedY = slotY;
                                    itemTransparency = 0.6f;
                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 9 + (slotX * 38) + 5, Y + 28 + (slotY * 35) + 5, 27, 24), f, Color.Black * 0.5f);
                                }

                                if (item.IsEquipped) { spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 9 + (slotX * 38) + 5, Y + 28 + (slotY * 35) + 5, 27, 24), f, Cache.Colors[GameColor.Friendly] * 0.1f); }
                                else if (item.IsBroken) { spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 9 + (slotX * 38) + 5, Y + 28 + (slotY * 35) + 5, 27, 24), f, Cache.Colors[GameColor.Enemy] * 0.1f); }

                                if (f.Width > 31 || f.Height > 31)
                                    spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, ((item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]) * itemTransparency);
                                else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Vector2(draw.X, draw.Y), f, ((item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]) * itemTransparency);

                                if (forSale) SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "SELL", FontType.DialogsSmallerSize7, new Vector2(X + 9 + (slotX * 38), Y + 44 + (slotY * 35)), 40, Cache.Colors[GameColor.Enemy]);
                                if (forTrade) SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "TRADE", FontType.DialogsSmallerSize7, new Vector2(X + 9 + (slotX * 38), Y + 44 + (slotY * 35)), 40, Cache.Colors[GameColor.Enemy]);

                                if (item.Count > 1)
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("x{0}", item.Count.ToString()), FontType.DialogsSmallerSize7, new Vector2(X + 9 + (slotX * 38), Y + 23 + (slotY * 35) + 24), 32, Cache.Colors[GameColor.Orange]);
                                if (item.SetNumber > 0)
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", item.SetNumber.ToString()), FontType.DialogsSmallerSize7, new Vector2(X + 9 + (slotX * 38) + 6, Y + 23 + (slotY * 35) + 8), Cache.Colors[GameColor.Orange]);
                            }
                            slotX++;
                            if (slotX > 5) { slotX = 0; slotY++; }
                        }
                    }
                    break;
            }
                                
            if (config.State != DialogBoxState.Compact)
            {
                /* CHARACTER BUTTON */
                if ((X + 32 < mouseX) && (X + 86 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrades", FontType.DialogsSmallSize8, new Vector2(X + 34, Y + 475), 54, Cache.Colors[GameColor.Orange]);
                else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrades", FontType.DialogsSmallSize8, new Vector2(X + 34, Y + 475), 54, Color.White);

                /* STATS BUTTON */
                if ((X + 93 < mouseX) && (X + 147 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Statistics", FontType.DialogsSmallSize8, new Vector2(X + 93, Y + 475), 54, Cache.Colors[GameColor.Orange]);
                else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Statistics", FontType.DialogsSmallSize8, new Vector2(X + 93, Y + 475), 54, Color.White);

                /* QUEST BUTTON */
                if ((X + 154 < mouseX) && (X + 208 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Quests", FontType.DialogsSmallSize8, new Vector2(X + 154, Y + 475), 54, Cache.Colors[GameColor.Orange]);
                else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Quests", FontType.DialogsSmallSize8, new Vector2(X + 154, Y + 475), 54, Color.White);
            }

            ///* DRAGGED ITEM */
            //if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex != -1 && player.Inventory[clickedItemIndex] != null)
            //    ((MainGame)Cache.DefaultState).SetDraggedItem(DraggedType.Inventory, clickedItemIndex);

            //{
            //    Item item = player.Inventory[clickedItemIndex];
            //    // get sprite frame
            //    Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Frames[item.SpriteFrame].GetRectangle();

            //    // get draw location offset from center of the slot
            //    Rectangle draw = new Rectangle(
            //        mouseX - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
            //        mouseY - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
            //        f.Width,
            //        f.Height
            //        );

            //    if (f.Width > 31 || f.Height > 31)
            //        spriteBatch.DrawItemPopup(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, SpriteHelper.ColourFromArgb(item.Colour));
            //    else spriteBatch.DrawItemPopup(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Vector2(draw.X, draw.Y), f, SpriteHelper.ColourFromArgb(item.Colour));

            //    if (item.Count > 1)
            //        SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("x{0}", item.Count.ToString()), FontType.DialogsV2Smaller, new Vector2(mouseX - 18, mouseY + 2), 32, Cache.DefaultState.Display.FontColours[FontType.DialogsV2Orange]);
            //    if (item.SetNumber > 0)
            //        SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", item.SetNumber.ToString()), FontType.DialogsV2Smaller, new Vector2(mouseX - 12, mouseY - 14), Cache.DefaultState.Display.FontColours[FontType.DialogsV2Orange]);
            //}

            /* POPUP */
            if (highlightedItemIndex >= 0 && highlightedItemIndex < Globals.MaximumTotalItems)
            {
                /* ITEM DETAILS */
                if (player.Inventory[highlightedItemIndex] != null)
                    ((MainGame)Cache.DefaultState).SetItemPopup(Type, player.Inventory[highlightedItemIndex], highlightedItemLocation, X, Y);
                    //ItemPopup.DrawItemPopup(spriteBatch, player.Inventory[highlightedItemIndex], highlightedItemLocation, X, Y);
                }

            if (frame != null && Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            player = ((MainGame)Cache.DefaultState).Player;
            transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);
        }

        public void LeftClicked()
        {
            switch (config.State)
            {
                case DialogBoxState.Compact:
                    /* MINIMIZE BUTTON */
                    if ((X + 222 < mouseX) && (X + 233 > mouseX) && (Y + 14 < mouseY) && (Y + 22 > mouseY))
                    {
                        config.State = DialogBoxState.Normal;
                        highlightedItemIndex = -1;
                        return;
                    }

                    /* PAGINATION SCROLL BUTTON*/
                    if ((Y + 201 < mouseY) && (Y + 201 + 7 > mouseY))
                    {
                        if ((X + 218 < mouseX) && (X + +218 + 11 > mouseX)) { Scroll(+1); }
                        if ((X + 218 + 11 < mouseX) && (X + +218 + 11 + 11 > mouseX)) { Scroll(-1); }
                    }

                    // SETS 1-5
                    if ((Y + 10 < mouseY) && (Y + 24 > mouseY))
                    {
                        // SET 1
                        if ((X + 35 < mouseX) && (X + 70 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(1); }
                        // SET 2
                        if ((X + 70 < mouseX) && (X + 105 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(2); }
                        // SET 3
                        if ((X + 105 < mouseX) && (X + 140 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(3); }
                        // SET 4
                        if ((X + 140 < mouseX) && (X + 175 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(4); }
                        // SET 5
                        if ((X + 175 < mouseX) && (X + 210 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(5); }
                    }
                    break;
                case DialogBoxState.Normal:
                    // SETS 1-5
                    if ((Y + 254 < mouseY) && (Y + 268 > mouseY))
                    {
                        // SET 1
                        if ((X + 17 < mouseX) && (X + 52 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(1); }
                        // SET 2
                        if ((X + 60 < mouseX) && (X + 95 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(2); }
                        // SET 3
                        if ((X + 103 < mouseX) && (X + 138 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(3); }
                        // SET 4
                        if ((X + 146 < mouseX) && (X + 181 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(4); }
                        // SET 5
                        if ((X + 189 < mouseX) && (X + 224 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(5); }
                    }

                    //CREATE SET
                    if ((Y + 225 < mouseY) && (Y + 247 > mouseY))
                    {
                        if ((X + 59 < mouseX) && (X + 116 > mouseX))
                        {
                            //Hide clear set dialog box
                            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Hide();

                            //Update x & y then show
                            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Config.X = X + 35;
                            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Config.Y = Y + 110;
                            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Toggle();
                        }

                        //CLEAR SET
                        if ((X + 123 < mouseX) && (X + 182 > mouseX))
                        {
                            //Hide create set dialog box
                            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Hide();

                            //Update x & y, then show
                            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Config.X = X + 35;
                            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Config.Y = Y + 110;
                            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Toggle();
                        }
                    }

                    //PARTY
                    if ((Y + 185 < mouseY) && (Y + 207 > mouseY))
                    {
                        if ((X + 59 < mouseX) && (X + 116 > mouseX))
                        {
                            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Party].Toggle();
                        }
                    }


                    /* UPGRADE BUTTON */
                    if ((X + 32 < mouseX) && (X + 86 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                    {
                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.UpgradeV2].Toggle();
                        //config.State = DialogBoxState.Normal;
                        //SetPagination();
                        //highlightedItemIndex = -1;
                        return;
                    }

                    /* STATS BUTTON */
                    if ((X + 93 < mouseX) && (X + 147 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                    {
                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Stats].Toggle();
                        return;
                    }

                    /* QUEST BUTTON */
                    if ((X + 154 < mouseX) && (X + 208 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                    {
                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Quest].Toggle();
                        return;
                    }

                    /* MAXIMIZE BUTTON */
                    if ((X + 222 < mouseX) && (X + 233 > mouseX) && (Y + 14 < mouseY) && (Y + 22 > mouseY))
                    {
                        config.State = DialogBoxState.Compact;
                        SetPagination();
                        highlightedItemIndex = -1;
                        return;
                    }

                    /* PAGINATION SCROLL BUTTON*/
                    if ((Y + 459 < mouseY) && (Y + 459 + 7 > mouseY))
                    {
                        if ((X + 215 < mouseX) && (X + +215 + 11 > mouseX)) { Scroll(+1); }
                        if ((X + 215 + 11 < mouseX) && (X + +215 + 11 + 11 > mouseX)) { Scroll(-1); }
                    }
                    break;
                default: break;
            }

            if (clickedItemIndex != -1 && player.Inventory[clickedItemIndex] != null)
                ((MainGame)Cache.DefaultState).SetDraggedItem(DraggedType.Inventory, clickedItemIndex);

            // unused - this is for sprite from 6 (has the action/drop/upgrade buttons on bottom of popup)
            /*Player player = ((MainGame)Cache.GameState).Player;
            Vector2 popupLocation;

            if (highlightedItemIndex != -1)
            {
                AnimationFrame popupFrame = Cache.Interface[sprite].Frames[6];
                int offsetX = 0; int offsetY = 0;
                if (x + highlightedItemLocation.X + 18 + popupFrame.Width > Cache.GameState.Display.ResolutionWidth) offsetX -= popupFrame.Width;
                if (y + highlightedItemLocation.Y + 17 + popupFrame.Height > Cache.GameState.Display.ResolutionHeight) offsetY -= popupFrame.Height;

                popupLocation = new Vector2(x + highlightedItemLocation.X + 18 + offsetX, y + highlightedItemLocation.Y + 17 + offsetY);

                if ((popupLocation.X < mouseX) && (popupLocation.X + 172 > mouseX) && (popupLocation.Y + 8 < mouseY) && (popupLocation.Y + 125 > mouseY))
                {
                    // ACTION CLICK 
                    if ((popupLocation.X + 16 < mouseX) && (popupLocation.X + 60 > mouseX) && (popupLocation.Y + 106 < mouseY) && (popupLocation.Y + 126 > mouseY))
                        if (player.InventoryV1[highlightedItemIndex] != null)
                        {
                            Item item = player.InventoryV1[highlightedItemIndex];
                            switch (item.Type)
                            {
                                case ItemType.Equip: 
                                    if (item.IsEquipped) 
                                        ((MainGame)Cache.GameState).UnEquipItem(highlightedItemIndex); 
                                    else ((MainGame)Cache.GameState).EquipItem(highlightedItemIndex); break;
                                default: ((MainGame)Cache.GameState).UseItem(highlightedItemIndex); break;
                            }
                            highlightedItemIndex = -1;
                        }

                    // UPGRADE CLICK 
                    if ((popupLocation.X + 63 < mouseX) && (popupLocation.X + 106 > mouseX) && (popupLocation.Y + 106 < mouseY) && (popupLocation.Y + 126 > mouseY))
                    {
                        highlightedItemIndex = -1;
                    }

                    // DROP CLICK 
                    if ((popupLocation.X + 110 < mouseX) && (popupLocation.X + 153 > mouseX) && (popupLocation.Y + 106 < mouseY) && (popupLocation.Y + 126 > mouseY))
                        if (player.InventoryV1[highlightedItemIndex] != null)
                        {
                            ((MainGame)Cache.GameState).DropItem(highlightedItemIndex); 
                            highlightedItemIndex = -1;
                        }
                }
                else if (selectedItemIndex == highlightedItemIndex) highlightedItemIndex = -1; // click same item = close popup
                else if (selectedItemIndex == -1) highlightedItemIndex = -1; // click blank item = close popup
                else highlightedItemIndex = selectedItemIndex; highlightedItemLocation = new Vector2(selectedItemLocation.X, selectedItemLocation.Y); // click other item = change popup
            }
            else if (selectedItemIndex != -1) { highlightedItemIndex = selectedItemIndex; highlightedItemLocation = new Vector2(selectedItemLocation.X, selectedItemLocation.Y); }
             */
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
            if (selectionMode == SelectionMode.ApplyItem)
            {                      
                if (SelectedItemIndex != -1)   
                    ((MainGame)Cache.DefaultState).ApplyItem(selectionModeId, selectedItemIndex);

                //clear selection mode for apply item
                selectionMode = SelectionMode.None;
                selectionModeId = -1;
            }
            // special case - add sell item if merchant window open + in sell state
            else if (SelectedItemIndex != -1)
            {
                if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant].Config.Visible)
                    switch (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant].Config.State)
                    {
                        case DialogBoxState.Sell: ((MainGame)Cache.DefaultState).AddSellItem(SelectedItemIndex); break;
                        case DialogBoxState.Repair: ((MainGame)Cache.DefaultState).AddRepairItem(SelectedItemIndex); break;
                    }
                else if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.TradeV2].Config.Visible)
                    ((MainGame)Cache.DefaultState).AddTradeItem(SelectedItemIndex);
                else if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.WarehouseV2].Config.Visible)
                    ((MainGame)Cache.DefaultState).ItemToWarehouse(SelectedItemIndex, -1);
                else if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.UpgradeV2].Config.Visible)
                    ((MainGame)Cache.DefaultState).ChangeUpgradeItem(SelectedItemIndex);
                else ((MainGame)Cache.DefaultState).UseItem(SelectedItemIndex);
            }
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {

        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
            switch (highlightedDialogBox)
            {
                case GameDialogBoxType.None: // no destination dialog box
                    // check for player highlight (not self)
                    int selectedObjectId = ((MainGame)Cache.DefaultState).SelectedObjectId;
                    if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
                    {
                        IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];
                        if (owner.OwnerType == OwnerType.Player)
                        {
                            ((MainGame)Cache.DefaultState).AddEvent(string.Format("Requesting trade with {0}...", owner.Name));
                            ((MainGame)Cache.DefaultState).RequestTrade(owner.ObjectId, clickedItemIndex);
                        }
                    }
                    else ((MainGame)Cache.DefaultState).DropItem(clickedItemIndex);
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.CharacterV2: // moving items around the dialog box //TODO - create a general equiplocation?
                    if (clickedItemIndex != highlightedDialogBoxItemIndex)
                        ((MainGame)Cache.DefaultState).SwapItem(clickedItemIndex, highlightedDialogBoxItemIndex);
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.Merchant:
                    if (clickedItemIndex != -1)
                        switch (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant].Config.State)
                        {
                            case DialogBoxState.Sell: ((MainGame)Cache.DefaultState).AddSellItem(clickedItemIndex); break;
                            case DialogBoxState.Repair: ((MainGame)Cache.DefaultState).AddRepairItem(clickedItemIndex); break;
                        }
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.TradeV2:
                    if (clickedItemIndex != -1) ((MainGame)Cache.DefaultState).AddTradeItem(clickedItemIndex);
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.UpgradeV2:
                    if (clickedItemIndex != -1)
                    {
                        ((MainGame)Cache.DefaultState).ChangeUpgradeItem(clickedItemIndex);
                        //dialogBoxes[clickedDialogBox].ClickedItemIndex = -1;
                    }
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.WarehouseV2:
                    if (clickedItemIndex != -1)
                        ((MainGame)Cache.DefaultState).ItemToWarehouse(clickedItemIndex, highlightedDialogBoxItemIndex);
                    highlightedItemIndex = -1;
                    break;
                default: 
                    ((MainGame)Cache.DefaultState).DropItem(clickedItemIndex);
                    highlightedItemIndex = -1;
                    break;
            }
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;

            highlightedItemIndex = -1; // clear popup
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
            if (config.Visible && state == config.State) { Hide(); }
            else
            {
                config.State = state;
                SetPagination();
                Show();
            }
        }

        public void Show()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
               ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }

            //reset important variables
            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            clickedX = clickedY = selectedItemIndexOffsetX = selectedItemIndexOffsetY = 0; 
            itemTransparency = 1.0f;
            
            config.Hidden = true;
        }

        private void SetPagination()
        {
            switch (config.State)
            {
                case DialogBoxState.Compact:
                case DialogBoxState.Normal:
                    config.MaxPages = 3;
                    config.Page = 1;
                    break;
                //case DialogBoxState.Statistics:
                //    config.MaxPages = 5;
                //    config.Page = 1;
                    //break;
                //case DialogBoxState.Quest:
                default:
                    break;
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
