﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Text;

//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Input;

//using HelbreathWorld.Common.Assets;
//using HelbreathWorld.Game.Assets.State;

//namespace HelbreathWorld.Game.Assets.UI
//{
//    public class SpellBookV2DialogBox : IGameDialogBox
//    {
//        private MagicAttribute element = MagicAttribute.Earth;

//        //Base graphic info
//        int largeSprite = (int)SpriteId.DialogsV2 + 1; //Main texture
//        int largeSpriteFrame = 7; //rectangle in texutre
//        AnimationFrame largeFrame;

//        // selected items
//        private int clickedItemIndex; // clicked item
//        private int selectedItemIndex; // mouse over item
//        private int selectedItemIndexOffsetX;
//        private int selectedItemIndexOffsetY;
//        private int highlightedItemIndex; // chosen item (non volatile)
//        private GameDialogBoxConfiguration config;

//        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
//        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
//        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
//        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
//        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }

//        public GameDialogBoxType Type { get { return GameDialogBoxType.SpellBookV2; } }
//        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }

//        int mouseX;
//        int mouseY;
//        int X;
//        int Y;
//        float transparency;
//        Player player;

//        public SpellBookV2DialogBox()
//        {
//            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
//            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
//            largeFrame = Cache.Interface[largeSprite].Frames[largeSpriteFrame];
//            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
//            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
//            X = config.X;
//            Y = config.Y;
//            transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);
//            player = ((MainGame)Cache.DefaultState).Player;
//            SetMaxPages();
//        }

//        public SpellBookV2DialogBox(GameDialogBoxConfiguration config)
//        {
//            this.config = config;
//            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
//            largeFrame = Cache.Interface[largeSprite].Frames[largeSpriteFrame];
//            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
//            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
//            X = config.X;
//            Y = config.Y;
//            transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);
//            player = ((MainGame)Cache.DefaultState).Player;
//            SetMaxPages();
//        }

//        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
//        {
//            if (config.Hidden)
//                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
//                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

//            highlightedItemIndex = -1;

//            spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X, Y), largeFrame.GetRectangle(), Color.White * transparency);
            
//            /* EARTH */
//            if (!Utility.IsSelected(mouseX, mouseY, X + 284, Y + 77, X + 284 + 30, Y + 77 + 32) && element != MagicAttribute.Earth)
//                spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 284, Y + 77), Cache.Interface[largeSprite].Frames[9].GetRectangle(), Color.White * transparency);
//            else spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 284, Y + 77), Cache.Interface[largeSprite].Frames[15].GetRectangle(), Color.White * transparency);
//            /* AIR */
//            if (!Utility.IsSelected(mouseX, mouseY, X + 325, Y + 101, X + 325 + 30, Y + 101 + 32) && element != MagicAttribute.Air)
//                spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 325, Y + 101), Cache.Interface[largeSprite].Frames[11].GetRectangle(), Color.White * transparency);
//            else spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 325, Y + 101), Cache.Interface[largeSprite].Frames[17].GetRectangle(), Color.White * transparency);
//            /* FIRE */
//            if (!Utility.IsSelected(mouseX, mouseY, X + 325, Y + 153, X + 325 + 30, Y + 153 + 32) && element != MagicAttribute.Fire)
//                spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 325, Y + 153), Cache.Interface[largeSprite].Frames[10].GetRectangle(), Color.White * transparency);
//            else spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 325, Y + 153), Cache.Interface[largeSprite].Frames[16].GetRectangle(), Color.White * transparency);
//            /* WATER */
//            if (!Utility.IsSelected(mouseX, mouseY, X + 244, Y + 101, X + 244 + 30, Y + 101 + 32) && element != MagicAttribute.Water)
//                spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 244, Y + 101), Cache.Interface[largeSprite].Frames[8].GetRectangle(), Color.White * transparency);
//            else spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 244, Y + 101), Cache.Interface[largeSprite].Frames[14].GetRectangle(), Color.White * transparency);
//            /* KINETIC */
//            if (!Utility.IsSelected(mouseX, mouseY, X + 244, Y + 153, X + 244 + 30, Y + 153 + 32) && element != MagicAttribute.Kinesis)
//                spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 244, Y + 153), Cache.Interface[largeSprite].Frames[12].GetRectangle(), Color.White * transparency);
//            else spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 244, Y + 153), Cache.Interface[largeSprite].Frames[18].GetRectangle(), Color.White * transparency);
//            /* SPIRIT */
//            if (!Utility.IsSelected(mouseX, mouseY, X + 284, Y + 173, X + 284 + 30, Y + 173 + 32) && element != MagicAttribute.Spirit)
//                spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 284, Y + 173), Cache.Interface[largeSprite].Frames[13].GetRectangle(), Color.White * transparency);
//            else spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 284, Y + 173), Cache.Interface[largeSprite].Frames[19].GetRectangle(), Color.White * transparency);
//            /* UTILITY */
//            if (!Utility.IsSelected(mouseX, mouseY, X + 284, Y + 233, X + 284 + 30, Y + 233 + 32) && element != MagicAttribute.None)
//                spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 284, Y + 233), Cache.Interface[largeSprite].Frames[24].GetRectangle(), Color.White * transparency);
//            else spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 284, Y + 233), Cache.Interface[largeSprite].Frames[23].GetRectangle(), Color.White * transparency);
//            /* ALL */
//            if (!Utility.IsSelected(mouseX, mouseY, X + 284, Y + 25, X + 284 + 30, Y + 25 + 32) && element != MagicAttribute.All)
//                spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 284, Y + 25), Cache.Interface[largeSprite].Frames[24].GetRectangle(), Color.White * transparency);
//            else spriteBatch.Draw(Cache.Interface[largeSprite].Texture, new Vector2(X + 284, Y + 25), Cache.Interface[largeSprite].Frames[23].GetRectangle(), Color.White * transparency);
            
//            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, (element == MagicAttribute.None ? "OTHER" : element == MagicAttribute.All ? "ALL" : element.ToString().ToUpper()), FontType.DialogsV2Small, new Vector2(X + 268, Y + 128), 65, Cache.DefaultState.Display.FontColours[FontType.DialogsV2Orange]);
//            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("{0}/{1}", config.Page, config.MaxPages), FontType.DialogsV2Smaller, new Vector2(X + 268, Y + 148), 65, Cache.DefaultState.Display.FontColours[FontType.DialogsV2Orange]);
            
//            // loops through each spell for each page and displays the image, name, manacost etc
//            // also tracks the current page and remembers the spell mouse is hovering over for the LeftClick() to handle
//            int index = 0; int indexLeft = 0; int indexRight = 0; int selectedIndex = -1; int column = 0; int row = 0;
            
//            if (element == MagicAttribute.All)
//            {
//                for (int i = 0; i < Globals.MaximumSpells; i++)
//                    if (Cache.MagicConfiguration.ContainsKey(i) && player.MagicLearned[i])
//                    {
//                        if (index >= (config.Page - 1) * 84 && index < config.Page * 84) //How many per page. 
//                        {
//                            bool cantCast = player.IsCasting || !player.CanCast(i) || !player.HandsFree;
            
//                            if (indexLeft < 42)
//                            {
//                                bool hover = Utility.IsSelected(mouseX, mouseY, X + 40 + (column * 32), Y + 41 + (row * 32), X + 40 + (column * 32) + 32, Y + 41 + (row * 32) + 32);
//                                spriteBatch.Draw(Cache.Interface[largeSprite + 3].Texture, new Vector2(X + 40 + (column * 32), Y + 41 + (row * 32)), Cache.Interface[largeSprite + 3].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), (cantCast ? Color.Gray : Color.White) * transparency);
//                                indexLeft++;
            
//                                if (hover)
//                                {
//                                    int probability = Math.Min(100, Utility.GetCastingProbability(player.Skills[(int)SkillType.Magic], Cache.MagicConfiguration[i].Level, player.Level, player.Intelligence, (((MainGame)Cache.DefaultState).Weather != null ? ((MainGame)Cache.DefaultState).Weather.Type : WeatherType.Clear), player.Inventory.CastProbabilityBonus, player.SP));
                                            
//                                    SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Spell: {0}", Cache.MagicConfiguration[i].Name), FontType.DialogsV2Small, new Vector2(X + 80, Y + 20), !cantCast ? Cache.DefaultState.Display.FontColours[FontType.Friendly] : Cache.DefaultState.Display.FontColours[FontType.Enemy]);
//                                    SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Mana: {0}    CP: {1}%", player.RequiredMana(i), probability), FontType.DialogsV2Small, new Vector2(X + 400, Y + 20), !cantCast ? Cache.DefaultState.Display.FontColours[FontType.Friendly] : Cache.DefaultState.Display.FontColours[FontType.Enemy]);
            
//                                    //DrawItemPopup name over icon
//                                    //SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.MagicConfiguration[i].Name, FontType.DialogsV2Small, new Vector2(x + 40 + (column * 32), y + 41 + (row * 32)), (hover ? (player.CanCast(i) ? Cache.GameState.Display.FontColours[FontType.Friendly] : Cache.GameState.Display.FontColours[FontType.Enemy]) : Cache.GameState.Display.FontColours[FontType.DialogsV2Orange]));
//                                    selectedIndex = i; // selected spell for cast
//                                }
//                            }
//                            else if (indexRight < 42)
//                            {
//                                bool hover = Utility.IsSelected(mouseX, mouseY, X + 365 + (column * 32), Y + 41 + (row * 32), X + 365 + (column * 32) + 32, Y + 41 + (row * 32) + 32);
            
//                                spriteBatch.Draw(Cache.Interface[largeSprite + 3].Texture, new Vector2(X + 365 + (column * 32), Y + 41 + (row * 32)), Cache.Interface[largeSprite + 3].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), (cantCast ? Color.Gray : Color.White) * transparency);
//                                indexRight++;
//                                if (hover)
//                                {
//                                    int probability = Math.Min(100, Utility.GetCastingProbability(player.Skills[(int)SkillType.Magic], Cache.MagicConfiguration[i].Level, player.Level, player.Intelligence, (((MainGame)Cache.DefaultState).Weather != null ? ((MainGame)Cache.DefaultState).Weather.Type : WeatherType.Clear), player.Inventory.CastProbabilityBonus, player.SP));
            
//                                    SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Spell: {0}", Cache.MagicConfiguration[i].Name), FontType.DialogsV2Small, new Vector2(X + 80, Y + 20), !cantCast ? Cache.DefaultState.Display.FontColours[FontType.Friendly] : Cache.DefaultState.Display.FontColours[FontType.Enemy]);
//                                    SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Mana: {0}    CP: {1}%", player.RequiredMana(i), probability), FontType.DialogsV2Small, new Vector2(X + 400, Y + 20), !cantCast ? Cache.DefaultState.Display.FontColours[FontType.Friendly] : Cache.DefaultState.Display.FontColours[FontType.Enemy]);
            
//                                    selectedIndex = i; // selected spell for cast
//                                }
//                            }
//                        }
//                        index++;
//                        row++;
            
//                        if (index % 7 == 0)
//                        {
//                            column++;
//                            row = 0;
//                        }
            
//                        if (index == 42)
//                        {
//                            column = 0;
//                        }
//                    }
//            }
//            else
//            {
//                for (int i = 0; i < Globals.MaximumSpells; i++)
//                    if (Cache.MagicConfiguration.ContainsKey(i) && Cache.MagicConfiguration[i].Attribute == element && player.MagicLearned[i])
//                    {
//                        if (index >= (config.Page - 1) * 10 && index < config.Page * 10)
//                        {
//                            bool cantCast = player.IsCasting || !player.CanCast(i) || !player.HandsFree;
            
//                            if (indexLeft < 5)
//                            {
//                                int probability = Math.Min(100, Utility.GetCastingProbability(player.Skills[(int)SkillType.Magic], Cache.MagicConfiguration[i].Level, player.Level, player.Intelligence, (((MainGame)Cache.DefaultState).Weather != null ? ((MainGame)Cache.DefaultState).Weather.Type : WeatherType.Clear), player.Inventory.CastProbabilityBonus, player.SP));
            
//                                bool hover = Utility.IsSelected(mouseX, mouseY, X + 45, Y + 41 + (indexLeft * 45) - 5, X + 45 + 150, Y + 41 + (indexLeft * 45) - 5 + 45);
            
//                                spriteBatch.Draw(Cache.Interface[largeSprite + 3].Texture, new Vector2(X + 55, Y + 41 + (indexLeft * 45) + 1), Cache.Interface[largeSprite + 3].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), (!cantCast ? Color.White : Color.Gray) * transparency);
//                                SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.MagicConfiguration[i].Name, FontType.DialogsV2Small, new Vector2(X + 95, Y + 40 + (indexLeft * 45)), (hover ? (!cantCast ? Cache.DefaultState.Display.FontColours[FontType.Friendly] : Color.DimGray) : !cantCast ? Cache.DefaultState.Display.FontColours[FontType.DialogsV2Orange] : Color.DimGray));
//                                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Mana: {0}    CP: {1}%", player.RequiredMana(i), probability), FontType.DialogsV2Smaller, new Vector2(X + 95, Y + 54 + (indexLeft * 45) + 1), Color.White);
//                                indexLeft++;
            
//                                if (hover) selectedIndex = i; // selected spell for cast
//                            }
//                            else if (indexRight < 5)
//                            {
//                                int probability = Math.Min(100, Utility.GetCastingProbability(player.Skills[(int)SkillType.Magic], Cache.MagicConfiguration[i].Level, player.Level, player.Intelligence, (((MainGame)Cache.DefaultState).Weather != null ? ((MainGame)Cache.DefaultState).Weather.Type : WeatherType.Clear), player.Inventory.CastProbabilityBonus, player.SP));
            
//                                bool hover = Utility.IsSelected(mouseX, mouseY, X + 390, Y + 41 + (indexRight * 45) - 5, X + 390 + 150, Y + 41 + (indexRight * 45) - 5 + 45);
            
//                                spriteBatch.Draw(Cache.Interface[largeSprite + 3].Texture, new Vector2(X + 400, Y + 41 + (indexRight * 45) + 1), Cache.Interface[largeSprite + 3].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), (!cantCast ? Color.White : Color.Gray) * transparency);
//                                SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.MagicConfiguration[i].Name, FontType.DialogsV2Small, new Vector2(X + 440, Y + 40 + (indexRight * 45)), (hover ? (!cantCast ? Cache.DefaultState.Display.FontColours[FontType.Friendly] : Color.DimGray) : !cantCast ? Cache.DefaultState.Display.FontColours[FontType.DialogsV2Orange] : Color.DimGray));
//                                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Mana: {0}    CP: {1}%", player.RequiredMana(i), probability), FontType.DialogsV2Smaller, new Vector2(X + 440, Y + 54 + (indexRight * 45) + 1), Color.White);
//                                indexRight++;
            
//                                if (hover) selectedIndex = i; // selected spell for cast
//                            }
//                        }
            
//                        index++;
//                    }
//            }
            
//            highlightedItemIndex = selectedIndex;
            
//            if (clickedItemIndex == -1) // if no spell dragging
//                if (selectedIndex != -1) // and no spell hovered
//                    selectedItemIndex = selectedIndex; // set hover index
//                else selectedItemIndex = -1; // cancel hovered spell when not dragging
            
//            //DRAGGED ITEM
//            //if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex != -1)
//            //    ((MainGame)Cache.DefaultState).SetDraggedItem(DraggedType.MagicIcon, clickedItemIndex);
            
//            /* POPUP */
//            if (element ==  MagicAttribute.All && highlightedItemIndex != -1)
//            {
//                ((MainGame)Cache.DefaultState).SetMagicPopup(Type, highlightedItemIndex, new Vector2(mouseX + 10, mouseY - 10), 0, 0);
//            }
            
//            if (Utility.IsSelected(mouseX, mouseY, X, Y, X + largeFrame.Width, Y + largeFrame.Height))
//                return true;
//            else return false;
//        }

//        public void Update(GameTime gameTime)
//        {
//            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
//            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
//            X = config.X;
//            Y = config.Y;
//            transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);
//            player = ((MainGame)Cache.DefaultState).Player;
//        }

//        public void LeftClicked()
//        {

//            if (Utility.IsSelected(mouseX, mouseY, X + 284, Y + 77, X + 284 + 30, Y + 77 + 32)) { element = MagicAttribute.Earth; SetMaxPages(); config.Page = 1; }
//            /* AIR */
//            if (Utility.IsSelected(mouseX, mouseY, X + 325, Y + 101, X + 325 + 30, Y + 101 + 32)) { element = MagicAttribute.Air; SetMaxPages(); config.Page = 1; }
//            /* FIRE */
//            if (Utility.IsSelected(mouseX, mouseY, X + 325, Y + 153, X + 325 + 30, Y + 153 + 32)) { element = MagicAttribute.Fire; SetMaxPages(); config.Page = 1; }
//            /* WATER */
//            if (Utility.IsSelected(mouseX, mouseY, X + 244, Y + 101, X + 244 + 30, Y + 101 + 32)) { element = MagicAttribute.Water; SetMaxPages(); config.Page = 1; }
//            /* KINETIC */
//            if (Utility.IsSelected(mouseX, mouseY, X + 244, Y + 153, X + 244 + 30, Y + 153 + 32)) { element = MagicAttribute.Kinesis; SetMaxPages(); config.Page = 1; }
//            /* SPIRIT */
//            if (Utility.IsSelected(mouseX, mouseY, X + 284, Y + 173, X + 284 + 30, Y + 173 + 32)) { element = MagicAttribute.Spirit; SetMaxPages(); config.Page = 1; }
//            /* UTILITY */
//            if (Utility.IsSelected(mouseX, mouseY, X + 284, Y + 233, X + 284 + 30, Y + 233 + 32)) { element = MagicAttribute.None; SetMaxPages(); config.Page = 1; }
//            /* ALL */
//            if (Utility.IsSelected(mouseX, mouseY, X + 284, Y + 25, X + 284 + 30, Y + 25 + 32)) { element = MagicAttribute.All; SetMaxPages(); config.Page = 1; }


//            /* CLICK TO CAST A SPELL */
//            if (highlightedItemIndex != -1)
//            {
//                if (player.HandsFree)
//                {
//                    if (player.CanCast(highlightedItemIndex))
//                    {
//                        if (!player.MoveReady) player.SpellRequest = highlightedItemIndex;
//                        else if (!player.IsCasting)
//                        {
//                            player.Idle(player.Direction, true);
//                            ((MainGame)Cache.DefaultState).Idle(player.Direction);

//                            player.Cast(highlightedItemIndex);
//                            ((MainGame)Cache.DefaultState).Cast(highlightedItemIndex);
//                            Hide();
//                        }
//                        else Cache.DefaultState.AddEvent("Already casting a spell");
//                    }
//                    else Cache.DefaultState.AddEvent("Not enough mana or intelligence to cast.");
//                }
//                else Cache.DefaultState.AddEvent("Your hands must be free to cast magic.");
//            }

//            //if (clickedItemIndex != -1)
//            //    ((MainGame)Cache.DefaultState).SetDraggedItem(DraggedType.MagicIcon, clickedItemIndex);
//        }

//        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
//        {

//        }

//        public void LeftHeld() { }

//        public void LeftDragged() { }

//        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex) { }

//        public void RightClicked() { }

//        public void RightHeld() { }

//        public void RightReleased() { }

//        public void Scroll(int direction)
//        {
//            if (config.Page - direction > config.MaxPages)
//            {
//                // clockwise
//                switch (element)
//                {
//                    case MagicAttribute.Water: element = MagicAttribute.Earth; break;
//                    case MagicAttribute.Earth: element = MagicAttribute.Air; break;
//                    case MagicAttribute.Air: element = MagicAttribute.Fire; break;
//                    case MagicAttribute.Fire: element = MagicAttribute.Spirit; break;
//                    case MagicAttribute.Spirit: element = MagicAttribute.Kinesis; break;
//                    case MagicAttribute.Kinesis: element = MagicAttribute.Water; break;
//                }
//                SetMaxPages();
//                config.Page = 1;
//            }
//            else if (config.Page - direction < 1)
//            {
//                // anti- clockwise
//                switch (element)
//                {
//                    case MagicAttribute.Water: element = MagicAttribute.Kinesis; break;
//                    case MagicAttribute.Kinesis: element = MagicAttribute.Spirit; break;
//                    case MagicAttribute.Spirit: element = MagicAttribute.Fire; break;
//                    case MagicAttribute.Fire: element = MagicAttribute.Air; break;
//                    case MagicAttribute.Air: element = MagicAttribute.Earth; break;
//                    case MagicAttribute.Earth: element = MagicAttribute.Water; break;
//                }
//                SetMaxPages();
//                config.Page = config.MaxPages;
//            }
//            else config.Page -= direction;
//            selectedItemIndex = highlightedItemIndex = -1; // clear popup
//        }

//        public void OffsetLocation(int x, int y)
//        {
//            config.X += x;
//            config.Y += y;
//        }

//        private void SetMaxPages()
//        {
//            // calculate maxPages for this element
//            // TODO - make this quicker
//            config.Page = 1;
//            Player player = ((MainGame)Cache.DefaultState).Player;
//            int spellCount = 0;

//            //if (element == MagicAttribute.All)
//            //{
//            //    for (int i = 0; i < Globals.MaximumSpells; i++)
//            //        if (Cache.MagicConfiguration.ContainsKey(i) && player.MagicLearned[i])
//            //        {
//            //            spellCount++;
//            //        }

//            //    if (spellCount > 0)
//            //        config.MaxPages = ((spellCount - 1) / 84) + 1;
//            //    else config.MaxPages = 1;
//            //}
//            //else
//            {
//                for (int i = 0; i < Globals.MaximumSpells; i++)
//                    if (Cache.MagicConfiguration.ContainsKey(i) && Cache.MagicConfiguration[i].Attribute == element && player.MagicLearned[i])
//                        spellCount++;

//                if (spellCount > 0)
//                    config.MaxPages = ((spellCount - 1) / 10) + 1;
//                else config.MaxPages = 1;
//            }
//        }

//        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
//        {
//            if (config.Visible && state == config.State) Hide();
//            else
//            {
//                if (config.State != state)
//                {
//                    config.State = state;
//                    SetMaxPages();
//                }
//                Show();
//            }
//        }

//        public void Show()
//        {
//            if (!config.AlwaysVisible && Cache.DefaultState != null)
//                ((IGameState)Cache.DefaultState).BringToFront(Type);
//            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
//            config.Hidden = false;
//        }

//        public void Hide()
//        {
//            if (!config.AlwaysVisible && Cache.DefaultState != null)
//            {
//                ((MainGame)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
//                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
//            }
//            config.Hidden = true;
//            clickedItemIndex = selectedItemIndex = highlightedItemIndex = -1;
//        }

//        public void SetData(byte[] data)
//        {

//        }
//    }
//}



