﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.UI
{
    public class ChatDialogBox : IGameDialogBox
    {
        int scrollPosition;
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items
              
        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 1; //Main texture
        int spriteFrame = 0; //rectangle in texutre
        int spriteFrameHover = -1; //frame to display when hovering mouse over
        int width = -1; //frame width
        int height = -1; //frame height 

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.Chat; } }

        public ChatDialogBox()
        {    
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);   
            //Assign height and width if has sprite
            if (sprite != -1 && spriteFrame != -1)
            {
                width = Cache.Interface[sprite].Frames[spriteFrame].Width;
                height = Cache.Interface[sprite].Frames[spriteFrame].Height;
            }
        }


        public ChatDialogBox(GameDialogBoxConfiguration config)
        {    
            this.config = config;
            //Assign height and width if has sprite
            if (sprite != -1 && spriteFrame != -1)
            {
                width = Cache.Interface[sprite].Frames[spriteFrame].Width;
                height = Cache.Interface[sprite].Frames[spriteFrame].Height;
            }
        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {       
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int x = config.X;
            int y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;
       
            //Draws base frame if not null
            AnimationFrame frame; // holds frame in case we need to check bounds later
            if (sprite != -1 && spriteFrame != -1) // some dialog boxes dont have a box sprite such as minimap
            {
                if (spriteFrameHover != -1 &&
                    (mouseX > x) && (mouseX < x + width) && (mouseY > y) && (mouseY <= y + height)) //Hover
                    frame = Cache.Interface[sprite].Frames[spriteFrameHover];
                else frame = Cache.Interface[sprite].Frames[spriteFrame]; //Normal 
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture
            }
            else frame = null;

            //draw extra 
            List<ChatMessage> messages = ((MainGame)Cache.DefaultState).ChatMessages;

            if (messages.Count > 14)
            {
                int position = 188 - (int)(((double)scrollPosition * 188.0f) / (double)Math.Min(Globals.MaximumChatHistory - 14, messages.Count - 14));
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + width - 26, y + 12 + position), Cache.Interface[sprite].Frames[4].GetRectangle(), Color.White * transparency);
            }

            /* LOCAL CHAT FILTER */
            if (Cache.GameSettings.ChatFilters[GameColor.Local])
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 93, y + 217), Cache.Interface[sprite].Frames[6].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Local", FontType.DialogsSmallSize8, new Vector2(x + 93, y + 221), Cache.Interface[sprite].Frames[6].Width, Cache.Colors[GameColor.Local] * transparency);
            }
            else
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 93, y + 217), Cache.Interface[sprite].Frames[5].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Local", FontType.DialogsSmallSize8, new Vector2(x + 93, y + 221), Cache.Interface[sprite].Frames[5].Width, Color.DarkGray * transparency);
            }
            /* WHISPER CHAT FILTER */
            if (Cache.GameSettings.ChatFilters[GameColor.Whisper])
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 141, y + 217), Cache.Interface[sprite].Frames[6].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Whisper", FontType.DialogsSmallSize8, new Vector2(x + 141, y + 221), Cache.Interface[sprite].Frames[6].Width, Cache.Colors[GameColor.Whisper] * transparency);
            }
            else
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 141, y + 217), Cache.Interface[sprite].Frames[5].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Whisper", FontType.DialogsSmallSize8, new Vector2(x + 141, y + 221), Cache.Interface[sprite].Frames[5].Width, Color.DarkGray * transparency);
            }
            /* GLOBAL CHAT FILTER */
            if (Cache.GameSettings.ChatFilters[GameColor.Global])
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 189, y + 217), Cache.Interface[sprite].Frames[6].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Global", FontType.DialogsSmallSize8, new Vector2(x + 189, y + 221), Cache.Interface[sprite].Frames[6].Width, Cache.Colors[GameColor.Global] * transparency);
            }
            else
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 189, y + 217), Cache.Interface[sprite].Frames[5].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Global", FontType.DialogsSmallSize8, new Vector2(x + 189, y + 221), Cache.Interface[sprite].Frames[5].Width, Color.DarkGray * transparency);
            }
            /* TOWN CHAT FILTER */
            if (Cache.GameSettings.ChatFilters[GameColor.Town])
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 237, y + 217), Cache.Interface[sprite].Frames[6].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Town", FontType.DialogsSmallSize8, new Vector2(x + 237, y + 221), Cache.Interface[sprite].Frames[6].Width, Cache.Colors[GameColor.Town] * transparency);
            }
            else
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 237, y + 217), Cache.Interface[sprite].Frames[5].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Town", FontType.DialogsSmallSize8, new Vector2(x + 237, y + 221), Cache.Interface[sprite].Frames[5].Width, Color.DarkGray * transparency);
            }
            /* PARTY CHAT FILTER */
            if (Cache.GameSettings.ChatFilters[GameColor.Party])
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 285, y + 217), Cache.Interface[sprite].Frames[6].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Party", FontType.DialogsSmallSize8, new Vector2(x + 285, y + 221), Cache.Interface[sprite].Frames[6].Width, Cache.Colors[GameColor.Party] * transparency);
            }
            else
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 285, y + 217), Cache.Interface[sprite].Frames[5].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Party", FontType.DialogsSmallSize8, new Vector2(x + 285, y + 221), Cache.Interface[sprite].Frames[5].Width, Color.DarkGray * transparency);
            }
            /* GUILD CHAT FILTER */
            if (Cache.GameSettings.ChatFilters[GameColor.Guild])
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 333, y + 217), Cache.Interface[sprite].Frames[6].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Guild", FontType.DialogsSmallSize8, new Vector2(x + 333, y + 221), Cache.Interface[sprite].Frames[6].Width, Cache.Colors[GameColor.Guild] * transparency);
            }
            else
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 333, y + 217), Cache.Interface[sprite].Frames[5].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Guild", FontType.DialogsSmallSize8, new Vector2(x + 333, y + 221), Cache.Interface[sprite].Frames[5].Width, Color.DarkGray * transparency);
            }

            // go through each message and display based on scroll position and chat filters
            int index = 0;
            for (int i = messages.Count-1; i > 0; i--)
            {
                if (index >= 14) break;

                if (messages.Count > i - scrollPosition && (i - scrollPosition > 0))
                {
                    ChatMessage message = messages[i - scrollPosition];

                    // check chat filter is enabled for this chat type
                    if (!Cache.GameSettings.ChatFilters.ContainsKey(message.Color) || Cache.GameSettings.ChatFilters[message.Color])
                    {
                        string text = string.Format("{0} [{1}] : {2}", message.OwnerName, message.MessageTime.ToString("HH:mm"), message.Message);
                        spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], text, new Vector2(x + 25 + 1, (y + 201 - index * 13) + 1), Color.Black);
                        spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], text, new Vector2(x + 25, y + 201 - index * 13), Cache.Colors[message.Color]);
                        index++; // increment index
                    }
                }
            }

            /* TITLE */
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(x + 163, y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Chat", FontType.DisplayNameSize13Spacing1, new Vector2(x + 173, y - 9), 140, Cache.Colors[GameColor.Orange]);

                  
            //figure out what this is used for
            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
                return true;
            else return false;                                                         
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int x = config.X;
            int y = config.Y;
            //Player player = ((MainGame)Cache.DefaultState).Player;
            
            if ((x + 93 < mouseX) && (x + 140 > mouseX) && (y + 217 < mouseY) && (y + 235 > mouseY))       
                Cache.GameSettings.ChatFilters[GameColor.Local] = !Cache.GameSettings.ChatFilters[GameColor.Local];
            if ((x + 141 < mouseX) && (x + 188 > mouseX) && (y + 217 < mouseY) && (y + 235 > mouseY))  
                Cache.GameSettings.ChatFilters[GameColor.Whisper] = !Cache.GameSettings.ChatFilters[GameColor.Whisper];
            if ((x + 189 < mouseX) && (x + 236 > mouseX) && (y + 217 < mouseY) && (y + 235 > mouseY))  
                Cache.GameSettings.ChatFilters[GameColor.Global] = !Cache.GameSettings.ChatFilters[GameColor.Global];   
            if ((x + 237 < mouseX) && (x + 284 > mouseX) && (y + 217 < mouseY) && (y + 235 > mouseY))       
                Cache.GameSettings.ChatFilters[GameColor.Town] = !Cache.GameSettings.ChatFilters[GameColor.Town];      
            if ((x + 285 < mouseX) && (x + 332 > mouseX) && (y + 217 < mouseY) && (y + 235 > mouseY))    
                Cache.GameSettings.ChatFilters[GameColor.Party] = !Cache.GameSettings.ChatFilters[GameColor.Party];      
            if ((x + 333 < mouseX) && (x + 380 > mouseX) && (y + 217 < mouseY) && (y + 235 > mouseY))         
                Cache.GameSettings.ChatFilters[GameColor.Guild] = !Cache.GameSettings.ChatFilters[GameColor.Guild];

            // save changes to settings (chat filters)            
            ((MainGame)Cache.DefaultState).SaveGameSettings();
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {

        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {                                
            List<ChatMessage> messages = ((MainGame)Cache.DefaultState).ChatMessages;    
            if (messages.Count > 14)
            {  
                if (scrollPosition + direction < 0) scrollPosition = 0;
                else if (scrollPosition + direction > Math.Min(Globals.MaximumChatHistory - 14, messages.Count - 14))
                    scrollPosition = Math.Min(Globals.MaximumChatHistory - 14, messages.Count - 14);
                else scrollPosition += direction;  
            }
        }

        public void OffsetLocation(int x, int y)
        {      
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {
                       
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {                   
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
                      
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }  
    }
}
         