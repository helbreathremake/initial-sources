﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Text;

//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Input;

//using HelbreathWorld.Common.Assets;
//using HelbreathWorld.Game.Assets.State;

//namespace HelbreathWorld.Game.Assets.UI
//{
//    class UpgradeV1DialogBox : IGameDialogBox
//    {
//        int selectedItemIndex = -1;
//        int clickedItemIndex = -1;
//        int highlightedItemIndex = -1;
//        int selectedItemIndexOffsetX; //Dragging items
//        int selectedItemIndexOffsetY; //Dragging items
              
//        GameDialogBoxConfiguration config;

//        //Base graphic info
//        int sprite = (int)SpriteId.Interface2; //Main texture
//        int spriteFrame = 0; //rectangle in texutre
//        AnimationFrame frame;

//        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
//        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
//        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
//        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
//        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
//        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
//        public GameDialogBoxType Type { get { return GameDialogBoxType.UpgradeV1; } }

//        public UpgradeV1DialogBox()
//        {    
//            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
//            frame = Cache.Interface[sprite].Frames[spriteFrame];
//        }


//        public UpgradeV1DialogBox(GameDialogBoxConfiguration config)
//        {    
//            this.config = config;
//            frame = Cache.Interface[sprite].Frames[spriteFrame];
//        }


//        public bool DrawItemPopup(SpriteBatch spriteBatch, GameTime gameTime)
//        {       
//            if (config.Hidden)
//                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
//                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

//            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
//            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
//            int x = config.X;
//            int y = config.Y;
//            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
//            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;
       
//            spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture

//            //draw extra 
//            //DIALOG TOP TEXT
//            spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceText].Texture, new Vector2(x, y), Cache.Interface[(int)SpriteId.InterfaceText].Frames[5].GetRectangle(), Color.White * transparency);
            
//            //INFO TEXT
//            string upgradeText1 = "LeftHeld item intended for upgrade";
//            string upgradeText2 = "from the inventory. Then";
//            string upgradeText3 = "press 'Upgrade' button";
            
//            //spriteBatch.DrawString(Cache.GameState.Display.Fonts[FontType.SpellBook], upgradeText1, new Vector2((int)(x + (width / 2) - (Cache.GameState.Display.Fonts[FontType.SpellBook].MeasureString(upgradeText1).X / 2)), (int)(y + 35)), Color.Black, 0.0f, new Vector2(0, 0), 0.7f, SpriteEffects.None, 0);
//            //spriteBatch.DrawString(Cache.GameState.Display.Fonts[FontType.SpellBook], upgradeText2, new Vector2((int)(x + (width / 2) - (Cache.GameState.Display.Fonts[FontType.SpellBook].MeasureString(upgradeText2).X / 2)), (int)(y + 46)), Color.Black, 0.0f, new Vector2(0, 0), 0.7f, SpriteEffects.None, 0);
//            //spriteBatch.DrawString(Cache.GameState.Display.Fonts[FontType.SpellBook], upgradeText3, new Vector2((int)(x + (width / 2) - (Cache.GameState.Display.Fonts[FontType.SpellBook].MeasureString(upgradeText3).X / 2)), (int)(y + 57)), Color.Black, 0.0f, new Vector2(0, 0), 0.7f, SpriteEffects.None, 0);
            
            
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], upgradeText1, new Vector2((int)(x + (frame.Width / 2) - (Cache.DefaultState.Display.Fonts[FontType.Dialog].MeasureString(upgradeText1).X / 2)), (int)(y + 35)), Color.Black);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], upgradeText2, new Vector2((int)(x + (frame.Width / 2) - (Cache.DefaultState.Display.Fonts[FontType.Dialog].MeasureString(upgradeText2).X / 2)), (int)(y + 46)), Color.Black);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], upgradeText3, new Vector2((int)(x + (frame.Width / 2) - (Cache.DefaultState.Display.Fonts[FontType.Dialog].MeasureString(upgradeText3).X / 2)), (int)(y + 57)), Color.Black);
            
//            //ITEM UPGRADE BOX
//            spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.Interface3].Texture, new Vector2(x + 89, y + 125), Cache.Interface[(int)SpriteId.Interface3].Frames[3].GetRectangle(), Color.White * transparency);
            
//            //UPGRADE BUTTON
//            spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 35, y + 280), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[46].GetRectangle(), Color.White * transparency);
            
//            //CANCEL BUTTON
//            spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 150, y + 280), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[16].GetRectangle(), Color.White * transparency);
            
//            //HIGHLIGHT UPGRADE & CANCEL BUTTON
//            if ((mouseY > y + 280) && (mouseY < y + 301))
//            {
//                if ((mouseX > x + 35) && (mouseX < x + 108))
//                    spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 35, y + 280), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[47].GetRectangle(), Color.White * Cache.TransparencyFaders.GlowFrame * transparency);
//                if ((mouseX > x + 150) && (mouseX < x + 220))
//                    spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 150, y + 280), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[17].GetRectangle(), Color.White * Cache.TransparencyFaders.GlowFrame * transparency);
//            }

                  
//            //figure out what this is used for
//            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
//                return true;
//            else return false;                                                         
//        }

//        public void Update(GameTime gameTime)
//        {

//        }

//        public void LeftClick()
//        {
//            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
//            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
//            int x = config.X;
//            int y = config.Y;
                                   
//            if ((mouseY > y + 280) && (mouseY < y + 301))
//            {             
//                //UPGRADE      
//                if ((mouseX > x + 35) && (mouseX < x + 108)) { }
        
//                //CANCEL         
//                if ((mouseX > x + 150) && (mouseX < x + 220)) { }

//            }
//        }

//        public bool LeftHeld()
//        {
//            return false;
//        }

//        public void Scroll(int direction)
//        {
//            if (config.Page - direction > config.MaxPages) config.Page = 1;
//            else if (config.Page - direction < 1) config.Page = config.MaxPages;
//            else config.Page -= direction;
//        }

//        public void OffsetLocation(int x, int y)
//        {      
//            config.X += x;
//            config.Y += y;
//        }

//        public void Show()
//        {                     
//            if (!config.AlwaysVisible && Cache.DefaultState != null)
//                ((IGameState)Cache.DefaultState).BringToFront(Type);
//            highlightedItemIndex = -1;
//            config.Hidden = false;
//        }

//        public void Hide()
//        {                   
//            if (!config.AlwaysVisible && Cache.DefaultState != null)
//            {
//                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
//                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
//            }
//            config.Hidden = true;
//        }

//        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
//        {
                      
//            if (config.Visible && state == config.State) Hide();
//            else
//            {
//                config.State = state;
//                Show();
//            }
//        }

//        public void SetData(byte[] data)
//        {

//        }
//    }
//}
