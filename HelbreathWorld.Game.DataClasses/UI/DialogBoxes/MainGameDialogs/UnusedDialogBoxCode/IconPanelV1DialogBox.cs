﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Input;

//using HelbreathWorld.Common;
//using HelbreathWorld.Common.Assets;
//using HelbreathWorld.Game.Assets.State;

//namespace HelbreathWorld.Game.Assets.UI
//{
//    public class IconPanelV1DialogBox : IGameDialogBox
//    {      
//        int selectedItemIndex = -1;
//        int clickedItemIndex = -1;
//        int highlightedItemIndex = -1;
//        int selectedItemIndexOffsetX; //Dragging items
//        int selectedItemIndexOffsetY; //Dragging items
              
//        GameDialogBoxConfiguration config;

//        //Base graphic info
//        int sprite = (int)SpriteId.InterfaceIconPanel; //Main texture
//        int spriteFrame = 14; //rectangle in texutre
//        int spriteFrameHover = -1; //frame to display when hovering mouse over
//        int width = -1; //frame width
//        int height = -1; //frame height 

//        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
//        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
//        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
//        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
//        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
//        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
//        public GameDialogBoxType Type { get { return GameDialogBoxType.IconPanelV1; } }

//        public IconPanelV1DialogBox()
//        {    
//            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);   
//            //Assign height and width if has sprite
//            if (sprite != -1 && spriteFrame != -1)
//            {
//                width = Cache.Interface[sprite].Frames[spriteFrame].Width;
//                height = Cache.Interface[sprite].Frames[spriteFrame].Height;
//            }
//        }

//        public IconPanelV1DialogBox(GameDialogBoxConfiguration config)
//        {    
//            this.config = config;
//            //Assign height and width if has sprite
//            if (sprite != -1 && spriteFrame != -1)
//            {
//                width = Cache.Interface[sprite].Frames[spriteFrame].Width;
//                height = Cache.Interface[sprite].Frames[spriteFrame].Height;
//            }
//        }

//        public bool DrawItemPopup(SpriteBatch spriteBatch, GameTime gameTime)
//        {       
//            if (config.Hidden)
//                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
//                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

//            Player player = ((MainGame)Cache.DefaultState).Player;
//            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
//            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
//            int x = config.X;
//            int y = config.Y;
//            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
//            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;
       
//            //Draws base frame if not null
//            AnimationFrame frame; // holds frame in case we need to check bounds later
//            if (sprite != -1 && spriteFrame != -1) // some dialog boxes dont have a box sprite such as minimap
//            {
//                if (spriteFrameHover != -1 &&
//                    (mouseX > x) && (mouseX < x + width) && (mouseY > y) && (mouseY <= y + height)) //Hover
//                    frame = Cache.Interface[sprite].Frames[spriteFrameHover];
//                else frame = Cache.Interface[sprite].Frames[spriteFrame]; //Normal 
//                spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture
//            }
//            else frame = null;

//            //draw extra 
//             // crit box highlight
//            if ((x + 362 < mouseX) && (x + 404 > mouseX) && (y + 7 < mouseY) && (y + 48 > mouseY))
//                spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 363, y + 7), Cache.Interface[sprite].Frames[16].GetRectangle(), Color.White);

//            // attack/safe mode
//            if (player.IsCombatMode)
//            {
//                if (player.SafeMode)
//                    spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 368, y + 13), Cache.Interface[sprite].Frames[4].GetRectangle(), Color.White);
//                else spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 368, y + 13), Cache.Interface[sprite].Frames[5].GetRectangle(), Color.White);
//            }

//            if (player.CriticalMode && player.Criticals > 0)
//            {
//                spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 367, y + 12), Cache.Interface[sprite].Frames[3].GetRectangle(), Color.White);
//                spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 367, y + 12), Cache.Interface[sprite].Frames[3].GetRectangle(), Color.DarkMagenta * (Cache.TransparencyFaders.GlowFrame - 0.6f));
//            }

//            // crits
//            if (player.Criticals > 0)
//            {
//                string crits = player.Criticals.ToString().Replace("1", "l");
//                spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.DamageMedium], crits, new Vector2((int)(x + 393 - (Cache.DefaultState.Display.Fonts[FontType.DamageMedium].MeasureString(crits).X / 2)), (int)(y + 28)), Color.Black);
//                if (player.CriticalMode && player.Criticals > 0)
//                {

//                    spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.DamageMedium], crits, new Vector2((int)(x + 392 - (Cache.DefaultState.Display.Fonts[FontType.DamageMedium].MeasureString(crits).X / 2)), (int)(y + 27)), Color.Yellow);
//                }
//                else
//                {
//                    spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.DamageMedium], crits, new Vector2((int)(x + 392 - (Cache.DefaultState.Display.Fonts[FontType.DamageMedium].MeasureString(crits).X / 2)), (int)(y + 27)), Color.White);
//                }
//            }


//            // dialogbox icons
//            if ((mouseY > y + 9) && (mouseY < y + 51))
//            {
//                if ((mouseX > x + 410) && (mouseX < x + 447)) // CharacterV1 screen
//                    spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 412, y + 7), Cache.Interface[sprite].Frames[6].GetRectangle(), Color.White);
//                if ((mouseX > x + 447) && (mouseX < x + 484)) // InventoryV1
//                    spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 449, y + 7), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White);
//                if ((mouseX > x + 484) && (mouseX < x + 521)) //SpellBook
//                    spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 486, y + 7), Cache.Interface[sprite].Frames[8].GetRectangle(), Color.White);
//                if ((mouseX > x + 521) && (mouseX < x + 558)) //Skill
//                    spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 523, y + 7), Cache.Interface[sprite].Frames[9].GetRectangle(), Color.White);
//                if ((mouseX > x + 558) && (mouseX < x + 595)) //Chat
//                    spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 560, y + 7), Cache.Interface[sprite].Frames[10].GetRectangle(), Color.White);
//                if ((mouseX > x + 595) && (mouseX < x + 631)) //System
//                    spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 597, y + 7), Cache.Interface[sprite].Frames[11].GetRectangle(), Color.White);
//            }

//            // guage panel
//            if (player.MaxHP > 0)
//            {
//                int hpWidth = (int)MathHelper.Clamp(101 - (player.HP * 101) / player.MaxHP, 0, 101);
//                spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 23, y + 10), Cache.Interface[sprite].Frames[12].GetRectangle(hpWidth), Color.White);
//                if (player.HP < player.MaxHP / 4) { spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 23 + (hpWidth), y + 10), Cache.Interface[sprite].Frames[17].GetRectangle(103 - hpWidth), Color.Black * Cache.TransparencyFaders.BlinkFrame); }
//                if (player.TickHP) { spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 23, y + 10), Cache.Interface[sprite].Frames[17].GetRectangle(hpWidth), Color.White * Cache.TransparencyFaders.PulseFrame); }
                
//                if (player.IsPoisoned)
//                {
//                    spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.MenuItem], player.HP.ToString(), new Vector2((int)(x + 90 + 1), (int)(y + 10 + 1)), Color.Black * 0.7f);
//                    spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.MenuItem], player.HP.ToString(), new Vector2((int)(x + 90), (int)(y + 10)), SpriteHelper.GetItemColour(ItemEffectType.Attack, 4) * 0.7f);
//                    spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.MenuItem], "Poisoned", new Vector2((int)(x + 40 + 1), (int)(y + 10 + 1)), Color.Black * 0.7f);
//                    spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.MenuItem], "Poisoned", new Vector2((int)(x + 40), (int)(y + 10)), SpriteHelper.GetItemColour(ItemEffectType.Attack, 4) * 0.7f);
//                }
//                else
//                {
//                    spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.MenuItem], player.HP.ToString(), new Vector2((int)(x + 90 + 1), (int)(y + 10 + 1)), Color.Black * 0.7f);
//                    spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.MenuItem], player.HP.ToString(), new Vector2((int)(x + 90), (int)(y + 10)), Color.White * 0.7f);
//                }
//            }
//            if (player.MaxMP > 0)
//            {
//                int mpWidth = (int)MathHelper.Clamp(101 - (player.MP * 101) / player.MaxMP, 0, 101);
//                spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 23, y + 32), Cache.Interface[sprite].Frames[12].GetRectangle(mpWidth), Color.White);
//                if (player.MP < player.MaxMP / 4) { spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 23 + (mpWidth), y + 32), Cache.Interface[sprite].Frames[18].GetRectangle(102 - mpWidth), Color.Black * Cache.TransparencyFaders.BlinkFrame); }
//                if (player.TickMP) { spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 23, y + 31), Cache.Interface[sprite].Frames[18].GetRectangle(mpWidth), Color.White * Cache.TransparencyFaders.PulseFrame); }
//                spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.MenuItem], player.MP.ToString(), new Vector2((int)(x + 90 + 1), (int)(y + 32 + 1)), Color.Black * 0.7f);
//                spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.MenuItem], player.MP.ToString(), new Vector2((int)(x + 90), (int)(y + 32)), Color.White * 0.7f);
//            }
//            if (player.MaxSP > 0)
//            {
//                int spWidth = (int)MathHelper.Clamp(167 - (player.SP * 167) / player.MaxSP, 0, 167);
//                spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 147, y + 8), Cache.Interface[sprite].Frames[13].GetRectangle(spWidth), Color.White);
//                if (player.SP < player.MaxSP / 4) { spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 147 + spWidth, y + 7), Cache.Interface[sprite].Frames[19].GetRectangle(167 - spWidth), Color.Black * Cache.TransparencyFaders.BlinkFrame); }
//                if (player.TickSP) { spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x + 147, y + 7), Cache.Interface[sprite].Frames[19].GetRectangle(spWidth), Color.White * Cache.TransparencyFaders.PulseFrame); }
//                spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.MenuItem], player.SP.ToString(), new Vector2((int)(x + 230 + 1), (int)(y + 6 + 1)), Color.Black * 0.7f);
//                spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.MenuItem], player.SP.ToString(), new Vector2((int)(x + 230), (int)(y + 6)), Color.White * 0.7f);
//            }

//            if (!string.IsNullOrEmpty(player.MapName))
//            {
//                string map = string.Format("{0} ({1},{2})", player.MapName, player.X, player.Y);
//                //spriteBatch.DrawString(Cache.GameState.Display.Fonts[FontType.General], map, new Vector2((int)(x + 310 + 1 - Cache.GameState.Display.Fonts[FontType.General].MeasureString(map).X), (int)(y + 28 + 1)), Color.Black); //  - (Cache.GameState.Display.Fonts[FontType.General].MeasureString(map).X / 2)
//                //spriteBatch.DrawString(Cache.GameState.Display.Fonts[FontType.General], map, new Vector2((int)(x + 310 - Cache.GameState.Display.Fonts[FontType.General].MeasureString(map).X), (int)(y + 28)), new Color(200, 200, 120)); //(Cache.GameState.Display.Fonts[FontType.General].MeasureString(map).X / 2)
//                spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.General], map, new Vector2((int)(x + 235 + 1 - (Cache.DefaultState.Display.Fonts[FontType.General].MeasureString(map).X / 2)), (int)(y + 28 + 1)), Color.Black);
//                spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.General], map, new Vector2((int)(x + 235 - (Cache.DefaultState.Display.Fonts[FontType.General].MeasureString(map).X / 2)), (int)(y + 28)), new Color(200, 200, 120));
//            }
                  
//            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
//                return true;
//            else return false;                                                         
//        }

//        public void Update(GameTime gameTime)
//        {

//        }

//        public void LeftClick()
//        {
//            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
//            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
//            int x = config.X;
//            int y = config.Y;
//            Player player = ((MainGame)Cache.DefaultState).Player;
//                            // crit box highlight
                           
//            if ((x + 362 < mouseX) && (x + 404 > mouseX) && (y + 7 < mouseY) && (y + 48 > mouseY))          
//                ((MainGame)Cache.DefaultState).ToggleCombatMode();

                           
//            if ((mouseY > y + 9) && (mouseY < y + 51))
//            {
//                if ((mouseX > x + 410) && (mouseX < x + 447)) // CharacterV1 screen
//                {
//                    if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CharacterV1].Config.Visible)
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CharacterV1].Hide();
//                    else
//                    {
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CharacterV1].Show();
//                        ((MainGame)Cache.DefaultState).BringToFront(GameDialogBoxType.CharacterV1);
//                    }
//                }
//                if ((mouseX > x + 447) && (mouseX < x + 484)) // InventoryV1
//                {
//                    if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.InventoryV1].Config.Visible)
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.InventoryV1].Hide();
//                    else
//                    {
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.InventoryV1].Show();
//                        ((MainGame)Cache.DefaultState).BringToFront(GameDialogBoxType.InventoryV1);
//                    }
//                }
//                if ((mouseX > x + 484) && (mouseX < x + 521))
//                {
//                    if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.SpellBook].Hide();
//                    else
//                    {
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.SpellBook].Show();
//                        ((MainGame)Cache.DefaultState).BringToFront(GameDialogBoxType.SpellBook);
//                    }
//                }
//                if ((mouseX > x + 521) && (mouseX < x + 558))
//                {
//                    if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Skill].Config.Visible)
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Skill].Hide();
//                    else
//                    {
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Skill].Show();
//                        ((MainGame)Cache.DefaultState).BringToFront(GameDialogBoxType.Skill);
//                    }
//                }
//                if ((mouseX > x + 558) && (mouseX < x + 595))
//                {
//                    if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Chat].Config.Visible)
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Chat].Hide();
//                    else
//                    {
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Chat].Show();
//                        ((MainGame)Cache.DefaultState).BringToFront(GameDialogBoxType.Chat);
//                    }
//                }
//                if ((mouseX > x + 595) && (mouseX < x + 631))
//                {
//                    if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.System].Config.Visible)
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.System].Hide();
//                    else
//                    {
//                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.System].Show();
//                        ((MainGame)Cache.DefaultState).BringToFront(GameDialogBoxType.System);
//                    }
//                }
//            }
//        }

//        public bool LeftHeld()
//        {
//            return false;
//        }

//        public void Scroll(int direction)
//        {
//            if (config.Page - direction > config.MaxPages) config.Page = 1;
//            else if (config.Page - direction < 1) config.Page = config.MaxPages;
//            else config.Page -= direction;
//        }

//        public void OffsetLocation(int x, int y)
//        {      
//            config.X += x;
//            config.Y += y;
//        }

//        public void Show()
//        {
                       
//            if (!config.AlwaysVisible && Cache.DefaultState != null)
//                ((IGameState)Cache.DefaultState).BringToFront(Type);
//            highlightedItemIndex = -1;
//            config.Hidden = false;
//        }

//        public void Hide()
//        {                   
//            if (!config.AlwaysVisible && Cache.DefaultState != null)
//            {
//                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
//                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
//            }
//            config.Hidden = true;
//        }

//        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
//        {
                      
//            if (config.Visible && state == config.State) Hide();
//            else
//            {
//                config.State = state;
//                Show();
//            }
//        }

//        public void SetData(byte[] data)
//        {

//        }
//    }
//}
