﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.UI
{
    public class WeaponIconPanelDialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items
              
        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2; //Main texture
        AnimationFrame frame = Cache.Interface[(int)SpriteId.DialogsV2].Frames[20]; // dummy for mouse selection

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.WeaponIconPanel; } }

        public WeaponIconPanelDialogBox()
        {    
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);   
        }


        public WeaponIconPanelDialogBox(GameDialogBoxConfiguration config)
        {    
            this.config = config;
        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {       
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            Player player = ((MainGame)Cache.DefaultState).Player;
            int x = config.X;
            int y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
       
            //draw extra                      
            if (player.CriticalMode)
            {
                if (!player.IsCombatMode) // peace mode - crit
                {
                    if (player.SafeMode){ spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), Cache.Interface[sprite].Frames[29].GetRectangle(), Color.White * transparency);}
                    else { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), Cache.Interface[sprite].Frames[27].GetRectangle(), Color.White * transparency);}
                }
                else if (player.SafeMode) // safe mode - crit
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), Cache.Interface[sprite].Frames[22].GetRectangle(), Color.White * transparency);
                else spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), Cache.Interface[sprite].Frames[21].GetRectangle(), Color.White * transparency);
                if (player.Criticals > 0)
                    SpriteHelper.DrawTextWithOutline(spriteBatch, player.Criticals.ToString(), FontType.DamageMediumSize13, new Vector2(x + 22, y + 20), (player.SafeMode ? Color.Yellow : Cache.Colors[GameColor.Orange]), Color.Black);
            }
            else
            {
                if (!player.IsCombatMode) // peace mode
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), Cache.Interface[sprite].Frames[26].GetRectangle(), Color.White * transparency);
                else if (player.SafeMode) // safe mode
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), Cache.Interface[sprite].Frames[23].GetRectangle(), Color.White * transparency);
                else spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), Cache.Interface[sprite].Frames[20].GetRectangle(), Color.White * transparency);
                if (player.Criticals > 0)
                    SpriteHelper.DrawTextWithOutline(spriteBatch, player.Criticals.ToString(), FontType.DamageMediumSize13, new Vector2(x + 22, y + 20), (player.SafeMode ? Color.Yellow : Cache.Colors[GameColor.Orange]), Color.Black);
            }
                  
            if (Utility.IsSelected((int)Cache.DefaultState.Display.Mouse.X, (int)Cache.DefaultState.Display.Mouse.Y, x, y, x + frame.Width - 6, y + frame.Height - 5))
                return true;
            else return false;                                     
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {      
            if (Cache.GameSettings.LockedDialogs) ((MainGame)Cache.DefaultState).ToggleCombatMode(); 
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {

        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {      
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {
                       
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {                   
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
                      
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }             
    }
}
