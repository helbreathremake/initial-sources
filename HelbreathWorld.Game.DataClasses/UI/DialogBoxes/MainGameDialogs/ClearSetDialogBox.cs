﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.UI
{
    public class ClearSetDialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedIndexOffsetX; //Dragging items
        int selectedIndexOffsetY; //Dragging items
              
        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 2; //Main texture
        int spriteFrame = 10; //rectangle in texutre
        AnimationFrame frame;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedIndexOffsetX; } set { selectedIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedIndexOffsetY; } set { selectedIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.ClearSet; } }

        public ClearSetDialogBox()
        {    
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }

        public ClearSetDialogBox(GameDialogBoxConfiguration config)
        {    
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {       
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            Player player = ((MainGame)Cache.DefaultState).Player;
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int x = config.X;
            int y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Clear items from set:", FontType.GeneralSize10, new Vector2(x, y + 30), frame.Width, Cache.Colors[GameColor.Orange]);

            //SET 1
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + 50, y + 62), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[16].GetRectangle(), Color.White * transparency);
            if ((x + 50 < mouseX) && (x + 82 > mouseX) && (y + 62 < mouseY) && (y + 73 > mouseY))
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 1", FontType.DialogsSmallerSize7, new Vector2(x + 51, y + 63), 34, Cache.Colors[GameColor.Friendly]);
            else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 1", FontType.DialogsSmallerSize7, new Vector2(x + 51, y + 63), 34, Color.White);

            //SET 2
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + 90, y + 62), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[16].GetRectangle(), Color.White * transparency);
            if ((x + 90 < mouseX) && (x + 122 > mouseX) && (y + 62 < mouseY) && (y + 73 > mouseY))
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 2", FontType.DialogsSmallerSize7, new Vector2(x + 91, y + 63), 34, Cache.Colors[GameColor.Friendly]);
            else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 2", FontType.DialogsSmallerSize7, new Vector2(x + 91, y + 63), 34, Color.White);

            //SET 3
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + 30, y + 80), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[16].GetRectangle(), Color.White * transparency);
            if ((x + 30 < mouseX) && (x + 62 > mouseX) && (y + 80 < mouseY) && (y + 91 > mouseY))
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 3", FontType.DialogsSmallerSize7, new Vector2(x + 31, y + 81), 34, Cache.Colors[GameColor.Friendly]);
            else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 3", FontType.DialogsSmallerSize7, new Vector2(x + 31, y + 81), 34, Color.White);

            //SET 4
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + 70, y + 80), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[16].GetRectangle(), Color.White * transparency);
            if ((x + 70 < mouseX) && (x + 102 > mouseX) && (y + 80 < mouseY) && (y + 91 > mouseY))
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 4", FontType.DialogsSmallerSize7, new Vector2(x + 71, y + 81), 34, Cache.Colors[GameColor.Friendly]);
            else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 4", FontType.DialogsSmallerSize7, new Vector2(x + 71, y + 81), 34, Color.White);

            //SET 5
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + 110, y + 80), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[16].GetRectangle(), Color.White * transparency);
            if ((x + 110 < mouseX) && (x + 142 > mouseX) && (y + 80 < mouseY) && (y + 91 > mouseY))
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 5", FontType.DialogsSmallerSize7, new Vector2(x + 111, y + 81), 34, Cache.Colors[GameColor.Friendly]);
            else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Set 5", FontType.DialogsSmallerSize7, new Vector2(x + 111, y + 81), 34, Color.White);

            //figure out what this is used for
            if (Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
                return true;
            else return false;                                                         
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int x = config.X;
            int y = config.Y;

            if ((y + 62 < mouseY) && (y + 73 > mouseY))
            {
                //SET 1
                if ((x + 50 < mouseX) && (x + 82 > mouseX)) { ((MainGame)Cache.DefaultState).ResetItemSet(1); Hide(); }
                //SET 2
                if ((x + 90 < mouseX) && (x + 122 > mouseX)) { ((MainGame)Cache.DefaultState).ResetItemSet(2); Hide(); }
            }

            if ((y + 80 < mouseY) && (y + 91 > mouseY))
            {
                //SET 3
                if ((x + 30 < mouseX) && (x + 62 > mouseX)) { ((MainGame)Cache.DefaultState).ResetItemSet(3); Hide(); }
                //SET 4
                if ((x + 70 < mouseX) && (x + 102 > mouseX)) { ((MainGame)Cache.DefaultState).ResetItemSet(4); Hide(); }
                //SET 5
                if ((x + 110 < mouseX) && (x + 142 > mouseX)) { ((MainGame)Cache.DefaultState).ResetItemSet(5); Hide(); }
            }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {

        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {      
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {
                       
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {                   
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
                      
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }  
    }
}


