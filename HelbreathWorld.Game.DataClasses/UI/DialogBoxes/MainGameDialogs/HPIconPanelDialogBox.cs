﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.UI
{
    public class HPIconPanelDialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedIndexOffsetX; //Dragging items
        int selectedIndexOffsetY; //Dragging items
              
        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2; //Main texture
        int spriteFrame = 1; //rectangle in texutre
        AnimationFrame frame;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedIndexOffsetX; } set { selectedIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedIndexOffsetY; } set { selectedIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.HPIconPanel; } }

        public HPIconPanelDialogBox()
        {    
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }

        public HPIconPanelDialogBox(GameDialogBoxConfiguration config)
        {    
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {       
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            Player player = ((MainGame)Cache.DefaultState).Player;
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int x = config.X;
            int y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture
             
            if (player.MaxHP > 0)
            {
                int hpWidth = (int)MathHelper.Clamp(105 - (player.HP * 105) / player.MaxHP, 0, 105);
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 4, y + 3), Cache.Interface[sprite].Frames[3].GetRectangle(106 - hpWidth), Color.Red * transparency);
                if (player.HP < player.MaxHP / 4) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 4, y + 3), Cache.Interface[sprite].Frames[3].GetRectangle(106 - hpWidth), Color.Black * Cache.TransparencyFaders.BlinkFrame); }
                if (player.TickHP) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 4, y + 3), Cache.Interface[sprite].Frames[3].GetRectangle(), Color.Red * Cache.TransparencyFaders.PulseFrame); }
            }

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.Transparent * transparency);

            string hpText = player.HP + "/" + player.MaxHP;
            if (player.IsPoisoned)
            {
                spriteBatch.DrawString(Cache.Fonts[FontType.DialogsSmallSize8], hpText, new Vector2((int)(x + (frame.Width / 2 - Cache.Fonts[FontType.DialogsSmallSize8].MeasureString(hpText).X / 2)), (int)(y + 2)), Color.White);
                spriteBatch.DrawString(Cache.Fonts[FontType.DialogsSmallSize8], "Poisoned", new Vector2((int)(x + 10), (int)(y + 2)), SpriteHelper.GetItemColour(ItemEffectType.Attack, 4) * 0.7f);
            }
            else
            {
                spriteBatch.DrawString(Cache.Fonts[FontType.DialogsSmallSize8], hpText, new Vector2((int)(x + (frame.Width / 2 - Cache.Fonts[FontType.DialogsSmallSize8].MeasureString(hpText).X / 2)), (int)(y + 2)), Color.White);
            }
            
            //figure out what this is used for
            if (Utility.IsSelected(mouseX, mouseY, x + 1, y, x + frame.Width - 5, y + frame.Height - 4))
                return true;
            else return false;                                                         
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {

        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {      
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {
                       
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {                   
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
                      
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }  
    }
}


