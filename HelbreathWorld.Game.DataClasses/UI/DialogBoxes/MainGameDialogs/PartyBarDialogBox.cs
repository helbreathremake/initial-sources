﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.UI
{
    public class PartyBarDialogBox : IGameDialogBox
    {
        // selected items
        private int clickedItemIndex; // clicked item
        private int selectedItemIndex; // mouse over item
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedItemIndex; // chosen item (non volatile)

        private GameDialogBoxConfiguration config;

        int sprite = (int)SpriteId.DialogsV2 + 5;
        int spriteFrame = 19;
        AnimationFrame frame;

        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }

        public GameDialogBoxType Type { get { return GameDialogBoxType.PartyBar; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }

        Player player;

        public PartyBarDialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            this.highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public PartyBarDialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (player.Party == null) return false;
            //if (player.Party == null) return false;
            //if (config.Hidden)
            //    if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
            //    else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int X = config.X;
            int Y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            bool hover; int selectedIndex = -1; highlightedItemIndex = -1;

            switch (config.State)
            {
                case DialogBoxState.Normal:
                    for (int i = 0; i < player.Party.MemberCount; i++)
                        if (player.Party.Members[i] != -1)
                            if (player.Party.Members[i] == player.ObjectId)
                            {
                                int hpWidth = (int)MathHelper.Clamp(63 - (player.HP * 63) / player.MaxHP, 0, 63);
                                int mpWidth = (int)MathHelper.Clamp(63 - (player.MP * 63) / player.MaxMP, 0, 63);
                                if (player.IsPoisoned)
                                     spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 123, Y + (i * 35) + 7), Cache.Interface[sprite].Frames[22].GetRectangle(63 - hpWidth), Color.White * transparency);
                                else spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 123, Y + (i * 35) + 7), Cache.Interface[sprite].Frames[20].GetRectangle(63 - hpWidth), Color.White * transparency);
                                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 123, Y + (i * 35) + 19), Cache.Interface[sprite].Frames[21].GetRectangle(63 - mpWidth), Color.White * transparency);
                                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + (i * 35)), frame.GetRectangle(), Color.White * transparency);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, player.Name, FontType.GeneralSize10, new Vector2(X + 10, Y + 7 + (i * 35)), Cache.Colors[GameColor.Orange]);
                            }
                            else if (player.Party.MemberInfo.ContainsKey(player.Party.Members[i]))
                            {
                                PartyMember member = player.Party.MemberInfo[player.Party.Members[i]];
                                int hpWidth = (int)MathHelper.Clamp(63 - (member.HP * 63) / member.MaxHP, 0, 63);
                                int mpWidth = (int)MathHelper.Clamp(63 - (member.MP * 63) / member.MaxMP, 0, 63);
                                if (member.Poison)
                                     spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 123, Y + (i * 35) + 7), Cache.Interface[sprite].Frames[22].GetRectangle(63 - hpWidth), Color.White * transparency);
                                else spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 123, Y + (i * 35) + 7), Cache.Interface[sprite].Frames[20].GetRectangle(63 - hpWidth), Color.White * transparency);
                                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 123, Y + (i * 35) + 19), Cache.Interface[sprite].Frames[21].GetRectangle(63 - mpWidth), Color.White * transparency);
                                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + (i * 35)), frame.GetRectangle(), Color.White * transparency);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, member.Name, FontType.GeneralSize10, new Vector2(X + 10, Y + 7 + (i * 35)), Cache.Colors[GameColor.Orange]);
                            }
                    break;
            }

            if (Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public void LeftClicked()
        {

        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
               ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Vertical)
        {
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
