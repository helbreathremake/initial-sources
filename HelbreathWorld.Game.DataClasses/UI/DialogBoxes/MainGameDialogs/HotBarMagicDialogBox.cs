﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.UI
{
    public class HotBarMagicDialogBox : IGameDialogBox
    {

        private GameDialogBoxConfiguration config;
         
        // selected items
        private int clickedItemIndex; // clicked item
        private int selectedItemIndex; // mouse over item
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedItemIndex; // chosen item (non volatile)

        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }

        public GameDialogBoxType Type { get { return GameDialogBoxType.HotBarMagic; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }

        int mouseX; 
        int mouseY;
        int X;
        int Y; 
        Player player;
        int sprite = (int)SpriteId.DialogsV2 + 1;
        float transparency;
        AnimationFrame frame;
        bool hover;
        MotionDirection direction;
        public int HotBarConfigIndex = -1;

        public HotBarMagicDialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            player = ((MainGame)Cache.DefaultState).Player;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
        }

        public HotBarMagicDialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            player = ((MainGame)Cache.DefaultState).Player;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            highlightedItemIndex = -1;

            int spellIndex = 0;
            int spellCount = 0;
            for (int i = 0; i < 10; i++)
            {
                spellCount = 0;
                for (int j = 0; j < 10; j++)
                {
                    if (player.MagicLearned[spellIndex] && Cache.MagicConfiguration.ContainsKey(spellIndex))
                    {
                        switch (direction)
                        {
                            case MotionDirection.North:                      
                                hover = Utility.IsSelected(mouseX, mouseY, X + (i * 30), Y - (spellCount * 30), X + (i * 30) + 30, Y - (spellCount * 30) + 30);
                                spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + (i * 30), Y - (spellCount * 30)), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[spellIndex].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[spellIndex].SmallIconSpriteFrame)].GetRectangle(), Color.White);
                                spellCount++;
                                if (hover) highlightedItemIndex = spellIndex;
                                break;
                            case MotionDirection.East:                       
                                hover = Utility.IsSelected(mouseX, mouseY, X + (spellCount * 30), Y + (i * 30), X + (spellCount * 30) + 30, Y + (i * 30) + 30);
                                spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + (spellCount * 30), Y + (i * 30)), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[spellIndex].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[spellIndex].SmallIconSpriteFrame)].GetRectangle(), Color.White);
                                spellCount++;
                                if (hover) highlightedItemIndex = spellIndex;
                                break;
                            case MotionDirection.South:                             
                                hover = Utility.IsSelected(mouseX, mouseY, X + (i * 30), Y + (spellCount * 30), X + (i * 30) + 30, Y + (spellCount * 30) + 30);
                                spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + (i * 30), Y + (spellCount * 30)), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[spellIndex].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[spellIndex].SmallIconSpriteFrame)].GetRectangle(), Color.White);
                                spellCount++;
                                if (hover) highlightedItemIndex = spellIndex;
                                break;
                            case MotionDirection.West:                
                                hover = Utility.IsSelected(mouseX, mouseY, X - (spellCount * 30), Y + (i * 30), X - (spellCount * 30) + 30, Y + (i * 30) + 30);
                                spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X - (spellCount * 30), Y + (i * 30)), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[spellIndex].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[spellIndex].SmallIconSpriteFrame)].GetRectangle(), Color.White);
                                spellCount++;
                                if (hover) highlightedItemIndex = spellIndex;
                                break;
                            default: break;
                        }
                    }
                    spellIndex++;
                }
            
            }

            if (Cache.GameSettings.LockedDialogs) // not locked dialogs
                if (clickedItemIndex == -1) // if no spell dragging
                    if (highlightedItemIndex != -1) // and no spell hovered
                        selectedItemIndex = highlightedItemIndex; // set hover index
                    else selectedItemIndex = -1; // cancel hovered spell when not dragging

            ///* POPUP */
            if (selectedItemIndex != -1)
            {
                ((MainGame)Cache.DefaultState).SetMagicPopup(Type, selectedItemIndex, new Vector2(mouseX + 10, mouseY - 10), 0, 0);
            }

            if (highlightedItemIndex != -1)
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            player = ((MainGame)Cache.DefaultState).Player;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);

            if (Config.Visible && (selectedItemIndex == -1 && (((MainGame)Cache.DefaultState).HighlightedDialogBox != GameDialogBoxType.HotBar) || player.IsCasting))
                Hide();
        }

        public void LeftClicked() { }

        public void LeftHeld() { }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId) { }

        public void LeftDragged()  {}

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
            if (highlightedItemIndex != -1 && HotBarConfigIndex != -1)
            {
                //set hotbar icon at destination
                HotBarIcon icon = new HotBarIcon(highlightedItemIndex);
                icon.Type = HotBarType.Spell;
                Cache.HotBarConfiguration.Add(HotBarConfigIndex, icon);
                ((MainGame)Cache.DefaultState).SaveHotKeyConfiguration();
                Hide();
                //((MainGame)Cache.DefaultState).ClickedDialogBox = Type;
            }
        }

        public void RightClicked()  {  }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void SetLocation(GameDialogBoxConfiguration location)
        {
            config = location;
        }

        public void Show()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = clickedItemIndex = HotBarConfigIndex = selectedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((MainGame)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
            HotBarConfigIndex = clickedItemIndex = highlightedItemIndex = selectedItemIndex = -1;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Vertical)
        {
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                IGameDialogBox box = ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.HotBar];
                switch (state)
                {
                    case DialogBoxState.Vertical:
                        if (box.Config.X < Cache.DefaultState.Display.ResolutionWidth / 2)
                        {
                            config.X = box.Config.X + 40;
                            config.Y = box.Config.Y + 13;
                            direction = MotionDirection.East;
                        }
                        else 
                        {
                            config.X = box.Config.X - 20;
                            config.Y = box.Config.Y + 13;
                            direction = MotionDirection.West;
                        }  
                        break;
                    case DialogBoxState.Horizontal:
                        if (box.Config.Y < Cache.DefaultState.Display.ResolutionHeight / 2)
                        {
                            config.X = box.Config.X + 13;
                            config.Y = box.Config.Y + 40;
                            direction = MotionDirection.South;
                        }
                        else 
                        {
                            config.X = box.Config.X + 13;
                            config.Y = box.Config.Y - 20;
                            direction = MotionDirection.North;
                        }
                        break;
                }
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}