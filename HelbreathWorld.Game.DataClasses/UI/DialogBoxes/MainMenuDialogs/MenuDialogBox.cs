﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common.Assets;
using HelbreathWorld.Game.Assets.State;

namespace HelbreathWorld.Game.Assets.UI
{
    public class MenuDialogBox : IMenuDialogBox
    {
        private object dataObject;
        private MenuDialogBoxType type;
        private int sprite;
        private int spriteFrame;
        private int spriteFrameHover;
        private int width;
        private int height;
        private bool isScrollable;
        private bool moveable;
        private int page;
        private int maxPages;
        private int x;
        private int y;
        private DialogBoxState state;
        private bool hidden;
        private bool alwaysVisible;

        // selected items
        private int clickedItemIndex; // clicked item
        private int selectedItemIndex; // mouse over item
        private Vector2 selectedSlot;
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedItemIndex; // chosen item (non volatile)
        private Vector2 highlightedSlot; // chose item grid coords (non volatile)

        public MenuDialogBox(MenuDialogBoxType type)
        {
            this.type = type;
            this.moveable = true;
            this.clickedItemIndex = -1;
            this.highlightedItemIndex = -1;

            //if (type != GameDialogBoxType.System && type != GameDialogBoxType.SpellBook) { maxPages = 0; }
            //else if (type == GameDialogBoxType.System) { maxPages = 4; page = 1; }
            //else if (type == GameDialogBoxType.SpellBook) { maxPages = 9; }

            //if (sprite != -1 && spriteFrame != -1)
            //{
            //    this.width = Cache.Interface[sprite].Frames[spriteFrame].Width;
            //    this.height = Cache.Interface[sprite].Frames[spriteFrame].Height;
            //}

            //Hide();
        }

        public MenuDialogBox(MenuDialogBoxType type, int sprite, int spriteFrame, int x = -1, int y = -1, int spriteFrameHover = -1)
        {
            this.type = type;
            this.sprite = sprite;
            this.spriteFrame = spriteFrame;
            this.spriteFrameHover = spriteFrameHover;
            this.x = x;
            this.y = y;
            this.moveable = true;
            this.clickedItemIndex = -1;
            this.highlightedItemIndex = -1;

            //Clean this upppp
            //if (Cache.GameState.Display.Resolution != null) { DefaultDialogValues(Cache.GameState.Display.Resolution); }

            //TODO CLEAN THIS UPPP UGGHHH
            //if (type != MenuDialogBoxType.System && type != MenuDialogBoxType.SpellBook) { maxPages = 0; }
            //else if (type == MenuDialogBoxType.System) { maxPages = 4; page = 1; }
            //else if (type == MenuDialogBoxType.SpellBook) { maxPages = 9; }

            if (sprite != -1 && spriteFrame != -1)
            {
                this.width = Cache.Interface[sprite].Frames[spriteFrame].Width;
                this.height = Cache.Interface[sprite].Frames[spriteFrame].Height;
            }

            Hide();
        }

        public void OffsetLocation(int x, int y)
        {
            this.x += x;
            this.y += y;
        }

        public void Update(GameTime gameTime)
        {
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (Hidden)
                if (!AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            bool selected = false;
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            selectedItemIndex = -1;
            selectedSlot = new Vector2(-1, -1);

            switch (Cache.DefaultState.State)
            {               
                case DefaultState.MainMenu:
                    {
                        AnimationFrame frame; // holds frame in case we need to check bounds later
                        if (sprite != -1) // some dialog boxes dont have a box sprite such as minimap
                        {
                            frame = Cache.Interface[sprite].Frames[spriteFrame];
                            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White);

                        }
                        else frame = null;

                        /* DRAW DATA FOR THIS DIALOG BOX */
                        switch (type)
                        {
                            case MenuDialogBoxType.Login:
                                {                                                                 
                                    break;
                                }
                            case MenuDialogBoxType.NewAccount:
                                {

                                    break;
                                }
                            case MenuDialogBoxType.Settings:
                                {
                                    break;
                                }
                        }

                        if (frame == null) return false; // no frame defined, unable to check if mouse within bounds
                        else return true;
                    }
                }
            return true;
        }

        public void Click()
        {
        }

        public void Scroll(int direction)
        {

        }

        public bool Drag()
        {
            return false;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
            if (Visible && state == State) Hide();
            else
            {
                State = state;
                Show();
            }
        }

        public void Show()
        {
            if (!AlwaysVisible && Cache.DefaultState != null)
                ((IMenuState)Cache.DefaultState).BringToFront(Type);
            Hidden = false;
        }

        public void Hide()
        {
            if (!AlwaysVisible && Cache.DefaultState != null)
            {
                ((IMenuState)Cache.DefaultState).DialogBoxDrawOrder.Remove(type); //Couldnt cast object correctlying when logging out of game, wait for full load?
                ((IMenuState)Cache.DefaultState).ClickedDialogBox = MenuDialogBoxType.None;
            }
            Hidden = true;
        }

        public void SetData(byte[] data)
        {

        }

        public int Page { get { return page; } set { page = value; } } //good
        public bool IsScrollable { get { return isScrollable; } set { isScrollable = value; } } //good
        public DialogBoxState State { get { return state; } set { state = value; } } //good
        public bool Hidden { get { return hidden; } set { hidden = value; } } //good
        public bool Visible { get { return ((AlwaysVisible && Cache.GameSettings.LockedDialogs) || !hidden); } } //goood
        public bool AlwaysVisible { get { return alwaysVisible; } set { alwaysVisible = value; } } //good
        public int X { get { return x; } set { x = value; } } //good
        public int Y { get { return y; } set { y = value; } } //good
        public bool Moveable { get { return moveable; } set { moveable = value; } } //good
        public MenuDialogBoxType Type { get { return type; } } //good
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } } //good
        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } } //good
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } } //good
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } } //good
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } } //good


        public int Width { get { return width; } }
        public int Height { get { return height; } }
        public int Sprite { get { return sprite; } }
        public int ScrollPosition;

    }
}
