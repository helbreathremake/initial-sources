﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace HelbreathWorld.Game.Assets
{
    public class AnimationFrame
    {
        private int x;
        private int y;
        private int width;
        private int height;
        private int pivotX;
        private int pivotY;

        public AnimationFrame(int x, int y, int width, int height, int pivotX, int pivotY)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.pivotX = pivotX;
            this.pivotY = pivotY;
        }

        public Rectangle GetRectangle(int maxWidth)
        {
            return new Rectangle(x, y, maxWidth, height);
        }

        public Rectangle GetRectangle()
        {
            return new Rectangle(x, y, width, height);
        }

        public int X { get { return x; } }
        public int Y { get { return y; } }
        public int Width { get { return width; } }
        public int Height { get { return height; } }
        public int PivotX { get { return pivotX; } }
        public int PivotY { get { return pivotY; } }
    }
}
