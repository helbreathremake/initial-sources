﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HelbreathWorld.Game.Assets.State;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Common.Assets.Objects;
using HelbreathWorld.Game.Assets.UI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace HelbreathWorld.Game.Assets
{
    public class TransparencyFaders
    {
        private float glowFrame = 0.00f;
        private bool glowDirection = true;
        private float blinkFrame = 0.00f;
        private bool blinkDirection = true;
        private float pulseFrame = 0.00f;
        private bool pulseDirection = true;

        public TransparencyFaders()
        {

        }

        public void Update()
        {
            UpdateGlow();
            UpdateBlink();
            UpdatePulse();
        }

        private void UpdateGlow()
        {
            if (glowDirection)
            {
                if (glowFrame >= 1.0f)
                {
                    glowFrame = 1.0f;
                    glowDirection = false;
                }
                else glowFrame = glowFrame + 0.025f;
            }
            else
            {
                if (glowFrame <= 0.2f)
                {
                    glowFrame = 0.2f;
                    glowDirection = true;
                }
                else glowFrame = glowFrame - 0.025f;
            }
        }

        private void UpdateBlink()
        {
            if (blinkDirection)
            {
                if (blinkFrame >= 0.4f)
                {
                    blinkFrame = 0.4f;
                    blinkDirection = false;
                }
                else blinkFrame = blinkFrame + 0.05f;
            }
            else
            {
                if (blinkFrame <= 0.001f)
                {
                    blinkFrame = 0.001f;
                    blinkDirection = true;
                }
                else blinkFrame = blinkFrame - 0.04f;
            }
        }

        private void UpdatePulse()
        {
            if (pulseDirection)
            {
                if (pulseFrame >= 0.5f)
                {
                    pulseFrame = 0.5f;
                    pulseDirection = false;
                }
                else pulseFrame = pulseFrame + 0.03f;
            }
            else
            {
                if (pulseFrame <= 0.001f)
                {
                    pulseFrame = 0.001f;
                    pulseDirection = true;
                }
                else pulseFrame = pulseFrame - 0.01f;
            }
        }

        public float GlowFrame { get { return glowFrame; } }
        public float BlinkFrame { get { return blinkFrame; } }
        public float PulseFrame { get { return pulseFrame; } }
    }
}
