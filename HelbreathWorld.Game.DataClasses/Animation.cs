﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using HelbreathWorld.Game;


namespace HelbreathWorld.Game.Assets
{
    public enum AnimationLoadState
    {
        NotLoaded,
        Loading,
        Loaded
    }

    public class Animation
    {
        private List<Texture2D> textures;
        private List<AnimationFrame> frames;
        private Color[] mask;
        private AnimationLoadState loadState;

        public Animation(Texture2D texture)
        {
            this.textures = new List<Texture2D>();

            this.textures.Add(texture);
            this.frames = new List<AnimationFrame>();

            this.mask = new Color[1];
            this.textures[0].GetData<Color>(0, new Rectangle(0,0,1,1) ,mask,0,1);
        }

        public Animation(byte[] data)
        {

        }

        public Color Mask { get { return mask[0]; } }
        public Texture2D Texture { get { return textures[0]; } }
        public List<AnimationFrame> Frames { get { return frames; } set { frames = value; } }
        public AnimationLoadState LoadState { get { return loadState; } set { loadState = value; } }
    }
}