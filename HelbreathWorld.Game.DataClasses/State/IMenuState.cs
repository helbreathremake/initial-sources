﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Game;
using HelbreathWorld.Game.Assets.UI;
using HelbreathWorld.Game.Assets.Effects;

namespace HelbreathWorld.Game.Assets.State
{
    public interface IMenuState
    {
        Dictionary<MenuDialogBoxType, IMenuDialogBox> DialogBoxes { get; }
        LinkedList<MenuDialogBoxType> DialogBoxDrawOrder { get; set; }
        void BringToFront(MenuDialogBoxType type);
        void HideMessageBox(MenuDialogBoxType type);
        MenuDialogBoxType ClickedDialogBox { get; set; }
        void ShowMessageBox(MenuMessageBox box);
    }
}
