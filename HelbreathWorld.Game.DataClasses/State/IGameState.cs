﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Game;
using HelbreathWorld.Game.Assets.UI;
using HelbreathWorld.Game.Assets.Effects;

namespace HelbreathWorld.Game.Assets.State
{
    public interface IGameState
    {
        Dictionary<GameDialogBoxType, IGameDialogBox> DialogBoxes { get; }
        LinkedList<GameDialogBoxType> DialogBoxDrawOrder { get; set; }
        void BringToFront(GameDialogBoxType type);
        void HideMessageBox(GameDialogBoxType type);
        GameDialogBoxType ClickedDialogBox { get; set; }
        void ShowMessageBox(GameMessageBox box);
    }
}
