﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common;
using HelbreathWorld.Game.Assets.UI;
using HelbreathWorld.Game.Assets.Effects;

namespace HelbreathWorld.Game.Assets.State
{
    public class MainMenu : IDefaultState, IMenuState
    {
        public event EventHandler Exit;

        private bool isComplete;
        private bool back;
        private ClientInfo loginServer;
        private string ipAddress;
        private int port;

        private Queue<Command> processQueue;
        public readonly object processQueueLock = new object();

        private GameDisplay display;
        private List<MenuItem> loginItems;
        private List<MenuItem> accountItems;
        private List<MenuItem> settingsNormal;
        private List<MenuItem> settingsKeyboard;
        private List<MenuItem> settingsAdvanced;
        private List<MenuItem> mainMenuItems;
        private List<TextBox> loginTextBoxes;
        private List<TextBox> accountTextBoxes;
        private List<Rectangle> textBoxes;
        private string errorMessage;
        private int errorMessageTime = 5000;
        private bool errorMessageTimer;
        private Resolution currentResolution;
        private bool resolutionChange = false;
        private int settingsScrollcounter = 0;
        private int keyboardSettingsScrollCounter = 0;
        private int advancedSettingsScrollCounter = 0;
        private int advancedSettingsMaxScroll;
        private float advancedSettingsScrollPixels;
        private int advancedSettingsMaxScrollCount;
        private int advancedSettingsScrollFactor;
        private bool settingsSaved = false;

        //BACKGROUND
        private LinkedList<BackGroundImage> backgroundImages;
        private BackGroundImage[] backgroundcollection;
        private BackGroundImage background;
        private BackGroundImage backgroundblackbar;
        private BackGroundImage legs;
        private BackGroundImage weapon;
        private BackGroundImage tree;
        private BackGroundImage grass;
        private BackGroundImage grass2;
        private BackGroundImage grass3;
        private BackGroundImage smoke1;
        private BackGroundImage smoke2;
        private BackGroundImage smoke3;
        private BackGroundImage smoke4;
        private BackGroundImage smoke5;
        private BackGroundImage smoke6;
        private BackGroundImage smoke7;
        private BackGroundImage smoke8;
        private BackGroundImage logo;
        private BackGroundImage logoglow;
        private BackGroundImage warriorhead;
        private BackGroundImage warriorbody;

        private Texture2D corner;
        private Texture2D textbox;
        private Texture2D dialogFader;
        private Texture2D dialogBorder;

        private bool fadeDirection;
        private bool fadeStage;
        private int fadeDelay = 10;
        private int backgroundDelay = 16;

        //effects
        private float glowFrame;
        private float blinkFrame;
        private float pulseFrame;

        private Texture2D backgroundTexture;
        private TextBox usernameBox;
        private TextBox passwordBox;
        private TextBox accountBox;
        private TextBox newPasswordBox;
        private TextBox reTypePasswordBox;
        private TextBox emailBox;
        private TextBox questionBox;
        private TextBox answerBox;
        private string username;
        private string password;
        private string account;
        private string newPassword;
        private string reTypePassword;
        private string email;
        private string question;
        private string answer;

        //DIALOG BOXES
        private Dictionary<MenuDialogBoxType, IMenuDialogBox> dialogBoxes;
        private LinkedList<MenuDialogBoxType> dialogBoxDrawOrder;
        private MenuDialogBoxType clickedDialogBox;
        private MenuDialogBoxType highlightedDialogBox;
        private MenuDialogBoxType topdialogBox;
        private int selectedObjectId;
        private int selectedObjectX;
        private int selectedObjectY;

        private List<HotKeyMenuItem> hotKeyMenuItems;
        private bool hotKeyConfigChanged = false;
        private bool hotKeyConfigReload = false;

        //Advanced Settings
        private List<AdvancedSettingsMenuItem> advancedSettingsMenuItems;
        private int advancedSettingsTotalHeight;

        //RenderTargets 
        private RenderTarget2D mainRenderTarget;
        private RenderTarget2D advancedSettingsRenderTarget;

        private List<Player> characters;

        private Timer spriteQueueCallback;

        public MainMenu(string ipAddress, int port)
        {
            processQueue = new Queue<Command>();
            loginItems = new List<MenuItem>();
            accountItems = new List<MenuItem>();
            settingsNormal = new List<MenuItem>();
            settingsAdvanced = new List<MenuItem>();
            settingsKeyboard = new List<MenuItem>();
            mainMenuItems = new List<MenuItem>();
            characters = new List<Player>();
            loginTextBoxes = new List<TextBox>();
            accountTextBoxes = new List<TextBox>();
            hotKeyMenuItems = new List<HotKeyMenuItem>();
            textBoxes = new List<Rectangle>();
            advancedSettingsMenuItems = new List<AdvancedSettingsMenuItem>();

            this.ipAddress = ipAddress;
            this.port = port;
        }

        public void Init(GameWindow window)
        {
            Display.Mouse.State = GameMouseState.Normal;

            advancedSettingsRenderTarget = new RenderTarget2D(display.Device, 281, 176);
            backgroundTexture = new Texture2D(Display.Device, 1, 1);
            backgroundTexture.SetData(new[] { Color.White });

            Viewport viewport = GraphicsDevice.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            int height = viewport.Height;
            int width = viewport.Width;
            currentResolution = display.Resolution;
            
            this.LoadHotKeyConfiguration();
            this.LoadItemConfiguration();

            //MAINMENU
            MenuItem mainTitleLabel = new MenuItem(MenuItemType.Title, "Helbreath Champions");
            mainTitleLabel.SetPosition(new Vector2((int)(((viewportSize.X - Cache.Fonts[FontType.MagicMedieval40].MeasureString("Helbreath Champions").X) / 2)), 65));
            mainMenuItems.Add(mainTitleLabel);

            MenuItem mainLoginLabel = new MenuItem(MenuItemType.Button, "Login");
            mainLoginLabel.SetPosition(new Vector2((int)((width / 2) - 140), (int)(viewport.Height - 33)));
            mainMenuItems.Add(mainLoginLabel);

            MenuItem mainAccountLabel = new MenuItem(MenuItemType.Button, "Account");
            mainAccountLabel.SetPosition(new Vector2((int)((width / 2) - 69), (int)(viewport.Height - 33)));
            mainMenuItems.Add(mainAccountLabel);

            MenuItem mainSettingsLabel = new MenuItem(MenuItemType.Button, "Options");
            mainSettingsLabel.SetPosition(new Vector2((int)((width / 2)) + 8, (int)(viewport.Height - 33)));
            mainMenuItems.Add(mainSettingsLabel);

            MenuItem mainExitLabel = new MenuItem(MenuItemType.Button, "Exit");
            mainExitLabel.SetPosition(new Vector2((int)((width / 2) + 100), (int)(viewport.Height - 33)));
            mainMenuItems.Add(mainExitLabel);

            //LOGIN DIALOG
            MenuItem usernameLabel = new MenuItem(MenuItemType.Text, "Account");
            usernameLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Account").X / 2) - 6), (int)((viewport.Height / 2) - 51)));
            loginItems.Add(usernameLabel);

            usernameBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.DialogsSmallSize8]);
            usernameBox.Width = 115;
            usernameBox.Height = 20;
            usernameBox.X = (viewport.Width / 2) - (usernameBox.Width/2) - 6;
            usernameBox.Y = viewport.Height / 2 - 30;
            usernameBox.MaxCharacters = 10;
            loginTextBoxes.Add(usernameBox);

            MenuItem passwordLabel = new MenuItem(MenuItemType.Text, "Password");
            passwordLabel.SetPosition(new Vector2((int)((viewport.Width / 2) -  (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Password").X / 2) - 6), (int)((viewport.Height / 2) + 1)));
            loginItems.Add(passwordLabel);

            passwordBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            passwordBox.PasswordBox = true;
            passwordBox.Width = 115;
            passwordBox.Height = 20;
            passwordBox.X = (viewport.Width / 2) - (passwordBox.Width / 2) - 6;
            passwordBox.Y = viewport.Height / 2 + 22;
            passwordBox.MaxCharacters = 10;
            loginTextBoxes.Add(passwordBox);

            MenuItem loginLabel = new MenuItem(MenuItemType.Button, "Connect");
            loginLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Connect").X / 2) - 6), (int)((viewport.Height / 2) + 70)));
            loginItems.Add(loginLabel);

            MenuItem loginErrorLabel = new MenuItem(MenuItemType.Text, "ERROR:");
            loginErrorLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - 6), (int)((viewport.Height / 2) + 130)));
            loginItems.Add(loginErrorLabel);

            //NEWACCOUNT DIALOG
            MenuItem accountTitleLabel = new MenuItem(MenuItemType.Title, "Create New Account");
            accountTitleLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval18].MeasureString("Create New Account").X / 2) + 4), (int)((viewport.Height / 2) - 102)));
            accountItems.Add(accountTitleLabel);

            MenuItem accountLabel = new MenuItem(MenuItemType.Text, "Account");
            accountLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 130 - 6), (int)((viewport.Height / 2) - 60)));
            accountItems.Add(accountLabel);

            accountBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            accountBox.Width = 115;
            accountBox.Height = 20;
            accountBox.X = (viewport.Width / 2) - (passwordBox.Width / 2) + 80 - 6;
            accountBox.Y = viewport.Height / 2 - 60;
            accountBox.MaxCharacters = 10;
            accountTextBoxes.Add(accountBox);

            MenuItem newPasswordLabel = new MenuItem(MenuItemType.Text, "Password");
            newPasswordLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 130 - 6), (int)((viewport.Height / 2) - 30)));
            accountItems.Add(newPasswordLabel);

            newPasswordBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            newPasswordBox.PasswordBox = true;
            newPasswordBox.Width = 115;
            newPasswordBox.Height = 20;
            newPasswordBox.X = (viewport.Width / 2) - (passwordBox.Width / 2) + 80 - 6;
            newPasswordBox.Y = viewport.Height / 2 - 30;
            newPasswordBox.MaxCharacters = 10;
            accountTextBoxes.Add(newPasswordBox);

            MenuItem reTypePasswordLabel = new MenuItem(MenuItemType.Text, "ReType Password");
            reTypePasswordLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 130 - 6), (int)((viewport.Height / 2))));
            accountItems.Add(reTypePasswordLabel);

            reTypePasswordBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            reTypePasswordBox.PasswordBox = true;
            reTypePasswordBox.Width = 115;
            reTypePasswordBox.Height = 20;
            reTypePasswordBox.X = (viewport.Width / 2) - (passwordBox.Width / 2) + 80 - 6;
            reTypePasswordBox.Y = viewport.Height / 2;
            reTypePasswordBox.MaxCharacters = 10;
            accountTextBoxes.Add(reTypePasswordBox);

            MenuItem emailLabel = new MenuItem(MenuItemType.Text, "Email");
            emailLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 130 - 6), (int)((viewport.Height / 2) + 30)));
            accountItems.Add(emailLabel);

            emailBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            emailBox.Width = 115;
            emailBox.Height = 20;
            emailBox.X = (viewport.Width / 2) - (passwordBox.Width / 2) + 80 - 6;
            emailBox.Y = viewport.Height / 2 + 30;
            emailBox.MaxCharacters = 25;
            accountTextBoxes.Add(emailBox);

            MenuItem questionLabel = new MenuItem(MenuItemType.Text, "Secret Question");
            questionLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 130 - 6), (int)((viewport.Height / 2) + 60)));
            accountItems.Add(questionLabel);

            questionBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            questionBox.Width = 115;
            questionBox.Height = 20;
            questionBox.X = (viewport.Width / 2) - (passwordBox.Width / 2) + 80 - 6;
            questionBox.Y = viewport.Height / 2 + 60;
            questionBox.MaxCharacters = 10;
            accountTextBoxes.Add(questionBox);

            MenuItem answerLabel = new MenuItem(MenuItemType.Text, "Answer");
            answerLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 130 - 6), (int)((viewport.Height / 2) + 90)));
            accountItems.Add(answerLabel);

            answerBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            answerBox.Width = 115;
            answerBox.Height = 20;
            answerBox.X = (viewport.Width / 2) - (passwordBox.Width / 2) + 80 - 6;
            answerBox.Y = viewport.Height / 2 + 90;
            answerBox.MaxCharacters = 10;
            accountTextBoxes.Add(answerBox);

            MenuItem cancelLabel = new MenuItem(MenuItemType.Button, "Cancel");
            cancelLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Cancel").X / 2)) - 80 - 6, (int)((viewport.Height / 2) + 148)));
            accountItems.Add(cancelLabel);

            MenuItem clearLabel = new MenuItem(MenuItemType.Button, "Clear");
            clearLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Clear").X / 2)) + 10 - 6, (int)((viewport.Height / 2) + 148)));
            accountItems.Add(clearLabel);

            MenuItem createLabel = new MenuItem(MenuItemType.Button, "Create");
            createLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Create").X / 2)) + 100 - 6, (int)((viewport.Height / 2) + 148)));
            accountItems.Add(createLabel);

            MenuItem accountErrorLabel = new MenuItem(MenuItemType.Text, "ERROR:");
            accountErrorLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 6), (int)((viewport.Height / 2) + 127)));
            accountItems.Add(accountErrorLabel);

            //MAIN SETTINGS DIALOG SETUP
            MenuItem settingsTitleLabel = new MenuItem(MenuItemType.Title, "Game Settings & Options");
            settingsTitleLabel.SetPosition(new Vector2((int)((width / 2) - (Cache.Fonts[FontType.MagicMedieval18].MeasureString("Create New Account").X / 2) - 20), (int)((height / 2) - 102)));
            settingsNormal.Add(settingsTitleLabel);
            settingsKeyboard.Add(settingsTitleLabel);
            settingsAdvanced.Add(settingsTitleLabel);

            MenuItem settingsLabel = new MenuItem(MenuItemType.Button, "Settings");
            settingsLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 66)));
            settingsNormal.Add(settingsLabel);
            settingsKeyboard.Add(settingsLabel);
            settingsAdvanced.Add(settingsLabel);

            MenuItem keyboardLabel = new MenuItem(MenuItemType.Button, "Keyboard");
            keyboardLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 29 - 6), (int)((viewport.Height / 2) - 66)));
            settingsNormal.Add(keyboardLabel);
            settingsKeyboard.Add(keyboardLabel);
            settingsAdvanced.Add(keyboardLabel);

            MenuItem advancedLabel = new MenuItem(MenuItemType.Button, "Advanced");
            advancedLabel.SetPosition(new Vector2((int)(viewport.Width / 2 + 65 - 6), (int)((viewport.Height / 2) - 66)));
            settingsNormal.Add(advancedLabel);
            settingsKeyboard.Add(advancedLabel);
            settingsAdvanced.Add(advancedLabel);

            MenuItem settingsCancelLabel = new MenuItem(MenuItemType.Button, "Cancel");
            settingsCancelLabel.SetPosition(new Vector2((int)((width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Cancel").X / 2)) - 76, (int)((height / 2) + 148)));
            settingsNormal.Add(settingsCancelLabel);
            settingsAdvanced.Add(settingsCancelLabel);

            MenuItem settingsCancelKeyboardLabel = new MenuItem(MenuItemType.Button, "Cancel");
            settingsCancelKeyboardLabel.SetPosition(new Vector2((int)((width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Cancel").X / 2)), (int)((height / 2) + 148)));
            settingsKeyboard.Add(settingsCancelKeyboardLabel);

            MenuItem applyChangesLabel = new MenuItem(MenuItemType.Button, "Save");
            applyChangesLabel.SetPosition(new Vector2((int)((width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Save").X / 2)) + 80, (int)((height / 2) + 148)));
            settingsNormal.Add(applyChangesLabel);
            settingsAdvanced.Add(applyChangesLabel);

            MenuItem settingsErrorLabel = new MenuItem(MenuItemType.Text, "ERROR:");
            settingsErrorLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 6), (int)((viewport.Height / 2) + 127)));
            settingsNormal.Add(settingsErrorLabel);
            settingsKeyboard.Add(settingsErrorLabel);
            settingsAdvanced.Add(settingsErrorLabel);

            //NORMAL SETTINGS
            MenuItem scrollBar = new MenuItem(MenuItemType.ScrollBar, "");
            scrollBar.SetPosition(new Vector2((int)(viewport.Width / 2 + 140), (int)((viewport.Height / 2) - 71)));
            scrollBar.Width = 4;
            scrollBar.Height = 211;
            settingsNormal.Add(scrollBar);

            int fullscreenLabelWidth = (int)((Cache.Fonts[FontType.MagicMedieval14].MeasureString("Fullscreen").X));

            MenuItem defaultLabel = new MenuItem(MenuItemType.Text, "Default Game Settings:");
            defaultLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 35)));
            defaultLabel.IsScrollable = true;
            settingsNormal.Add(defaultLabel);

            MenuItem defaultButton = new MenuItem(MenuItemType.SmallButton, "Restore");
            defaultButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 70), (int)((viewport.Height / 2) - 35)));
            defaultButton.IsScrollable = true;
            settingsNormal.Add(defaultButton);

            MenuItem fullscreenLabel = new MenuItem(MenuItemType.Text, "Screen Mode");
            fullscreenLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 10)));
            fullscreenLabel.IsScrollable = true;
            settingsNormal.Add(fullscreenLabel);

            MenuItem windowedButton = new MenuItem(MenuItemType.SmallButton, "Windowed");
            windowedButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 90), (int)((viewport.Height / 2) - 10)));
            windowedButton.IsScrollable = true;
            settingsNormal.Add(windowedButton);
            
            MenuItem fullscreenButton = new MenuItem(MenuItemType.SmallButton, "Fullscreen");
            fullscreenButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 170), (int)((viewport.Height / 2) - 10)));
            fullscreenButton.IsScrollable = true;
            settingsNormal.Add(fullscreenButton);

            MenuItem resolutionLabel = new MenuItem(MenuItemType.Text, "Resolution");
            resolutionLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) + 15)));
            resolutionLabel.IsScrollable = true;
            settingsNormal.Add(resolutionLabel);

            MenuItem resolutionButtonClassic = new MenuItem(MenuItemType.SmallButton, "640 x 480");
            resolutionButtonClassic.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 270), (int)((viewport.Height / 2) + 40)));
            resolutionButtonClassic.SettingType = SettingsType.Numbers;
            resolutionButtonClassic.IsScrollable = true;
            settingsNormal.Add(resolutionButtonClassic);

            MenuItem resolutionButtonStandard = new MenuItem(MenuItemType.SmallButton, "800 x 600");
            resolutionButtonStandard.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 178), (int)((viewport.Height / 2) + 40)));
            resolutionButtonStandard.SettingType = SettingsType.Numbers;
            resolutionButtonStandard.IsScrollable = true;
            settingsNormal.Add(resolutionButtonStandard);

            MenuItem resolutionButtonLarge = new MenuItem(MenuItemType.SmallButton, "1024 x 768");
            resolutionButtonLarge.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 90), (int)((viewport.Height / 2) + 40)));
            resolutionButtonLarge.SettingType = SettingsType.Numbers;
            resolutionButtonLarge.IsScrollable = true;
            settingsNormal.Add(resolutionButtonLarge);

            MenuItem musicLabel = new MenuItem(MenuItemType.Text, "Music");
            musicLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) + 70)));
            musicLabel.IsScrollable = true;
            settingsNormal.Add(musicLabel);

            MenuItem musicOnButton = new MenuItem(MenuItemType.SmallButton, "On");
            musicOnButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 33), (int)((viewport.Height / 2) + 70)));
            musicOnButton.IsScrollable = true;
            musicOnButton.SettingType = SettingsType.Music;
            settingsNormal.Add(musicOnButton);

            MenuItem musicOffButton = new MenuItem(MenuItemType.SmallButton, "Off");
            musicOffButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 80), (int)((viewport.Height / 2) + 70)));
            musicOffButton.IsScrollable = true;
            musicOffButton.SettingType = SettingsType.Music;
            settingsNormal.Add(musicOffButton);

            MenuItem musicVolumeLabel = new MenuItem(MenuItemType.Text, "Volume");
            musicVolumeLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) + 100)));
            musicVolumeLabel.IsScrollable = true;
            settingsNormal.Add(musicVolumeLabel);

            MenuItem musicVolumeMin = new MenuItem(MenuItemType.Text, "0");
            musicVolumeMin.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 163), (int)((viewport.Height / 2) + 100)));
            musicVolumeMin.SettingType = SettingsType.Numbers;
            musicVolumeMin.IsScrollable = true;
            settingsNormal.Add(musicVolumeMin);

            MenuItem musicVolumeMax = new MenuItem(MenuItemType.Text, "100");
            musicVolumeMax.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 32), (int)((viewport.Height / 2) + 100)));
            musicVolumeMax.SettingType = SettingsType.Numbers;
            musicVolumeMax.IsScrollable = true;
            settingsNormal.Add(musicVolumeMax);

            MenuItem musicVolumeSlider = new MenuItem(MenuItemType.SliderBar, "");
            musicVolumeSlider.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 150), (int)((viewport.Height / 2) + 110)));
            musicVolumeSlider.Width = 110;
            musicVolumeSlider.Height = 2;
            musicVolumeSlider.IsScrollable = true;
            musicVolumeSlider.SettingType = SettingsType.Music;
            settingsNormal.Add(musicVolumeSlider);

            MenuItem soundLabel = new MenuItem(MenuItemType.Text, "Sound");
            soundLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) + 130)));
            soundLabel.IsScrollable = true;
            settingsNormal.Add(soundLabel);

            MenuItem soundOnButton = new MenuItem(MenuItemType.SmallButton, "On");
            soundOnButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 33), (int)((viewport.Height / 2) + 130)));
            soundOnButton.IsScrollable = true;
            soundOnButton.SettingType = SettingsType.Sound;
            settingsNormal.Add(soundOnButton);

            MenuItem soundOffButton = new MenuItem(MenuItemType.SmallButton, "Off");
            soundOffButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 80), (int)((viewport.Height / 2) + 130)));
            soundOffButton.IsScrollable = true;
            soundOffButton.SettingType = SettingsType.Sound;
            settingsNormal.Add(soundOffButton);

            MenuItem soundVolumeLabel = new MenuItem(MenuItemType.Text, "Volume");
            soundVolumeLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) + 160)));
            soundVolumeLabel.IsScrollable = true;
            settingsNormal.Add(soundVolumeLabel);

            MenuItem soundVolumeMin = new MenuItem(MenuItemType.Text, "0");
            soundVolumeMin.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 163), (int)((viewport.Height / 2) + 160)));
            soundVolumeMin.SettingType = SettingsType.Numbers;
            soundVolumeMin.IsScrollable = true;
            settingsNormal.Add(soundVolumeMin);

            MenuItem soundVolumeMax = new MenuItem(MenuItemType.Text, "100");
            soundVolumeMax.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 32), (int)((viewport.Height / 2) + 160)));
            soundVolumeMax.SettingType = SettingsType.Numbers;
            soundVolumeMax.IsScrollable = true;
            settingsNormal.Add(soundVolumeMax);

            MenuItem soundVolumeSlider = new MenuItem(MenuItemType.SliderBar, "");
            soundVolumeSlider.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 150), (int)((viewport.Height / 2) + 170)));
            soundVolumeSlider.Width = 110;
            soundVolumeSlider.Height = 2;
            soundVolumeSlider.IsScrollable = true;
            soundVolumeSlider.SettingType = SettingsType.Sound;
            settingsNormal.Add(soundVolumeSlider);

            MenuItem detailLevelLabel = new MenuItem(MenuItemType.Text, "Detail Mode");
            detailLevelLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) + 190)));
            detailLevelLabel.IsScrollable = true;
            settingsNormal.Add(detailLevelLabel);

            MenuItem detailLowButton = new MenuItem(MenuItemType.SmallButton, "Low");
            detailLowButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 45), (int)((viewport.Height / 2) + 190)));
            detailLowButton.IsScrollable = true;
            settingsNormal.Add(detailLowButton);

            MenuItem detailMediumButton = new MenuItem(MenuItemType.SmallButton, "Medium");
            detailMediumButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 115), (int)((viewport.Height / 2) + 190)));
            detailMediumButton.IsScrollable = true;
            settingsNormal.Add(detailMediumButton);

            MenuItem detailHighButton = new MenuItem(MenuItemType.SmallButton, "High");
            detailHighButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 160), (int)((viewport.Height / 2) + 190)));
            detailHighButton.IsScrollable = true;
            settingsNormal.Add(detailHighButton);

            MenuItem transparencyLabel = new MenuItem(MenuItemType.Text, "Dialog Transparency");
            transparencyLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) + 220)));
            transparencyLabel.IsScrollable = true;
            settingsNormal.Add(transparencyLabel);

            MenuItem transparencyOnButton = new MenuItem(MenuItemType.SmallButton, "On");
            transparencyOnButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 33), (int)((viewport.Height / 2) + 220)));
            transparencyOnButton.IsScrollable = true;
            transparencyOnButton.SettingType = SettingsType.Transparency;
            settingsNormal.Add(transparencyOnButton);

            MenuItem transparencyOffButton = new MenuItem(MenuItemType.SmallButton, "Off");
            transparencyOffButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 80), (int)((viewport.Height / 2) + 220)));
            transparencyOffButton.IsScrollable = true;
            transparencyOffButton.SettingType = SettingsType.Transparency;
            settingsNormal.Add(transparencyOffButton);

            MenuItem miniMapLabel = new MenuItem(MenuItemType.Text, "Mini Map");
            miniMapLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) + 250)));
            miniMapLabel.IsScrollable = true;
            settingsNormal.Add(miniMapLabel);

            MenuItem miniMapOnButton = new MenuItem(MenuItemType.SmallButton, "On");
            miniMapOnButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 33), (int)((viewport.Height / 2) + 250)));
            miniMapOnButton.IsScrollable = true;
            miniMapOnButton.SettingType = SettingsType.MiniMap;
            settingsNormal.Add(miniMapOnButton);

            MenuItem miniMapOffButton = new MenuItem(MenuItemType.SmallButton, "Off");
            miniMapOffButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 80), (int)((viewport.Height / 2) + 250)));
            miniMapOffButton.IsScrollable = true;
            miniMapOffButton.SettingType = SettingsType.MiniMap;
            settingsNormal.Add(miniMapOffButton);

            MenuItem wisperLabel = new MenuItem(MenuItemType.Text, "Wisper");
            wisperLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) + 280)));
            wisperLabel.IsScrollable = true;
            settingsNormal.Add(wisperLabel);

            MenuItem wisperOnButton = new MenuItem(MenuItemType.SmallButton, "On");
            wisperOnButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 33), (int)((viewport.Height / 2) + 280)));
            wisperOnButton.IsScrollable = true;
            wisperOnButton.SettingType = SettingsType.Wisper;
            settingsNormal.Add(wisperOnButton);

            MenuItem wisperOffButton = new MenuItem(MenuItemType.SmallButton, "Off");
            wisperOffButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 80), (int)((viewport.Height / 2) + 280)));
            wisperOffButton.IsScrollable = true;
            wisperOffButton.SettingType = SettingsType.Wisper;
            settingsNormal.Add(wisperOffButton);

            MenuItem shoutLabel = new MenuItem(MenuItemType.Text, "Shout");
            shoutLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) + 310)));
            shoutLabel.IsScrollable = true;
            settingsNormal.Add(shoutLabel);

            MenuItem shoutOnButton = new MenuItem(MenuItemType.SmallButton, "On");
            shoutOnButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 33), (int)((viewport.Height / 2) + 310)));
            shoutOnButton.IsScrollable = true;
            shoutOnButton.SettingType = SettingsType.Shout;
            settingsNormal.Add(shoutOnButton);

            MenuItem shoutOffButton = new MenuItem(MenuItemType.SmallButton, "Off");
            shoutOffButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 140 - 80), (int)((viewport.Height / 2) + 310)));
            shoutOffButton.IsScrollable = true;
            shoutOffButton.SettingType = SettingsType.Shout;
            settingsNormal.Add(shoutOffButton);


            //ADVANCED SETTINGS
            MenuItem scrollBarAdvanced = new MenuItem(MenuItemType.ScrollBar, "");
            scrollBarAdvanced.SetPosition(new Vector2((int)(viewport.Width / 2 + 140), (int)((viewport.Height / 2) - 71)));
            scrollBarAdvanced.Width = 4;
            scrollBarAdvanced.Height = 211;
            settingsAdvanced.Add(scrollBarAdvanced);

            //MenuItem advancedtitle = new MenuItem(MenuItemType.Text, "Advanced Settings");
            //advancedtitle.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 15)));
            //advancedtitle.IsScrollable = true;
            //settingsAdvanced.Add(advancedtitle);

            AdvancedSettingsMenuItem MainTitle = new AdvancedSettingsMenuItem(display, AdvancedSettingsType.TeamColor);
            AdvancedSettingsMenuItem tab1 = new AdvancedSettingsMenuItem(display, AdvancedSettingsType.Lalala);
            AdvancedSettingsMenuItem tab2 = new AdvancedSettingsMenuItem(display, AdvancedSettingsType.Falalal);
            advancedSettingsMenuItems.Add(MainTitle);
            advancedSettingsMenuItems.Add(tab1);
            advancedSettingsMenuItems.Add(tab2);

            advancedSettingsTotalHeight = advancedSettingsMenuItems.Count * 32; //normal height for button. 
            //KEYBOARD SETTINGS
            MenuItem scrollBarKeyboard = new MenuItem(MenuItemType.ScrollBar, "");
            scrollBarKeyboard.SetPosition(new Vector2((int)(viewport.Width / 2 + 140), (int)((viewport.Height / 2) - 71)));
            scrollBarKeyboard.Width = 4;
            scrollBarKeyboard.Height = 211;
            settingsKeyboard.Add(scrollBarKeyboard);

            HotKeyMenuItem hotKeyTitle = new HotKeyMenuItem(display, "Customizable Hotkeys", hotKeyMenuItems.Count, MenuItemType.Text);
            hotKeyMenuItems.Add(hotKeyTitle);

            if (Cache.HotKeyConfiguration != null)
            {
                foreach (KeyValuePair<int, HotKey> hotKeyButton in Cache.HotKeyConfiguration)
                {
                    HotKeyMenuItem hotKeyMenuItem = new HotKeyMenuItem(display, new HotKey(hotKeyButton.Value.Press, hotKeyButton.Value.Hold, hotKeyButton.Value.Action, hotKeyButton.Value.Index), Cache.HotKeyConfiguration, hotKeyMenuItems.Count);
                    hotKeyMenuItems.Add(hotKeyMenuItem);
                }
            }

            HotKeyMenuItem newHotKey = new HotKeyMenuItem(display,"Create new Hotkey", hotKeyMenuItems.Count, MenuItemType.CreateHotKey);
            hotKeyMenuItems.Add(newHotKey);

            //DIALOG BOXES
            highlightedDialogBox = clickedDialogBox = MenuDialogBoxType.None;

            IMenuDialogBox dialogBox;

            dialogBoxes = new Dictionary<MenuDialogBoxType, IMenuDialogBox>();
            dialogBoxDrawOrder = new LinkedList<MenuDialogBoxType>();

            //LOGIN
            dialogBox = new MenuDialogBox(MenuDialogBoxType.Login, -1, -1, (width / 2), (height / 2));
            dialogBox.Moveable = false;
            dialogBoxes.Add(MenuDialogBoxType.Login, dialogBox);
            Display.Keyboard.Dispatcher.Subscriber = usernameBox;

            //NEW ACCOUNT
            dialogBox = new MenuDialogBox(MenuDialogBoxType.NewAccount, -1, -1, (width / 2), (height / 2));
            dialogBox.Moveable = false;
            dialogBoxes.Add(MenuDialogBoxType.NewAccount, dialogBox);

            //SETTINGS
            dialogBox = new MenuDialogBox(MenuDialogBoxType.Settings, -1, -1, (width / 2), (height / 2));
            dialogBox.Moveable = false;
            dialogBoxes.Add(MenuDialogBoxType.Settings, dialogBox);

            //KEYBOARD
            dialogBox = new MenuDialogBox(MenuDialogBoxType.Keyboard, -1, -1, (width / 2), (height / 2));
            dialogBox.Moveable = false;
            dialogBoxes.Add(MenuDialogBoxType.Keyboard, dialogBox);

            //ADVANCED
            dialogBox = new MenuDialogBox(MenuDialogBoxType.Advanced, -1, -1, (width / 2), (height / 2));
            dialogBox.Moveable = false;
            dialogBoxes.Add(MenuDialogBoxType.Advanced, dialogBox);

            //SHOW DIALOGBOX
            dialogBoxes[MenuDialogBoxType.Login].Show();

            // Creates threads to process sprite loading
            spriteQueueCallback = new Timer(new TimerCallback(SpriteHelper.HandleSpriteQueue), null, 0, 250);
        }

        public void ShowMessageBox(MenuMessageBox box)
        {

        }

        public void HideMessageBox(MenuDialogBoxType type)
        {

        }

        public void Update(GameTime gameTime, Microsoft.Xna.Framework.Game game)
        {
            display.Mouse.Update(Mouse.GetState());
            display.Keyboard.Update(Keyboard.GetState()); //File load exception wasn't handled. Could not load file or assembly 'System.Core, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' or one of its dependencies. Not enough storage is available to process this command. (Exception from HRESULT: 0x80070008)

            display.Mouse.State = GameMouseState.Normal;

            HandleMouse(game.IsActive);
            HandleKeyboard(game.IsActive);

            Cache.TransparencyFaders.Update();

            if (errorMessageTimer)
            {
                errorMessageTime -= gameTime.ElapsedGameTime.Milliseconds;
                if (errorMessageTime <= 0) { errorMessage = string.Empty; errorMessageTime = 5000; errorMessageTimer = false; }
            }

            if (settingsSaved)
            {
                errorMessage = "Settings Have Been Saved";
                settingsSaved = false;
            }

            foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
            {
                if (key.Value.Visible)
                {
                    switch (key.Key)
                    {
                        default: break;
                        case MenuDialogBoxType.Settings:
                            {
                                foreach (MenuItem item in settingsNormal)
                                {
                                    if (item.Type == MenuItemType.SmallButton)
                                    {
                                        item.Selected = false;
                                        if (Display.Resolution == Resolution.Classic && item.Text == "640 x 480") { item.Selected = true; }
                                        if (Display.Resolution == Resolution.Standard && item.Text == "800 x 600") { item.Selected = true; }
                                        if (Display.Resolution == Resolution.Large && item.Text == "1024 x 768") { item.Selected = true; }
                                        if (Display.DeviceManager.IsFullScreen && item.Text == "Fullscreen") { item.Selected = true; }
                                        if (!Display.DeviceManager.IsFullScreen && item.Text == "Windowed") { item.Selected = true; }
                                        if (Cache.GameSettings.SoundsOn && item.Text == "On" && item.SettingType == SettingsType.Sound) { item.Selected = true; }
                                        if (!Cache.GameSettings.SoundsOn && item.Text == "Off" && item.SettingType == SettingsType.Sound) { item.Selected = true; }
                                        if (Cache.GameSettings.MusicOn && item.Text == "On" && item.SettingType == SettingsType.Music) { item.Selected = true; }
                                        if (!Cache.GameSettings.MusicOn && item.Text == "Off" && item.SettingType == SettingsType.Music) { item.Selected = true; }
                                        if (Cache.GameSettings.DetailMode == GraphicsDetail.Low && item.Text == "Low") { item.Selected = true; }
                                        if (Cache.GameSettings.DetailMode == GraphicsDetail.Medium && item.Text == "Medium") { item.Selected = true; }
                                        if (Cache.GameSettings.DetailMode == GraphicsDetail.High && item.Text == "High") { item.Selected = true; }
                                        if (Cache.GameSettings.TransparentDialogs && item.Text == "On" && item.SettingType == SettingsType.Transparency) { item.Selected = true; }
                                        if (!Cache.GameSettings.TransparentDialogs && item.Text == "Off" && item.SettingType == SettingsType.Transparency) { item.Selected = true; }
                                        //if (Cache.GameState.DialogBoxes[DialogBoxType.MiniMap].Visible && item.Text == "On" && item.SettingType == SettingsType.MiniMap) { item.Selected = true; }
                                        //if (!Cache.GameState.DialogBoxes[DialogBoxType.MiniMap].Visible && item.Text == "Off" && item.SettingType == SettingsType.MiniMap) { item.Selected = true; }
                                        //TODO Wisper + Shout on/off
                                    }
                                }
                                break;
                            }
                        case MenuDialogBoxType.Advanced:
                            {
                                //ADVANCED MENU ITEMS LOGIC
                                //Setup Previous vs Current State to determine if a move should happen
                                bool expanded = false;
                                bool contracted = false;
                                int location = -1;
                                int count = advancedSettingsMenuItems.Count;
                                bool changed;
                                for (int i = 0; i < count; i++)
                                {
                                    changed = advancedSettingsMenuItems[i].Update();
                                    if (changed)                                   
                                    {
                                        switch (advancedSettingsMenuItems[i].State)
                                        {
                                            case MenuItemState.Normal:
                                                {
                                                    contracted = true;
                                                    break;
                                                }
                                            case MenuItemState.Expanded:
                                                {
                                                    expanded = true;
                                                    break;
                                                }
                                        }
                                        location = i;
                                        advancedSettingsMenuItems[i].HasChangedState = false;
                                        break;
                                    }
                                }

                                if (expanded && location != -1)
                                {
                                    for (int i = location + 1; i < count; i++)
                                    {
                                        advancedSettingsMenuItems[i].MoveDown(advancedSettingsMenuItems[location].ExtendedHeight);                                     
                                    }
                                    advancedSettingsTotalHeight += advancedSettingsMenuItems[location].ExtendedHeight;
                                }

                                if (contracted && location != -1)
                                {
                                    for (int i = location + 1; i < count; i++)
                                    {
                                        advancedSettingsMenuItems[i].MoveUp(advancedSettingsMenuItems[location].ExtendedHeight);
                                    }
                                    advancedSettingsTotalHeight -= advancedSettingsMenuItems[location].ExtendedHeight;
                                }

                                //SCROLL BAR LOGIC
                                //Reset to 0 scrollbarposition if can't scroll
                                if (advancedSettingsTotalHeight < advancedSettingsRenderTarget.Bounds.Height)
                                {
                                    advancedSettingsScrollCounter = 0;
                                    advancedSettingsMaxScroll = 0;
                                    advancedSettingsScrollPixels = 211;
                                    for (int i = 0; i < count; i++)
                                    {
                                        advancedSettingsMenuItems[i].ScrollBarPosition = 0;
                                    }
                                }
                                else
                                {
                                    //Update max scroll settings                                  
                                    advancedSettingsMaxScroll = advancedSettingsTotalHeight - advancedSettingsRenderTarget.Bounds.Height;
                                    advancedSettingsMaxScrollCount = (advancedSettingsMaxScroll < 1000) ? 10 : (advancedSettingsMaxScroll < 2000) ? 20 : (advancedSettingsMaxScroll < 4000) ? 50 : 200;
                                    advancedSettingsScrollFactor = (advancedSettingsMaxScrollCount == 10) ? 20 : (advancedSettingsMaxScrollCount == 50) ? 10 : (advancedSettingsMaxScrollCount == 100) ? 4 : 1;
                                    advancedSettingsScrollPixels = ((float)(advancedSettingsMaxScroll) / (float)(200)) * (float)(advancedSettingsScrollFactor); 

                                    //Update items for new scroll position
                                    for (int i = 0; i < count; i++)
                                    {
                                        advancedSettingsMenuItems[i].ScrollBarPosition = (int)(advancedSettingsScrollPixels * (float)-advancedSettingsScrollCounter);
                                    }
                                }
                                break;
                            }
                        case MenuDialogBoxType.Keyboard:
                            {
                                int count = hotKeyMenuItems.Count;
                                HotKeyMenuItem[] previousHotKeyState = hotKeyMenuItems.ToArray();
                                List<HotKeyMenuItem> previousHotKeyStateList = new List<HotKeyMenuItem>();
                                for (int i = 0; i < count; i ++ )
                                {
                                    HotKeyMenuItem item = new HotKeyMenuItem(previousHotKeyState[i]);
                                    previousHotKeyStateList.Add(item);
                                }

                                HotKeyMenuItem[] currentHotKeyState = hotKeyMenuItems.ToArray();
                                List<HotKeyMenuItem> currentHotKeyStateList = currentHotKeyState.ToList<HotKeyMenuItem>();

                                //MOUSE COLLISION DETECTION
                                foreach (HotKeyMenuItem item in currentHotKeyStateList)
                                {
                                   ConfigurationState update;
                                   update = item.Update();
                                   switch (update)
                                   {
                                       case ConfigurationState.Normal:
                                           {
                                               break;
                                           }
                                       case ConfigurationState.Changed:
                                           {
                                               hotKeyConfigChanged = true;
                                               break;
                                           }
                                       case ConfigurationState.Reload:
                                           {
                                               hotKeyConfigReload = true;
                                               break;
                                           }
                                   }
                                }
                          
                                int stateCount = 0;
                                foreach (HotKeyMenuItem state in currentHotKeyStateList)
                                {
                                    if (state.Selected) { stateCount = stateCount + 1; }
                                }

                                if (stateCount > 1) 
                                { 
                                    hotKeyMenuItems = previousHotKeyStateList; 
                                }

                                if (hotKeyConfigChanged)
                                {
                                    SaveHotKeyConfiguration();
                                    ReloadHotKeySettings();
                                    hotKeyConfigChanged = false;
                                }

                                if (hotKeyConfigReload)
                                {
                                    ReloadHotKeySettings();
                                    hotKeyConfigReload = false;
                                }
                                break;
                            }
                        case MenuDialogBoxType.NewAccount:
                            {
                                accountBox.Update(gameTime);
                                newPasswordBox.Update(gameTime);
                                reTypePasswordBox.Update(gameTime);
                                emailBox.Update(gameTime);
                                questionBox.Update(gameTime);
                                answerBox.Update(gameTime);

                                // MOUSE COLLISION DETECTION
                                //foreach (MenuItem item in accountItems)
                                //{
                                //    if (display.Mouse.X >= item.Position.X && display.Mouse.X <= item.Position.X + item.Width &&
                                //        display.Mouse.Y >= item.Position.Y & display.Mouse.Y <= item.Position.Y + item.Height)
                                //    {
                                //        item.Selected = true;
                                //    }
                                //    else item.Selected = false;
                                //}
                                break;
                            }
                        case MenuDialogBoxType.Login:
                            {
                                usernameBox.Update(gameTime);
                                passwordBox.Update(gameTime);

                                // MOUSE COLLISION DETECTION
                                //foreach (MenuItem item in loginItems)
                                //{
                                //    if (display.Mouse.X >= item.Position.X && display.Mouse.X <= item.Position.X + item.Width &&
                                //        display.Mouse.Y >= item.Position.Y & display.Mouse.Y <= item.Position.Y + item.Height)
                                //    {
                                //        item.Selected = true;
                                //    }
                                //    else item.Selected = false;
                                //}
                                break;
                            }
                    }
                }
            }

            //BACKGROUND
            backgroundDelay -= gameTime.ElapsedGameTime.Milliseconds;
            if (backgroundDelay <= 0)
            {
                backgroundDelay = 16;
                foreach (BackGroundImage image in backgroundImages)
                {
                    if (image.Type == BackgroundType.Background)
                    {
                        fadeDelay -= gameTime.ElapsedGameTime.Milliseconds;
                        if (fadeDelay <= 0)
                        {
                            fadeDelay = 10;
                            image.Color = SpriteHelper.Fade(image.Color, ref fadeDirection, ref fadeStage);
                        }

                    }

                    if (image.Type == BackgroundType.Smoke || image.Type == BackgroundType.Smokebehind)
                    {
                        if (image.Drawposition.X <= -1919 || image.Drawposition.X >= 1919)
                        {
                            image.Drawposition = new Vector2(0, image.Drawposition.Y);
                        }
                        else
                        {
                            image.ShiftRight(image);
                        }
                    }

                    if (image.Type == BackgroundType.Midground || image.Type == BackgroundType.Foreground)
                    {
                        if (image.Drawposition.X <= -1919 || image.Drawposition.X >= 1919)
                        {
                            image.Drawposition = new Vector2(0, image.Drawposition.Y);
                        }
                        else
                        {
                            image.ShiftLeft(image);
                        }
                    }

                    if (image.Type == BackgroundType.Weapon)
                    {
                        if (image.Counter < 80)
                        {
                            if (image.Counter == 79)
                            {
                                image.ShiftUp(image, 0.1f);
                                image.Counter = 81;
                            }
                            else
                            {
                                image.ShiftUp(image, 0.1f);
                                image.Rotate(image, 0.001f);
                                image.Counter++;
                            }
                        }
                        else if (image.Counter > 80)
                        {
                            if (image.Counter == 160)
                            {
                                image.ShiftDown(image, 0.1f);
                                image.Counter = 0;
                            }
                            else
                            {
                                image.ShiftDown(image, 0.1f);
                                image.Rotate(image, -0.001f);
                                image.Counter++;
                            }
                        }
                    }

                    if (image.Type == BackgroundType.Body)
                    {
                        if (image.Counter < 80)
                        {
                            if (image.Counter == 79)
                            {
                                image.ShiftUp(image, 0.025f);
                                image.Counter = 81;
                            }
                            else
                            {
                                image.ShiftUp(image, 0.025f);
                                image.Scale = image.Scale + 0.0008f;
                                image.ShiftLeft(image);
                                image.Counter++;
                            }
                        }
                        else if (image.Counter > 80)
                        {
                            if (image.Counter == 160)
                            {
                                image.ShiftDown(image, 0.025f);
                                image.Counter = 0;
                            }
                            else
                            {
                                image.ShiftDown(image, 0.025f);
                                image.Scale = image.Scale - 0.0008f;
                                image.ShiftRight(image);
                                image.Counter++;
                            }
                        }
                    }

                    if (image.Type == BackgroundType.Grass)
                    {
                        if (image.Counter < 40)
                        {
                            if (image.Counter == 39)
                            {
                                image.ShiftDown(image, 0.02f);
                                image.ShiftLeft(image);
                                image.Counter = 41;
                            }
                            else
                            {
                                image.ShiftDown(image, 0.02f);
                                image.ShiftLeft(image);
                                image.Counter++;
                            }
                        }
                        else if (image.Counter > 40)
                        {
                            if (image.Counter == 80)
                            {
                                image.ShiftUp(image, 0.02f);
                                image.ShiftRight(image);
                                image.Counter = 0;
                            }
                            else
                            {
                                image.ShiftUp(image, 0.02f);
                                image.ShiftRight(image);
                                image.Counter++;
                            }
                        }
                    }
                }
            }

            while (processQueue.Count > 0 && processQueue.Peek() != null)
            {
                Command command;
                lock (processQueueLock)
                {
                    command = processQueue.Dequeue();
                    ServerProcess(Utility.Decrypt(command.Data), command.Identity, command.IPAddress);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            Viewport viewport = GraphicsDevice.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            int width = display.ResolutionWidth;
            int height = display.ResolutionHeight;
            int mouseX = (int)display.Mouse.X;
            int mouseY = (int)display.Mouse.Y;

            blinkFrame = Cache.TransparencyFaders.BlinkFrame;
            glowFrame = Cache.TransparencyFaders.GlowFrame;
            pulseFrame = Cache.TransparencyFaders.PulseFrame;

            //CREATE AN ORDERED ARRAY OF BACKGROUNDIMAGES
            backgroundcollection = new BackGroundImage[backgroundImages.Count];
            backgroundImages.CopyTo(backgroundcollection, 0);

            //ITERATE THROUGH ARRAY DRAWING BACKGROUNDIMAGES   
            for (int a = 0; a < backgroundcollection.Length; a++)
            {
                BackGroundImage image = backgroundcollection[a];
                {
                    if (image.Drawposition.X == 0)
                    {
                        spriteBatch.Draw(image.Texture, image.Drawposition, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0);
                    }
                    else
                    {
                        if (image.Drawposition.X < 0)
                        {
                            Vector2 after = new Vector2(image.Drawposition.X + 1918, image.Drawposition.Y);
                            spriteBatch.Draw(image.Texture, image.Drawposition, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //Second       
                            spriteBatch.Draw(image.Texture, after, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //Third                     
                        }
                        else
                        {
                            Vector2 before = new Vector2(image.Drawposition.X - 1918, image.Drawposition.Y);
                            spriteBatch.Draw(image.Texture, before, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //First                       
                            spriteBatch.Draw(image.Texture, image.Drawposition, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //Second
                        }
                    }
                }
            }

            //DRAW DECORATIVE EDGE
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15 + 1, 0 + 1, 164, 168), null, Microsoft.Xna.Framework.Color.Black, 0, new Vector2(0, 0), SpriteEffects.FlipVertically, 1); //Top left black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15, 0, 164, 168), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipVertically, 1); //Top left
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15 + 1, 0 + 1, 164, 168), null, Microsoft.Xna.Framework.Color.Black, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 1); //Top right black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15, 0, 164, 168), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 1); //Top right
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15 + 1, height - 168 + 1, 164, 168), Microsoft.Xna.Framework.Color.Black); //Button left black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15, height - 168, 164, 168), Microsoft.Xna.Framework.Color.White); //Button left
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15 + 1, height - 168 + 1, 164, 168), null, Microsoft.Xna.Framework.Color.Black, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 1); //Button right black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15, height - 168, 164, 168), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 1); //Button right

            //FOR TESTING PURPOSES
            //int displaycount = 0;
            //foreach (DisplayMode mode in GraphicsAdapter.DefaultAdapter.SupportedDisplayModes)
            //{

            //    spriteBatch.DrawString(Display.Fonts[FontType.DamageSmall], "Resolution " + mode.Width.ToString() + "X" + mode.Height.ToString() + "   Aspect Ration: " + mode.AspectRatio.ToString() , new Vector2(400, 100 + (displaycount * 20)), Color.Black);
            //    spriteBatch.DrawString(Display.Fonts[FontType.DamageSmall], "Resolution " + mode.Width.ToString() + "X" + mode.Height.ToString() + "   Aspect Ration: " + mode.AspectRatio.ToString(), new Vector2(400, 100 + (displaycount * 20)), Color.Red);
            //    displaycount++;
            //    //mode.whatever (and use any of avaliable information)
            //}

            //MAINMENU
            foreach (MenuItem item in mainMenuItems)
            {
                                                        
                if (item.Type == MenuItemType.Title)                        
                {                        
                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval40], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);                        
                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval40], item.Text, item.Position, Color.White * 0.8f);                                                     
                }

                if (item.Type == MenuItemType.Button)
                {  
                    Rectangle itemLocation = new Rectangle((int)(item.Position.X) - ((80 - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X)) / 2), (int)(item.Position.Y) - 16, 80, 50);    
                    spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X, itemLocation.Y + 6, itemLocation.Width, itemLocation.Height - 12), Color.DarkOrange);        
                    spriteBatch.Draw(textbox, itemLocation, Color.White);              
                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.White);

                    //MOUSE OVER HIGHLIGHT                     
                    if ((mouseY > itemLocation.Y + 2) && (mouseY < (itemLocation.Y + itemLocation.Height - 9)))                  
                    {                           
                        if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))                                     
                        {
                            display.Mouse.State = GameMouseState.Pickup;     
                            spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);                                      
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.DarkOrange * glowFrame);                                                      
                        }                     
                    }
                }             
            }
            
            //DIALOGBOXES - From mainMenu
            foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
            {
                if (key.Value.Visible)
                {

                    switch (key.Key)
                    {
                        default: break;
                        case MenuDialogBoxType.Advanced:
                            {     
                                //Borders
                                Rectangle backgroundLocation = new Rectangle((width / 2) - 137 - 6, (height / 2) - 110, 290, 262);
                                spriteBatch.Draw(dialogFader, backgroundLocation, Color.White);

                                //LEFT AND RIGHT BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 134 - 6, (height / 2) - 110, 271, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) + 160 - 6, (height / 2) - 110, 271, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);

                                //TOP AND BOTTOM BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) + 136, 310, 44), null, Color.White);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) - 117, 310, 56), null, Color.White);

                                foreach (MenuItem item in settingsAdvanced)
                                {
                                    switch (item.Type)
                                    {
                                        case MenuItemType.Title:
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, item.Position, Color.White);
                                                break;
                                            }
                                        case MenuItemType.Button:
                                            {
                                                Rectangle itemLocation = new Rectangle((int)(item.Position.X) - ((90 - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X)) / 2), (int)(item.Position.Y) - 16, 90, 50);
                                                spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X, itemLocation.Y + 6, itemLocation.Width, itemLocation.Height - 12), Color.DarkOrange);
                                                spriteBatch.Draw(textbox, itemLocation, Color.White);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.White);

                                                //HIGHLIGHT BUTTONS
                                                if ((mouseY > itemLocation.Y + 7) && (mouseY < (itemLocation.Y + itemLocation.Height - 8)))
                                                {
                                                    if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                                    {
                                                        display.Mouse.State = GameMouseState.Pickup;
                                                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.DarkOrange * glowFrame);
                                                    }
                                                }
                                                break;
                                            }
                                        case MenuItemType.ScrollBar:
                                            {
                                                spriteBatch.Draw(backgroundTexture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height), Color.Black);
                                                SpriteHelper.DrawScrollBarMainMenu(spriteBatch, (int)(item.Position.X), (int)(item.Position.Y), advancedSettingsScrollCounter * advancedSettingsScrollFactor);
                                                if ((mouseY > item.Position.Y - 4) && (mouseY < (item.Position.Y + item.Height + 4)))
                                                {
                                                    if ((mouseX > item.Position.X) && (mouseX < (item.Position.X + item.Width + 8)))
                                                    {
                                                        display.Mouse.State = GameMouseState.Pickup;
                                                        spriteBatch.Draw(backgroundTexture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height), Color.DarkRed * glowFrame);
                                                        SpriteHelper.DrawScrollBarMainMenu(spriteBatch, (int)(item.Position.X), (int)(item.Position.Y), advancedSettingsScrollCounter * advancedSettingsScrollFactor, Color.DarkRed * glowFrame);
                                                    }
                                                }
                                                break;
                                            }
                                        case MenuItemType.Text:
                                            if (item.Text.StartsWith("ERROR"))
                                            {
                                                if (!string.IsNullOrEmpty(errorMessage))
                                                {
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2) + 1), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2) + 1)), Color.Black);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2)), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2))), Color.Red);
                                                }
                                            }
                                            else
                                            {
                                                if (item.Position.Y + item.ScrollPosition > (int)((viewport.Height / 2) - 37) && (item.Position.Y + item.ScrollPosition < (int)((viewport.Height / 2) + 120)))
                                                {
                                                    if (item.SettingType == SettingsType.Numbers)
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                                    }
                                                    else
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                                    }
                                                }
                                            }
                                            break;
                                    }

                                }

                                //HIGHLIGHT ON TEXT OF MENU   
                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Options", new Vector2((int)((width / 2)) + 8, (int)(viewport.Height - 33)), Color.DarkOrange * 0.8f);
                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Advanced", new Vector2((int)(viewport.Width / 2 + 65 - 6), (int)((viewport.Height / 2) - 66)), Color.DarkOrange * 0.8f);

                                DrawAdvancedSettingsToTexture(spriteBatch, gameTime, advancedSettingsRenderTarget);
                                break;
                            }
                        case MenuDialogBoxType.Keyboard:
                            {
                                //Borders
                                Rectangle backgroundLocation = new Rectangle((width / 2) - 137 - 6, (height / 2) - 110, 290, 262);
                                spriteBatch.Draw(dialogFader, backgroundLocation, Color.White);

                                //LEFT AND RIGHT BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 134 - 6, (height / 2) - 110, 271, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) + 160 - 6, (height / 2) - 110, 271, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);

                                //TOP AND BOTTOM BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) + 136, 310, 44), null, Color.White);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) - 117, 310, 56), null, Color.White);

                                foreach (MenuItem item in settingsKeyboard)
                                {
                                    switch (item.Type)
                                    {
                                        case MenuItemType.Title:
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, item.Position, Color.White);
                                                break;
                                            }
                                        case MenuItemType.Button:
                                            {
                                                Rectangle itemLocation = new Rectangle((int)(item.Position.X) - ((90 - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X)) / 2), (int)(item.Position.Y) - 16, 90, 50);
                                                spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X, itemLocation.Y + 6, itemLocation.Width, itemLocation.Height - 12), Color.DarkOrange);
                                                spriteBatch.Draw(textbox, itemLocation, Color.White);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.White);

                                                //HIGHLIGHT BUTTONS

                                                //Skip highlight if hotkey item is selected
                                                bool selected = false;
                                                for (int i = 0; i < hotKeyMenuItems.Count; i++)
                                                {
                                                    HotKeyMenuItem hotKeyItem = hotKeyMenuItems[i];
                                                    selected = hotKeyItem.Selected;
                                                    if (selected) { break; }
                                                }
                                                if (selected) { break; }

                                                if ((mouseY > itemLocation.Y + 7) && (mouseY < (itemLocation.Y + itemLocation.Height - 8)))
                                                {
                                                    if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                                    {
                                                        display.Mouse.State = GameMouseState.Pickup;
                                                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.DarkOrange * glowFrame);
                                                    }
                                                }
                                                break;
                                            }
                                        case MenuItemType.ScrollBar:
                                            {
                                                spriteBatch.Draw(backgroundTexture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height), Color.Black);
                                                SpriteHelper.DrawScrollBarMainMenu(spriteBatch, (int)(item.Position.X), (int)(item.Position.Y), (int)((float)(item.Height - 16) * ((float)(keyboardSettingsScrollCounter) / (float)(hotKeyMenuItems.Count - 3))));
                                                if ((mouseY > item.Position.Y - 4) && (mouseY < (item.Position.Y + item.Height + 4)))
                                                {
                                                    if ((mouseX > item.Position.X) && (mouseX < (item.Position.X + item.Width + 8)))
                                                    {
                                                        display.Mouse.State = GameMouseState.Pickup;
                                                        spriteBatch.Draw(backgroundTexture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height), Color.DarkRed * glowFrame);
                                                        SpriteHelper.DrawScrollBarMainMenu(spriteBatch, (int)(item.Position.X), (int)(item.Position.Y), (int)((float)(item.Height - 16) * ((float)(keyboardSettingsScrollCounter) / (float)(hotKeyMenuItems.Count - 3))), Color.DarkRed * glowFrame);
                                                    }
                                                }
                                                break;
                                            }
                                        case MenuItemType.Text:
                                            if (item.Text.StartsWith("ERROR"))
                                            {
                                                if (!string.IsNullOrEmpty(errorMessage))
                                                {
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2) + 1), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2) + 1)), Color.Black);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2)), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2))), Color.Red);
                                                }
                                            }
                                            else
                                            {
                                                if (item.Position.Y + item.ScrollPosition > (int)((viewport.Height / 2) - 37) && (item.Position.Y + item.ScrollPosition < (int)((viewport.Height / 2) + 120)))
                                                {
                                                    if (item.SettingType == SettingsType.Numbers)
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                                    }
                                                    else
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                                    }
                                                }
                                            }
                                            break;
                                    }
                                }

                                //HIGHLIGHT ON TEXT OF MENU   
                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Options", new Vector2((int)((width / 2)) + 8, (int)(viewport.Height - 33)), Color.DarkOrange * 0.8f);
                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Keyboard", new Vector2((int)(viewport.Width / 2 - 29 - 6), (int)((viewport.Height / 2) - 66)), Color.DarkOrange * 0.8f);

                                for (int i = hotKeyMenuItems.Count - 1; i > -1; i--)
                                {
                                    HotKeyMenuItem item = hotKeyMenuItems[i];
                                    item.Draw(spriteBatch, gameTime, glowFrame);
                                }
                                break;
                            }
                        case MenuDialogBoxType.Settings:
                            {
                                //Borders
                                Rectangle backgroundLocation = new Rectangle(( width / 2) - 137 - 6, (height / 2) - 110, 290, 262 );
                                spriteBatch.Draw(dialogFader, backgroundLocation, Color.White);

                                //LEFT AND RIGHT BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 134 - 6, (height / 2) - 110, 271, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) + 160 - 6, (height / 2) - 110, 271, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);
                       
                                //TOP AND BOTTOM BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) + 136, 310, 44), null, Color.White);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) - 117, 310, 56), null, Color.White);


                                foreach (MenuItem item in settingsNormal)
                                {
                                    switch (item.Type)
                                    {
                                        case MenuItemType.Title:
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, item.Position, Color.White);
                                                break;
                                            }
                                        case MenuItemType.Button:
                                            {
                                                Rectangle itemLocation = new Rectangle((int)(item.Position.X) - ((90 - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X)) / 2), (int)(item.Position.Y) - 16, 90, 50);
                                                spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X, itemLocation.Y + 6, itemLocation.Width, itemLocation.Height - 12), Color.DarkOrange);
                                                spriteBatch.Draw(textbox, itemLocation, Color.White);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.White);

                                                //HIGHLIGHT BUTTONS
                                                if ((mouseY > itemLocation.Y + 7) && (mouseY < (itemLocation.Y + itemLocation.Height - 8)))
                                                {
                                                    if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                                    {
                                                        display.Mouse.State = GameMouseState.Pickup;
                                                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.DarkOrange * glowFrame);
                                                    }
                                                }
                                                break;
                                            }
                                        case MenuItemType.SmallButton:
                                            {
                                                if (item.Position.Y + item.ScrollPosition > (int)((viewport.Height / 2) - 37) && (item.Position.Y + item.ScrollPosition < (int)((viewport.Height / 2) + 120)))
                                                {
                                                    if (item.Selected)
                                                    {
                                                        if (item.SettingType == SettingsType.Numbers)
                                                        {
                                                            spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X) + 1, (int)(item.Position.Y + item.ScrollPosition) + 1), Color.Black);
                                                            spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkOrange);
                                                        }
                                                        else
                                                        {
                                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X) + 1, (int)(item.Position.Y + item.ScrollPosition) + 1), Color.Black);
                                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkOrange);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (item.SettingType == SettingsType.Numbers)
                                                        {
                                                            spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X) + 1, (int)(item.Position.Y + item.ScrollPosition) + 1), Color.Black);
                                                            spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkGray * 0.7f);
                                                        }
                                                        else
                                                        {
                                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X) + 1, (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkGray * 0.7f);
                                                        }
                                                    }

                                                    //HIGHLIGHT BUTTONS
                                                    if ((mouseY > item.Position.Y + item.ScrollPosition) && (mouseY < (item.Position.Y + item.ScrollPosition + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y))))
                                                    {
                                                        if ((mouseX > item.Position.X) && (mouseX < (item.Position.X + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X))))
                                                        {
                                                            if (!item.Selected) { display.Mouse.State = GameMouseState.Pickup; }
                                                            if (item.SettingType == SettingsType.Numbers)
                                                            {
                                                                spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X) + 1, (int)(item.Position.Y + item.ScrollPosition) + 1), Color.Black * glowFrame);
                                                                spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed * glowFrame);
                                                            }
                                                            else
                                                            {
                                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X) + 1, (int)(item.Position.Y + item.ScrollPosition) + 1), Color.Black * glowFrame);
                                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed * glowFrame);
                                                                
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                            //add mouse over effects
                                        case MenuItemType.ScrollBar:
                                            {
                                                spriteBatch.Draw(backgroundTexture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height), Color.Black);
                                                SpriteHelper.DrawScrollBarMainMenu(spriteBatch, (int)(item.Position.X), (int)(item.Position.Y), (int)((float)(211) * ((float)(settingsScrollcounter) / (float)(22))));
                                                if ((mouseY > item.Position.Y - 4) && (mouseY < (item.Position.Y + item.Height + 4)))
                                                {
                                                    if ((mouseX > item.Position.X) && (mouseX < (item.Position.X + item.Width + 8)))
                                                    {
                                                        display.Mouse.State = GameMouseState.Pickup;
                                                        spriteBatch.Draw(backgroundTexture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height), Color.DarkRed * glowFrame);
                                                        SpriteHelper.DrawScrollBarMainMenu(spriteBatch, (int)(item.Position.X), (int)(item.Position.Y), (int)((float)(211) * ((float)(settingsScrollcounter) / (float)(22))), Color.DarkRed * glowFrame);
                                                    }
                                                }
                                                break;
                                            }


                                        case MenuItemType.SliderBar:
                                            {
                                                if (item.Position.Y + item.ScrollPosition > (int)((viewport.Height / 2) - 37) && (item.Position.Y + item.ScrollPosition < (int)((viewport.Height / 2) + 120)))
                                                {
                                                    spriteBatch.Draw(backgroundTexture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y) + item.ScrollPosition, item.Width, item.Height), Color.Black);
                                                    if (item.SettingType == SettingsType.Sound )SpriteHelper.DrawSliderMainMenu(spriteBatch, (int)(item.Position.X), (int)(item.Position.Y) + item.ScrollPosition - 6, (int)Cache.GameSettings.SoundVolume);
                                                    if (item.SettingType == SettingsType.Music) SpriteHelper.DrawSliderMainMenu(spriteBatch, (int)(item.Position.X), (int)(item.Position.Y) + item.ScrollPosition - 6, (int)Cache.GameSettings.MusicVolume);
                                                    if ((mouseY > item.Position.Y + item.ScrollPosition - 12) && (mouseY < (item.Position.Y + item.ScrollPosition + item.Height + 8)))
                                                    {
                                                        if ((mouseX > item.Position.X - 4) && (mouseX < (item.Position.X + item.Width + 4)))
                                                        {
                                                            display.Mouse.State = GameMouseState.Pickup;
                                                            spriteBatch.Draw(backgroundTexture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y) + item.ScrollPosition, item.Width, item.Height), Color.DarkRed * glowFrame);
                                                            if (item.SettingType == SettingsType.Sound) SpriteHelper.DrawSliderMainMenu(spriteBatch, (int)(item.Position.X), (int)(item.Position.Y) + item.ScrollPosition - 6, (int)Cache.GameSettings.SoundVolume, Color.DarkRed * glowFrame);
                                                            if (item.SettingType == SettingsType.Music) SpriteHelper.DrawSliderMainMenu(spriteBatch, (int)(item.Position.X), (int)(item.Position.Y) + item.ScrollPosition - 6, (int)Cache.GameSettings.MusicVolume, Color.DarkRed * glowFrame);
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        case MenuItemType.Text:
                                            if (item.Text.StartsWith("ERROR"))
                                            {
                                                if (!string.IsNullOrEmpty(errorMessage))
                                                {
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2) + 1), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2) + 1)), Color.Black);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2)), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2))), Color.Red);
                                                }
                                            }
                                            else
                                            {
                                                if (item.Position.Y + item.ScrollPosition > (int)((viewport.Height / 2) - 37) && (item.Position.Y + item.ScrollPosition < (int)((viewport.Height / 2) + 120)))
                                                {
                                                    if (item.SettingType == SettingsType.Numbers)
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                                    }
                                                    else
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                                    }
                                                }
                                            }
                                            break;
                                    }

                                }

                                //HIGHLIGHT ON TEXT OF MENU   
                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Options", new Vector2((int)((width / 2)) + 8, (int)(viewport.Height - 33)), Microsoft.Xna.Framework.Color.DarkOrange * 0.8f);
                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Settings", new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 66)), Microsoft.Xna.Framework.Color.DarkOrange * 0.8f);
                                break;
                            }
                        case MenuDialogBoxType.NewAccount:
                            {
                                //HIGHLIGHT ON TEXT OF MENU
                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Account", new Vector2((int)((width / 2) - 69), (int)(viewport.Height - 33)), Microsoft.Xna.Framework.Color.DarkOrange * 0.8f);

                                //Borders
                                Rectangle backgroundLocation = new Rectangle(( width / 2) - 137 - 6, (height / 2) - 110, 290, 262 );
                                spriteBatch.Draw(dialogFader, backgroundLocation, Color.White);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 134 - 6, (height / 2) - 110, 271, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) + 160 - 6, (height / 2) - 110, 271, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) + 136, 310, 44), null, Color.White);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) - 117, 310, 56), null, Color.White);
                                textBoxes.Clear();

                                Rectangle accountLocation = new Rectangle(accountBox.X - 8, accountBox.Y - 9, accountBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, accountLocation, Color.White);
                                textBoxes.Add(accountLocation);

                                Rectangle newPasswordLocation = new Rectangle(newPasswordBox.X - 8, newPasswordBox.Y - 9, newPasswordBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, newPasswordLocation, Color.White);
                                textBoxes.Add(newPasswordLocation);

                                Rectangle reTypePasswordLocation = new Rectangle(reTypePasswordBox.X - 8, reTypePasswordBox.Y - 9, reTypePasswordBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, reTypePasswordLocation, Color.White);
                                textBoxes.Add(reTypePasswordLocation);

                                Rectangle emailLocation = new Rectangle(emailBox.X - 8, emailBox.Y - 9, emailBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, emailLocation, Color.White);
                                textBoxes.Add(emailLocation);

                                Rectangle questionLocation = new Rectangle(questionBox.X - 8, questionBox.Y - 9, questionBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, questionLocation, Color.White);
                                textBoxes.Add(questionLocation);

                                Rectangle answerLocation = new Rectangle(answerBox.X - 8, answerBox.Y - 9, answerBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, answerLocation, Color.White);
                                textBoxes.Add(answerLocation);

                                foreach (Rectangle itemLocation in textBoxes)
                                {
                                    if ((mouseY > itemLocation.Y + 2) && (mouseY < (itemLocation.Y + itemLocation.Height - 9)))
                                    {
                                        if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                        {
                                            display.Mouse.State = GameMouseState.Pickup;
                                            spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                        }
                                    }
                                }

                                accountBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                newPasswordBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                reTypePasswordBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                emailBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                questionBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                answerBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                foreach (MenuItem item in accountItems)
                                {
                                    switch (item.Type)
                                    {
                                        default: break;
                                        case MenuItemType.Title:
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, item.Position, Color.White);
                                                break;
                                            }
                                        case MenuItemType.Button:
                                            {
                                                Rectangle itemLocation = new Rectangle((int)(item.Position.X) - ((80 - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X)) / 2), (int)(item.Position.Y) - 16, 80, 50);  
                                                spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X, itemLocation.Y + 6, itemLocation.Width, itemLocation.Height - 12), Color.DarkOrange);
                                                spriteBatch.Draw(textbox, itemLocation, Color.White);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.White);
                                                //HIGHLIGHT BUTTONS
                                                if ((mouseY > itemLocation.Y) && (mouseY < (itemLocation.Y + itemLocation.Height - 6)))
                                                {
                                                    if ((mouseX > itemLocation.X) && (mouseX < (itemLocation.X + itemLocation.Width)))
                                                    {
                                                        display.Mouse.State = GameMouseState.Pickup;
                                                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.DarkOrange * glowFrame);
                                                    }
                                                }
                                                break;
                                            }
                                        case MenuItemType.Text:
                                            if (item.Text.StartsWith("ERROR"))
                                            {
                                                if (!string.IsNullOrEmpty(errorMessage))
                                                {
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2) + 1), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2) + 1)), Color.Black);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2)),(int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2))), Color.Red);
                                                }
                                            }
                                            else
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + 1)), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y)), Color.White);
                                            }
                                            break;
                                    }

                                }

                                break;
                            }
                        case MenuDialogBoxType.Login:
                            {
                                //HIGHLIGHT ON TEXT OF MENU
                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Login", new Vector2((int)((width / 2) - 140), (int)(viewport.Height - 33)), Microsoft.Xna.Framework.Color.DarkOrange * 0.8f);

                                foreach (MenuItem item in loginItems)
                                {
                                    switch (item.Type)
                                    {
                                        default: break;
                                        case MenuItemType.Button:
                                            {
                                                Rectangle itemLocation = new Rectangle((int)(item.Position.X) - 34, (int)(item.Position.Y) - 12, (int)(130), (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y) + 18);
                                                spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X, itemLocation.Y + 6, itemLocation.Width, itemLocation.Height - 12), Color.DarkOrange);
                                                spriteBatch.Draw(textbox, itemLocation, Color.White);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.White);
                                                //HIGHLIGHT BUTTONS
                                                if ((mouseY > itemLocation.Y) && (mouseY < (itemLocation.Y + itemLocation.Height - 6)))
                                                {
                                                    if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                                    {
                                                        display.Mouse.State = GameMouseState.Pickup;
                                                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, item.Position, Color.DarkOrange * glowFrame);
                                                    }
                                                }
                                                break;
                                            }
                                        case MenuItemType.Text:
                                            if (item.Text.StartsWith("ERROR"))
                                            {
                                                if (!string.IsNullOrEmpty(errorMessage))
                                                {
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2) + 1), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2) + 1)), Color.Black);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2)), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2))), Color.Red);
                                                }
                                            }
                                            else
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + 1)), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y)), Color.White);
                                            }
                                            break;
                                    }                         
                                }
                                textBoxes.Clear();

                                Rectangle usernameLocation = new Rectangle(usernameBox.X - 8, usernameBox.Y - 9, usernameBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, usernameLocation, Color.White);
                                textBoxes.Add(usernameLocation);

                                Rectangle passwordLocation = new Rectangle(passwordBox.X - 8, passwordBox.Y - 9, passwordBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, passwordLocation, Color.White);
                                textBoxes.Add(passwordLocation);

                                foreach (Rectangle itemLocation in textBoxes)
                                {
                                    if ((mouseY > itemLocation.Y + 2) && (mouseY < (itemLocation.Y + itemLocation.Height - 9)))
                                    {
                                        if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                        {
                                            display.Mouse.State = GameMouseState.Pickup;
                                            spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                        }
                                    }
                                }

                                usernameBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                passwordBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                break;
                            }
                    }
                }
            }

            /* DIALOG BOXES */
            highlightedDialogBox = MenuDialogBoxType.None; // checks mouse/dialog collisions
            if (dialogBoxes.Count > 0)
                foreach (MenuDialogBoxType dialogType in dialogBoxDrawOrder)
                {
                    IMenuDialogBox box = dialogBoxes[dialogType];

                    if (box.Visible && box.Draw(spriteBatch, gameTime))
                        highlightedDialogBox = box.Type;
                }

            // DRAW MOUSE
            display.Mouse.Draw(spriteBatch);

            //Rectangle titleLocation = new Rectangle((width / 2) - 80, 0, 1, 1080);
            //Rectangle titleLocation1 = new Rectangle((width / 2) - 155, 0, 1, 1080);
            //Rectangle titleLocation2 = new Rectangle((width / 2) + 80, 0, 1, 1080);
            //Rectangle titleLocation4 = new Rectangle((width / 2) + 155, 0, 1, 1080);
            //Rectangle titleLocation3 = new Rectangle((width / 2), 0, 1, 1080);
            //spriteBatch.DrawItemPopup(dialogBorder, titleLocation, Color.Cyan);
            //spriteBatch.DrawItemPopup(dialogBorder, titleLocation1, Color.Cyan);
            //spriteBatch.DrawItemPopup(dialogBorder, titleLocation2, Color.Cyan);
            //spriteBatch.DrawItemPopup(dialogBorder, titleLocation3, Color.Cyan);
            //spriteBatch.DrawItemPopup(dialogBorder, titleLocation4, Color.Cyan);
        }

        public void HandleMouse(bool isActive)
        {
            /* LEFT MOUSE */
            if (display.Mouse.LeftHeld && isActive)
            {
                int mouseX = (int)Display.Mouse.X;
                int mouseY = (int)Display.Mouse.Y;
                Viewport viewport = GraphicsDevice.Viewport;
                int width = viewport.Width;
                int height = viewport.Height;

                //MAIN MENU BUTTONS
                foreach (MenuItem item in mainMenuItems)
                {
                    if (item.Type == MenuItemType.Button)
                    {
                        Rectangle itemLocation = new Rectangle((int)(item.Position.X) - ((80 - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X)) / 2), (int)(item.Position.Y) - 16, 80, 50);
                        //BUTTON ACTION                      
                        if ((mouseY > itemLocation.Y + 2) && (mouseY < (itemLocation.Y + itemLocation.Height - 9)))
                        {
                            if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                            {
                                switch (item.Text)
                                {
                                    default: break;

                                    //LOGIN
                                    case "Login":
                                        {
                                            if (Display.Mouse.LeftClick)
                                            {
                                                if (dialogBoxes[MenuDialogBoxType.Login].Visible)
                                                {
                                                    //Display.Keyboard.Dispatcher.Subscriber = null;
                                                    //dialogBoxes[DialogBoxType.Login].Hide();
                                                }
                                                else
                                                {
                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                                                    {
                                                        if (key.Key != MenuDialogBoxType.Login)
                                                        {
                                                            key.Value.Hide();
                                                        }
                                                    }
                                                    usernameBox.Text = null;
                                                    passwordBox.Text = null;
                                                    dialogBoxes[MenuDialogBoxType.Login].Show();
                                                    Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                                }
                                                //Display.Mouse.LeftClick = false;
                                            }
                                            break;
                                        }

                                    //NEW ACCOUNT
                                    case "Account":
                                        {
                                            if (display.Mouse.LeftClick)
                                            {
                                                if (dialogBoxes[MenuDialogBoxType.NewAccount].Visible)
                                                {
                                                    //Display.Keyboard.Dispatcher.Subscriber = null;
                                                    //dialogBoxes[DialogBoxType.NewAccount].Hide();
                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                                                    {
                                                        if (key.Key != MenuDialogBoxType.Login)
                                                        {
                                                            key.Value.Hide();
                                                        }
                                                    }
                                                    usernameBox.Text = null;
                                                    passwordBox.Text = null;
                                                    dialogBoxes[MenuDialogBoxType.Login].Show();
                                                    Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                                }
                                                else
                                                {
                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                                                    {
                                                        if (key.Key != MenuDialogBoxType.NewAccount)
                                                        {
                                                            key.Value.Hide();
                                                        }
                                                    }
                                                    accountBox.Text = null;
                                                    newPasswordBox.Text = null;
                                                    reTypePasswordBox.Text = null;
                                                    emailBox.Text = null;
                                                    questionBox.Text = null;
                                                    answerBox.Text = null;
                                                    dialogBoxes[MenuDialogBoxType.NewAccount].Show();
                                                    Display.Keyboard.Dispatcher.Subscriber = accountBox;
                                                }

                                                //Display.Mouse.LeftClick = false;
                                            }
                                            break;
                                        }

                                    //OPTIONS
                                    case "Options":
                                        {
                                            if (display.Mouse.LeftClick)
                                            {
                                                if (dialogBoxes[MenuDialogBoxType.Settings].Visible)
                                                {
                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                                                    {
                                                        if (key.Key != MenuDialogBoxType.Login)
                                                        {
                                                            key.Value.Hide();
                                                        }
                                                    }
                                                    usernameBox.Text = null;
                                                    passwordBox.Text = null;
                                                    dialogBoxes[MenuDialogBoxType.Login].Show();
                                                    Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                                    //dialogBoxes[DialogBoxType.Settings].Hide();
                                                }
                                                else
                                                {
                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                                                    {
                                                        if (key.Key != MenuDialogBoxType.Settings)
                                                        {
                                                            key.Value.Hide();
                                                        }
                                                    }
                                                    usernameBox.Text = null;
                                                    passwordBox.Text = null;
                                                    dialogBoxes[MenuDialogBoxType.Settings].Show();
                                                }

                                                //Display.Mouse.LeftClick = false;
                                            }
                                            break;
                                        }

                                    //EXIT
                                    case "Exit":
                                        {
                                            if (display.Mouse.LeftClick)
                                            {
                                                Exit(null, null); //TODO fix this
                                                //Display.Mouse.LeftClick = false;
                                            }
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }

                //DIALOG BOX VISIBLE
                foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                {
                    if (key.Value.Visible)
                    {
                        switch (key.Key)
                        {
                            default: break;
                            case MenuDialogBoxType.NewAccount:
                                {
                                    foreach (TextBox box in accountTextBoxes)
                                    {
                                        if (display.Mouse.X >= box.X && display.Mouse.X <= box.X + box.Width &&
                                            display.Mouse.Y >= box.Y & display.Mouse.Y <= box.Y + box.Height)
                                        {
                                            display.Keyboard.Dispatcher.Subscriber = box;
                                            //Display.Mouse.LeftClick = false;
                                        }
                                    }

                                    foreach (MenuItem item in accountItems)
                                    {
                                        Rectangle itemLocation = new Rectangle((int)(item.Position.X) - ((80 - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X)) / 2), (int)(item.Position.Y) - 16, 80, 50);
                                        //BUTTON ACTION                            
                                        if ((mouseY > itemLocation.Y) && (mouseY < (itemLocation.Y + itemLocation.Height - 6)))
                                        {
                                            if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                            {
                                                switch (item.Text)
                                                {
                                                    default: break;
                                                    case "Cancel":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                display.Keyboard.Dispatcher.Subscriber = null;
                                                                //dialogBoxes[DialogBoxType.NewAccount].Hide();
                                                                foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> dialogbox in dialogBoxes)
                                                                {
                                                                    if (dialogbox.Key != MenuDialogBoxType.Login)
                                                                    {
                                                                        key.Value.Hide();
                                                                    }
                                                                }
                                                                usernameBox.Text = null;
                                                                passwordBox.Text = null;
                                                                dialogBoxes[MenuDialogBoxType.Login].Show();
                                                                Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }

                                                    case "Clear":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                accountBox.Text = null;
                                                                newPasswordBox.Text = null;
                                                                reTypePasswordBox.Text = null;
                                                                emailBox.Text = null;
                                                                questionBox.Text = null;
                                                                answerBox.Text = null;
                                                                display.Keyboard.Dispatcher.Subscriber = accountBox;
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }

                                                    case "Create":
                                                        if (Display.Mouse.LeftClick)
                                                        {
                                                            account = accountBox.Text;
                                                            newPassword = newPasswordBox.Text;
                                                            reTypePassword = reTypePasswordBox.Text;
                                                            email = emailBox.Text;
                                                            question = questionBox.Text;
                                                            answer = answerBox.Text;
                                                            NewAccount();

                                                            //Display.Mouse.LeftClick = false;
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }

                            case MenuDialogBoxType.Login:
                                {
                                    foreach (TextBox box in loginTextBoxes)
                                    {
                                        if (display.Mouse.X >= box.X && display.Mouse.X <= box.X + box.Width &&
                                            display.Mouse.Y >= box.Y & display.Mouse.Y <= box.Y + box.Height)
                                        {
                                            display.Keyboard.Dispatcher.Subscriber = box;
                                            //Display.Mouse.LeftClick = false;
                                        }
                                    }

                                    foreach (MenuItem item in loginItems)
                                    {
                                        Rectangle itemLocation = new Rectangle((int)(item.Position.X) - 34, (int)(item.Position.Y) - 12, (int)(130), (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y) + 18);
                                        //BUTTON ACTION                      
                                        if ((mouseY > itemLocation.Y) && (mouseY < (itemLocation.Y + itemLocation.Height - 6)))
                                        {
                                            if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                            {
                                                switch (item.Text)
                                                {
                                                    default: break;
                                                    case "Connect":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                username = usernameBox.Text;
                                                                password = passwordBox.Text;
                                                                Login();
                                                                usernameBox.Text = null;
                                                                passwordBox.Text = null;
                                                                Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }
                            case MenuDialogBoxType.Settings:
                                {
                                    foreach (MenuItem item in settingsNormal)
                                    {
                                        if (item.Type == MenuItemType.ScrollBar)
                                        {
                                            if ((mouseY > item.Position.Y - 4) && (mouseY < (item.Position.Y + item.Height + 4)))
                                            {
                                                if ((mouseX > item.Position.X) && (mouseX < (item.Position.X + item.Width + 8)))
                                                {
                                                    if (Display.Mouse.LeftClick)
                                                    {
                                                        settingsScrollcounter = (int)(MathHelper.Clamp((float)(mouseY - item.Position.Y) / (float)(9.59), 0, 21)); //TODO almost working
                                                        foreach (MenuItem scrollItem in settingsNormal)
                                                        {
                                                            if (scrollItem.IsScrollable)
                                                            {
                                                                scrollItem.ScrollPosition = -settingsScrollcounter * 10;
                                                            }
                                                        }
                                                        //Display.Mouse.LeftClick = false;
                                                    }
                                                    else if (!Display.Mouse.LeftClick && Display.Mouse.IsDragging)
                                                    {
                                                        settingsScrollcounter = (int)(MathHelper.Clamp((float)(mouseY - item.Position.Y) / (float)(9.59), 0, 21)); //TODO almost working
                                                        foreach (MenuItem scrollItem in settingsNormal)
                                                        {
                                                            if (scrollItem.IsScrollable)
                                                            {
                                                                scrollItem.ScrollPosition = -settingsScrollcounter * 10;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (item.Type == MenuItemType.SliderBar)
                                        {
                                            if ((mouseY > item.Position.Y + item.ScrollPosition - 12) && (mouseY < (item.Position.Y + item.ScrollPosition + item.Height + 8)))
                                            {
                                                if ((mouseX > item.Position.X - 4) && (mouseX < (item.Position.X + item.Width + 4)))
                                                {
                                                    if (Display.Mouse.LeftClick && item.SettingType == SettingsType.Music)
                                                    { 
                                                        Cache.GameSettings.MusicVolume = MathHelper.Clamp(mouseX - (item.Position.X), 0, 100);
                                                        //display.Mouse.LeftClick = false;
                                                    }
                                                    else if(!Display.Mouse.LeftClick && item.SettingType == SettingsType.Music)
                                                    {
                                                        Cache.GameSettings.MusicVolume = MathHelper.Clamp(mouseX - (item.Position.X), 0, 100);
                                                    }

                                                    if (Display.Mouse.LeftClick && item.SettingType == SettingsType.Sound)
                                                    { 
                                                        Cache.GameSettings.SoundVolume = MathHelper.Clamp(mouseX - (item.Position.X), 0, 100);
                                                        //display.Mouse.LeftClick = false; 
                                                    }
                                                    else if (!Display.Mouse.LeftClick && item.SettingType == SettingsType.Sound)
                                                    {
                                                        Cache.GameSettings.SoundVolume = MathHelper.Clamp(mouseX - (item.Position.X), 0, 100);
                                                    }
                                                }
                                            }
                                        }
                                        if (item.Type == MenuItemType.SmallButton)
                                        {
                                            if ((mouseY > item.Position.Y + item.ScrollPosition) && (mouseY < (item.Position.Y + item.ScrollPosition + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y))))
                                            {
                                                if ((mouseX > item.Position.X) && (mouseX < (item.Position.X + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X))))
                                                {
                                                    switch (item.Text)
                                                    {
                                                        default: break;
                                                        case "Low":
                                                            {
                                                                if (Display.Mouse.LeftClick && Cache.GameSettings.DetailMode != GraphicsDetail.Low)
                                                                {
                                                                    Cache.GameSettings.DetailMode = GraphicsDetail.Low;
                                                                    //display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }
                                                        case "Medium":
                                                            {
                                                                if (Display.Mouse.LeftClick && Cache.GameSettings.DetailMode != GraphicsDetail.Medium)
                                                                {
                                                                    Cache.GameSettings.DetailMode = GraphicsDetail.Medium;
                                                                    //display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }
                                                        case "High":
                                                            {
                                                                if (Display.Mouse.LeftClick && Cache.GameSettings.DetailMode != GraphicsDetail.High)
                                                                {
                                                                    Cache.GameSettings.DetailMode = GraphicsDetail.High;
                                                                    //display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }
                                                        case "On":
                                                            {
                                                                if (Display.Mouse.LeftClick && !Cache.GameSettings.MusicOn && item.SettingType == SettingsType.Music) { Cache.GameSettings.MusicOn = true; }
                                                                if (Display.Mouse.LeftClick && !Cache.GameSettings.SoundsOn && item.SettingType == SettingsType.Sound) { Cache.GameSettings.SoundsOn = true; }
                                                                if (Display.Mouse.LeftClick && !Cache.GameSettings.TransparentDialogs && item.SettingType == SettingsType.Transparency) { Cache.GameSettings.TransparentDialogs = true; }
                                                                //if (Display.Mouse.LeftClickCheck && !Cache.GameState.DialogBoxes[DialogBoxType.MiniMap].Visible && item.SettingType == SettingsType.MiniMap)
                                                                //{
                                                                //    Cache.GameState.DialogBoxes[DialogBoxType.MiniMap].Show();
                                                                //    Cache.GameState.BringToFront(DialogBoxType.MiniMap);
                                                                //}
                                                                //TODO Wisper/Shout
                                                                //Display.Mouse.LeftClick = false;
                                                                break;
                                                            }
                                                        case "Off":
                                                            {
                                                                if (Display.Mouse.LeftClick && Cache.GameSettings.MusicOn && item.SettingType == SettingsType.Music) { Cache.GameSettings.MusicOn = false; }
                                                                if (Display.Mouse.LeftClick && Cache.GameSettings.SoundsOn && item.SettingType == SettingsType.Sound) { Cache.GameSettings.SoundsOn = false; }
                                                                if (Display.Mouse.LeftClick && Cache.GameSettings.TransparentDialogs && item.SettingType == SettingsType.Transparency) { Cache.GameSettings.TransparentDialogs = false; }
                                                                //if (Display.Mouse.LeftClickCheck && Cache.GameState.DialogBoxes[DialogBoxType.MiniMap].Visible && item.SettingType == SettingsType.MiniMap) { Cache.GameState.DialogBoxes[DialogBoxType.MiniMap].Hide(); }
                                                                //TODO Wisper/Shout
                                                                //Display.Mouse.LeftClick = false;
                                                                break;
                                                            }
                                                        case "640 x 480":
                                                            {
                                                                if (Display.Mouse.LeftClick && Display.Resolution != Resolution.Classic)
                                                                {
                                                                    Display.Resolution = Resolution.Classic;
                                                                    this.resolutionChange = true;
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }
                                                        case "800 x 600":
                                                            {
                                                                if (Display.Mouse.LeftClick && Display.Resolution != Resolution.Standard)
                                                                {
                                                                    Display.Resolution = Resolution.Standard;
                                                                    this.resolutionChange = true;
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }
                                                        case "1024 x 768":
                                                            {
                                                                if (Display.Mouse.LeftClick && Display.Resolution != Resolution.Large)
                                                                {
                                                                    Display.Resolution = Resolution.Large;
                                                                    this.resolutionChange = true;
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }
                                                        case "Windowed":
                                                            {
                                                                if (Display.Mouse.LeftClick && Display.DeviceManager.IsFullScreen)
                                                                {
                                                                    Display.ToggleFullScreen();
                                                                    Cache.GameSettings.FullScreen = display.DeviceManager.IsFullScreen;
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }

                                                        case "Fullscreen":
                                                            {
                                                                if (Display.Mouse.LeftClick && !Display.DeviceManager.IsFullScreen)
                                                                {
                                                                    Display.ToggleFullScreen();
                                                                    Cache.GameSettings.FullScreen = Display.DeviceManager.IsFullScreen;
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }
                                                        case "Restore":
                                                            {
                                                                if (Display.Mouse.LeftClick)
                                                                {
                                                                    string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\gamesettings.xml";
                                                                    Cache.GameSettings = GameSettings.Default();
                                                                    Display.Resolution = Cache.GameSettings.Resolution;
                                                                    Display.DeviceManager.IsFullScreen = Cache.GameSettings.FullScreen;
                                                                    this.resolutionChange = true;
                                                                    ConfigurationHelper.SaveGameSettings(configPath);
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }
                                                    }
                                                }
                                            }
                                        }
                                        if (item.Type == MenuItemType.Button)
                                        {
                                            Rectangle itemLocation = new Rectangle((int)(item.Position.X) - ((90 - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X)) / 2), (int)(item.Position.Y) - 16, 90, 50);
                                            //BUTTON ACTION                      
                                            if ((mouseY > itemLocation.Y + 7) && (mouseY < (itemLocation.Y + itemLocation.Height - 8)))
                                            {
                                                if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                                {
                                                    switch (item.Text)
                                                    {
                                                        default: break;
                                                        case "Settings":
                                                            {
                                                                if (Display.Mouse.LeftClick)
                                                                {
                                                                    if (!dialogBoxes[MenuDialogBoxType.Settings].Visible)
                                                                    {
                                                                        foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                                        {
                                                                            if (box.Key != MenuDialogBoxType.Settings)
                                                                            {
                                                                                box.Value.Hide();
                                                                            }
                                                                        }
                                                                        dialogBoxes[MenuDialogBoxType.Settings].Show();
                                                                    }
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }

                                                        case "Keyboard":
                                                            {
                                                                if (Display.Mouse.LeftClick)
                                                                {
                                                                    if (!dialogBoxes[MenuDialogBoxType.Keyboard].Visible)
                                                                    {
                                                                        foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                                        {
                                                                            if (box.Key != MenuDialogBoxType.Keyboard)
                                                                            {
                                                                                box.Value.Hide();
                                                                            }
                                                                        }
                                                                        dialogBoxes[MenuDialogBoxType.Keyboard].Show();
                                                                    }
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }

                                                        case "Advanced":
                                                            {
                                                                if (Display.Mouse.LeftClick)
                                                                {
                                                                    if (!dialogBoxes[MenuDialogBoxType.Advanced].Visible)
                                                                    {
                                                                        foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                                        {
                                                                            if (box.Key != MenuDialogBoxType.Advanced)
                                                                            {
                                                                                box.Value.Hide();
                                                                            }
                                                                        }
                                                                        dialogBoxes[MenuDialogBoxType.Advanced].Show();
                                                                    }
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }

                                                        case "Cancel":
                                                            {
                                                                if (Display.Mouse.LeftClick)
                                                                {
                                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> dialogbox in dialogBoxes)
                                                                    {
                                                                        if (dialogbox.Key != MenuDialogBoxType.Login)
                                                                        {
                                                                            key.Value.Hide();
                                                                        }
                                                                    }
                                                                    usernameBox.Text = null;
                                                                    passwordBox.Text = null;
                                                                    dialogBoxes[MenuDialogBoxType.Login].Show();
                                                                    Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                                                    //dialogBoxes[DialogBoxType.Settings].Hide();
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }

                                                        case "Save":
                                                            {
                                                                if (Display.Mouse.LeftClick)
                                                                {
                                                                    string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\gamesettings.xml";
                                                                    ConfigurationHelper.SaveGameSettings(configPath);
                                                                    settingsSaved = true;
                                                                    //Display.Mouse.LeftClick = false;
                                                                }
                                                                break;
                                                            }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }
                            case MenuDialogBoxType.Keyboard:
                                {
                                    bool exit = false;
                                    for (int i = 0; i < hotKeyMenuItems.Count; i++)
                                    {
                                        HotKeyMenuItem item = hotKeyMenuItems[i];
                                        //display.Mouse.LeftClick = item.HandleMouse(display.Mouse.LeftClick);
                                        if (item.Selected) { exit = true; break; } 
                                    }
                                    if (exit) { break; }

                                    foreach (MenuItem item in settingsKeyboard)
                                    {
                                        if (item.Type == MenuItemType.ScrollBar)
                                        {
                                            if ((mouseY > item.Position.Y - 4) && (mouseY < (item.Position.Y + item.Height + 4)))
                                            {
                                                if ((mouseX > item.Position.X) && (mouseX < (item.Position.X + item.Width + 8)))
                                                {
                                                    if (Display.Mouse.LeftClick)
                                                    {
                                                        keyboardSettingsScrollCounter = (int)(MathHelper.Clamp((float)(mouseY - item.Position.Y) / ((float)(item.Height) / (float)(hotKeyMenuItems.Count)), 0, hotKeyMenuItems.Count - 3));
                                                        foreach (HotKeyMenuItem scrollItem in hotKeyMenuItems)
                                                        {
                                                            scrollItem.Scroll(keyboardSettingsScrollCounter);
                                                        }
                                                        //Display.Mouse.LeftClick = false;
                                                    }
                                                    else if (!Display.Mouse.LeftClick && Display.Mouse.IsDragging)
                                                    {
                                                        keyboardSettingsScrollCounter = (int)(MathHelper.Clamp((float)(mouseY - item.Position.Y) / ((float)(item.Height) / (float)(hotKeyMenuItems.Count)), 0, hotKeyMenuItems.Count - 3));
                                                        foreach (HotKeyMenuItem scrollItem in hotKeyMenuItems)
                                                        {
                                                            scrollItem.Scroll(keyboardSettingsScrollCounter);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        Rectangle itemLocation = new Rectangle((int)(item.Position.X) - ((90 - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X)) / 2), (int)(item.Position.Y) - 16, 90, 50);
                                        //BUTTON ACTION                      
                                        if ((mouseY > itemLocation.Y + 7) && (mouseY < (itemLocation.Y + itemLocation.Height - 8)))
                                        {
                                            if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                            {
                                                switch (item.Text)
                                                {
                                                    default: break;

                                                    case "Settings":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                if (!dialogBoxes[MenuDialogBoxType.Settings].Visible)
                                                                {
                                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                                    {
                                                                        if (box.Key != MenuDialogBoxType.Settings)
                                                                        {
                                                                            box.Value.Hide();
                                                                        }
                                                                    }
                                                                    dialogBoxes[MenuDialogBoxType.Settings].Show();
                                                                }
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }

                                                    case "Keyboard":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                if (!dialogBoxes[MenuDialogBoxType.Keyboard].Visible)
                                                                {
                                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                                    {
                                                                        if (box.Key != MenuDialogBoxType.Keyboard)
                                                                        {
                                                                            box.Value.Hide();
                                                                        }
                                                                    }
                                                                    dialogBoxes[MenuDialogBoxType.Keyboard].Show();
                                                                }
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }

                                                    case "Advanced":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                if (!dialogBoxes[MenuDialogBoxType.Advanced].Visible)
                                                                {
                                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                                    {
                                                                        if (box.Key != MenuDialogBoxType.Advanced)
                                                                        {
                                                                            box.Value.Hide();
                                                                        }
                                                                    }
                                                                    dialogBoxes[MenuDialogBoxType.Advanced].Show();
                                                                }
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }

                                                    case "Cancel":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> dialogbox in dialogBoxes)
                                                                {
                                                                    if (dialogbox.Key != MenuDialogBoxType.Login)
                                                                    {
                                                                        key.Value.Hide();
                                                                    }
                                                                }
                                                                usernameBox.Text = null;
                                                                passwordBox.Text = null;
                                                                dialogBoxes[MenuDialogBoxType.Login].Show();
                                                                Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                                                //dialogBoxes[DialogBoxType.Keyboard].Hide();
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }

                                                    case "Apply":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                //TODO - ADD SAVE CHANGES FUNCTION + CLOSE DIALOG BOX AFTER SAVED. MAYBE SHOW PROMPT SCREEN
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }
                            case MenuDialogBoxType.Advanced:
                                {
                                    for (int i = 0; i < advancedSettingsMenuItems.Count; i++)
                                    {
                                        advancedSettingsMenuItems[i].HandleMouse(display.Mouse.LeftClick);
                                        if (!display.Mouse.LeftClick) { break; }
                                    }

                                    foreach (MenuItem item in settingsAdvanced)
                                    {
                                        if (item.Type == MenuItemType.ScrollBar)
                                        {
                                            if ((mouseY > item.Position.Y - 4) && (mouseY < (item.Position.Y + item.Height + 4)))
                                            {
                                                if ((mouseX > item.Position.X) && (mouseX < (item.Position.X + item.Width + 8)))
                                                {
                                                    if (Display.Mouse.LeftClick)
                                                    {
                                                        advancedSettingsScrollCounter = (int)(MathHelper.Clamp((float)(((float)mouseY - item.Position.Y) / advancedSettingsScrollPixels), 0, advancedSettingsMaxScrollCount)); 
                                                        //Display.Mouse.LeftClick = false;
                                                    }

                                                    if (!Display.Mouse.LeftClick && Display.Mouse.IsDragging)
                                                    {
                                                        advancedSettingsScrollCounter = (int)(MathHelper.Clamp((float)(((float)mouseY - item.Position.Y) / advancedSettingsScrollPixels), 0, advancedSettingsMaxScrollCount));    
                                                    }
                                                }
                                            }
                                        }
                                        Rectangle itemLocation = new Rectangle((int)(item.Position.X) - ((90 - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X)) / 2), (int)(item.Position.Y) - 16, 90, 50);
                                        //BUTTON ACTION                      
                                        if ((mouseY > itemLocation.Y + 7) && (mouseY < (itemLocation.Y + itemLocation.Height - 8)))
                                        {
                                            if ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2)))
                                            {
                                                switch (item.Text)
                                                {
                                                    default: break;

                                                    case "Settings":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                if (!dialogBoxes[MenuDialogBoxType.Settings].Visible)
                                                                {
                                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                                    {
                                                                        if (box.Key != MenuDialogBoxType.Settings)
                                                                        {
                                                                            box.Value.Hide();
                                                                        }
                                                                    }
                                                                    dialogBoxes[MenuDialogBoxType.Settings].Show();
                                                                }
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }

                                                    case "Keyboard":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                if (!dialogBoxes[MenuDialogBoxType.Keyboard].Visible)
                                                                {
                                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                                    {
                                                                        if (box.Key != MenuDialogBoxType.Keyboard)
                                                                        {
                                                                            box.Value.Hide();
                                                                        }
                                                                    }
                                                                    dialogBoxes[MenuDialogBoxType.Keyboard].Show();
                                                                }
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }

                                                    case "Advanced":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                if (!dialogBoxes[MenuDialogBoxType.Advanced].Visible)
                                                                {
                                                                    foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                                    {
                                                                        if (box.Key != MenuDialogBoxType.Advanced)
                                                                        {
                                                                            box.Value.Hide();
                                                                        }
                                                                    }
                                                                    dialogBoxes[MenuDialogBoxType.Advanced].Show();
                                                                }
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }

                                                    case "Cancel":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> dialogbox in dialogBoxes)
                                                                {
                                                                    if (dialogbox.Key != MenuDialogBoxType.Login)
                                                                    {
                                                                        key.Value.Hide();
                                                                    }
                                                                }
                                                                usernameBox.Text = null;
                                                                passwordBox.Text = null;
                                                                dialogBoxes[MenuDialogBoxType.Login].Show();
                                                                Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                                                //dialogBoxes[DialogBoxType.Advanced].Hide();
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }

                                                    case "Save":
                                                        {
                                                            if (Display.Mouse.LeftClick)
                                                            {
                                                                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\gamesettings.xml";
                                                                ConfigurationHelper.SaveGameSettings(configPath);
                                                                settingsSaved = true;
                                                                //Display.Mouse.LeftClick = false;
                                                            }
                                                            break;
                                                        }
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }
                        }
                    }
                }

                /* DIALOG CLICK */
                if (clickedDialogBox != MenuDialogBoxType.None)
                {
                    /* DRAGGING CLICKED DIALOG BOX */
                    if (display.Mouse.IsDragging)
                    {
                        // can only move "Moveable" dialogs, unless LockedDialogs is false and we aren't trying to move the padlock (firm bottom right)
                        //if ((dialogBoxes[clickedDialogBox].Moveable || (!Cache.GameSettings.LockedDialogs && clickedDialogBox != MenuDialogBoxType.LockIconPanel)) &&
                        //    display.Mouse.X >= 0 && display.Mouse.X <= display.ResolutionWidth &&
                        //    display.Mouse.Y >= 0 && display.Mouse.Y <= display.ResolutionHeight)
                        //{
                        //    /* DRAGGING OBJECT ON DIALOG BOX */
                        //    //if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1)
                        //    //{
                        //    //    switch (clickedDialogBox)
                        //    //    {
                        //    //        case DialogBoxType.CharacterV1:
                        //    //            if (player.InventoryV1[dialogBoxes[clickedDialogBox].ClickedItemIndex] != null)
                        //    //            {
                        //    //                dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetX += ((int)display.Mouse.X - (int)display.Mouse.DragX);
                        //    //                dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetY += ((int)display.Mouse.Y - (int)display.Mouse.DragY);
                        //    //            }
                        //    //            break;
                        //    //        case DialogBoxType.InventoryV1:
                        //    //            if (player.InventoryV1[dialogBoxes[clickedDialogBox].ClickedItemIndex] != null)
                        //    //            {
                        //    //                Item item = player.InventoryV1[dialogBoxes[clickedDialogBox].ClickedItemIndex];
                        //    //                item.BagX += ((int)display.Mouse.X - (int)display.Mouse.DragX);
                        //    //                item.BagY += ((int)display.Mouse.Y - (int)display.Mouse.DragY);
                        //    //            }
                        //    //            break;
                        //    //    }
                        //    //}
                        //    /* DRAGGING THE DIALOG BOX ITSELF */
                        //    //else
                        //    //{
                        //    //dialogBoxes[clickedDialogBox].X += ((int)display.Mouse.X - (int)display.Mouse.DragX);
                        //    //dialogBoxes[clickedDialogBox].Y += ((int)display.Mouse.Y - (int)display.Mouse.DragY);
                        //    //}

                        //    //display.Mouse.DragX = (int)display.Mouse.X;
                        //    //display.Mouse.DragY = (int)display.Mouse.Y;
                        //}
                    }
                }
                /* DIALOG HIGHLIGHT */
                else if (highlightedDialogBox != MenuDialogBoxType.None)
                {
                    if (!Display.Mouse.IsDragging)
                    {
                        clickedDialogBox = highlightedDialogBox;
                        //if (clickedDialogBox != MenuDialogBoxType.IconPanelV1)
                        //{
                        //    BringToFront(clickedDialogBox);
                        //}

                        if (dialogBoxes[clickedDialogBox].SelectedItemIndex != -1)
                        {
                            //Item item = player.InventoryV1[dialogBoxes[clickedDialogBox].SelectedItemIndex];
                            //item.BagOldX = item.BagX;
                            //item.BagOldY = item.BagY;
                        }
                    }
                }
            }
            //RIGHT MOUSE
            else if (Display.Mouse.RightHeld && isActive)
            {
                //bool idle = false;
                if (highlightedDialogBox != MenuDialogBoxType.None && Display.Mouse.RightClick)
                {
                    dialogBoxes[(MenuDialogBoxType)highlightedDialogBox].Hide();
                    //Display.Mouse.RightClick = false;
                }
                //Display.Mouse.RightHeld = false;
            }

            if (!Display.Mouse.LeftHeld && Display.Mouse.LeftClick && isActive)
            {
                /* SINGLE CLICK */
                if (highlightedDialogBox != MenuDialogBoxType.None)
                    dialogBoxes[highlightedDialogBox].Click();

                if (clickedDialogBox != MenuDialogBoxType.None)
                {
                    /* DOUBLE CLICK */
                    //if (!Display.Mouse.IsDragging)
                        //if ((DateTime.Now - Display.Mouse.LeftClickTime).TotalMilliseconds < 300)
                            //switch (clickedDialogBox)
                            //{
                            //    case MenuDialogBoxType.CharacterV1:
                            //    case MenuDialogBoxType.InventoryV1:
                            //        //if (dialogBoxes[clickedDialogBox].SelectedItemIndex != -1)
                            //        //    //UseItem(dialogBoxes[clickedDialogBox].SelectedItemIndex);
                            //        break;
                            //}
                        //else Display.Mouse.LeftClickTime = DateTime.Now;

                    /* DROP THE DRAGGING ITEM */
                    if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1)
                    {
                        switch (clickedDialogBox)
                        {
                            //case MenuDialogBoxType.InventoryV1:
                            //    switch (highlightedDialogBox)
                            //    {
                            //        case MenuDialogBoxType.InventoryV1:
                            //            //SetBagLocation(dialogBoxes[clickedDialogBox].ClickedItemIndex);
                            //            break;
                            //        case MenuDialogBoxType.CharacterV1:
                            //            //Item item = player.InventoryV1[dialogBoxes[clickedDialogBox].ClickedItemIndex];
                            //            //item.BagX = player.InventoryV1[dialogBoxes[clickedDialogBox].ClickedItemIndex].BagOldX;
                            //            //item.BagY = player.InventoryV1[dialogBoxes[clickedDialogBox].ClickedItemIndex].BagOldY;
                            //            //EquipItem(dialogBoxes[clickedDialogBox].ClickedItemIndex);
                            //            break;
                            //        case MenuDialogBoxType.None:
                            //            //item = player.InventoryV1[dialogBoxes[clickedDialogBox].ClickedItemIndex];
                            //            //item.BagX = player.InventoryV1[dialogBoxes[clickedDialogBox].ClickedItemIndex].BagOldX;
                            //            //item.BagY = player.InventoryV1[dialogBoxes[clickedDialogBox].ClickedItemIndex].BagOldY;
                            //            //DropItem(dialogBoxes[clickedDialogBox].ClickedItemIndex);
                            //            break;
                            //    }
                            //    break;
                            //case MenuDialogBoxType.CharacterV1:
                            //    switch (highlightedDialogBox)
                            //    {
                            //        case MenuDialogBoxType.InventoryV1:
                            //            //UnEquipItem(dialogBoxes[clickedDialogBox].ClickedItemIndex);
                            //            break;
                            //        case MenuDialogBoxType.None:
                            //            //DropItem(dialogBoxes[clickedDialogBox].ClickedItemIndex);
                            //            break;
                            //    }
                            //    dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetX = 0;
                            //    dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetY = 0;
                            //    break;
                        }

                        dialogBoxes[clickedDialogBox].ClickedItemIndex = -1;
                    }

                    clickedDialogBox = MenuDialogBoxType.None;
                }
                //Display.Mouse.LeftClick = false;
            }


            /* MOUSE SCROLL */
            if (Display.Mouse.Scroll != 0 && isActive)
            {
                foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                {
                    if (key.Value.Visible)
                    {
                        switch (key.Key)
                        {
                            default: break;
                            case MenuDialogBoxType.Advanced:
                                {
                                    if (display.Mouse.Scroll == -1 && advancedSettingsScrollCounter + 1 <= advancedSettingsMaxScrollCount && advancedSettingsMaxScroll > 0)
                                    {
                                        advancedSettingsScrollCounter = advancedSettingsScrollCounter + 1;
                                    }
                                    else if (display.Mouse.Scroll == 1 && advancedSettingsScrollCounter - 1 > -1 && advancedSettingsMaxScroll > 0)
                                    {
                                        advancedSettingsScrollCounter = advancedSettingsScrollCounter - 1;
                                    }
                                    break;
                                }
                            case MenuDialogBoxType.Keyboard:
                                {                                    
                                    bool scrollHotKeyMenuItem = false;
                                    foreach (HotKeyMenuItem hotkeymenuitem in hotKeyMenuItems)
                                    {
                                        if (hotkeymenuitem.State == MenuItemState.Action)
                                        {
                                            scrollHotKeyMenuItem = true;
                                            hotkeymenuitem.ScrollAction();
                                            break;
                                        }
                                    }

                                    if (!scrollHotKeyMenuItem)
                                    {
                                        if (display.Mouse.Scroll == -1 && keyboardSettingsScrollCounter < hotKeyMenuItems.Count - 3)
                                        {
                                            keyboardSettingsScrollCounter = keyboardSettingsScrollCounter + 1;
                                            foreach (HotKeyMenuItem item in hotKeyMenuItems)
                                            {
                                                item.Scroll(keyboardSettingsScrollCounter);
                                            }
                                        }
                                        else if (display.Mouse.Scroll == 1 && keyboardSettingsScrollCounter > 0)
                                        {
                                            keyboardSettingsScrollCounter = keyboardSettingsScrollCounter - 1;
                                            foreach (HotKeyMenuItem item in hotKeyMenuItems)
                                            {
                                                item.Scroll(keyboardSettingsScrollCounter);
                                            }
                                        }
                                    }
                                    break;
                                }
                            case MenuDialogBoxType.Settings:
                                {
                                    if (display.Mouse.Scroll == -1 && settingsScrollcounter + 1 < 22)
                                    {
                                        foreach (MenuItem item in settingsNormal)
                                        {
                                            if (item.IsScrollable)
                                            {
                                                item.ScrollPosition = item.ScrollPosition + display.Mouse.Scroll * 10;
                                            }
                                        }
                                        settingsScrollcounter = settingsScrollcounter + 1;
                                    }
                                    else if (display.Mouse.Scroll == 1 && settingsScrollcounter - 1 >= 0)
                                    {
                                        foreach (MenuItem item in settingsNormal)
                                        {
                                            if (item.IsScrollable)
                                            {
                                                item.ScrollPosition = item.ScrollPosition + display.Mouse.Scroll * 10;
                                            }
                                        }
                                        settingsScrollcounter = settingsScrollcounter - 1;
                                    }                                  
                                    break;
                                }
                        }
                    }
                }
            }
        }


       private void HandleKeyboard(bool isActive)
       {
           if (isActive)
           {
               foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
               {
                   if (key.Value.Visible)
                   {
                       switch (key.Key)
                       {                                            
                           default: break;
                           case MenuDialogBoxType.Advanced: break;
                           case MenuDialogBoxType.Keyboard:
                               {
                                   for (int i = 0; i < hotKeyMenuItems.Count; i++)
                                   {
                                       HotKeyMenuItem item = hotKeyMenuItems[i];
                                       item.HandleKeyboard();
                                   }
                                   break;
                               }
                           case MenuDialogBoxType.Settings:
                               {
                                   /* PRESS ESC */
                                   if (Display.Keyboard.Esc) Display.Keyboard.KeyCheck[Keys.Escape] = true;
                                   if (!Display.Keyboard.Esc && Display.Keyboard.KeyCheck[Keys.Escape])
                                   {
                                       key.Value.Hide();
                                       Display.Keyboard.Dispatcher.Subscriber = null;
                                       Display.Keyboard.KeyCheck[Keys.Escape] = false;
                                       dialogBoxes[MenuDialogBoxType.Login].Show();
                                       Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                   }
                                   break;
                               }
                           case MenuDialogBoxType.NewAccount:
                               {
                                   /* PRESS ESC */
                                   if (Display.Keyboard.Esc) Display.Keyboard.KeyCheck[Keys.Escape] = true;
                                   if (!Display.Keyboard.Esc && Display.Keyboard.KeyCheck[Keys.Escape])
                                   {
                                       accountBox.Text = null;
                                       newPasswordBox.Text = null;
                                       reTypePasswordBox.Text = null;
                                       emailBox.Text = null;
                                       questionBox.Text = null;
                                       answerBox.Text = null;
                                       key.Value.Hide();
                                       Display.Keyboard.Dispatcher.Subscriber = null;
                                       Display.Keyboard.KeyCheck[Keys.Escape] = false;
                                       dialogBoxes[MenuDialogBoxType.Login].Show();
                                       Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                   }

                                   /* PRESS TAB */
                                   if (Display.Keyboard.Tab) Display.Keyboard.KeyCheck[Keys.Tab] = true;
                                   if (!Display.Keyboard.Tab && Display.Keyboard.KeyCheck[Keys.Tab])
                                   {
                                       if (Display.Keyboard.Dispatcher.Subscriber == accountBox) { Display.Keyboard.Dispatcher.Subscriber = newPasswordBox; }
                                       else if (Display.Keyboard.Dispatcher.Subscriber == newPasswordBox) { Display.Keyboard.Dispatcher.Subscriber = reTypePasswordBox; }
                                       else if (Display.Keyboard.Dispatcher.Subscriber == reTypePasswordBox) { Display.Keyboard.Dispatcher.Subscriber = emailBox; }
                                       else if (Display.Keyboard.Dispatcher.Subscriber == emailBox) { Display.Keyboard.Dispatcher.Subscriber = questionBox; }
                                       else if (Display.Keyboard.Dispatcher.Subscriber == questionBox) { Display.Keyboard.Dispatcher.Subscriber = answerBox; }
                                       else if (Display.Keyboard.Dispatcher.Subscriber == answerBox) { Display.Keyboard.Dispatcher.Subscriber = accountBox; }
         
                                       Display.Keyboard.KeyCheck[Keys.Tab] = false;
                                   }

                                   /* PRESS ENTER */
                                   if (Display.Keyboard.Enter) Display.Keyboard.KeyCheck[Keys.Enter] = true;
                                   if (!Display.Keyboard.Enter && Display.Keyboard.KeyCheck[Keys.Enter])
                                   {
                                       if (Display.Keyboard.Dispatcher.Subscriber == answerBox)
                                       {
                                           account = accountBox.Text;
                                           newPassword = newPasswordBox.Text;
                                           reTypePassword = reTypePasswordBox.Text;
                                           email = emailBox.Text;
                                           question = questionBox.Text;
                                           answer = answerBox.Text;
                                           NewAccount(); 
                                           accountBox.Text = null;
                                           newPasswordBox.Text = null;
                                           reTypePasswordBox.Text = null;
                                           emailBox.Text = null;
                                           questionBox.Text = null;
                                           answerBox.Text = null;

                                           //SHOW ERROR MESSAGE (badpassword/already used account ect) and do work
                                           key.Value.Hide();
                                           //Display.Keyboard.Dispatcher.Subscriber = accountBox; 
                                       }
                                       else
                                       {
                                           if (Display.Keyboard.Dispatcher.Subscriber == accountBox) { Display.Keyboard.Dispatcher.Subscriber = newPasswordBox; }
                                           else if (Display.Keyboard.Dispatcher.Subscriber == newPasswordBox) { Display.Keyboard.Dispatcher.Subscriber = reTypePasswordBox; }
                                           else if (Display.Keyboard.Dispatcher.Subscriber == reTypePasswordBox) { Display.Keyboard.Dispatcher.Subscriber = emailBox; }
                                           else if (Display.Keyboard.Dispatcher.Subscriber == emailBox) { Display.Keyboard.Dispatcher.Subscriber = questionBox; }
                                           else if (Display.Keyboard.Dispatcher.Subscriber == questionBox) { Display.Keyboard.Dispatcher.Subscriber = answerBox; }
                                       }

                                       Display.Keyboard.KeyCheck[Keys.Enter] = false;
                                   }
                                   break;
                               }
                           case MenuDialogBoxType.Login:
                               {
                                   /* PRESS TAB */
                                   if (Display.Keyboard.Tab) Display.Keyboard.KeyCheck[Keys.Tab] = true;
                                   if (!Display.Keyboard.Tab && Display.Keyboard.KeyCheck[Keys.Tab])
                                   {
                                       if (Display.Keyboard.Dispatcher.Subscriber == usernameBox)
                                           Display.Keyboard.Dispatcher.Subscriber = passwordBox;
                                       else Display.Keyboard.Dispatcher.Subscriber = usernameBox;

                                       Display.Keyboard.KeyCheck[Keys.Tab] = false;
                                   }

                                   /* PRESS ENTER */
                                   if (Display.Keyboard.Enter) Display.Keyboard.KeyCheck[Keys.Enter] = true;
                                   if (!Display.Keyboard.Enter && Display.Keyboard.KeyCheck[Keys.Enter])
                                   {
                                       if (Display.Keyboard.Dispatcher.Subscriber == usernameBox)
                                           Display.Keyboard.Dispatcher.Subscriber = passwordBox;
                                       else
                                       {
                                           username = usernameBox.Text;
                                           password = passwordBox.Text;
                                           Login();
                                           usernameBox.Text = null;
                                           passwordBox.Text = null;
                                           Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                       }

                                       Display.Keyboard.KeyCheck[Keys.Enter] = false;
                                   }

                                   /* PRESS ESC */
                                   if (Display.Keyboard.Esc) Display.Keyboard.KeyCheck[Keys.Escape] = true;
                                   if (!Display.Keyboard.Esc && Display.Keyboard.KeyCheck[Keys.Escape])
                                   {
                                       usernameBox.Text = null;
                                       passwordBox.Text = null;
                                       Display.Keyboard.Dispatcher.Subscriber = usernameBox;
                                       Display.Keyboard.KeyCheck[Keys.Escape] = false;
                                   }
                                   break;
                               }
                       }
                   }
               }
           }
       }

       private void NewAccount()
       {
           try
           {
               errorMessage = string.Empty;

               /* INITIALIZE LOGIN SERVER TCP CONNECTION */
               if (loginServer == null)
               {
                   Socket socket = Sockets.CreateTCPSocket(ipAddress, port);
                   loginServer = new ClientInfo(socket, true);
                   loginServer.EncryptionType = EncryptionType.None;
                   loginServer.OnReadBytes += new ConnectionReadBytes(ReadMessageFromLoginServer);
               }

               byte[] command = new byte[250];
               Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.CreateAccount), 0, command, 0, 4);
               Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Confirm), 0, command, 4, 2);
               Buffer.BlockCopy(account.GetBytes(10), 0, command, 6, 10);
               Buffer.BlockCopy(newPassword.GetBytes(10), 0, command, 16, 10);
               Buffer.BlockCopy(email.GetBytes(50), 0, command, 26, 50);
               Buffer.BlockCopy(question.GetBytes(45), 0, command, 149, 45);
               Buffer.BlockCopy(answer.GetBytes(20), 0, command, 194, 20);

               byte[] data = Utility.Encrypt(command, false);
               loginServer.Send(data);

               errorMessage = "Connecting...";
               errorMessageTimer = true;
           }
           catch
           {
               errorMessage = "Login Server is Unvailable.";
               errorMessageTimer = true;
           }
       }

        private void Login()
        {
            try
            {
                errorMessage = string.Empty;

                /* INITIALIZE LOGIN SERVER TCP CONNECTION */
                if (loginServer == null)
                {
                    Socket socket = Sockets.CreateTCPSocket(ipAddress, port);
                    loginServer = new ClientInfo(socket, true);
                    loginServer.EncryptionType = EncryptionType.None;
                    loginServer.OnReadBytes += new ConnectionReadBytes(ReadMessageFromLoginServer);
                }

                byte[] command = new byte[70];
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestLogin), 0, command, 0, 4);
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Confirm), 0, command, 4, 2);
                Buffer.BlockCopy(username.GetBytes(10), 0, command, 6, 10);
                Buffer.BlockCopy(password.GetBytes(10), 0, command, 16, 10);
                Buffer.BlockCopy("".GetBytes(30), 0, command, 26, 30);
                Buffer.BlockCopy(Assembly.GetExecutingAssembly().GetName().Version.ToString().GetBytes(10), 0, command, 56, 10);

                byte[] data = Utility.Encrypt(command, false);
                loginServer.Send(data);

                errorMessage = "Connecting...";
                errorMessageTimer = true;
            }
            catch
            {
                errorMessage = "Login Server is Unvailable.";
                errorMessageTimer = true;
            }
        }

        public void BringToFront(MenuDialogBoxType type)
        {
            dialogBoxDrawOrder.Remove(type);
            dialogBoxDrawOrder.AddLast(type);
        }

        public void AddEvent(string message)
        {
            throw new NotImplementedException();
        }

        private void ServerProcess(byte[] data, int identity, string ipAddress)
        {
            try
            {
                int typeTemp = (int)BitConverter.ToInt16(data, 4);
                CommandType type;

                if (Enum.TryParse<CommandType>(typeTemp.ToString(), out type))
                {
                    switch (type)
                    {
                        case CommandType.Confirm:
                            characters = new List<Player>();

                            int characterCount = (int)data[23];
                            for (int i = 0; i < characterCount; i++)
                            {
                                // create dummy characters for the character select screen
                                Player character = new Player("", "", "");
                                character.Name = Encoding.ASCII.GetString(data, 24 + (i * 116), 10).Trim('\0');
                                character.PlayType = (PlayType)(int)data[34];
                                character.Appearance1 = BitConverter.ToInt16(data, 35 + (i * 116));
                                character.Appearance2 = BitConverter.ToInt16(data, 37 + (i * 116));
                                character.Appearance3 = BitConverter.ToInt16(data, 39 + (i * 116));
                                character.Appearance4 = BitConverter.ToInt16(data, 41 + (i * 116));
                                int gender = BitConverter.ToInt16(data, 43 + (i * 116));
                                int skinColour = (int)data[45 + (i * 116)];
                                character.Side = (OwnerSide)(int)data[46 + (i * 116)];
                                character.Type = (((GenderType)gender) == GenderType.Male ? 1 : 4) + (skinColour - 1);
                                character.Level = BitConverter.ToInt16(data, 47 + (i * 116));
                                character.Experience = BitConverter.ToInt32(data, 49 + (i * 116));
                                character.Strength = BitConverter.ToInt16(data, 53 + (i * 116));
                                character.Vitality = BitConverter.ToInt16(data, 55 + (i * 116));
                                character.Dexterity = BitConverter.ToInt16(data, 57 + (i * 116));
                                character.Intelligence = BitConverter.ToInt16(data, 59 + (i * 116));
                                character.Magic = BitConverter.ToInt16(data, 61 + (i * 116));
                                character.Agility = BitConverter.ToInt16(data, 63 + (i * 116));
                                character.AppearanceColour = BitConverter.ToInt32(data, 65 + (i * 116));
                                character.LastLogin = new DateTime(BitConverter.ToInt16(data, 69 + (i * 116)),
                                                                    BitConverter.ToInt16(data, 71 + (i * 116)),
                                                                    BitConverter.ToInt16(data, 73 + (i * 116)),
                                                                    BitConverter.ToInt16(data, 75 + (i * 116)),
                                                                    BitConverter.ToInt16(data, 77 + (i * 116)),
                                                                    BitConverter.ToInt16(data, 79 + (i * 116)));
                                character.MapName = Encoding.ASCII.GetString(data, 81 + (i * 116), 10).Trim('\0');
                                int itemId = BitConverter.ToInt16(data, 91 + (i * 116));
                                if (Cache.ItemConfiguration.ContainsKey(itemId))
                                {
                                    character.Inventory[(int)EquipType.Head] = Cache.ItemConfiguration[itemId].Copy();
                                    character.Inventory[(int)EquipType.Head].Colour = BitConverter.ToInt32(data, 93 + (i * 116));
                                }
                                itemId = BitConverter.ToInt16(data, 97 + (i * 116));
                                if (Cache.ItemConfiguration.ContainsKey(itemId))
                                {
                                    character.Inventory[(int)EquipType.Body] = Cache.ItemConfiguration[itemId].Copy();
                                    character.Inventory[(int)EquipType.Body].Colour = BitConverter.ToInt32(data, 99 + (i * 116));
                                }
                                itemId = BitConverter.ToInt16(data, 103 + (i * 116));
                                if (Cache.ItemConfiguration.ContainsKey(itemId))
                                {
                                    character.Inventory[(int)EquipType.Arms] = Cache.ItemConfiguration[itemId].Copy();
                                    character.Inventory[(int)EquipType.Arms].Colour = BitConverter.ToInt32(data, 105 + (i * 116));
                                }
                                itemId = BitConverter.ToInt16(data, 109 + (i * 116));
                                if (Cache.ItemConfiguration.ContainsKey(itemId))
                                {
                                    character.Inventory[(int)EquipType.Legs] = Cache.ItemConfiguration[itemId].Copy();
                                    character.Inventory[(int)EquipType.Legs].Colour = BitConverter.ToInt32(data, 111 + (i * 116));
                                }
                                itemId = BitConverter.ToInt16(data, 115 + (i * 116));
                                if (Cache.ItemConfiguration.ContainsKey(itemId))
                                {
                                    character.Inventory[(int)EquipType.Back] = Cache.ItemConfiguration[itemId].Copy();
                                    character.Inventory[(int)EquipType.Back].Colour = BitConverter.ToInt32(data, 117 + (i * 116));
                                }
                                itemId = BitConverter.ToInt16(data, 121 + (i * 116));
                                if (Cache.ItemConfiguration.ContainsKey(itemId))
                                {
                                    character.Inventory[(int)EquipType.RightHand] = Cache.ItemConfiguration[itemId].Copy();
                                    character.Inventory[(int)EquipType.RightHand].Colour = BitConverter.ToInt32(data, 123 + (i * 116));
                                }
                                itemId = BitConverter.ToInt16(data, 127 + (i * 116));
                                if (Cache.ItemConfiguration.ContainsKey(itemId))
                                {
                                    character.Inventory[(int)EquipType.LeftHand] = Cache.ItemConfiguration[itemId].Copy();
                                    character.Inventory[(int)EquipType.LeftHand].Colour = BitConverter.ToInt32(data, 129 + (i * 116));
                                }
                                itemId = BitConverter.ToInt16(data, 133 + (i * 116));
                                if (Cache.ItemConfiguration.ContainsKey(itemId))
                                {
                                    character.Inventory[(int)EquipType.Feet] = Cache.ItemConfiguration[itemId].Copy();
                                    character.Inventory[(int)EquipType.Feet].Colour = BitConverter.ToInt32(data, 135 + (i * 116));
                                }
                                // todo 139 - utility
                                characters.Add(character);
                            }

                            loginServer.Close();
                            isComplete = true;
                            break;
                        case CommandType.NewAccountCreated:
                            newPasswordBox.Text = null;
                            reTypePasswordBox.Text = null;
                            emailBox.Text = null;
                            questionBox.Text = null;
                            answerBox.Text = null;
                            username = account;
                            password = newPassword;
                            Login();
                            accountBox.Text = null;
                            break;
                        case CommandType.PasswordIncorrect: errorMessage = "Password incorrect."; errorMessageTimer = true; break;
                        case CommandType.Reject: errorMessage = "No account found."; errorMessageTimer = true; break;
                        case CommandType.LoginUnavailable: errorMessage = "Login Server is Unavailable."; errorMessageTimer = true; break;
                        case CommandType.IncorrectVersion: errorMessage = "Version mismatch, please run the updater."; errorMessageTimer = true; break;
                        case CommandType.NewAccountAlreadyExists: errorMessage = "Name already exists!"; errorMessageTimer = true; break;
                        case CommandType.NewAccountFailed: errorMessage = "Unable to create account at this time."; errorMessageTimer = true; break;
                        default: errorMessage = "Unable to connect."; errorMessageTimer = true; break;
                    }

#if DEBUG
                    Console.Write("Received: " + typeTemp.ToString());
#endif
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void ReadMessageFromLoginServer(ClientInfo ci, byte[] data, int len)
        {
            try
            {
                lock (processQueueLock)
                {
                    // find out if there is more than 1 packet here, if there is we need to split them up
                    // and process them individually. this is by nature of the way TCP works using streams
                    bool allPacketsProcessed = false;
                    int pointer = 0; // packet start pointer
                    while (!allPacketsProcessed)
                    {
                        int size = BitConverter.ToInt16(data, pointer + 1); // data[1] and data[2] - size of the packet
                        byte[] packetData = new byte[size];
                        Buffer.BlockCopy(data, pointer, packetData, 0, size);

                        if (packetData.Length > 2)
                        {
                            Command command = new Command(packetData, ci.ID);
                            lock (processQueueLock)
                            {
                                processQueue.Enqueue(command);
                            }
                        }

                        // check if another header can be found. if so, check that its size > 0. if not, all packets processed
                        pointer += size + 3; // set the pointer to the index start of the next packet
                        //if ((len >= pointer) && BitConverter.ToInt16(data, pointer + 1) > 0)
                        //{ }
                        //else

                        if (len <= pointer || BitConverter.ToInt16(data, pointer + 1) <= 0) allPacketsProcessed = true; // we're all good
                    }
                }
            }
            catch
            {
            }
        }

        private bool LoadHotKeyConfiguration()
        {
            try
            {
                bool exists;
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\hotkey.xml";
                exists = File.Exists(configPath);
                if (!exists) { ConfigurationHelper.ResetHotKeys(configPath); }
                Cache.HotKeyConfiguration = ConfigurationHelper.LoadHotKeys(configPath);
                Cache.HotBarConfiguration = ConfigurationHelper.LoadHotBar(configPath); //Hotbar?
                foreach (KeyValuePair<int, HotKey> hotKey in Cache.HotKeyConfiguration) 
                {
                    foreach (KeyValuePair<int, HotKey> testKey in Cache.HotKeyConfiguration)
                    {
                        if (hotKey.Value.Action != testKey.Value.Action)
                        {
                            int KeyCount = 0;
                            if ((hotKey.Value.Press == hotKey.Value.Hold) && hotKey.Value.Press != Keys.None) { hotKey.Value.Press = Keys.None; hotKey.Value.Hold = Keys.None; }
                            if (hotKey.Value.Press == Keys.None) { KeyCount += 1; } 
                            if (hotKey.Value.Hold == Keys.None) { KeyCount += 1; }
                            if (KeyCount <= 1)
                            {
                                if (hotKey.Value.Press == testKey.Value.Press && hotKey.Value.Hold == testKey.Value.Hold) 
                                {
                                    hotKey.Value.Press = Keys.None;
                                    hotKey.Value.Hold = Keys.None;
                                    testKey.Value.Press = Keys.None;
                                    testKey.Value.Hold = Keys.None;
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void SaveHotKeyConfiguration()
        {
            string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\hotkey.xml";
            ConfigurationHelper.SaveHotKeys(configPath, Cache.HotKeyConfiguration);
        }

        private void ReloadHotKeySettings()
        {
            //CLEAR PREVIOUS
            hotKeyMenuItems.Clear();
            Cache.HotKeyConfiguration.Clear();


            //LOAD HOTKEY CACHE
            this.LoadHotKeyConfiguration();

            //CREATE HOTKEY SETTINGS ITEMS
            HotKeyMenuItem hotKeyTitle = new HotKeyMenuItem(display, "Customizable Hotkeys", hotKeyMenuItems.Count, MenuItemType.Text);
            hotKeyMenuItems.Add(hotKeyTitle);

            foreach (KeyValuePair<int, HotKey> hotKeyButton in Cache.HotKeyConfiguration)
            {
                HotKeyMenuItem hotKeyMenuItem = new HotKeyMenuItem(display, new HotKey(hotKeyButton.Value.Press, hotKeyButton.Value.Hold, hotKeyButton.Value.Action, hotKeyButton.Value.Index), Cache.HotKeyConfiguration, hotKeyMenuItems.Count);
                hotKeyMenuItems.Add(hotKeyMenuItem);
            }

            HotKeyMenuItem newHotKey = new HotKeyMenuItem(display, "Create new Hotkey", hotKeyMenuItems.Count, MenuItemType.CreateHotKey);
            hotKeyMenuItems.Add(newHotKey);

            foreach (HotKeyMenuItem item in hotKeyMenuItems)
            {
                item.Scroll(keyboardSettingsScrollCounter);
            }
        }

        private bool LoadItemConfiguration()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\items.xml";

                Cache.ItemConfiguration = ItemHelper.LoadItems(configPath);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void DrawAdvancedSettingsToTexture(SpriteBatch spriteBatch, GameTime gameTime, RenderTarget2D renderTarget)
        {
            // Set the render target
            spriteBatch.End();
            GraphicsDevice.SetRenderTarget(renderTarget);

            spriteBatch.Begin();
            // DrawItemPopup the scene
            GraphicsDevice.Clear(Color.Transparent);
            for (int i = advancedSettingsMenuItems.Count - 1; i >= 0; i--)
            {
                AdvancedSettingsMenuItem item = advancedSettingsMenuItems[i];
                item.Draw(spriteBatch, gameTime, glowFrame);
            }

            // Drop the render target
            spriteBatch.End();
            GraphicsDevice.SetRenderTarget(mainRenderTarget);
            spriteBatch.Begin();
            spriteBatch.Draw(renderTarget, new Vector2((display.Device.Viewport.Width / 2) - 142, (display.Device.Viewport.Height / 2) - 37), Color.White);

        }

        public void AddEffect(IGameEffect e) { AddEffect(e, -1, -1); }
        public void AddEffect(IGameEffect e, int pivotX, int pivotY)
        {
            
        }

        public bool IsComplete { get { return isComplete; } }
        public bool Back { get { return back; } }

        public BackGroundImage Background { get { return background; } set { background = value; } }
        public BackGroundImage Backgroundblackbar { get { return backgroundblackbar; } set { backgroundblackbar = value; } }
        public BackGroundImage Legs { get { return legs; } set { legs = value; } }
        public BackGroundImage Weapon { get { return weapon; } set { weapon = value; } }
        public BackGroundImage Tree { get { return tree; } set { tree = value; } }
        public BackGroundImage Grass { get { return grass; } set { grass = value; } }
        public BackGroundImage Grass2 { get { return grass2; } set { grass2 = value; } }
        public BackGroundImage Grass3 { get { return grass3; } set { grass3 = value; } }
        public BackGroundImage Smoke1 { get { return smoke1; } set { smoke1 = value; } }
        public BackGroundImage Smoke2 { get { return smoke2; } set { smoke2 = value; } }
        public BackGroundImage Smoke3 { get { return smoke3; } set { smoke3 = value; } }
        public BackGroundImage Smoke4 { get { return smoke4; } set { smoke4 = value; } }
        public BackGroundImage Smoke5 { get { return smoke5; } set { smoke5 = value; } }
        public BackGroundImage Smoke6 { get { return smoke6; } set { smoke6 = value; } }
        public BackGroundImage Smoke7 { get { return smoke7; } set { smoke7 = value; } }
        public BackGroundImage Smoke8 { get { return smoke8; } set { smoke8 = value; } }
        public BackGroundImage Logo { get { return logo; } set { logo = value; } }
        public BackGroundImage LogoGlow { get { return logoglow; } set { logoglow = value; } }
        public BackGroundImage WarriorHead { get { return warriorhead; } set { warriorhead = value; } }
        public BackGroundImage WarriorBody { get { return warriorbody; } set { warriorbody = value; } }
        public Texture2D Corner { get { return corner; } set { corner = value; } }
        public Texture2D Textbox { get { return textbox; } set { textbox = value; } }
        public Texture2D DialogFader { get { return dialogFader; } set { dialogFader = value; } }
        public Texture2D DialogBorder { get { return dialogBorder; } set { dialogBorder = value; } }
        public LinkedList<BackGroundImage> BackgroundImages { get { return backgroundImages; } set { backgroundImages = value; } }

        public DefaultState State { get { return DefaultState.MainMenu; } }
        public GameDisplay Display { get { return display; } set { display = value; } }
        public GraphicsDevice GraphicsDevice { get { return display.Device; } }
        public string Username { get { return username; } }
        public string Password { get { return password; } }
        public string Account { get { return account; } }
        public string NewPassword { get { return newPassword; } }
        public string ReTypePassword { get { return reTypePassword; } }
        public string Email { get { return email; } }
        public string Question { get { return question; } }
        public string Answer { get { return answer; } }
        public TextBox UsernameBox { get { return usernameBox; } set { usernameBox = value; } }
        public TextBox PasswordBox { get { return passwordBox; } set { passwordBox = value; } }
        public TextBox AccountBox { get { return accountBox; } set { accountBox = value; } }
        public TextBox NewPasswordBox { get { return newPasswordBox; } set { newPasswordBox = value; } }
        public TextBox ReTypePasswordBox { get { return reTypePasswordBox; } set { reTypePasswordBox = value; } }
        public TextBox EmailBox { get { return emailBox; } set { emailBox = value; } }
        public TextBox QuestionBox { get { return questionBox; } set { questionBox = value; } }
        public TextBox AnswerBox { get { return answerBox; } set { answerBox = value; } }
        public List<Player> Characters { get { return characters; } }
        public List<IGameEffect> Effects { get { return null; } set { } }
        public Dictionary<MenuDialogBoxType, IMenuDialogBox> DialogBoxes { get { return dialogBoxes; } }
        public LinkedList<MenuDialogBoxType> DialogBoxDrawOrder { get { return dialogBoxDrawOrder; } set { dialogBoxDrawOrder = value; } }
        public List<MenuItem> LoginItems { get { return loginItems; } set { loginItems = value; } }
        public List<MenuItem> SettingsNormal { get { return settingsNormal; } set { settingsNormal = value; } }
        public List<MenuItem> SettingsKeyboard { get { return settingsKeyboard; } set { settingsKeyboard = value; } }
        public List<MenuItem> SettingsAdvanced { get { return settingsAdvanced; } set { settingsAdvanced = value; } }
        public List<MenuItem> AccountItems { get { return accountItems; } set { accountItems = value; } }
        public bool ResolutionChange { get { return resolutionChange; } set { resolutionChange = value; } }
        public RenderTarget2D MainRenderTarget { get { return mainRenderTarget; } set { mainRenderTarget = value; } }
        public MenuDialogBoxType ClickedDialogBox { get { return clickedDialogBox; } set { clickedDialogBox = value; } }
    }
}
