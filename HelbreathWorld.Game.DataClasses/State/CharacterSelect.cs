﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Game.Assets.UI;
using HelbreathWorld.Game.Assets.Effects;

namespace HelbreathWorld.Game.Assets.State
{
    public class CharacterSelect : IDefaultState, IMenuState
    {
        private bool isComplete;
        private bool back;
        private ClientInfo loginServer;
        private string loginAddress;
        private int loginPort;
        private string gameAddress;
        private int gamePort;
        private string account;
        private Queue<Command> processQueue;
        public readonly object processQueueLock = new object();

        private bool resolutionChange = false;

        private GameDisplay display;
        private List<MenuItem> mainMenuItems;
        private List<MenuItem> characterItems;
        private List<Player> characters;
        private List<MenuItem> selectCharacterItems;
        private List<MenuItem> createCharacterItems;
        private List<MenuItem> deleteCharacterItems;
        private List<MenuItem> changePasswordItems;
        private List<TextBox> passwordTextBoxes;
        private List<TextBox> createCharacterTextBoxes;
        private int selectedIndex;
        private int selectedButtonIndex;
        private string selectedCharacter;
        private string errorMessage;

        private bool selectedUpdated = false;
        private OwnerSide selectedSide;
        private int selectedHairType;
        private int selectedHairColour;
        private int selectedUnderwearColour;
        private int selectedSkin;
        private GenderType selectedGender;
        private string deletedName;
        private int selectedAppearance1;
        private int selectedType;

        //BACKGROUND
        private LinkedList<BackGroundImage> backgroundImages;
        private BackGroundImage[] backgroundcollection;
        private BackGroundImage background;
        private BackGroundImage backgroundblackbar;
        private BackGroundImage legs;
        private BackGroundImage weapon;
        private BackGroundImage tree;
        private BackGroundImage grass;
        private BackGroundImage grass2;
        private BackGroundImage grass3;
        private BackGroundImage smoke1;
        private BackGroundImage smoke2;
        private BackGroundImage smoke3;
        private BackGroundImage smoke4;
        private BackGroundImage smoke5;
        private BackGroundImage smoke6;
        private BackGroundImage smoke7;
        private BackGroundImage smoke8;
        private BackGroundImage logo;
        private BackGroundImage logoglow;
        private BackGroundImage warriorhead;
        private BackGroundImage warriorbody;


        private Texture2D corner;
        private Texture2D textbox;
        private Texture2D dialogFader;
        private Texture2D dialogBorder;
        private Texture2D bg;

        private bool fadeDirection;
        private bool fadeStage;
        private int fadeDelay = 10;
        private int backgroundDelay = 16;

        //effects
        private float glowFrame;
        private float blinkFrame;
        private float pulseFrame;

        private int flagFrame = 0;
        private OwnerSide flagSide;
        private int flagAnimationTimer = 0;

        private Texture2D backgroundTexture;
        private TextBox oldPasswordBox;
        private TextBox newPasswordBox;
        private TextBox reTypePasswordBox;
        private TextBox characterNameBox;
        private string oldPassword;
        private string newPassword;
        private string reTypePassword;

        Player newPlayer = new Player();
        private RenderTarget2D mainRenderTarget;

        //DIALOG BOXES
        private LinkedList<MenuDialogBoxType> dialogBoxDrawOrder;
        private Dictionary<MenuDialogBoxType, IMenuDialogBox> dialogBoxes;
        private MenuDialogBoxType clickedDialogBox;
        private MenuDialogBoxType highlightedDialogBox;
        private MenuDialogBoxType topdialogBox;

        public CharacterSelect(string ipAddress, int port, string account, List<Player> characters)
        {
            processQueue = new Queue<Command>();
            this.account = account;
            this.characters = characters;

            this.loginAddress = ipAddress;
            this.loginPort = port;
            this.selectedButtonIndex = -1;
            this.selectedIndex = -1;

            Cache.DefaultState = this;       
        }

        public void Init(GameWindow window)
        {           
            mainMenuItems = new List<MenuItem>();
            characterItems = new List<MenuItem>();
            selectCharacterItems = new List<MenuItem>();
            createCharacterItems = new List<MenuItem>();
            deleteCharacterItems = new List<MenuItem>();
            changePasswordItems = new List<MenuItem>();
            passwordTextBoxes = new List<TextBox>();
            createCharacterTextBoxes = new List<TextBox>();
            this.selectedGender = GenderType.Male;

            Viewport viewport = GraphicsDevice.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            int height = viewport.Height;
            int width = viewport.Width;

            display.Mouse.State = GameMouseState.Normal;

            //MAINMENU
            MenuItem mainTitleLabel = new MenuItem(MenuItemType.Title, "Select A Character");
            mainTitleLabel.SetPosition(new Vector2((int)(((viewportSize.X - Cache.Fonts[FontType.MagicMedieval24].MeasureString("Select A Character").X) / 2)), 10));
            mainMenuItems.Add(mainTitleLabel);


            int menuWidth = (int)((Cache.Fonts[FontType.MagicMedieval14].MeasureString("Change Password" + "Log Out").X) + (30 * 2));

            MenuItem createCharacterButton = new MenuItem(MenuItemType.Button, "New Character");
            createCharacterButton.Width = (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString("New Character").X + 30);
            createCharacterButton.Height = 50;
            createCharacterButton.SetPosition(new Vector2((int)((width / 2) - (createCharacterButton.Width / 2)), (int)(viewport.Height - 76)));
            mainMenuItems.Add(createCharacterButton);

            MenuItem changePasswordButton = new MenuItem(MenuItemType.Button, "Change Password");
            changePasswordButton.Width = (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString("Change Password").X + 30);
            changePasswordButton.Height = 50;
            changePasswordButton.SetPosition(new Vector2((int)((width / 2) - (menuWidth / 2)), (int)(viewport.Height - 41)));
            mainMenuItems.Add(changePasswordButton);

            MenuItem logOutButton = new MenuItem(MenuItemType.Button, "Log Out");
            logOutButton.Width = (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString("Log Out").X + 30);
            logOutButton.Height = 50;
            logOutButton.SetPosition(new Vector2((int)((width / 2) - (menuWidth / 2) + changePasswordButton.Width), (int)(viewport.Height - 41)));
            mainMenuItems.Add(logOutButton);

            MenuItem errorLabel = new MenuItem(MenuItemType.Text, "ERROR:");
            errorLabel.Width = 100;
            errorLabel.Height = 50;
            errorLabel.SetPosition(new Vector2(viewport.Width / 2, (viewport.Height) - 80));
            mainMenuItems.Add(errorLabel);

            CreateCharacterDisplay();

            //CHARACTER SELECT
            MenuItem selectCharacterLabel = new MenuItem(MenuItemType.Character, "");
            selectCharacterLabel.Height = 50;
            selectCharacterItems.Add(selectCharacterLabel);

            int characterSelectButtonWidth = (int)((Cache.Fonts[FontType.MagicMedieval14].MeasureString("Play Game" + "Delete").X) + (30 * 2));

            MenuItem playButton = new MenuItem(MenuItemType.Button, "Play Game");
            playButton.Width = (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString("Play Game").X + 30);
            playButton.Height = 50;
            playButton.SetPosition(new Vector2((int)((width / 2) - (characterSelectButtonWidth / 2)), (int)(viewport.Height / 2 + 99)));
            selectCharacterItems.Add(playButton);

            MenuItem deleteCharacterButton = new MenuItem(MenuItemType.Button, "Delete");
            deleteCharacterButton.Width = (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString("Delete").X + 30);
            deleteCharacterButton.Height = 50;
            deleteCharacterButton.SetPosition(new Vector2((int)((width / 2) - (characterSelectButtonWidth / 2) + playButton.Width), (int)(viewport.Height / 2 + 99)));
            selectCharacterItems.Add(deleteCharacterButton);

            MenuItem  newCharacterTitleLabel = new MenuItem(MenuItemType.Title, "New Character");
            newCharacterTitleLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval18].MeasureString("New Character").X / 2)), (int)((viewport.Height / 2) - 102)));
            createCharacterItems.Add(newCharacterTitleLabel);

            MenuItem characterNameLabel = new MenuItem(MenuItemType.Text, "Character Name:");
            characterNameLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 130 - 6), (int)((viewport.Height / 2) - 60)));
            createCharacterItems.Add(characterNameLabel);

            characterNameBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            characterNameBox.Width = 115;
            characterNameBox.Height = 20;
            characterNameBox.X = (viewport.Width / 2) - (characterNameBox.Width / 2) + 80 - 6;
            characterNameBox.Y = viewport.Height / 2 - 60;
            characterNameBox.MaxCharacters = 10;
            createCharacterTextBoxes.Add(characterNameBox);

            MenuItem townLabel = new MenuItem(MenuItemType.Text, "Town:");
            townLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 103), viewport.Height / 2 - 20));
            createCharacterItems.Add(townLabel);

            MenuItem leftTownLabel = new MenuItem(MenuItemType.Image, "LeftTown");
            leftTownLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            leftTownLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[3].GetRectangle();
            leftTownLabel.Width = leftTownLabel.DestinationRectangle.Width;
            leftTownLabel.Height = leftTownLabel.DestinationRectangle.Height;
            leftTownLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6, viewport.Height / 2 - 20));
            createCharacterItems.Add(leftTownLabel);

            MenuItem rightTownLabel = new MenuItem(MenuItemType.Image, "RightTown");
            rightTownLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            rightTownLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[4].GetRectangle();
            rightTownLabel.Width = rightTownLabel.DestinationRectangle.Width;
            rightTownLabel.Height = rightTownLabel.DestinationRectangle.Height;
            rightTownLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6 + rightTownLabel.Width, viewport.Height / 2 - 20));
            createCharacterItems.Add(rightTownLabel);


            MenuItem genderLabel = new MenuItem(MenuItemType.Text, "Gender:");
            genderLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 103), viewport.Height / 2));
            createCharacterItems.Add(genderLabel);

            MenuItem leftGenderLabel = new MenuItem(MenuItemType.Image, "LeftGender");
            leftGenderLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            leftGenderLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[3].GetRectangle();
            leftGenderLabel.Width = leftGenderLabel.DestinationRectangle.Width;
            leftGenderLabel.Height = leftGenderLabel.DestinationRectangle.Height;
            leftGenderLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6, viewport.Height / 2));
            createCharacterItems.Add(leftGenderLabel);

            MenuItem rightGenderLabel = new MenuItem(MenuItemType.Image, "RightGender");
            rightGenderLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            rightGenderLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[4].GetRectangle();
            rightGenderLabel.Width = rightGenderLabel.DestinationRectangle.Width;
            rightGenderLabel.Height = rightGenderLabel.DestinationRectangle.Height;
            rightGenderLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6 + rightGenderLabel.Width, viewport.Height / 2));
            createCharacterItems.Add(rightGenderLabel);

            MenuItem skinToneLabel = new MenuItem(MenuItemType.Text, "Skin Tone:");
            skinToneLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 103), viewport.Height / 2 + 20));
            createCharacterItems.Add(skinToneLabel);

            MenuItem leftSkinToneLabel = new MenuItem(MenuItemType.Image, "LeftSkinTone");
            leftSkinToneLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            leftSkinToneLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[3].GetRectangle();
            leftSkinToneLabel.Width = leftSkinToneLabel.DestinationRectangle.Width;
            leftSkinToneLabel.Height = leftSkinToneLabel.DestinationRectangle.Height;
            leftSkinToneLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6, viewport.Height / 2 + 20));
            createCharacterItems.Add(leftSkinToneLabel);

            MenuItem rightSkinToneLabel = new MenuItem(MenuItemType.Image, "RightSkinTone");
            rightSkinToneLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            rightSkinToneLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[4].GetRectangle();
            rightSkinToneLabel.Width = rightSkinToneLabel.DestinationRectangle.Width;
            rightSkinToneLabel.Height = rightSkinToneLabel.DestinationRectangle.Height;
            rightSkinToneLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6 + rightSkinToneLabel.Width, viewport.Height / 2 + 20));
            createCharacterItems.Add(rightSkinToneLabel);

            MenuItem hairStyleLabel = new MenuItem(MenuItemType.Text, "Hair Style:");
            hairStyleLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 103), (viewport.Height / 2) + 40));
            createCharacterItems.Add(hairStyleLabel);

            MenuItem leftHairStyleLabel = new MenuItem(MenuItemType.Image, "LeftHairStyle");
            leftHairStyleLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            leftHairStyleLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[3].GetRectangle();
            leftHairStyleLabel.Width = leftHairStyleLabel.DestinationRectangle.Width;
            leftHairStyleLabel.Height = leftHairStyleLabel.DestinationRectangle.Height;
            leftHairStyleLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6, viewport.Height / 2 + 40));
            createCharacterItems.Add(leftHairStyleLabel);

            MenuItem rightHairStyleLabel = new MenuItem(MenuItemType.Image, "RightHairStyle");
            rightHairStyleLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            rightHairStyleLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[4].GetRectangle();
            rightHairStyleLabel.Width = rightHairStyleLabel.DestinationRectangle.Width;
            rightHairStyleLabel.Height = rightHairStyleLabel.DestinationRectangle.Height;
            rightHairStyleLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6 + rightHairStyleLabel.Width, viewport.Height / 2 + 40));
            createCharacterItems.Add(rightHairStyleLabel);

            MenuItem hairColorLabel = new MenuItem(MenuItemType.Text, "Hair Color:");
            hairColorLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 103), viewport.Height / 2 + 60));
            createCharacterItems.Add(hairColorLabel);

            MenuItem leftHairColorLabel = new MenuItem(MenuItemType.Image, "LeftHairColor");
            leftHairColorLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            leftHairColorLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[3].GetRectangle();
            leftHairColorLabel.Width = leftHairColorLabel.DestinationRectangle.Width;
            leftHairColorLabel.Height = leftHairColorLabel.DestinationRectangle.Height;
            leftHairColorLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6, viewport.Height / 2 + 60));
            createCharacterItems.Add(leftHairColorLabel);

            MenuItem rightHairColorLabel = new MenuItem(MenuItemType.Image, "RightHairColor");
            rightHairColorLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            rightHairColorLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[4].GetRectangle();
            rightHairColorLabel.Width = rightHairColorLabel.DestinationRectangle.Width;
            rightHairColorLabel.Height = rightHairColorLabel.DestinationRectangle.Height;
            rightHairColorLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6 + rightHairColorLabel.Width, viewport.Height / 2 + 60));
            createCharacterItems.Add(rightHairColorLabel);

            MenuItem underwareColorLabel = new MenuItem(MenuItemType.Text, "Underware:");
            underwareColorLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 103), viewport.Height / 2 + 80));
            createCharacterItems.Add(underwareColorLabel);

            MenuItem leftUnderwareLabel = new MenuItem(MenuItemType.Image, "LeftUnderware");
            leftUnderwareLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            leftUnderwareLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[3].GetRectangle();
            leftUnderwareLabel.Width = leftUnderwareLabel.DestinationRectangle.Width;
            leftUnderwareLabel.Height = leftUnderwareLabel.DestinationRectangle.Height;
            leftUnderwareLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6, viewport.Height / 2 + 80));
            createCharacterItems.Add(leftUnderwareLabel);

            MenuItem rightUnderwareLabel = new MenuItem(MenuItemType.Image, "RightUnderware");
            rightUnderwareLabel.Texture = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture;
            rightUnderwareLabel.DestinationRectangle = Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[4].GetRectangle();
            rightUnderwareLabel.Width = rightUnderwareLabel.DestinationRectangle.Width;
            rightUnderwareLabel.Height = rightUnderwareLabel.DestinationRectangle.Height;
            rightUnderwareLabel.SetPosition(new Vector2(viewport.Width / 2 - 130 - 6 + rightUnderwareLabel.Width, viewport.Height / 2 + 80));
            createCharacterItems.Add(rightUnderwareLabel);

            MenuItem createButton = new MenuItem(MenuItemType.Button, "Create");
            createButton.Width = (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString("Create").X + 30);
            createButton.Height = 50;
            createButton.SetPosition(new Vector2((int)((width / 2) - (createButton.Width / 2) + 80), (int)(viewport.Height / 2 + 99)));
            createCharacterItems.Add(createButton);

            MenuItem cancelButton = new MenuItem(MenuItemType.Button, "Cancel");
            cancelButton.Width = (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString("Cancel").X + 30);
            cancelButton.Height = 50;
            cancelButton.SetPosition(new Vector2((int)((width / 2) - (cancelButton.Width / 2) - 80), (int)(viewport.Height / 2 + 99)));
            createCharacterItems.Add(cancelButton);

            //CHARACTER DELETE
            MenuItem deleteCharacterTitleLabel = new MenuItem(MenuItemType.Title, "Delete Character");
            deleteCharacterTitleLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval18].MeasureString("Delete Character").X / 2)), (int)((viewport.Height / 2) - 102)));
            deleteCharacterItems.Add(deleteCharacterTitleLabel);

            MenuItem deleteCharacterLabel = new MenuItem(MenuItemType.Character, "");
            deleteCharacterLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval18].MeasureString("").X / 2)), (int)((viewport.Height / 2) - 10)));
            deleteCharacterItems.Add(deleteCharacterLabel);

            MenuItem deleteCharacterText = new MenuItem(MenuItemType.Text, "Are you sure you want to");
            deleteCharacterText.SetPosition(new Vector2((int)(viewport.Width / 2 - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Are you sure you want to").X / 2)), (int)((viewport.Height / 2) + 45)));
            deleteCharacterItems.Add(deleteCharacterText);

            MenuItem deleteCharacterText2 = new MenuItem(MenuItemType.Text, "Delete this Character?");
            deleteCharacterText2.SetPosition(new Vector2((int)(viewport.Width / 2 - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Delete this Character?").X / 2)), (int)((viewport.Height / 2) + 67)));
            deleteCharacterItems.Add(deleteCharacterText2);

            int deleteCharacterButtonWidth = (int)((Cache.Fonts[FontType.MagicMedieval14].MeasureString("Yes" + "No").X) + (30 * 2));

            MenuItem deleteYesButton = new MenuItem(MenuItemType.Button, "Yes");
            deleteYesButton.Width = (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString("Yes").X + 30);
            deleteYesButton.Height = 50;
            deleteYesButton.SetPosition(new Vector2((int)((width / 2) - (deleteCharacterButtonWidth / 2)), (int)(viewport.Height / 2 + 99)));
            deleteCharacterItems.Add(deleteYesButton);

            MenuItem deleteNoButton = new MenuItem(MenuItemType.Button, "No");
            deleteNoButton.Width = (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString("No").X + 30);
            deleteNoButton.Height = 50;
            deleteNoButton.SetPosition(new Vector2((int)((width / 2) - (deleteCharacterButtonWidth / 2) + deleteYesButton.Width), (int)(viewport.Height / 2 + 99)));
            deleteCharacterItems.Add(deleteNoButton);

            //CHANGE PASSWORD
            MenuItem changePasswordTitleLabel = new MenuItem(MenuItemType.Title, "Change Account Password");
            changePasswordTitleLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval18].MeasureString("Change Account Password").X / 2) + 4), (int)((viewport.Height / 2) - 102)));
            changePasswordItems.Add(changePasswordTitleLabel);

            MenuItem oldPasswordLabel = new MenuItem(MenuItemType.Text, "Old Password");
            oldPasswordLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("Old Password").X / 2) - 6), (int)((viewport.Height / 2) + 1)));
            changePasswordItems.Add(oldPasswordLabel);

            oldPasswordBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.DamageSmallSize11]);
            oldPasswordBox.PasswordBox = true;
            oldPasswordBox.Width = 115;
            oldPasswordBox.Height = 20;
            oldPasswordBox.X = (viewport.Width / 2) - (oldPasswordBox.Width / 2) + 30 - 6;
            oldPasswordBox.Y = viewport.Height / 2 + 20;
            oldPasswordBox.MaxCharacters = 10;
            passwordTextBoxes.Add(oldPasswordBox);

            MenuItem newPasswordLabel = new MenuItem(MenuItemType.Text, "New Password");
            newPasswordLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("New Password").X / 2) - 6), (int)((viewport.Height / 2) + 1)));
            changePasswordItems.Add(newPasswordLabel);

            newPasswordBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.DamageSmallSize11]);
            newPasswordBox.PasswordBox = true;
            newPasswordBox.Width = 115;
            newPasswordBox.Height = 20;
            newPasswordBox.X = (viewport.Width / 2) - (newPasswordBox.Width / 2) + 30 - 6;
            newPasswordBox.Y = viewport.Height / 2 + 50;
            newPasswordBox.MaxCharacters = 10;
            passwordTextBoxes.Add(newPasswordBox);

            MenuItem reTypePasswordLabel = new MenuItem(MenuItemType.Text, "ReType New Password");
            reTypePasswordLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString("ReType New Password").X / 2) - 6), (int)((viewport.Height / 2) + 1)));
            changePasswordItems.Add(reTypePasswordLabel);

            reTypePasswordBox = new TextBox(backgroundTexture, Cache.Fonts[FontType.DamageSmallSize11]);
            reTypePasswordBox.PasswordBox = true;
            reTypePasswordBox.Width = 115;
            reTypePasswordBox.Height = 20;
            reTypePasswordBox.X = (viewport.Width / 2) - (reTypePasswordBox.Width / 2) - 6;
            reTypePasswordBox.Y = viewport.Height / 2 + 22;
            reTypePasswordBox.MaxCharacters = 10;
            passwordTextBoxes.Add(reTypePasswordBox);

            //DIALOG BOXES
            highlightedDialogBox = clickedDialogBox = MenuDialogBoxType.None;

            IMenuDialogBox dialogBox;

            dialogBoxes = new Dictionary<MenuDialogBoxType, IMenuDialogBox>();
            dialogBoxDrawOrder = new LinkedList<MenuDialogBoxType>();

            //SELECT CHARACTER
            dialogBox = new MenuDialogBox(MenuDialogBoxType.SelectCharacter, -1, -1, (width / 2), (height / 2));
            dialogBox.Moveable = false;
            dialogBoxes.Add(MenuDialogBoxType.SelectCharacter, dialogBox);
            dialogBox.Show();

            //CREATE CHARACTER
            dialogBox = new MenuDialogBox(MenuDialogBoxType.CreateCharacter, -1, -1, (width / 2), (height / 2));
            dialogBox.Moveable = false;
            dialogBoxes.Add(MenuDialogBoxType.CreateCharacter, dialogBox);

            //DELETE CHARACTER
            dialogBox = new MenuDialogBox(MenuDialogBoxType.DeleteCharacter, -1, -1, (width / 2), (height / 2));
            dialogBox.Moveable = false;
            dialogBoxes.Add(MenuDialogBoxType.DeleteCharacter, dialogBox);

            //CHANGE PASSWORD
            dialogBox = new MenuDialogBox(MenuDialogBoxType.ChangePassword, -1, -1, (width / 2), (height / 2));
            dialogBox.Moveable = false;
            dialogBoxes.Add(MenuDialogBoxType.ChangePassword, dialogBox);
        }

        public void ShowMessageBox(MenuMessageBox box)
        { 
        
        }

        public void HideMessageBox(MenuDialogBoxType type)
        {

        }

        private void CreateCharacterDisplay()
        {
            characterItems.Clear();

            //CHARACTER NAME DISPLAY
            Viewport viewport = GraphicsDevice.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            int height = viewport.Height;
            int width = viewport.Width;
            int index = 0;

            foreach (Player character in characters)
            {
                MenuItem characterLabel = new MenuItem(MenuItemType.Character, character.Name);
                characterLabel.Width = 100;
                characterLabel.Height = 50;
                characterLabel.SetPosition(new Vector2((width / 2) - 200 + (index * 100), 45));
                characterItems.Add(characterLabel);
                index++;
            }

            while (index < 4)
            {
                MenuItem characterLabel = new MenuItem(MenuItemType.UnusedCharacter, "Empty");
                characterLabel.Width = 100;
                characterLabel.Height = 50;
                characterLabel.SetPosition(new Vector2((width / 2) - 200 + (index * 100), 45));
                characterItems.Add(characterLabel);
                index++;
            }

            while (index >= 4 && index < 8)
            {
                MenuItem characterLabel = new MenuItem(MenuItemType.LockedCharacter, "Locked");
                characterLabel.Width = 100;
                characterLabel.Height = 50;
                characterLabel.SetPosition(new Vector2((width / 2) - 200 + ((index - 4) * 100), 85));
                characterItems.Add(characterLabel);
                index++;
            }
        }

        private void DeleteCharacter()
        {
            if (selectedIndex < 0) return;

            try
            {
                errorMessage = string.Empty;
                deletedName = characters[selectedIndex].Name;

                /* INITIALIZE LOGIN SERVER TCP CONNECTION */
                if (loginServer == null)
                {
                    Socket socket = Sockets.CreateTCPSocket(loginAddress, loginPort);
                    loginServer = new ClientInfo(socket, true);
                    loginServer.EncryptionType = EncryptionType.None;
                    loginServer.OnReadBytes += new ConnectionReadBytes(ReadMessageFromLoginServer);
                }

                byte[] command = new byte[250];
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.DeleteCharacter), 0, command, 0, 4);
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Confirm), 0, command, 4, 2);
                Buffer.BlockCopy(deletedName.GetBytes(10), 0, command, 6, 10);
                Buffer.BlockCopy(account.GetBytes(10), 0, command, 16, 10);



                byte[] data = Utility.Encrypt(command, false);
                loginServer.Send(data);
            
                errorMessage = "Connecting...";
            }
            catch
            {
                errorMessage = "Login Server is Unvailable.";
            }
        }

        private void NewCharacter()
        {
            try
            {
                errorMessage = string.Empty;

                /* INITIALIZE LOGIN SERVER TCP CONNECTION */
                if (loginServer == null)
                {
                    Socket socket = Sockets.CreateTCPSocket(loginAddress, loginPort);
                    loginServer = new ClientInfo(socket, true);
                    loginServer.EncryptionType = EncryptionType.None;
                    loginServer.OnReadBytes += new ConnectionReadBytes(ReadMessageFromLoginServer);
                }

                byte[] command = new byte[250];
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.CreateCharacter), 0, command, 0, 4);
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Confirm), 0, command, 4, 2);
                Buffer.BlockCopy(characterNameBox.Text.GetBytes(10), 0, command, 6, 10);
                Buffer.BlockCopy(account.GetBytes(10), 0, command, 16, 10);
                //Buffer.BlockCopy(password.GetBytes(10), 0, command, 26, 10);
                //36-66 ?
                command[66] = (byte)(int)(selectedGender); // gender
                command[67] = (byte)selectedSkin; // skin
                command[68] = (byte)selectedHairType; // hair style
                command[69] = (byte)selectedHairColour; // hair colour
                command[70] = (byte)selectedUnderwearColour; // underwear colour
                command[71] = (byte)10; // strength
                command[72] = (byte)10; // vit
                command[73] = (byte)10; // dex
                command[74] = (byte)10; // int
                command[75] = (byte)10; // mag
                command[76] = (byte)10; // chr
                command[77] = (byte)(int)selectedSide; // town
              
                byte[] data = Utility.Encrypt(command, false);
                loginServer.Send(data);

                
                errorMessage = "Connecting...";
            }
            catch
            {
                errorMessage = "Login Server is Unvailable.";
            }
        }

        public void Update(GameTime gameTime, Microsoft.Xna.Framework.Game game)
        {
            display.Mouse.Update(Mouse.GetState());
            display.Keyboard.Update(Keyboard.GetState());

            display.Mouse.State = GameMouseState.Normal;

            int mouseX = (int)display.Mouse.X;
            int mouseY = (int)display.Mouse.Y;

            Cache.TransparencyFaders.Update();

            //DIALOG BOXES
            foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
            {
                if (key.Value.Visible)
                {
                    switch (key.Key)
                    {
                        default: break;
                        case MenuDialogBoxType.SelectCharacter:
                            {
                                // MOUSE COLLISION DETECTION
                                for (int i = 0; i < selectCharacterItems.Count; i++)
                                {
                                    MenuItem item = selectCharacterItems[i];
                                    Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height);
                                    if (((mouseY > itemLocation.Y + 4) && (mouseY < (itemLocation.Y + itemLocation.Height - 10))) && ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2))))
                                    {
                                        item.Highlighted = true;
                                        if (item.Type == MenuItemType.Button)
                                        {
                                        display.Mouse.State = GameMouseState.Pickup;
                                        }
                                        if (display.Mouse.LeftClick)
                                        {
                                            selectedButtonIndex = i;
                                            item.Selected = true;
                                        }
                                    }
                                    else item.Highlighted = false;

                                    // reset previously selected items
                                    if (selectedButtonIndex != i && item.Selected) item.Selected = false;
                                }

                                if (selectedIndex != -1)
                                {
                                    Player selectedCharacter = characters[selectedIndex];

                                    selectedCharacter.Update();

                                    if (selectedCharacter.Motion == MotionType.None || selectedCharacter.Direction == MotionDirection.None)
                                        selectedCharacter.Idle(MotionDirection.South);
                                    else
                                    {
                                        if (selectedCharacter.AnimationFrame >= selectedCharacter.AnimationFrameCount - 1)
                                        {
                                            switch (selectedCharacter.Direction)
                                            {
                                                case MotionDirection.South: selectedCharacter.Direction = MotionDirection.SouthWest; break;
                                                case MotionDirection.SouthWest: selectedCharacter.Direction = MotionDirection.West; break;
                                                case MotionDirection.West: selectedCharacter.Direction = MotionDirection.NorthWest; break;
                                                case MotionDirection.NorthWest: selectedCharacter.Direction = MotionDirection.North; break;
                                                case MotionDirection.North: selectedCharacter.Direction = MotionDirection.NorthEast; break;
                                                case MotionDirection.NorthEast: selectedCharacter.Direction = MotionDirection.East; break;
                                                case MotionDirection.East: selectedCharacter.Direction = MotionDirection.SouthEast; break;
                                                case MotionDirection.SouthEast: selectedCharacter.Direction = MotionDirection.South; break;
                                            }
                                            selectedCharacter.AnimationFrame = 0;
                                        }
                                    }
                                }
                                else if (characters.Count > 0)
                                {
                                    int lastUsed = -1; DateTime lastLogin = new DateTime();
                                    for (int i = 0; i < characters.Count; i++)
                                        if (characters[i] != null && characters[i].LastLogin > lastLogin)
                                        {
                                            lastLogin = characters[i].LastLogin;
                                            lastUsed = i;
                                        }

                                    if (lastUsed != -1)
                                    {
                                        selectedIndex = lastUsed;
                                        characterItems[lastUsed].Selected = true;
                                    }
                                    else
                                    {
                                        selectedIndex = 0;
                                        characterItems[0].Selected = true;
                                    }
                                }
                                break;
                            }
                        case MenuDialogBoxType.CreateCharacter:
                            {
                                if (flagAnimationTimer > 80)
                                {
                                    if (flagSide == selectedSide)
                                    {
                                        if (flagSide == OwnerSide.Aresden)
                                        {
                                            if (flagFrame == 3 || flagFrame < 0 || flagFrame > 3)
                                            {
                                                flagFrame = 0;
                                            }
                                            else
                                            {
                                                flagFrame = flagFrame + 1;
                                            }
                                        }
                                        else
                                        {
                                            if (flagFrame == 7 || flagFrame < 3 || flagFrame > 7)
                                            {
                                                flagFrame = 4;
                                            }
                                            else
                                            {
                                                flagFrame = flagFrame + 1;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        flagSide = selectedSide;
                                        if (flagSide == OwnerSide.Aresden)
                                        {
                                            flagFrame = 0;
                                        }
                                        else
                                        {
                                            flagFrame = 4;
                                        }
                                    }
                                    flagAnimationTimer = 0;
                                }
                                else
                                {
                                    flagAnimationTimer = flagAnimationTimer + gameTime.ElapsedGameTime.Milliseconds;
                                }

                                if (selectedUpdated) //TODO once appearance is removed redo this
                                {
                                    newPlayer.Type = (selectedGender == GenderType.Male ? 1 : 4) + ((int)selectedSkin - 1);
                                    selectedType = newPlayer.Type;
                                    newPlayer.Appearance1 = 0;
                                    newPlayer.Appearance1 = newPlayer.Appearance1 | selectedUnderwearColour;
                                    newPlayer.Appearance1 = newPlayer.Appearance1 | (selectedHairType << 8);
                                    newPlayer.Appearance1 = newPlayer.Appearance1 | (selectedHairColour << 4);
                                    selectedAppearance1 = newPlayer.Appearance1;
                                    selectedUpdated = false;
                                }

                                newPlayer.Update();

                                if (newPlayer.Motion == MotionType.None || newPlayer.Direction == MotionDirection.None)
                                    newPlayer.Idle(MotionDirection.South);
                                else
                                {
                                    if (newPlayer.AnimationFrame >= newPlayer.AnimationFrameCount - 1)
                                    {
                                        switch (newPlayer.Direction)
                                        {
                                            case MotionDirection.South: newPlayer.Direction = MotionDirection.SouthWest; break;
                                            case MotionDirection.SouthWest: newPlayer.Direction = MotionDirection.West; break;
                                            case MotionDirection.West: newPlayer.Direction = MotionDirection.NorthWest; break;
                                            case MotionDirection.NorthWest: newPlayer.Direction = MotionDirection.North; break;
                                            case MotionDirection.North: newPlayer.Direction = MotionDirection.NorthEast; break;
                                            case MotionDirection.NorthEast: newPlayer.Direction = MotionDirection.East; break;
                                            case MotionDirection.East: newPlayer.Direction = MotionDirection.SouthEast; break;
                                            case MotionDirection.SouthEast: newPlayer.Direction = MotionDirection.South; break;
                                        }
                                        newPlayer.AnimationFrame = 0;
                                    }
                                }

                                // MOUSE COLLISION DETECTION
                                for (int i = 0; i < createCharacterItems.Count; i++)
                                {
                                    MenuItem item = createCharacterItems[i];
                                    Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height);
                                    if (item.Type == MenuItemType.Image)
                                    {
                                        if (((mouseY > itemLocation.Y) && (mouseY < (itemLocation.Y + itemLocation.Height))) && ((mouseX > itemLocation.X) && (mouseX < (itemLocation.X + itemLocation.Width))))
                                        {
                                            item.Highlighted = true;
                                            display.Mouse.State = GameMouseState.Pickup;
                                            if (display.Mouse.LeftClick)
                                            {
                                                selectedButtonIndex = i;
                                                item.Selected = true;
                                            }
                                        }
                                        else item.Highlighted = false;
                                    }
                                    else
                                    {
                                        if (((mouseY > itemLocation.Y + 4) && (mouseY < (itemLocation.Y + itemLocation.Height - 10))) && ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2))))
                                        {
                                            item.Highlighted = true;
                                            display.Mouse.State = GameMouseState.Pickup;
                                            if (display.Mouse.LeftClick)
                                            {
                                                selectedButtonIndex = i;
                                                item.Selected = true;
                                            }
                                        }
                                        else item.Highlighted = false;
                                    }

                                    // reset previously selected items
                                    if (selectedButtonIndex != i && item.Selected) item.Selected = false;
                                }
                                break;
                            }
                        case MenuDialogBoxType.DeleteCharacter:
                            {
                                // MOUSE COLLISION DETECTION
                                for (int i = 0; i < deleteCharacterItems.Count; i++)
                                {
                                    MenuItem item = deleteCharacterItems[i];
                                    if (item.Type == MenuItemType.Character)
                                    {
                                        item.Text = characters[selectedIndex].Name;
                                    }
                                    Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height);
                                    if (((mouseY > itemLocation.Y + 4) && (mouseY < (itemLocation.Y + itemLocation.Height - 10))) && ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2))))
                                    {
                                        item.Highlighted = true;
                                        display.Mouse.State = GameMouseState.Pickup;
                                        if (display.Mouse.LeftClick)
                                        {
                                            selectedButtonIndex = i;
                                            item.Selected = true;
                                        }
                                    }
                                    else item.Highlighted = false;

                                    // reset previously selected items
                                    if (selectedButtonIndex != i && item.Selected) item.Selected = false;
                                }
                                break;
                            }
                        case MenuDialogBoxType.ChangePassword:
                            {
                                oldPasswordBox.Update(gameTime);
                                newPasswordBox.Update(gameTime);
                                reTypePasswordBox.Update(gameTime);

                                // MOUSE COLLISION DETECTION
                                for (int i = 0; i < changePasswordItems.Count; i++)
                                {
                                    MenuItem item = changePasswordItems[i];
                                    Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height);
                                    if (((mouseY > itemLocation.Y + 4) && (mouseY < (itemLocation.Y + itemLocation.Height - 10))) && ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2))))
                                    {
                                        item.Highlighted = true;
                                        display.Mouse.State = GameMouseState.Pickup;
                                        if (display.Mouse.LeftClick)
                                        {
                                            selectedButtonIndex = i;
                                            item.Selected = true;
                                        }
                                    }
                                    else item.Highlighted = false;

                                    // reset previously selected items
                                    if (selectedButtonIndex != i && item.Selected) item.Selected = false;
                                }
                                break;
                            }
                    }
                }
            }

            // MOUSE COLLISION DETECTION FOR CHARACTER ITEMS
            for (int i = 0; i < characterItems.Count; i++)
            {
                MenuItem item = characterItems[i];
                Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height);
                if (((mouseY > itemLocation.Y + 4) && (mouseY < (itemLocation.Y + itemLocation.Height - 10))) && ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2))))
                {
                    item.Highlighted = true;
                    display.Mouse.State = GameMouseState.Pickup;
                    if (display.Mouse.LeftClick)
                    {
                        if (item.Type == MenuItemType.Character)
                        {
                            selectedIndex = i;
                            item.Selected = true;
                        }
                    }
                }
                else item.Highlighted = false;

                // reset previously selected items
                if (selectedIndex != i && item.Selected) item.Selected = false;
            }

            // MOUSE COLLISION DETECTION FOR MAINMENU ITEMS
            for (int i = 0; i < mainMenuItems.Count; i++)
            {
                MenuItem item = mainMenuItems[i];
                Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height);
                if (((mouseY > itemLocation.Y + 4) && (mouseY < (itemLocation.Y + itemLocation.Height - 10))) && ((mouseX > itemLocation.X + 2) && (mouseX < (itemLocation.X + itemLocation.Width - 2))))
                {
                    item.Highlighted = true;
                    display.Mouse.State = GameMouseState.Pickup;
                    if (display.Mouse.LeftClick)
                    {
                        selectedButtonIndex = i;
                        item.Selected = true;
                    }
                }
                else item.Highlighted = false;

                // reset previously selected items
                if (selectedButtonIndex != i && item.Selected) item.Selected = false;
            }

            HandleMouse();
            HandleKeyboard();

            if (!display.Mouse.LeftHeld && display.Mouse.LeftClick)
            {
                //display.Mouse.LeftClick = false;
            }

            //BACKGROUND
            backgroundDelay -= gameTime.ElapsedGameTime.Milliseconds;
            if (backgroundDelay <= 0)
            {
                backgroundDelay = 16;
                foreach (BackGroundImage image in backgroundImages)
                {
                    if (image.Type == BackgroundType.Background)
                    {
                        fadeDelay -= gameTime.ElapsedGameTime.Milliseconds;
                        if (fadeDelay <= 0)
                        {
                            fadeDelay = 10;
                            image.Color = SpriteHelper.Fade(image.Color, ref fadeDirection, ref fadeStage);
                        }

                    }

                    if (image.Type == BackgroundType.Smoke || image.Type == BackgroundType.Smokebehind)
                    {
                        if (image.Drawposition.X <= -1919 || image.Drawposition.X >= 1919)
                        {
                            image.Drawposition = new Vector2(0, image.Drawposition.Y);
                        }
                        else
                        {
                            image.ShiftRight(image);
                        }
                    }

                    if (image.Type == BackgroundType.Midground || image.Type == BackgroundType.Foreground)
                    {
                        if (image.Drawposition.X <= -1919 || image.Drawposition.X >= 1919)
                        {
                            image.Drawposition = new Vector2(0, image.Drawposition.Y);
                        }
                        else
                        {
                            image.ShiftLeft(image);
                        }
                    }

                    if (image.Type == BackgroundType.Weapon)
                    {
                        if (image.Counter < 80)
                        {
                            if (image.Counter == 79)
                            {
                                image.ShiftUp(image, 0.1f);
                                image.Counter = 81;
                            }
                            else
                            {
                                image.ShiftUp(image, 0.1f);
                                image.Rotate(image, 0.001f);
                                image.Counter++;
                            }
                        }
                        else if (image.Counter > 80)
                        {
                            if (image.Counter == 160)
                            {
                                image.ShiftDown(image, 0.1f);
                                image.Counter = 0;
                            }
                            else
                            {
                                image.ShiftDown(image, 0.1f);
                                image.Rotate(image, -0.001f);
                                image.Counter++;
                            }
                        }
                    }

                    if (image.Type == BackgroundType.Body)
                    {
                        if (image.Counter < 80)
                        {
                            if (image.Counter == 79)
                            {
                                image.ShiftUp(image, 0.025f);
                                image.Counter = 81;
                            }
                            else
                            {
                                image.ShiftUp(image, 0.025f);
                                image.Scale = image.Scale + 0.0008f;
                                image.ShiftLeft(image);
                                image.Counter++;
                            }
                        }
                        else if (image.Counter > 80)
                        {
                            if (image.Counter == 160)
                            {
                                image.ShiftDown(image, 0.025f);
                                image.Counter = 0;
                            }
                            else
                            {
                                image.ShiftDown(image, 0.025f);
                                image.Scale = image.Scale - 0.0008f;
                                image.ShiftRight(image);
                                image.Counter++;
                            }
                        }
                    }

                    if (image.Type == BackgroundType.Grass)
                    {
                        if (image.Counter < 40)
                        {
                            if (image.Counter == 39)
                            {
                                image.ShiftDown(image, 0.02f);
                                image.ShiftLeft(image);
                                image.Counter = 41;
                            }
                            else
                            {
                                image.ShiftDown(image, 0.02f);
                                image.ShiftLeft(image);
                                image.Counter++;
                            }
                        }
                        else if (image.Counter > 40)
                        {
                            if (image.Counter == 80)
                            {
                                image.ShiftUp(image, 0.02f);
                                image.ShiftRight(image);
                                image.Counter = 0;
                            }
                            else
                            {
                                image.ShiftUp(image, 0.02f);
                                image.ShiftRight(image);
                                image.Counter++;
                            }
                        }
                    }
                }
            }

            while (processQueue.Count > 0 && processQueue.Peek() != null)
            {
                Command command;
                lock (processQueueLock)
                {
                    command = processQueue.Dequeue();
                    ServerProcess(Utility.Decrypt(command.Data), command.Identity, command.IPAddress);
                }
            }
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch, GameTime gameTime)
        {
            Viewport viewport = GraphicsDevice.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            int height = viewport.Height;
            int width = viewport.Width;
            int mouseX = (int)display.Mouse.X;
            int mouseY = (int)display.Mouse.Y;

            glowFrame = Cache.TransparencyFaders.GlowFrame;
            blinkFrame = Cache.TransparencyFaders.BlinkFrame;
            pulseFrame = Cache.TransparencyFaders.PulseFrame;

            //CREATE AN ORDERED ARRAY OF BACKGROUNDIMAGES
            backgroundcollection = new BackGroundImage[backgroundImages.Count];
            backgroundImages.CopyTo(backgroundcollection, 0);

            //ITERATE THROUGH ARRAY DRAWING BACKGROUNDIMAGES   
            for (int a = 0; a < backgroundcollection.Length; a++)
            {
                BackGroundImage image = backgroundcollection[a];
                {
                    if (image.Drawposition.X == 0)
                    {
                        spriteBatch.Draw(image.Texture, image.Drawposition, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0);
                    }
                    else
                    {
                        if (image.Drawposition.X < 0)
                        {
                            Vector2 after = new Vector2(image.Drawposition.X + 1918, image.Drawposition.Y);
                            spriteBatch.Draw(image.Texture, image.Drawposition, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //Second       
                            spriteBatch.Draw(image.Texture, after, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //Third                     
                        }
                        else
                        {
                            Vector2 before = new Vector2(image.Drawposition.X - 1918, image.Drawposition.Y);
                            spriteBatch.Draw(image.Texture, before, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //First                       
                            spriteBatch.Draw(image.Texture, image.Drawposition, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //Second
                        }
                    }
                }
            }

            //DRAW DECORATIVE EDGE
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15 + 1, 0 + 1, 164, 168), null, Microsoft.Xna.Framework.Color.Black, 0, new Vector2(0, 0), SpriteEffects.FlipVertically, 1); //Top left black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15, 0, 164, 168), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipVertically, 1); //Top left
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15 + 1, 0 + 1, 164, 168), null, Microsoft.Xna.Framework.Color.Black, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 1); //Top right black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15, 0, 164, 168), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 1); //Top right
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15 + 1, height - 168 + 1, 164, 168), Microsoft.Xna.Framework.Color.Black); //Button left black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15, height - 168, 164, 168), Microsoft.Xna.Framework.Color.White); //Button left
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15 + 1, height - 168 + 1, 164, 168), null, Microsoft.Xna.Framework.Color.Black, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 1); //Button right black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15, height - 168, 164, 168), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 1); //Button right

            //CHARACTER SELECTION 
            foreach (MenuItem item in characterItems)
            {
                if (item.Type == MenuItemType.LockedCharacter)
                {
                    Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height);
                    spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X + 2, itemLocation.Y + 8, itemLocation.Width - 4, itemLocation.Height - 16), Color.Black);
                    spriteBatch.Draw(textbox, itemLocation, Color.Black);

                    //MOUSE OVER HIGHLIGHT   
                    if (item.Highlighted)
                    {
                        string purchaseHighlight = "Purchase";
                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], purchaseHighlight, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(purchaseHighlight).X / 2)), (int)((item.Position.Y + 1) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(purchaseHighlight).Y / 2))), Color.Black);
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], purchaseHighlight, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(purchaseHighlight).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(purchaseHighlight).Y / 2))), Color.Red * glowFrame);
                    }
                    else
                    {
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], item.Text, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).X / 2)), (int)((item.Position.Y + 1) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).Y / 2))), Color.Black);
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).Y / 2))), Color.Black);
                    }
                }
                if (item.Type == MenuItemType.UnusedCharacter)
                {
                    Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height);
                    spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X + 2, itemLocation.Y + 8, itemLocation.Width - 4, itemLocation.Height - 16), Color.LightSlateGray);
                    spriteBatch.Draw(textbox, itemLocation, Color.LightSlateGray);

                    //MOUSE OVER HIGHLIGHT   
                    if (item.Highlighted)
                    {
                        string createCharacter = "Create New";
                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], createCharacter, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(createCharacter).X / 2)), (int)((item.Position.Y + 1) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(createCharacter).Y / 2))), Color.Black);
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], createCharacter, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(createCharacter).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(createCharacter).Y / 2))), Color.DarkOrange * glowFrame);
                    }
                    else
                    {
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], item.Text, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).X / 2)), (int)((item.Position.Y + 1) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).Y / 2))), Color.Black);
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).Y / 2))), Color.LightSlateGray);
                    }
                }

                if (item.Type == MenuItemType.Character)
                {
                    Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), item.Width, item.Height);
                    spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X + 2, itemLocation.Y + 8, itemLocation.Width - 4, itemLocation.Height - 16), Color.DarkOrange);
                    spriteBatch.Draw(textbox, itemLocation, Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], item.Text, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).X / 2)), (int)((item.Position.Y + 1) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).Y / 2))), Color.Black);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).Y / 2))), Color.White);

                    //MOUSE OVER HIGHLIGHT
                    if (item.Highlighted)
                    {
                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).Y / 2))), Color.DarkOrange * glowFrame);
                    }

                    if (item.Text == characters[selectedIndex].Name)
                    {
                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * 0.8f);
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.GeneralSize10].MeasureString(item.Text).Y / 2))), Color.DarkOrange * 0.8f);
                    }
                }
            }

            //MENU BUTTONS
            foreach (MenuItem item in mainMenuItems)
                switch (item.Type)
                {
                    case MenuItemType.Title:
                        {
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval24], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval24], item.Text, item.Position, Color.White);
                            break;
                        }
                    case MenuItemType.Button:
                        if (item.Type == MenuItemType.Button)
                        {
                            Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), (int)(item.Width), (int)(item.Height));
                            spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X + 2, itemLocation.Y + 8, itemLocation.Width - 4, itemLocation.Height - 16), Color.DarkOrange);
                            spriteBatch.Draw(textbox, itemLocation, Color.White);
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y + 1) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.Black);
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.White);

                            //MOUSE OVER HIGHLIGHT                     
                            if (item.Highlighted)
                            {
                                spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.DarkOrange * glowFrame);
                            }

                        }
                        break;
                    case MenuItemType.Text:
                        if (item.Text.StartsWith("ERROR"))
                        {
                            if (!string.IsNullOrEmpty(errorMessage))
                                spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], errorMessage, (item.Position - Cache.Fonts[FontType.GeneralSize10].MeasureString(errorMessage) / 2), Color.Red);
                        }
                        break;
                }

            //DIALOGBOXES - From mainMenu
            foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
            {
                if (key.Value.Visible)
                {

                    switch (key.Key)
                    {
                        default: break;
                        case MenuDialogBoxType.SelectCharacter:
                            {
                                //CHARACTER DISPLAY

                                //BACKGROUND FADER
                                Rectangle backgroundLocation = new Rectangle((width / 2) - 137 - 6, (height / 2) - 110, 290, 222);
                                spriteBatch.Draw(dialogFader, backgroundLocation, Color.White);

                                //LEFT AND RIGHT BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 134 - 6, (height / 2) - 86, 195, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) + 160 - 6, (height / 2) - 86, 195, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);

                                //TOP AND BOTTOM BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) + 104, 310, 44), null, Color.White);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) - 117, 310, 56), null, Color.White);

                                if (selectedIndex != -1 && characters != null && characters.Count > 0 && characters.Count >= selectedIndex && characters[selectedIndex] != null)
                                {
                                    Player selectedCharacter = characters[selectedIndex];

                                    selectedCharacter.Draw(spriteBatch, display, (width / 2) - 75, (height / 2) + 30, selectedCharacter.OffsetX, selectedCharacter.OffsetY);
                                    string level = string.Format("Lvl. {0}", selectedCharacter.Level);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], level, new Vector2((int)((width / 2) - 75 - (Cache.Fonts[FontType.GeneralSize10].MeasureString(level).X / 2)+1), (int)((height / 2) + 70)+1), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], level, new Vector2((int)((width / 2) - 75 - (Cache.Fonts[FontType.GeneralSize10].MeasureString(level).X / 2)), (int)((height / 2) + 70)), Color.White);
                                    //spriteBatch.DrawString(display.Fonts[FontType.MenuItem], selectedCharacter.Name, new Vector2((int)((width / 2) - 75 + 1 - (display.Fonts[FontType.MenuItem].MeasureString(selectedCharacter.Name).X / 2)), (int)((height / 2) - 70 + 1)), Color.Black);
                                    //spriteBatch.DrawString(display.Fonts[FontType.MenuItem], selectedCharacter.Name, new Vector2((int)((width / 2) - 75 - (display.Fonts[FontType.MenuItem].MeasureString(selectedCharacter.Name).X / 2)), (int)((height / 2) - 70)), Color.DarkOrange);

                                    //spriteBatch.DrawString(display.Fonts[FontType.MenuItem], string.Format("Experience: {0}", selectedCharacter.Experience), new Vector2((int)(width / 2 - 5), (int)((height / 2) - 70)), Color.White);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Allegiance: {0}", selectedCharacter.Side), new Vector2((int)(width / 2 - 5) + 1, (int)((height / 2) - 70) + 1), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Location: {0}", selectedCharacter.MapName), new Vector2((int)(width / 2 - 5) + 1, (int)((height / 2) - 50) + 1), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Strength: {0}", selectedCharacter.Strength), new Vector2((int)(width / 2 - 5) + 1, (int)((height / 2) - 30) + 1), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Dexterity: {0}", selectedCharacter.Dexterity), new Vector2((int)(width / 2 - 5) + 1, (int)((height / 2) - 10) + 1), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Vitality: {0}", selectedCharacter.Vitality), new Vector2((int)(width / 2 - 5) + 1, (int)((height / 2) + 10) + 1), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Magic: {0}", selectedCharacter.Magic), new Vector2((int)(width / 2 - 5) + 1, (int)((height / 2) + 30) + 1), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Intelligence: {0}", selectedCharacter.Intelligence), new Vector2((int)(width / 2 - 5) + 1, (int)((height / 2) + 50) + 1), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Agility: {0}", selectedCharacter.Agility), new Vector2((int)(width / 2 - 5) + 1, (int)((height / 2) + 70) + 1), Color.Black);

                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Allegiance: {0}", selectedCharacter.Side), new Vector2((int)(width / 2 - 5), (int)((height / 2) - 70)), Color.White);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Location: {0}", selectedCharacter.MapName), new Vector2((int)(width / 2 - 5), (int)((height / 2) - 50)), Color.White);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Strength: {0}", selectedCharacter.Strength), new Vector2((int)(width / 2 - 5), (int)((height / 2) - 30)), Color.White);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Dexterity: {0}", selectedCharacter.Dexterity), new Vector2((int)(width / 2 - 5), (int)((height / 2) - 10)), Color.White);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Vitality: {0}", selectedCharacter.Vitality), new Vector2((int)(width / 2 - 5), (int)((height / 2) + 10)), Color.White);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Magic: {0}", selectedCharacter.Magic), new Vector2((int)(width / 2 - 5), (int)((height / 2) + 30)), Color.White);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Intelligence: {0}", selectedCharacter.Intelligence), new Vector2((int)(width / 2 - 5), (int)((height / 2) + 50)), Color.White);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Agility: {0}", selectedCharacter.Agility), new Vector2((int)(width / 2 - 5), (int)((height / 2) + 70)), Color.White);
                                }

                                foreach (MenuItem item in selectCharacterItems)
                                {
                                    switch (item.Type)
                                    {
                                        case MenuItemType.Character:
                                            if (characters.Count > 0)
                                            {
                                                item.Text = "Character: " + characters[selectedIndex].Name;
                                                item.Width = (int)(Cache.Fonts[FontType.MagicMedieval18].MeasureString(item.Text).X + 30);
                                                item.SetPosition(new Vector2((int)((viewport.Width / 2) - (item.Width / 2)), (int)((viewport.Height / 2) - 102)));
                                                Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), (int)(item.Width), (int)(item.Height));
                                                //spriteBatch.DrawItemPopup(dialogFader, new Rectangle(itemLocation.X + 2, itemLocation.Y + 8, itemLocation.Width - 4, itemLocation.Height - 16), Color.WhiteSmoke);
                                                //spriteBatch.DrawItemPopup(textbox, itemLocation, Color.White);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval18].MeasureString(item.Text).X / 2)), (int)item.Position.Y+1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval18].MeasureString(item.Text).X / 2)), (int)item.Position.Y), Color.White);
                                            }
                                            break;
                                        case MenuItemType.Title:
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, item.Position, Color.White);
                                                break;
                                            }
                                        case MenuItemType.Button:
                                            if (item.Type == MenuItemType.Button)
                                            {
                                                Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), (int)(item.Width), (int)(item.Height));
                                                spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X + 2, itemLocation.Y + 8, itemLocation.Width - 4, itemLocation.Height - 16), Color.DarkOrange);
                                                spriteBatch.Draw(textbox, itemLocation, Color.White);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y + 1) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.White);

                                                //MOUSE OVER HIGHLIGHT                     
                                                if (item.Highlighted)
                                                {
                                                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.DarkOrange * glowFrame);
                                                    }

                                                }
                                                break;
                                        case MenuItemType.Text:
                                            if (item.Text.StartsWith("ERROR"))
                                            {
                                                if (!string.IsNullOrEmpty(errorMessage))
                                                {
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2) + 1), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2) + 1)), Color.Black);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2)), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2))), Color.Red);
                                                }
                                            }
                                            else
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + 1)), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y)), Color.White);
                                            }
                                            break;
                                    }

                                }
                                break;
                            }
                        case MenuDialogBoxType.CreateCharacter:
                            {
                                //BACKGROUND FADER
                                Rectangle backgroundLocation = new Rectangle((width / 2) - 137 - 6, (height / 2) - 110, 290, 222);
                                spriteBatch.Draw(dialogFader, backgroundLocation, Color.White);

                                //LEFT AND RIGHT BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 134 - 6, (height / 2) - 86, 195, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) + 160 - 6, (height / 2) - 86, 195, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);

                                //TOP AND BOTTOM BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) + 104, 310, 44), null, Color.White);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) - 117, 310, 56), null, Color.White);

                                newPlayer.Draw(spriteBatch, display, (width / 2) + 75, (height / 2) + 70, newPlayer.OffsetX, newPlayer.OffsetY);
                                spriteBatch.Draw(Cache.Effects[(int)SpriteId.DynamicObject + 2].Texture, new Rectangle(width / 2 + 100, height / 2, Cache.Effects[(int)SpriteId.DynamicObject + 2].Frames[flagFrame].Width , Cache.Effects[(int)SpriteId.DynamicObject + 2].Frames[flagFrame].Height), Cache.Effects[(int)SpriteId.DynamicObject + 2].Frames[flagFrame].GetRectangle(), Color.White);

                                //DrawItemPopup Current Selected Values new Vector2((int)(viewport.Width / 2 - 103), viewport.Height / 2 - 40));
                                if (selectedSide == OwnerSide.Aresden)
                                {
                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], selectedSide.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Town: ").X + 1), viewport.Height / 2 - 20 + 1), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], selectedSide.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Town: ").X), viewport.Height / 2 - 20), new Color(255, 45, 0));
                                }
                                else
                                {
                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], selectedSide.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Town: ").X + 1), viewport.Height / 2 - 20 + 1), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], selectedSide.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Town: ").X), viewport.Height / 2 - 20), new Color(0, 85, 255));
                                }

                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], selectedGender.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Gender: ").X + 1), viewport.Height / 2 + 1), Color.Black);
                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], selectedGender.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Gender: ").X), viewport.Height / 2), Color.DarkOrange);
           
                                spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], selectedSkin.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Skin Tone: ").X + 1), viewport.Height / 2 + 20 + 1), Color.Black);
                                spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], selectedSkin.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Skin Tone: ").X), viewport.Height / 2 + 20), Color.DarkOrange);
            
                                spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], selectedHairType.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Hair Style: ").X + 1), viewport.Height / 2 + 40 + 1), Color.Black);
                                spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], selectedHairType.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Hair Style: ").X), viewport.Height / 2 + 40), Color.DarkOrange);
             
                                spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], selectedHairColour.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Hair Color: ").X + 1), viewport.Height / 2 + 60 + 1), Color.Black);
                                spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], selectedHairColour.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Hair Color: ").X), viewport.Height / 2 + 60), Color.DarkOrange);
                 
                                spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], selectedUnderwearColour.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Underware: ").X + 1), viewport.Height / 2 + 80 + 1), Color.Black);
                                spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], selectedUnderwearColour.ToString(), new Vector2((int)(viewport.Width / 2 - 103 + Cache.Fonts[FontType.MagicMedieval14].MeasureString("Underware: ").X), viewport.Height / 2 + 80), Color.DarkOrange);

                                foreach (MenuItem item in createCharacterItems)
                                {
                                    switch (item.Type)
                                    {
                                        case MenuItemType.Image:
                                            {
                                                spriteBatch.Draw(item.Texture, item.Position, item.DestinationRectangle, Color.White);
                                                if (item.Highlighted)
                                                {
                                                    spriteBatch.Draw(item.Texture, item.Position, item.DestinationRectangle, Color.DarkOrange * glowFrame);
                                                }
                                                break;

                                            }
                                        case MenuItemType.Title:
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, item.Position, Color.White);
                                                break;
                                            }
                                        case MenuItemType.Button:
                                            if (item.Type == MenuItemType.Button)
                                            {
                                                Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), (int)(item.Width), (int)(item.Height));
                                                spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X + 2, itemLocation.Y + 8, itemLocation.Width - 4, itemLocation.Height - 16), Color.DarkOrange);
                                                spriteBatch.Draw(textbox, itemLocation, Color.White);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y + 1) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.White);

                                                //MOUSE OVER HIGHLIGHT                     
                                                if (item.Highlighted)
                                                {
                                                    spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.DarkOrange * glowFrame);
                                                }

                                                // SELECTED GENDER
                                                if (item.Text.Equals(selectedGender))
                                                {
                                                    spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.DarkOrange);
                                                }

                                            }
                                            break;
                                        case MenuItemType.Text:
                                            if (item.Text.StartsWith("ERROR"))
                                            {
                                                if (!string.IsNullOrEmpty(errorMessage))
                                                {
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2) + 1), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2) + 1)), Color.Black);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2)), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2))), Color.Red);
                                                }
                                            }
                                            else
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + 1)), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y)), Color.White);
                                            }
                                            break;
                                    }

                                }

                                

                                Rectangle characterBoxLocation = new Rectangle(characterNameBox.X - 8, characterNameBox.Y - 9, characterNameBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, characterBoxLocation, Color.White);

                                characterNameBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                break;
                            }
                        case MenuDialogBoxType.DeleteCharacter:
                            {
                                //BACKGROUND FADER
                                Rectangle backgroundLocation = new Rectangle((width / 2) - 137 - 6, (height / 2) - 110, 290, 222);
                                spriteBatch.Draw(dialogFader, backgroundLocation, Color.White);

                                //LEFT AND RIGHT BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 134 - 6, (height / 2) - 86, 195, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) + 160 - 6, (height / 2) - 86, 195, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);

                                //TOP AND BOTTOM BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) + 104, 310, 44), null, Color.White);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) - 117, 310, 56), null, Color.White);

                                foreach (MenuItem item in deleteCharacterItems)
                                {
                                    switch (item.Type)
                                    {
                                        case MenuItemType.Character:
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2((int)(item.Position.X + 1 - (Cache.Fonts[FontType.MagicMedieval18].MeasureString(item.Text).X / 2)), (int)(item.Position.Y + 1)), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.MagicMedieval18].MeasureString(item.Text).X / 2)), (int)(item.Position.Y)), Color.DarkOrange);
                                                break;
                                            }
                                        case MenuItemType.Title:
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, item.Position, Color.Red);
                                                break;
                                            }
                                        case MenuItemType.Button:
                                            if (item.Type == MenuItemType.Button)
                                            {
                                                Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), (int)(item.Width), (int)(item.Height));
                                                spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X + 2, itemLocation.Y + 8, itemLocation.Width - 4, itemLocation.Height - 16), Color.DarkOrange);
                                                spriteBatch.Draw(textbox, itemLocation, Color.White);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y + 1) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.White);

                                                //MOUSE OVER HIGHLIGHT                     
                                                if (item.Highlighted)
                                                    {
                                                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.DarkOrange * glowFrame);
                                                    }

                                                }
                                                break;
                                        case MenuItemType.Text:
                                            if (item.Text.StartsWith("ERROR"))
                                            {
                                                if (!string.IsNullOrEmpty(errorMessage))
                                                {
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2) + 1), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2) + 1)), Color.Black);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2)), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2))), Color.Red);
                                                }
                                            }
                                            else
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + 1)), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y)), Color.White);
                                            }
                                            break;
                                    }

                                }
                                break;
                            }
                        case MenuDialogBoxType.ChangePassword:
                            {
                                //BACKGROUND FADER
                                Rectangle backgroundLocation = new Rectangle((width / 2) - 137 - 6, (height / 2) - 110, 290, 222);
                                spriteBatch.Draw(dialogFader, backgroundLocation, Color.White);

                                //LEFT AND RIGHT BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 134 - 6, (height / 2) - 86, 195, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) + 160 - 6, (height / 2) - 86, 195, 11), null, Color.White, 1.57079633f, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 0);

                                //TOP AND BOTTOM BORDERS
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) + 104, 310, 44), null, Color.White);
                                spriteBatch.Draw(dialogBorder, new Rectangle((width / 2) - 148 - 6, (height / 2) - 117, 310, 56), null, Color.White);

                                Rectangle oldPasswordLocation = new Rectangle(oldPasswordBox.X - 8, oldPasswordBox.Y - 9, oldPasswordBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, oldPasswordLocation, Color.White);

                                Rectangle newPasswordLocation = new Rectangle(newPasswordBox.X - 8, newPasswordBox.Y - 9, newPasswordBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, newPasswordLocation, Color.White);

                                Rectangle reTypePasswordLocation = new Rectangle(reTypePasswordBox.X - 8, reTypePasswordBox.Y - 9, reTypePasswordBox.Width + 16, 20 + 16);
                                spriteBatch.Draw(textbox, reTypePasswordLocation, Color.White);

                                oldPasswordBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                newPasswordBox.Draw(spriteBatch, gameTime, Color.DarkGray);
                                reTypePasswordBox.Draw(spriteBatch, gameTime, Color.DarkGray);

                                foreach (MenuItem item in changePasswordItems)
                                {
                                    switch (item.Type)
                                    {
                                        default: break;
                                        case MenuItemType.Title:
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, new Vector2(item.Position.X + 1, item.Position.Y + 1), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], item.Text, item.Position, Color.White);
                                                break;
                                            }
                                        case MenuItemType.Button:
                                            if (item.Type == MenuItemType.Button)
                                            {
                                                Rectangle itemLocation = new Rectangle((int)(item.Position.X), (int)(item.Position.Y), (int)(item.Width), (int)(item.Height));
                                                spriteBatch.Draw(dialogFader, new Rectangle(itemLocation.X + 2, itemLocation.Y + 8, itemLocation.Width - 4, itemLocation.Height - 16), Color.DarkOrange);
                                                spriteBatch.Draw(textbox, itemLocation, Color.White);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X + 1) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y + 1) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.White);

                                                //MOUSE OVER HIGHLIGHT                     
                                                if (item.Highlighted)
                                                    {
                                                        spriteBatch.Draw(textbox, itemLocation, Color.DarkOrange * glowFrame);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)((item.Position.X) + (itemLocation.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X / 2)), (int)((item.Position.Y) + (itemLocation.Height / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y / 2))), Color.DarkOrange * glowFrame);
                                                    }

                                                }
                                                break;
                                        case MenuItemType.Text:
                                            if (item.Text.StartsWith("ERROR"))
                                            {
                                                if (!string.IsNullOrEmpty(errorMessage))
                                                {
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2) + 1), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2) + 1)), Color.Black);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], errorMessage, new Vector2((int)(item.Position.X - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).X / 2)), (int)(item.Position.Y - (Cache.Fonts[FontType.DamageMediumSize13].MeasureString(errorMessage).Y / 2))), Color.Red);
                                                }
                                            }
                                            else
                                            {
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + 1)), Color.Black);
                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y)), Color.White);
                                            }
                                            break;
                                    }

                                }

                                break;
                            }                     
                    }
                }
            }

            /* DIALOG BOXES */
            highlightedDialogBox = MenuDialogBoxType.None; // checks mouse/dialog collisions
            if (dialogBoxes.Count > 0)
                foreach (MenuDialogBoxType dialogType in dialogBoxDrawOrder)
                {
                    IMenuDialogBox box = dialogBoxes[dialogType];

                    if (box.Visible && box.Draw(spriteBatch, gameTime))
                       highlightedDialogBox = box.Type;
                }

            // DRAW MOUSE
            display.Mouse.Draw(spriteBatch);
        }

        private void HandleKeyboard()
        {
            /* PRESS ESCAPE */
            if (display.Keyboard.Esc) display.Keyboard.KeyCheck[Keys.Escape] = true;
            if (!display.Keyboard.Esc && display.Keyboard.KeyCheck[Keys.Escape])
            {
                if (loginServer != null) loginServer.Close();
                back = true;
                display.Keyboard.KeyCheck[Keys.Escape] = false;
            }
        }

        private void HandleMouse()
        {
            //if (display.Mouse.IsDragging){//display.Mouse.LeftClick = false;}
            if (display.Mouse.LeftClick)
            {

                foreach (MenuItem item in characterItems)
                    if (item.Type == MenuItemType.Character && item.Selected)
                    {
                        if (!dialogBoxes[MenuDialogBoxType.SelectCharacter].Visible)
                            foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                            {
                                if (key.Key != MenuDialogBoxType.SelectCharacter)
                                {
                                    key.Value.Hide();
                                }
                            }
                        dialogBoxes[MenuDialogBoxType.SelectCharacter].Show();
                        item.Selected = false;
                        //Display.Mouse.LeftClick = false;
                        break;
                    }
                //MAIN MENU
                foreach (MenuItem item in mainMenuItems)
                    if (item.Type == MenuItemType.Button && item.Selected)
                    {
                        switch (item.Text)
                        {
                            case "New Character": //TODO code create character - create a new method for this as we need to use the same function for when players click on empty character spaces.
                                {
                                    if (dialogBoxes[MenuDialogBoxType.CreateCharacter].Visible)
                                    {
                                        dialogBoxes[MenuDialogBoxType.CreateCharacter].Hide();
                                        dialogBoxes[MenuDialogBoxType.SelectCharacter].Show();
                                    }
                                    else
                                    {
                                        foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                                        {
                                            if (key.Key != MenuDialogBoxType.CreateCharacter)
                                            {
                                                key.Value.Hide();
                                            }
                                        }
                                        dialogBoxes[MenuDialogBoxType.CreateCharacter].Show();
                                        Display.Keyboard.Dispatcher.Subscriber = characterNameBox;
                                    }

                                    //Randomize Settings
                                    selectedSide = (OwnerSide)Dice.Roll(1, 2);
                                    selectedHairType = (Dice.Roll(1, 4) - 1);
                                    selectedHairColour = (Dice.Roll(1, 15) - 1);
                                    selectedUnderwearColour = (Dice.Roll(1, 4) - 1);
                                    selectedSkin = Dice.Roll(1, 3);
                                    selectedGender = (GenderType)Dice.Roll(1,2);
                                    selectedUpdated = true;

                                    flagSide = selectedSide;

                                    if (flagSide == OwnerSide.Aresden)
                                    {
                                        newPlayer.Inventory[(int)EquipType.Back] = Cache.ItemConfiguration[226].Copy();
                                    }
                                    else
                                    {
                                        newPlayer.Inventory[(int)EquipType.Back] = Cache.ItemConfiguration[227].Copy();
                                    }

                                    item.Selected = false;
                                    //Display.Mouse.LeftClick = false;
                                    break;
                                }
                            case "Change Password":
                                {
                                    if (dialogBoxes[MenuDialogBoxType.ChangePassword].Visible)
                                    {
                                        dialogBoxes[MenuDialogBoxType.ChangePassword].Hide();
                                        dialogBoxes[MenuDialogBoxType.SelectCharacter].Show();
                                    }
                                    else
                                    {
                                        foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                                        {
                                            if (key.Key != MenuDialogBoxType.ChangePassword)
                                            {
                                                key.Value.Hide();
                                            }
                                        }
                                        dialogBoxes[MenuDialogBoxType.ChangePassword].Show();
                                    }
                                    item.Selected = false;
                                    //Display.Mouse.LeftClick = false;
                                    break;
                                }
                            case "Log Out":
                                {
                                    if (loginServer != null) loginServer.Close();
                                    back = true;
                                    item.Selected = false;
                                    //display.Mouse.LeftClick = false;
                                    break;
                                }
                            default: break;
                        }
                    }

                //DIALOG BOX VISIBLE
                foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> key in dialogBoxes)
                {
                    if (key.Value.Visible)
                    {
                        switch (key.Key)
                        {
                            default: break;
                            case MenuDialogBoxType.SelectCharacter:
                                {
                                    foreach (MenuItem item in selectCharacterItems)
                                        if (item.Type == MenuItemType.Button && item.Selected)
                                        {
                                            switch (item.Text)
                                            {
                                                case "Play Game":
                                                    if (characters.Count > 0)
                                                    {
                                                        item.Selected = false;
                                                        //display.Mouse.LeftClick = false;
                                                        selectedCharacter = characters[selectedIndex].Name;

                                                        try
                                                        {
                                                            /* INITIALIZE LOGIN SERVER TCP CONNECTION */
                                                            if (loginServer == null)
                                                            {
                                                                Socket socket = Sockets.CreateTCPSocket(loginAddress, loginPort);
                                                                loginServer = new ClientInfo(socket, true);
                                                                loginServer.EncryptionType = EncryptionType.None;
                                                                loginServer.OnReadBytes += new ConnectionReadBytes(ReadMessageFromLoginServer);
                                                            }

                                                            byte[] command = new byte[220];
                                                            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestEnterGame), 0, command, 0, 4);
                                                            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Confirm), 0, command, 4, 2);
                                                            Buffer.BlockCopy(selectedCharacter.GetBytes(10), 0, command, 6, 10);
                                                            Buffer.BlockCopy(characters[selectedIndex].MapName.GetBytes(10), 0, command, 16, 10); //TODO Maybe bug?
                                                            Buffer.BlockCopy("username".GetBytes(10), 0, command, 26, 10);
                                                            Buffer.BlockCopy("password".GetBytes(10), 0, command, 36, 10);
                                                            Buffer.BlockCopy((1).GetBytes(), 0, command, 46, 4);
                                                            Buffer.BlockCopy("world".GetBytes(30), 0, command, 50, 30);
                                                            Buffer.BlockCopy("token".GetBytes(120), 0, command, 80, 120);

                                                            byte[] data = Utility.Encrypt(command, false);
                                                            loginServer.Send(data);

                                                            errorMessage = "Connecting...";
                                                        }
                                                        catch
                                                        {
                                                            errorMessage = "Login Server is Unvailable.";
                                                        }
                                                    }
                                                    break;
                                                case "Delete":
                                                    if (characters.Count > 0)
                                                    {
                                                        if (dialogBoxes[MenuDialogBoxType.DeleteCharacter].Visible)
                                                        {
                                                            dialogBoxes[MenuDialogBoxType.DeleteCharacter].Hide();
                                                            dialogBoxes[MenuDialogBoxType.SelectCharacter].Show();
                                                        }
                                                        else
                                                        {
                                                            foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                            {
                                                                if (box.Key != MenuDialogBoxType.DeleteCharacter)
                                                                {
                                                                    box.Value.Hide();
                                                                }
                                                            }
                                                            dialogBoxes[MenuDialogBoxType.DeleteCharacter].Show();
                                                        }
                                                        item.Selected = false;
                                                        //Display.Mouse.LeftClick = false;
                                                    }
                                                    break;
                                                default: break;
                                            }
                                        }
                                    break;
                                }
                            case MenuDialogBoxType.CreateCharacter:
                                {
                                    foreach (MenuItem item in createCharacterItems)
                                    {
                                        if (item.Type == MenuItemType.Image && item.Selected)
                                        {
                                            switch (item.Text)
                                            {
                                                case "LeftTown":
                                                    {
                                                        if (selectedSide == OwnerSide.Elvine)
                                                        {
                                                            flagSide = selectedSide = OwnerSide.Aresden;
                                                            newPlayer.Inventory[(int)EquipType.Back] = Cache.ItemConfiguration[226].Copy();
                                                        }
                                                        else
                                                        {
                                                            flagSide = selectedSide = OwnerSide.Elvine;
                                                            newPlayer.Inventory[(int)EquipType.Back] = Cache.ItemConfiguration[227].Copy();
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "RightTown":
                                                    {
                                                        if (selectedSide == OwnerSide.Elvine)
                                                        {
                                                            flagSide = selectedSide = OwnerSide.Aresden;
                                                            newPlayer.Inventory[(int)EquipType.Back] = Cache.ItemConfiguration[226].Copy();
                                                        }
                                                        else
                                                        {
                                                            flagSide = selectedSide = OwnerSide.Elvine;
                                                            newPlayer.Inventory[(int)EquipType.Back] = Cache.ItemConfiguration[227].Copy();
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "LeftGender":
                                                    {
                                                        if (selectedGender == GenderType.Male)
                                                        {
                                                            selectedGender = GenderType.Female;
                                                        }
                                                        else
                                                        {
                                                            selectedGender = GenderType.Male;
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "RightGender":
                                                    {
                                                        if (selectedGender == GenderType.Male)
                                                        {
                                                            selectedGender = GenderType.Female;
                                                        }
                                                        else
                                                        {
                                                            selectedGender = GenderType.Male;
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "LeftSkinTone":
                                                    {
                                                        if (selectedSkin <= 1)
                                                        {
                                                            selectedSkin = 3;
                                                        }
                                                        else
                                                        {
                                                            selectedSkin = selectedSkin - 1;
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "RightSkinTone":
                                                    {
                                                        if (selectedSkin >= 3)
                                                        {
                                                            selectedSkin = 1;
                                                        }
                                                        else
                                                        {
                                                            selectedSkin = selectedSkin + 1;
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "LeftHairStyle":
                                                    {
                                                        if (selectedHairType == 0)
                                                        {
                                                            selectedHairType = 3;
                                                        }
                                                        else
                                                        {
                                                            selectedHairType = selectedHairType - 1;
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "RightHairStyle":
                                                    {
                                                        if (selectedHairType == 3)
                                                        {
                                                            selectedHairType = 0;
                                                        }
                                                        else
                                                        {
                                                            selectedHairType = selectedHairType + 1;
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "LeftHairColor":
                                                    {
                                                        if (selectedHairColour == 0)
                                                        {
                                                            selectedHairColour = 14;
                                                        }
                                                        else
                                                        {
                                                            selectedHairColour = selectedHairColour - 1;
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "RightHairColor":
                                                    {
                                                        if (selectedHairColour == 14)
                                                        {
                                                            selectedHairColour = 0;
                                                        }
                                                        else
                                                        {
                                                            selectedHairColour = selectedHairColour + 1;
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "LeftUnderware":
                                                    {
                                                        if (selectedUnderwearColour == 0)
                                                        {
                                                            selectedUnderwearColour = 3;
                                                        }
                                                        else
                                                        {
                                                            selectedUnderwearColour = selectedUnderwearColour - 1;
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                                case "RightUnderware":
                                                    {
                                                        if (selectedUnderwearColour == 3)
                                                        {
                                                            selectedUnderwearColour = 0;
                                                        }
                                                        else
                                                        {
                                                            selectedUnderwearColour = selectedUnderwearColour + 1;
                                                        }
                                                        selectedUpdated = true;
                                                        break;
                                                    }
                                            }
                                            item.Selected = false;
                                            //Display.Mouse.LeftClick = false;
                                        }

                                        if (item.Type == MenuItemType.Button && item.Selected)
                                        {
                                            switch (item.Text)
                                            {
                                                case "Create":
                                                    if (characterNameBox.Text.Length < 1) break;
                                                    NewCharacter();
                                                    break;
                                                case "Cancel":
                                                    {
                                                        dialogBoxes[MenuDialogBoxType.CreateCharacter].Hide();
                                                        dialogBoxes[MenuDialogBoxType.SelectCharacter].Show();
                                                        break;
                                                    }
                                            }
                                            item.Selected = false;
                                            //Display.Mouse.LeftClick = false;
                                        }
                                    }
                                    foreach (TextBox box in createCharacterTextBoxes)
                                    {
                                        if (display.Mouse.X >= box.X && display.Mouse.X <= box.X + box.Width &&
                                            display.Mouse.Y >= box.Y & display.Mouse.Y <= box.Y + box.Height)
                                        {
                                            display.Keyboard.Dispatcher.Subscriber = box;
                                            //Display.Mouse.LeftClick = false;
                                        }
                                    }

                                    break;
                                }
                            case MenuDialogBoxType.DeleteCharacter:
                                {
                                    foreach (MenuItem item in deleteCharacterItems)
                                        if (item.Type == MenuItemType.Button && item.Selected)
                                        {
                                            switch (item.Text)
                                            {
                                                case "Yes": 
                                                    DeleteCharacter();                                                   
                                                    item.Selected = false;
                                                    //Display.Mouse.LeftClick = false; 
                                                    break;
                                                case "No":
                                                    {
                                                        if (dialogBoxes[MenuDialogBoxType.DeleteCharacter].Visible)
                                                        {
                                                            dialogBoxes[MenuDialogBoxType.DeleteCharacter].Hide();
                                                            dialogBoxes[MenuDialogBoxType.SelectCharacter].Show();
                                                        }
                                                        else
                                                        {
                                                            foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in dialogBoxes)
                                                            {
                                                                if (box.Key != MenuDialogBoxType.SelectCharacter)
                                                                {
                                                                    box.Value.Hide();
                                                                }
                                                            }
                                                            dialogBoxes[MenuDialogBoxType.SelectCharacter].Show();
                                                        }
                                                        item.Selected = false;
                                                        //Display.Mouse.LeftClick = false;
                                                        break;
                                                    }
                                                default: break;
                                            }
                                        }
                                    break;
                                }
                            case MenuDialogBoxType.ChangePassword:
                                {
                                    break;
                                }
                        }
                    }
                }

            }
        }

        public void BringToFront(MenuDialogBoxType type)
        {
            dialogBoxDrawOrder.Remove(type);
            dialogBoxDrawOrder.AddLast(type);
        }

        public void AddEvent(string message)
        {
            throw new NotImplementedException();
        }

        private void ServerProcess(byte[] data, int identity, string ipAddress)
        {
            try
            {
                int typeTemp = (int)BitConverter.ToInt16(data, 4);
                CommandType type;

                if (Enum.TryParse<CommandType>(typeTemp.ToString(), out type))
                {
                    switch (type)
                    {
                        case CommandType.EnterGameSucceed:
                            gameAddress = Encoding.ASCII.GetString(data, 6, 16).Trim('\0');
                            gamePort = BitConverter.ToInt16(data, 22);
                            // worldName = Encoding.ASCII.GetString(data, 24, 20);

                            loginServer.Close();
                            isComplete = true;
                            break;
                        case CommandType.EnterGameFailed: errorMessage = "Requested map not loaded"; break;
                        case CommandType.EnterGameAlreadyLoggedIn: errorMessage = "Character already logged in."; break;
                        case CommandType.NewCharacterCreated:
                            Player p = new Player(account, "", characterNameBox.Text);
                            p.Side = selectedSide;
                            p.Type = selectedType;
                            p.Level = 1;
                            p.Experience = 10;
                            p.Strength = 10;
                            p.Vitality = 10;
                            p.Dexterity = 10;
                            p.Intelligence = 10;
                            p.Magic = 10;
                            p.Agility = 10;
                            p.MapName = "cityhall_2";
                            p.Appearance1 = selectedAppearance1;                    
                            characters.Add(p);
                      
                            //selectedHairColour = selectedHairType = selectedSkin = selectedUnderwearColour = 0;
                            //selectedGender = GenderType.Male;
                            selectedIndex = characters.Count - 1;

                            Init(null); 
                            errorMessage = "";
                            break;
                        case CommandType.DeleteCharacterFailed: errorMessage = "Delete character failed."; break;
                        case CommandType.DeleteCharacterSucceed:
                            for (int i = 0; i < characters.Count; i++)
                                if (characters[i].Name.Equals(deletedName))
                                {
                                    characters.RemoveAt(i);
                                    characterItems.RemoveAt(i);
                                }
                            selectedIndex = characters.Count - 1;

                            Init(null); 
                            errorMessage = "";
                            break;
                        case CommandType.NewCharacterAlreadyExists: errorMessage = "Character name already exists!"; break;
                        case CommandType.NewCharacterFailed: errorMessage = "Character creation failed."; break;
                        default: errorMessage = "Unable to connect."; break;
                    }
                }
            }
            catch
            {

            }
        }

        private void ReadMessageFromLoginServer(ClientInfo ci, byte[] data, int len)
        {
            try
            {
                lock (processQueueLock)
                {
                    // find out if there is more than 1 packet here, if there is we need to split them up
                    // and process them individually. this is by nature of the way TCP works using streams
                    bool allPacketsProcessed = false;
                    int pointer = 0; // packet start pointer
                    while (!allPacketsProcessed)
                    {
                        int size = BitConverter.ToInt16(data, pointer + 1); // data[1] and data[2] - size of the packet
                        byte[] packetData = new byte[size];
                        Buffer.BlockCopy(data, pointer, packetData, 0, size);

                        if (packetData.Length > 2)
                        {
                            Command command = new Command(packetData, ci.ID);
                            processQueue.Enqueue(command);
                        }

                        // check if another header can be found. if so, check that its size > 0. if not, all packets processed
                        pointer += size + 3; // set the pointer to the index start of the next packet
                        if ((len >= pointer)
                            && BitConverter.ToInt16(data, pointer + 1) > 0)
                        { }
                        else allPacketsProcessed = true; // we're all good
                    }
                }
            }
            catch
            {
            }
        }

        public void AddEffect(IGameEffect e) { AddEffect(e, -1, -1); }
        public void AddEffect(IGameEffect e, int pivotX, int pivotY)
        {
            
        }

        public bool IsComplete { get { return isComplete; } }
        public bool Back { get { return back; } }
        public DefaultState State { get { return DefaultState.CharacterSelect; } }
        public GameDisplay Display { get { return display; } set { display = value; } }
        public GraphicsDevice GraphicsDevice { get { return display.Device; } }

        public BackGroundImage Background { get { return background; } set { background = value; } }
        public BackGroundImage Backgroundblackbar { get { return backgroundblackbar; } set { backgroundblackbar = value; } }
        public BackGroundImage Legs { get { return legs; } set { legs = value; } }
        public BackGroundImage Weapon { get { return weapon; } set { weapon = value; } }
        public BackGroundImage Tree { get { return tree; } set { tree = value; } }
        public BackGroundImage Grass { get { return grass; } set { grass = value; } }
        public BackGroundImage Grass2 { get { return grass2; } set { grass2 = value; } }
        public BackGroundImage Grass3 { get { return grass3; } set { grass3 = value; } }
        public BackGroundImage Smoke1 { get { return smoke1; } set { smoke1 = value; } }
        public BackGroundImage Smoke2 { get { return smoke2; } set { smoke2 = value; } }
        public BackGroundImage Smoke3 { get { return smoke3; } set { smoke3 = value; } }
        public BackGroundImage Smoke4 { get { return smoke4; } set { smoke4 = value; } }
        public BackGroundImage Smoke5 { get { return smoke5; } set { smoke5 = value; } }
        public BackGroundImage Smoke6 { get { return smoke6; } set { smoke6 = value; } }
        public BackGroundImage Smoke7 { get { return smoke7; } set { smoke7 = value; } }
        public BackGroundImage Smoke8 { get { return smoke8; } set { smoke8 = value; } }
        public BackGroundImage Logo { get { return logo; } set { logo = value; } }
        public BackGroundImage LogoGlow { get { return logoglow; } set { logoglow = value; } }
        public BackGroundImage WarriorHead { get { return warriorhead; } set { warriorhead = value; } }
        public BackGroundImage WarriorBody { get { return warriorbody; } set { warriorbody = value; } }
        public Texture2D Corner { get { return corner; } set { corner = value; } }
        public Texture2D Textbox { get { return textbox; } set { textbox = value; } }
        public Texture2D DialogFader { get { return dialogFader; } set { dialogFader = value; } }
        public Texture2D DialogBorder { get { return dialogBorder; } set { dialogBorder = value; } }
        public Texture2D BG { get { return bg; } set { bg = value; } }

        public LinkedList<BackGroundImage> BackgroundImages { get { return backgroundImages; } set { backgroundImages = value; } }


        public string SelectedCharacter { get { return selectedCharacter; } }
        public string GameServerAddress { get { return gameAddress; } }
        public int GameServerPort { get { return gamePort; } }
        public List<IGameEffect> Effects { get { return null; } set { } }
        public Dictionary<MenuDialogBoxType, IMenuDialogBox> DialogBoxes { get { return dialogBoxes; } }
        public LinkedList<MenuDialogBoxType> DialogBoxDrawOrder { get { return dialogBoxDrawOrder; } set { dialogBoxDrawOrder = value; } }
        public List<MenuItem> SelectCharacterItems { get { return selectCharacterItems; } set { selectCharacterItems = value; } }
        public List<MenuItem> CreateCharacterItems { get { return createCharacterItems; } set { createCharacterItems = value; } }
        public List<MenuItem> DeleteCharacterItems { get { return deleteCharacterItems; } set { deleteCharacterItems = value; } }
        public List<MenuItem> ChangePasswordItems { get { return changePasswordItems; } set { changePasswordItems = value; } }
        public bool ResolutionChange { get { return resolutionChange; } set { resolutionChange = value; } }
        public RenderTarget2D MainRenderTarget { get { return mainRenderTarget; } set { mainRenderTarget = value; } }
        public MenuDialogBoxType ClickedDialogBox { get { return clickedDialogBox; } set { clickedDialogBox = value; } }
    }
}
