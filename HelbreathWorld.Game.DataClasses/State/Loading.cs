﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using HelbreathWorld.Common;
using HelbreathWorld.Game.Assets.UI;
using HelbreathWorld.Game.Assets.Effects;

namespace HelbreathWorld.Game.Assets.State
{
    public class Loading : IDefaultState, IMenuState
    {
        private RenderTarget2D mainRenderTarget;
        private GameDisplay display;
        private TextureLoader textureLoader;
        private MenuDialogBoxType clickedDialogBox;

        private bool isInitialized;
        private bool isComplete;
        private bool back;
        private int percentage;
        private bool updated;

        private int millisecondsPerFrame = 50; //update draw
        private double timeSinceLastUpdate = 0; //Accumulate the elapsed time

        private bool resolutionChange = false;

        //BACKGROUND
        private LinkedList<BackGroundImage> backgroundImages;
        private BackGroundImage[] backgroundcollection;
        private BackGroundImage background;
        private BackGroundImage backgroundblackbar;
        private BackGroundImage legs;
        private BackGroundImage weapon;
        private BackGroundImage tree;
        private BackGroundImage grass;
        private BackGroundImage grass2;
        private BackGroundImage grass3;
        private BackGroundImage smoke1;
        private BackGroundImage smoke2;
        private BackGroundImage smoke3;
        private BackGroundImage smoke4;
        private BackGroundImage smoke5;
        private BackGroundImage smoke6;
        private BackGroundImage smoke7;
        private BackGroundImage smoke8;
        private BackGroundImage logo;
        private BackGroundImage logoglow;
        private BackGroundImage warriorhead;
        private BackGroundImage warriorbody;

        private Texture2D corner;
        private Texture2D textbox;
        private Texture2D dialogFader;
        private Texture2D dialogBorder;

        private bool fadeDirection;
        private bool fadeStage;
        private int fadeDelay = 10;
        private int backgroundDelay = 16;

        // loading animations
        private MotionDirection animationDirection;
        private int bodyIndex;
        private int headIndex;
        private int playerAnim;
        private int playerFrame;
        private DateTime playerTime;
        private int effectAnim;
        private int effectFrame;
        private DateTime effectTime;
        private Vector2 effectTarget;
        private int effectAnim2;
        private int effectFrame2;
        private DateTime effectTime2;
        private Vector2 effectTarget2;
        private string loadingStage;
        private object loadingLock = new object();

        //DIALOG BOXES
        private LinkedList<MenuDialogBoxType> dialogBoxDrawOrder;
        private Dictionary<MenuDialogBoxType, IMenuDialogBox> dialogBoxes;

        public Loading(bool updated)
        {
            this.updated = updated;
        }

        public void Init(GameWindow window)
        {
            Viewport viewport = GraphicsDevice.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            int height = viewport.Height;
            int width = viewport.Width;

            Display.Mouse.State = GameMouseState.Normal;

            if (updated) { Thread t = new Thread(new ThreadStart(Load)); t.Start(); }

            isInitialized = true;
        }

        public void ShowMessageBox(MenuMessageBox box)
        {

        }

        public void HideMessageBox(MenuDialogBoxType type)
        {

        }

        public void Update(GameTime gameTime, Microsoft.Xna.Framework.Game game)
        {

            if (!isInitialized) return;
            //BACKGROUND
            backgroundDelay -= gameTime.ElapsedGameTime.Milliseconds;
            if (backgroundDelay <= 0)
            {
                backgroundDelay = 16;
                foreach (BackGroundImage image in backgroundImages)
                {
                    if (image.Type == BackgroundType.Background)
                    {
                        fadeDelay -= gameTime.ElapsedGameTime.Milliseconds;
                        if (fadeDelay <= 0)
                        {
                            fadeDelay = 10;
                            image.Color = SpriteHelper.Fade(image.Color, ref fadeDirection, ref fadeStage);
                        }

                    }

                    if (image.Type == BackgroundType.Smoke || image.Type == BackgroundType.Smokebehind)
                    {
                        if (image.Drawposition.X <= -1919 || image.Drawposition.X >= 1919)
                        {
                            image.Drawposition = new Vector2(0, image.Drawposition.Y);
                        }
                        else
                        {
                            image.ShiftRight(image);
                        }
                    }

                    if (image.Type == BackgroundType.Midground || image.Type == BackgroundType.Foreground)
                    {
                        if (image.Drawposition.X <= -1919 || image.Drawposition.X >= 1919)
                        {
                            image.Drawposition = new Vector2(0, image.Drawposition.Y);
                        }
                        else
                        {
                            image.ShiftLeft(image);
                        }
                    }

                    if (image.Type == BackgroundType.Weapon)
                    {
                        if (image.Counter < 80)
                        {
                            if (image.Counter == 79)
                            {
                                image.ShiftUp(image, 0.1f);
                                image.Counter = 81;
                            }
                            else
                            {
                                image.ShiftUp(image, 0.1f);
                                image.Rotate(image, 0.001f);
                                image.Counter++;
                            }
                        }
                        else if (image.Counter > 80)
                        {
                            if (image.Counter == 160)
                            {
                                image.ShiftDown(image, 0.1f);
                                image.Counter = 0;
                            }
                            else
                            {
                                image.ShiftDown(image, 0.1f);
                                image.Rotate(image, -0.001f);
                                image.Counter++;
                            }
                        }
                    }

                    if (image.Type == BackgroundType.Body)
                    {
                        if (image.Counter < 80)
                        {
                            if (image.Counter == 79)
                            {
                                image.ShiftUp(image, 0.025f);
                                image.Counter = 81;
                            }
                            else
                            {
                                image.ShiftUp(image, 0.025f);
                                image.Scale = image.Scale + 0.0008f;
                                image.ShiftLeft(image);
                                image.Counter++;
                            }
                        }
                        else if (image.Counter > 80)
                        {
                            if (image.Counter == 160)
                            {
                                image.ShiftDown(image, 0.025f);
                                image.Counter = 0;
                            }
                            else
                            {
                                image.ShiftDown(image, 0.025f);
                                image.Scale = image.Scale - 0.0008f;
                                image.ShiftRight(image);
                                image.Counter++;
                            }
                        }
                    }

                    if (image.Type == BackgroundType.Grass)
                    {
                        if (image.Counter < 40)
                        {
                            if (image.Counter == 39)
                            {
                                image.ShiftDown(image, 0.02f);
                                image.ShiftLeft(image);
                                image.Counter = 41;
                            }
                            else
                            {
                                image.ShiftDown(image, 0.02f);
                                image.ShiftLeft(image);
                                image.Counter++;
                            }
                        }
                        else if (image.Counter > 40)
                        {
                            if (image.Counter == 80)
                            {
                                image.ShiftUp(image, 0.02f);
                                image.ShiftRight(image);
                                image.Counter = 0;
                            }
                            else
                            {
                                image.ShiftUp(image, 0.02f);
                                image.ShiftRight(image);
                                image.Counter++;
                            }
                        }
                    }
                }
            }

            display.Mouse.Update(Mouse.GetState());

            if (percentage >= 100)
            {
                isComplete = true;
                GC.Collect(); // cleanup memory
            }

        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (!isInitialized) return;

            Viewport viewport = GraphicsDevice.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            int height = viewport.Height;
            int width = viewport.Width;

            //CREATE AN ORDERED ARRAY OF BACKGROUNDIMAGES
            backgroundcollection = new BackGroundImage[backgroundImages.Count];
            backgroundImages.CopyTo(backgroundcollection, 0);

            //ITERATE THROUGH ARRAY DRAWING BACKGROUNDIMAGES   
            for (int a = 0; a < backgroundcollection.Length; a++)
            {
                BackGroundImage image = backgroundcollection[a];
                {
                    if (image.Drawposition.X == 0)
                    {
                        spriteBatch.Draw(image.Texture, image.Drawposition, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0);
                    }
                    else
                    {
                        if (image.Drawposition.X < 0)
                        {
                            Vector2 after = new Vector2(image.Drawposition.X + 1918, image.Drawposition.Y);
                            spriteBatch.Draw(image.Texture, image.Drawposition, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //Second       
                            spriteBatch.Draw(image.Texture, after, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //Third                     
                        }
                        else
                        {
                            Vector2 before = new Vector2(image.Drawposition.X - 1918, image.Drawposition.Y);
                            spriteBatch.Draw(image.Texture, before, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //First                       
                            spriteBatch.Draw(image.Texture, image.Drawposition, image.Source, image.Color, image.Rotation, image.Origin, image.Scale, SpriteEffects.None, 0); //Second
                        }
                    }
                }
            }

            // DRAW LOADING TEXT
            Vector2 textSize = Cache.Fonts[FontType.MagicMedieval40].MeasureString("Helbreath Champions");
            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval40], "Helbreath Champions", new Vector2((int)(((viewportSize - textSize) / 2).X + 1), (int)(65 + 1)), Microsoft.Xna.Framework.Color.Black);
            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval40], "Helbreath Champions", new Vector2((int)(((viewportSize - textSize) / 2).X), (int)(65)), Microsoft.Xna.Framework.Color.White);


            string message = "Loading";
            textSize = Cache.Fonts[FontType.MagicMedieval14].MeasureString(message);
            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], message, new Vector2((int)(((viewportSize - textSize) / 2).X - 8), (int)(viewport.Height - 33)), Microsoft.Xna.Framework.Color.White);
            spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], ":  " + percentage + "%", new Vector2((int)(((viewportSize - textSize) / 2).X + textSize.X - 8), (int)(viewport.Height - 36)), Microsoft.Xna.Framework.Color.White);

                if (!updated)
                {
                    message = "To load client";
                    textSize = Cache.Fonts[FontType.MagicMedieval18].MeasureString(message);
                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], message, new Vector2((int)(((viewportSize - textSize) / 2).X + 1), (int)((viewport.Height / 2) - (textSize.Y / 2)) + 1), Microsoft.Xna.Framework.Color.Black);
                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], message, new Vector2((int)(((viewportSize - textSize) / 2).X), (int)((viewport.Height / 2) - (textSize.Y / 2))), Microsoft.Xna.Framework.Color.White);

                    message = "Launch from updater";
                    textSize = Cache.Fonts[FontType.MagicMedieval18].MeasureString(message);
                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], message, new Vector2((int)(((viewportSize - textSize) / 2).X + 1), (int)(((viewportSize - textSize) / 2).Y) + 30 + 1), Microsoft.Xna.Framework.Color.Black);
                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval18], message, new Vector2((int)(((viewportSize - textSize) / 2).X), (int)(((viewportSize - textSize) / 2).Y) + 30), Microsoft.Xna.Framework.Color.White);
                }


            //DRAW DECORATIVE EDGE
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15 + 1, 0 + 1, 164, 168), null, Microsoft.Xna.Framework.Color.Black, 0, new Vector2(0, 0), SpriteEffects.FlipVertically, 1); //Top left black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15, 0, 164, 168), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipVertically, 1); //Top left
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15 + 1, 0 + 1, 164, 168), null, Microsoft.Xna.Framework.Color.Black, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 1); //Top right black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15, 0, 164, 168), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically, 1); //Top right
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15 + 1, height - 168 + 1, 164, 168), Microsoft.Xna.Framework.Color.Black); //Button left black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(-15, height - 168, 164, 168), Microsoft.Xna.Framework.Color.White); //Button left
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15 + 1, height - 168 + 1, 164, 168), null, Microsoft.Xna.Framework.Color.Black, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 1); //Button right black
            spriteBatch.Draw(corner, new Microsoft.Xna.Framework.Rectangle(width - 164 + 15, height - 168, 164, 168), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 1); //Button right

            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "Version " + Assembly.GetExecutingAssembly().GetName().Version, new Vector2(355 + 1, 115 + 1), Microsoft.Xna.Framework.Color.Black);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "Version " + Assembly.GetExecutingAssembly().GetName().Version, new Vector2(355, 115), Microsoft.Xna.Framework.Color.White);

            // DRAW MOUSE
            display.Mouse.Draw(spriteBatch);
        }


        private void Load()
        {
            LoadAnimationMetaData();

            for (int i = 0; i < 100; i++)
                ThreadPool.QueueUserWorkItem(new WaitCallback(Load), i);
        }

        private void Load(object stage) { Load((int)stage); }
        private void Load(int stage)
        {
            switch (stage)
            {
                case 0:
                    loadingStage = "Humans";
                    LoadSprite(SpriteType.Human, "Sprites\\bm.spr", 100, 96);
                    LoadSprite(SpriteType.Human, "Sprites\\wm.spr", 200, 96);
                    LoadSprite(SpriteType.Human, "Sprites\\ym.spr", 300, 96);
                    LoadSprite(SpriteType.Human, "Sprites\\bw.spr", 400, 96);
                    LoadSprite(SpriteType.Human, "Sprites\\ww.spr", 500, 96);
                    LoadSprite(SpriteType.Human, "Sprites\\yw.spr", 600, 96);
                    break;
                case 1:
                    loadingStage = "Map Tiles";
                    LoadSprite(SpriteType.Tiles, "Sprites\\maptiles1.spr", 0, 32);
                    LoadSprite(SpriteType.Tiles, "Sprites\\structures1.spr", 51, 5);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Sinside1.spr", 70, 27);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Trees1.spr", 100, 46);
                    LoadSprite(SpriteType.Tiles, "Sprites\\TreeShadows.spr", 150, 46);
                    break;
                case 2:
                    LoadSprite(SpriteType.Tiles, "Sprites\\objects1.spr", 200, 10);
                    LoadSprite(SpriteType.Tiles, "Sprites\\objects2.spr", 211, 5);
                    LoadSprite(SpriteType.Tiles, "Sprites\\objects3.spr", 216, 4);
                    LoadSprite(SpriteType.Tiles, "Sprites\\objects4.spr", 220, 2);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile223-225.spr", 223, 3);
                    break;
                case 3:
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile226-229.spr", 226, 4);
                    LoadSprite(SpriteType.Tiles, "Sprites\\objects5.spr", 230, 9);
                    LoadSprite(SpriteType.Tiles, "Sprites\\objects6.spr", 238, 4);
                    LoadSprite(SpriteType.Tiles, "Sprites\\objects7.spr", 242, 7);
                    LoadSprite(SpriteType.Tiles, "Sprites\\maptiles2.spr", 300, 15);
                    break;
                case 4:
                    LoadSprite(SpriteType.Tiles, "Sprites\\maptiles4.spr", 320, 10);
                    LoadSprite(SpriteType.Tiles, "Sprites\\maptiles5.spr", 330, 19);
                    LoadSprite(SpriteType.Tiles, "Sprites\\maptiles6.spr", 349, 4);
                    LoadSprite(SpriteType.Tiles, "Sprites\\maptiles353-361.spr", 353, 9);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile363-366.spr", 363, 4);
                    break;
                case 5:
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile367-367.spr", 367, 1);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile370-381.spr", 370, 12);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile382-387.spr", 382, 6);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile388-402.spr", 388, 15);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile403-405.spr", 403, 3);
                    break;
                case 6:
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile406-421.spr", 406, 16);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile422-429.spr", 422, 8);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile430-443.spr", 430, 14);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile444-444.spr", 444, 1);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile445-461.spr", 445, 17);
                    break;
                case 7:
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile462-473.spr", 462, 12);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile474-478.spr", 474, 5);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile479-488.spr", 479, 10);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile489-522.spr", 489, 34);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile523-530.spr", 523, 8);
                    break;
                case 8:
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile531-540.spr", 531, 10);
                    LoadSprite(SpriteType.Tiles, "Sprites\\Tile541-545.spr", 541, 5);
                    break;
                case 9: break;
                /*case 10:
                    loadingStage = "Monsters";
                    LoadSprite(SpriteType.Monster, "Sprites\\Slm.spr", 1000, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\ske.spr", 1100, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Gol.spr", 1200, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Cyc.spr", 1300, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\orc.spr", 1400, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Shopkpr.spr", 1500, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Ant.spr", 1600, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Scp.spr", 1700, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\zom.spr", 1800, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Gandlf.spr", 1900, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Howard.spr", 2000, 56);                 
                    break;
                case 11:
                    LoadSprite(SpriteType.Monster, "Sprites\\Guard.spr", 2100, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Amp.spr", 2200, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Cla.spr", 2300, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\tom.spr", 2400, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Wiliam.spr", 2500, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Kennedy.spr", 2600, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Helb.spr", 2700, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Troll.spr", 2800, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Orge.spr", 2900, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Liche.spr", 3000, 56);
                    break;
                case 12:
                    LoadSprite(SpriteType.Monster, "Sprites\\Demon.spr", 3100, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Unicorn.spr", 3200, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\WereWorlf.spr", 3300, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Dummy.spr", 3400, 56);
                    // 3500 is effect5 (0) - lightning ball
                    LoadSprite(SpriteType.Monster, "Sprites\\GT-Arrow.spr", 3600, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\GT-Cannon.spr", 3700, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\ManaCollector.spr", 3800, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Detector.spr", 3900, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\ESG.spr", 4000, 56);
                    break;
                case 13:
                    LoadSprite(SpriteType.Monster, "Sprites\\GMG.spr", 4100, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\ManaStone.spr", 4200, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\LWB.spr", 4300, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\GHK.spr", 4400, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\GHKABS.spr", 4500, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\TK.spr", 4600, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\BG.spr", 4700, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Stalker.spr", 4800, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Hellclaw.spr", 4900, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Tigerworm.spr", 5000, 56);
                    break;
                case 14:
                    LoadSprite(SpriteType.Monster, "Sprites\\Catapult.spr", 5100, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Gagoyle.spr", 5200, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Beholder.spr", 5300, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\DarkElf.spr", 5400, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Bunny.spr", 5500, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Cat.spr", 5600, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\GiantFrog.spr", 5700, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\MTGiant.spr", 5800, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Ettin.spr", 5900, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\CanPlant.spr", 6000, 56);
                    break;
                case 15:
                    LoadSprite(SpriteType.Monster, "Sprites\\Rudolph.spr", 6100, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\DireBoar.spr", 6200, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\frost.spr", 6300, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Crop.spr", 6400, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\IceGolem.spr", 6500, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Wyvern.spr", 6600, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\McGaffin.spr", 6700, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Perry.spr", 6800, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Devlin.spr", 6900, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Barlog.spr", 7000, 56);
                    break;
                case 16:
                    LoadSprite(SpriteType.Monster, "Sprites\\Centaurus.spr", 7100, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\ClawTurtle.spr", 7200, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\FireWyvern.spr", 7300, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\GiantCrayfish.spr", 7400, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\GiantLizard.spr", 7500, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\GiantPlant.spr", 7600, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\MasterMageOrc.spr", 7700, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Minotaurs.spr", 7800, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Nizie.spr", 7900, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Tentocle.spr", 8000, 56);
                    break;
                case 17:
                    LoadSprite(SpriteType.Monster, "Sprites\\yspro.spr", 8100, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Sorceress.spr", 8200, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\TPKnight.spr", 8300, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\ElfMaster.spr", 8400, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\DarkKnight.spr", 8500, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\HBTank.spr", 8600, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\CBTurret.spr", 8700, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Babarian.spr", 8800, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\ACannon.spr", 8900, 56);
                    LoadSprite(SpriteType.Monster, "Sprites\\Gail.spr", 9000, 56);
                    break;
                case 18:
                    LoadSprite(SpriteType.Monster, "Sprites\\Gate.spr", 9100, 56);
                    break;*/
                case 21:
                    loadingStage = "Female Armour";
                    //LoadSprite(SpriteType.Equipment, "Sprites\\WBodice1.spr", (int)SpriteId.FemaleBody + 15 * 1, 12);
                    //LoadSprite(SpriteType.Equipment, "Sprites\\WBodice2.spr", (int)SpriteId.FemaleBody + 15 * 2, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WLArmor.spr", (int)SpriteId.FemaleBody + 15 * 3, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WCMail.spr", (int)SpriteId.FemaleBody + 15 * 4, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WSMail.spr", (int)SpriteId.FemaleBody + 15 * 5, 12);
                    break;
                case 22:
                    //LoadSprite(SpriteType.Armour, "Sprites\\WPMail.spr", (int)SpriteId.FemaleBody + 15 * 6, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WRobe1.spr", (int)SpriteId.FemaleBody + 15 * 7, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WSanta.spr", (int)SpriteId.FemaleBody + 15 * 8, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHRobe1.spr", (int)SpriteId.FemaleBody + 15 * 11, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHRobe2.spr", (int)SpriteId.FemaleBody + 15 * 12, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHLArmor1.spr", (int)SpriteId.FemaleBody + 15 * 13, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHLArmor2.spr", (int)SpriteId.FemaleBody + 15 * 14, 12);
                    break;
                case 23:
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHBPlate1.spr", (int)SpriteId.FemaleBody + 15 * 15, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHBPlate2.spr", (int)SpriteId.FemaleBody + 15 * 16, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHPMail1.spr", (int)SpriteId.FemaleBody + 15 * 9, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHPMail2.spr", (int)SpriteId.FemaleBody + 15 * 10, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WChemiss.spr", (int)SpriteId.FemaleArms + 15 * 1, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WShirt.spr", (int)SpriteId.FemaleArms + 15 * 2, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHauberk.spr", (int)SpriteId.FemaleArms + 15 * 3, 12);
                    break;
                case 24:
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHHauberk1.spr", (int)SpriteId.FemaleArms + 15 * 4, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHHauberk2.spr", (int)SpriteId.FemaleArms + 15 * 5, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WSkirt.spr", (int)SpriteId.FemaleLegs + 15 * 1, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WTrouser.spr", (int)SpriteId.FemaleLegs + 15 * 2, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHTrouser.spr", (int)SpriteId.FemaleLegs + 15 * 3, 12);
                    break;
                case 25:
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHLeggings1.spr", (int)SpriteId.FemaleLegs + 15 * 6, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHLeggings2.spr", (int)SpriteId.FemaleLegs + 15 * 7, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WCHoses.spr", (int)SpriteId.FemaleLegs + 15 * 4, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WLeggings.spr", (int)SpriteId.FemaleLegs + 15 * 5, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WShoes.spr", (int)SpriteId.FemaleBoots + 15 * 1, 12);
                    break;
                case 26:
                    LoadSprite(SpriteType.Armour, "Sprites\\WLBoots.spr", (int)SpriteId.FemaleBoots + 15 * 2, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHelm1.spr", (int)SpriteId.FemaleHead + 15 * 1, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHelm4.spr", (int)SpriteId.FemaleHead + 15 * 4, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\NWHelm1.spr", (int)SpriteId.FemaleHead + 15 * 5, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\NWHelm2.spr", (int)SpriteId.FemaleHead + 15 * 6, 12);
                    break;
                case 27:
                    //LoadSprite(SpriteType.Armour, "Sprites\\NWHelm3.spr", (int)SpriteId.FemaleHead + 15 * 7, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\NWHelm4.spr", (int)SpriteId.FemaleHead + 15 * 8, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHHelm1.spr", (int)SpriteId.FemaleHead + 15 * 9, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHHelm2.spr", (int)SpriteId.FemaleHead + 15 * 10, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHCap1.spr", (int)SpriteId.FemaleHead + 15 * 11, 12);
                    break;
                case 28:
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHCap2.spr", (int)SpriteId.FemaleHead + 15 * 12, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHHood1.spr", (int)SpriteId.FemaleHead + 15 * 13, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WHHood2.spr", (int)SpriteId.FemaleHead + 15 * 14, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\WBCap.spr", (int)SpriteId.FemaleHead + 15 * 15, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\Wmantle01.spr", (int)SpriteId.FemaleCape + 15 * 1, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\Wmantle02.spr", (int)SpriteId.FemaleCape + 15 * 2, 12);
                    LoadSprite(SpriteType.Armour, "Sprites\\Wmantle03.spr", (int)SpriteId.FemaleCape + 15 * 3, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\Wmantle04.spr", (int)SpriteId.FemaleCape + 15 * 4, 12);
                    break;
                case 29:
                    //LoadSprite(SpriteType.Armour, "Sprites\\Wmantle05.spr", (int)SpriteId.FemaleCape + 15 * 5, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\Wmantle06.spr", (int)SpriteId.FemaleCape + 15 * 6, 12);
                    LoadSprite(SpriteType.HairAndUndies, "Sprites\\Wpt.spr", (int)SpriteId.FemaleUndies, 96);
                    LoadSprite(SpriteType.HairAndUndies, "Sprites\\Whr.spr", (int)SpriteId.FemaleHair, 96);
                    break;
                case 31:
                    loadingStage = "Male Armour";
                    //LoadSprite(SpriteType.Armour, "Sprites\\MLArmor.spr", (int)SpriteId.MaleBody + 15 * 1, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MCMail.spr", (int)SpriteId.MaleBody + 15 * 2, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MSMail.spr", (int)SpriteId.MaleBody + 15 * 3, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MPMail.spr", (int)SpriteId.MaleBody + 15 * 4, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\Mtunic.spr", (int)SpriteId.MaleBody + 15 * 5, 12);
                    break;
                case 32:
                    //LoadSprite(SpriteType.Armour, "Sprites\\MRobe1.spr", (int)SpriteId.MaleBody + 15 * 6, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MSanta.spr", (int)SpriteId.MaleBody + 15 * 7, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHPMail1.spr", (int)SpriteId.MaleBody + 15 * 8, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHPMail2.spr", (int)SpriteId.MaleBody + 15 * 9, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHRobe1.spr", (int)SpriteId.MaleBody + 15 * 10, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHRobe2.spr", (int)SpriteId.MaleBody + 15 * 11, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHLArmor1.spr", (int)SpriteId.MaleBody + 15 * 12, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHLArmor2.spr", (int)SpriteId.MaleBody + 15 * 13, 12);
                    break;
                case 33:
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHBPlate1.spr", (int)SpriteId.MaleBody + 15 * 14, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHBPlate2.spr", (int)SpriteId.MaleBody + 15 * 15, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MShirt.spr", (int)SpriteId.MaleArms + 15 * 1, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHauberk.spr", (int)SpriteId.MaleArms + 15 * 2, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHHauberk1.spr", (int)SpriteId.MaleArms + 15 * 3, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHHauberk2.spr", (int)SpriteId.MaleArms + 15 * 4, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MShoes.spr", (int)SpriteId.MaleBoots + 15 * 1, 12);
                    LoadSprite(SpriteType.Armour, "Sprites\\MLBoots.spr", (int)SpriteId.MaleBoots + 15 * 2, 12);
                    break;
                case 34:
                    //LoadSprite(SpriteType.Armour, "Sprites\\MTrouser.spr", (int)SpriteId.MaleLegs + 15 * 1, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHTrouser.spr", (int)SpriteId.MaleLegs + 15 * 2, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MCHoses.spr", (int)SpriteId.MaleLegs + 15 * 3, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MLeggings.spr", (int)SpriteId.MaleLegs + 15 * 4, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHLeggings1.spr", (int)SpriteId.MaleLegs + 15 * 5, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHLeggings2.spr", (int)SpriteId.MaleLegs + 15 * 6, 12);
                    break;
                case 35:
                    //LoadSprite(SpriteType.Armour, "Sprites\\Mmantle01.spr", (int)SpriteId.MaleCape + 15 * 1, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\Mmantle02.spr", (int)SpriteId.MaleCape + 15 * 2, 12);
                    LoadSprite(SpriteType.Armour, "Sprites\\Mmantle03.spr", (int)SpriteId.MaleCape + 15 * 3, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\Mmantle04.spr", (int)SpriteId.MaleCape + 15 * 4, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\Mmantle05.spr", (int)SpriteId.MaleCape + 15 * 5, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\Mmantle06.spr", (int)SpriteId.MaleCape + 15 * 6, 12);
                    break;
                case 36:
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHelm1.spr", (int)SpriteId.MaleHead + 15 * 1, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHelm2.spr", (int)SpriteId.MaleHead + 15 * 2, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHelm3.spr", (int)SpriteId.MaleHead + 15 * 3, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHelm4.spr", (int)SpriteId.MaleHead + 15 * 4, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\NMHelm1.spr", (int)SpriteId.MaleHead + 15 * 5, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\NMHelm2.spr", (int)SpriteId.MaleHead + 15 * 6, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\NMHelm3.spr", (int)SpriteId.MaleHead + 15 * 7, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\NMHelm4.spr", (int)SpriteId.MaleHead + 15 * 8, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHHelm1.spr", (int)SpriteId.MaleHead + 15 * 9, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHHelm2.spr", (int)SpriteId.MaleHead + 15 * 10, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHCap1.spr", (int)SpriteId.MaleHead + 15 * 11, 12);
                    break;
                case 37:
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHCap2.spr", (int)SpriteId.MaleHead + 15 * 12, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHHood1.spr", (int)SpriteId.MaleHead + 15 * 13, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MHHood2.spr", (int)SpriteId.MaleHead + 15 * 14, 12);
                    //LoadSprite(SpriteType.Armour, "Sprites\\MBCap.spr", (int)SpriteId.MaleHead + 15 * 15, 12);
                    break;
                case 38:
                    LoadSprite(SpriteType.HairAndUndies, "Sprites\\Mpt.spr", (int)SpriteId.MaleUndies, 96);
                    LoadSprite(SpriteType.HairAndUndies, "Sprites\\Mhr.spr", (int)SpriteId.MaleHair, 96);
                    break;
                case 40:
                    loadingStage = "Male Weapons";
                    //LoadSprite(SpriteType.Shields, "Sprites\\Msh.spr", (int)SpriteId.MaleShield + 8, 63);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Msw.spr", (int)SpriteId.MaleWeapon + 64, 672);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Mswx.spr", (int)SpriteId.MaleWeapon + 64 * 5, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Msw2.spr", (int)SpriteId.MaleWeapon + 64 * 13, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Msw3.spr", (int)SpriteId.MaleWeapon + 64 * 14, 56);
                    //LoadSprite(SpriteType.Equipment, "Sprites\\Mbo.spr", (int)SpriteId.MaleWeapon + 64 * 40, 56);
                    break;
                case 41:
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MStormBringer.spr", (int)SpriteId.MaleWeapon + 64 * 15, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MDarkExec.spr", (int)SpriteId.MaleWeapon + 64 * 16, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MKlonessBlade.spr", (int)SpriteId.MaleWeapon + 64 * 17, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MKlonessAstock.spr", (int)SpriteId.MaleWeapon + 64 * 18, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MDebastator.spr", (int)SpriteId.MaleWeapon + 64 * 19, 56);
                    break;
                case 42:
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MAxe1.spr", (int)SpriteId.MaleWeapon + 64 * 20, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MAxe2.spr", (int)SpriteId.MaleWeapon + 64 * 21, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MAxe3.spr", (int)SpriteId.MaleWeapon + 64 * 22, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MAxe4.spr", (int)SpriteId.MaleWeapon + 64 * 23, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MAxe5.spr", (int)SpriteId.MaleWeapon + 64 * 24, 56);
                    break;
                case 43:
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MpickAxe1.spr", (int)SpriteId.MaleWeapon + 64 * 25, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MAxe6.spr", (int)SpriteId.MaleWeapon + 64 * 26, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Mhoe.spr", (int)SpriteId.MaleWeapon + 64 * 27, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MKlonessAxe.spr", (int)SpriteId.MaleWeapon + 64 * 28, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MLightBlade.spr", (int)SpriteId.MaleWeapon + 64 * 29, 56);
                    break;
                case 44:
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MHammer.spr", (int)SpriteId.MaleWeapon + 64 * 30, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MBHammer.spr", (int)SpriteId.MaleWeapon + 64 * 31, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MBabHammer.spr", (int)SpriteId.MaleWeapon + 64 * 32, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MBShadowSword.spr", (int)SpriteId.MaleWeapon + 64 * 33, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MBerserkWand.spr", (int)SpriteId.MaleWeapon + 64 * 34, 56);
                    break;
                case 45:
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Mstaff1.spr", (int)SpriteId.MaleWeapon + 64 * 35, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Mstaff2.spr", (int)SpriteId.MaleWeapon + 64 * 36, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MStaff3.spr", (int)SpriteId.MaleWeapon + 64 * 37, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Mstaff4.spr", (int)SpriteId.MaleWeapon + 64 * 51, 56); // battlestaff
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MKlonessWand.spr", (int)SpriteId.MaleWeapon + 64 * 39, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\MReMagicWand.spr", (int)SpriteId.MaleWeapon + 64 * 38, 56);
                    break;
                case 46:
                    //LoadSprite(SpriteType.Bows, "Sprites\\Mbo.spr", (int)SpriteId.MaleWeapon + 64 * 41, 56);
                    //LoadSprite(SpriteType.Bows, "Sprites\\MDirectBow.spr", (int)SpriteId.MaleWeapon + 64 * 42, 56);
                    //LoadSprite(SpriteType.Bows, "Sprites\\MFireBow.spr", (int)SpriteId.MaleWeapon + 64 * 43, 56);
                    break;
                case 60:
                    loadingStage = "Female Weapons";
                    //LoadSprite(SpriteType.Shields, "Sprites\\Wsh.spr", (int)SpriteId.FemaleShield + 8, 63);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Wsw.spr", (int)SpriteId.FemaleWeapon + 64, 672);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Wswx.spr", (int)SpriteId.FemaleWeapon + 64 * 5, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Wsw2.spr", (int)SpriteId.FemaleWeapon + 64 * 13, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Wsw3.spr", (int)SpriteId.FemaleWeapon + 64 * 14, 56);
                    //LoadSprite(SpriteType.Bows, "Sprites\\Wbo.spr", (int)SpriteId.FemaleWeapon + 64 * 40, 56);
                    break;
                case 61:
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WStormBringer.spr", (int)SpriteId.FemaleWeapon + 64 * 15, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WDarkExec.spr", (int)SpriteId.FemaleWeapon + 64 * 16, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WKlonessBlade.spr", (int)SpriteId.FemaleWeapon + 64 * 17, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WKlonessAstock.spr", (int)SpriteId.FemaleWeapon + 64 * 18, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WDebastator.spr", (int)SpriteId.FemaleWeapon + 64 * 19, 56);
                    break;
                case 62:
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WAxe1.spr", (int)SpriteId.FemaleWeapon + 64 * 20, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WAxe2.spr", (int)SpriteId.FemaleWeapon + 64 * 21, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WAxe3.spr", (int)SpriteId.FemaleWeapon + 64 * 22, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WAxe4.spr", (int)SpriteId.FemaleWeapon + 64 * 23, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WAxe5.spr", (int)SpriteId.FemaleWeapon + 64 * 24, 56);
                    break;
                case 63:
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WpickAxe1.spr", (int)SpriteId.FemaleWeapon + 64 * 25, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WAxe6.spr", (int)SpriteId.FemaleWeapon + 64 * 26, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Whoe.spr", (int)SpriteId.FemaleWeapon + 64 * 27, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WKlonessAxe.spr", (int)SpriteId.FemaleWeapon + 64 * 28, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WLightBlade.spr", (int)SpriteId.FemaleWeapon + 64 * 29, 56);
                    break;
                case 64:
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WHammer.spr", (int)SpriteId.FemaleWeapon + 64 * 30, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WBHammer.spr", (int)SpriteId.FemaleWeapon + 64 * 31, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WBabHammer.spr", (int)SpriteId.FemaleWeapon + 64 * 32, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WBShadowSword.spr", (int)SpriteId.FemaleWeapon + 64 * 33, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WBerserkWand.spr", (int)SpriteId.FemaleWeapon + 64 * 34, 56);
                    break;
                case 65:
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Wstaff1.spr", (int)SpriteId.FemaleWeapon + 64 * 35, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Wstaff2.spr", (int)SpriteId.FemaleWeapon + 64 * 36, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WStaff3.spr", (int)SpriteId.FemaleWeapon + 64 * 37, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\Wstaff4.spr", (int)SpriteId.FemaleWeapon + 64 * 51, 56); // battlestaff
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WKlonessWand.spr", (int)SpriteId.FemaleWeapon + 64 * 39, 56);
                    //LoadSprite(SpriteType.Weapons, "Sprites\\WReMagicWand.spr", (int)SpriteId.FemaleWeapon + 64 * 38, 56);
                    break;
                case 66:
                    //LoadSprite(SpriteType.Bows, "Sprites\\Wbo.spr", (int)SpriteId.FemaleWeapon + 64 * 41, 56);
                    //LoadSprite(SpriteType.Bows, "Sprites\\WDirectBow.spr", (int)SpriteId.FemaleWeapon + 64 * 42, 56);
                    //LoadSprite(SpriteType.Bows, "Sprites\\WFireBow.spr", (int)SpriteId.FemaleWeapon + 64 * 43, 56);
                    break;
                case 70:
                    loadingStage = "Effects";
                    LoadSprite(SpriteType.Effect, "Sprites\\effect.spr", 0, 10);
                    LoadSprite(SpriteType.Effect, "Sprites\\effect2.spr", 10, 3);
                    LoadSprite(SpriteType.Effect, "Sprites\\effect3.spr", 13, 6);
                    break;
                case 71:
                    LoadSprite(SpriteType.Effect, "Sprites\\effect4.spr", 19, 5);
                    LoadSprite(SpriteType.Effect, "Sprites\\effect5.spr", 24, 6, 1); // 0 index is football (ignored)
                    LoadSprite(SpriteType.Effect, "Sprites\\CruEffect1.spr", 31, 9);
                    break;
                case 72:
                    LoadSprite(SpriteType.Effect, "Sprites\\effect6.spr", 40, 5);
                    LoadSprite(SpriteType.Effect, "Sprites\\effect7.spr", 45, 12);
                    LoadSprite(SpriteType.Effect, "Sprites\\effect8.spr", 57, 9);
                    break;
                case 73:
                    LoadSprite(SpriteType.Effect, "Sprites\\effect9.spr", 66, 21);
                    LoadSprite(SpriteType.Effect, "Sprites\\effect10.spr", 87, 2);
                    LoadSprite(SpriteType.Effect, "Sprites\\effect11.spr", 89, 14);
                    break;
                case 74:
                    LoadSprite(SpriteType.Effect, "Sprites\\effect11s.spr", 104, 1);
                    LoadSprite(SpriteType.Effect, "Sprites\\yseffect2.spr", 140, 8);
                    LoadSprite(SpriteType.Effect, "Sprites\\effect12.spr", 148, 4);
                    break;
                case 75:
                    LoadSprite(SpriteType.Effect, "Sprites\\yseffect3.spr", 152, 16);
                    LoadSprite(SpriteType.Effect, "Sprites\\yseffect4.spr", 133, 7);
                    break;
                case 76:
                    Cache.LightningSegment = Content.Load<Texture2D>("LightningSegment");
			        Cache.LightningEnd = Content.Load<Texture2D>("LightningEnd");
                    break;
                case 78:
                    loadingStage = "Angels";
                    //LoadSprite(SpriteType.Armour, "Sprites\\TutelarAngel1.spr", (int)SpriteId.Angels, 48);
                    //LoadSprite(SpriteType.Armour, "Sprites\\TutelarAngel2.spr", (int)SpriteId.Angels + 50, 48);
                    //LoadSprite(SpriteType.Armour, "Sprites\\TutelarAngel3.spr", (int)SpriteId.Angels + 100, 48);
                    //LoadSprite(SpriteType.Armour, "Sprites\\TutelarAngel4.spr", (int)SpriteId.Angels + 150, 48);
                    break;
                case 79:
                    // removed in Dialogs V2
                    //loadingStage = "Equipment";
                    //LoadSprite(SpriteType.EquipmentPack, "Sprites\\item-equipM.spr", (int)SpriteId.EquipmentPack, 15);
                    //LoadSprite(SpriteType.EquipmentPack, "Sprites\\item-equipW.spr", (int)SpriteId.EquipmentPack + 40, 15);
                    break;
                case 80:
                    loadingStage = "Items";
                    LoadSprite(SpriteType.Armour, "Sprites\\item-ground.spr", (int)SpriteId.ItemGround, 20);
                    LoadSprite(SpriteType.Armour, "Sprites\\item-pack.spr", (int)SpriteId.ItemBag, 27); // v1
                    LoadSprite(SpriteType.Effect, "Sprites\\item-dynamic.spr", (int)SpriteId.DynamicObject, 3);
                    break;
                case 85:
                    loadingStage = "User Interface";
                    LoadSprite(SpriteType.Interface, "Sprites\\dialogsv2.spr", (int)SpriteId.DialogsV2, 8);
                    LoadSprite(SpriteType.Interface, "Sprites\\GameDialog.spr", (int)SpriteId.Interface1, 11);
                    LoadSprite(SpriteType.Interface, "Sprites\\DialogText.spr", (int)SpriteId.InterfaceText, 2);
                    LoadSprite(SpriteType.Interface, "Sprites\\interface.spr", (int)SpriteId.Interface5, 2);
                    LoadSprite(SpriteType.Effect, "Sprites\\Blood.spr", (int)SpriteId.Blood, 3);

                    // telescopes are replace by auto minimap generation
                    //LoadSprite(SpriteType.Interface, "Sprites\\Telescope.spr", (int)SpriteId.InterfaceMiniMap, 32);
                    //LoadSprite(SpriteType.Interface, "Sprites\\Telescope2.spr", (int)SpriteId.InterfaceMiniMap+35, 4);
                    break;
                case 90: // load configs
                    LoadSpriteConfiguration();
                    LoadItemConfiguration();
                    LoadMagicConfiguration();
                    LoadNpcConfiguration();
                    break;
                case 95: // draw sprites manually
                    ImageConverter converter = new ImageConverter();
                    Animation anim;
                    byte[] imageData;

                    // Minimap Marker
                    using (Bitmap bitmap = new Bitmap(7, 7))
                    {
                        using (Graphics g = Graphics.FromImage(bitmap))
                        using (Brush b = new SolidBrush(System.Drawing.Color.LightGray))
                            g.FillEllipse(b, 0, 0, 6, 6);
                        imageData = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));

                        lock (loadingLock)
                        {
                            using (Stream stream = new MemoryStream(imageData))
                                anim = new Animation(Texture2D.FromStream(GraphicsDevice, stream));
                            anim.Frames.Add(new AnimationFrame(0, 0, bitmap.Width - 1, bitmap.Height - 1, 0, 0));
                            Cache.Interface.Add((int)SpriteId.InterfaceMiniMapMarker, anim);
                        }
                    }


                    break;
                case 96:
                    loadingStage = "Music";
                    try
                    {
                        foreach (string s in Directory.GetFiles(Content.RootDirectory + "\\Music\\"))
                        {
                            string name = Path.GetFileNameWithoutExtension(s);
                            if (!Cache.Music.ContainsKey(name))
                            {
                                Cache.Music.Add(name, Content.Load<SoundEffect>(string.Format("Music\\{0}", name)));
                            }
                        }
                    }
                    catch { }
                    break;
                case 97:
                    loadingStage = "Sounds";
                    try
                    {
                        for (int i = 1; i <= 24; i++)
                            if (File.Exists(Content.RootDirectory + string.Format("\\Sounds\\C{0}.xnb", i)))
                            {
                                SoundEffect e = Content.Load<SoundEffect>(string.Format("Sounds\\C{0}", i));
                                Cache.SoundEffects.Add(string.Format("C{0}", i), e); //Object reference not set to an instance of an object.
                            }
                    }
                    catch { }
                    break;
                case 98:
                    loadingStage = "Sounds";
                    try
                    {
                        for (int i = 1; i <= 156; i++)
                            if (File.Exists(Content.RootDirectory + string.Format("\\Sounds\\M{0}.xnb", i)))
                            {
                                SoundEffect e = Content.Load<SoundEffect>(string.Format("Sounds\\M{0}", i));
                                Cache.SoundEffects.Add(string.Format("M{0}", i), e); //randomly get error here - TODO figureout why? Object reference not set to an instance of an object.
                            }
                    }
                    catch { }
                    break;
                case 99:
                    loadingStage = "Sounds";
                    try
                    {
                        for (int i = 1; i <= 53; i++)
                            if (File.Exists(Content.RootDirectory + string.Format("\\Sounds\\E{0}.xnb", i)))
                            {
                                SoundEffect e = Content.Load<SoundEffect>(string.Format("Sounds\\E{0}", i));
                                Cache.SoundEffects.Add(string.Format("E{0}", i), e);
                            }
                    }
                    catch { }
                    break;
                default:
                    break;
            }
            percentage++;
        }



        private void LoadSprite(SpriteType type, string fileName, int start, int end, int startIndex = 0)
        {
            try
            {
                SpriteFile spriteFile = new SpriteFile(fileName);

                Sprite sprite;
                Stream stream;
                Animation anim;
                Tile tile;

                DateTime timeStart = DateTime.Now;
                end += startIndex;

                int indexModifier = 0, index = 0;
                switch (type)
                {
                    case SpriteType.EquipmentPack:
                        for (int i = 0; i < end; i++)
                            if (spriteFile.Sprites.Count > i)
                            {
                                // TODO - oh my god this is shit
                                if (fileName.Contains("item-equipM"))
                                    switch (i)
                                    {
                                        case 6: index = 7; break;
                                        case 7: index = 8; break;
                                        case 8: index = 9; break;
                                        case 9: index = 18; break;
                                        case 10: index = 19; break;
                                        case 11: index = 15; break;
                                        case 13: index = 20; break;
                                        case 14: index = 21; break;
                                        default: index = i; break;
                                    }
                                else if (fileName.Contains("item-equipW"))
                                    switch (i)
                                    {
                                        case 4: index = 5; break;
                                        case 5: index = 10; break;
                                        case 6: index = 11; break;
                                        case 7: index = 12; break;
                                        case 8: index = 13; break;
                                        case 11: index = 15; break;
                                        case 12: index = 17; break;
                                        case 9: index = 18; break;
                                        case 10: index = 19; break;
                                        case 13: index = 20; break;
                                        case 14: index = 21; break;
                                        default: index = i; break;
                                    }

                                sprite = spriteFile[i + startIndex];

                                if (Cache.Equipment.ContainsKey(start + index + (indexModifier * 15))) break;

                                lock (loadingLock)
                                {
                                    using (stream = new MemoryStream(sprite.ImageData))
                                    {
                                        anim = new Animation(Texture2D.FromStream(GraphicsDevice, stream));
                                    }

                                    foreach (SpriteFrame frame in sprite.Frames)
                                        anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                    Cache.Equipment.Add(start + index, anim);
                                }

                                index++;
                            }
                        break;
                    case SpriteType.HairAndUndies: // does something funky with undies and hair sprite indexes
                        for (int i = 0; i < end; i++)
                            if (spriteFile.Sprites.Count > i)
                            {
                                if (i > 0 && i % 12 == 0)
                                {
                                    index = 0;
                                    indexModifier++; // every 12 animations, split by 15 indexes instead of 12
                                }

                                sprite = spriteFile[i + startIndex];

                                if (Cache.Equipment.ContainsKey(start + index + (indexModifier * 15))) break;

                                lock (loadingLock)
                                {
                                    using (stream = new MemoryStream(sprite.ImageData))
                                    {
                                        anim = new Animation(Texture2D.FromStream(GraphicsDevice, stream));
                                    }

                                    foreach (SpriteFrame frame in sprite.Frames)
                                        anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                    Cache.Equipment.Add(start + index + (indexModifier * 15), anim);
                                }

                                index++;
                            }
                        break;
                    case SpriteType.Bows:
                    case SpriteType.Weapons:
                        for (int i = 0; i < end; i++)
                            if (spriteFile.Sprites.Count > i)
                            {
                                if (i > 0 && i % 56 == 0)
                                {
                                    index = 0;
                                    indexModifier++; // every 56 animations, split by 64 indexes instead of 56
                                }

                                sprite = spriteFile[i + startIndex];

                                lock (loadingLock)
                                {
                                    if (Cache.Equipment.ContainsKey(start + index + (indexModifier * 64))) break;
                                    using (stream = new MemoryStream(sprite.ImageData))
                                    {
                                        anim = new Animation(Texture2D.FromStream(GraphicsDevice, stream));
                                    }
                                    foreach (SpriteFrame frame in sprite.Frames)
                                        anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                    Cache.Equipment.Add(start + index + (indexModifier * 64), anim);
                                }

                                index++;
                            }
                        break;
                    case SpriteType.Shields:
                        for (int i = 0; i < end; i++)
                            if (spriteFile.Sprites.Count > i)
                            {
                                if (i > 0 && i % 7 == 0)
                                {
                                    index = 0;
                                    indexModifier++; // every 7 animations, split by 8 indexes instead of 7
                                }

                                sprite = spriteFile[i + startIndex];

                                if (Cache.Equipment.ContainsKey(start + index + (indexModifier * 8))) break;

                                lock (loadingLock)
                                {
                                    using (stream = new MemoryStream(sprite.ImageData))
                                    {
                                        anim = new Animation(Texture2D.FromStream(GraphicsDevice, stream));
                                    }
                                    foreach (SpriteFrame frame in sprite.Frames)
                                        anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                    Cache.Equipment.Add(start + index + (indexModifier * 8), anim);
                                }

                                index++;
                            }
                        break;
                    default:
                        for (int i = 0; i < end; i++)
                            if (spriteFile.Sprites.Count > i)
                            {
                                sprite = spriteFile[i + startIndex];

                                switch (type)
                                {
                                    case SpriteType.Human:
                                        if (Cache.HumanAnimations.ContainsKey(start + i)) break;
                                        lock (loadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                anim = new Animation(Texture2D.FromStream(GraphicsDevice, stream));
                                            }
                                            foreach (SpriteFrame frame in sprite.Frames)
                                                anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.HumanAnimations.Add(start + i, anim);
                                        }
                                        break;
                                    case SpriteType.Monster:
                                        if (Cache.MonsterAnimations.ContainsKey(start + i)) break;
                                        lock (loadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                anim = new Animation(Texture2D.FromStream(GraphicsDevice, stream));
                                            }
                                            foreach (SpriteFrame frame in sprite.Frames)
                                                anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.MonsterAnimations.Add(start + i, anim);
                                            anim.LoadState = AnimationLoadState.Loaded;
                                        }
                                        break;
                                    case SpriteType.Armour:
                                        if (Cache.Equipment.ContainsKey(start + i)) break;
                                        lock (loadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                anim = new Animation(Texture2D.FromStream(GraphicsDevice, stream));
                                            }

                                            foreach (SpriteFrame frame in sprite.Frames)
                                                anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.Equipment.Add(start + i, anim);
                                        }
                                        break;
                                    case SpriteType.Tiles:
                                        if (Cache.Tiles.ContainsKey(start + i)) break;
                                        lock (loadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                tile = new Tile(Texture2D.FromStream(GraphicsDevice, stream));
                                                tile.MiniMapImage = MapHelper.ResizeImage((System.Drawing.Bitmap)System.Drawing.Image.FromStream(stream), tile.Texture.Width / MapHelper.MiniMapTempScaleFactor, tile.Texture.Height / MapHelper.MiniMapTempScaleFactor);
                                            }
                                            foreach (SpriteFrame frame in sprite.Frames)
                                                tile.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.Tiles.Add(start + i, tile);
                                        }
                                        break;
                                    case SpriteType.Effect:
                                        if (Cache.Effects.ContainsKey(start + i)) break;
                                        lock (loadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                anim = new Animation(Texture2D.FromStream(GraphicsDevice, stream).PreMultiply());
                                            }
                                            foreach (SpriteFrame frame in sprite.Frames)
                                                anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.Effects.Add(start + i, anim);
                                        }
                                        break;
                                    case SpriteType.Interface:
                                        if (Cache.Interface.ContainsKey(start + i)) break;
                                        lock (loadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                anim = new Animation(Texture2D.FromStream(GraphicsDevice, stream).PreMultiply());
                                            }
                                            foreach (SpriteFrame frame in sprite.Frames)
                                                anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.Interface.Add(start + i, anim);
                                        }
                                        break;
                                }

                            }
                        break;
                }
                spriteFile.Dispose();
                TimeSpan timeTaken = DateTime.Now - timeStart;
                Console.WriteLine("Load {0} : {1} seconds", fileName, timeTaken.TotalSeconds);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.ToString());
            }
        }

        public void BringToFront(MenuDialogBoxType type)
        {
            throw new NotImplementedException();
        }

        public void AddEvent(string message)
        {
            throw new NotImplementedException();
        }

        private bool LoadSpriteConfiguration()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\sprites.xml";

                Cache.SpriteConfiguration = SpriteHelper.LoadSpriteConfiguration(configPath);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool LoadItemConfiguration()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\items.xml";

                Cache.ItemConfiguration = ItemHelper.LoadItems(configPath);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool LoadMagicConfiguration()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\magic.xml";

                Cache.MagicConfiguration = MagicHelper.LoadMagic(configPath);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool LoadNpcConfiguration()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\npc.xml";

                Cache.NpcConfiguration = NpcHelper.LoadNpcs(configPath);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void LoadAnimationMetaData()
        {
            //note delay modifiers
            // attackspeed = speed*12ms
            // fast cast = -13ms
            // freeze - >>2 ms

            loadingStage = "Animation Metadata";

            Cache.AnimationSpeeds = new int[200, 110];
            Cache.AnimationSounds = new string[200, 110];

            // human males
            for (int i = 1; i <= 3; i++)
            {
                Cache.AnimationSpeeds[i, (int)MotionType.Idle] = 120; // was 60, but old code had 14 max frames, and did frame/2 during draw?
                Cache.AnimationSpeeds[i, (int)MotionType.Move] = 70;
                Cache.AnimationSpeeds[i, (int)MotionType.Fly] = 50;
                Cache.AnimationSpeeds[i, (int)MotionType.Run] = 42;
                Cache.AnimationSpeeds[i, (int)MotionType.Attack] = 78;
                Cache.AnimationSpeeds[i, (int)MotionType.Bow] = 78;
                Cache.AnimationSpeeds[i, (int)MotionType.BowAttack] = 78;
                Cache.AnimationSpeeds[i, (int)MotionType.Dash] = 78;
                Cache.AnimationSpeeds[i, (int)MotionType.Magic] = 88;
                Cache.AnimationSpeeds[i, (int)MotionType.PickUp] = 150;
                Cache.AnimationSpeeds[i, (int)MotionType.TakeDamage] = 70;
                Cache.AnimationSpeeds[i, (int)MotionType.Die] = 80;
                Cache.AnimationSpeeds[i, (int)MotionType.Dead] = 50;
                Cache.AnimationSpeeds[i, (int)MotionType.DeadFadeOut] = 50;

                Cache.AnimationSounds[i, (int)MotionType.TakeDamage] = "C12";
                Cache.AnimationSounds[i, (int)MotionType.Die] = "C14";
            }

            // human females
            for (int i = 4; i <= 6; i++)
            {
                Cache.AnimationSpeeds[i, (int)MotionType.Idle] = 60;
                Cache.AnimationSpeeds[i, (int)MotionType.Move] = 70;
                Cache.AnimationSpeeds[i, (int)MotionType.Fly] = 50;
                Cache.AnimationSpeeds[i, (int)MotionType.Run] = 42;
                Cache.AnimationSpeeds[i, (int)MotionType.Attack] = 78;
                Cache.AnimationSpeeds[i, (int)MotionType.Bow] = 78;
                Cache.AnimationSpeeds[i, (int)MotionType.BowAttack] = 78;
                Cache.AnimationSpeeds[i, (int)MotionType.Dash] = 78;
                Cache.AnimationSpeeds[i, (int)MotionType.Magic] = 88;
                Cache.AnimationSpeeds[i, (int)MotionType.PickUp] = 150;
                Cache.AnimationSpeeds[i, (int)MotionType.TakeDamage] = 70;
                Cache.AnimationSpeeds[i, (int)MotionType.Die] = 80;
                Cache.AnimationSpeeds[i, (int)MotionType.Dead] = 50;
                Cache.AnimationSpeeds[i, (int)MotionType.DeadFadeOut] = 50;

                Cache.AnimationSounds[i, (int)MotionType.TakeDamage] = "C13";
                Cache.AnimationSounds[i, (int)MotionType.Die] = "C15";
            }

            // slime
            Cache.AnimationSpeeds[10, (int)MotionType.Idle] = 240;
            Cache.AnimationSpeeds[10, (int)MotionType.Move] = 120;
            Cache.AnimationSpeeds[10, (int)MotionType.Attack] = 90;
            Cache.AnimationSpeeds[10, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[10, (int)MotionType.Die] = 240;
            Cache.AnimationSpeeds[10, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[10, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[10, (int)MotionType.Move] = "M1";
            Cache.AnimationSounds[10, (int)MotionType.Attack] = "M2";
            Cache.AnimationSounds[10, (int)MotionType.TakeDamage] = "M3";
            Cache.AnimationSounds[10, (int)MotionType.Die] = "M4";


            // skeleton
            Cache.AnimationSpeeds[11, (int)MotionType.Idle] = 150;
            Cache.AnimationSpeeds[11, (int)MotionType.Move] = 90;
            Cache.AnimationSpeeds[11, (int)MotionType.Attack] = 90;
            Cache.AnimationSpeeds[11, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[11, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[11, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[11, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[11, (int)MotionType.Move] = "M13";
            Cache.AnimationSounds[11, (int)MotionType.Attack] = "M14";
            Cache.AnimationSounds[11, (int)MotionType.TakeDamage] = "M15";
            Cache.AnimationSounds[11, (int)MotionType.Die] = "M16";

            // stone-golem
            Cache.AnimationSpeeds[12, (int)MotionType.Idle] = 210;
            Cache.AnimationSpeeds[12, (int)MotionType.Move] = 100;
            Cache.AnimationSpeeds[12, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[12, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[12, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[12, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[12, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[12, (int)MotionType.Move] = "M33";
            Cache.AnimationSounds[12, (int)MotionType.Attack] = "M34";
            Cache.AnimationSounds[12, (int)MotionType.TakeDamage] = "M35";
            Cache.AnimationSounds[12, (int)MotionType.Die] = "M36";

            // cyclops
            Cache.AnimationSpeeds[13, (int)MotionType.Idle] = 210;
            Cache.AnimationSpeeds[13, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[13, (int)MotionType.Attack] = 90;
            Cache.AnimationSpeeds[13, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[13, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[13, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[13, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[13, (int)MotionType.Move] = "M41";
            Cache.AnimationSounds[13, (int)MotionType.Attack] = "M42";
            Cache.AnimationSounds[13, (int)MotionType.TakeDamage] = "M43";
            Cache.AnimationSounds[13, (int)MotionType.Die] = "M44";

            // orc
            Cache.AnimationSpeeds[14, (int)MotionType.Idle] = 180;
            Cache.AnimationSpeeds[14, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[14, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[14, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[14, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[14, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[14, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[14, (int)MotionType.Move] = "M9";
            Cache.AnimationSounds[14, (int)MotionType.Attack] = "M10";
            Cache.AnimationSounds[14, (int)MotionType.TakeDamage] = "M11";
            Cache.AnimationSounds[14, (int)MotionType.Die] = "M12";

            // shopkeeper-w
            Cache.AnimationSpeeds[15, (int)MotionType.Idle] = 180;
            Cache.AnimationSpeeds[15, (int)MotionType.Move] = 100;
            Cache.AnimationSpeeds[15, (int)MotionType.Attack] = 150;
            Cache.AnimationSpeeds[15, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[15, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[15, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[15, (int)MotionType.DeadFadeOut] = 50;

            // giant-ant
            Cache.AnimationSpeeds[16, (int)MotionType.Idle] = 120;
            Cache.AnimationSpeeds[16, (int)MotionType.Move] = 60;
            Cache.AnimationSpeeds[16, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[16, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[16, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[16, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[16, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[16, (int)MotionType.Move] = "M29";
            Cache.AnimationSounds[16, (int)MotionType.Attack] = "M30";
            Cache.AnimationSounds[16, (int)MotionType.TakeDamage] = "M31";
            Cache.AnimationSounds[16, (int)MotionType.Die] = "M32";

            // Scorpion
            Cache.AnimationSpeeds[17, (int)MotionType.Idle] = 120;
            Cache.AnimationSpeeds[17, (int)MotionType.Move] = 45;
            Cache.AnimationSpeeds[17, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[17, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[17, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[17, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[17, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[17, (int)MotionType.Move] = "M21";
            Cache.AnimationSounds[17, (int)MotionType.Attack] = "M22";
            Cache.AnimationSounds[17, (int)MotionType.TakeDamage] = "M23";
            Cache.AnimationSounds[17, (int)MotionType.Die] = "M24";

            // Zombie
            Cache.AnimationSpeeds[18, (int)MotionType.Idle] = 210;
            Cache.AnimationSpeeds[18, (int)MotionType.Move] = 130;
            Cache.AnimationSpeeds[18, (int)MotionType.Attack] = 150;
            Cache.AnimationSpeeds[18, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[18, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[18, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[18, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[18, (int)MotionType.Move] = "M17";
            Cache.AnimationSounds[18, (int)MotionType.Attack] = "M18";
            Cache.AnimationSounds[18, (int)MotionType.TakeDamage] = "M19";
            Cache.AnimationSounds[18, (int)MotionType.Die] = "M20";

            // Gandlf
            Cache.AnimationSpeeds[19, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[19, (int)MotionType.Move] = 100;
            Cache.AnimationSpeeds[19, (int)MotionType.Attack] = 150;
            Cache.AnimationSpeeds[19, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[19, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[19, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[19, (int)MotionType.DeadFadeOut] = 50;

            // Howard
            Cache.AnimationSpeeds[20, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[20, (int)MotionType.Move] = 100;
            Cache.AnimationSpeeds[20, (int)MotionType.Attack] = 150;
            Cache.AnimationSpeeds[20, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[20, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[20, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[20, (int)MotionType.DeadFadeOut] = 50;

            // Guard
            Cache.AnimationSpeeds[21, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[21, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[21, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[21, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[21, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[21, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[21, (int)MotionType.DeadFadeOut] = 50;

            // Snake
            Cache.AnimationSpeeds[22, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[22, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[22, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[22, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[22, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[22, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[22, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[22, (int)MotionType.Move] = "M25";
            Cache.AnimationSounds[22, (int)MotionType.Attack] = "M26";
            Cache.AnimationSounds[22, (int)MotionType.TakeDamage] = "M27";
            Cache.AnimationSounds[22, (int)MotionType.Die] = "M28";

            // Clay-golem
            Cache.AnimationSpeeds[23, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[23, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[23, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[23, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[23, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[23, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[23, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[23, (int)MotionType.Move] = "M37";
            Cache.AnimationSounds[23, (int)MotionType.Attack] = "M38";
            Cache.AnimationSounds[23, (int)MotionType.TakeDamage] = "M39";
            Cache.AnimationSounds[23, (int)MotionType.Die] = "M40";

            // Tom
            Cache.AnimationSpeeds[24, (int)MotionType.Idle] = 150;
            Cache.AnimationSpeeds[24, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[24, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[24, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[24, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[24, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[24, (int)MotionType.DeadFadeOut] = 50;

            // William
            Cache.AnimationSpeeds[25, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[25, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[25, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[25, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[25, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[25, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[25, (int)MotionType.DeadFadeOut] = 50;

            // Kenedy
            Cache.AnimationSpeeds[26, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[26, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[26, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[26, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[26, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[26, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[26, (int)MotionType.DeadFadeOut] = 50;

            // Hellhound
            Cache.AnimationSpeeds[27, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[27, (int)MotionType.Move] = 50;
            Cache.AnimationSpeeds[27, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[27, (int)MotionType.TakeDamage] = 120;
            Cache.AnimationSpeeds[27, (int)MotionType.Die] = 180;
            Cache.AnimationSpeeds[27, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[27, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[27, (int)MotionType.Move] = "M5";
            Cache.AnimationSounds[27, (int)MotionType.Attack] = "M6";
            Cache.AnimationSounds[27, (int)MotionType.TakeDamage] = "M7";
            Cache.AnimationSounds[27, (int)MotionType.Die] = "M8";

            // Troll
            Cache.AnimationSpeeds[28, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[28, (int)MotionType.Move] = 100;
            Cache.AnimationSpeeds[28, (int)MotionType.Attack] = 60;
            Cache.AnimationSpeeds[28, (int)MotionType.TakeDamage] = 120;
            Cache.AnimationSpeeds[28, (int)MotionType.Die] = 100;
            Cache.AnimationSpeeds[28, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[28, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[28, (int)MotionType.Move] = "M46";
            Cache.AnimationSounds[28, (int)MotionType.Attack] = "M47";
            Cache.AnimationSounds[28, (int)MotionType.TakeDamage] = "M48";
            Cache.AnimationSounds[28, (int)MotionType.Die] = "M49";

            // Ogre
            Cache.AnimationSpeeds[29, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[29, (int)MotionType.Move] = 100;
            Cache.AnimationSpeeds[29, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[29, (int)MotionType.TakeDamage] = 120;
            Cache.AnimationSpeeds[29, (int)MotionType.Die] = 100;
            Cache.AnimationSpeeds[29, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[29, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[29, (int)MotionType.Move] = "M51";
            Cache.AnimationSounds[29, (int)MotionType.Attack] = "M52";
            Cache.AnimationSounds[29, (int)MotionType.TakeDamage] = "M53";
            Cache.AnimationSounds[29, (int)MotionType.Die] = "M54";

            // Liche
            Cache.AnimationSpeeds[30, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[30, (int)MotionType.Move] = 100;
            Cache.AnimationSpeeds[30, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[30, (int)MotionType.TakeDamage] = 120;
            Cache.AnimationSpeeds[30, (int)MotionType.Die] = 100;
            Cache.AnimationSpeeds[30, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[30, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[30, (int)MotionType.Move] = "M55";
            Cache.AnimationSounds[30, (int)MotionType.Attack] = "M56";
            Cache.AnimationSounds[30, (int)MotionType.TakeDamage] = "M57";
            Cache.AnimationSounds[30, (int)MotionType.Die] = "M58";

            // Demon
            Cache.AnimationSpeeds[31, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[31, (int)MotionType.Move] = 100;
            Cache.AnimationSpeeds[31, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[31, (int)MotionType.TakeDamage] = 120;
            Cache.AnimationSpeeds[31, (int)MotionType.Die] = 100;
            Cache.AnimationSpeeds[31, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[31, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[31, (int)MotionType.Move] = "M59";
            Cache.AnimationSounds[31, (int)MotionType.Attack] = "M60";
            Cache.AnimationSounds[31, (int)MotionType.TakeDamage] = "M61";
            Cache.AnimationSounds[31, (int)MotionType.Die] = "M62";

            // Unicorn
            Cache.AnimationSpeeds[32, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[32, (int)MotionType.Move] = 100;
            Cache.AnimationSpeeds[32, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[32, (int)MotionType.TakeDamage] = 120;
            Cache.AnimationSpeeds[32, (int)MotionType.Die] = 100;
            Cache.AnimationSpeeds[32, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[32, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[32, (int)MotionType.Move] = "M63";
            Cache.AnimationSounds[32, (int)MotionType.Attack] = "M64";
            Cache.AnimationSounds[32, (int)MotionType.TakeDamage] = "M65";
            Cache.AnimationSounds[32, (int)MotionType.Die] = "M66";

            // Wereworlf
            Cache.AnimationSpeeds[33, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[33, (int)MotionType.Move] = 120;
            Cache.AnimationSpeeds[33, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[33, (int)MotionType.TakeDamage] = 120;
            Cache.AnimationSpeeds[33, (int)MotionType.Die] = 100;
            Cache.AnimationSpeeds[33, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[33, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[33, (int)MotionType.Move] = "M67";
            Cache.AnimationSounds[33, (int)MotionType.Attack] = "M68";
            Cache.AnimationSounds[33, (int)MotionType.TakeDamage] = "M69";
            Cache.AnimationSounds[33, (int)MotionType.Die] = "M70";

            // Dummy
            Cache.AnimationSpeeds[34, (int)MotionType.Idle] = 240;
            Cache.AnimationSpeeds[34, (int)MotionType.Move] = 120;
            Cache.AnimationSpeeds[34, (int)MotionType.Attack] = 90;
            Cache.AnimationSpeeds[34, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[34, (int)MotionType.Die] = 240;
            Cache.AnimationSpeeds[34, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[34, (int)MotionType.DeadFadeOut] = 50;

            Cache.AnimationSounds[34, (int)MotionType.TakeDamage] = "M2";

            // Energy-Ball
            Cache.AnimationSpeeds[35, (int)MotionType.Idle] = 80;
            Cache.AnimationSpeeds[35, (int)MotionType.Move] = 20;
            Cache.AnimationSpeeds[35, (int)MotionType.Attack] = 80;
            Cache.AnimationSpeeds[35, (int)MotionType.TakeDamage] = 80;
            Cache.AnimationSpeeds[35, (int)MotionType.Die] = 80;
            Cache.AnimationSpeeds[35, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[35, (int)MotionType.DeadFadeOut] = 50;

            // Crossbow Guard Tower
            Cache.AnimationSpeeds[36, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[36, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[36, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[36, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[36, (int)MotionType.Die] = 200;
            Cache.AnimationSpeeds[36, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[36, (int)MotionType.DeadFadeOut] = 50;

            // Cannon Guard Tower
            Cache.AnimationSpeeds[37, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[37, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[37, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[37, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[37, (int)MotionType.Die] = 200;
            Cache.AnimationSpeeds[37, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[37, (int)MotionType.DeadFadeOut] = 50;

            // Mana Collector
            Cache.AnimationSpeeds[38, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[38, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[38, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[38, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[38, (int)MotionType.Die] = 200;
            Cache.AnimationSpeeds[38, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[38, (int)MotionType.DeadFadeOut] = 50;

            // Detector
            Cache.AnimationSpeeds[39, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[39, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[39, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[39, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[39, (int)MotionType.Die] = 200;
            Cache.AnimationSpeeds[39, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[39, (int)MotionType.DeadFadeOut] = 50;

            // Energy Shield Generator
            Cache.AnimationSpeeds[40, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[40, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[40, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[40, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[40, (int)MotionType.Die] = 200;
            Cache.AnimationSpeeds[40, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[40, (int)MotionType.DeadFadeOut] = 50;

            // Grand SpellBook Generator
            Cache.AnimationSpeeds[41, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[41, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[41, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[41, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[41, (int)MotionType.Die] = 200;
            Cache.AnimationSpeeds[41, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[41, (int)MotionType.DeadFadeOut] = 50;

            // Mana Stone
            Cache.AnimationSpeeds[42, (int)MotionType.Idle] = 250;
            Cache.AnimationSpeeds[42, (int)MotionType.Move] = 80;
            Cache.AnimationSpeeds[42, (int)MotionType.Attack] = 120;
            Cache.AnimationSpeeds[42, (int)MotionType.TakeDamage] = 150;
            Cache.AnimationSpeeds[42, (int)MotionType.Die] = 200;
            Cache.AnimationSpeeds[42, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[42, (int)MotionType.DeadFadeOut] = 50;

            // wyvern
            Cache.AnimationSpeeds[66, (int)MotionType.Idle] = 100;
            Cache.AnimationSpeeds[66, (int)MotionType.Move] = 90;
            Cache.AnimationSpeeds[66, (int)MotionType.Attack] = 80;
            Cache.AnimationSpeeds[66, (int)MotionType.TakeDamage] = 60;
            Cache.AnimationSpeeds[66, (int)MotionType.Die] = 65;
            Cache.AnimationSpeeds[66, (int)MotionType.Dead] = 50;
            Cache.AnimationSpeeds[66, (int)MotionType.DeadFadeOut] = 50;
        }

        public void AddEffect(IGameEffect e) { AddEffect(e, -1, -1); }
        public void AddEffect(IGameEffect e, int pivotX, int pivotY)
        {
            
        }

        public bool IsComplete { get { return isComplete; } }
        public bool Back { get { return back; } }
        public DefaultState State { get { return DefaultState.Loading; } }
        public GameDisplay Display { get { return display; } set { display = value; } }
        public GraphicsDevice GraphicsDevice { get { return display.Device; } }
        public List<IGameEffect> Effects { get { return null; } set { } }
        public Dictionary<MenuDialogBoxType, IMenuDialogBox> DialogBoxes { get { return dialogBoxes; } }

        public BackGroundImage Background { get { return background; } set { background = value; } }
        public BackGroundImage Backgroundblackbar { get { return backgroundblackbar; } set { backgroundblackbar = value; } }
        public BackGroundImage Legs { get { return legs; } set { legs = value; } }
        public BackGroundImage Weapon { get { return weapon; } set { weapon = value; } }
        public BackGroundImage Tree { get { return tree; } set { tree = value; } }
        public BackGroundImage Grass { get { return grass; } set { grass = value; } }
        public BackGroundImage Grass2 { get { return grass2; } set { grass2 = value; } }
        public BackGroundImage Grass3 { get { return grass3; } set { grass3 = value; } }
        public BackGroundImage Smoke1 { get { return smoke1; } set { smoke1 = value; } }
        public BackGroundImage Smoke2 { get { return smoke2; } set { smoke2 = value; } }
        public BackGroundImage Smoke3 { get { return smoke3; } set { smoke3 = value; } }
        public BackGroundImage Smoke4 { get { return smoke4; } set { smoke4 = value; } }
        public BackGroundImage Smoke5 { get { return smoke5; } set { smoke5 = value; } }
        public BackGroundImage Smoke6 { get { return smoke6; } set { smoke6 = value; } }
        public BackGroundImage Smoke7 { get { return smoke7; } set { smoke7 = value; } }
        public BackGroundImage Smoke8 { get { return smoke8; } set { smoke8 = value; } }
        public BackGroundImage Logo { get { return logo; } set { logo = value; } }
        public BackGroundImage LogoGlow { get { return logoglow; } set { logoglow = value; } }
        public BackGroundImage WarriorHead { get { return warriorhead; } set { warriorhead = value; } }
        public BackGroundImage WarriorBody { get { return warriorbody; } set { warriorbody = value; } }
        public Texture2D Corner { get { return corner; } set { corner = value; } }
        public Texture2D Textbox { get { return textbox; } set { textbox = value; } }
        public Texture2D DialogFader { get { return dialogFader; } set { dialogFader = value; } }
        public Texture2D DialogBorder { get { return dialogBorder; } set { dialogBorder = value; } }
        public LinkedList<BackGroundImage> BackgroundImages { get { return backgroundImages; } set { backgroundImages = value; } }

        public ContentManager Content;
        public LinkedList<MenuDialogBoxType> DialogBoxDrawOrder { get { return dialogBoxDrawOrder; } set { dialogBoxDrawOrder = value; } }
        public bool ResolutionChange { get { return resolutionChange; } set { resolutionChange = value; } }
        public RenderTarget2D MainRenderTarget { get { return mainRenderTarget; } set { mainRenderTarget = value; } }
        public MenuDialogBoxType ClickedDialogBox { get { return clickedDialogBox; } set { clickedDialogBox = value; } }
    }

    public class LoadingStage
    {
        private int stage;

        public LoadingStage(int stage)
        {
            this.stage = stage;
        }

        public int Stage { get { return stage; } }
    }
}

