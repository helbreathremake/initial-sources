﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using HelbreathWorld;
using HelbreathWorld.Common.Assets;

namespace ItemLoader
{
    public partial class ItemLoader : Form
    {
        [XmlArray("Items")]
        [XmlArrayItem("Item", typeof(Item), IsNullable = false)] 
        private List<Item> itemConfig;
        public ItemLoader()
        {
            InitializeComponent();

            itemConfig = new List<Item>();
            dataGridView1.Columns.Add("Name", "Name");
            dataGridView1.Columns.Add("Type", "Type");
            dataGridView1.Columns.Add("EquipType", "EquipType");
            dataGridView1.Columns.Add("Effect", "Effect");
            dataGridView1.Columns.Add("Effect1", "Effect1");
            dataGridView1.Columns.Add("Effect2", "Effect2");
            dataGridView1.Columns.Add("Effect3", "Effect3");
            dataGridView1.Columns.Add("Effect4", "Effect4");
            dataGridView1.Columns.Add("Effect5", "Effect5");
            dataGridView1.Columns.Add("Effect6", "Effect6");
            dataGridView1.Columns.Add("MaxEnd", "MaxEnd");
            dataGridView1.Columns.Add("SpecAblt", "SpecAblt");
            dataGridView1.Columns.Add("Sprite", "Sprite");
            dataGridView1.Columns.Add("SpriteFr", "SpriteFr");
            dataGridView1.Columns.Add("Price", "Price");
            dataGridView1.Columns.Add("Weight", "Weight");
            dataGridView1.Columns.Add("Appr", "Appr");
            dataGridView1.Columns.Add("Speed", "Speed");
            dataGridView1.Columns.Add("Level", "Level");
            dataGridView1.Columns.Add("Gender", "Gender");
            dataGridView1.Columns.Add("SpecEffect1", "SpecEffect1");
            dataGridView1.Columns.Add("SpecEffect2", "SpecEffect2");
            dataGridView1.Columns.Add("Skill", "Skill");
            dataGridView1.Columns.Add("Category", "Category");
            dataGridView1.Columns.Add("Colour", "Colour");
            dataGridView1.Columns.Add("Rarity", "Rarity");
            dataGridView1.Columns.Add("IsForSale", "IsForSale");
        }

        private void StartOpenFile(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void LoadFile(object sender, CancelEventArgs e)
        {
            string filename = openFileDialog1.FileName;

            StreamReader file = new StreamReader(filename);

            string line;
            int itemId = 1;
            while ((line = file.ReadLine()) != null)
            {
                if (line.StartsWith("Item"))
                {

                    string[] tokens = line.Replace('\t', ' ').Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens.Length < 4) continue;
                    Item item = new Item();
                    item.ItemId = itemId;
                    item.Type = (ItemType)Convert.ToInt32(tokens[4]);
                    item.EquipType = (EquipType)Convert.ToInt32(tokens[5]);
                    if (tokens[3].Equals("GuildAdmissionTicket"))
                        item.EffectType = ItemEffectType.GuildAdmission;
                    else if (tokens[3].Equals("GuildSecessionTicket"))
                        item.EffectType = ItemEffectType.GuildDismissal;
                    else item.EffectType = (ItemEffectType)Convert.ToInt32(tokens[6]);
                    item.Effect1 = Convert.ToInt32(tokens[7]);
                    if (tokens[3].Equals("LuckyGoldRing"))
                        item.Effect2 = 10;
                    else item.Effect2 = Convert.ToInt32(tokens[8]);
                    item.Effect3 = Convert.ToInt32(tokens[9]);
                    item.Effect4 = Convert.ToInt32(tokens[10]);
                    item.Effect5 = Convert.ToInt32(tokens[11]);
                    item.Effect6 = Convert.ToInt32(tokens[12]);
                    item.BaseMaximumEndurance = Convert.ToInt32(tokens[13]);
                    if (tokens[3].Equals("DemonSlayer"))
                        item.SpecialAbilityType = ItemSpecialAbilityType.DemonSlayer;
                    else if (tokens[3].Contains("Kloness"))
                        item.SpecialAbilityType = ItemSpecialAbilityType.Kloness;
                    else if (tokens[3].Equals("DarkExecutor"))
                        item.SpecialAbilityType = ItemSpecialAbilityType.Dark;
                    else if (tokens[3].Equals("LightingBlade"))
                        item.SpecialAbilityType = ItemSpecialAbilityType.Light;
                    else if (tokens[3].StartsWith("Berserk"))
                        item.SpecialAbilityType = ItemSpecialAbilityType.Berserk;
                    else item.SpecialAbilityType = (ItemSpecialAbilityType)Convert.ToInt32(tokens[14]);
                    if (item.EffectType == ItemEffectType.AttackMaxHPDown)
                    {
                        item.EffectType = ItemEffectType.Attack;
                        item.SpecialAbilityType = ItemSpecialAbilityType.Blood;
                    }
                    item.Sprite = Convert.ToInt32(tokens[15]);
                    item.SpriteFrame = Convert.ToInt32(tokens[16]);
                    item.Price = Math.Abs(Convert.ToInt32(tokens[17]));
                    item.Weight = Convert.ToInt32(tokens[18]);
                    item.Appearance = Convert.ToInt32(tokens[19]);
                    item.Speed = Convert.ToInt32(tokens[20]);
                    item.LevelLimit = Convert.ToInt32(tokens[21]);
                    item.GenderLimit = (GenderType)Convert.ToInt32(tokens[22]);
                    item.SpecialEffect1 = Convert.ToInt32(tokens[23]);
                    item.SpecialEffect2 = Convert.ToInt32(tokens[24]);
                    switch (Convert.ToInt32(tokens[25]))
                    {
                        case 0:
                            if (tokens[3].Equals("PickAxe"))
                                item.RelatedSkill = SkillType.Mining;
                            else if (tokens[3].Equals("Hoe"))
                                item.RelatedSkill = SkillType.Farming;
                            else item.RelatedSkill = SkillType.None;
                            break;
                        default: item.RelatedSkill = (SkillType)Convert.ToInt32(tokens[25]); break;
                    }
                    //item.Category = Convert.ToInt32(tokens[26]);
                    //item.Colour = Convert.ToInt32(tokens[27]);
                    item.Rarity = GetRarity(tokens[3]);
                    item.IsForSale = (Convert.ToInt32(tokens[17]) > 0);
                    if (tokens[3].Equals("StormBringer"))
                    {
                        item.HasExtendedRange = true;
                        item.MinimumIntelligence = 65;
                    }
                    else item.HasExtendedRange = false;
                    item.MinimumStrength = item.Weight / 10;

                    itemConfig.Add(item);
                    dataGridView1.Rows.Add(
                        new object[]
                    {
                        item.ItemId,
                        item.Type,
                        item.EquipType,
                        item.EffectType ,
                        item.Effect1 ,
                        item.Effect2 ,
                        item.Effect3,
                        item.Effect4 ,
                        item.Effect5 ,
                        item.Effect6 ,
                        item.MaximumEndurance ,
                        item.SpecialAbilityType ,
                        item.Sprite ,
                        item.SpriteFrame ,
                        item.Price ,
                        item.Weight,
                        item.Appearance,
                        item.Speed ,
                        item.LevelLimit ,
                        item.GenderLimit ,
                        item.SpecialEffect1,
                        item.SpecialEffect2 ,
                        item.RelatedSkill,
                        item.Category ,
                        item.Colour,
                        item.Rarity,
                        item.IsForSale
                    });

                    itemId++;
                        
                }
            }


            file.Close();

        }

        private ItemRarity GetRarity(string itemName)
        {
            switch (itemName)
            {
                case "Gold":
                case "GreenPotion":
                case "RedPotion":
                case "BluePotion":
                case "BigGreenPotion":
                case "BigRedPotion":
                case "BigBluePotion":
                case "RedCandy":
                case "BlueCandy":
                case "GreenCandy":
                case "Dagger":
                case "ShortSword":
                case "LightAxe":
                case "Rapier":
                case "MagicWand(MS0)":
                case "WoodShield":
                case "LeatherArmor(M)":
                case "LeatherArmor(W)":
                    return ItemRarity.VeryCommon;
                case "PowerGreenPotion":
                case "MainGauche":
                case "SexonAxe":
                case "Tomahoc":
                case "Sabre":
                case "Esterk":
                case "Scimitar":
                case "Falchion":
                case "MagicWand(MS10)":
                case "TargeShield":
                case "ScaleMail(M)":
                case "ScaleMail(W)":
                case "SlimeJelly":
                case "AntLeg":
                case "AntFeeler":
                case "SnakeMeat":
                case "SnakeSkin":
                case "SnakeTeeth":
                case "SnakeTongue":
                case "OrcMeat":
                case "OrcLeather":
                case "OrcTeeth":
                case "ScorpionPincers":
                case "ScorpionMeat":
                case "ScorpionSting":
                case "ScorpionSkin":
                case "SkeletonBones":
                case "LumpOfCalay":
                case "StoneGolemPiece":
                    return ItemRarity.Common;
                case "LongSword":
                case "DoubleAxe":
                case "GreatSword":
                case "Claymore":
                case "MagicWand(MS20)":
                case "Hauberk(M)":
                case "Hauberk(W)":
                case "ChainHose(M)":
                case "ChainHose(W)":
                case "BlondeShield":
                case "IronShield":
                case "ChainMail(M)":
                case "ChainMail(W)":
                case "LagiShield":
                case "Wizard-Cap(M)":
                case "Wizard-Cap(W)":
                case "Helm(M)":
                case "Helm(W)":
                case "HelboundHeart":
                case "HelboundLeather":
                case "HelboundTail":
                case "HelboundTeeth":
                case "HelboundClaw":
                case "HelboundTongue":
                case "CyclopsEye":
                case "CyclopsHandEdge":
                case "CyclopsHeart":
                case "CyclopsMeat":
                case "CyclopsLeather":
                case "TrollHeart":
                case "TrollMeat":
                case "TrollLeather":
                case "TrollClaw":
                case "OgreHair":
                case "OgreHeart":
                case "OgreMeat":
                case "OgreLeather":
                case "OgreTeeth":
                case "OgreClaw":
                    return ItemRarity.Uncommon;
                case "AcientTablet":
                case "AcientTablet(LU)":
                case "AcientTablet(LD)":
                case "AcientTablet(RU)":
                case "AcientTablet(RD)":
                case "StoneOfXelima":
                case "StoneOfMerien":
                case "ZemstoneofSacrifice":
                case "WarAxe":
                case "Flameberge":
                case "Hammer":
                case "PlateMail(M)":
                case "PlateMail(W)":
                case "Wings-Helm(M)":
                case "Wings-Helm(W)":
                case "Wizard-Hat(M)":
                case "Wizard-Hat(W)":
                case "KnightShield":
                case "TowerShield":
                case "Full-Helm(M)":
                case "Full-Helm(W)":
                case "Cape":
                case "MagicNecklace(RM10)":
                case "MagicWand(M.Shield)":
                case "MagicNecklace(MS10)":
                case "MagicNecklace(DF+10)":
                case "MagicNecklace(DM+1)":
                case "Flameberge+3(LLF)":
                case "BloodRapier":
                case "BloodSword":
                case "BloodAxe":
                case "MagicWand(MS30-LLF)":
                case "LuckyGoldRing":
                case "SapphireRing":
                case "PlatinumRing":
                case "RingofOgrePower":
                case "ResurWand(MS.10)":
                case "BloodyShockW.Manual":
                case "DarkElf-Bow":
                case "DemonHeart":
                case "DemonMeat":
                case "DemonLeather":
                case "DemonEye":
                case "UnicornHeart":
                case "UnicornHorn":
                case "UnicornMeat":
                case "UnicornLeather":
                case "WerewolfTail":
                case "WerewolfHeart":
                case "WerewolfMeat":
                case "WerewolfLeather":
                case "WerewolfTeeth":
                case "WerewolfClaw":
                case "WerewolfNail":
                    return ItemRarity.Rare;
                case "BattleAxe":
                case "Flameberge+1":
                case "GiantSword":
                case "GiantHammer":
                case "Horned-Helm(M)":
                case "Horned-Helm(W)":
                case "RubyRing":
                case "EmeraldRing":
                case "RingofWizard":
                case "RingofMage":
                case "KnecklaceOfIcePro":
                case "KnecklaceOfLightPro":
                case "KnecklaceOfFirePro":
                case "KnecklaceOfPoisonPro":
                case "SwordofIceElemental":
                case "RingofGrandMage":
                case "RingofDemonPower":
                case "Excaliber":
                case "KnecklaceOfStoneGol":
                case "LightingBlade":
                case "4BladeGoldenAxe(LLF)":
                case "IceStormManual":
                case "NecklaceOfLiche":
                case "ResurWand(MS.20)":
                case "MassFireStrikeManual":
                case "DemonSlayer":
                case "BerserkWand(MS.10)":
                case "RingofDragonpower":
                case "DarkExecutor":
                case "GiantBattleHammer":
                    return ItemRarity.VeryRare;
                case "KnecklaceOfSufferent":
                case "KnecklaceOfAirEle":
                case "MerienShield":
                case "KneclaceOfIceEle":
                case "RingoftheXelima":
                case "XelimaRapier":
                case "XelimaBlade":
                case "XelimaAxe":
                case "CancelManual":
                case "SwordOfMedusa":
                case "KnecklaceOfMedusa":
                case "MerienPlateMailM":
                case "MerienPlateMailW":
                case "KneclaceOfMerien":
                case "E.S.W.Manual":
                case "KlonessBlade":
                case "KlonessAxe":
                case "KlonessEsterk":
                case "NecklaceOfKloness":
                case "KlonessWand(MS.20)":
                case "KlonessWand(MS.10)":
                case "RingofArcMage":
                case "BerserkWand(MS.20)":
                case "KnecklaceOfXelima":
                case "RingoftheAbaddon":
                case "I.M.CManual":
                case "StormBringer":
                    return ItemRarity.UltraRare;
                default: return ItemRarity.None;
            }
        }

        private void StartSaveFile(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void ConvertItemConfig(object sender, CancelEventArgs e)
        {
            string filename = saveFileDialog1.FileName;

            TextWriter file = new StreamWriter(filename);

            file.WriteLine("<Items>");

            foreach (Item item in itemConfig)
            {
                StringBuilder line = new StringBuilder();
                line.Append("   <Item Id=\"" + item.ItemId + "\" ");
                line.Append("Type=\"" + Enum.GetName(typeof(ItemType), item.Type) + "\" ");
                line.Append("EquipType=\"" + Enum.GetName(typeof(EquipType), item.EquipType) + "\" ");
                line.Append("EffectType=\"" + Enum.GetName(typeof(ItemEffectType), item.EffectType) + "\" ");
                line.Append("Effect1=\"" + item.Effect1 + "\" ");
                line.Append("Effect2=\"" + item.Effect2 + "\" ");
                line.Append("Effect3=\"" + item.Effect3 + "\" ");
                line.Append("Effect4=\"" + item.Effect4 + "\" ");
                line.Append("Effect5=\"" + item.Effect5 + "\" ");
                line.Append("Effect6=\"" + item.Effect6 + "\" ");
                line.Append("MaximumEndurance=\"" + item.MaximumEndurance + "\" ");
                line.Append("SpecialAbilityType=\"" + Enum.GetName(typeof(ItemSpecialAbilityType), item.SpecialAbilityType) + "\" ");
                line.Append("Sprite=\"" + item.Sprite + "\" ");
                line.Append("SpriteFrame=\"" + item.SpriteFrame + "\" ");
                line.Append("Price=\"" + item.Price + "\" ");
                line.Append("Weight=\"" + item.Weight + "\" ");
                line.Append("Appearance=\"" + item.Appearance + "\" ");
                line.Append("Speed=\"" + item.Speed + "\" ");
                line.Append("LevelLimit=\"" + item.LevelLimit + "\" ");
                line.Append("GenderLimit=\"" +  Enum.GetName(typeof(GenderType), item.GenderLimit) + "\" ");
                line.Append("SpecialEffect1=\"" + item.SpecialEffect1 + "\" ");
                line.Append("SpecialEffect2=\"" + item.SpecialEffect2 + "\" ");
                line.Append("RelatedSkill=\"" + Enum.GetName(typeof(SkillType), item.RelatedSkill) + "\" ");
                line.Append("Category=\"" + item.Category + "\" ");
                line.Append("Colour=\"" + item.Colour + "\" ");
                line.Append("Rarity=\"" + Enum.GetName(typeof(ItemRarity), item.Rarity) + "\" ");
                if (item.IsForSale) line.Append("IsForSale=\"" + item.IsForSale + "\" ");
                if (item.MinimumStrength > 0) line.Append("MinimumStrength=\"" + item.MinimumStrength + "\" ");
                if (item.MinimumDexterity > 0) line.Append("MinimumDexterity=\"" + item.MinimumDexterity + "\" ");
                if (item.MinimumVitality > 0) line.Append("MinimumVitality=\"" + item.MinimumVitality + "\" ");
                if (item.MinimumMagic > 0) line.Append("MinimumMagic=\"" + item.MinimumMagic + "\" ");
                if (item.MinimumIntelligence > 0) line.Append("MinimumIntelligence=\"" + item.MinimumIntelligence + "\" ");
                if (item.MinimumAgility > 0) line.Append("MinimumAgility=\"" + item.MinimumAgility + "\" ");
                if (item.IsUpgradeable)    line.Append("IsUpgradeable=\"True\" ");
                if (item.HasExtendedRange) line.Append("ExtendedRange=\"True\" ");
                line.Append("/>");

                file.WriteLine(line.ToString());
            }

            file.WriteLine("</Items>");

            file.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
