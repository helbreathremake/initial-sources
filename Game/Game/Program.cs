using System;

namespace Game
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] arg)
        {

            bool updated = (arg.Length > 0 &&  arg[0] == "UpToDate") ? true : false;

            using (HelGame game = new HelGame(updated))
            {
                game.Run();
            }
        }
    }
#endif
}

