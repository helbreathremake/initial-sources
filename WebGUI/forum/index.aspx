﻿<%@ Page Title="" Language="C#" MasterPageFile="~/forum/forum.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="forum_index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script src="../jquery/jquery-1.9.1.min.js"></script>
<link href="../styles/jquery.pageslide.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="../jquery/slide/jquery.pageslide.min.js"></script>
    <div id="content">      
        <asp:DataList ID="ForumGroupsList" runat="server" DataKeyField="Id" 
            DataSourceID="ForumGroupsSource" Width="100%" 
            onitemdatabound="UpdateForumList">
            <ItemTemplate>
                <asp:Label ID="lblHiddenId" runat="server" Text='<%# Eval("Id") %>' style="visibility:hidden;display:none;" />
                <div>
                    <table class="forumTable">
                        <tr class="forumGroup">
                            <td class="description" colspan="2">
                                <asp:HyperLink ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' NavigateUrl="#" />
                            </td>
                            <td class="topics">Topics</td>
                            <td class="posts">Posts</td>
                            <td class="lastPost">Last Post</td>
                        </tr>
                        <asp:Repeater ID="ForumsList" runat="server" DataSourceID="ForumSource">
                            <ItemTemplate>
                                <tr class="forum">
                                    <td class="header"></td>
                                    <td class="description">
                                        <asp:HyperLink ID="innerNameLabel2" runat="server" Text='<%# Eval("Name") %>' NavigateUrl="#" />
                                        <br />
                                        <asp:Label ID="innerDescriptionLabel2" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                    </td>
                                    <td class="topics">
                                        <asp:Label ID="innerTopicsLabel2" runat="server" Text='<%# Eval("Topics") %>'></asp:Label>
                                    </td>
                                    <td class="posts">
                                        <asp:Label ID="innerPostsLabel2" runat="server" Text='<%# Eval("Posts") %>'></asp:Label>
                                    </td>
                                    <td class="lastPost">
                                        <asp:Panel ID="pnlLastPost" runat="server" Visible='<%# ((int)Eval("Posts") > 0) %>'>
                                            <asp:Label ID="innerLastPostDescription2" runat="server" Text='<%# (Eval("LastPostDescription").ToString().Length > 20) ? Eval("LastPostDescription").ToString().Substring(0, 20) + "..." : Eval("LastPostDescription").ToString() %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="innerLastPostDate" runat="server" Text='<%# ((int)Eval("Posts") > 0) ? DateTime.Parse(Eval("LastPostDate").ToString()).ToString("ddd MMM d, yyyy hh:mm tt") : "" %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="innerLastPostAccount" runat="server" Text='<%# Eval("LastPostAccount") %>'></asp:Label>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlNoLastPost" runat="server" Visible='<%# ((int)Eval("Posts") <= 0) %>'>
                                            No Posts
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="forumAlternating">
                                    <td class="header"></td>
                                    <td class="description">
                                        <asp:HyperLink ID="innerNameLabel2" runat="server" Text='<%# Eval("Name") %>' NavigateUrl="#" />
                                        <br />
                                        <asp:Label ID="innerDescriptionLabel2" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                    </td>
                                    <td class="topics">
                                        <asp:Label ID="innerTopicsLabel2" runat="server" Text='<%# Eval("Topics") %>'></asp:Label>
                                    </td>
                                    <td class="posts">
                                        <asp:Label ID="innerPostsLabel2" runat="server" Text='<%# Eval("Posts") %>'></asp:Label>
                                    </td>
                                    <td class="lastPost">
                                        <asp:Panel ID="pnlLastPost" runat="server" Visible='<%# ((int)Eval("Posts") > 0) %>'>
                                            <asp:Label ID="innerLastPostDescription2" runat="server" Text='<%# (Eval("LastPostDescription").ToString().Length > 17) ? Eval("LastPostDescription").ToString().Substring(17) + "..." : Eval("LastPostDescription").ToString() %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="innerLastPostDate" runat="server" Text='<%# ((int)Eval("Posts") > 0) ? DateTime.Parse(Eval("LastPostDescription").ToString()).ToString() : "" %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="innerLastPostAccount" runat="server" Text='<%# Eval("LastPostAccount") %>'></asp:Label>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlNoLastPost" runat="server" Visible='<%# ((int)Eval("Posts") <= 0) %>'>
                                            No Posts
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource ID="ForumSource" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:HBWorld %>" 
                            SelectCommand="GetForumList" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:Parameter Name="GroupId" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </table>
                 </div>
            </ItemTemplate>
        </asp:DataList>
        <asp:SqlDataSource ID="ForumGroupsSource" runat="server" 
            ConnectionString="<%$ ConnectionStrings:HBWorld %>" 
            SelectCommand="GetForumGroupList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        
        <br /><br />

        <a href="#modal" class="Post" style="visibility:hidden; display: none;">Post.</a>

        <div id="modal">
            <div id="slideMenu">
                <div class="left">
                    <ul>
                        <li>
                            <a id="buttonClose" href="javascript:$.pageslide.close();">
                                <img src="../images/back_off.png" class="img-swap" alt="Back" style="border: 0px;" />
                            </a>    
                        </li>
                    </ul>
                </div>
                <div class="right">
                    &nbsp;
                </div>
            </div>
        </div>
        <script>
            $("#pageslide").width($(document).width());
            $(".Post").pageslide({ direction: "left", modal: true });
            jQuery(function () {
                $(".img-swap").hover(
                  function () { this.src = this.src.replace("_off", "_on"); },
                  function () {
                      this.src = this.src.replace("_on", "_off");
                  });
                    });
        </script>
    </div>
</asp:Content>

