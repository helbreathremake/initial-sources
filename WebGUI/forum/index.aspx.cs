﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class forum_index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void UpdateForumList(object sender, DataListItemEventArgs e)
    {
        string id = ((Label)e.Item.FindControl("lblHiddenId")).Text;
        Repeater list = (Repeater)e.Item.FindControl("ForumsList");
        SqlDataSource forumSource = (SqlDataSource)e.Item.FindControl("ForumSource");
        forumSource.SelectParameters["GroupId"].DefaultValue = id;
        list.DataBind();        
    }
}