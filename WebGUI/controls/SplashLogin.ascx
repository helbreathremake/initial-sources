﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SplashLogin.ascx.cs" Inherits="controls_SplashLogin" %>

<asp:Panel ID="LoginFormPanel" runat="server" Visible="false">
    <div class="heading">Logged in as: <asp:Label ID="UsernameLabel" runat="server"></asp:Label></div>
    <asp:LinkButton ID="ProfileButton" runat="server" Text="[Profile]" OnClick="ProfileButton_Click"></asp:LinkButton>
    &nbsp;
    <asp:LinkButton ID="LogoutButton" runat="server" Text="[Logout]" OnClick="LogoutButton_Click"></asp:LinkButton>
    <br /><br />
    <asp:Label ID="CharactersLabel" runat="server"></asp:Label>
    <br />
    <asp:Label ID="CharactersList" runat="server"></asp:Label>
</asp:Panel>

<asp:Panel ID="LoggedInPanel" runat="server" Visible="false" CssClass="loginForm">
    <div class="field">
        <asp:TextBox ID="UsernameTextbox" runat="server" placeholder="Username"></asp:TextBox>
    </div>
    <br />
    <div class="field">
        <asp:TextBox ID="PasswordTextbox" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>
    </div>
    <br />
    <asp:LinkButton ID="LoginButton" runat="server" Text="LOGIN" OnClick="LoginButton_Click" />
</asp:Panel>