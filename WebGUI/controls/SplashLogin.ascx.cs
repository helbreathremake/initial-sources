﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using HelbreathWorld.Data;
using HelbreathWorld.Common.Assets;
using HelbreathWorld.Common.Assets.Objects;

public partial class controls_SplashLogin : System.Web.UI.UserControl
{
    public bool LoggedIn { get { return Session["Username"] != null; } }
    public string Username { get { return Session["Username"].ToString(); } set { Session["Username"] = value; } }
    public List<string> Characters { get { return ((string[])Session["CharacterList"]).ToList(); } set { Session["CharacterList"] = value.ToArray(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        Load();
    }

    private void Load()
    {
        if (LoggedIn)
        {
            LoginFormPanel.Visible = true;
            LoggedInPanel.Visible = false;

            UsernameLabel.Text = Username;

            StringBuilder html = new StringBuilder("<table class='characterList'>");
            html.Append("<tr><td>Character</td><td>Level</td></tr>");
            foreach (string character in Characters)
            {
                CharacterData data = new CharacterData(new Connection(ConfigurationManager.ConnectionStrings["HBWorld"].ConnectionString));
                Character c = data.Load(character);
                html.Append("<tr><td><a href='/splash/Character.aspx?n=" + c.Name + "'>");
                html.Append(c.Name);
                html.Append("</a></td><td>");
                html.Append(c.Level);
                html.Append("</td></tr>");
            }
            html.Append("</table>");

            CharactersList.Text = html.ToString();
        }
        else
        {
            LoggedInPanel.Visible = true;
            LoginFormPanel.Visible = false;
        }
    }

    protected void LoginButton_Click(object sender, EventArgs e)
    {
        if (UsernameTextbox.Text.Length < 2) return;
        if (PasswordTextbox.Text.Length < 2) return;

        AccountData data = new AccountData(new Connection(ConfigurationManager.ConnectionStrings["HBWorld"].ConnectionString));
        if (data.LoginExists(UsernameTextbox.Text))
        {
            Account a = data.LoadAccount(UsernameTextbox.Text);

            if (a != null)
            {
                Username = a.Name;

                List<string> characters = new List<string>();
                foreach (string characterName in a.Characters.Keys)
                    characters.Add(characterName);
                this.Characters = characters;

                Load();
            }
        }
    }
    protected void LogoutButton_Click(object sender, EventArgs e)
    {

    }
    protected void ProfileButton_Click(object sender, EventArgs e)
    {

    }
}