﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Splash : System.Web.UI.Page
{
    public bool LoggedIn { get { return Session["Username"] != null; } }
    public string Username { get { return Session["Username"].ToString(); } set { Session["Username"] = value; } }
    public List<string> Characters { get { return ((string[])Session["CharacterList"]).ToList(); } set { Session["CharacterList"] = value.ToArray(); } }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}