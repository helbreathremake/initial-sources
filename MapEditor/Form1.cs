﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

using HelbreathWorld.Game.Assets;

namespace MapEditor
{
    public partial class Form1 : Form
    {
        private static readonly string ConfigurationFileName = "hbmap.config";
        private string installPath;
        private string spriteLocation = @"E:\Program Files\Helbreath USA\SPRITES\";
        private Dictionary<int, LegacySprite> tileSprites = new Dictionary<int, LegacySprite>();
        private Dictionary<int, LegacySprite> objectSprites = new Dictionary<int, LegacySprite>();
        private Map map;
        private Color highlightedCellColour = Color.Blue;
        private Color impassableCellColour = Color.Red;
        private Color passableCellColour = Color.LightGray;
        private Color teleportCellColour = Color.Yellow;
        private Pen penHighlightedCellColour = new Pen(Color.Blue);
        private Pen penHighlightedCellColourSize2 = new Pen(Color.Blue);
        private Pen penImpassableCellColour = new Pen(Color.Red);
        private Pen penPassableCellColour = new Pen(Color.LightGray);
        private Pen penTeleportCellColour = new Pen(Color.Yellow);
        private int spriteErrors;
        private int mouseX, mouseY, startMouseX, startMouseY, scrollX, scrollY;
        private bool draggingEvent;
        private List<Point> selectedTiles;
        private Bitmap bmp; 

        public Form1()
        {
            DoubleBuffered = true;
            InitializeComponent();
            bmp = new Bitmap(panel1.Width, panel1.Height);
            LoadConfig();
            LoadPakFiles();
            panel1.Resize += new EventHandler(OnPanelResized);
            panel1.MouseEnter += new EventHandler(OnMouseEnterPanel1);
            panel1.MouseWheel += new MouseEventHandler(OnMouseScroll);
            vScrollBar1.MouseWheel += new MouseEventHandler(OnMouseScroll);
            hScrollBar1.MouseWheel += new MouseEventHandler(OnMouseScroll);
        }

        private void LoadConfig()
        {
            installPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");

            if (!File.Exists(ConfigurationFileName))
            {
                if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    spriteLocation = folderBrowserDialog1.SelectedPath + @"\";
                    SaveConfig();
                }
            }
            else
            {
                try
                {
                    XmlTextReader config = new XmlTextReader(installPath + @"\" + ConfigurationFileName);

                    while (config.Read())
                        if (config.IsStartElement())
                            switch (config.Name)
                            {
                                case "Mapper":
                                    if (config["SpriteLocation"] != null)
                                    {
                                        spriteLocation = config["SpriteLocation"];
                                        if (!Directory.Exists(spriteLocation) && folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                        {
                                            spriteLocation = folderBrowserDialog1.SelectedPath + @"\";
                                        }
                                    }
                                    else if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                    {
                                        spriteLocation = folderBrowserDialog1.SelectedPath + @"\";
                                    }
                                    if (config["HighlightedCellColour"] != null) penHighlightedCellColour.Color = penHighlightedCellColourSize2.Color = highlightedCellColour = Color.FromName(config["HighlightedCellColour"]);
                                    if (config["ImpassableCellColour"] != null) penImpassableCellColour.Color = impassableCellColour = Color.FromName(config["ImpassableCellColour"]);
                                    if (config["PassableCellColour"] != null) penPassableCellColour.Color = passableCellColour = Color.FromName(config["PassableCellColour"]);
                                    if (config["TeleportCellColour"] != null) penTeleportCellColour.Color = teleportCellColour = Color.FromName(config["TeleportCellColour"]);
                                    SaveConfig();
                                    break;
                            }
                    config.Close();
                }
                catch
                { }
            }
        }

        private void SaveConfig()
        {
            TextWriter file = new StreamWriter(ConfigurationFileName);

            file.WriteLine("<Mapper SpriteLocation=\"" + spriteLocation + "\" " +
                                   "HighlightedCellColour=\"" + highlightedCellColour.Name + "\" " +
                                   "ImpassableCellColour=\"" + impassableCellColour.Name + "\" " +
                                   "PassableCellColour=\"" + passableCellColour.Name + "\" " +
                                   "TeleportCellColour=\"" + teleportCellColour.Name + "\" " +
                                   "/>");

            file.Close();
        }

        private void LoadPakFiles()
        {
            spriteErrors = 0;

            LoadSprite("maptiles1.pak", 0, 32);
            LoadSprite("maptiles2.pak", 300, 15);
            LoadSprite("maptiles4.pak", 320, 10);
            LoadSprite("maptiles5.pak", 330, 19);
            LoadSprite("maptiles6.pak", 349, 4);
            LoadSprite("maptiles353-361.pak", 353, 9);
            LoadSprite("structures1.pak", 50, 5);
            LoadSprite("Sinside1.pak", 70, 27);
            LoadSprite("Trees1.pak", 100, 46);
            LoadSprite("TreeShadows.pak", 150, 46);
            LoadSprite("objects1.pak", 200, 10);
            LoadSprite("objects2.pak", 211, 5);
            LoadSprite("objects3.pak", 216, 4);
            LoadSprite("objects4.pak", 220, 2);
            LoadSprite("objects5.pak", 230, 9);
            LoadSprite("objects6.pak", 238, 4);
            LoadSprite("objects7.pak", 242, 7);
            LoadSprite("Tile223-225.pak", 223, 3);
            LoadSprite("Tile226-229.pak", 226, 4);
            LoadSprite("Tile363-366.pak", 363, 4);
            LoadSprite("Tile367-367.pak", 367, 1);
            LoadSprite("Tile370-381.pak", 370, 12);
            LoadSprite("Tile382-387.pak", 382, 6);
            LoadSprite("Tile388-402.pak", 388, 15);
            LoadSprite("Tile403-405.pak", 403, 3);
            LoadSprite("Tile406-421.pak", 406, 16);
            LoadSprite("Tile422-429.pak", 422, 8);
            LoadSprite("Tile430-443.pak", 430, 14);
            LoadSprite("Tile444-444.pak", 444, 1);
            LoadSprite("Tile445-461.pak", 445, 17);
            LoadSprite("Tile462-473.pak", 462, 12);
            LoadSprite("Tile474-478.pak", 474, 5);
            LoadSprite("Tile479-488.pak", 479, 10);
            LoadSprite("Tile489-522.pak", 489, 34);
            LoadSprite("Tile523-530.pak", 523, 8);
            LoadSprite("Tile531-540.pak", 531, 10);
            LoadSprite("Tile541-545.pak", 541, 5);

            if (spriteErrors > 0) MessageBox.Show("Could not load " + spriteErrors + " sprites. Check all sprites exist in " + spriteLocation);
        }

        private void LoadSprite(string fileName, int start, int count)
        {
            try
            {
                LegacyPakFile pak = new LegacyPakFile(spriteLocation + fileName);
                for (int i = 0; i < count; i++)
                    if (pak.Sprites.ContainsKey(i) && !tileSprites.ContainsKey(start + i))
                        tileSprites.Add(start + i, pak[i]);
            }
            catch
            {
                spriteErrors++;
            }
        }

        private void LoadAMDFile(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void LoadMap(object sender, CancelEventArgs e)
        {
            map = new Map();
            map.Load(openFileDialog1.FileName);
            scrollX = scrollY = 0;
            mouseX = mouseY = startMouseX = startMouseY = -1;

            hScrollBar1.Value = 0;
            hScrollBar1.Minimum = 0;
            hScrollBar1.Maximum = map.Width - (panel1.Width / 32);
            hScrollBar1.Scroll += new ScrollEventHandler(OnScroll);

            vScrollBar1.Value = 0;
            vScrollBar1.Minimum = 0;
            vScrollBar1.Maximum = map.Height - (panel1.Height / 32);
            vScrollBar1.Scroll += new ScrollEventHandler(OnScroll);

            DrawViewPort(0, 0);
            
            pictureBox1.Dock = DockStyle.Fill;
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
            pictureBox1.MouseMove += new MouseEventHandler(OnMouseMove);
            pictureBox1.MouseLeave += new EventHandler(OnMouseLeave);
            pictureBox1.Paint += new PaintEventHandler(OnPaint);
            pictureBox1.MouseDown += new MouseEventHandler(OnMouseDown);
            pictureBox1.MouseUp += new MouseEventHandler(OnMouseUp);
        }

        private void DrawViewPort(int pivotX, int pivotY)
        {
            if (map == null) return;
         
            using (Graphics g = Graphics.FromImage(bmp))
            {
                
                int drawY = 0;
                for (int y = pivotY; y < pivotY + (panel1.Height / 32); y++)
                {
                    if (y > map.Rows.Count-1) break;
                    int drawX = 0;
                    for (int x = pivotX; x < pivotX + (panel1.Width / 32); x++)
                    {
                        if (x > map[y].Tiles.Count-1) break;
                        if (tileSprites.ContainsKey(map[y][x].Sprite))
                            tileSprites[map[y][x].Sprite].DrawFrame(map[y][x].SpriteFrame, g, drawX * 32, drawY * 32);

                        //if (sprites.ContainsKey(map[y][x].ObjectSprite) && map[y][x].ObjectSprite != 0)
                        //g.DrawImage(sprites[map[y][x].ObjectSprite].GetFrame(map[y][x].ObjectSpriteFrame), x * 32, y * 32);

                        if (gridLinesToolStripMenuItem.Checked)
                        {
                            if (map[y][x].IsTeleport)
                                g.DrawRectangle(penTeleportCellColour, (drawX * 32), (drawY * 32), 31, 31);
                            else
                            {
                                if (!map[y][x].IsMoveAllowed)
                                    g.DrawRectangle(penImpassableCellColour, (drawX * 32), (drawY * 32), 31, 31);
                                else g.DrawRectangle(penPassableCellColour, (drawX * 32), (drawY * 32), 31, 31);
                            }
                        }
                        drawX++;
                    }

                    drawY++;
                }
            }

            pictureBox1.Image = bmp;
            //bmp.Dispose();
        }

        private void OnScroll(object sender, ScrollEventArgs e)
        {
            switch (e.ScrollOrientation)
            {
                case ScrollOrientation.HorizontalScroll: scrollX = e.NewValue; break;
                case ScrollOrientation.VerticalScroll: scrollY = e.NewValue; break;
            }
            DrawViewPort(scrollX, scrollY);
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            draggingEvent = true;
            startMouseX = ((e.X + 1) / 32) + scrollX;
            startMouseY = ((e.Y + 1) / 32) + scrollY;
        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            if (draggingEvent)
            {
                selectedTiles = new List<Point>();
                int distanceX = Math.Abs(startMouseX - mouseX);
                int distanceY = Math.Abs(startMouseY - mouseY);

                for (int y = 0; y < distanceY; y++)
                    for (int x = 0; x < distanceX; x++)
                        selectedTiles.Add(new Point(1,1));

                draggingEvent = false;
                startMouseX = startMouseY = -1;
            }
        }

        private void OnPaint(object sender, PaintEventArgs e)
        {
            if (!(mouseX == -1 || mouseY == -1))
            {
                if (draggingEvent)
                {
                    int distanceX = Math.Abs(startMouseX - mouseX);
                    int distanceY = Math.Abs(startMouseY - mouseY);

                    if (startMouseX > mouseX)
                    {
                        if (startMouseY > mouseY)
                            e.Graphics.DrawRectangle(penHighlightedCellColourSize2, (mouseX - scrollX) * 32, (mouseY - scrollY) * 32, (distanceX * 32) , (distanceY * 32) );
                        else e.Graphics.DrawRectangle(penHighlightedCellColourSize2, (mouseX - scrollX) * 32, (startMouseY - scrollY) * 32, (distanceX * 32) , (distanceY * 32) );
                    }
                    else if (startMouseX < mouseX)
                    {
                        if (startMouseY > mouseY)
                            e.Graphics.DrawRectangle(penHighlightedCellColourSize2, (startMouseX - scrollX) * 32, (mouseY - scrollY) * 32, (distanceX * 32) , (distanceY * 32) );
                        else e.Graphics.DrawRectangle(penHighlightedCellColourSize2, (startMouseX - scrollX) * 32, (startMouseY - scrollY) * 32, (distanceX * 32), (distanceY * 32));
                    }
                }
                else e.Graphics.DrawRectangle(penHighlightedCellColourSize2, (mouseX - scrollX) * 32, (mouseY - scrollY) * 32, 32, 32);
            }
        }

        private void OnMouseLeave(object sender, EventArgs e)
        {
            mouseX = -1; mouseY = -1;
            draggingEvent = false;
        }

        private void OnMouseScroll(object sender, MouseEventArgs e)
        {

            if (ModifierKeys.HasFlag(Keys.Control))
            {
                if (e.Delta > 0) { if (scrollY < vScrollBar1.Maximum) vScrollBar1.Value++; scrollY++; }  // If the mouse wheel delta is positive, move the box up.
                if (e.Delta < 0) { if (scrollY > vScrollBar1.Minimum) vScrollBar1.Value--; scrollY--; }  // If the mouse wheel delta is negative, move the box down.
            }
            else
            {
                if (e.Delta > 0) { if (scrollX < hScrollBar1.Maximum) hScrollBar1.Value++; scrollX++; }  // If the mouse wheel delta is positive, move the box up.
                if (e.Delta < 0) { if (scrollX > hScrollBar1.Minimum) hScrollBar1.Value--; scrollX--; }  // If the mouse wheel delta is negative, move the box down.
            }
        }    

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (map != null)
            {
                this.mouseX = ((e.X + 1) / 32) + scrollX;
                this.mouseY = ((e.Y + 1) / 32) + scrollY;
                if (mouseX < map.Width && mouseY < map.Height)
                {
                    toolStripStatusLabel1.Text = "Coords: " + e.X + " (" + mouseX + "), " + e.Y + " (" + mouseY + ")  Sprite: " + map[mouseY][mouseX].Sprite + "   Frame: " + map[mouseY][mouseX].SpriteFrame
                                               + "    ObjectSprite: " + map[mouseY][mouseX].ObjectSprite + "   ObjectFrame: " + map[mouseY][mouseX].ObjectSpriteFrame
                                               + "    Scroll: " + scrollX + ", " + scrollY;
                    pictureBox1.Invalidate();
                }
            }
        }

        private void ShowGridLines(object sender, EventArgs e)
        {
            
        }

        private void Exit(object sender, EventArgs e)
        {
            SaveConfig();
            Close();
        }

        private void GenerateMiniMap(object sender, EventArgs e)
        {
            float scaleX = (float)((double)128 / (double)map.Width);
            float scaleY = (float)((double)128 / (double)map.Height);

            Bitmap bmp = new Bitmap(map.Width * 32, map.Height * 32);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                using (SolidBrush brush = new SolidBrush(Color.FromArgb(0, 0, 0)))
                {
                    g.FillRectangle(brush, 0, 0, map.Width * 32, map.Height * 32);
                }

                for (int y = 0; y < map.Height; y++)
                {
                    for (int x = 0; x < map.Width; x++)
                    {

                        if (tileSprites.ContainsKey(map[y][x].Sprite))
                        {
                            tileSprites[map[y][x].Sprite].DrawFrame(map[y][x].SpriteFrame, g, x * 32, y * 32);
                        }
                    }
                }
            }

            Bitmap bitmap = ResizeBitmap(bmp, 128, 128);

            MiniMap mapForm = new MiniMap();
            mapForm.SetPicture(bitmap);
            mapForm.Show();
        }

        private Bitmap ResizeBitmap(Bitmap sourceBMP, int width, int height)
        {
            Bitmap result = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(result))
                g.DrawImage(sourceBMP, 0, 0, width, height);
            return result;
        }

        private void OnPanelResized(object sender, EventArgs e)
        {
            bmp = new Bitmap(panel1.Width, panel1.Height);
        }

        private void OnMouseEnterPanel1(object sender, EventArgs e)
        {
            panel1.Focus();
        }
    }
}
